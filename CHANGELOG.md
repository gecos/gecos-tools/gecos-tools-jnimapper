## Release version 1.4.0 (22 july 2019)
* Support for InterfaceClass and generics
* Removed JNI prefix and jni sub-package in generated files

## Release version 1.1.0 (13 juin 2018)

* move objStatusUpdate back into finally block, after releasing semaphore
* move objStatusUpdate (calls to taken) to before the JNI call (used to be after)
* changed the placement of taken() calls to ensure semaphores are released
* [CI] gitlab-ci can now use variable 'JENKINSi\_FAIL\_MODE' to specify the behaviour depending on test results:
* [CI] fix Jenkins pipeline configuration
* [CI] deploying a release version now automatically update 'gecos-composite' to the latest version in a dedicated branch.
* fix connection section in pom.xml
* [Releng] update module 'gecos-buildtools' to latest version.

---
