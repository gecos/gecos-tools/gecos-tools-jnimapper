package fr.irisa.cairn.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import fr.irisa.cairn.services.JniMapGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalJniMapParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'extern'", "'static'", "'unsigned'", "'int'", "'long'", "'char'", "'float'", "'double'", "'linux_32'", "'linux_64'", "'macosx_32'", "'macosx_64'", "'mingw_32'", "'cygwin_32'", "'mapping'", "';'", "'('", "'package'", "'='", "')'", "'['", "']'", "','", "'import'", "'<'", "'>'", "'as'", "'extending'", "'implements'", "'unmapped'", "'interface'", "'use'", "'from'", "'library'", "'{'", "'libname'", "'}'", "'include'", "'module'", "'typedef'", "'struct'", "'class'", "'enum'", "'group'", "'this'", "'const'", "'abstract'", "'give'", "'take'", "'keep'", "'constructor'", "'destructor'", "'instanceOf'", "'private'", "'protected'", "'rename'", "'stub'", "'boolean'", "'string'", "'void'", "'*'", "'ignored'", "'&'", "'copyOnGet'", "'many'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__67=67;
    public static final int T__24=24;
    public static final int T__68=68;
    public static final int T__25=25;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__20=20;
    public static final int T__64=64;
    public static final int T__21=21;
    public static final int T__65=65;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__74=74;
    public static final int T__31=31;
    public static final int T__75=75;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalJniMapParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalJniMapParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalJniMapParser.tokenNames; }
    public String getGrammarFileName() { return "InternalJniMap.g"; }


     
     	private JniMapGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(JniMapGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleMapping"
    // InternalJniMap.g:60:1: entryRuleMapping : ruleMapping EOF ;
    public final void entryRuleMapping() throws RecognitionException {
        try {
            // InternalJniMap.g:61:1: ( ruleMapping EOF )
            // InternalJniMap.g:62:1: ruleMapping EOF
            {
             before(grammarAccess.getMappingRule()); 
            pushFollow(FOLLOW_1);
            ruleMapping();

            state._fsp--;

             after(grammarAccess.getMappingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMapping"


    // $ANTLR start "ruleMapping"
    // InternalJniMap.g:69:1: ruleMapping : ( ( rule__Mapping__Group__0 ) ) ;
    public final void ruleMapping() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:73:2: ( ( ( rule__Mapping__Group__0 ) ) )
            // InternalJniMap.g:74:1: ( ( rule__Mapping__Group__0 ) )
            {
            // InternalJniMap.g:74:1: ( ( rule__Mapping__Group__0 ) )
            // InternalJniMap.g:75:1: ( rule__Mapping__Group__0 )
            {
             before(grammarAccess.getMappingAccess().getGroup()); 
            // InternalJniMap.g:76:1: ( rule__Mapping__Group__0 )
            // InternalJniMap.g:76:2: rule__Mapping__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMapping"


    // $ANTLR start "entryRuleImport"
    // InternalJniMap.g:88:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalJniMap.g:89:1: ( ruleImport EOF )
            // InternalJniMap.g:90:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalJniMap.g:97:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:101:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalJniMap.g:102:1: ( ( rule__Import__Group__0 ) )
            {
            // InternalJniMap.g:102:1: ( ( rule__Import__Group__0 ) )
            // InternalJniMap.g:103:1: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // InternalJniMap.g:104:1: ( rule__Import__Group__0 )
            // InternalJniMap.g:104:2: rule__Import__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleClassMapping"
    // InternalJniMap.g:116:1: entryRuleClassMapping : ruleClassMapping EOF ;
    public final void entryRuleClassMapping() throws RecognitionException {
        try {
            // InternalJniMap.g:117:1: ( ruleClassMapping EOF )
            // InternalJniMap.g:118:1: ruleClassMapping EOF
            {
             before(grammarAccess.getClassMappingRule()); 
            pushFollow(FOLLOW_1);
            ruleClassMapping();

            state._fsp--;

             after(grammarAccess.getClassMappingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassMapping"


    // $ANTLR start "ruleClassMapping"
    // InternalJniMap.g:125:1: ruleClassMapping : ( ( rule__ClassMapping__Alternatives ) ) ;
    public final void ruleClassMapping() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:129:2: ( ( ( rule__ClassMapping__Alternatives ) ) )
            // InternalJniMap.g:130:1: ( ( rule__ClassMapping__Alternatives ) )
            {
            // InternalJniMap.g:130:1: ( ( rule__ClassMapping__Alternatives ) )
            // InternalJniMap.g:131:1: ( rule__ClassMapping__Alternatives )
            {
             before(grammarAccess.getClassMappingAccess().getAlternatives()); 
            // InternalJniMap.g:132:1: ( rule__ClassMapping__Alternatives )
            // InternalJniMap.g:132:2: rule__ClassMapping__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ClassMapping__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getClassMappingAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassMapping"


    // $ANTLR start "entryRuleGenericsDeclaration"
    // InternalJniMap.g:144:1: entryRuleGenericsDeclaration : ruleGenericsDeclaration EOF ;
    public final void entryRuleGenericsDeclaration() throws RecognitionException {
        try {
            // InternalJniMap.g:145:1: ( ruleGenericsDeclaration EOF )
            // InternalJniMap.g:146:1: ruleGenericsDeclaration EOF
            {
             before(grammarAccess.getGenericsDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleGenericsDeclaration();

            state._fsp--;

             after(grammarAccess.getGenericsDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGenericsDeclaration"


    // $ANTLR start "ruleGenericsDeclaration"
    // InternalJniMap.g:153:1: ruleGenericsDeclaration : ( ( rule__GenericsDeclaration__Group__0 ) ) ;
    public final void ruleGenericsDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:157:2: ( ( ( rule__GenericsDeclaration__Group__0 ) ) )
            // InternalJniMap.g:158:1: ( ( rule__GenericsDeclaration__Group__0 ) )
            {
            // InternalJniMap.g:158:1: ( ( rule__GenericsDeclaration__Group__0 ) )
            // InternalJniMap.g:159:1: ( rule__GenericsDeclaration__Group__0 )
            {
             before(grammarAccess.getGenericsDeclarationAccess().getGroup()); 
            // InternalJniMap.g:160:1: ( rule__GenericsDeclaration__Group__0 )
            // InternalJniMap.g:160:2: rule__GenericsDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GenericsDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGenericsDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGenericsDeclaration"


    // $ANTLR start "entryRuleGenericsInstantiation"
    // InternalJniMap.g:172:1: entryRuleGenericsInstantiation : ruleGenericsInstantiation EOF ;
    public final void entryRuleGenericsInstantiation() throws RecognitionException {
        try {
            // InternalJniMap.g:173:1: ( ruleGenericsInstantiation EOF )
            // InternalJniMap.g:174:1: ruleGenericsInstantiation EOF
            {
             before(grammarAccess.getGenericsInstantiationRule()); 
            pushFollow(FOLLOW_1);
            ruleGenericsInstantiation();

            state._fsp--;

             after(grammarAccess.getGenericsInstantiationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGenericsInstantiation"


    // $ANTLR start "ruleGenericsInstantiation"
    // InternalJniMap.g:181:1: ruleGenericsInstantiation : ( ( rule__GenericsInstantiation__Group__0 ) ) ;
    public final void ruleGenericsInstantiation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:185:2: ( ( ( rule__GenericsInstantiation__Group__0 ) ) )
            // InternalJniMap.g:186:1: ( ( rule__GenericsInstantiation__Group__0 ) )
            {
            // InternalJniMap.g:186:1: ( ( rule__GenericsInstantiation__Group__0 ) )
            // InternalJniMap.g:187:1: ( rule__GenericsInstantiation__Group__0 )
            {
             before(grammarAccess.getGenericsInstantiationAccess().getGroup()); 
            // InternalJniMap.g:188:1: ( rule__GenericsInstantiation__Group__0 )
            // InternalJniMap.g:188:2: rule__GenericsInstantiation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GenericsInstantiation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGenericsInstantiationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGenericsInstantiation"


    // $ANTLR start "entryRuleInterfaceImplementation"
    // InternalJniMap.g:200:1: entryRuleInterfaceImplementation : ruleInterfaceImplementation EOF ;
    public final void entryRuleInterfaceImplementation() throws RecognitionException {
        try {
            // InternalJniMap.g:201:1: ( ruleInterfaceImplementation EOF )
            // InternalJniMap.g:202:1: ruleInterfaceImplementation EOF
            {
             before(grammarAccess.getInterfaceImplementationRule()); 
            pushFollow(FOLLOW_1);
            ruleInterfaceImplementation();

            state._fsp--;

             after(grammarAccess.getInterfaceImplementationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInterfaceImplementation"


    // $ANTLR start "ruleInterfaceImplementation"
    // InternalJniMap.g:209:1: ruleInterfaceImplementation : ( ( rule__InterfaceImplementation__Group__0 ) ) ;
    public final void ruleInterfaceImplementation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:213:2: ( ( ( rule__InterfaceImplementation__Group__0 ) ) )
            // InternalJniMap.g:214:1: ( ( rule__InterfaceImplementation__Group__0 ) )
            {
            // InternalJniMap.g:214:1: ( ( rule__InterfaceImplementation__Group__0 ) )
            // InternalJniMap.g:215:1: ( rule__InterfaceImplementation__Group__0 )
            {
             before(grammarAccess.getInterfaceImplementationAccess().getGroup()); 
            // InternalJniMap.g:216:1: ( rule__InterfaceImplementation__Group__0 )
            // InternalJniMap.g:216:2: rule__InterfaceImplementation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__InterfaceImplementation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInterfaceImplementationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInterfaceImplementation"


    // $ANTLR start "entryRuleEnumToClassMapping"
    // InternalJniMap.g:228:1: entryRuleEnumToClassMapping : ruleEnumToClassMapping EOF ;
    public final void entryRuleEnumToClassMapping() throws RecognitionException {
        try {
            // InternalJniMap.g:229:1: ( ruleEnumToClassMapping EOF )
            // InternalJniMap.g:230:1: ruleEnumToClassMapping EOF
            {
             before(grammarAccess.getEnumToClassMappingRule()); 
            pushFollow(FOLLOW_1);
            ruleEnumToClassMapping();

            state._fsp--;

             after(grammarAccess.getEnumToClassMappingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEnumToClassMapping"


    // $ANTLR start "ruleEnumToClassMapping"
    // InternalJniMap.g:237:1: ruleEnumToClassMapping : ( ( rule__EnumToClassMapping__Group__0 ) ) ;
    public final void ruleEnumToClassMapping() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:241:2: ( ( ( rule__EnumToClassMapping__Group__0 ) ) )
            // InternalJniMap.g:242:1: ( ( rule__EnumToClassMapping__Group__0 ) )
            {
            // InternalJniMap.g:242:1: ( ( rule__EnumToClassMapping__Group__0 ) )
            // InternalJniMap.g:243:1: ( rule__EnumToClassMapping__Group__0 )
            {
             before(grammarAccess.getEnumToClassMappingAccess().getGroup()); 
            // InternalJniMap.g:244:1: ( rule__EnumToClassMapping__Group__0 )
            // InternalJniMap.g:244:2: rule__EnumToClassMapping__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EnumToClassMapping__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEnumToClassMappingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEnumToClassMapping"


    // $ANTLR start "entryRuleStructToClassMapping"
    // InternalJniMap.g:256:1: entryRuleStructToClassMapping : ruleStructToClassMapping EOF ;
    public final void entryRuleStructToClassMapping() throws RecognitionException {
        try {
            // InternalJniMap.g:257:1: ( ruleStructToClassMapping EOF )
            // InternalJniMap.g:258:1: ruleStructToClassMapping EOF
            {
             before(grammarAccess.getStructToClassMappingRule()); 
            pushFollow(FOLLOW_1);
            ruleStructToClassMapping();

            state._fsp--;

             after(grammarAccess.getStructToClassMappingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStructToClassMapping"


    // $ANTLR start "ruleStructToClassMapping"
    // InternalJniMap.g:265:1: ruleStructToClassMapping : ( ( rule__StructToClassMapping__Group__0 ) ) ;
    public final void ruleStructToClassMapping() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:269:2: ( ( ( rule__StructToClassMapping__Group__0 ) ) )
            // InternalJniMap.g:270:1: ( ( rule__StructToClassMapping__Group__0 ) )
            {
            // InternalJniMap.g:270:1: ( ( rule__StructToClassMapping__Group__0 ) )
            // InternalJniMap.g:271:1: ( rule__StructToClassMapping__Group__0 )
            {
             before(grammarAccess.getStructToClassMappingAccess().getGroup()); 
            // InternalJniMap.g:272:1: ( rule__StructToClassMapping__Group__0 )
            // InternalJniMap.g:272:2: rule__StructToClassMapping__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StructToClassMapping__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStructToClassMappingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStructToClassMapping"


    // $ANTLR start "entryRuleClassToClassMapping"
    // InternalJniMap.g:284:1: entryRuleClassToClassMapping : ruleClassToClassMapping EOF ;
    public final void entryRuleClassToClassMapping() throws RecognitionException {
        try {
            // InternalJniMap.g:285:1: ( ruleClassToClassMapping EOF )
            // InternalJniMap.g:286:1: ruleClassToClassMapping EOF
            {
             before(grammarAccess.getClassToClassMappingRule()); 
            pushFollow(FOLLOW_1);
            ruleClassToClassMapping();

            state._fsp--;

             after(grammarAccess.getClassToClassMappingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassToClassMapping"


    // $ANTLR start "ruleClassToClassMapping"
    // InternalJniMap.g:293:1: ruleClassToClassMapping : ( ( rule__ClassToClassMapping__Group__0 ) ) ;
    public final void ruleClassToClassMapping() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:297:2: ( ( ( rule__ClassToClassMapping__Group__0 ) ) )
            // InternalJniMap.g:298:1: ( ( rule__ClassToClassMapping__Group__0 ) )
            {
            // InternalJniMap.g:298:1: ( ( rule__ClassToClassMapping__Group__0 ) )
            // InternalJniMap.g:299:1: ( rule__ClassToClassMapping__Group__0 )
            {
             before(grammarAccess.getClassToClassMappingAccess().getGroup()); 
            // InternalJniMap.g:300:1: ( rule__ClassToClassMapping__Group__0 )
            // InternalJniMap.g:300:2: rule__ClassToClassMapping__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ClassToClassMapping__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getClassToClassMappingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassToClassMapping"


    // $ANTLR start "entryRuleAliasToClassMapping"
    // InternalJniMap.g:312:1: entryRuleAliasToClassMapping : ruleAliasToClassMapping EOF ;
    public final void entryRuleAliasToClassMapping() throws RecognitionException {
        try {
            // InternalJniMap.g:313:1: ( ruleAliasToClassMapping EOF )
            // InternalJniMap.g:314:1: ruleAliasToClassMapping EOF
            {
             before(grammarAccess.getAliasToClassMappingRule()); 
            pushFollow(FOLLOW_1);
            ruleAliasToClassMapping();

            state._fsp--;

             after(grammarAccess.getAliasToClassMappingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAliasToClassMapping"


    // $ANTLR start "ruleAliasToClassMapping"
    // InternalJniMap.g:321:1: ruleAliasToClassMapping : ( ( rule__AliasToClassMapping__Group__0 ) ) ;
    public final void ruleAliasToClassMapping() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:325:2: ( ( ( rule__AliasToClassMapping__Group__0 ) ) )
            // InternalJniMap.g:326:1: ( ( rule__AliasToClassMapping__Group__0 ) )
            {
            // InternalJniMap.g:326:1: ( ( rule__AliasToClassMapping__Group__0 ) )
            // InternalJniMap.g:327:1: ( rule__AliasToClassMapping__Group__0 )
            {
             before(grammarAccess.getAliasToClassMappingAccess().getGroup()); 
            // InternalJniMap.g:328:1: ( rule__AliasToClassMapping__Group__0 )
            // InternalJniMap.g:328:2: rule__AliasToClassMapping__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAliasToClassMappingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAliasToClassMapping"


    // $ANTLR start "entryRuleUnmappedClass"
    // InternalJniMap.g:340:1: entryRuleUnmappedClass : ruleUnmappedClass EOF ;
    public final void entryRuleUnmappedClass() throws RecognitionException {
        try {
            // InternalJniMap.g:341:1: ( ruleUnmappedClass EOF )
            // InternalJniMap.g:342:1: ruleUnmappedClass EOF
            {
             before(grammarAccess.getUnmappedClassRule()); 
            pushFollow(FOLLOW_1);
            ruleUnmappedClass();

            state._fsp--;

             after(grammarAccess.getUnmappedClassRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnmappedClass"


    // $ANTLR start "ruleUnmappedClass"
    // InternalJniMap.g:349:1: ruleUnmappedClass : ( ( rule__UnmappedClass__Group__0 ) ) ;
    public final void ruleUnmappedClass() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:353:2: ( ( ( rule__UnmappedClass__Group__0 ) ) )
            // InternalJniMap.g:354:1: ( ( rule__UnmappedClass__Group__0 ) )
            {
            // InternalJniMap.g:354:1: ( ( rule__UnmappedClass__Group__0 ) )
            // InternalJniMap.g:355:1: ( rule__UnmappedClass__Group__0 )
            {
             before(grammarAccess.getUnmappedClassAccess().getGroup()); 
            // InternalJniMap.g:356:1: ( rule__UnmappedClass__Group__0 )
            // InternalJniMap.g:356:2: rule__UnmappedClass__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUnmappedClassAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnmappedClass"


    // $ANTLR start "entryRuleInterfaceClass"
    // InternalJniMap.g:368:1: entryRuleInterfaceClass : ruleInterfaceClass EOF ;
    public final void entryRuleInterfaceClass() throws RecognitionException {
        try {
            // InternalJniMap.g:369:1: ( ruleInterfaceClass EOF )
            // InternalJniMap.g:370:1: ruleInterfaceClass EOF
            {
             before(grammarAccess.getInterfaceClassRule()); 
            pushFollow(FOLLOW_1);
            ruleInterfaceClass();

            state._fsp--;

             after(grammarAccess.getInterfaceClassRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInterfaceClass"


    // $ANTLR start "ruleInterfaceClass"
    // InternalJniMap.g:377:1: ruleInterfaceClass : ( ( rule__InterfaceClass__Group__0 ) ) ;
    public final void ruleInterfaceClass() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:381:2: ( ( ( rule__InterfaceClass__Group__0 ) ) )
            // InternalJniMap.g:382:1: ( ( rule__InterfaceClass__Group__0 ) )
            {
            // InternalJniMap.g:382:1: ( ( rule__InterfaceClass__Group__0 ) )
            // InternalJniMap.g:383:1: ( rule__InterfaceClass__Group__0 )
            {
             before(grammarAccess.getInterfaceClassAccess().getGroup()); 
            // InternalJniMap.g:384:1: ( rule__InterfaceClass__Group__0 )
            // InternalJniMap.g:384:2: rule__InterfaceClass__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__InterfaceClass__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInterfaceClassAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInterfaceClass"


    // $ANTLR start "entryRuleExternalLibrary"
    // InternalJniMap.g:396:1: entryRuleExternalLibrary : ruleExternalLibrary EOF ;
    public final void entryRuleExternalLibrary() throws RecognitionException {
        try {
            // InternalJniMap.g:397:1: ( ruleExternalLibrary EOF )
            // InternalJniMap.g:398:1: ruleExternalLibrary EOF
            {
             before(grammarAccess.getExternalLibraryRule()); 
            pushFollow(FOLLOW_1);
            ruleExternalLibrary();

            state._fsp--;

             after(grammarAccess.getExternalLibraryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExternalLibrary"


    // $ANTLR start "ruleExternalLibrary"
    // InternalJniMap.g:405:1: ruleExternalLibrary : ( ( rule__ExternalLibrary__Group__0 ) ) ;
    public final void ruleExternalLibrary() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:409:2: ( ( ( rule__ExternalLibrary__Group__0 ) ) )
            // InternalJniMap.g:410:1: ( ( rule__ExternalLibrary__Group__0 ) )
            {
            // InternalJniMap.g:410:1: ( ( rule__ExternalLibrary__Group__0 ) )
            // InternalJniMap.g:411:1: ( rule__ExternalLibrary__Group__0 )
            {
             before(grammarAccess.getExternalLibraryAccess().getGroup()); 
            // InternalJniMap.g:412:1: ( rule__ExternalLibrary__Group__0 )
            // InternalJniMap.g:412:2: rule__ExternalLibrary__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ExternalLibrary__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExternalLibraryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExternalLibrary"


    // $ANTLR start "entryRuleLibrary"
    // InternalJniMap.g:424:1: entryRuleLibrary : ruleLibrary EOF ;
    public final void entryRuleLibrary() throws RecognitionException {
        try {
            // InternalJniMap.g:425:1: ( ruleLibrary EOF )
            // InternalJniMap.g:426:1: ruleLibrary EOF
            {
             before(grammarAccess.getLibraryRule()); 
            pushFollow(FOLLOW_1);
            ruleLibrary();

            state._fsp--;

             after(grammarAccess.getLibraryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLibrary"


    // $ANTLR start "ruleLibrary"
    // InternalJniMap.g:433:1: ruleLibrary : ( ( rule__Library__Group__0 ) ) ;
    public final void ruleLibrary() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:437:2: ( ( ( rule__Library__Group__0 ) ) )
            // InternalJniMap.g:438:1: ( ( rule__Library__Group__0 ) )
            {
            // InternalJniMap.g:438:1: ( ( rule__Library__Group__0 ) )
            // InternalJniMap.g:439:1: ( rule__Library__Group__0 )
            {
             before(grammarAccess.getLibraryAccess().getGroup()); 
            // InternalJniMap.g:440:1: ( rule__Library__Group__0 )
            // InternalJniMap.g:440:2: rule__Library__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Library__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLibraryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLibrary"


    // $ANTLR start "entryRuleLibraryFileName"
    // InternalJniMap.g:452:1: entryRuleLibraryFileName : ruleLibraryFileName EOF ;
    public final void entryRuleLibraryFileName() throws RecognitionException {
        try {
            // InternalJniMap.g:453:1: ( ruleLibraryFileName EOF )
            // InternalJniMap.g:454:1: ruleLibraryFileName EOF
            {
             before(grammarAccess.getLibraryFileNameRule()); 
            pushFollow(FOLLOW_1);
            ruleLibraryFileName();

            state._fsp--;

             after(grammarAccess.getLibraryFileNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLibraryFileName"


    // $ANTLR start "ruleLibraryFileName"
    // InternalJniMap.g:461:1: ruleLibraryFileName : ( ( rule__LibraryFileName__Group__0 ) ) ;
    public final void ruleLibraryFileName() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:465:2: ( ( ( rule__LibraryFileName__Group__0 ) ) )
            // InternalJniMap.g:466:1: ( ( rule__LibraryFileName__Group__0 ) )
            {
            // InternalJniMap.g:466:1: ( ( rule__LibraryFileName__Group__0 ) )
            // InternalJniMap.g:467:1: ( rule__LibraryFileName__Group__0 )
            {
             before(grammarAccess.getLibraryFileNameAccess().getGroup()); 
            // InternalJniMap.g:468:1: ( rule__LibraryFileName__Group__0 )
            // InternalJniMap.g:468:2: rule__LibraryFileName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LibraryFileName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLibraryFileNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLibraryFileName"


    // $ANTLR start "entryRuleIncludeDef"
    // InternalJniMap.g:480:1: entryRuleIncludeDef : ruleIncludeDef EOF ;
    public final void entryRuleIncludeDef() throws RecognitionException {
        try {
            // InternalJniMap.g:481:1: ( ruleIncludeDef EOF )
            // InternalJniMap.g:482:1: ruleIncludeDef EOF
            {
             before(grammarAccess.getIncludeDefRule()); 
            pushFollow(FOLLOW_1);
            ruleIncludeDef();

            state._fsp--;

             after(grammarAccess.getIncludeDefRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIncludeDef"


    // $ANTLR start "ruleIncludeDef"
    // InternalJniMap.g:489:1: ruleIncludeDef : ( ( rule__IncludeDef__Group__0 ) ) ;
    public final void ruleIncludeDef() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:493:2: ( ( ( rule__IncludeDef__Group__0 ) ) )
            // InternalJniMap.g:494:1: ( ( rule__IncludeDef__Group__0 ) )
            {
            // InternalJniMap.g:494:1: ( ( rule__IncludeDef__Group__0 ) )
            // InternalJniMap.g:495:1: ( rule__IncludeDef__Group__0 )
            {
             before(grammarAccess.getIncludeDefAccess().getGroup()); 
            // InternalJniMap.g:496:1: ( rule__IncludeDef__Group__0 )
            // InternalJniMap.g:496:2: rule__IncludeDef__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__IncludeDef__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIncludeDefAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIncludeDef"


    // $ANTLR start "entryRuleUserModule"
    // InternalJniMap.g:508:1: entryRuleUserModule : ruleUserModule EOF ;
    public final void entryRuleUserModule() throws RecognitionException {
        try {
            // InternalJniMap.g:509:1: ( ruleUserModule EOF )
            // InternalJniMap.g:510:1: ruleUserModule EOF
            {
             before(grammarAccess.getUserModuleRule()); 
            pushFollow(FOLLOW_1);
            ruleUserModule();

            state._fsp--;

             after(grammarAccess.getUserModuleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUserModule"


    // $ANTLR start "ruleUserModule"
    // InternalJniMap.g:517:1: ruleUserModule : ( ( rule__UserModule__Group__0 ) ) ;
    public final void ruleUserModule() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:521:2: ( ( ( rule__UserModule__Group__0 ) ) )
            // InternalJniMap.g:522:1: ( ( rule__UserModule__Group__0 ) )
            {
            // InternalJniMap.g:522:1: ( ( rule__UserModule__Group__0 ) )
            // InternalJniMap.g:523:1: ( rule__UserModule__Group__0 )
            {
             before(grammarAccess.getUserModuleAccess().getGroup()); 
            // InternalJniMap.g:524:1: ( rule__UserModule__Group__0 )
            // InternalJniMap.g:524:2: rule__UserModule__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__UserModule__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUserModuleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUserModule"


    // $ANTLR start "entryRuleCMethod"
    // InternalJniMap.g:536:1: entryRuleCMethod : ruleCMethod EOF ;
    public final void entryRuleCMethod() throws RecognitionException {
        try {
            // InternalJniMap.g:537:1: ( ruleCMethod EOF )
            // InternalJniMap.g:538:1: ruleCMethod EOF
            {
             before(grammarAccess.getCMethodRule()); 
            pushFollow(FOLLOW_1);
            ruleCMethod();

            state._fsp--;

             after(grammarAccess.getCMethodRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCMethod"


    // $ANTLR start "ruleCMethod"
    // InternalJniMap.g:545:1: ruleCMethod : ( ( rule__CMethod__Group__0 ) ) ;
    public final void ruleCMethod() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:549:2: ( ( ( rule__CMethod__Group__0 ) ) )
            // InternalJniMap.g:550:1: ( ( rule__CMethod__Group__0 ) )
            {
            // InternalJniMap.g:550:1: ( ( rule__CMethod__Group__0 ) )
            // InternalJniMap.g:551:1: ( rule__CMethod__Group__0 )
            {
             before(grammarAccess.getCMethodAccess().getGroup()); 
            // InternalJniMap.g:552:1: ( rule__CMethod__Group__0 )
            // InternalJniMap.g:552:2: rule__CMethod__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CMethod__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCMethodAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCMethod"


    // $ANTLR start "entryRuleTypeAliasDeclaration"
    // InternalJniMap.g:566:1: entryRuleTypeAliasDeclaration : ruleTypeAliasDeclaration EOF ;
    public final void entryRuleTypeAliasDeclaration() throws RecognitionException {
        try {
            // InternalJniMap.g:567:1: ( ruleTypeAliasDeclaration EOF )
            // InternalJniMap.g:568:1: ruleTypeAliasDeclaration EOF
            {
             before(grammarAccess.getTypeAliasDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeAliasDeclaration();

            state._fsp--;

             after(grammarAccess.getTypeAliasDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeAliasDeclaration"


    // $ANTLR start "ruleTypeAliasDeclaration"
    // InternalJniMap.g:575:1: ruleTypeAliasDeclaration : ( ( rule__TypeAliasDeclaration__Group__0 ) ) ;
    public final void ruleTypeAliasDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:579:2: ( ( ( rule__TypeAliasDeclaration__Group__0 ) ) )
            // InternalJniMap.g:580:1: ( ( rule__TypeAliasDeclaration__Group__0 ) )
            {
            // InternalJniMap.g:580:1: ( ( rule__TypeAliasDeclaration__Group__0 ) )
            // InternalJniMap.g:581:1: ( rule__TypeAliasDeclaration__Group__0 )
            {
             before(grammarAccess.getTypeAliasDeclarationAccess().getGroup()); 
            // InternalJniMap.g:582:1: ( rule__TypeAliasDeclaration__Group__0 )
            // InternalJniMap.g:582:2: rule__TypeAliasDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__TypeAliasDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeAliasDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeAliasDeclaration"


    // $ANTLR start "entryRuleStructDeclaration"
    // InternalJniMap.g:594:1: entryRuleStructDeclaration : ruleStructDeclaration EOF ;
    public final void entryRuleStructDeclaration() throws RecognitionException {
        try {
            // InternalJniMap.g:595:1: ( ruleStructDeclaration EOF )
            // InternalJniMap.g:596:1: ruleStructDeclaration EOF
            {
             before(grammarAccess.getStructDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleStructDeclaration();

            state._fsp--;

             after(grammarAccess.getStructDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStructDeclaration"


    // $ANTLR start "ruleStructDeclaration"
    // InternalJniMap.g:603:1: ruleStructDeclaration : ( ( rule__StructDeclaration__Group__0 ) ) ;
    public final void ruleStructDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:607:2: ( ( ( rule__StructDeclaration__Group__0 ) ) )
            // InternalJniMap.g:608:1: ( ( rule__StructDeclaration__Group__0 ) )
            {
            // InternalJniMap.g:608:1: ( ( rule__StructDeclaration__Group__0 ) )
            // InternalJniMap.g:609:1: ( rule__StructDeclaration__Group__0 )
            {
             before(grammarAccess.getStructDeclarationAccess().getGroup()); 
            // InternalJniMap.g:610:1: ( rule__StructDeclaration__Group__0 )
            // InternalJniMap.g:610:2: rule__StructDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StructDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStructDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStructDeclaration"


    // $ANTLR start "entryRuleClassDeclaration"
    // InternalJniMap.g:622:1: entryRuleClassDeclaration : ruleClassDeclaration EOF ;
    public final void entryRuleClassDeclaration() throws RecognitionException {
        try {
            // InternalJniMap.g:623:1: ( ruleClassDeclaration EOF )
            // InternalJniMap.g:624:1: ruleClassDeclaration EOF
            {
             before(grammarAccess.getClassDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleClassDeclaration();

            state._fsp--;

             after(grammarAccess.getClassDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassDeclaration"


    // $ANTLR start "ruleClassDeclaration"
    // InternalJniMap.g:631:1: ruleClassDeclaration : ( ( rule__ClassDeclaration__Group__0 ) ) ;
    public final void ruleClassDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:635:2: ( ( ( rule__ClassDeclaration__Group__0 ) ) )
            // InternalJniMap.g:636:1: ( ( rule__ClassDeclaration__Group__0 ) )
            {
            // InternalJniMap.g:636:1: ( ( rule__ClassDeclaration__Group__0 ) )
            // InternalJniMap.g:637:1: ( rule__ClassDeclaration__Group__0 )
            {
             before(grammarAccess.getClassDeclarationAccess().getGroup()); 
            // InternalJniMap.g:638:1: ( rule__ClassDeclaration__Group__0 )
            // InternalJniMap.g:638:2: rule__ClassDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ClassDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getClassDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassDeclaration"


    // $ANTLR start "entryRuleEnumDeclaration"
    // InternalJniMap.g:650:1: entryRuleEnumDeclaration : ruleEnumDeclaration EOF ;
    public final void entryRuleEnumDeclaration() throws RecognitionException {
        try {
            // InternalJniMap.g:651:1: ( ruleEnumDeclaration EOF )
            // InternalJniMap.g:652:1: ruleEnumDeclaration EOF
            {
             before(grammarAccess.getEnumDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleEnumDeclaration();

            state._fsp--;

             after(grammarAccess.getEnumDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEnumDeclaration"


    // $ANTLR start "ruleEnumDeclaration"
    // InternalJniMap.g:659:1: ruleEnumDeclaration : ( ( rule__EnumDeclaration__Group__0 ) ) ;
    public final void ruleEnumDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:663:2: ( ( ( rule__EnumDeclaration__Group__0 ) ) )
            // InternalJniMap.g:664:1: ( ( rule__EnumDeclaration__Group__0 ) )
            {
            // InternalJniMap.g:664:1: ( ( rule__EnumDeclaration__Group__0 ) )
            // InternalJniMap.g:665:1: ( rule__EnumDeclaration__Group__0 )
            {
             before(grammarAccess.getEnumDeclarationAccess().getGroup()); 
            // InternalJniMap.g:666:1: ( rule__EnumDeclaration__Group__0 )
            // InternalJniMap.g:666:2: rule__EnumDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEnumDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEnumDeclaration"


    // $ANTLR start "entryRuleEnumType"
    // InternalJniMap.g:678:1: entryRuleEnumType : ruleEnumType EOF ;
    public final void entryRuleEnumType() throws RecognitionException {
        try {
            // InternalJniMap.g:679:1: ( ruleEnumType EOF )
            // InternalJniMap.g:680:1: ruleEnumType EOF
            {
             before(grammarAccess.getEnumTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleEnumType();

            state._fsp--;

             after(grammarAccess.getEnumTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEnumType"


    // $ANTLR start "ruleEnumType"
    // InternalJniMap.g:687:1: ruleEnumType : ( ( rule__EnumType__Group__0 ) ) ;
    public final void ruleEnumType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:691:2: ( ( ( rule__EnumType__Group__0 ) ) )
            // InternalJniMap.g:692:1: ( ( rule__EnumType__Group__0 ) )
            {
            // InternalJniMap.g:692:1: ( ( rule__EnumType__Group__0 ) )
            // InternalJniMap.g:693:1: ( rule__EnumType__Group__0 )
            {
             before(grammarAccess.getEnumTypeAccess().getGroup()); 
            // InternalJniMap.g:694:1: ( rule__EnumType__Group__0 )
            // InternalJniMap.g:694:2: rule__EnumType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EnumType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEnumTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEnumType"


    // $ANTLR start "entryRuleEnumValue"
    // InternalJniMap.g:706:1: entryRuleEnumValue : ruleEnumValue EOF ;
    public final void entryRuleEnumValue() throws RecognitionException {
        try {
            // InternalJniMap.g:707:1: ( ruleEnumValue EOF )
            // InternalJniMap.g:708:1: ruleEnumValue EOF
            {
             before(grammarAccess.getEnumValueRule()); 
            pushFollow(FOLLOW_1);
            ruleEnumValue();

            state._fsp--;

             after(grammarAccess.getEnumValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // InternalJniMap.g:715:1: ruleEnumValue : ( ( rule__EnumValue__Group__0 ) ) ;
    public final void ruleEnumValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:719:2: ( ( ( rule__EnumValue__Group__0 ) ) )
            // InternalJniMap.g:720:1: ( ( rule__EnumValue__Group__0 ) )
            {
            // InternalJniMap.g:720:1: ( ( rule__EnumValue__Group__0 ) )
            // InternalJniMap.g:721:1: ( rule__EnumValue__Group__0 )
            {
             before(grammarAccess.getEnumValueAccess().getGroup()); 
            // InternalJniMap.g:722:1: ( rule__EnumValue__Group__0 )
            // InternalJniMap.g:722:2: rule__EnumValue__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EnumValue__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEnumValueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleParamDef"
    // InternalJniMap.g:734:1: entryRuleParamDef : ruleParamDef EOF ;
    public final void entryRuleParamDef() throws RecognitionException {
        try {
            // InternalJniMap.g:735:1: ( ruleParamDef EOF )
            // InternalJniMap.g:736:1: ruleParamDef EOF
            {
             before(grammarAccess.getParamDefRule()); 
            pushFollow(FOLLOW_1);
            ruleParamDef();

            state._fsp--;

             after(grammarAccess.getParamDefRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParamDef"


    // $ANTLR start "ruleParamDef"
    // InternalJniMap.g:743:1: ruleParamDef : ( ( rule__ParamDef__Group__0 ) ) ;
    public final void ruleParamDef() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:747:2: ( ( ( rule__ParamDef__Group__0 ) ) )
            // InternalJniMap.g:748:1: ( ( rule__ParamDef__Group__0 ) )
            {
            // InternalJniMap.g:748:1: ( ( rule__ParamDef__Group__0 ) )
            // InternalJniMap.g:749:1: ( rule__ParamDef__Group__0 )
            {
             before(grammarAccess.getParamDefAccess().getGroup()); 
            // InternalJniMap.g:750:1: ( rule__ParamDef__Group__0 )
            // InternalJniMap.g:750:2: rule__ParamDef__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ParamDef__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getParamDefAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParamDef"


    // $ANTLR start "entryRuleMethodGroup"
    // InternalJniMap.g:762:1: entryRuleMethodGroup : ruleMethodGroup EOF ;
    public final void entryRuleMethodGroup() throws RecognitionException {
        try {
            // InternalJniMap.g:763:1: ( ruleMethodGroup EOF )
            // InternalJniMap.g:764:1: ruleMethodGroup EOF
            {
             before(grammarAccess.getMethodGroupRule()); 
            pushFollow(FOLLOW_1);
            ruleMethodGroup();

            state._fsp--;

             after(grammarAccess.getMethodGroupRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMethodGroup"


    // $ANTLR start "ruleMethodGroup"
    // InternalJniMap.g:771:1: ruleMethodGroup : ( ( rule__MethodGroup__Group__0 ) ) ;
    public final void ruleMethodGroup() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:775:2: ( ( ( rule__MethodGroup__Group__0 ) ) )
            // InternalJniMap.g:776:1: ( ( rule__MethodGroup__Group__0 ) )
            {
            // InternalJniMap.g:776:1: ( ( rule__MethodGroup__Group__0 ) )
            // InternalJniMap.g:777:1: ( rule__MethodGroup__Group__0 )
            {
             before(grammarAccess.getMethodGroupAccess().getGroup()); 
            // InternalJniMap.g:778:1: ( rule__MethodGroup__Group__0 )
            // InternalJniMap.g:778:2: rule__MethodGroup__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MethodGroup__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMethodGroupAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMethodGroup"


    // $ANTLR start "entryRuleMethod"
    // InternalJniMap.g:790:1: entryRuleMethod : ruleMethod EOF ;
    public final void entryRuleMethod() throws RecognitionException {
        try {
            // InternalJniMap.g:791:1: ( ruleMethod EOF )
            // InternalJniMap.g:792:1: ruleMethod EOF
            {
             before(grammarAccess.getMethodRule()); 
            pushFollow(FOLLOW_1);
            ruleMethod();

            state._fsp--;

             after(grammarAccess.getMethodRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMethod"


    // $ANTLR start "ruleMethod"
    // InternalJniMap.g:799:1: ruleMethod : ( ( rule__Method__Group__0 ) ) ;
    public final void ruleMethod() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:803:2: ( ( ( rule__Method__Group__0 ) ) )
            // InternalJniMap.g:804:1: ( ( rule__Method__Group__0 ) )
            {
            // InternalJniMap.g:804:1: ( ( rule__Method__Group__0 ) )
            // InternalJniMap.g:805:1: ( rule__Method__Group__0 )
            {
             before(grammarAccess.getMethodAccess().getGroup()); 
            // InternalJniMap.g:806:1: ( rule__Method__Group__0 )
            // InternalJniMap.g:806:2: rule__Method__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Method__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMethodAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMethod"


    // $ANTLR start "entryRuleType"
    // InternalJniMap.g:818:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // InternalJniMap.g:819:1: ( ruleType EOF )
            // InternalJniMap.g:820:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalJniMap.g:827:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:831:2: ( ( ( rule__Type__Alternatives ) ) )
            // InternalJniMap.g:832:1: ( ( rule__Type__Alternatives ) )
            {
            // InternalJniMap.g:832:1: ( ( rule__Type__Alternatives ) )
            // InternalJniMap.g:833:1: ( rule__Type__Alternatives )
            {
             before(grammarAccess.getTypeAccess().getAlternatives()); 
            // InternalJniMap.g:834:1: ( rule__Type__Alternatives )
            // InternalJniMap.g:834:2: rule__Type__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Type__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleBuiltInType"
    // InternalJniMap.g:846:1: entryRuleBuiltInType : ruleBuiltInType EOF ;
    public final void entryRuleBuiltInType() throws RecognitionException {
        try {
            // InternalJniMap.g:847:1: ( ruleBuiltInType EOF )
            // InternalJniMap.g:848:1: ruleBuiltInType EOF
            {
             before(grammarAccess.getBuiltInTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleBuiltInType();

            state._fsp--;

             after(grammarAccess.getBuiltInTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBuiltInType"


    // $ANTLR start "ruleBuiltInType"
    // InternalJniMap.g:855:1: ruleBuiltInType : ( ( rule__BuiltInType__Alternatives ) ) ;
    public final void ruleBuiltInType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:859:2: ( ( ( rule__BuiltInType__Alternatives ) ) )
            // InternalJniMap.g:860:1: ( ( rule__BuiltInType__Alternatives ) )
            {
            // InternalJniMap.g:860:1: ( ( rule__BuiltInType__Alternatives ) )
            // InternalJniMap.g:861:1: ( rule__BuiltInType__Alternatives )
            {
             before(grammarAccess.getBuiltInTypeAccess().getAlternatives()); 
            // InternalJniMap.g:862:1: ( rule__BuiltInType__Alternatives )
            // InternalJniMap.g:862:2: rule__BuiltInType__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__BuiltInType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBuiltInTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBuiltInType"


    // $ANTLR start "entryRuleBooleanType"
    // InternalJniMap.g:874:1: entryRuleBooleanType : ruleBooleanType EOF ;
    public final void entryRuleBooleanType() throws RecognitionException {
        try {
            // InternalJniMap.g:875:1: ( ruleBooleanType EOF )
            // InternalJniMap.g:876:1: ruleBooleanType EOF
            {
             before(grammarAccess.getBooleanTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleBooleanType();

            state._fsp--;

             after(grammarAccess.getBooleanTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanType"


    // $ANTLR start "ruleBooleanType"
    // InternalJniMap.g:883:1: ruleBooleanType : ( ( rule__BooleanType__NameAssignment ) ) ;
    public final void ruleBooleanType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:887:2: ( ( ( rule__BooleanType__NameAssignment ) ) )
            // InternalJniMap.g:888:1: ( ( rule__BooleanType__NameAssignment ) )
            {
            // InternalJniMap.g:888:1: ( ( rule__BooleanType__NameAssignment ) )
            // InternalJniMap.g:889:1: ( rule__BooleanType__NameAssignment )
            {
             before(grammarAccess.getBooleanTypeAccess().getNameAssignment()); 
            // InternalJniMap.g:890:1: ( rule__BooleanType__NameAssignment )
            // InternalJniMap.g:890:2: rule__BooleanType__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__BooleanType__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getBooleanTypeAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanType"


    // $ANTLR start "entryRuleIntegerType"
    // InternalJniMap.g:902:1: entryRuleIntegerType : ruleIntegerType EOF ;
    public final void entryRuleIntegerType() throws RecognitionException {
        try {
            // InternalJniMap.g:903:1: ( ruleIntegerType EOF )
            // InternalJniMap.g:904:1: ruleIntegerType EOF
            {
             before(grammarAccess.getIntegerTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleIntegerType();

            state._fsp--;

             after(grammarAccess.getIntegerTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerType"


    // $ANTLR start "ruleIntegerType"
    // InternalJniMap.g:911:1: ruleIntegerType : ( ( rule__IntegerType__NameAssignment ) ) ;
    public final void ruleIntegerType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:915:2: ( ( ( rule__IntegerType__NameAssignment ) ) )
            // InternalJniMap.g:916:1: ( ( rule__IntegerType__NameAssignment ) )
            {
            // InternalJniMap.g:916:1: ( ( rule__IntegerType__NameAssignment ) )
            // InternalJniMap.g:917:1: ( rule__IntegerType__NameAssignment )
            {
             before(grammarAccess.getIntegerTypeAccess().getNameAssignment()); 
            // InternalJniMap.g:918:1: ( rule__IntegerType__NameAssignment )
            // InternalJniMap.g:918:2: rule__IntegerType__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__IntegerType__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getIntegerTypeAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerType"


    // $ANTLR start "entryRuleRealType"
    // InternalJniMap.g:930:1: entryRuleRealType : ruleRealType EOF ;
    public final void entryRuleRealType() throws RecognitionException {
        try {
            // InternalJniMap.g:931:1: ( ruleRealType EOF )
            // InternalJniMap.g:932:1: ruleRealType EOF
            {
             before(grammarAccess.getRealTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleRealType();

            state._fsp--;

             after(grammarAccess.getRealTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRealType"


    // $ANTLR start "ruleRealType"
    // InternalJniMap.g:939:1: ruleRealType : ( ( rule__RealType__NameAssignment ) ) ;
    public final void ruleRealType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:943:2: ( ( ( rule__RealType__NameAssignment ) ) )
            // InternalJniMap.g:944:1: ( ( rule__RealType__NameAssignment ) )
            {
            // InternalJniMap.g:944:1: ( ( rule__RealType__NameAssignment ) )
            // InternalJniMap.g:945:1: ( rule__RealType__NameAssignment )
            {
             before(grammarAccess.getRealTypeAccess().getNameAssignment()); 
            // InternalJniMap.g:946:1: ( rule__RealType__NameAssignment )
            // InternalJniMap.g:946:2: rule__RealType__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__RealType__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getRealTypeAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRealType"


    // $ANTLR start "entryRuleStringType"
    // InternalJniMap.g:958:1: entryRuleStringType : ruleStringType EOF ;
    public final void entryRuleStringType() throws RecognitionException {
        try {
            // InternalJniMap.g:959:1: ( ruleStringType EOF )
            // InternalJniMap.g:960:1: ruleStringType EOF
            {
             before(grammarAccess.getStringTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleStringType();

            state._fsp--;

             after(grammarAccess.getStringTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringType"


    // $ANTLR start "ruleStringType"
    // InternalJniMap.g:967:1: ruleStringType : ( ( rule__StringType__NameAssignment ) ) ;
    public final void ruleStringType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:971:2: ( ( ( rule__StringType__NameAssignment ) ) )
            // InternalJniMap.g:972:1: ( ( rule__StringType__NameAssignment ) )
            {
            // InternalJniMap.g:972:1: ( ( rule__StringType__NameAssignment ) )
            // InternalJniMap.g:973:1: ( rule__StringType__NameAssignment )
            {
             before(grammarAccess.getStringTypeAccess().getNameAssignment()); 
            // InternalJniMap.g:974:1: ( rule__StringType__NameAssignment )
            // InternalJniMap.g:974:2: rule__StringType__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__StringType__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getStringTypeAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringType"


    // $ANTLR start "entryRuleVoidType"
    // InternalJniMap.g:986:1: entryRuleVoidType : ruleVoidType EOF ;
    public final void entryRuleVoidType() throws RecognitionException {
        try {
            // InternalJniMap.g:987:1: ( ruleVoidType EOF )
            // InternalJniMap.g:988:1: ruleVoidType EOF
            {
             before(grammarAccess.getVoidTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleVoidType();

            state._fsp--;

             after(grammarAccess.getVoidTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVoidType"


    // $ANTLR start "ruleVoidType"
    // InternalJniMap.g:995:1: ruleVoidType : ( ( rule__VoidType__NameAssignment ) ) ;
    public final void ruleVoidType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:999:2: ( ( ( rule__VoidType__NameAssignment ) ) )
            // InternalJniMap.g:1000:1: ( ( rule__VoidType__NameAssignment ) )
            {
            // InternalJniMap.g:1000:1: ( ( rule__VoidType__NameAssignment ) )
            // InternalJniMap.g:1001:1: ( rule__VoidType__NameAssignment )
            {
             before(grammarAccess.getVoidTypeAccess().getNameAssignment()); 
            // InternalJniMap.g:1002:1: ( rule__VoidType__NameAssignment )
            // InternalJniMap.g:1002:2: rule__VoidType__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__VoidType__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getVoidTypeAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVoidType"


    // $ANTLR start "entryRuleTypeAlias"
    // InternalJniMap.g:1018:1: entryRuleTypeAlias : ruleTypeAlias EOF ;
    public final void entryRuleTypeAlias() throws RecognitionException {
        try {
            // InternalJniMap.g:1019:1: ( ruleTypeAlias EOF )
            // InternalJniMap.g:1020:1: ruleTypeAlias EOF
            {
             before(grammarAccess.getTypeAliasRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeAlias();

            state._fsp--;

             after(grammarAccess.getTypeAliasRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeAlias"


    // $ANTLR start "ruleTypeAlias"
    // InternalJniMap.g:1027:1: ruleTypeAlias : ( ( rule__TypeAlias__AliasAssignment ) ) ;
    public final void ruleTypeAlias() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1031:2: ( ( ( rule__TypeAlias__AliasAssignment ) ) )
            // InternalJniMap.g:1032:1: ( ( rule__TypeAlias__AliasAssignment ) )
            {
            // InternalJniMap.g:1032:1: ( ( rule__TypeAlias__AliasAssignment ) )
            // InternalJniMap.g:1033:1: ( rule__TypeAlias__AliasAssignment )
            {
             before(grammarAccess.getTypeAliasAccess().getAliasAssignment()); 
            // InternalJniMap.g:1034:1: ( rule__TypeAlias__AliasAssignment )
            // InternalJniMap.g:1034:2: rule__TypeAlias__AliasAssignment
            {
            pushFollow(FOLLOW_2);
            rule__TypeAlias__AliasAssignment();

            state._fsp--;


            }

             after(grammarAccess.getTypeAliasAccess().getAliasAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeAlias"


    // $ANTLR start "entryRuleArrayType"
    // InternalJniMap.g:1046:1: entryRuleArrayType : ruleArrayType EOF ;
    public final void entryRuleArrayType() throws RecognitionException {
        try {
            // InternalJniMap.g:1047:1: ( ruleArrayType EOF )
            // InternalJniMap.g:1048:1: ruleArrayType EOF
            {
             before(grammarAccess.getArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleArrayType();

            state._fsp--;

             after(grammarAccess.getArrayTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleArrayType"


    // $ANTLR start "ruleArrayType"
    // InternalJniMap.g:1055:1: ruleArrayType : ( ( rule__ArrayType__Group__0 ) ) ;
    public final void ruleArrayType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1059:2: ( ( ( rule__ArrayType__Group__0 ) ) )
            // InternalJniMap.g:1060:1: ( ( rule__ArrayType__Group__0 ) )
            {
            // InternalJniMap.g:1060:1: ( ( rule__ArrayType__Group__0 ) )
            // InternalJniMap.g:1061:1: ( rule__ArrayType__Group__0 )
            {
             before(grammarAccess.getArrayTypeAccess().getGroup()); 
            // InternalJniMap.g:1062:1: ( rule__ArrayType__Group__0 )
            // InternalJniMap.g:1062:2: rule__ArrayType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ArrayType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getArrayTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleArrayType"


    // $ANTLR start "entryRulePtrType"
    // InternalJniMap.g:1074:1: entryRulePtrType : rulePtrType EOF ;
    public final void entryRulePtrType() throws RecognitionException {
        try {
            // InternalJniMap.g:1075:1: ( rulePtrType EOF )
            // InternalJniMap.g:1076:1: rulePtrType EOF
            {
             before(grammarAccess.getPtrTypeRule()); 
            pushFollow(FOLLOW_1);
            rulePtrType();

            state._fsp--;

             after(grammarAccess.getPtrTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePtrType"


    // $ANTLR start "rulePtrType"
    // InternalJniMap.g:1083:1: rulePtrType : ( ( rule__PtrType__Group__0 ) ) ;
    public final void rulePtrType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1087:2: ( ( ( rule__PtrType__Group__0 ) ) )
            // InternalJniMap.g:1088:1: ( ( rule__PtrType__Group__0 ) )
            {
            // InternalJniMap.g:1088:1: ( ( rule__PtrType__Group__0 ) )
            // InternalJniMap.g:1089:1: ( rule__PtrType__Group__0 )
            {
             before(grammarAccess.getPtrTypeAccess().getGroup()); 
            // InternalJniMap.g:1090:1: ( rule__PtrType__Group__0 )
            // InternalJniMap.g:1090:2: rule__PtrType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PtrType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPtrTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePtrType"


    // $ANTLR start "entryRuleModifiableType"
    // InternalJniMap.g:1102:1: entryRuleModifiableType : ruleModifiableType EOF ;
    public final void entryRuleModifiableType() throws RecognitionException {
        try {
            // InternalJniMap.g:1103:1: ( ruleModifiableType EOF )
            // InternalJniMap.g:1104:1: ruleModifiableType EOF
            {
             before(grammarAccess.getModifiableTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleModifiableType();

            state._fsp--;

             after(grammarAccess.getModifiableTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModifiableType"


    // $ANTLR start "ruleModifiableType"
    // InternalJniMap.g:1111:1: ruleModifiableType : ( ( rule__ModifiableType__Group__0 ) ) ;
    public final void ruleModifiableType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1115:2: ( ( ( rule__ModifiableType__Group__0 ) ) )
            // InternalJniMap.g:1116:1: ( ( rule__ModifiableType__Group__0 ) )
            {
            // InternalJniMap.g:1116:1: ( ( rule__ModifiableType__Group__0 ) )
            // InternalJniMap.g:1117:1: ( rule__ModifiableType__Group__0 )
            {
             before(grammarAccess.getModifiableTypeAccess().getGroup()); 
            // InternalJniMap.g:1118:1: ( rule__ModifiableType__Group__0 )
            // InternalJniMap.g:1118:2: rule__ModifiableType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ModifiableType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModifiableTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModifiableType"


    // $ANTLR start "entryRuleStructType"
    // InternalJniMap.g:1130:1: entryRuleStructType : ruleStructType EOF ;
    public final void entryRuleStructType() throws RecognitionException {
        try {
            // InternalJniMap.g:1131:1: ( ruleStructType EOF )
            // InternalJniMap.g:1132:1: ruleStructType EOF
            {
             before(grammarAccess.getStructTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleStructType();

            state._fsp--;

             after(grammarAccess.getStructTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStructType"


    // $ANTLR start "ruleStructType"
    // InternalJniMap.g:1139:1: ruleStructType : ( ( rule__StructType__Group__0 ) ) ;
    public final void ruleStructType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1143:2: ( ( ( rule__StructType__Group__0 ) ) )
            // InternalJniMap.g:1144:1: ( ( rule__StructType__Group__0 ) )
            {
            // InternalJniMap.g:1144:1: ( ( rule__StructType__Group__0 ) )
            // InternalJniMap.g:1145:1: ( rule__StructType__Group__0 )
            {
             before(grammarAccess.getStructTypeAccess().getGroup()); 
            // InternalJniMap.g:1146:1: ( rule__StructType__Group__0 )
            // InternalJniMap.g:1146:2: rule__StructType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StructType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStructTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStructType"


    // $ANTLR start "entryRuleFieldDef"
    // InternalJniMap.g:1158:1: entryRuleFieldDef : ruleFieldDef EOF ;
    public final void entryRuleFieldDef() throws RecognitionException {
        try {
            // InternalJniMap.g:1159:1: ( ruleFieldDef EOF )
            // InternalJniMap.g:1160:1: ruleFieldDef EOF
            {
             before(grammarAccess.getFieldDefRule()); 
            pushFollow(FOLLOW_1);
            ruleFieldDef();

            state._fsp--;

             after(grammarAccess.getFieldDefRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFieldDef"


    // $ANTLR start "ruleFieldDef"
    // InternalJniMap.g:1167:1: ruleFieldDef : ( ( rule__FieldDef__Group__0 ) ) ;
    public final void ruleFieldDef() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1171:2: ( ( ( rule__FieldDef__Group__0 ) ) )
            // InternalJniMap.g:1172:1: ( ( rule__FieldDef__Group__0 ) )
            {
            // InternalJniMap.g:1172:1: ( ( rule__FieldDef__Group__0 ) )
            // InternalJniMap.g:1173:1: ( rule__FieldDef__Group__0 )
            {
             before(grammarAccess.getFieldDefAccess().getGroup()); 
            // InternalJniMap.g:1174:1: ( rule__FieldDef__Group__0 )
            // InternalJniMap.g:1174:2: rule__FieldDef__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FieldDef__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFieldDefAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFieldDef"


    // $ANTLR start "entryRuleConstType"
    // InternalJniMap.g:1186:1: entryRuleConstType : ruleConstType EOF ;
    public final void entryRuleConstType() throws RecognitionException {
        try {
            // InternalJniMap.g:1187:1: ( ruleConstType EOF )
            // InternalJniMap.g:1188:1: ruleConstType EOF
            {
             before(grammarAccess.getConstTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleConstType();

            state._fsp--;

             after(grammarAccess.getConstTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstType"


    // $ANTLR start "ruleConstType"
    // InternalJniMap.g:1195:1: ruleConstType : ( ( rule__ConstType__Group__0 ) ) ;
    public final void ruleConstType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1199:2: ( ( ( rule__ConstType__Group__0 ) ) )
            // InternalJniMap.g:1200:1: ( ( rule__ConstType__Group__0 ) )
            {
            // InternalJniMap.g:1200:1: ( ( rule__ConstType__Group__0 ) )
            // InternalJniMap.g:1201:1: ( rule__ConstType__Group__0 )
            {
             before(grammarAccess.getConstTypeAccess().getGroup()); 
            // InternalJniMap.g:1202:1: ( rule__ConstType__Group__0 )
            // InternalJniMap.g:1202:2: rule__ConstType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ConstType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConstTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstType"


    // $ANTLR start "entryRuleBaseType"
    // InternalJniMap.g:1214:1: entryRuleBaseType : ruleBaseType EOF ;
    public final void entryRuleBaseType() throws RecognitionException {
        try {
            // InternalJniMap.g:1215:1: ( ruleBaseType EOF )
            // InternalJniMap.g:1216:1: ruleBaseType EOF
            {
             before(grammarAccess.getBaseTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleBaseType();

            state._fsp--;

             after(grammarAccess.getBaseTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBaseType"


    // $ANTLR start "ruleBaseType"
    // InternalJniMap.g:1223:1: ruleBaseType : ( ( rule__BaseType__Alternatives ) ) ;
    public final void ruleBaseType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1227:2: ( ( ( rule__BaseType__Alternatives ) ) )
            // InternalJniMap.g:1228:1: ( ( rule__BaseType__Alternatives ) )
            {
            // InternalJniMap.g:1228:1: ( ( rule__BaseType__Alternatives ) )
            // InternalJniMap.g:1229:1: ( rule__BaseType__Alternatives )
            {
             before(grammarAccess.getBaseTypeAccess().getAlternatives()); 
            // InternalJniMap.g:1230:1: ( rule__BaseType__Alternatives )
            // InternalJniMap.g:1230:2: rule__BaseType__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__BaseType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBaseTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBaseType"


    // $ANTLR start "ruleHostType"
    // InternalJniMap.g:1243:1: ruleHostType : ( ( rule__HostType__Alternatives ) ) ;
    public final void ruleHostType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1247:1: ( ( ( rule__HostType__Alternatives ) ) )
            // InternalJniMap.g:1248:1: ( ( rule__HostType__Alternatives ) )
            {
            // InternalJniMap.g:1248:1: ( ( rule__HostType__Alternatives ) )
            // InternalJniMap.g:1249:1: ( rule__HostType__Alternatives )
            {
             before(grammarAccess.getHostTypeAccess().getAlternatives()); 
            // InternalJniMap.g:1250:1: ( rule__HostType__Alternatives )
            // InternalJniMap.g:1250:2: rule__HostType__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__HostType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getHostTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHostType"


    // $ANTLR start "rule__Mapping__Alternatives_9"
    // InternalJniMap.g:1261:1: rule__Mapping__Alternatives_9 : ( ( ( rule__Mapping__ClassMappingAssignment_9_0 ) ) | ( ( rule__Mapping__IncludesAssignment_9_1 ) ) );
    public final void rule__Mapping__Alternatives_9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1265:1: ( ( ( rule__Mapping__ClassMappingAssignment_9_0 ) ) | ( ( rule__Mapping__IncludesAssignment_9_1 ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( ((LA1_0>=40 && LA1_0<=41)||(LA1_0>=50 && LA1_0<=53)) ) {
                alt1=1;
            }
            else if ( (LA1_0==48) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalJniMap.g:1266:1: ( ( rule__Mapping__ClassMappingAssignment_9_0 ) )
                    {
                    // InternalJniMap.g:1266:1: ( ( rule__Mapping__ClassMappingAssignment_9_0 ) )
                    // InternalJniMap.g:1267:1: ( rule__Mapping__ClassMappingAssignment_9_0 )
                    {
                     before(grammarAccess.getMappingAccess().getClassMappingAssignment_9_0()); 
                    // InternalJniMap.g:1268:1: ( rule__Mapping__ClassMappingAssignment_9_0 )
                    // InternalJniMap.g:1268:2: rule__Mapping__ClassMappingAssignment_9_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__ClassMappingAssignment_9_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMappingAccess().getClassMappingAssignment_9_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1272:6: ( ( rule__Mapping__IncludesAssignment_9_1 ) )
                    {
                    // InternalJniMap.g:1272:6: ( ( rule__Mapping__IncludesAssignment_9_1 ) )
                    // InternalJniMap.g:1273:1: ( rule__Mapping__IncludesAssignment_9_1 )
                    {
                     before(grammarAccess.getMappingAccess().getIncludesAssignment_9_1()); 
                    // InternalJniMap.g:1274:1: ( rule__Mapping__IncludesAssignment_9_1 )
                    // InternalJniMap.g:1274:2: rule__Mapping__IncludesAssignment_9_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__IncludesAssignment_9_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getMappingAccess().getIncludesAssignment_9_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Alternatives_9"


    // $ANTLR start "rule__ClassMapping__Alternatives"
    // InternalJniMap.g:1283:1: rule__ClassMapping__Alternatives : ( ( ruleStructToClassMapping ) | ( ruleEnumToClassMapping ) | ( ruleAliasToClassMapping ) | ( ruleClassToClassMapping ) | ( ruleUnmappedClass ) | ( ruleInterfaceClass ) );
    public final void rule__ClassMapping__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1287:1: ( ( ruleStructToClassMapping ) | ( ruleEnumToClassMapping ) | ( ruleAliasToClassMapping ) | ( ruleClassToClassMapping ) | ( ruleUnmappedClass ) | ( ruleInterfaceClass ) )
            int alt2=6;
            switch ( input.LA(1) ) {
            case 51:
                {
                alt2=1;
                }
                break;
            case 53:
                {
                alt2=2;
                }
                break;
            case 50:
                {
                alt2=3;
                }
                break;
            case 52:
                {
                alt2=4;
                }
                break;
            case 40:
                {
                alt2=5;
                }
                break;
            case 41:
                {
                alt2=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalJniMap.g:1288:1: ( ruleStructToClassMapping )
                    {
                    // InternalJniMap.g:1288:1: ( ruleStructToClassMapping )
                    // InternalJniMap.g:1289:1: ruleStructToClassMapping
                    {
                     before(grammarAccess.getClassMappingAccess().getStructToClassMappingParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleStructToClassMapping();

                    state._fsp--;

                     after(grammarAccess.getClassMappingAccess().getStructToClassMappingParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1294:6: ( ruleEnumToClassMapping )
                    {
                    // InternalJniMap.g:1294:6: ( ruleEnumToClassMapping )
                    // InternalJniMap.g:1295:1: ruleEnumToClassMapping
                    {
                     before(grammarAccess.getClassMappingAccess().getEnumToClassMappingParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleEnumToClassMapping();

                    state._fsp--;

                     after(grammarAccess.getClassMappingAccess().getEnumToClassMappingParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalJniMap.g:1300:6: ( ruleAliasToClassMapping )
                    {
                    // InternalJniMap.g:1300:6: ( ruleAliasToClassMapping )
                    // InternalJniMap.g:1301:1: ruleAliasToClassMapping
                    {
                     before(grammarAccess.getClassMappingAccess().getAliasToClassMappingParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleAliasToClassMapping();

                    state._fsp--;

                     after(grammarAccess.getClassMappingAccess().getAliasToClassMappingParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalJniMap.g:1306:6: ( ruleClassToClassMapping )
                    {
                    // InternalJniMap.g:1306:6: ( ruleClassToClassMapping )
                    // InternalJniMap.g:1307:1: ruleClassToClassMapping
                    {
                     before(grammarAccess.getClassMappingAccess().getClassToClassMappingParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleClassToClassMapping();

                    state._fsp--;

                     after(grammarAccess.getClassMappingAccess().getClassToClassMappingParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalJniMap.g:1312:6: ( ruleUnmappedClass )
                    {
                    // InternalJniMap.g:1312:6: ( ruleUnmappedClass )
                    // InternalJniMap.g:1313:1: ruleUnmappedClass
                    {
                     before(grammarAccess.getClassMappingAccess().getUnmappedClassParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleUnmappedClass();

                    state._fsp--;

                     after(grammarAccess.getClassMappingAccess().getUnmappedClassParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalJniMap.g:1318:6: ( ruleInterfaceClass )
                    {
                    // InternalJniMap.g:1318:6: ( ruleInterfaceClass )
                    // InternalJniMap.g:1319:1: ruleInterfaceClass
                    {
                     before(grammarAccess.getClassMappingAccess().getInterfaceClassParserRuleCall_5()); 
                    pushFollow(FOLLOW_2);
                    ruleInterfaceClass();

                    state._fsp--;

                     after(grammarAccess.getClassMappingAccess().getInterfaceClassParserRuleCall_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassMapping__Alternatives"


    // $ANTLR start "rule__CMethod__Alternatives_0"
    // InternalJniMap.g:1329:1: rule__CMethod__Alternatives_0 : ( ( 'extern' ) | ( 'static' ) );
    public final void rule__CMethod__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1333:1: ( ( 'extern' ) | ( 'static' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==11) ) {
                alt3=1;
            }
            else if ( (LA3_0==12) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalJniMap.g:1334:1: ( 'extern' )
                    {
                    // InternalJniMap.g:1334:1: ( 'extern' )
                    // InternalJniMap.g:1335:1: 'extern'
                    {
                     before(grammarAccess.getCMethodAccess().getExternKeyword_0_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getCMethodAccess().getExternKeyword_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1342:6: ( 'static' )
                    {
                    // InternalJniMap.g:1342:6: ( 'static' )
                    // InternalJniMap.g:1343:1: 'static'
                    {
                     before(grammarAccess.getCMethodAccess().getStaticKeyword_0_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getCMethodAccess().getStaticKeyword_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Alternatives_0"


    // $ANTLR start "rule__EnumValue__Alternatives_1_1"
    // InternalJniMap.g:1356:1: rule__EnumValue__Alternatives_1_1 : ( ( ( rule__EnumValue__ValueAssignment_1_1_0 ) ) | ( ( rule__EnumValue__EnumNextAssignment_1_1_1 ) ) );
    public final void rule__EnumValue__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1360:1: ( ( ( rule__EnumValue__ValueAssignment_1_1_0 ) ) | ( ( rule__EnumValue__EnumNextAssignment_1_1_1 ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_INT) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_ID) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalJniMap.g:1361:1: ( ( rule__EnumValue__ValueAssignment_1_1_0 ) )
                    {
                    // InternalJniMap.g:1361:1: ( ( rule__EnumValue__ValueAssignment_1_1_0 ) )
                    // InternalJniMap.g:1362:1: ( rule__EnumValue__ValueAssignment_1_1_0 )
                    {
                     before(grammarAccess.getEnumValueAccess().getValueAssignment_1_1_0()); 
                    // InternalJniMap.g:1363:1: ( rule__EnumValue__ValueAssignment_1_1_0 )
                    // InternalJniMap.g:1363:2: rule__EnumValue__ValueAssignment_1_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__EnumValue__ValueAssignment_1_1_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getEnumValueAccess().getValueAssignment_1_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1367:6: ( ( rule__EnumValue__EnumNextAssignment_1_1_1 ) )
                    {
                    // InternalJniMap.g:1367:6: ( ( rule__EnumValue__EnumNextAssignment_1_1_1 ) )
                    // InternalJniMap.g:1368:1: ( rule__EnumValue__EnumNextAssignment_1_1_1 )
                    {
                     before(grammarAccess.getEnumValueAccess().getEnumNextAssignment_1_1_1()); 
                    // InternalJniMap.g:1369:1: ( rule__EnumValue__EnumNextAssignment_1_1_1 )
                    // InternalJniMap.g:1369:2: rule__EnumValue__EnumNextAssignment_1_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__EnumValue__EnumNextAssignment_1_1_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getEnumValueAccess().getEnumNextAssignment_1_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Alternatives_1_1"


    // $ANTLR start "rule__ParamDef__Alternatives_0"
    // InternalJniMap.g:1378:1: rule__ParamDef__Alternatives_0 : ( ( ( rule__ParamDef__GiveAssignment_0_0 ) ) | ( ( rule__ParamDef__TakeAssignment_0_1 ) ) | ( ( rule__ParamDef__KeepAssignment_0_2 ) ) );
    public final void rule__ParamDef__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1382:1: ( ( ( rule__ParamDef__GiveAssignment_0_0 ) ) | ( ( rule__ParamDef__TakeAssignment_0_1 ) ) | ( ( rule__ParamDef__KeepAssignment_0_2 ) ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case 58:
                {
                alt5=1;
                }
                break;
            case 59:
                {
                alt5=2;
                }
                break;
            case 60:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalJniMap.g:1383:1: ( ( rule__ParamDef__GiveAssignment_0_0 ) )
                    {
                    // InternalJniMap.g:1383:1: ( ( rule__ParamDef__GiveAssignment_0_0 ) )
                    // InternalJniMap.g:1384:1: ( rule__ParamDef__GiveAssignment_0_0 )
                    {
                     before(grammarAccess.getParamDefAccess().getGiveAssignment_0_0()); 
                    // InternalJniMap.g:1385:1: ( rule__ParamDef__GiveAssignment_0_0 )
                    // InternalJniMap.g:1385:2: rule__ParamDef__GiveAssignment_0_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ParamDef__GiveAssignment_0_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getParamDefAccess().getGiveAssignment_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1389:6: ( ( rule__ParamDef__TakeAssignment_0_1 ) )
                    {
                    // InternalJniMap.g:1389:6: ( ( rule__ParamDef__TakeAssignment_0_1 ) )
                    // InternalJniMap.g:1390:1: ( rule__ParamDef__TakeAssignment_0_1 )
                    {
                     before(grammarAccess.getParamDefAccess().getTakeAssignment_0_1()); 
                    // InternalJniMap.g:1391:1: ( rule__ParamDef__TakeAssignment_0_1 )
                    // InternalJniMap.g:1391:2: rule__ParamDef__TakeAssignment_0_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__ParamDef__TakeAssignment_0_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getParamDefAccess().getTakeAssignment_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalJniMap.g:1395:6: ( ( rule__ParamDef__KeepAssignment_0_2 ) )
                    {
                    // InternalJniMap.g:1395:6: ( ( rule__ParamDef__KeepAssignment_0_2 ) )
                    // InternalJniMap.g:1396:1: ( rule__ParamDef__KeepAssignment_0_2 )
                    {
                     before(grammarAccess.getParamDefAccess().getKeepAssignment_0_2()); 
                    // InternalJniMap.g:1397:1: ( rule__ParamDef__KeepAssignment_0_2 )
                    // InternalJniMap.g:1397:2: rule__ParamDef__KeepAssignment_0_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__ParamDef__KeepAssignment_0_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getParamDefAccess().getKeepAssignment_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamDef__Alternatives_0"


    // $ANTLR start "rule__Method__Alternatives_0_1_0"
    // InternalJniMap.g:1406:1: rule__Method__Alternatives_0_1_0 : ( ( ( rule__Method__ConstructorAssignment_0_1_0_0 ) ) | ( ( rule__Method__DestructorAssignment_0_1_0_1 ) ) | ( ( rule__Method__InstanceofAssignment_0_1_0_2 ) ) );
    public final void rule__Method__Alternatives_0_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1410:1: ( ( ( rule__Method__ConstructorAssignment_0_1_0_0 ) ) | ( ( rule__Method__DestructorAssignment_0_1_0_1 ) ) | ( ( rule__Method__InstanceofAssignment_0_1_0_2 ) ) )
            int alt6=3;
            switch ( input.LA(1) ) {
            case 61:
                {
                alt6=1;
                }
                break;
            case 62:
                {
                alt6=2;
                }
                break;
            case 63:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalJniMap.g:1411:1: ( ( rule__Method__ConstructorAssignment_0_1_0_0 ) )
                    {
                    // InternalJniMap.g:1411:1: ( ( rule__Method__ConstructorAssignment_0_1_0_0 ) )
                    // InternalJniMap.g:1412:1: ( rule__Method__ConstructorAssignment_0_1_0_0 )
                    {
                     before(grammarAccess.getMethodAccess().getConstructorAssignment_0_1_0_0()); 
                    // InternalJniMap.g:1413:1: ( rule__Method__ConstructorAssignment_0_1_0_0 )
                    // InternalJniMap.g:1413:2: rule__Method__ConstructorAssignment_0_1_0_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__ConstructorAssignment_0_1_0_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMethodAccess().getConstructorAssignment_0_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1417:6: ( ( rule__Method__DestructorAssignment_0_1_0_1 ) )
                    {
                    // InternalJniMap.g:1417:6: ( ( rule__Method__DestructorAssignment_0_1_0_1 ) )
                    // InternalJniMap.g:1418:1: ( rule__Method__DestructorAssignment_0_1_0_1 )
                    {
                     before(grammarAccess.getMethodAccess().getDestructorAssignment_0_1_0_1()); 
                    // InternalJniMap.g:1419:1: ( rule__Method__DestructorAssignment_0_1_0_1 )
                    // InternalJniMap.g:1419:2: rule__Method__DestructorAssignment_0_1_0_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__DestructorAssignment_0_1_0_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getMethodAccess().getDestructorAssignment_0_1_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalJniMap.g:1423:6: ( ( rule__Method__InstanceofAssignment_0_1_0_2 ) )
                    {
                    // InternalJniMap.g:1423:6: ( ( rule__Method__InstanceofAssignment_0_1_0_2 ) )
                    // InternalJniMap.g:1424:1: ( rule__Method__InstanceofAssignment_0_1_0_2 )
                    {
                     before(grammarAccess.getMethodAccess().getInstanceofAssignment_0_1_0_2()); 
                    // InternalJniMap.g:1425:1: ( rule__Method__InstanceofAssignment_0_1_0_2 )
                    // InternalJniMap.g:1425:2: rule__Method__InstanceofAssignment_0_1_0_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__InstanceofAssignment_0_1_0_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getMethodAccess().getInstanceofAssignment_0_1_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Alternatives_0_1_0"


    // $ANTLR start "rule__Method__Alternatives_0_1_2"
    // InternalJniMap.g:1434:1: rule__Method__Alternatives_0_1_2 : ( ( ( rule__Method__PrivateAssignment_0_1_2_0 ) ) | ( ( rule__Method__ProtectedAssignment_0_1_2_1 ) ) );
    public final void rule__Method__Alternatives_0_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1438:1: ( ( ( rule__Method__PrivateAssignment_0_1_2_0 ) ) | ( ( rule__Method__ProtectedAssignment_0_1_2_1 ) ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==64) ) {
                alt7=1;
            }
            else if ( (LA7_0==65) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalJniMap.g:1439:1: ( ( rule__Method__PrivateAssignment_0_1_2_0 ) )
                    {
                    // InternalJniMap.g:1439:1: ( ( rule__Method__PrivateAssignment_0_1_2_0 ) )
                    // InternalJniMap.g:1440:1: ( rule__Method__PrivateAssignment_0_1_2_0 )
                    {
                     before(grammarAccess.getMethodAccess().getPrivateAssignment_0_1_2_0()); 
                    // InternalJniMap.g:1441:1: ( rule__Method__PrivateAssignment_0_1_2_0 )
                    // InternalJniMap.g:1441:2: rule__Method__PrivateAssignment_0_1_2_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__PrivateAssignment_0_1_2_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMethodAccess().getPrivateAssignment_0_1_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1445:6: ( ( rule__Method__ProtectedAssignment_0_1_2_1 ) )
                    {
                    // InternalJniMap.g:1445:6: ( ( rule__Method__ProtectedAssignment_0_1_2_1 ) )
                    // InternalJniMap.g:1446:1: ( rule__Method__ProtectedAssignment_0_1_2_1 )
                    {
                     before(grammarAccess.getMethodAccess().getProtectedAssignment_0_1_2_1()); 
                    // InternalJniMap.g:1447:1: ( rule__Method__ProtectedAssignment_0_1_2_1 )
                    // InternalJniMap.g:1447:2: rule__Method__ProtectedAssignment_0_1_2_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__ProtectedAssignment_0_1_2_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getMethodAccess().getProtectedAssignment_0_1_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Alternatives_0_1_2"


    // $ANTLR start "rule__Method__Alternatives_1"
    // InternalJniMap.g:1456:1: rule__Method__Alternatives_1 : ( ( 'extern' ) | ( 'static' ) );
    public final void rule__Method__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1460:1: ( ( 'extern' ) | ( 'static' ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==11) ) {
                alt8=1;
            }
            else if ( (LA8_0==12) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalJniMap.g:1461:1: ( 'extern' )
                    {
                    // InternalJniMap.g:1461:1: ( 'extern' )
                    // InternalJniMap.g:1462:1: 'extern'
                    {
                     before(grammarAccess.getMethodAccess().getExternKeyword_1_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getMethodAccess().getExternKeyword_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1469:6: ( 'static' )
                    {
                    // InternalJniMap.g:1469:6: ( 'static' )
                    // InternalJniMap.g:1470:1: 'static'
                    {
                     before(grammarAccess.getMethodAccess().getStaticKeyword_1_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getMethodAccess().getStaticKeyword_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Alternatives_1"


    // $ANTLR start "rule__Type__Alternatives"
    // InternalJniMap.g:1482:1: rule__Type__Alternatives : ( ( rulePtrType ) | ( ruleModifiableType ) | ( ruleBaseType ) | ( ruleConstType ) | ( ruleArrayType ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1486:1: ( ( rulePtrType ) | ( ruleModifiableType ) | ( ruleBaseType ) | ( ruleConstType ) | ( ruleArrayType ) )
            int alt9=5;
            alt9 = dfa9.predict(input);
            switch (alt9) {
                case 1 :
                    // InternalJniMap.g:1487:1: ( rulePtrType )
                    {
                    // InternalJniMap.g:1487:1: ( rulePtrType )
                    // InternalJniMap.g:1488:1: rulePtrType
                    {
                     before(grammarAccess.getTypeAccess().getPtrTypeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    rulePtrType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getPtrTypeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1493:6: ( ruleModifiableType )
                    {
                    // InternalJniMap.g:1493:6: ( ruleModifiableType )
                    // InternalJniMap.g:1494:1: ruleModifiableType
                    {
                     before(grammarAccess.getTypeAccess().getModifiableTypeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleModifiableType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getModifiableTypeParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalJniMap.g:1499:6: ( ruleBaseType )
                    {
                    // InternalJniMap.g:1499:6: ( ruleBaseType )
                    // InternalJniMap.g:1500:1: ruleBaseType
                    {
                     before(grammarAccess.getTypeAccess().getBaseTypeParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleBaseType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getBaseTypeParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalJniMap.g:1505:6: ( ruleConstType )
                    {
                    // InternalJniMap.g:1505:6: ( ruleConstType )
                    // InternalJniMap.g:1506:1: ruleConstType
                    {
                     before(grammarAccess.getTypeAccess().getConstTypeParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleConstType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getConstTypeParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalJniMap.g:1511:6: ( ruleArrayType )
                    {
                    // InternalJniMap.g:1511:6: ( ruleArrayType )
                    // InternalJniMap.g:1512:1: ruleArrayType
                    {
                     before(grammarAccess.getTypeAccess().getArrayTypeParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleArrayType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getArrayTypeParserRuleCall_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__BuiltInType__Alternatives"
    // InternalJniMap.g:1522:1: rule__BuiltInType__Alternatives : ( ( ruleBooleanType ) | ( ruleIntegerType ) | ( ruleRealType ) | ( ruleStringType ) | ( ruleVoidType ) );
    public final void rule__BuiltInType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1526:1: ( ( ruleBooleanType ) | ( ruleIntegerType ) | ( ruleRealType ) | ( ruleStringType ) | ( ruleVoidType ) )
            int alt10=5;
            switch ( input.LA(1) ) {
            case 68:
                {
                alt10=1;
                }
                break;
            case 13:
            case 14:
            case 15:
            case 16:
                {
                alt10=2;
                }
                break;
            case 17:
            case 18:
                {
                alt10=3;
                }
                break;
            case 69:
                {
                alt10=4;
                }
                break;
            case 70:
                {
                alt10=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalJniMap.g:1527:1: ( ruleBooleanType )
                    {
                    // InternalJniMap.g:1527:1: ( ruleBooleanType )
                    // InternalJniMap.g:1528:1: ruleBooleanType
                    {
                     before(grammarAccess.getBuiltInTypeAccess().getBooleanTypeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleBooleanType();

                    state._fsp--;

                     after(grammarAccess.getBuiltInTypeAccess().getBooleanTypeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1533:6: ( ruleIntegerType )
                    {
                    // InternalJniMap.g:1533:6: ( ruleIntegerType )
                    // InternalJniMap.g:1534:1: ruleIntegerType
                    {
                     before(grammarAccess.getBuiltInTypeAccess().getIntegerTypeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleIntegerType();

                    state._fsp--;

                     after(grammarAccess.getBuiltInTypeAccess().getIntegerTypeParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalJniMap.g:1539:6: ( ruleRealType )
                    {
                    // InternalJniMap.g:1539:6: ( ruleRealType )
                    // InternalJniMap.g:1540:1: ruleRealType
                    {
                     before(grammarAccess.getBuiltInTypeAccess().getRealTypeParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleRealType();

                    state._fsp--;

                     after(grammarAccess.getBuiltInTypeAccess().getRealTypeParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalJniMap.g:1545:6: ( ruleStringType )
                    {
                    // InternalJniMap.g:1545:6: ( ruleStringType )
                    // InternalJniMap.g:1546:1: ruleStringType
                    {
                     before(grammarAccess.getBuiltInTypeAccess().getStringTypeParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleStringType();

                    state._fsp--;

                     after(grammarAccess.getBuiltInTypeAccess().getStringTypeParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalJniMap.g:1551:6: ( ruleVoidType )
                    {
                    // InternalJniMap.g:1551:6: ( ruleVoidType )
                    // InternalJniMap.g:1552:1: ruleVoidType
                    {
                     before(grammarAccess.getBuiltInTypeAccess().getVoidTypeParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleVoidType();

                    state._fsp--;

                     after(grammarAccess.getBuiltInTypeAccess().getVoidTypeParserRuleCall_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BuiltInType__Alternatives"


    // $ANTLR start "rule__IntegerType__NameAlternatives_0"
    // InternalJniMap.g:1562:1: rule__IntegerType__NameAlternatives_0 : ( ( 'unsigned' ) | ( 'int' ) | ( 'long' ) | ( 'char' ) );
    public final void rule__IntegerType__NameAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1566:1: ( ( 'unsigned' ) | ( 'int' ) | ( 'long' ) | ( 'char' ) )
            int alt11=4;
            switch ( input.LA(1) ) {
            case 13:
                {
                alt11=1;
                }
                break;
            case 14:
                {
                alt11=2;
                }
                break;
            case 15:
                {
                alt11=3;
                }
                break;
            case 16:
                {
                alt11=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalJniMap.g:1567:1: ( 'unsigned' )
                    {
                    // InternalJniMap.g:1567:1: ( 'unsigned' )
                    // InternalJniMap.g:1568:1: 'unsigned'
                    {
                     before(grammarAccess.getIntegerTypeAccess().getNameUnsignedKeyword_0_0()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getIntegerTypeAccess().getNameUnsignedKeyword_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1575:6: ( 'int' )
                    {
                    // InternalJniMap.g:1575:6: ( 'int' )
                    // InternalJniMap.g:1576:1: 'int'
                    {
                     before(grammarAccess.getIntegerTypeAccess().getNameIntKeyword_0_1()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getIntegerTypeAccess().getNameIntKeyword_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalJniMap.g:1583:6: ( 'long' )
                    {
                    // InternalJniMap.g:1583:6: ( 'long' )
                    // InternalJniMap.g:1584:1: 'long'
                    {
                     before(grammarAccess.getIntegerTypeAccess().getNameLongKeyword_0_2()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getIntegerTypeAccess().getNameLongKeyword_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalJniMap.g:1591:6: ( 'char' )
                    {
                    // InternalJniMap.g:1591:6: ( 'char' )
                    // InternalJniMap.g:1592:1: 'char'
                    {
                     before(grammarAccess.getIntegerTypeAccess().getNameCharKeyword_0_3()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getIntegerTypeAccess().getNameCharKeyword_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerType__NameAlternatives_0"


    // $ANTLR start "rule__RealType__NameAlternatives_0"
    // InternalJniMap.g:1604:1: rule__RealType__NameAlternatives_0 : ( ( 'float' ) | ( 'double' ) );
    public final void rule__RealType__NameAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1608:1: ( ( 'float' ) | ( 'double' ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==17) ) {
                alt12=1;
            }
            else if ( (LA12_0==18) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalJniMap.g:1609:1: ( 'float' )
                    {
                    // InternalJniMap.g:1609:1: ( 'float' )
                    // InternalJniMap.g:1610:1: 'float'
                    {
                     before(grammarAccess.getRealTypeAccess().getNameFloatKeyword_0_0()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getRealTypeAccess().getNameFloatKeyword_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1617:6: ( 'double' )
                    {
                    // InternalJniMap.g:1617:6: ( 'double' )
                    // InternalJniMap.g:1618:1: 'double'
                    {
                     before(grammarAccess.getRealTypeAccess().getNameDoubleKeyword_0_1()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getRealTypeAccess().getNameDoubleKeyword_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RealType__NameAlternatives_0"


    // $ANTLR start "rule__BaseType__Alternatives"
    // InternalJniMap.g:1632:1: rule__BaseType__Alternatives : ( ( ruleBuiltInType ) | ( ruleStructType ) | ( ruleEnumType ) | ( ruleTypeAlias ) );
    public final void rule__BaseType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1636:1: ( ( ruleBuiltInType ) | ( ruleStructType ) | ( ruleEnumType ) | ( ruleTypeAlias ) )
            int alt13=4;
            switch ( input.LA(1) ) {
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 68:
            case 69:
            case 70:
                {
                alt13=1;
                }
                break;
            case 51:
                {
                alt13=2;
                }
                break;
            case 53:
                {
                alt13=3;
                }
                break;
            case RULE_ID:
                {
                alt13=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // InternalJniMap.g:1637:1: ( ruleBuiltInType )
                    {
                    // InternalJniMap.g:1637:1: ( ruleBuiltInType )
                    // InternalJniMap.g:1638:1: ruleBuiltInType
                    {
                     before(grammarAccess.getBaseTypeAccess().getBuiltInTypeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleBuiltInType();

                    state._fsp--;

                     after(grammarAccess.getBaseTypeAccess().getBuiltInTypeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1643:6: ( ruleStructType )
                    {
                    // InternalJniMap.g:1643:6: ( ruleStructType )
                    // InternalJniMap.g:1644:1: ruleStructType
                    {
                     before(grammarAccess.getBaseTypeAccess().getStructTypeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleStructType();

                    state._fsp--;

                     after(grammarAccess.getBaseTypeAccess().getStructTypeParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalJniMap.g:1649:6: ( ruleEnumType )
                    {
                    // InternalJniMap.g:1649:6: ( ruleEnumType )
                    // InternalJniMap.g:1650:1: ruleEnumType
                    {
                     before(grammarAccess.getBaseTypeAccess().getEnumTypeParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleEnumType();

                    state._fsp--;

                     after(grammarAccess.getBaseTypeAccess().getEnumTypeParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalJniMap.g:1655:6: ( ruleTypeAlias )
                    {
                    // InternalJniMap.g:1655:6: ( ruleTypeAlias )
                    // InternalJniMap.g:1656:1: ruleTypeAlias
                    {
                     before(grammarAccess.getBaseTypeAccess().getTypeAliasParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleTypeAlias();

                    state._fsp--;

                     after(grammarAccess.getBaseTypeAccess().getTypeAliasParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BaseType__Alternatives"


    // $ANTLR start "rule__HostType__Alternatives"
    // InternalJniMap.g:1666:1: rule__HostType__Alternatives : ( ( ( 'linux_32' ) ) | ( ( 'linux_64' ) ) | ( ( 'macosx_32' ) ) | ( ( 'macosx_64' ) ) | ( ( 'mingw_32' ) ) | ( ( 'cygwin_32' ) ) );
    public final void rule__HostType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1670:1: ( ( ( 'linux_32' ) ) | ( ( 'linux_64' ) ) | ( ( 'macosx_32' ) ) | ( ( 'macosx_64' ) ) | ( ( 'mingw_32' ) ) | ( ( 'cygwin_32' ) ) )
            int alt14=6;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt14=1;
                }
                break;
            case 20:
                {
                alt14=2;
                }
                break;
            case 21:
                {
                alt14=3;
                }
                break;
            case 22:
                {
                alt14=4;
                }
                break;
            case 23:
                {
                alt14=5;
                }
                break;
            case 24:
                {
                alt14=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalJniMap.g:1671:1: ( ( 'linux_32' ) )
                    {
                    // InternalJniMap.g:1671:1: ( ( 'linux_32' ) )
                    // InternalJniMap.g:1672:1: ( 'linux_32' )
                    {
                     before(grammarAccess.getHostTypeAccess().getLinux32EnumLiteralDeclaration_0()); 
                    // InternalJniMap.g:1673:1: ( 'linux_32' )
                    // InternalJniMap.g:1673:3: 'linux_32'
                    {
                    match(input,19,FOLLOW_2); 

                    }

                     after(grammarAccess.getHostTypeAccess().getLinux32EnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1678:6: ( ( 'linux_64' ) )
                    {
                    // InternalJniMap.g:1678:6: ( ( 'linux_64' ) )
                    // InternalJniMap.g:1679:1: ( 'linux_64' )
                    {
                     before(grammarAccess.getHostTypeAccess().getLinux64EnumLiteralDeclaration_1()); 
                    // InternalJniMap.g:1680:1: ( 'linux_64' )
                    // InternalJniMap.g:1680:3: 'linux_64'
                    {
                    match(input,20,FOLLOW_2); 

                    }

                     after(grammarAccess.getHostTypeAccess().getLinux64EnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalJniMap.g:1685:6: ( ( 'macosx_32' ) )
                    {
                    // InternalJniMap.g:1685:6: ( ( 'macosx_32' ) )
                    // InternalJniMap.g:1686:1: ( 'macosx_32' )
                    {
                     before(grammarAccess.getHostTypeAccess().getMacosx32EnumLiteralDeclaration_2()); 
                    // InternalJniMap.g:1687:1: ( 'macosx_32' )
                    // InternalJniMap.g:1687:3: 'macosx_32'
                    {
                    match(input,21,FOLLOW_2); 

                    }

                     after(grammarAccess.getHostTypeAccess().getMacosx32EnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalJniMap.g:1692:6: ( ( 'macosx_64' ) )
                    {
                    // InternalJniMap.g:1692:6: ( ( 'macosx_64' ) )
                    // InternalJniMap.g:1693:1: ( 'macosx_64' )
                    {
                     before(grammarAccess.getHostTypeAccess().getMacosx64EnumLiteralDeclaration_3()); 
                    // InternalJniMap.g:1694:1: ( 'macosx_64' )
                    // InternalJniMap.g:1694:3: 'macosx_64'
                    {
                    match(input,22,FOLLOW_2); 

                    }

                     after(grammarAccess.getHostTypeAccess().getMacosx64EnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalJniMap.g:1699:6: ( ( 'mingw_32' ) )
                    {
                    // InternalJniMap.g:1699:6: ( ( 'mingw_32' ) )
                    // InternalJniMap.g:1700:1: ( 'mingw_32' )
                    {
                     before(grammarAccess.getHostTypeAccess().getMingw32EnumLiteralDeclaration_4()); 
                    // InternalJniMap.g:1701:1: ( 'mingw_32' )
                    // InternalJniMap.g:1701:3: 'mingw_32'
                    {
                    match(input,23,FOLLOW_2); 

                    }

                     after(grammarAccess.getHostTypeAccess().getMingw32EnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalJniMap.g:1706:6: ( ( 'cygwin_32' ) )
                    {
                    // InternalJniMap.g:1706:6: ( ( 'cygwin_32' ) )
                    // InternalJniMap.g:1707:1: ( 'cygwin_32' )
                    {
                     before(grammarAccess.getHostTypeAccess().getCygwin32EnumLiteralDeclaration_5()); 
                    // InternalJniMap.g:1708:1: ( 'cygwin_32' )
                    // InternalJniMap.g:1708:3: 'cygwin_32'
                    {
                    match(input,24,FOLLOW_2); 

                    }

                     after(grammarAccess.getHostTypeAccess().getCygwin32EnumLiteralDeclaration_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HostType__Alternatives"


    // $ANTLR start "rule__Mapping__Group__0"
    // InternalJniMap.g:1720:1: rule__Mapping__Group__0 : rule__Mapping__Group__0__Impl rule__Mapping__Group__1 ;
    public final void rule__Mapping__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1724:1: ( rule__Mapping__Group__0__Impl rule__Mapping__Group__1 )
            // InternalJniMap.g:1725:2: rule__Mapping__Group__0__Impl rule__Mapping__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Mapping__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__0"


    // $ANTLR start "rule__Mapping__Group__0__Impl"
    // InternalJniMap.g:1732:1: rule__Mapping__Group__0__Impl : ( ( rule__Mapping__ImportsAssignment_0 )* ) ;
    public final void rule__Mapping__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1736:1: ( ( ( rule__Mapping__ImportsAssignment_0 )* ) )
            // InternalJniMap.g:1737:1: ( ( rule__Mapping__ImportsAssignment_0 )* )
            {
            // InternalJniMap.g:1737:1: ( ( rule__Mapping__ImportsAssignment_0 )* )
            // InternalJniMap.g:1738:1: ( rule__Mapping__ImportsAssignment_0 )*
            {
             before(grammarAccess.getMappingAccess().getImportsAssignment_0()); 
            // InternalJniMap.g:1739:1: ( rule__Mapping__ImportsAssignment_0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==34) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalJniMap.g:1739:2: rule__Mapping__ImportsAssignment_0
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Mapping__ImportsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getImportsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__0__Impl"


    // $ANTLR start "rule__Mapping__Group__1"
    // InternalJniMap.g:1749:1: rule__Mapping__Group__1 : rule__Mapping__Group__1__Impl rule__Mapping__Group__2 ;
    public final void rule__Mapping__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1753:1: ( rule__Mapping__Group__1__Impl rule__Mapping__Group__2 )
            // InternalJniMap.g:1754:2: rule__Mapping__Group__1__Impl rule__Mapping__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Mapping__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__1"


    // $ANTLR start "rule__Mapping__Group__1__Impl"
    // InternalJniMap.g:1761:1: rule__Mapping__Group__1__Impl : ( 'mapping' ) ;
    public final void rule__Mapping__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1765:1: ( ( 'mapping' ) )
            // InternalJniMap.g:1766:1: ( 'mapping' )
            {
            // InternalJniMap.g:1766:1: ( 'mapping' )
            // InternalJniMap.g:1767:1: 'mapping'
            {
             before(grammarAccess.getMappingAccess().getMappingKeyword_1()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getMappingKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__1__Impl"


    // $ANTLR start "rule__Mapping__Group__2"
    // InternalJniMap.g:1780:1: rule__Mapping__Group__2 : rule__Mapping__Group__2__Impl rule__Mapping__Group__3 ;
    public final void rule__Mapping__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1784:1: ( rule__Mapping__Group__2__Impl rule__Mapping__Group__3 )
            // InternalJniMap.g:1785:2: rule__Mapping__Group__2__Impl rule__Mapping__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Mapping__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__2"


    // $ANTLR start "rule__Mapping__Group__2__Impl"
    // InternalJniMap.g:1792:1: rule__Mapping__Group__2__Impl : ( ( rule__Mapping__NameAssignment_2 ) ) ;
    public final void rule__Mapping__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1796:1: ( ( ( rule__Mapping__NameAssignment_2 ) ) )
            // InternalJniMap.g:1797:1: ( ( rule__Mapping__NameAssignment_2 ) )
            {
            // InternalJniMap.g:1797:1: ( ( rule__Mapping__NameAssignment_2 ) )
            // InternalJniMap.g:1798:1: ( rule__Mapping__NameAssignment_2 )
            {
             before(grammarAccess.getMappingAccess().getNameAssignment_2()); 
            // InternalJniMap.g:1799:1: ( rule__Mapping__NameAssignment_2 )
            // InternalJniMap.g:1799:2: rule__Mapping__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__2__Impl"


    // $ANTLR start "rule__Mapping__Group__3"
    // InternalJniMap.g:1809:1: rule__Mapping__Group__3 : rule__Mapping__Group__3__Impl rule__Mapping__Group__4 ;
    public final void rule__Mapping__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1813:1: ( rule__Mapping__Group__3__Impl rule__Mapping__Group__4 )
            // InternalJniMap.g:1814:2: rule__Mapping__Group__3__Impl rule__Mapping__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Mapping__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__3"


    // $ANTLR start "rule__Mapping__Group__3__Impl"
    // InternalJniMap.g:1821:1: rule__Mapping__Group__3__Impl : ( ( rule__Mapping__Group_3__0 )? ) ;
    public final void rule__Mapping__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1825:1: ( ( ( rule__Mapping__Group_3__0 )? ) )
            // InternalJniMap.g:1826:1: ( ( rule__Mapping__Group_3__0 )? )
            {
            // InternalJniMap.g:1826:1: ( ( rule__Mapping__Group_3__0 )? )
            // InternalJniMap.g:1827:1: ( rule__Mapping__Group_3__0 )?
            {
             before(grammarAccess.getMappingAccess().getGroup_3()); 
            // InternalJniMap.g:1828:1: ( rule__Mapping__Group_3__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==27) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalJniMap.g:1828:2: rule__Mapping__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMappingAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__3__Impl"


    // $ANTLR start "rule__Mapping__Group__4"
    // InternalJniMap.g:1838:1: rule__Mapping__Group__4 : rule__Mapping__Group__4__Impl rule__Mapping__Group__5 ;
    public final void rule__Mapping__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1842:1: ( rule__Mapping__Group__4__Impl rule__Mapping__Group__5 )
            // InternalJniMap.g:1843:2: rule__Mapping__Group__4__Impl rule__Mapping__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__Mapping__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__4"


    // $ANTLR start "rule__Mapping__Group__4__Impl"
    // InternalJniMap.g:1850:1: rule__Mapping__Group__4__Impl : ( ( rule__Mapping__Group_4__0 ) ) ;
    public final void rule__Mapping__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1854:1: ( ( ( rule__Mapping__Group_4__0 ) ) )
            // InternalJniMap.g:1855:1: ( ( rule__Mapping__Group_4__0 ) )
            {
            // InternalJniMap.g:1855:1: ( ( rule__Mapping__Group_4__0 ) )
            // InternalJniMap.g:1856:1: ( rule__Mapping__Group_4__0 )
            {
             before(grammarAccess.getMappingAccess().getGroup_4()); 
            // InternalJniMap.g:1857:1: ( rule__Mapping__Group_4__0 )
            // InternalJniMap.g:1857:2: rule__Mapping__Group_4__0
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_4__0();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__4__Impl"


    // $ANTLR start "rule__Mapping__Group__5"
    // InternalJniMap.g:1867:1: rule__Mapping__Group__5 : rule__Mapping__Group__5__Impl rule__Mapping__Group__6 ;
    public final void rule__Mapping__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1871:1: ( rule__Mapping__Group__5__Impl rule__Mapping__Group__6 )
            // InternalJniMap.g:1872:2: rule__Mapping__Group__5__Impl rule__Mapping__Group__6
            {
            pushFollow(FOLLOW_8);
            rule__Mapping__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__5"


    // $ANTLR start "rule__Mapping__Group__5__Impl"
    // InternalJniMap.g:1879:1: rule__Mapping__Group__5__Impl : ( ';' ) ;
    public final void rule__Mapping__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1883:1: ( ( ';' ) )
            // InternalJniMap.g:1884:1: ( ';' )
            {
            // InternalJniMap.g:1884:1: ( ';' )
            // InternalJniMap.g:1885:1: ';'
            {
             before(grammarAccess.getMappingAccess().getSemicolonKeyword_5()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__5__Impl"


    // $ANTLR start "rule__Mapping__Group__6"
    // InternalJniMap.g:1898:1: rule__Mapping__Group__6 : rule__Mapping__Group__6__Impl rule__Mapping__Group__7 ;
    public final void rule__Mapping__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1902:1: ( rule__Mapping__Group__6__Impl rule__Mapping__Group__7 )
            // InternalJniMap.g:1903:2: rule__Mapping__Group__6__Impl rule__Mapping__Group__7
            {
            pushFollow(FOLLOW_8);
            rule__Mapping__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__6"


    // $ANTLR start "rule__Mapping__Group__6__Impl"
    // InternalJniMap.g:1910:1: rule__Mapping__Group__6__Impl : ( ( rule__Mapping__ExternalLibrariesAssignment_6 )* ) ;
    public final void rule__Mapping__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1914:1: ( ( ( rule__Mapping__ExternalLibrariesAssignment_6 )* ) )
            // InternalJniMap.g:1915:1: ( ( rule__Mapping__ExternalLibrariesAssignment_6 )* )
            {
            // InternalJniMap.g:1915:1: ( ( rule__Mapping__ExternalLibrariesAssignment_6 )* )
            // InternalJniMap.g:1916:1: ( rule__Mapping__ExternalLibrariesAssignment_6 )*
            {
             before(grammarAccess.getMappingAccess().getExternalLibrariesAssignment_6()); 
            // InternalJniMap.g:1917:1: ( rule__Mapping__ExternalLibrariesAssignment_6 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==42) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalJniMap.g:1917:2: rule__Mapping__ExternalLibrariesAssignment_6
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Mapping__ExternalLibrariesAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getExternalLibrariesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__6__Impl"


    // $ANTLR start "rule__Mapping__Group__7"
    // InternalJniMap.g:1927:1: rule__Mapping__Group__7 : rule__Mapping__Group__7__Impl rule__Mapping__Group__8 ;
    public final void rule__Mapping__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1931:1: ( rule__Mapping__Group__7__Impl rule__Mapping__Group__8 )
            // InternalJniMap.g:1932:2: rule__Mapping__Group__7__Impl rule__Mapping__Group__8
            {
            pushFollow(FOLLOW_8);
            rule__Mapping__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__7"


    // $ANTLR start "rule__Mapping__Group__7__Impl"
    // InternalJniMap.g:1939:1: rule__Mapping__Group__7__Impl : ( ( rule__Mapping__LibrariesAssignment_7 )* ) ;
    public final void rule__Mapping__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1943:1: ( ( ( rule__Mapping__LibrariesAssignment_7 )* ) )
            // InternalJniMap.g:1944:1: ( ( rule__Mapping__LibrariesAssignment_7 )* )
            {
            // InternalJniMap.g:1944:1: ( ( rule__Mapping__LibrariesAssignment_7 )* )
            // InternalJniMap.g:1945:1: ( rule__Mapping__LibrariesAssignment_7 )*
            {
             before(grammarAccess.getMappingAccess().getLibrariesAssignment_7()); 
            // InternalJniMap.g:1946:1: ( rule__Mapping__LibrariesAssignment_7 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==44) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalJniMap.g:1946:2: rule__Mapping__LibrariesAssignment_7
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Mapping__LibrariesAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getLibrariesAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__7__Impl"


    // $ANTLR start "rule__Mapping__Group__8"
    // InternalJniMap.g:1956:1: rule__Mapping__Group__8 : rule__Mapping__Group__8__Impl rule__Mapping__Group__9 ;
    public final void rule__Mapping__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1960:1: ( rule__Mapping__Group__8__Impl rule__Mapping__Group__9 )
            // InternalJniMap.g:1961:2: rule__Mapping__Group__8__Impl rule__Mapping__Group__9
            {
            pushFollow(FOLLOW_8);
            rule__Mapping__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__8"


    // $ANTLR start "rule__Mapping__Group__8__Impl"
    // InternalJniMap.g:1968:1: rule__Mapping__Group__8__Impl : ( ( rule__Mapping__UserModulesAssignment_8 )* ) ;
    public final void rule__Mapping__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1972:1: ( ( ( rule__Mapping__UserModulesAssignment_8 )* ) )
            // InternalJniMap.g:1973:1: ( ( rule__Mapping__UserModulesAssignment_8 )* )
            {
            // InternalJniMap.g:1973:1: ( ( rule__Mapping__UserModulesAssignment_8 )* )
            // InternalJniMap.g:1974:1: ( rule__Mapping__UserModulesAssignment_8 )*
            {
             before(grammarAccess.getMappingAccess().getUserModulesAssignment_8()); 
            // InternalJniMap.g:1975:1: ( rule__Mapping__UserModulesAssignment_8 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==49) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalJniMap.g:1975:2: rule__Mapping__UserModulesAssignment_8
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__Mapping__UserModulesAssignment_8();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getUserModulesAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__8__Impl"


    // $ANTLR start "rule__Mapping__Group__9"
    // InternalJniMap.g:1985:1: rule__Mapping__Group__9 : rule__Mapping__Group__9__Impl rule__Mapping__Group__10 ;
    public final void rule__Mapping__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:1989:1: ( rule__Mapping__Group__9__Impl rule__Mapping__Group__10 )
            // InternalJniMap.g:1990:2: rule__Mapping__Group__9__Impl rule__Mapping__Group__10
            {
            pushFollow(FOLLOW_12);
            rule__Mapping__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__9"


    // $ANTLR start "rule__Mapping__Group__9__Impl"
    // InternalJniMap.g:1997:1: rule__Mapping__Group__9__Impl : ( ( ( rule__Mapping__Alternatives_9 ) ) ( ( rule__Mapping__Alternatives_9 )* ) ) ;
    public final void rule__Mapping__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2001:1: ( ( ( ( rule__Mapping__Alternatives_9 ) ) ( ( rule__Mapping__Alternatives_9 )* ) ) )
            // InternalJniMap.g:2002:1: ( ( ( rule__Mapping__Alternatives_9 ) ) ( ( rule__Mapping__Alternatives_9 )* ) )
            {
            // InternalJniMap.g:2002:1: ( ( ( rule__Mapping__Alternatives_9 ) ) ( ( rule__Mapping__Alternatives_9 )* ) )
            // InternalJniMap.g:2003:1: ( ( rule__Mapping__Alternatives_9 ) ) ( ( rule__Mapping__Alternatives_9 )* )
            {
            // InternalJniMap.g:2003:1: ( ( rule__Mapping__Alternatives_9 ) )
            // InternalJniMap.g:2004:1: ( rule__Mapping__Alternatives_9 )
            {
             before(grammarAccess.getMappingAccess().getAlternatives_9()); 
            // InternalJniMap.g:2005:1: ( rule__Mapping__Alternatives_9 )
            // InternalJniMap.g:2005:2: rule__Mapping__Alternatives_9
            {
            pushFollow(FOLLOW_13);
            rule__Mapping__Alternatives_9();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getAlternatives_9()); 

            }

            // InternalJniMap.g:2008:1: ( ( rule__Mapping__Alternatives_9 )* )
            // InternalJniMap.g:2009:1: ( rule__Mapping__Alternatives_9 )*
            {
             before(grammarAccess.getMappingAccess().getAlternatives_9()); 
            // InternalJniMap.g:2010:1: ( rule__Mapping__Alternatives_9 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( ((LA20_0>=40 && LA20_0<=41)||LA20_0==48||(LA20_0>=50 && LA20_0<=53)) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalJniMap.g:2010:2: rule__Mapping__Alternatives_9
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__Mapping__Alternatives_9();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getAlternatives_9()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__9__Impl"


    // $ANTLR start "rule__Mapping__Group__10"
    // InternalJniMap.g:2021:1: rule__Mapping__Group__10 : rule__Mapping__Group__10__Impl ;
    public final void rule__Mapping__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2025:1: ( rule__Mapping__Group__10__Impl )
            // InternalJniMap.g:2026:2: rule__Mapping__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__10"


    // $ANTLR start "rule__Mapping__Group__10__Impl"
    // InternalJniMap.g:2032:1: rule__Mapping__Group__10__Impl : ( ( ( rule__Mapping__MethodGroupsAssignment_10 ) ) ( ( rule__Mapping__MethodGroupsAssignment_10 )* ) ) ;
    public final void rule__Mapping__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2036:1: ( ( ( ( rule__Mapping__MethodGroupsAssignment_10 ) ) ( ( rule__Mapping__MethodGroupsAssignment_10 )* ) ) )
            // InternalJniMap.g:2037:1: ( ( ( rule__Mapping__MethodGroupsAssignment_10 ) ) ( ( rule__Mapping__MethodGroupsAssignment_10 )* ) )
            {
            // InternalJniMap.g:2037:1: ( ( ( rule__Mapping__MethodGroupsAssignment_10 ) ) ( ( rule__Mapping__MethodGroupsAssignment_10 )* ) )
            // InternalJniMap.g:2038:1: ( ( rule__Mapping__MethodGroupsAssignment_10 ) ) ( ( rule__Mapping__MethodGroupsAssignment_10 )* )
            {
            // InternalJniMap.g:2038:1: ( ( rule__Mapping__MethodGroupsAssignment_10 ) )
            // InternalJniMap.g:2039:1: ( rule__Mapping__MethodGroupsAssignment_10 )
            {
             before(grammarAccess.getMappingAccess().getMethodGroupsAssignment_10()); 
            // InternalJniMap.g:2040:1: ( rule__Mapping__MethodGroupsAssignment_10 )
            // InternalJniMap.g:2040:2: rule__Mapping__MethodGroupsAssignment_10
            {
            pushFollow(FOLLOW_14);
            rule__Mapping__MethodGroupsAssignment_10();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getMethodGroupsAssignment_10()); 

            }

            // InternalJniMap.g:2043:1: ( ( rule__Mapping__MethodGroupsAssignment_10 )* )
            // InternalJniMap.g:2044:1: ( rule__Mapping__MethodGroupsAssignment_10 )*
            {
             before(grammarAccess.getMappingAccess().getMethodGroupsAssignment_10()); 
            // InternalJniMap.g:2045:1: ( rule__Mapping__MethodGroupsAssignment_10 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==54) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalJniMap.g:2045:2: rule__Mapping__MethodGroupsAssignment_10
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Mapping__MethodGroupsAssignment_10();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getMethodGroupsAssignment_10()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__10__Impl"


    // $ANTLR start "rule__Mapping__Group_3__0"
    // InternalJniMap.g:2078:1: rule__Mapping__Group_3__0 : rule__Mapping__Group_3__0__Impl rule__Mapping__Group_3__1 ;
    public final void rule__Mapping__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2082:1: ( rule__Mapping__Group_3__0__Impl rule__Mapping__Group_3__1 )
            // InternalJniMap.g:2083:2: rule__Mapping__Group_3__0__Impl rule__Mapping__Group_3__1
            {
            pushFollow(FOLLOW_15);
            rule__Mapping__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__0"


    // $ANTLR start "rule__Mapping__Group_3__0__Impl"
    // InternalJniMap.g:2090:1: rule__Mapping__Group_3__0__Impl : ( '(' ) ;
    public final void rule__Mapping__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2094:1: ( ( '(' ) )
            // InternalJniMap.g:2095:1: ( '(' )
            {
            // InternalJniMap.g:2095:1: ( '(' )
            // InternalJniMap.g:2096:1: '('
            {
             before(grammarAccess.getMappingAccess().getLeftParenthesisKeyword_3_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getLeftParenthesisKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__0__Impl"


    // $ANTLR start "rule__Mapping__Group_3__1"
    // InternalJniMap.g:2109:1: rule__Mapping__Group_3__1 : rule__Mapping__Group_3__1__Impl rule__Mapping__Group_3__2 ;
    public final void rule__Mapping__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2113:1: ( rule__Mapping__Group_3__1__Impl rule__Mapping__Group_3__2 )
            // InternalJniMap.g:2114:2: rule__Mapping__Group_3__1__Impl rule__Mapping__Group_3__2
            {
            pushFollow(FOLLOW_16);
            rule__Mapping__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__1"


    // $ANTLR start "rule__Mapping__Group_3__1__Impl"
    // InternalJniMap.g:2121:1: rule__Mapping__Group_3__1__Impl : ( 'package' ) ;
    public final void rule__Mapping__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2125:1: ( ( 'package' ) )
            // InternalJniMap.g:2126:1: ( 'package' )
            {
            // InternalJniMap.g:2126:1: ( 'package' )
            // InternalJniMap.g:2127:1: 'package'
            {
             before(grammarAccess.getMappingAccess().getPackageKeyword_3_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getPackageKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__1__Impl"


    // $ANTLR start "rule__Mapping__Group_3__2"
    // InternalJniMap.g:2140:1: rule__Mapping__Group_3__2 : rule__Mapping__Group_3__2__Impl rule__Mapping__Group_3__3 ;
    public final void rule__Mapping__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2144:1: ( rule__Mapping__Group_3__2__Impl rule__Mapping__Group_3__3 )
            // InternalJniMap.g:2145:2: rule__Mapping__Group_3__2__Impl rule__Mapping__Group_3__3
            {
            pushFollow(FOLLOW_17);
            rule__Mapping__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__2"


    // $ANTLR start "rule__Mapping__Group_3__2__Impl"
    // InternalJniMap.g:2152:1: rule__Mapping__Group_3__2__Impl : ( '=' ) ;
    public final void rule__Mapping__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2156:1: ( ( '=' ) )
            // InternalJniMap.g:2157:1: ( '=' )
            {
            // InternalJniMap.g:2157:1: ( '=' )
            // InternalJniMap.g:2158:1: '='
            {
             before(grammarAccess.getMappingAccess().getEqualsSignKeyword_3_2()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getEqualsSignKeyword_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__2__Impl"


    // $ANTLR start "rule__Mapping__Group_3__3"
    // InternalJniMap.g:2171:1: rule__Mapping__Group_3__3 : rule__Mapping__Group_3__3__Impl rule__Mapping__Group_3__4 ;
    public final void rule__Mapping__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2175:1: ( rule__Mapping__Group_3__3__Impl rule__Mapping__Group_3__4 )
            // InternalJniMap.g:2176:2: rule__Mapping__Group_3__3__Impl rule__Mapping__Group_3__4
            {
            pushFollow(FOLLOW_7);
            rule__Mapping__Group_3__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_3__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__3"


    // $ANTLR start "rule__Mapping__Group_3__3__Impl"
    // InternalJniMap.g:2183:1: rule__Mapping__Group_3__3__Impl : ( ( rule__Mapping__PackageAssignment_3_3 ) ) ;
    public final void rule__Mapping__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2187:1: ( ( ( rule__Mapping__PackageAssignment_3_3 ) ) )
            // InternalJniMap.g:2188:1: ( ( rule__Mapping__PackageAssignment_3_3 ) )
            {
            // InternalJniMap.g:2188:1: ( ( rule__Mapping__PackageAssignment_3_3 ) )
            // InternalJniMap.g:2189:1: ( rule__Mapping__PackageAssignment_3_3 )
            {
             before(grammarAccess.getMappingAccess().getPackageAssignment_3_3()); 
            // InternalJniMap.g:2190:1: ( rule__Mapping__PackageAssignment_3_3 )
            // InternalJniMap.g:2190:2: rule__Mapping__PackageAssignment_3_3
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__PackageAssignment_3_3();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getPackageAssignment_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__3__Impl"


    // $ANTLR start "rule__Mapping__Group_3__4"
    // InternalJniMap.g:2200:1: rule__Mapping__Group_3__4 : rule__Mapping__Group_3__4__Impl rule__Mapping__Group_3__5 ;
    public final void rule__Mapping__Group_3__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2204:1: ( rule__Mapping__Group_3__4__Impl rule__Mapping__Group_3__5 )
            // InternalJniMap.g:2205:2: rule__Mapping__Group_3__4__Impl rule__Mapping__Group_3__5
            {
            pushFollow(FOLLOW_18);
            rule__Mapping__Group_3__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_3__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__4"


    // $ANTLR start "rule__Mapping__Group_3__4__Impl"
    // InternalJniMap.g:2212:1: rule__Mapping__Group_3__4__Impl : ( ';' ) ;
    public final void rule__Mapping__Group_3__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2216:1: ( ( ';' ) )
            // InternalJniMap.g:2217:1: ( ';' )
            {
            // InternalJniMap.g:2217:1: ( ';' )
            // InternalJniMap.g:2218:1: ';'
            {
             before(grammarAccess.getMappingAccess().getSemicolonKeyword_3_4()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getSemicolonKeyword_3_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__4__Impl"


    // $ANTLR start "rule__Mapping__Group_3__5"
    // InternalJniMap.g:2231:1: rule__Mapping__Group_3__5 : rule__Mapping__Group_3__5__Impl ;
    public final void rule__Mapping__Group_3__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2235:1: ( rule__Mapping__Group_3__5__Impl )
            // InternalJniMap.g:2236:2: rule__Mapping__Group_3__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_3__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__5"


    // $ANTLR start "rule__Mapping__Group_3__5__Impl"
    // InternalJniMap.g:2242:1: rule__Mapping__Group_3__5__Impl : ( ')' ) ;
    public final void rule__Mapping__Group_3__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2246:1: ( ( ')' ) )
            // InternalJniMap.g:2247:1: ( ')' )
            {
            // InternalJniMap.g:2247:1: ( ')' )
            // InternalJniMap.g:2248:1: ')'
            {
             before(grammarAccess.getMappingAccess().getRightParenthesisKeyword_3_5()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getRightParenthesisKeyword_3_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__5__Impl"


    // $ANTLR start "rule__Mapping__Group_4__0"
    // InternalJniMap.g:2273:1: rule__Mapping__Group_4__0 : rule__Mapping__Group_4__0__Impl rule__Mapping__Group_4__1 ;
    public final void rule__Mapping__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2277:1: ( rule__Mapping__Group_4__0__Impl rule__Mapping__Group_4__1 )
            // InternalJniMap.g:2278:2: rule__Mapping__Group_4__0__Impl rule__Mapping__Group_4__1
            {
            pushFollow(FOLLOW_19);
            rule__Mapping__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__0"


    // $ANTLR start "rule__Mapping__Group_4__0__Impl"
    // InternalJniMap.g:2285:1: rule__Mapping__Group_4__0__Impl : ( '[' ) ;
    public final void rule__Mapping__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2289:1: ( ( '[' ) )
            // InternalJniMap.g:2290:1: ( '[' )
            {
            // InternalJniMap.g:2290:1: ( '[' )
            // InternalJniMap.g:2291:1: '['
            {
             before(grammarAccess.getMappingAccess().getLeftSquareBracketKeyword_4_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getLeftSquareBracketKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__0__Impl"


    // $ANTLR start "rule__Mapping__Group_4__1"
    // InternalJniMap.g:2304:1: rule__Mapping__Group_4__1 : rule__Mapping__Group_4__1__Impl rule__Mapping__Group_4__2 ;
    public final void rule__Mapping__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2308:1: ( rule__Mapping__Group_4__1__Impl rule__Mapping__Group_4__2 )
            // InternalJniMap.g:2309:2: rule__Mapping__Group_4__1__Impl rule__Mapping__Group_4__2
            {
            pushFollow(FOLLOW_20);
            rule__Mapping__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__1"


    // $ANTLR start "rule__Mapping__Group_4__1__Impl"
    // InternalJniMap.g:2316:1: rule__Mapping__Group_4__1__Impl : ( ( rule__Mapping__HostsAssignment_4_1 ) ) ;
    public final void rule__Mapping__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2320:1: ( ( ( rule__Mapping__HostsAssignment_4_1 ) ) )
            // InternalJniMap.g:2321:1: ( ( rule__Mapping__HostsAssignment_4_1 ) )
            {
            // InternalJniMap.g:2321:1: ( ( rule__Mapping__HostsAssignment_4_1 ) )
            // InternalJniMap.g:2322:1: ( rule__Mapping__HostsAssignment_4_1 )
            {
             before(grammarAccess.getMappingAccess().getHostsAssignment_4_1()); 
            // InternalJniMap.g:2323:1: ( rule__Mapping__HostsAssignment_4_1 )
            // InternalJniMap.g:2323:2: rule__Mapping__HostsAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__HostsAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getHostsAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__1__Impl"


    // $ANTLR start "rule__Mapping__Group_4__2"
    // InternalJniMap.g:2333:1: rule__Mapping__Group_4__2 : rule__Mapping__Group_4__2__Impl rule__Mapping__Group_4__3 ;
    public final void rule__Mapping__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2337:1: ( rule__Mapping__Group_4__2__Impl rule__Mapping__Group_4__3 )
            // InternalJniMap.g:2338:2: rule__Mapping__Group_4__2__Impl rule__Mapping__Group_4__3
            {
            pushFollow(FOLLOW_20);
            rule__Mapping__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__2"


    // $ANTLR start "rule__Mapping__Group_4__2__Impl"
    // InternalJniMap.g:2345:1: rule__Mapping__Group_4__2__Impl : ( ( rule__Mapping__Group_4_2__0 )* ) ;
    public final void rule__Mapping__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2349:1: ( ( ( rule__Mapping__Group_4_2__0 )* ) )
            // InternalJniMap.g:2350:1: ( ( rule__Mapping__Group_4_2__0 )* )
            {
            // InternalJniMap.g:2350:1: ( ( rule__Mapping__Group_4_2__0 )* )
            // InternalJniMap.g:2351:1: ( rule__Mapping__Group_4_2__0 )*
            {
             before(grammarAccess.getMappingAccess().getGroup_4_2()); 
            // InternalJniMap.g:2352:1: ( rule__Mapping__Group_4_2__0 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==33) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalJniMap.g:2352:2: rule__Mapping__Group_4_2__0
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__Mapping__Group_4_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getGroup_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__2__Impl"


    // $ANTLR start "rule__Mapping__Group_4__3"
    // InternalJniMap.g:2362:1: rule__Mapping__Group_4__3 : rule__Mapping__Group_4__3__Impl ;
    public final void rule__Mapping__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2366:1: ( rule__Mapping__Group_4__3__Impl )
            // InternalJniMap.g:2367:2: rule__Mapping__Group_4__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__3"


    // $ANTLR start "rule__Mapping__Group_4__3__Impl"
    // InternalJniMap.g:2373:1: rule__Mapping__Group_4__3__Impl : ( ']' ) ;
    public final void rule__Mapping__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2377:1: ( ( ']' ) )
            // InternalJniMap.g:2378:1: ( ']' )
            {
            // InternalJniMap.g:2378:1: ( ']' )
            // InternalJniMap.g:2379:1: ']'
            {
             before(grammarAccess.getMappingAccess().getRightSquareBracketKeyword_4_3()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getRightSquareBracketKeyword_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__3__Impl"


    // $ANTLR start "rule__Mapping__Group_4_2__0"
    // InternalJniMap.g:2400:1: rule__Mapping__Group_4_2__0 : rule__Mapping__Group_4_2__0__Impl rule__Mapping__Group_4_2__1 ;
    public final void rule__Mapping__Group_4_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2404:1: ( rule__Mapping__Group_4_2__0__Impl rule__Mapping__Group_4_2__1 )
            // InternalJniMap.g:2405:2: rule__Mapping__Group_4_2__0__Impl rule__Mapping__Group_4_2__1
            {
            pushFollow(FOLLOW_19);
            rule__Mapping__Group_4_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_4_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4_2__0"


    // $ANTLR start "rule__Mapping__Group_4_2__0__Impl"
    // InternalJniMap.g:2412:1: rule__Mapping__Group_4_2__0__Impl : ( ',' ) ;
    public final void rule__Mapping__Group_4_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2416:1: ( ( ',' ) )
            // InternalJniMap.g:2417:1: ( ',' )
            {
            // InternalJniMap.g:2417:1: ( ',' )
            // InternalJniMap.g:2418:1: ','
            {
             before(grammarAccess.getMappingAccess().getCommaKeyword_4_2_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getCommaKeyword_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4_2__0__Impl"


    // $ANTLR start "rule__Mapping__Group_4_2__1"
    // InternalJniMap.g:2431:1: rule__Mapping__Group_4_2__1 : rule__Mapping__Group_4_2__1__Impl ;
    public final void rule__Mapping__Group_4_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2435:1: ( rule__Mapping__Group_4_2__1__Impl )
            // InternalJniMap.g:2436:2: rule__Mapping__Group_4_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_4_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4_2__1"


    // $ANTLR start "rule__Mapping__Group_4_2__1__Impl"
    // InternalJniMap.g:2442:1: rule__Mapping__Group_4_2__1__Impl : ( ( rule__Mapping__HostsAssignment_4_2_1 ) ) ;
    public final void rule__Mapping__Group_4_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2446:1: ( ( ( rule__Mapping__HostsAssignment_4_2_1 ) ) )
            // InternalJniMap.g:2447:1: ( ( rule__Mapping__HostsAssignment_4_2_1 ) )
            {
            // InternalJniMap.g:2447:1: ( ( rule__Mapping__HostsAssignment_4_2_1 ) )
            // InternalJniMap.g:2448:1: ( rule__Mapping__HostsAssignment_4_2_1 )
            {
             before(grammarAccess.getMappingAccess().getHostsAssignment_4_2_1()); 
            // InternalJniMap.g:2449:1: ( rule__Mapping__HostsAssignment_4_2_1 )
            // InternalJniMap.g:2449:2: rule__Mapping__HostsAssignment_4_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__HostsAssignment_4_2_1();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getHostsAssignment_4_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4_2__1__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalJniMap.g:2463:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2467:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalJniMap.g:2468:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalJniMap.g:2475:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2479:1: ( ( 'import' ) )
            // InternalJniMap.g:2480:1: ( 'import' )
            {
            // InternalJniMap.g:2480:1: ( 'import' )
            // InternalJniMap.g:2481:1: 'import'
            {
             before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalJniMap.g:2494:1: rule__Import__Group__1 : rule__Import__Group__1__Impl rule__Import__Group__2 ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2498:1: ( rule__Import__Group__1__Impl rule__Import__Group__2 )
            // InternalJniMap.g:2499:2: rule__Import__Group__1__Impl rule__Import__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Import__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalJniMap.g:2506:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportURIAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2510:1: ( ( ( rule__Import__ImportURIAssignment_1 ) ) )
            // InternalJniMap.g:2511:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            {
            // InternalJniMap.g:2511:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            // InternalJniMap.g:2512:1: ( rule__Import__ImportURIAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            // InternalJniMap.g:2513:1: ( rule__Import__ImportURIAssignment_1 )
            // InternalJniMap.g:2513:2: rule__Import__ImportURIAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Import__ImportURIAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getImportURIAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__Import__Group__2"
    // InternalJniMap.g:2523:1: rule__Import__Group__2 : rule__Import__Group__2__Impl ;
    public final void rule__Import__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2527:1: ( rule__Import__Group__2__Impl )
            // InternalJniMap.g:2528:2: rule__Import__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__2"


    // $ANTLR start "rule__Import__Group__2__Impl"
    // InternalJniMap.g:2534:1: rule__Import__Group__2__Impl : ( ';' ) ;
    public final void rule__Import__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2538:1: ( ( ';' ) )
            // InternalJniMap.g:2539:1: ( ';' )
            {
            // InternalJniMap.g:2539:1: ( ';' )
            // InternalJniMap.g:2540:1: ';'
            {
             before(grammarAccess.getImportAccess().getSemicolonKeyword_2()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__2__Impl"


    // $ANTLR start "rule__GenericsDeclaration__Group__0"
    // InternalJniMap.g:2559:1: rule__GenericsDeclaration__Group__0 : rule__GenericsDeclaration__Group__0__Impl rule__GenericsDeclaration__Group__1 ;
    public final void rule__GenericsDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2563:1: ( rule__GenericsDeclaration__Group__0__Impl rule__GenericsDeclaration__Group__1 )
            // InternalJniMap.g:2564:2: rule__GenericsDeclaration__Group__0__Impl rule__GenericsDeclaration__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__GenericsDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GenericsDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__Group__0"


    // $ANTLR start "rule__GenericsDeclaration__Group__0__Impl"
    // InternalJniMap.g:2571:1: rule__GenericsDeclaration__Group__0__Impl : ( '<' ) ;
    public final void rule__GenericsDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2575:1: ( ( '<' ) )
            // InternalJniMap.g:2576:1: ( '<' )
            {
            // InternalJniMap.g:2576:1: ( '<' )
            // InternalJniMap.g:2577:1: '<'
            {
             before(grammarAccess.getGenericsDeclarationAccess().getLessThanSignKeyword_0()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getGenericsDeclarationAccess().getLessThanSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__Group__0__Impl"


    // $ANTLR start "rule__GenericsDeclaration__Group__1"
    // InternalJniMap.g:2590:1: rule__GenericsDeclaration__Group__1 : rule__GenericsDeclaration__Group__1__Impl rule__GenericsDeclaration__Group__2 ;
    public final void rule__GenericsDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2594:1: ( rule__GenericsDeclaration__Group__1__Impl rule__GenericsDeclaration__Group__2 )
            // InternalJniMap.g:2595:2: rule__GenericsDeclaration__Group__1__Impl rule__GenericsDeclaration__Group__2
            {
            pushFollow(FOLLOW_22);
            rule__GenericsDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GenericsDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__Group__1"


    // $ANTLR start "rule__GenericsDeclaration__Group__1__Impl"
    // InternalJniMap.g:2602:1: rule__GenericsDeclaration__Group__1__Impl : ( ( rule__GenericsDeclaration__NamesAssignment_1 ) ) ;
    public final void rule__GenericsDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2606:1: ( ( ( rule__GenericsDeclaration__NamesAssignment_1 ) ) )
            // InternalJniMap.g:2607:1: ( ( rule__GenericsDeclaration__NamesAssignment_1 ) )
            {
            // InternalJniMap.g:2607:1: ( ( rule__GenericsDeclaration__NamesAssignment_1 ) )
            // InternalJniMap.g:2608:1: ( rule__GenericsDeclaration__NamesAssignment_1 )
            {
             before(grammarAccess.getGenericsDeclarationAccess().getNamesAssignment_1()); 
            // InternalJniMap.g:2609:1: ( rule__GenericsDeclaration__NamesAssignment_1 )
            // InternalJniMap.g:2609:2: rule__GenericsDeclaration__NamesAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__GenericsDeclaration__NamesAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getGenericsDeclarationAccess().getNamesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__Group__1__Impl"


    // $ANTLR start "rule__GenericsDeclaration__Group__2"
    // InternalJniMap.g:2619:1: rule__GenericsDeclaration__Group__2 : rule__GenericsDeclaration__Group__2__Impl rule__GenericsDeclaration__Group__3 ;
    public final void rule__GenericsDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2623:1: ( rule__GenericsDeclaration__Group__2__Impl rule__GenericsDeclaration__Group__3 )
            // InternalJniMap.g:2624:2: rule__GenericsDeclaration__Group__2__Impl rule__GenericsDeclaration__Group__3
            {
            pushFollow(FOLLOW_22);
            rule__GenericsDeclaration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GenericsDeclaration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__Group__2"


    // $ANTLR start "rule__GenericsDeclaration__Group__2__Impl"
    // InternalJniMap.g:2631:1: rule__GenericsDeclaration__Group__2__Impl : ( ( rule__GenericsDeclaration__Group_2__0 )* ) ;
    public final void rule__GenericsDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2635:1: ( ( ( rule__GenericsDeclaration__Group_2__0 )* ) )
            // InternalJniMap.g:2636:1: ( ( rule__GenericsDeclaration__Group_2__0 )* )
            {
            // InternalJniMap.g:2636:1: ( ( rule__GenericsDeclaration__Group_2__0 )* )
            // InternalJniMap.g:2637:1: ( rule__GenericsDeclaration__Group_2__0 )*
            {
             before(grammarAccess.getGenericsDeclarationAccess().getGroup_2()); 
            // InternalJniMap.g:2638:1: ( rule__GenericsDeclaration__Group_2__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==33) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalJniMap.g:2638:2: rule__GenericsDeclaration__Group_2__0
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__GenericsDeclaration__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getGenericsDeclarationAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__Group__2__Impl"


    // $ANTLR start "rule__GenericsDeclaration__Group__3"
    // InternalJniMap.g:2648:1: rule__GenericsDeclaration__Group__3 : rule__GenericsDeclaration__Group__3__Impl ;
    public final void rule__GenericsDeclaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2652:1: ( rule__GenericsDeclaration__Group__3__Impl )
            // InternalJniMap.g:2653:2: rule__GenericsDeclaration__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GenericsDeclaration__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__Group__3"


    // $ANTLR start "rule__GenericsDeclaration__Group__3__Impl"
    // InternalJniMap.g:2659:1: rule__GenericsDeclaration__Group__3__Impl : ( '>' ) ;
    public final void rule__GenericsDeclaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2663:1: ( ( '>' ) )
            // InternalJniMap.g:2664:1: ( '>' )
            {
            // InternalJniMap.g:2664:1: ( '>' )
            // InternalJniMap.g:2665:1: '>'
            {
             before(grammarAccess.getGenericsDeclarationAccess().getGreaterThanSignKeyword_3()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getGenericsDeclarationAccess().getGreaterThanSignKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__Group__3__Impl"


    // $ANTLR start "rule__GenericsDeclaration__Group_2__0"
    // InternalJniMap.g:2686:1: rule__GenericsDeclaration__Group_2__0 : rule__GenericsDeclaration__Group_2__0__Impl rule__GenericsDeclaration__Group_2__1 ;
    public final void rule__GenericsDeclaration__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2690:1: ( rule__GenericsDeclaration__Group_2__0__Impl rule__GenericsDeclaration__Group_2__1 )
            // InternalJniMap.g:2691:2: rule__GenericsDeclaration__Group_2__0__Impl rule__GenericsDeclaration__Group_2__1
            {
            pushFollow(FOLLOW_5);
            rule__GenericsDeclaration__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GenericsDeclaration__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__Group_2__0"


    // $ANTLR start "rule__GenericsDeclaration__Group_2__0__Impl"
    // InternalJniMap.g:2698:1: rule__GenericsDeclaration__Group_2__0__Impl : ( ',' ) ;
    public final void rule__GenericsDeclaration__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2702:1: ( ( ',' ) )
            // InternalJniMap.g:2703:1: ( ',' )
            {
            // InternalJniMap.g:2703:1: ( ',' )
            // InternalJniMap.g:2704:1: ','
            {
             before(grammarAccess.getGenericsDeclarationAccess().getCommaKeyword_2_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getGenericsDeclarationAccess().getCommaKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__Group_2__0__Impl"


    // $ANTLR start "rule__GenericsDeclaration__Group_2__1"
    // InternalJniMap.g:2717:1: rule__GenericsDeclaration__Group_2__1 : rule__GenericsDeclaration__Group_2__1__Impl ;
    public final void rule__GenericsDeclaration__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2721:1: ( rule__GenericsDeclaration__Group_2__1__Impl )
            // InternalJniMap.g:2722:2: rule__GenericsDeclaration__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GenericsDeclaration__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__Group_2__1"


    // $ANTLR start "rule__GenericsDeclaration__Group_2__1__Impl"
    // InternalJniMap.g:2728:1: rule__GenericsDeclaration__Group_2__1__Impl : ( ( rule__GenericsDeclaration__NamesAssignment_2_1 ) ) ;
    public final void rule__GenericsDeclaration__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2732:1: ( ( ( rule__GenericsDeclaration__NamesAssignment_2_1 ) ) )
            // InternalJniMap.g:2733:1: ( ( rule__GenericsDeclaration__NamesAssignment_2_1 ) )
            {
            // InternalJniMap.g:2733:1: ( ( rule__GenericsDeclaration__NamesAssignment_2_1 ) )
            // InternalJniMap.g:2734:1: ( rule__GenericsDeclaration__NamesAssignment_2_1 )
            {
             before(grammarAccess.getGenericsDeclarationAccess().getNamesAssignment_2_1()); 
            // InternalJniMap.g:2735:1: ( rule__GenericsDeclaration__NamesAssignment_2_1 )
            // InternalJniMap.g:2735:2: rule__GenericsDeclaration__NamesAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__GenericsDeclaration__NamesAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getGenericsDeclarationAccess().getNamesAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__Group_2__1__Impl"


    // $ANTLR start "rule__GenericsInstantiation__Group__0"
    // InternalJniMap.g:2749:1: rule__GenericsInstantiation__Group__0 : rule__GenericsInstantiation__Group__0__Impl rule__GenericsInstantiation__Group__1 ;
    public final void rule__GenericsInstantiation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2753:1: ( rule__GenericsInstantiation__Group__0__Impl rule__GenericsInstantiation__Group__1 )
            // InternalJniMap.g:2754:2: rule__GenericsInstantiation__Group__0__Impl rule__GenericsInstantiation__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__GenericsInstantiation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GenericsInstantiation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__Group__0"


    // $ANTLR start "rule__GenericsInstantiation__Group__0__Impl"
    // InternalJniMap.g:2761:1: rule__GenericsInstantiation__Group__0__Impl : ( '<' ) ;
    public final void rule__GenericsInstantiation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2765:1: ( ( '<' ) )
            // InternalJniMap.g:2766:1: ( '<' )
            {
            // InternalJniMap.g:2766:1: ( '<' )
            // InternalJniMap.g:2767:1: '<'
            {
             before(grammarAccess.getGenericsInstantiationAccess().getLessThanSignKeyword_0()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getGenericsInstantiationAccess().getLessThanSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__Group__0__Impl"


    // $ANTLR start "rule__GenericsInstantiation__Group__1"
    // InternalJniMap.g:2780:1: rule__GenericsInstantiation__Group__1 : rule__GenericsInstantiation__Group__1__Impl rule__GenericsInstantiation__Group__2 ;
    public final void rule__GenericsInstantiation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2784:1: ( rule__GenericsInstantiation__Group__1__Impl rule__GenericsInstantiation__Group__2 )
            // InternalJniMap.g:2785:2: rule__GenericsInstantiation__Group__1__Impl rule__GenericsInstantiation__Group__2
            {
            pushFollow(FOLLOW_22);
            rule__GenericsInstantiation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GenericsInstantiation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__Group__1"


    // $ANTLR start "rule__GenericsInstantiation__Group__1__Impl"
    // InternalJniMap.g:2792:1: rule__GenericsInstantiation__Group__1__Impl : ( ( rule__GenericsInstantiation__InstanceNamesAssignment_1 ) ) ;
    public final void rule__GenericsInstantiation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2796:1: ( ( ( rule__GenericsInstantiation__InstanceNamesAssignment_1 ) ) )
            // InternalJniMap.g:2797:1: ( ( rule__GenericsInstantiation__InstanceNamesAssignment_1 ) )
            {
            // InternalJniMap.g:2797:1: ( ( rule__GenericsInstantiation__InstanceNamesAssignment_1 ) )
            // InternalJniMap.g:2798:1: ( rule__GenericsInstantiation__InstanceNamesAssignment_1 )
            {
             before(grammarAccess.getGenericsInstantiationAccess().getInstanceNamesAssignment_1()); 
            // InternalJniMap.g:2799:1: ( rule__GenericsInstantiation__InstanceNamesAssignment_1 )
            // InternalJniMap.g:2799:2: rule__GenericsInstantiation__InstanceNamesAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__GenericsInstantiation__InstanceNamesAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getGenericsInstantiationAccess().getInstanceNamesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__Group__1__Impl"


    // $ANTLR start "rule__GenericsInstantiation__Group__2"
    // InternalJniMap.g:2809:1: rule__GenericsInstantiation__Group__2 : rule__GenericsInstantiation__Group__2__Impl rule__GenericsInstantiation__Group__3 ;
    public final void rule__GenericsInstantiation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2813:1: ( rule__GenericsInstantiation__Group__2__Impl rule__GenericsInstantiation__Group__3 )
            // InternalJniMap.g:2814:2: rule__GenericsInstantiation__Group__2__Impl rule__GenericsInstantiation__Group__3
            {
            pushFollow(FOLLOW_22);
            rule__GenericsInstantiation__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GenericsInstantiation__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__Group__2"


    // $ANTLR start "rule__GenericsInstantiation__Group__2__Impl"
    // InternalJniMap.g:2821:1: rule__GenericsInstantiation__Group__2__Impl : ( ( rule__GenericsInstantiation__Group_2__0 )* ) ;
    public final void rule__GenericsInstantiation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2825:1: ( ( ( rule__GenericsInstantiation__Group_2__0 )* ) )
            // InternalJniMap.g:2826:1: ( ( rule__GenericsInstantiation__Group_2__0 )* )
            {
            // InternalJniMap.g:2826:1: ( ( rule__GenericsInstantiation__Group_2__0 )* )
            // InternalJniMap.g:2827:1: ( rule__GenericsInstantiation__Group_2__0 )*
            {
             before(grammarAccess.getGenericsInstantiationAccess().getGroup_2()); 
            // InternalJniMap.g:2828:1: ( rule__GenericsInstantiation__Group_2__0 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==33) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalJniMap.g:2828:2: rule__GenericsInstantiation__Group_2__0
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__GenericsInstantiation__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getGenericsInstantiationAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__Group__2__Impl"


    // $ANTLR start "rule__GenericsInstantiation__Group__3"
    // InternalJniMap.g:2838:1: rule__GenericsInstantiation__Group__3 : rule__GenericsInstantiation__Group__3__Impl ;
    public final void rule__GenericsInstantiation__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2842:1: ( rule__GenericsInstantiation__Group__3__Impl )
            // InternalJniMap.g:2843:2: rule__GenericsInstantiation__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GenericsInstantiation__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__Group__3"


    // $ANTLR start "rule__GenericsInstantiation__Group__3__Impl"
    // InternalJniMap.g:2849:1: rule__GenericsInstantiation__Group__3__Impl : ( '>' ) ;
    public final void rule__GenericsInstantiation__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2853:1: ( ( '>' ) )
            // InternalJniMap.g:2854:1: ( '>' )
            {
            // InternalJniMap.g:2854:1: ( '>' )
            // InternalJniMap.g:2855:1: '>'
            {
             before(grammarAccess.getGenericsInstantiationAccess().getGreaterThanSignKeyword_3()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getGenericsInstantiationAccess().getGreaterThanSignKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__Group__3__Impl"


    // $ANTLR start "rule__GenericsInstantiation__Group_2__0"
    // InternalJniMap.g:2876:1: rule__GenericsInstantiation__Group_2__0 : rule__GenericsInstantiation__Group_2__0__Impl rule__GenericsInstantiation__Group_2__1 ;
    public final void rule__GenericsInstantiation__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2880:1: ( rule__GenericsInstantiation__Group_2__0__Impl rule__GenericsInstantiation__Group_2__1 )
            // InternalJniMap.g:2881:2: rule__GenericsInstantiation__Group_2__0__Impl rule__GenericsInstantiation__Group_2__1
            {
            pushFollow(FOLLOW_5);
            rule__GenericsInstantiation__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GenericsInstantiation__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__Group_2__0"


    // $ANTLR start "rule__GenericsInstantiation__Group_2__0__Impl"
    // InternalJniMap.g:2888:1: rule__GenericsInstantiation__Group_2__0__Impl : ( ',' ) ;
    public final void rule__GenericsInstantiation__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2892:1: ( ( ',' ) )
            // InternalJniMap.g:2893:1: ( ',' )
            {
            // InternalJniMap.g:2893:1: ( ',' )
            // InternalJniMap.g:2894:1: ','
            {
             before(grammarAccess.getGenericsInstantiationAccess().getCommaKeyword_2_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getGenericsInstantiationAccess().getCommaKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__Group_2__0__Impl"


    // $ANTLR start "rule__GenericsInstantiation__Group_2__1"
    // InternalJniMap.g:2907:1: rule__GenericsInstantiation__Group_2__1 : rule__GenericsInstantiation__Group_2__1__Impl ;
    public final void rule__GenericsInstantiation__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2911:1: ( rule__GenericsInstantiation__Group_2__1__Impl )
            // InternalJniMap.g:2912:2: rule__GenericsInstantiation__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GenericsInstantiation__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__Group_2__1"


    // $ANTLR start "rule__GenericsInstantiation__Group_2__1__Impl"
    // InternalJniMap.g:2918:1: rule__GenericsInstantiation__Group_2__1__Impl : ( ( rule__GenericsInstantiation__InstanceNamesAssignment_2_1 ) ) ;
    public final void rule__GenericsInstantiation__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2922:1: ( ( ( rule__GenericsInstantiation__InstanceNamesAssignment_2_1 ) ) )
            // InternalJniMap.g:2923:1: ( ( rule__GenericsInstantiation__InstanceNamesAssignment_2_1 ) )
            {
            // InternalJniMap.g:2923:1: ( ( rule__GenericsInstantiation__InstanceNamesAssignment_2_1 ) )
            // InternalJniMap.g:2924:1: ( rule__GenericsInstantiation__InstanceNamesAssignment_2_1 )
            {
             before(grammarAccess.getGenericsInstantiationAccess().getInstanceNamesAssignment_2_1()); 
            // InternalJniMap.g:2925:1: ( rule__GenericsInstantiation__InstanceNamesAssignment_2_1 )
            // InternalJniMap.g:2925:2: rule__GenericsInstantiation__InstanceNamesAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__GenericsInstantiation__InstanceNamesAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getGenericsInstantiationAccess().getInstanceNamesAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__Group_2__1__Impl"


    // $ANTLR start "rule__InterfaceImplementation__Group__0"
    // InternalJniMap.g:2939:1: rule__InterfaceImplementation__Group__0 : rule__InterfaceImplementation__Group__0__Impl rule__InterfaceImplementation__Group__1 ;
    public final void rule__InterfaceImplementation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2943:1: ( rule__InterfaceImplementation__Group__0__Impl rule__InterfaceImplementation__Group__1 )
            // InternalJniMap.g:2944:2: rule__InterfaceImplementation__Group__0__Impl rule__InterfaceImplementation__Group__1
            {
            pushFollow(FOLLOW_23);
            rule__InterfaceImplementation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InterfaceImplementation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceImplementation__Group__0"


    // $ANTLR start "rule__InterfaceImplementation__Group__0__Impl"
    // InternalJniMap.g:2951:1: rule__InterfaceImplementation__Group__0__Impl : ( ( rule__InterfaceImplementation__InterfaceAssignment_0 ) ) ;
    public final void rule__InterfaceImplementation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2955:1: ( ( ( rule__InterfaceImplementation__InterfaceAssignment_0 ) ) )
            // InternalJniMap.g:2956:1: ( ( rule__InterfaceImplementation__InterfaceAssignment_0 ) )
            {
            // InternalJniMap.g:2956:1: ( ( rule__InterfaceImplementation__InterfaceAssignment_0 ) )
            // InternalJniMap.g:2957:1: ( rule__InterfaceImplementation__InterfaceAssignment_0 )
            {
             before(grammarAccess.getInterfaceImplementationAccess().getInterfaceAssignment_0()); 
            // InternalJniMap.g:2958:1: ( rule__InterfaceImplementation__InterfaceAssignment_0 )
            // InternalJniMap.g:2958:2: rule__InterfaceImplementation__InterfaceAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__InterfaceImplementation__InterfaceAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getInterfaceImplementationAccess().getInterfaceAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceImplementation__Group__0__Impl"


    // $ANTLR start "rule__InterfaceImplementation__Group__1"
    // InternalJniMap.g:2968:1: rule__InterfaceImplementation__Group__1 : rule__InterfaceImplementation__Group__1__Impl ;
    public final void rule__InterfaceImplementation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2972:1: ( rule__InterfaceImplementation__Group__1__Impl )
            // InternalJniMap.g:2973:2: rule__InterfaceImplementation__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InterfaceImplementation__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceImplementation__Group__1"


    // $ANTLR start "rule__InterfaceImplementation__Group__1__Impl"
    // InternalJniMap.g:2979:1: rule__InterfaceImplementation__Group__1__Impl : ( ( rule__InterfaceImplementation__GenericsInstantiationAssignment_1 )? ) ;
    public final void rule__InterfaceImplementation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:2983:1: ( ( ( rule__InterfaceImplementation__GenericsInstantiationAssignment_1 )? ) )
            // InternalJniMap.g:2984:1: ( ( rule__InterfaceImplementation__GenericsInstantiationAssignment_1 )? )
            {
            // InternalJniMap.g:2984:1: ( ( rule__InterfaceImplementation__GenericsInstantiationAssignment_1 )? )
            // InternalJniMap.g:2985:1: ( rule__InterfaceImplementation__GenericsInstantiationAssignment_1 )?
            {
             before(grammarAccess.getInterfaceImplementationAccess().getGenericsInstantiationAssignment_1()); 
            // InternalJniMap.g:2986:1: ( rule__InterfaceImplementation__GenericsInstantiationAssignment_1 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==35) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalJniMap.g:2986:2: rule__InterfaceImplementation__GenericsInstantiationAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__InterfaceImplementation__GenericsInstantiationAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getInterfaceImplementationAccess().getGenericsInstantiationAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceImplementation__Group__1__Impl"


    // $ANTLR start "rule__EnumToClassMapping__Group__0"
    // InternalJniMap.g:3000:1: rule__EnumToClassMapping__Group__0 : rule__EnumToClassMapping__Group__0__Impl rule__EnumToClassMapping__Group__1 ;
    public final void rule__EnumToClassMapping__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3004:1: ( rule__EnumToClassMapping__Group__0__Impl rule__EnumToClassMapping__Group__1 )
            // InternalJniMap.g:3005:2: rule__EnumToClassMapping__Group__0__Impl rule__EnumToClassMapping__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__EnumToClassMapping__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EnumToClassMapping__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumToClassMapping__Group__0"


    // $ANTLR start "rule__EnumToClassMapping__Group__0__Impl"
    // InternalJniMap.g:3012:1: rule__EnumToClassMapping__Group__0__Impl : ( ( rule__EnumToClassMapping__Enum_Assignment_0 ) ) ;
    public final void rule__EnumToClassMapping__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3016:1: ( ( ( rule__EnumToClassMapping__Enum_Assignment_0 ) ) )
            // InternalJniMap.g:3017:1: ( ( rule__EnumToClassMapping__Enum_Assignment_0 ) )
            {
            // InternalJniMap.g:3017:1: ( ( rule__EnumToClassMapping__Enum_Assignment_0 ) )
            // InternalJniMap.g:3018:1: ( rule__EnumToClassMapping__Enum_Assignment_0 )
            {
             before(grammarAccess.getEnumToClassMappingAccess().getEnum_Assignment_0()); 
            // InternalJniMap.g:3019:1: ( rule__EnumToClassMapping__Enum_Assignment_0 )
            // InternalJniMap.g:3019:2: rule__EnumToClassMapping__Enum_Assignment_0
            {
            pushFollow(FOLLOW_2);
            rule__EnumToClassMapping__Enum_Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getEnumToClassMappingAccess().getEnum_Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumToClassMapping__Group__0__Impl"


    // $ANTLR start "rule__EnumToClassMapping__Group__1"
    // InternalJniMap.g:3029:1: rule__EnumToClassMapping__Group__1 : rule__EnumToClassMapping__Group__1__Impl rule__EnumToClassMapping__Group__2 ;
    public final void rule__EnumToClassMapping__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3033:1: ( rule__EnumToClassMapping__Group__1__Impl rule__EnumToClassMapping__Group__2 )
            // InternalJniMap.g:3034:2: rule__EnumToClassMapping__Group__1__Impl rule__EnumToClassMapping__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__EnumToClassMapping__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EnumToClassMapping__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumToClassMapping__Group__1"


    // $ANTLR start "rule__EnumToClassMapping__Group__1__Impl"
    // InternalJniMap.g:3041:1: rule__EnumToClassMapping__Group__1__Impl : ( 'as' ) ;
    public final void rule__EnumToClassMapping__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3045:1: ( ( 'as' ) )
            // InternalJniMap.g:3046:1: ( 'as' )
            {
            // InternalJniMap.g:3046:1: ( 'as' )
            // InternalJniMap.g:3047:1: 'as'
            {
             before(grammarAccess.getEnumToClassMappingAccess().getAsKeyword_1()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getEnumToClassMappingAccess().getAsKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumToClassMapping__Group__1__Impl"


    // $ANTLR start "rule__EnumToClassMapping__Group__2"
    // InternalJniMap.g:3060:1: rule__EnumToClassMapping__Group__2 : rule__EnumToClassMapping__Group__2__Impl rule__EnumToClassMapping__Group__3 ;
    public final void rule__EnumToClassMapping__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3064:1: ( rule__EnumToClassMapping__Group__2__Impl rule__EnumToClassMapping__Group__3 )
            // InternalJniMap.g:3065:2: rule__EnumToClassMapping__Group__2__Impl rule__EnumToClassMapping__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__EnumToClassMapping__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EnumToClassMapping__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumToClassMapping__Group__2"


    // $ANTLR start "rule__EnumToClassMapping__Group__2__Impl"
    // InternalJniMap.g:3072:1: rule__EnumToClassMapping__Group__2__Impl : ( ( rule__EnumToClassMapping__NameAssignment_2 ) ) ;
    public final void rule__EnumToClassMapping__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3076:1: ( ( ( rule__EnumToClassMapping__NameAssignment_2 ) ) )
            // InternalJniMap.g:3077:1: ( ( rule__EnumToClassMapping__NameAssignment_2 ) )
            {
            // InternalJniMap.g:3077:1: ( ( rule__EnumToClassMapping__NameAssignment_2 ) )
            // InternalJniMap.g:3078:1: ( rule__EnumToClassMapping__NameAssignment_2 )
            {
             before(grammarAccess.getEnumToClassMappingAccess().getNameAssignment_2()); 
            // InternalJniMap.g:3079:1: ( rule__EnumToClassMapping__NameAssignment_2 )
            // InternalJniMap.g:3079:2: rule__EnumToClassMapping__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__EnumToClassMapping__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getEnumToClassMappingAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumToClassMapping__Group__2__Impl"


    // $ANTLR start "rule__EnumToClassMapping__Group__3"
    // InternalJniMap.g:3089:1: rule__EnumToClassMapping__Group__3 : rule__EnumToClassMapping__Group__3__Impl ;
    public final void rule__EnumToClassMapping__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3093:1: ( rule__EnumToClassMapping__Group__3__Impl )
            // InternalJniMap.g:3094:2: rule__EnumToClassMapping__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EnumToClassMapping__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumToClassMapping__Group__3"


    // $ANTLR start "rule__EnumToClassMapping__Group__3__Impl"
    // InternalJniMap.g:3100:1: rule__EnumToClassMapping__Group__3__Impl : ( ';' ) ;
    public final void rule__EnumToClassMapping__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3104:1: ( ( ';' ) )
            // InternalJniMap.g:3105:1: ( ';' )
            {
            // InternalJniMap.g:3105:1: ( ';' )
            // InternalJniMap.g:3106:1: ';'
            {
             before(grammarAccess.getEnumToClassMappingAccess().getSemicolonKeyword_3()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getEnumToClassMappingAccess().getSemicolonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumToClassMapping__Group__3__Impl"


    // $ANTLR start "rule__StructToClassMapping__Group__0"
    // InternalJniMap.g:3127:1: rule__StructToClassMapping__Group__0 : rule__StructToClassMapping__Group__0__Impl rule__StructToClassMapping__Group__1 ;
    public final void rule__StructToClassMapping__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3131:1: ( rule__StructToClassMapping__Group__0__Impl rule__StructToClassMapping__Group__1 )
            // InternalJniMap.g:3132:2: rule__StructToClassMapping__Group__0__Impl rule__StructToClassMapping__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__StructToClassMapping__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructToClassMapping__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group__0"


    // $ANTLR start "rule__StructToClassMapping__Group__0__Impl"
    // InternalJniMap.g:3139:1: rule__StructToClassMapping__Group__0__Impl : ( ( rule__StructToClassMapping__StructAssignment_0 ) ) ;
    public final void rule__StructToClassMapping__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3143:1: ( ( ( rule__StructToClassMapping__StructAssignment_0 ) ) )
            // InternalJniMap.g:3144:1: ( ( rule__StructToClassMapping__StructAssignment_0 ) )
            {
            // InternalJniMap.g:3144:1: ( ( rule__StructToClassMapping__StructAssignment_0 ) )
            // InternalJniMap.g:3145:1: ( rule__StructToClassMapping__StructAssignment_0 )
            {
             before(grammarAccess.getStructToClassMappingAccess().getStructAssignment_0()); 
            // InternalJniMap.g:3146:1: ( rule__StructToClassMapping__StructAssignment_0 )
            // InternalJniMap.g:3146:2: rule__StructToClassMapping__StructAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__StructToClassMapping__StructAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getStructToClassMappingAccess().getStructAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group__0__Impl"


    // $ANTLR start "rule__StructToClassMapping__Group__1"
    // InternalJniMap.g:3156:1: rule__StructToClassMapping__Group__1 : rule__StructToClassMapping__Group__1__Impl rule__StructToClassMapping__Group__2 ;
    public final void rule__StructToClassMapping__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3160:1: ( rule__StructToClassMapping__Group__1__Impl rule__StructToClassMapping__Group__2 )
            // InternalJniMap.g:3161:2: rule__StructToClassMapping__Group__1__Impl rule__StructToClassMapping__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__StructToClassMapping__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructToClassMapping__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group__1"


    // $ANTLR start "rule__StructToClassMapping__Group__1__Impl"
    // InternalJniMap.g:3168:1: rule__StructToClassMapping__Group__1__Impl : ( 'as' ) ;
    public final void rule__StructToClassMapping__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3172:1: ( ( 'as' ) )
            // InternalJniMap.g:3173:1: ( 'as' )
            {
            // InternalJniMap.g:3173:1: ( 'as' )
            // InternalJniMap.g:3174:1: 'as'
            {
             before(grammarAccess.getStructToClassMappingAccess().getAsKeyword_1()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getStructToClassMappingAccess().getAsKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group__1__Impl"


    // $ANTLR start "rule__StructToClassMapping__Group__2"
    // InternalJniMap.g:3187:1: rule__StructToClassMapping__Group__2 : rule__StructToClassMapping__Group__2__Impl rule__StructToClassMapping__Group__3 ;
    public final void rule__StructToClassMapping__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3191:1: ( rule__StructToClassMapping__Group__2__Impl rule__StructToClassMapping__Group__3 )
            // InternalJniMap.g:3192:2: rule__StructToClassMapping__Group__2__Impl rule__StructToClassMapping__Group__3
            {
            pushFollow(FOLLOW_25);
            rule__StructToClassMapping__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructToClassMapping__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group__2"


    // $ANTLR start "rule__StructToClassMapping__Group__2__Impl"
    // InternalJniMap.g:3199:1: rule__StructToClassMapping__Group__2__Impl : ( ( rule__StructToClassMapping__NameAssignment_2 ) ) ;
    public final void rule__StructToClassMapping__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3203:1: ( ( ( rule__StructToClassMapping__NameAssignment_2 ) ) )
            // InternalJniMap.g:3204:1: ( ( rule__StructToClassMapping__NameAssignment_2 ) )
            {
            // InternalJniMap.g:3204:1: ( ( rule__StructToClassMapping__NameAssignment_2 ) )
            // InternalJniMap.g:3205:1: ( rule__StructToClassMapping__NameAssignment_2 )
            {
             before(grammarAccess.getStructToClassMappingAccess().getNameAssignment_2()); 
            // InternalJniMap.g:3206:1: ( rule__StructToClassMapping__NameAssignment_2 )
            // InternalJniMap.g:3206:2: rule__StructToClassMapping__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__StructToClassMapping__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getStructToClassMappingAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group__2__Impl"


    // $ANTLR start "rule__StructToClassMapping__Group__3"
    // InternalJniMap.g:3216:1: rule__StructToClassMapping__Group__3 : rule__StructToClassMapping__Group__3__Impl rule__StructToClassMapping__Group__4 ;
    public final void rule__StructToClassMapping__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3220:1: ( rule__StructToClassMapping__Group__3__Impl rule__StructToClassMapping__Group__4 )
            // InternalJniMap.g:3221:2: rule__StructToClassMapping__Group__3__Impl rule__StructToClassMapping__Group__4
            {
            pushFollow(FOLLOW_25);
            rule__StructToClassMapping__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructToClassMapping__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group__3"


    // $ANTLR start "rule__StructToClassMapping__Group__3__Impl"
    // InternalJniMap.g:3228:1: rule__StructToClassMapping__Group__3__Impl : ( ( rule__StructToClassMapping__Group_3__0 )? ) ;
    public final void rule__StructToClassMapping__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3232:1: ( ( ( rule__StructToClassMapping__Group_3__0 )? ) )
            // InternalJniMap.g:3233:1: ( ( rule__StructToClassMapping__Group_3__0 )? )
            {
            // InternalJniMap.g:3233:1: ( ( rule__StructToClassMapping__Group_3__0 )? )
            // InternalJniMap.g:3234:1: ( rule__StructToClassMapping__Group_3__0 )?
            {
             before(grammarAccess.getStructToClassMappingAccess().getGroup_3()); 
            // InternalJniMap.g:3235:1: ( rule__StructToClassMapping__Group_3__0 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==38) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalJniMap.g:3235:2: rule__StructToClassMapping__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__StructToClassMapping__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStructToClassMappingAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group__3__Impl"


    // $ANTLR start "rule__StructToClassMapping__Group__4"
    // InternalJniMap.g:3245:1: rule__StructToClassMapping__Group__4 : rule__StructToClassMapping__Group__4__Impl ;
    public final void rule__StructToClassMapping__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3249:1: ( rule__StructToClassMapping__Group__4__Impl )
            // InternalJniMap.g:3250:2: rule__StructToClassMapping__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StructToClassMapping__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group__4"


    // $ANTLR start "rule__StructToClassMapping__Group__4__Impl"
    // InternalJniMap.g:3256:1: rule__StructToClassMapping__Group__4__Impl : ( ';' ) ;
    public final void rule__StructToClassMapping__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3260:1: ( ( ';' ) )
            // InternalJniMap.g:3261:1: ( ';' )
            {
            // InternalJniMap.g:3261:1: ( ';' )
            // InternalJniMap.g:3262:1: ';'
            {
             before(grammarAccess.getStructToClassMappingAccess().getSemicolonKeyword_4()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getStructToClassMappingAccess().getSemicolonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group__4__Impl"


    // $ANTLR start "rule__StructToClassMapping__Group_3__0"
    // InternalJniMap.g:3285:1: rule__StructToClassMapping__Group_3__0 : rule__StructToClassMapping__Group_3__0__Impl rule__StructToClassMapping__Group_3__1 ;
    public final void rule__StructToClassMapping__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3289:1: ( rule__StructToClassMapping__Group_3__0__Impl rule__StructToClassMapping__Group_3__1 )
            // InternalJniMap.g:3290:2: rule__StructToClassMapping__Group_3__0__Impl rule__StructToClassMapping__Group_3__1
            {
            pushFollow(FOLLOW_5);
            rule__StructToClassMapping__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructToClassMapping__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group_3__0"


    // $ANTLR start "rule__StructToClassMapping__Group_3__0__Impl"
    // InternalJniMap.g:3297:1: rule__StructToClassMapping__Group_3__0__Impl : ( 'extending' ) ;
    public final void rule__StructToClassMapping__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3301:1: ( ( 'extending' ) )
            // InternalJniMap.g:3302:1: ( 'extending' )
            {
            // InternalJniMap.g:3302:1: ( 'extending' )
            // InternalJniMap.g:3303:1: 'extending'
            {
             before(grammarAccess.getStructToClassMappingAccess().getExtendingKeyword_3_0()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getStructToClassMappingAccess().getExtendingKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group_3__0__Impl"


    // $ANTLR start "rule__StructToClassMapping__Group_3__1"
    // InternalJniMap.g:3316:1: rule__StructToClassMapping__Group_3__1 : rule__StructToClassMapping__Group_3__1__Impl ;
    public final void rule__StructToClassMapping__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3320:1: ( rule__StructToClassMapping__Group_3__1__Impl )
            // InternalJniMap.g:3321:2: rule__StructToClassMapping__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StructToClassMapping__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group_3__1"


    // $ANTLR start "rule__StructToClassMapping__Group_3__1__Impl"
    // InternalJniMap.g:3327:1: rule__StructToClassMapping__Group_3__1__Impl : ( ( rule__StructToClassMapping__SuperAssignment_3_1 ) ) ;
    public final void rule__StructToClassMapping__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3331:1: ( ( ( rule__StructToClassMapping__SuperAssignment_3_1 ) ) )
            // InternalJniMap.g:3332:1: ( ( rule__StructToClassMapping__SuperAssignment_3_1 ) )
            {
            // InternalJniMap.g:3332:1: ( ( rule__StructToClassMapping__SuperAssignment_3_1 ) )
            // InternalJniMap.g:3333:1: ( rule__StructToClassMapping__SuperAssignment_3_1 )
            {
             before(grammarAccess.getStructToClassMappingAccess().getSuperAssignment_3_1()); 
            // InternalJniMap.g:3334:1: ( rule__StructToClassMapping__SuperAssignment_3_1 )
            // InternalJniMap.g:3334:2: rule__StructToClassMapping__SuperAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__StructToClassMapping__SuperAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getStructToClassMappingAccess().getSuperAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__Group_3__1__Impl"


    // $ANTLR start "rule__ClassToClassMapping__Group__0"
    // InternalJniMap.g:3348:1: rule__ClassToClassMapping__Group__0 : rule__ClassToClassMapping__Group__0__Impl rule__ClassToClassMapping__Group__1 ;
    public final void rule__ClassToClassMapping__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3352:1: ( rule__ClassToClassMapping__Group__0__Impl rule__ClassToClassMapping__Group__1 )
            // InternalJniMap.g:3353:2: rule__ClassToClassMapping__Group__0__Impl rule__ClassToClassMapping__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__ClassToClassMapping__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassToClassMapping__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group__0"


    // $ANTLR start "rule__ClassToClassMapping__Group__0__Impl"
    // InternalJniMap.g:3360:1: rule__ClassToClassMapping__Group__0__Impl : ( ( rule__ClassToClassMapping__ClassAssignment_0 ) ) ;
    public final void rule__ClassToClassMapping__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3364:1: ( ( ( rule__ClassToClassMapping__ClassAssignment_0 ) ) )
            // InternalJniMap.g:3365:1: ( ( rule__ClassToClassMapping__ClassAssignment_0 ) )
            {
            // InternalJniMap.g:3365:1: ( ( rule__ClassToClassMapping__ClassAssignment_0 ) )
            // InternalJniMap.g:3366:1: ( rule__ClassToClassMapping__ClassAssignment_0 )
            {
             before(grammarAccess.getClassToClassMappingAccess().getClassAssignment_0()); 
            // InternalJniMap.g:3367:1: ( rule__ClassToClassMapping__ClassAssignment_0 )
            // InternalJniMap.g:3367:2: rule__ClassToClassMapping__ClassAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ClassToClassMapping__ClassAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getClassToClassMappingAccess().getClassAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group__0__Impl"


    // $ANTLR start "rule__ClassToClassMapping__Group__1"
    // InternalJniMap.g:3377:1: rule__ClassToClassMapping__Group__1 : rule__ClassToClassMapping__Group__1__Impl rule__ClassToClassMapping__Group__2 ;
    public final void rule__ClassToClassMapping__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3381:1: ( rule__ClassToClassMapping__Group__1__Impl rule__ClassToClassMapping__Group__2 )
            // InternalJniMap.g:3382:2: rule__ClassToClassMapping__Group__1__Impl rule__ClassToClassMapping__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__ClassToClassMapping__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassToClassMapping__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group__1"


    // $ANTLR start "rule__ClassToClassMapping__Group__1__Impl"
    // InternalJniMap.g:3389:1: rule__ClassToClassMapping__Group__1__Impl : ( 'as' ) ;
    public final void rule__ClassToClassMapping__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3393:1: ( ( 'as' ) )
            // InternalJniMap.g:3394:1: ( 'as' )
            {
            // InternalJniMap.g:3394:1: ( 'as' )
            // InternalJniMap.g:3395:1: 'as'
            {
             before(grammarAccess.getClassToClassMappingAccess().getAsKeyword_1()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getClassToClassMappingAccess().getAsKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group__1__Impl"


    // $ANTLR start "rule__ClassToClassMapping__Group__2"
    // InternalJniMap.g:3408:1: rule__ClassToClassMapping__Group__2 : rule__ClassToClassMapping__Group__2__Impl rule__ClassToClassMapping__Group__3 ;
    public final void rule__ClassToClassMapping__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3412:1: ( rule__ClassToClassMapping__Group__2__Impl rule__ClassToClassMapping__Group__3 )
            // InternalJniMap.g:3413:2: rule__ClassToClassMapping__Group__2__Impl rule__ClassToClassMapping__Group__3
            {
            pushFollow(FOLLOW_25);
            rule__ClassToClassMapping__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassToClassMapping__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group__2"


    // $ANTLR start "rule__ClassToClassMapping__Group__2__Impl"
    // InternalJniMap.g:3420:1: rule__ClassToClassMapping__Group__2__Impl : ( ( rule__ClassToClassMapping__NameAssignment_2 ) ) ;
    public final void rule__ClassToClassMapping__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3424:1: ( ( ( rule__ClassToClassMapping__NameAssignment_2 ) ) )
            // InternalJniMap.g:3425:1: ( ( rule__ClassToClassMapping__NameAssignment_2 ) )
            {
            // InternalJniMap.g:3425:1: ( ( rule__ClassToClassMapping__NameAssignment_2 ) )
            // InternalJniMap.g:3426:1: ( rule__ClassToClassMapping__NameAssignment_2 )
            {
             before(grammarAccess.getClassToClassMappingAccess().getNameAssignment_2()); 
            // InternalJniMap.g:3427:1: ( rule__ClassToClassMapping__NameAssignment_2 )
            // InternalJniMap.g:3427:2: rule__ClassToClassMapping__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ClassToClassMapping__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getClassToClassMappingAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group__2__Impl"


    // $ANTLR start "rule__ClassToClassMapping__Group__3"
    // InternalJniMap.g:3437:1: rule__ClassToClassMapping__Group__3 : rule__ClassToClassMapping__Group__3__Impl rule__ClassToClassMapping__Group__4 ;
    public final void rule__ClassToClassMapping__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3441:1: ( rule__ClassToClassMapping__Group__3__Impl rule__ClassToClassMapping__Group__4 )
            // InternalJniMap.g:3442:2: rule__ClassToClassMapping__Group__3__Impl rule__ClassToClassMapping__Group__4
            {
            pushFollow(FOLLOW_25);
            rule__ClassToClassMapping__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassToClassMapping__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group__3"


    // $ANTLR start "rule__ClassToClassMapping__Group__3__Impl"
    // InternalJniMap.g:3449:1: rule__ClassToClassMapping__Group__3__Impl : ( ( rule__ClassToClassMapping__Group_3__0 )? ) ;
    public final void rule__ClassToClassMapping__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3453:1: ( ( ( rule__ClassToClassMapping__Group_3__0 )? ) )
            // InternalJniMap.g:3454:1: ( ( rule__ClassToClassMapping__Group_3__0 )? )
            {
            // InternalJniMap.g:3454:1: ( ( rule__ClassToClassMapping__Group_3__0 )? )
            // InternalJniMap.g:3455:1: ( rule__ClassToClassMapping__Group_3__0 )?
            {
             before(grammarAccess.getClassToClassMappingAccess().getGroup_3()); 
            // InternalJniMap.g:3456:1: ( rule__ClassToClassMapping__Group_3__0 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==38) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalJniMap.g:3456:2: rule__ClassToClassMapping__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ClassToClassMapping__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getClassToClassMappingAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group__3__Impl"


    // $ANTLR start "rule__ClassToClassMapping__Group__4"
    // InternalJniMap.g:3466:1: rule__ClassToClassMapping__Group__4 : rule__ClassToClassMapping__Group__4__Impl ;
    public final void rule__ClassToClassMapping__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3470:1: ( rule__ClassToClassMapping__Group__4__Impl )
            // InternalJniMap.g:3471:2: rule__ClassToClassMapping__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ClassToClassMapping__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group__4"


    // $ANTLR start "rule__ClassToClassMapping__Group__4__Impl"
    // InternalJniMap.g:3477:1: rule__ClassToClassMapping__Group__4__Impl : ( ';' ) ;
    public final void rule__ClassToClassMapping__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3481:1: ( ( ';' ) )
            // InternalJniMap.g:3482:1: ( ';' )
            {
            // InternalJniMap.g:3482:1: ( ';' )
            // InternalJniMap.g:3483:1: ';'
            {
             before(grammarAccess.getClassToClassMappingAccess().getSemicolonKeyword_4()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getClassToClassMappingAccess().getSemicolonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group__4__Impl"


    // $ANTLR start "rule__ClassToClassMapping__Group_3__0"
    // InternalJniMap.g:3506:1: rule__ClassToClassMapping__Group_3__0 : rule__ClassToClassMapping__Group_3__0__Impl rule__ClassToClassMapping__Group_3__1 ;
    public final void rule__ClassToClassMapping__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3510:1: ( rule__ClassToClassMapping__Group_3__0__Impl rule__ClassToClassMapping__Group_3__1 )
            // InternalJniMap.g:3511:2: rule__ClassToClassMapping__Group_3__0__Impl rule__ClassToClassMapping__Group_3__1
            {
            pushFollow(FOLLOW_5);
            rule__ClassToClassMapping__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassToClassMapping__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group_3__0"


    // $ANTLR start "rule__ClassToClassMapping__Group_3__0__Impl"
    // InternalJniMap.g:3518:1: rule__ClassToClassMapping__Group_3__0__Impl : ( 'extending' ) ;
    public final void rule__ClassToClassMapping__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3522:1: ( ( 'extending' ) )
            // InternalJniMap.g:3523:1: ( 'extending' )
            {
            // InternalJniMap.g:3523:1: ( 'extending' )
            // InternalJniMap.g:3524:1: 'extending'
            {
             before(grammarAccess.getClassToClassMappingAccess().getExtendingKeyword_3_0()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getClassToClassMappingAccess().getExtendingKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group_3__0__Impl"


    // $ANTLR start "rule__ClassToClassMapping__Group_3__1"
    // InternalJniMap.g:3537:1: rule__ClassToClassMapping__Group_3__1 : rule__ClassToClassMapping__Group_3__1__Impl ;
    public final void rule__ClassToClassMapping__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3541:1: ( rule__ClassToClassMapping__Group_3__1__Impl )
            // InternalJniMap.g:3542:2: rule__ClassToClassMapping__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ClassToClassMapping__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group_3__1"


    // $ANTLR start "rule__ClassToClassMapping__Group_3__1__Impl"
    // InternalJniMap.g:3548:1: rule__ClassToClassMapping__Group_3__1__Impl : ( ( rule__ClassToClassMapping__SuperAssignment_3_1 ) ) ;
    public final void rule__ClassToClassMapping__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3552:1: ( ( ( rule__ClassToClassMapping__SuperAssignment_3_1 ) ) )
            // InternalJniMap.g:3553:1: ( ( rule__ClassToClassMapping__SuperAssignment_3_1 ) )
            {
            // InternalJniMap.g:3553:1: ( ( rule__ClassToClassMapping__SuperAssignment_3_1 ) )
            // InternalJniMap.g:3554:1: ( rule__ClassToClassMapping__SuperAssignment_3_1 )
            {
             before(grammarAccess.getClassToClassMappingAccess().getSuperAssignment_3_1()); 
            // InternalJniMap.g:3555:1: ( rule__ClassToClassMapping__SuperAssignment_3_1 )
            // InternalJniMap.g:3555:2: rule__ClassToClassMapping__SuperAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__ClassToClassMapping__SuperAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getClassToClassMappingAccess().getSuperAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__Group_3__1__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group__0"
    // InternalJniMap.g:3569:1: rule__AliasToClassMapping__Group__0 : rule__AliasToClassMapping__Group__0__Impl rule__AliasToClassMapping__Group__1 ;
    public final void rule__AliasToClassMapping__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3573:1: ( rule__AliasToClassMapping__Group__0__Impl rule__AliasToClassMapping__Group__1 )
            // InternalJniMap.g:3574:2: rule__AliasToClassMapping__Group__0__Impl rule__AliasToClassMapping__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__AliasToClassMapping__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__0"


    // $ANTLR start "rule__AliasToClassMapping__Group__0__Impl"
    // InternalJniMap.g:3581:1: rule__AliasToClassMapping__Group__0__Impl : ( ( rule__AliasToClassMapping__AliasAssignment_0 ) ) ;
    public final void rule__AliasToClassMapping__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3585:1: ( ( ( rule__AliasToClassMapping__AliasAssignment_0 ) ) )
            // InternalJniMap.g:3586:1: ( ( rule__AliasToClassMapping__AliasAssignment_0 ) )
            {
            // InternalJniMap.g:3586:1: ( ( rule__AliasToClassMapping__AliasAssignment_0 ) )
            // InternalJniMap.g:3587:1: ( rule__AliasToClassMapping__AliasAssignment_0 )
            {
             before(grammarAccess.getAliasToClassMappingAccess().getAliasAssignment_0()); 
            // InternalJniMap.g:3588:1: ( rule__AliasToClassMapping__AliasAssignment_0 )
            // InternalJniMap.g:3588:2: rule__AliasToClassMapping__AliasAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__AliasAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAliasToClassMappingAccess().getAliasAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__0__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group__1"
    // InternalJniMap.g:3598:1: rule__AliasToClassMapping__Group__1 : rule__AliasToClassMapping__Group__1__Impl rule__AliasToClassMapping__Group__2 ;
    public final void rule__AliasToClassMapping__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3602:1: ( rule__AliasToClassMapping__Group__1__Impl rule__AliasToClassMapping__Group__2 )
            // InternalJniMap.g:3603:2: rule__AliasToClassMapping__Group__1__Impl rule__AliasToClassMapping__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__AliasToClassMapping__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__1"


    // $ANTLR start "rule__AliasToClassMapping__Group__1__Impl"
    // InternalJniMap.g:3610:1: rule__AliasToClassMapping__Group__1__Impl : ( 'as' ) ;
    public final void rule__AliasToClassMapping__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3614:1: ( ( 'as' ) )
            // InternalJniMap.g:3615:1: ( 'as' )
            {
            // InternalJniMap.g:3615:1: ( 'as' )
            // InternalJniMap.g:3616:1: 'as'
            {
             before(grammarAccess.getAliasToClassMappingAccess().getAsKeyword_1()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getAliasToClassMappingAccess().getAsKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__1__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group__2"
    // InternalJniMap.g:3629:1: rule__AliasToClassMapping__Group__2 : rule__AliasToClassMapping__Group__2__Impl rule__AliasToClassMapping__Group__3 ;
    public final void rule__AliasToClassMapping__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3633:1: ( rule__AliasToClassMapping__Group__2__Impl rule__AliasToClassMapping__Group__3 )
            // InternalJniMap.g:3634:2: rule__AliasToClassMapping__Group__2__Impl rule__AliasToClassMapping__Group__3
            {
            pushFollow(FOLLOW_26);
            rule__AliasToClassMapping__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__2"


    // $ANTLR start "rule__AliasToClassMapping__Group__2__Impl"
    // InternalJniMap.g:3641:1: rule__AliasToClassMapping__Group__2__Impl : ( ( rule__AliasToClassMapping__NameAssignment_2 ) ) ;
    public final void rule__AliasToClassMapping__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3645:1: ( ( ( rule__AliasToClassMapping__NameAssignment_2 ) ) )
            // InternalJniMap.g:3646:1: ( ( rule__AliasToClassMapping__NameAssignment_2 ) )
            {
            // InternalJniMap.g:3646:1: ( ( rule__AliasToClassMapping__NameAssignment_2 ) )
            // InternalJniMap.g:3647:1: ( rule__AliasToClassMapping__NameAssignment_2 )
            {
             before(grammarAccess.getAliasToClassMappingAccess().getNameAssignment_2()); 
            // InternalJniMap.g:3648:1: ( rule__AliasToClassMapping__NameAssignment_2 )
            // InternalJniMap.g:3648:2: rule__AliasToClassMapping__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAliasToClassMappingAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__2__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group__3"
    // InternalJniMap.g:3658:1: rule__AliasToClassMapping__Group__3 : rule__AliasToClassMapping__Group__3__Impl rule__AliasToClassMapping__Group__4 ;
    public final void rule__AliasToClassMapping__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3662:1: ( rule__AliasToClassMapping__Group__3__Impl rule__AliasToClassMapping__Group__4 )
            // InternalJniMap.g:3663:2: rule__AliasToClassMapping__Group__3__Impl rule__AliasToClassMapping__Group__4
            {
            pushFollow(FOLLOW_26);
            rule__AliasToClassMapping__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__3"


    // $ANTLR start "rule__AliasToClassMapping__Group__3__Impl"
    // InternalJniMap.g:3670:1: rule__AliasToClassMapping__Group__3__Impl : ( ( rule__AliasToClassMapping__GenericsDeclAssignment_3 )? ) ;
    public final void rule__AliasToClassMapping__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3674:1: ( ( ( rule__AliasToClassMapping__GenericsDeclAssignment_3 )? ) )
            // InternalJniMap.g:3675:1: ( ( rule__AliasToClassMapping__GenericsDeclAssignment_3 )? )
            {
            // InternalJniMap.g:3675:1: ( ( rule__AliasToClassMapping__GenericsDeclAssignment_3 )? )
            // InternalJniMap.g:3676:1: ( rule__AliasToClassMapping__GenericsDeclAssignment_3 )?
            {
             before(grammarAccess.getAliasToClassMappingAccess().getGenericsDeclAssignment_3()); 
            // InternalJniMap.g:3677:1: ( rule__AliasToClassMapping__GenericsDeclAssignment_3 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==35) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalJniMap.g:3677:2: rule__AliasToClassMapping__GenericsDeclAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__AliasToClassMapping__GenericsDeclAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAliasToClassMappingAccess().getGenericsDeclAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__3__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group__4"
    // InternalJniMap.g:3687:1: rule__AliasToClassMapping__Group__4 : rule__AliasToClassMapping__Group__4__Impl rule__AliasToClassMapping__Group__5 ;
    public final void rule__AliasToClassMapping__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3691:1: ( rule__AliasToClassMapping__Group__4__Impl rule__AliasToClassMapping__Group__5 )
            // InternalJniMap.g:3692:2: rule__AliasToClassMapping__Group__4__Impl rule__AliasToClassMapping__Group__5
            {
            pushFollow(FOLLOW_26);
            rule__AliasToClassMapping__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__4"


    // $ANTLR start "rule__AliasToClassMapping__Group__4__Impl"
    // InternalJniMap.g:3699:1: rule__AliasToClassMapping__Group__4__Impl : ( ( rule__AliasToClassMapping__Group_4__0 )? ) ;
    public final void rule__AliasToClassMapping__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3703:1: ( ( ( rule__AliasToClassMapping__Group_4__0 )? ) )
            // InternalJniMap.g:3704:1: ( ( rule__AliasToClassMapping__Group_4__0 )? )
            {
            // InternalJniMap.g:3704:1: ( ( rule__AliasToClassMapping__Group_4__0 )? )
            // InternalJniMap.g:3705:1: ( rule__AliasToClassMapping__Group_4__0 )?
            {
             before(grammarAccess.getAliasToClassMappingAccess().getGroup_4()); 
            // InternalJniMap.g:3706:1: ( rule__AliasToClassMapping__Group_4__0 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==38) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalJniMap.g:3706:2: rule__AliasToClassMapping__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AliasToClassMapping__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAliasToClassMappingAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__4__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group__5"
    // InternalJniMap.g:3716:1: rule__AliasToClassMapping__Group__5 : rule__AliasToClassMapping__Group__5__Impl rule__AliasToClassMapping__Group__6 ;
    public final void rule__AliasToClassMapping__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3720:1: ( rule__AliasToClassMapping__Group__5__Impl rule__AliasToClassMapping__Group__6 )
            // InternalJniMap.g:3721:2: rule__AliasToClassMapping__Group__5__Impl rule__AliasToClassMapping__Group__6
            {
            pushFollow(FOLLOW_26);
            rule__AliasToClassMapping__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__5"


    // $ANTLR start "rule__AliasToClassMapping__Group__5__Impl"
    // InternalJniMap.g:3728:1: rule__AliasToClassMapping__Group__5__Impl : ( ( rule__AliasToClassMapping__Group_5__0 )? ) ;
    public final void rule__AliasToClassMapping__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3732:1: ( ( ( rule__AliasToClassMapping__Group_5__0 )? ) )
            // InternalJniMap.g:3733:1: ( ( rule__AliasToClassMapping__Group_5__0 )? )
            {
            // InternalJniMap.g:3733:1: ( ( rule__AliasToClassMapping__Group_5__0 )? )
            // InternalJniMap.g:3734:1: ( rule__AliasToClassMapping__Group_5__0 )?
            {
             before(grammarAccess.getAliasToClassMappingAccess().getGroup_5()); 
            // InternalJniMap.g:3735:1: ( rule__AliasToClassMapping__Group_5__0 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==39) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalJniMap.g:3735:2: rule__AliasToClassMapping__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AliasToClassMapping__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAliasToClassMappingAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__5__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group__6"
    // InternalJniMap.g:3745:1: rule__AliasToClassMapping__Group__6 : rule__AliasToClassMapping__Group__6__Impl ;
    public final void rule__AliasToClassMapping__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3749:1: ( rule__AliasToClassMapping__Group__6__Impl )
            // InternalJniMap.g:3750:2: rule__AliasToClassMapping__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__6"


    // $ANTLR start "rule__AliasToClassMapping__Group__6__Impl"
    // InternalJniMap.g:3756:1: rule__AliasToClassMapping__Group__6__Impl : ( ';' ) ;
    public final void rule__AliasToClassMapping__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3760:1: ( ( ';' ) )
            // InternalJniMap.g:3761:1: ( ';' )
            {
            // InternalJniMap.g:3761:1: ( ';' )
            // InternalJniMap.g:3762:1: ';'
            {
             before(grammarAccess.getAliasToClassMappingAccess().getSemicolonKeyword_6()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getAliasToClassMappingAccess().getSemicolonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group__6__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group_4__0"
    // InternalJniMap.g:3789:1: rule__AliasToClassMapping__Group_4__0 : rule__AliasToClassMapping__Group_4__0__Impl rule__AliasToClassMapping__Group_4__1 ;
    public final void rule__AliasToClassMapping__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3793:1: ( rule__AliasToClassMapping__Group_4__0__Impl rule__AliasToClassMapping__Group_4__1 )
            // InternalJniMap.g:3794:2: rule__AliasToClassMapping__Group_4__0__Impl rule__AliasToClassMapping__Group_4__1
            {
            pushFollow(FOLLOW_5);
            rule__AliasToClassMapping__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_4__0"


    // $ANTLR start "rule__AliasToClassMapping__Group_4__0__Impl"
    // InternalJniMap.g:3801:1: rule__AliasToClassMapping__Group_4__0__Impl : ( 'extending' ) ;
    public final void rule__AliasToClassMapping__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3805:1: ( ( 'extending' ) )
            // InternalJniMap.g:3806:1: ( 'extending' )
            {
            // InternalJniMap.g:3806:1: ( 'extending' )
            // InternalJniMap.g:3807:1: 'extending'
            {
             before(grammarAccess.getAliasToClassMappingAccess().getExtendingKeyword_4_0()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getAliasToClassMappingAccess().getExtendingKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_4__0__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group_4__1"
    // InternalJniMap.g:3820:1: rule__AliasToClassMapping__Group_4__1 : rule__AliasToClassMapping__Group_4__1__Impl rule__AliasToClassMapping__Group_4__2 ;
    public final void rule__AliasToClassMapping__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3824:1: ( rule__AliasToClassMapping__Group_4__1__Impl rule__AliasToClassMapping__Group_4__2 )
            // InternalJniMap.g:3825:2: rule__AliasToClassMapping__Group_4__1__Impl rule__AliasToClassMapping__Group_4__2
            {
            pushFollow(FOLLOW_23);
            rule__AliasToClassMapping__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_4__1"


    // $ANTLR start "rule__AliasToClassMapping__Group_4__1__Impl"
    // InternalJniMap.g:3832:1: rule__AliasToClassMapping__Group_4__1__Impl : ( ( rule__AliasToClassMapping__SuperAssignment_4_1 ) ) ;
    public final void rule__AliasToClassMapping__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3836:1: ( ( ( rule__AliasToClassMapping__SuperAssignment_4_1 ) ) )
            // InternalJniMap.g:3837:1: ( ( rule__AliasToClassMapping__SuperAssignment_4_1 ) )
            {
            // InternalJniMap.g:3837:1: ( ( rule__AliasToClassMapping__SuperAssignment_4_1 ) )
            // InternalJniMap.g:3838:1: ( rule__AliasToClassMapping__SuperAssignment_4_1 )
            {
             before(grammarAccess.getAliasToClassMappingAccess().getSuperAssignment_4_1()); 
            // InternalJniMap.g:3839:1: ( rule__AliasToClassMapping__SuperAssignment_4_1 )
            // InternalJniMap.g:3839:2: rule__AliasToClassMapping__SuperAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__SuperAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getAliasToClassMappingAccess().getSuperAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_4__1__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group_4__2"
    // InternalJniMap.g:3849:1: rule__AliasToClassMapping__Group_4__2 : rule__AliasToClassMapping__Group_4__2__Impl ;
    public final void rule__AliasToClassMapping__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3853:1: ( rule__AliasToClassMapping__Group_4__2__Impl )
            // InternalJniMap.g:3854:2: rule__AliasToClassMapping__Group_4__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_4__2"


    // $ANTLR start "rule__AliasToClassMapping__Group_4__2__Impl"
    // InternalJniMap.g:3860:1: rule__AliasToClassMapping__Group_4__2__Impl : ( ( rule__AliasToClassMapping__GenericsInstantiationAssignment_4_2 )? ) ;
    public final void rule__AliasToClassMapping__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3864:1: ( ( ( rule__AliasToClassMapping__GenericsInstantiationAssignment_4_2 )? ) )
            // InternalJniMap.g:3865:1: ( ( rule__AliasToClassMapping__GenericsInstantiationAssignment_4_2 )? )
            {
            // InternalJniMap.g:3865:1: ( ( rule__AliasToClassMapping__GenericsInstantiationAssignment_4_2 )? )
            // InternalJniMap.g:3866:1: ( rule__AliasToClassMapping__GenericsInstantiationAssignment_4_2 )?
            {
             before(grammarAccess.getAliasToClassMappingAccess().getGenericsInstantiationAssignment_4_2()); 
            // InternalJniMap.g:3867:1: ( rule__AliasToClassMapping__GenericsInstantiationAssignment_4_2 )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==35) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalJniMap.g:3867:2: rule__AliasToClassMapping__GenericsInstantiationAssignment_4_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__AliasToClassMapping__GenericsInstantiationAssignment_4_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAliasToClassMappingAccess().getGenericsInstantiationAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_4__2__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group_5__0"
    // InternalJniMap.g:3883:1: rule__AliasToClassMapping__Group_5__0 : rule__AliasToClassMapping__Group_5__0__Impl rule__AliasToClassMapping__Group_5__1 ;
    public final void rule__AliasToClassMapping__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3887:1: ( rule__AliasToClassMapping__Group_5__0__Impl rule__AliasToClassMapping__Group_5__1 )
            // InternalJniMap.g:3888:2: rule__AliasToClassMapping__Group_5__0__Impl rule__AliasToClassMapping__Group_5__1
            {
            pushFollow(FOLLOW_5);
            rule__AliasToClassMapping__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_5__0"


    // $ANTLR start "rule__AliasToClassMapping__Group_5__0__Impl"
    // InternalJniMap.g:3895:1: rule__AliasToClassMapping__Group_5__0__Impl : ( 'implements' ) ;
    public final void rule__AliasToClassMapping__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3899:1: ( ( 'implements' ) )
            // InternalJniMap.g:3900:1: ( 'implements' )
            {
            // InternalJniMap.g:3900:1: ( 'implements' )
            // InternalJniMap.g:3901:1: 'implements'
            {
             before(grammarAccess.getAliasToClassMappingAccess().getImplementsKeyword_5_0()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getAliasToClassMappingAccess().getImplementsKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_5__0__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group_5__1"
    // InternalJniMap.g:3914:1: rule__AliasToClassMapping__Group_5__1 : rule__AliasToClassMapping__Group_5__1__Impl rule__AliasToClassMapping__Group_5__2 ;
    public final void rule__AliasToClassMapping__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3918:1: ( rule__AliasToClassMapping__Group_5__1__Impl rule__AliasToClassMapping__Group_5__2 )
            // InternalJniMap.g:3919:2: rule__AliasToClassMapping__Group_5__1__Impl rule__AliasToClassMapping__Group_5__2
            {
            pushFollow(FOLLOW_27);
            rule__AliasToClassMapping__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_5__1"


    // $ANTLR start "rule__AliasToClassMapping__Group_5__1__Impl"
    // InternalJniMap.g:3926:1: rule__AliasToClassMapping__Group_5__1__Impl : ( ( rule__AliasToClassMapping__InterfacesAssignment_5_1 ) ) ;
    public final void rule__AliasToClassMapping__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3930:1: ( ( ( rule__AliasToClassMapping__InterfacesAssignment_5_1 ) ) )
            // InternalJniMap.g:3931:1: ( ( rule__AliasToClassMapping__InterfacesAssignment_5_1 ) )
            {
            // InternalJniMap.g:3931:1: ( ( rule__AliasToClassMapping__InterfacesAssignment_5_1 ) )
            // InternalJniMap.g:3932:1: ( rule__AliasToClassMapping__InterfacesAssignment_5_1 )
            {
             before(grammarAccess.getAliasToClassMappingAccess().getInterfacesAssignment_5_1()); 
            // InternalJniMap.g:3933:1: ( rule__AliasToClassMapping__InterfacesAssignment_5_1 )
            // InternalJniMap.g:3933:2: rule__AliasToClassMapping__InterfacesAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__InterfacesAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getAliasToClassMappingAccess().getInterfacesAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_5__1__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group_5__2"
    // InternalJniMap.g:3943:1: rule__AliasToClassMapping__Group_5__2 : rule__AliasToClassMapping__Group_5__2__Impl ;
    public final void rule__AliasToClassMapping__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3947:1: ( rule__AliasToClassMapping__Group_5__2__Impl )
            // InternalJniMap.g:3948:2: rule__AliasToClassMapping__Group_5__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group_5__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_5__2"


    // $ANTLR start "rule__AliasToClassMapping__Group_5__2__Impl"
    // InternalJniMap.g:3954:1: rule__AliasToClassMapping__Group_5__2__Impl : ( ( rule__AliasToClassMapping__Group_5_2__0 )* ) ;
    public final void rule__AliasToClassMapping__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3958:1: ( ( ( rule__AliasToClassMapping__Group_5_2__0 )* ) )
            // InternalJniMap.g:3959:1: ( ( rule__AliasToClassMapping__Group_5_2__0 )* )
            {
            // InternalJniMap.g:3959:1: ( ( rule__AliasToClassMapping__Group_5_2__0 )* )
            // InternalJniMap.g:3960:1: ( rule__AliasToClassMapping__Group_5_2__0 )*
            {
             before(grammarAccess.getAliasToClassMappingAccess().getGroup_5_2()); 
            // InternalJniMap.g:3961:1: ( rule__AliasToClassMapping__Group_5_2__0 )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==33) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // InternalJniMap.g:3961:2: rule__AliasToClassMapping__Group_5_2__0
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__AliasToClassMapping__Group_5_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);

             after(grammarAccess.getAliasToClassMappingAccess().getGroup_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_5__2__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group_5_2__0"
    // InternalJniMap.g:3977:1: rule__AliasToClassMapping__Group_5_2__0 : rule__AliasToClassMapping__Group_5_2__0__Impl rule__AliasToClassMapping__Group_5_2__1 ;
    public final void rule__AliasToClassMapping__Group_5_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3981:1: ( rule__AliasToClassMapping__Group_5_2__0__Impl rule__AliasToClassMapping__Group_5_2__1 )
            // InternalJniMap.g:3982:2: rule__AliasToClassMapping__Group_5_2__0__Impl rule__AliasToClassMapping__Group_5_2__1
            {
            pushFollow(FOLLOW_5);
            rule__AliasToClassMapping__Group_5_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group_5_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_5_2__0"


    // $ANTLR start "rule__AliasToClassMapping__Group_5_2__0__Impl"
    // InternalJniMap.g:3989:1: rule__AliasToClassMapping__Group_5_2__0__Impl : ( ',' ) ;
    public final void rule__AliasToClassMapping__Group_5_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:3993:1: ( ( ',' ) )
            // InternalJniMap.g:3994:1: ( ',' )
            {
            // InternalJniMap.g:3994:1: ( ',' )
            // InternalJniMap.g:3995:1: ','
            {
             before(grammarAccess.getAliasToClassMappingAccess().getCommaKeyword_5_2_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getAliasToClassMappingAccess().getCommaKeyword_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_5_2__0__Impl"


    // $ANTLR start "rule__AliasToClassMapping__Group_5_2__1"
    // InternalJniMap.g:4008:1: rule__AliasToClassMapping__Group_5_2__1 : rule__AliasToClassMapping__Group_5_2__1__Impl ;
    public final void rule__AliasToClassMapping__Group_5_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4012:1: ( rule__AliasToClassMapping__Group_5_2__1__Impl )
            // InternalJniMap.g:4013:2: rule__AliasToClassMapping__Group_5_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__Group_5_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_5_2__1"


    // $ANTLR start "rule__AliasToClassMapping__Group_5_2__1__Impl"
    // InternalJniMap.g:4019:1: rule__AliasToClassMapping__Group_5_2__1__Impl : ( ( rule__AliasToClassMapping__InterfacesAssignment_5_2_1 ) ) ;
    public final void rule__AliasToClassMapping__Group_5_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4023:1: ( ( ( rule__AliasToClassMapping__InterfacesAssignment_5_2_1 ) ) )
            // InternalJniMap.g:4024:1: ( ( rule__AliasToClassMapping__InterfacesAssignment_5_2_1 ) )
            {
            // InternalJniMap.g:4024:1: ( ( rule__AliasToClassMapping__InterfacesAssignment_5_2_1 ) )
            // InternalJniMap.g:4025:1: ( rule__AliasToClassMapping__InterfacesAssignment_5_2_1 )
            {
             before(grammarAccess.getAliasToClassMappingAccess().getInterfacesAssignment_5_2_1()); 
            // InternalJniMap.g:4026:1: ( rule__AliasToClassMapping__InterfacesAssignment_5_2_1 )
            // InternalJniMap.g:4026:2: rule__AliasToClassMapping__InterfacesAssignment_5_2_1
            {
            pushFollow(FOLLOW_2);
            rule__AliasToClassMapping__InterfacesAssignment_5_2_1();

            state._fsp--;


            }

             after(grammarAccess.getAliasToClassMappingAccess().getInterfacesAssignment_5_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__Group_5_2__1__Impl"


    // $ANTLR start "rule__UnmappedClass__Group__0"
    // InternalJniMap.g:4040:1: rule__UnmappedClass__Group__0 : rule__UnmappedClass__Group__0__Impl rule__UnmappedClass__Group__1 ;
    public final void rule__UnmappedClass__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4044:1: ( rule__UnmappedClass__Group__0__Impl rule__UnmappedClass__Group__1 )
            // InternalJniMap.g:4045:2: rule__UnmappedClass__Group__0__Impl rule__UnmappedClass__Group__1
            {
            pushFollow(FOLLOW_28);
            rule__UnmappedClass__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__0"


    // $ANTLR start "rule__UnmappedClass__Group__0__Impl"
    // InternalJniMap.g:4052:1: rule__UnmappedClass__Group__0__Impl : ( 'unmapped' ) ;
    public final void rule__UnmappedClass__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4056:1: ( ( 'unmapped' ) )
            // InternalJniMap.g:4057:1: ( 'unmapped' )
            {
            // InternalJniMap.g:4057:1: ( 'unmapped' )
            // InternalJniMap.g:4058:1: 'unmapped'
            {
             before(grammarAccess.getUnmappedClassAccess().getUnmappedKeyword_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getUnmappedClassAccess().getUnmappedKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__0__Impl"


    // $ANTLR start "rule__UnmappedClass__Group__1"
    // InternalJniMap.g:4071:1: rule__UnmappedClass__Group__1 : rule__UnmappedClass__Group__1__Impl rule__UnmappedClass__Group__2 ;
    public final void rule__UnmappedClass__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4075:1: ( rule__UnmappedClass__Group__1__Impl rule__UnmappedClass__Group__2 )
            // InternalJniMap.g:4076:2: rule__UnmappedClass__Group__1__Impl rule__UnmappedClass__Group__2
            {
            pushFollow(FOLLOW_28);
            rule__UnmappedClass__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__1"


    // $ANTLR start "rule__UnmappedClass__Group__1__Impl"
    // InternalJniMap.g:4083:1: rule__UnmappedClass__Group__1__Impl : ( ( rule__UnmappedClass__AbstractAssignment_1 )? ) ;
    public final void rule__UnmappedClass__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4087:1: ( ( ( rule__UnmappedClass__AbstractAssignment_1 )? ) )
            // InternalJniMap.g:4088:1: ( ( rule__UnmappedClass__AbstractAssignment_1 )? )
            {
            // InternalJniMap.g:4088:1: ( ( rule__UnmappedClass__AbstractAssignment_1 )? )
            // InternalJniMap.g:4089:1: ( rule__UnmappedClass__AbstractAssignment_1 )?
            {
             before(grammarAccess.getUnmappedClassAccess().getAbstractAssignment_1()); 
            // InternalJniMap.g:4090:1: ( rule__UnmappedClass__AbstractAssignment_1 )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==57) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalJniMap.g:4090:2: rule__UnmappedClass__AbstractAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__UnmappedClass__AbstractAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getUnmappedClassAccess().getAbstractAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__1__Impl"


    // $ANTLR start "rule__UnmappedClass__Group__2"
    // InternalJniMap.g:4100:1: rule__UnmappedClass__Group__2 : rule__UnmappedClass__Group__2__Impl rule__UnmappedClass__Group__3 ;
    public final void rule__UnmappedClass__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4104:1: ( rule__UnmappedClass__Group__2__Impl rule__UnmappedClass__Group__3 )
            // InternalJniMap.g:4105:2: rule__UnmappedClass__Group__2__Impl rule__UnmappedClass__Group__3
            {
            pushFollow(FOLLOW_26);
            rule__UnmappedClass__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__2"


    // $ANTLR start "rule__UnmappedClass__Group__2__Impl"
    // InternalJniMap.g:4112:1: rule__UnmappedClass__Group__2__Impl : ( ( rule__UnmappedClass__NameAssignment_2 ) ) ;
    public final void rule__UnmappedClass__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4116:1: ( ( ( rule__UnmappedClass__NameAssignment_2 ) ) )
            // InternalJniMap.g:4117:1: ( ( rule__UnmappedClass__NameAssignment_2 ) )
            {
            // InternalJniMap.g:4117:1: ( ( rule__UnmappedClass__NameAssignment_2 ) )
            // InternalJniMap.g:4118:1: ( rule__UnmappedClass__NameAssignment_2 )
            {
             before(grammarAccess.getUnmappedClassAccess().getNameAssignment_2()); 
            // InternalJniMap.g:4119:1: ( rule__UnmappedClass__NameAssignment_2 )
            // InternalJniMap.g:4119:2: rule__UnmappedClass__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__UnmappedClass__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getUnmappedClassAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__2__Impl"


    // $ANTLR start "rule__UnmappedClass__Group__3"
    // InternalJniMap.g:4129:1: rule__UnmappedClass__Group__3 : rule__UnmappedClass__Group__3__Impl rule__UnmappedClass__Group__4 ;
    public final void rule__UnmappedClass__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4133:1: ( rule__UnmappedClass__Group__3__Impl rule__UnmappedClass__Group__4 )
            // InternalJniMap.g:4134:2: rule__UnmappedClass__Group__3__Impl rule__UnmappedClass__Group__4
            {
            pushFollow(FOLLOW_26);
            rule__UnmappedClass__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__3"


    // $ANTLR start "rule__UnmappedClass__Group__3__Impl"
    // InternalJniMap.g:4141:1: rule__UnmappedClass__Group__3__Impl : ( ( rule__UnmappedClass__GenericsDeclAssignment_3 )? ) ;
    public final void rule__UnmappedClass__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4145:1: ( ( ( rule__UnmappedClass__GenericsDeclAssignment_3 )? ) )
            // InternalJniMap.g:4146:1: ( ( rule__UnmappedClass__GenericsDeclAssignment_3 )? )
            {
            // InternalJniMap.g:4146:1: ( ( rule__UnmappedClass__GenericsDeclAssignment_3 )? )
            // InternalJniMap.g:4147:1: ( rule__UnmappedClass__GenericsDeclAssignment_3 )?
            {
             before(grammarAccess.getUnmappedClassAccess().getGenericsDeclAssignment_3()); 
            // InternalJniMap.g:4148:1: ( rule__UnmappedClass__GenericsDeclAssignment_3 )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==35) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalJniMap.g:4148:2: rule__UnmappedClass__GenericsDeclAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__UnmappedClass__GenericsDeclAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getUnmappedClassAccess().getGenericsDeclAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__3__Impl"


    // $ANTLR start "rule__UnmappedClass__Group__4"
    // InternalJniMap.g:4158:1: rule__UnmappedClass__Group__4 : rule__UnmappedClass__Group__4__Impl rule__UnmappedClass__Group__5 ;
    public final void rule__UnmappedClass__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4162:1: ( rule__UnmappedClass__Group__4__Impl rule__UnmappedClass__Group__5 )
            // InternalJniMap.g:4163:2: rule__UnmappedClass__Group__4__Impl rule__UnmappedClass__Group__5
            {
            pushFollow(FOLLOW_26);
            rule__UnmappedClass__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__4"


    // $ANTLR start "rule__UnmappedClass__Group__4__Impl"
    // InternalJniMap.g:4170:1: rule__UnmappedClass__Group__4__Impl : ( ( rule__UnmappedClass__Group_4__0 )? ) ;
    public final void rule__UnmappedClass__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4174:1: ( ( ( rule__UnmappedClass__Group_4__0 )? ) )
            // InternalJniMap.g:4175:1: ( ( rule__UnmappedClass__Group_4__0 )? )
            {
            // InternalJniMap.g:4175:1: ( ( rule__UnmappedClass__Group_4__0 )? )
            // InternalJniMap.g:4176:1: ( rule__UnmappedClass__Group_4__0 )?
            {
             before(grammarAccess.getUnmappedClassAccess().getGroup_4()); 
            // InternalJniMap.g:4177:1: ( rule__UnmappedClass__Group_4__0 )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==38) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalJniMap.g:4177:2: rule__UnmappedClass__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__UnmappedClass__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getUnmappedClassAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__4__Impl"


    // $ANTLR start "rule__UnmappedClass__Group__5"
    // InternalJniMap.g:4187:1: rule__UnmappedClass__Group__5 : rule__UnmappedClass__Group__5__Impl rule__UnmappedClass__Group__6 ;
    public final void rule__UnmappedClass__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4191:1: ( rule__UnmappedClass__Group__5__Impl rule__UnmappedClass__Group__6 )
            // InternalJniMap.g:4192:2: rule__UnmappedClass__Group__5__Impl rule__UnmappedClass__Group__6
            {
            pushFollow(FOLLOW_26);
            rule__UnmappedClass__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__5"


    // $ANTLR start "rule__UnmappedClass__Group__5__Impl"
    // InternalJniMap.g:4199:1: rule__UnmappedClass__Group__5__Impl : ( ( rule__UnmappedClass__Group_5__0 )? ) ;
    public final void rule__UnmappedClass__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4203:1: ( ( ( rule__UnmappedClass__Group_5__0 )? ) )
            // InternalJniMap.g:4204:1: ( ( rule__UnmappedClass__Group_5__0 )? )
            {
            // InternalJniMap.g:4204:1: ( ( rule__UnmappedClass__Group_5__0 )? )
            // InternalJniMap.g:4205:1: ( rule__UnmappedClass__Group_5__0 )?
            {
             before(grammarAccess.getUnmappedClassAccess().getGroup_5()); 
            // InternalJniMap.g:4206:1: ( rule__UnmappedClass__Group_5__0 )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==39) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalJniMap.g:4206:2: rule__UnmappedClass__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__UnmappedClass__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getUnmappedClassAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__5__Impl"


    // $ANTLR start "rule__UnmappedClass__Group__6"
    // InternalJniMap.g:4216:1: rule__UnmappedClass__Group__6 : rule__UnmappedClass__Group__6__Impl ;
    public final void rule__UnmappedClass__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4220:1: ( rule__UnmappedClass__Group__6__Impl )
            // InternalJniMap.g:4221:2: rule__UnmappedClass__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__6"


    // $ANTLR start "rule__UnmappedClass__Group__6__Impl"
    // InternalJniMap.g:4227:1: rule__UnmappedClass__Group__6__Impl : ( ';' ) ;
    public final void rule__UnmappedClass__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4231:1: ( ( ';' ) )
            // InternalJniMap.g:4232:1: ( ';' )
            {
            // InternalJniMap.g:4232:1: ( ';' )
            // InternalJniMap.g:4233:1: ';'
            {
             before(grammarAccess.getUnmappedClassAccess().getSemicolonKeyword_6()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getUnmappedClassAccess().getSemicolonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group__6__Impl"


    // $ANTLR start "rule__UnmappedClass__Group_4__0"
    // InternalJniMap.g:4260:1: rule__UnmappedClass__Group_4__0 : rule__UnmappedClass__Group_4__0__Impl rule__UnmappedClass__Group_4__1 ;
    public final void rule__UnmappedClass__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4264:1: ( rule__UnmappedClass__Group_4__0__Impl rule__UnmappedClass__Group_4__1 )
            // InternalJniMap.g:4265:2: rule__UnmappedClass__Group_4__0__Impl rule__UnmappedClass__Group_4__1
            {
            pushFollow(FOLLOW_5);
            rule__UnmappedClass__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_4__0"


    // $ANTLR start "rule__UnmappedClass__Group_4__0__Impl"
    // InternalJniMap.g:4272:1: rule__UnmappedClass__Group_4__0__Impl : ( 'extending' ) ;
    public final void rule__UnmappedClass__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4276:1: ( ( 'extending' ) )
            // InternalJniMap.g:4277:1: ( 'extending' )
            {
            // InternalJniMap.g:4277:1: ( 'extending' )
            // InternalJniMap.g:4278:1: 'extending'
            {
             before(grammarAccess.getUnmappedClassAccess().getExtendingKeyword_4_0()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getUnmappedClassAccess().getExtendingKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_4__0__Impl"


    // $ANTLR start "rule__UnmappedClass__Group_4__1"
    // InternalJniMap.g:4291:1: rule__UnmappedClass__Group_4__1 : rule__UnmappedClass__Group_4__1__Impl rule__UnmappedClass__Group_4__2 ;
    public final void rule__UnmappedClass__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4295:1: ( rule__UnmappedClass__Group_4__1__Impl rule__UnmappedClass__Group_4__2 )
            // InternalJniMap.g:4296:2: rule__UnmappedClass__Group_4__1__Impl rule__UnmappedClass__Group_4__2
            {
            pushFollow(FOLLOW_23);
            rule__UnmappedClass__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_4__1"


    // $ANTLR start "rule__UnmappedClass__Group_4__1__Impl"
    // InternalJniMap.g:4303:1: rule__UnmappedClass__Group_4__1__Impl : ( ( rule__UnmappedClass__SuperAssignment_4_1 ) ) ;
    public final void rule__UnmappedClass__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4307:1: ( ( ( rule__UnmappedClass__SuperAssignment_4_1 ) ) )
            // InternalJniMap.g:4308:1: ( ( rule__UnmappedClass__SuperAssignment_4_1 ) )
            {
            // InternalJniMap.g:4308:1: ( ( rule__UnmappedClass__SuperAssignment_4_1 ) )
            // InternalJniMap.g:4309:1: ( rule__UnmappedClass__SuperAssignment_4_1 )
            {
             before(grammarAccess.getUnmappedClassAccess().getSuperAssignment_4_1()); 
            // InternalJniMap.g:4310:1: ( rule__UnmappedClass__SuperAssignment_4_1 )
            // InternalJniMap.g:4310:2: rule__UnmappedClass__SuperAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__UnmappedClass__SuperAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getUnmappedClassAccess().getSuperAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_4__1__Impl"


    // $ANTLR start "rule__UnmappedClass__Group_4__2"
    // InternalJniMap.g:4320:1: rule__UnmappedClass__Group_4__2 : rule__UnmappedClass__Group_4__2__Impl ;
    public final void rule__UnmappedClass__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4324:1: ( rule__UnmappedClass__Group_4__2__Impl )
            // InternalJniMap.g:4325:2: rule__UnmappedClass__Group_4__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_4__2"


    // $ANTLR start "rule__UnmappedClass__Group_4__2__Impl"
    // InternalJniMap.g:4331:1: rule__UnmappedClass__Group_4__2__Impl : ( ( rule__UnmappedClass__GenericsInstantiationAssignment_4_2 )? ) ;
    public final void rule__UnmappedClass__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4335:1: ( ( ( rule__UnmappedClass__GenericsInstantiationAssignment_4_2 )? ) )
            // InternalJniMap.g:4336:1: ( ( rule__UnmappedClass__GenericsInstantiationAssignment_4_2 )? )
            {
            // InternalJniMap.g:4336:1: ( ( rule__UnmappedClass__GenericsInstantiationAssignment_4_2 )? )
            // InternalJniMap.g:4337:1: ( rule__UnmappedClass__GenericsInstantiationAssignment_4_2 )?
            {
             before(grammarAccess.getUnmappedClassAccess().getGenericsInstantiationAssignment_4_2()); 
            // InternalJniMap.g:4338:1: ( rule__UnmappedClass__GenericsInstantiationAssignment_4_2 )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==35) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalJniMap.g:4338:2: rule__UnmappedClass__GenericsInstantiationAssignment_4_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__UnmappedClass__GenericsInstantiationAssignment_4_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getUnmappedClassAccess().getGenericsInstantiationAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_4__2__Impl"


    // $ANTLR start "rule__UnmappedClass__Group_5__0"
    // InternalJniMap.g:4354:1: rule__UnmappedClass__Group_5__0 : rule__UnmappedClass__Group_5__0__Impl rule__UnmappedClass__Group_5__1 ;
    public final void rule__UnmappedClass__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4358:1: ( rule__UnmappedClass__Group_5__0__Impl rule__UnmappedClass__Group_5__1 )
            // InternalJniMap.g:4359:2: rule__UnmappedClass__Group_5__0__Impl rule__UnmappedClass__Group_5__1
            {
            pushFollow(FOLLOW_5);
            rule__UnmappedClass__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_5__0"


    // $ANTLR start "rule__UnmappedClass__Group_5__0__Impl"
    // InternalJniMap.g:4366:1: rule__UnmappedClass__Group_5__0__Impl : ( 'implements' ) ;
    public final void rule__UnmappedClass__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4370:1: ( ( 'implements' ) )
            // InternalJniMap.g:4371:1: ( 'implements' )
            {
            // InternalJniMap.g:4371:1: ( 'implements' )
            // InternalJniMap.g:4372:1: 'implements'
            {
             before(grammarAccess.getUnmappedClassAccess().getImplementsKeyword_5_0()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getUnmappedClassAccess().getImplementsKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_5__0__Impl"


    // $ANTLR start "rule__UnmappedClass__Group_5__1"
    // InternalJniMap.g:4385:1: rule__UnmappedClass__Group_5__1 : rule__UnmappedClass__Group_5__1__Impl rule__UnmappedClass__Group_5__2 ;
    public final void rule__UnmappedClass__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4389:1: ( rule__UnmappedClass__Group_5__1__Impl rule__UnmappedClass__Group_5__2 )
            // InternalJniMap.g:4390:2: rule__UnmappedClass__Group_5__1__Impl rule__UnmappedClass__Group_5__2
            {
            pushFollow(FOLLOW_27);
            rule__UnmappedClass__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_5__1"


    // $ANTLR start "rule__UnmappedClass__Group_5__1__Impl"
    // InternalJniMap.g:4397:1: rule__UnmappedClass__Group_5__1__Impl : ( ( rule__UnmappedClass__InterfacesAssignment_5_1 ) ) ;
    public final void rule__UnmappedClass__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4401:1: ( ( ( rule__UnmappedClass__InterfacesAssignment_5_1 ) ) )
            // InternalJniMap.g:4402:1: ( ( rule__UnmappedClass__InterfacesAssignment_5_1 ) )
            {
            // InternalJniMap.g:4402:1: ( ( rule__UnmappedClass__InterfacesAssignment_5_1 ) )
            // InternalJniMap.g:4403:1: ( rule__UnmappedClass__InterfacesAssignment_5_1 )
            {
             before(grammarAccess.getUnmappedClassAccess().getInterfacesAssignment_5_1()); 
            // InternalJniMap.g:4404:1: ( rule__UnmappedClass__InterfacesAssignment_5_1 )
            // InternalJniMap.g:4404:2: rule__UnmappedClass__InterfacesAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__UnmappedClass__InterfacesAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getUnmappedClassAccess().getInterfacesAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_5__1__Impl"


    // $ANTLR start "rule__UnmappedClass__Group_5__2"
    // InternalJniMap.g:4414:1: rule__UnmappedClass__Group_5__2 : rule__UnmappedClass__Group_5__2__Impl ;
    public final void rule__UnmappedClass__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4418:1: ( rule__UnmappedClass__Group_5__2__Impl )
            // InternalJniMap.g:4419:2: rule__UnmappedClass__Group_5__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group_5__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_5__2"


    // $ANTLR start "rule__UnmappedClass__Group_5__2__Impl"
    // InternalJniMap.g:4425:1: rule__UnmappedClass__Group_5__2__Impl : ( ( rule__UnmappedClass__Group_5_2__0 )* ) ;
    public final void rule__UnmappedClass__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4429:1: ( ( ( rule__UnmappedClass__Group_5_2__0 )* ) )
            // InternalJniMap.g:4430:1: ( ( rule__UnmappedClass__Group_5_2__0 )* )
            {
            // InternalJniMap.g:4430:1: ( ( rule__UnmappedClass__Group_5_2__0 )* )
            // InternalJniMap.g:4431:1: ( rule__UnmappedClass__Group_5_2__0 )*
            {
             before(grammarAccess.getUnmappedClassAccess().getGroup_5_2()); 
            // InternalJniMap.g:4432:1: ( rule__UnmappedClass__Group_5_2__0 )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( (LA38_0==33) ) {
                    alt38=1;
                }


                switch (alt38) {
            	case 1 :
            	    // InternalJniMap.g:4432:2: rule__UnmappedClass__Group_5_2__0
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__UnmappedClass__Group_5_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);

             after(grammarAccess.getUnmappedClassAccess().getGroup_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_5__2__Impl"


    // $ANTLR start "rule__UnmappedClass__Group_5_2__0"
    // InternalJniMap.g:4448:1: rule__UnmappedClass__Group_5_2__0 : rule__UnmappedClass__Group_5_2__0__Impl rule__UnmappedClass__Group_5_2__1 ;
    public final void rule__UnmappedClass__Group_5_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4452:1: ( rule__UnmappedClass__Group_5_2__0__Impl rule__UnmappedClass__Group_5_2__1 )
            // InternalJniMap.g:4453:2: rule__UnmappedClass__Group_5_2__0__Impl rule__UnmappedClass__Group_5_2__1
            {
            pushFollow(FOLLOW_5);
            rule__UnmappedClass__Group_5_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group_5_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_5_2__0"


    // $ANTLR start "rule__UnmappedClass__Group_5_2__0__Impl"
    // InternalJniMap.g:4460:1: rule__UnmappedClass__Group_5_2__0__Impl : ( ',' ) ;
    public final void rule__UnmappedClass__Group_5_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4464:1: ( ( ',' ) )
            // InternalJniMap.g:4465:1: ( ',' )
            {
            // InternalJniMap.g:4465:1: ( ',' )
            // InternalJniMap.g:4466:1: ','
            {
             before(grammarAccess.getUnmappedClassAccess().getCommaKeyword_5_2_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getUnmappedClassAccess().getCommaKeyword_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_5_2__0__Impl"


    // $ANTLR start "rule__UnmappedClass__Group_5_2__1"
    // InternalJniMap.g:4479:1: rule__UnmappedClass__Group_5_2__1 : rule__UnmappedClass__Group_5_2__1__Impl ;
    public final void rule__UnmappedClass__Group_5_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4483:1: ( rule__UnmappedClass__Group_5_2__1__Impl )
            // InternalJniMap.g:4484:2: rule__UnmappedClass__Group_5_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UnmappedClass__Group_5_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_5_2__1"


    // $ANTLR start "rule__UnmappedClass__Group_5_2__1__Impl"
    // InternalJniMap.g:4490:1: rule__UnmappedClass__Group_5_2__1__Impl : ( ( rule__UnmappedClass__InterfacesAssignment_5_2_1 ) ) ;
    public final void rule__UnmappedClass__Group_5_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4494:1: ( ( ( rule__UnmappedClass__InterfacesAssignment_5_2_1 ) ) )
            // InternalJniMap.g:4495:1: ( ( rule__UnmappedClass__InterfacesAssignment_5_2_1 ) )
            {
            // InternalJniMap.g:4495:1: ( ( rule__UnmappedClass__InterfacesAssignment_5_2_1 ) )
            // InternalJniMap.g:4496:1: ( rule__UnmappedClass__InterfacesAssignment_5_2_1 )
            {
             before(grammarAccess.getUnmappedClassAccess().getInterfacesAssignment_5_2_1()); 
            // InternalJniMap.g:4497:1: ( rule__UnmappedClass__InterfacesAssignment_5_2_1 )
            // InternalJniMap.g:4497:2: rule__UnmappedClass__InterfacesAssignment_5_2_1
            {
            pushFollow(FOLLOW_2);
            rule__UnmappedClass__InterfacesAssignment_5_2_1();

            state._fsp--;


            }

             after(grammarAccess.getUnmappedClassAccess().getInterfacesAssignment_5_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__Group_5_2__1__Impl"


    // $ANTLR start "rule__InterfaceClass__Group__0"
    // InternalJniMap.g:4511:1: rule__InterfaceClass__Group__0 : rule__InterfaceClass__Group__0__Impl rule__InterfaceClass__Group__1 ;
    public final void rule__InterfaceClass__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4515:1: ( rule__InterfaceClass__Group__0__Impl rule__InterfaceClass__Group__1 )
            // InternalJniMap.g:4516:2: rule__InterfaceClass__Group__0__Impl rule__InterfaceClass__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__InterfaceClass__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InterfaceClass__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group__0"


    // $ANTLR start "rule__InterfaceClass__Group__0__Impl"
    // InternalJniMap.g:4523:1: rule__InterfaceClass__Group__0__Impl : ( 'interface' ) ;
    public final void rule__InterfaceClass__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4527:1: ( ( 'interface' ) )
            // InternalJniMap.g:4528:1: ( 'interface' )
            {
            // InternalJniMap.g:4528:1: ( 'interface' )
            // InternalJniMap.g:4529:1: 'interface'
            {
             before(grammarAccess.getInterfaceClassAccess().getInterfaceKeyword_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getInterfaceClassAccess().getInterfaceKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group__0__Impl"


    // $ANTLR start "rule__InterfaceClass__Group__1"
    // InternalJniMap.g:4542:1: rule__InterfaceClass__Group__1 : rule__InterfaceClass__Group__1__Impl rule__InterfaceClass__Group__2 ;
    public final void rule__InterfaceClass__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4546:1: ( rule__InterfaceClass__Group__1__Impl rule__InterfaceClass__Group__2 )
            // InternalJniMap.g:4547:2: rule__InterfaceClass__Group__1__Impl rule__InterfaceClass__Group__2
            {
            pushFollow(FOLLOW_29);
            rule__InterfaceClass__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InterfaceClass__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group__1"


    // $ANTLR start "rule__InterfaceClass__Group__1__Impl"
    // InternalJniMap.g:4554:1: rule__InterfaceClass__Group__1__Impl : ( ( rule__InterfaceClass__NameAssignment_1 ) ) ;
    public final void rule__InterfaceClass__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4558:1: ( ( ( rule__InterfaceClass__NameAssignment_1 ) ) )
            // InternalJniMap.g:4559:1: ( ( rule__InterfaceClass__NameAssignment_1 ) )
            {
            // InternalJniMap.g:4559:1: ( ( rule__InterfaceClass__NameAssignment_1 ) )
            // InternalJniMap.g:4560:1: ( rule__InterfaceClass__NameAssignment_1 )
            {
             before(grammarAccess.getInterfaceClassAccess().getNameAssignment_1()); 
            // InternalJniMap.g:4561:1: ( rule__InterfaceClass__NameAssignment_1 )
            // InternalJniMap.g:4561:2: rule__InterfaceClass__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__InterfaceClass__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getInterfaceClassAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group__1__Impl"


    // $ANTLR start "rule__InterfaceClass__Group__2"
    // InternalJniMap.g:4571:1: rule__InterfaceClass__Group__2 : rule__InterfaceClass__Group__2__Impl rule__InterfaceClass__Group__3 ;
    public final void rule__InterfaceClass__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4575:1: ( rule__InterfaceClass__Group__2__Impl rule__InterfaceClass__Group__3 )
            // InternalJniMap.g:4576:2: rule__InterfaceClass__Group__2__Impl rule__InterfaceClass__Group__3
            {
            pushFollow(FOLLOW_29);
            rule__InterfaceClass__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InterfaceClass__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group__2"


    // $ANTLR start "rule__InterfaceClass__Group__2__Impl"
    // InternalJniMap.g:4583:1: rule__InterfaceClass__Group__2__Impl : ( ( rule__InterfaceClass__GenericsDeclAssignment_2 )? ) ;
    public final void rule__InterfaceClass__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4587:1: ( ( ( rule__InterfaceClass__GenericsDeclAssignment_2 )? ) )
            // InternalJniMap.g:4588:1: ( ( rule__InterfaceClass__GenericsDeclAssignment_2 )? )
            {
            // InternalJniMap.g:4588:1: ( ( rule__InterfaceClass__GenericsDeclAssignment_2 )? )
            // InternalJniMap.g:4589:1: ( rule__InterfaceClass__GenericsDeclAssignment_2 )?
            {
             before(grammarAccess.getInterfaceClassAccess().getGenericsDeclAssignment_2()); 
            // InternalJniMap.g:4590:1: ( rule__InterfaceClass__GenericsDeclAssignment_2 )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==35) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalJniMap.g:4590:2: rule__InterfaceClass__GenericsDeclAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__InterfaceClass__GenericsDeclAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getInterfaceClassAccess().getGenericsDeclAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group__2__Impl"


    // $ANTLR start "rule__InterfaceClass__Group__3"
    // InternalJniMap.g:4600:1: rule__InterfaceClass__Group__3 : rule__InterfaceClass__Group__3__Impl rule__InterfaceClass__Group__4 ;
    public final void rule__InterfaceClass__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4604:1: ( rule__InterfaceClass__Group__3__Impl rule__InterfaceClass__Group__4 )
            // InternalJniMap.g:4605:2: rule__InterfaceClass__Group__3__Impl rule__InterfaceClass__Group__4
            {
            pushFollow(FOLLOW_29);
            rule__InterfaceClass__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InterfaceClass__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group__3"


    // $ANTLR start "rule__InterfaceClass__Group__3__Impl"
    // InternalJniMap.g:4612:1: rule__InterfaceClass__Group__3__Impl : ( ( rule__InterfaceClass__Group_3__0 )? ) ;
    public final void rule__InterfaceClass__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4616:1: ( ( ( rule__InterfaceClass__Group_3__0 )? ) )
            // InternalJniMap.g:4617:1: ( ( rule__InterfaceClass__Group_3__0 )? )
            {
            // InternalJniMap.g:4617:1: ( ( rule__InterfaceClass__Group_3__0 )? )
            // InternalJniMap.g:4618:1: ( rule__InterfaceClass__Group_3__0 )?
            {
             before(grammarAccess.getInterfaceClassAccess().getGroup_3()); 
            // InternalJniMap.g:4619:1: ( rule__InterfaceClass__Group_3__0 )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==38) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalJniMap.g:4619:2: rule__InterfaceClass__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__InterfaceClass__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getInterfaceClassAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group__3__Impl"


    // $ANTLR start "rule__InterfaceClass__Group__4"
    // InternalJniMap.g:4629:1: rule__InterfaceClass__Group__4 : rule__InterfaceClass__Group__4__Impl ;
    public final void rule__InterfaceClass__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4633:1: ( rule__InterfaceClass__Group__4__Impl )
            // InternalJniMap.g:4634:2: rule__InterfaceClass__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InterfaceClass__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group__4"


    // $ANTLR start "rule__InterfaceClass__Group__4__Impl"
    // InternalJniMap.g:4640:1: rule__InterfaceClass__Group__4__Impl : ( ';' ) ;
    public final void rule__InterfaceClass__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4644:1: ( ( ';' ) )
            // InternalJniMap.g:4645:1: ( ';' )
            {
            // InternalJniMap.g:4645:1: ( ';' )
            // InternalJniMap.g:4646:1: ';'
            {
             before(grammarAccess.getInterfaceClassAccess().getSemicolonKeyword_4()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getInterfaceClassAccess().getSemicolonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group__4__Impl"


    // $ANTLR start "rule__InterfaceClass__Group_3__0"
    // InternalJniMap.g:4669:1: rule__InterfaceClass__Group_3__0 : rule__InterfaceClass__Group_3__0__Impl rule__InterfaceClass__Group_3__1 ;
    public final void rule__InterfaceClass__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4673:1: ( rule__InterfaceClass__Group_3__0__Impl rule__InterfaceClass__Group_3__1 )
            // InternalJniMap.g:4674:2: rule__InterfaceClass__Group_3__0__Impl rule__InterfaceClass__Group_3__1
            {
            pushFollow(FOLLOW_5);
            rule__InterfaceClass__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InterfaceClass__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group_3__0"


    // $ANTLR start "rule__InterfaceClass__Group_3__0__Impl"
    // InternalJniMap.g:4681:1: rule__InterfaceClass__Group_3__0__Impl : ( 'extending' ) ;
    public final void rule__InterfaceClass__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4685:1: ( ( 'extending' ) )
            // InternalJniMap.g:4686:1: ( 'extending' )
            {
            // InternalJniMap.g:4686:1: ( 'extending' )
            // InternalJniMap.g:4687:1: 'extending'
            {
             before(grammarAccess.getInterfaceClassAccess().getExtendingKeyword_3_0()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getInterfaceClassAccess().getExtendingKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group_3__0__Impl"


    // $ANTLR start "rule__InterfaceClass__Group_3__1"
    // InternalJniMap.g:4700:1: rule__InterfaceClass__Group_3__1 : rule__InterfaceClass__Group_3__1__Impl rule__InterfaceClass__Group_3__2 ;
    public final void rule__InterfaceClass__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4704:1: ( rule__InterfaceClass__Group_3__1__Impl rule__InterfaceClass__Group_3__2 )
            // InternalJniMap.g:4705:2: rule__InterfaceClass__Group_3__1__Impl rule__InterfaceClass__Group_3__2
            {
            pushFollow(FOLLOW_27);
            rule__InterfaceClass__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InterfaceClass__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group_3__1"


    // $ANTLR start "rule__InterfaceClass__Group_3__1__Impl"
    // InternalJniMap.g:4712:1: rule__InterfaceClass__Group_3__1__Impl : ( ( rule__InterfaceClass__InterfacesAssignment_3_1 ) ) ;
    public final void rule__InterfaceClass__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4716:1: ( ( ( rule__InterfaceClass__InterfacesAssignment_3_1 ) ) )
            // InternalJniMap.g:4717:1: ( ( rule__InterfaceClass__InterfacesAssignment_3_1 ) )
            {
            // InternalJniMap.g:4717:1: ( ( rule__InterfaceClass__InterfacesAssignment_3_1 ) )
            // InternalJniMap.g:4718:1: ( rule__InterfaceClass__InterfacesAssignment_3_1 )
            {
             before(grammarAccess.getInterfaceClassAccess().getInterfacesAssignment_3_1()); 
            // InternalJniMap.g:4719:1: ( rule__InterfaceClass__InterfacesAssignment_3_1 )
            // InternalJniMap.g:4719:2: rule__InterfaceClass__InterfacesAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__InterfaceClass__InterfacesAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getInterfaceClassAccess().getInterfacesAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group_3__1__Impl"


    // $ANTLR start "rule__InterfaceClass__Group_3__2"
    // InternalJniMap.g:4729:1: rule__InterfaceClass__Group_3__2 : rule__InterfaceClass__Group_3__2__Impl ;
    public final void rule__InterfaceClass__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4733:1: ( rule__InterfaceClass__Group_3__2__Impl )
            // InternalJniMap.g:4734:2: rule__InterfaceClass__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InterfaceClass__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group_3__2"


    // $ANTLR start "rule__InterfaceClass__Group_3__2__Impl"
    // InternalJniMap.g:4740:1: rule__InterfaceClass__Group_3__2__Impl : ( ( rule__InterfaceClass__Group_3_2__0 )* ) ;
    public final void rule__InterfaceClass__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4744:1: ( ( ( rule__InterfaceClass__Group_3_2__0 )* ) )
            // InternalJniMap.g:4745:1: ( ( rule__InterfaceClass__Group_3_2__0 )* )
            {
            // InternalJniMap.g:4745:1: ( ( rule__InterfaceClass__Group_3_2__0 )* )
            // InternalJniMap.g:4746:1: ( rule__InterfaceClass__Group_3_2__0 )*
            {
             before(grammarAccess.getInterfaceClassAccess().getGroup_3_2()); 
            // InternalJniMap.g:4747:1: ( rule__InterfaceClass__Group_3_2__0 )*
            loop41:
            do {
                int alt41=2;
                int LA41_0 = input.LA(1);

                if ( (LA41_0==33) ) {
                    alt41=1;
                }


                switch (alt41) {
            	case 1 :
            	    // InternalJniMap.g:4747:2: rule__InterfaceClass__Group_3_2__0
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__InterfaceClass__Group_3_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop41;
                }
            } while (true);

             after(grammarAccess.getInterfaceClassAccess().getGroup_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group_3__2__Impl"


    // $ANTLR start "rule__InterfaceClass__Group_3_2__0"
    // InternalJniMap.g:4763:1: rule__InterfaceClass__Group_3_2__0 : rule__InterfaceClass__Group_3_2__0__Impl rule__InterfaceClass__Group_3_2__1 ;
    public final void rule__InterfaceClass__Group_3_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4767:1: ( rule__InterfaceClass__Group_3_2__0__Impl rule__InterfaceClass__Group_3_2__1 )
            // InternalJniMap.g:4768:2: rule__InterfaceClass__Group_3_2__0__Impl rule__InterfaceClass__Group_3_2__1
            {
            pushFollow(FOLLOW_5);
            rule__InterfaceClass__Group_3_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InterfaceClass__Group_3_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group_3_2__0"


    // $ANTLR start "rule__InterfaceClass__Group_3_2__0__Impl"
    // InternalJniMap.g:4775:1: rule__InterfaceClass__Group_3_2__0__Impl : ( ',' ) ;
    public final void rule__InterfaceClass__Group_3_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4779:1: ( ( ',' ) )
            // InternalJniMap.g:4780:1: ( ',' )
            {
            // InternalJniMap.g:4780:1: ( ',' )
            // InternalJniMap.g:4781:1: ','
            {
             before(grammarAccess.getInterfaceClassAccess().getCommaKeyword_3_2_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getInterfaceClassAccess().getCommaKeyword_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group_3_2__0__Impl"


    // $ANTLR start "rule__InterfaceClass__Group_3_2__1"
    // InternalJniMap.g:4794:1: rule__InterfaceClass__Group_3_2__1 : rule__InterfaceClass__Group_3_2__1__Impl ;
    public final void rule__InterfaceClass__Group_3_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4798:1: ( rule__InterfaceClass__Group_3_2__1__Impl )
            // InternalJniMap.g:4799:2: rule__InterfaceClass__Group_3_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InterfaceClass__Group_3_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group_3_2__1"


    // $ANTLR start "rule__InterfaceClass__Group_3_2__1__Impl"
    // InternalJniMap.g:4805:1: rule__InterfaceClass__Group_3_2__1__Impl : ( ( rule__InterfaceClass__InterfacesAssignment_3_2_1 ) ) ;
    public final void rule__InterfaceClass__Group_3_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4809:1: ( ( ( rule__InterfaceClass__InterfacesAssignment_3_2_1 ) ) )
            // InternalJniMap.g:4810:1: ( ( rule__InterfaceClass__InterfacesAssignment_3_2_1 ) )
            {
            // InternalJniMap.g:4810:1: ( ( rule__InterfaceClass__InterfacesAssignment_3_2_1 ) )
            // InternalJniMap.g:4811:1: ( rule__InterfaceClass__InterfacesAssignment_3_2_1 )
            {
             before(grammarAccess.getInterfaceClassAccess().getInterfacesAssignment_3_2_1()); 
            // InternalJniMap.g:4812:1: ( rule__InterfaceClass__InterfacesAssignment_3_2_1 )
            // InternalJniMap.g:4812:2: rule__InterfaceClass__InterfacesAssignment_3_2_1
            {
            pushFollow(FOLLOW_2);
            rule__InterfaceClass__InterfacesAssignment_3_2_1();

            state._fsp--;


            }

             after(grammarAccess.getInterfaceClassAccess().getInterfacesAssignment_3_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__Group_3_2__1__Impl"


    // $ANTLR start "rule__ExternalLibrary__Group__0"
    // InternalJniMap.g:4826:1: rule__ExternalLibrary__Group__0 : rule__ExternalLibrary__Group__0__Impl rule__ExternalLibrary__Group__1 ;
    public final void rule__ExternalLibrary__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4830:1: ( rule__ExternalLibrary__Group__0__Impl rule__ExternalLibrary__Group__1 )
            // InternalJniMap.g:4831:2: rule__ExternalLibrary__Group__0__Impl rule__ExternalLibrary__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__ExternalLibrary__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ExternalLibrary__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExternalLibrary__Group__0"


    // $ANTLR start "rule__ExternalLibrary__Group__0__Impl"
    // InternalJniMap.g:4838:1: rule__ExternalLibrary__Group__0__Impl : ( 'use' ) ;
    public final void rule__ExternalLibrary__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4842:1: ( ( 'use' ) )
            // InternalJniMap.g:4843:1: ( 'use' )
            {
            // InternalJniMap.g:4843:1: ( 'use' )
            // InternalJniMap.g:4844:1: 'use'
            {
             before(grammarAccess.getExternalLibraryAccess().getUseKeyword_0()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getExternalLibraryAccess().getUseKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExternalLibrary__Group__0__Impl"


    // $ANTLR start "rule__ExternalLibrary__Group__1"
    // InternalJniMap.g:4857:1: rule__ExternalLibrary__Group__1 : rule__ExternalLibrary__Group__1__Impl rule__ExternalLibrary__Group__2 ;
    public final void rule__ExternalLibrary__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4861:1: ( rule__ExternalLibrary__Group__1__Impl rule__ExternalLibrary__Group__2 )
            // InternalJniMap.g:4862:2: rule__ExternalLibrary__Group__1__Impl rule__ExternalLibrary__Group__2
            {
            pushFollow(FOLLOW_30);
            rule__ExternalLibrary__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ExternalLibrary__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExternalLibrary__Group__1"


    // $ANTLR start "rule__ExternalLibrary__Group__1__Impl"
    // InternalJniMap.g:4869:1: rule__ExternalLibrary__Group__1__Impl : ( ( rule__ExternalLibrary__LibraryAssignment_1 ) ) ;
    public final void rule__ExternalLibrary__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4873:1: ( ( ( rule__ExternalLibrary__LibraryAssignment_1 ) ) )
            // InternalJniMap.g:4874:1: ( ( rule__ExternalLibrary__LibraryAssignment_1 ) )
            {
            // InternalJniMap.g:4874:1: ( ( rule__ExternalLibrary__LibraryAssignment_1 ) )
            // InternalJniMap.g:4875:1: ( rule__ExternalLibrary__LibraryAssignment_1 )
            {
             before(grammarAccess.getExternalLibraryAccess().getLibraryAssignment_1()); 
            // InternalJniMap.g:4876:1: ( rule__ExternalLibrary__LibraryAssignment_1 )
            // InternalJniMap.g:4876:2: rule__ExternalLibrary__LibraryAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ExternalLibrary__LibraryAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getExternalLibraryAccess().getLibraryAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExternalLibrary__Group__1__Impl"


    // $ANTLR start "rule__ExternalLibrary__Group__2"
    // InternalJniMap.g:4886:1: rule__ExternalLibrary__Group__2 : rule__ExternalLibrary__Group__2__Impl rule__ExternalLibrary__Group__3 ;
    public final void rule__ExternalLibrary__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4890:1: ( rule__ExternalLibrary__Group__2__Impl rule__ExternalLibrary__Group__3 )
            // InternalJniMap.g:4891:2: rule__ExternalLibrary__Group__2__Impl rule__ExternalLibrary__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__ExternalLibrary__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ExternalLibrary__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExternalLibrary__Group__2"


    // $ANTLR start "rule__ExternalLibrary__Group__2__Impl"
    // InternalJniMap.g:4898:1: rule__ExternalLibrary__Group__2__Impl : ( 'from' ) ;
    public final void rule__ExternalLibrary__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4902:1: ( ( 'from' ) )
            // InternalJniMap.g:4903:1: ( 'from' )
            {
            // InternalJniMap.g:4903:1: ( 'from' )
            // InternalJniMap.g:4904:1: 'from'
            {
             before(grammarAccess.getExternalLibraryAccess().getFromKeyword_2()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getExternalLibraryAccess().getFromKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExternalLibrary__Group__2__Impl"


    // $ANTLR start "rule__ExternalLibrary__Group__3"
    // InternalJniMap.g:4917:1: rule__ExternalLibrary__Group__3 : rule__ExternalLibrary__Group__3__Impl rule__ExternalLibrary__Group__4 ;
    public final void rule__ExternalLibrary__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4921:1: ( rule__ExternalLibrary__Group__3__Impl rule__ExternalLibrary__Group__4 )
            // InternalJniMap.g:4922:2: rule__ExternalLibrary__Group__3__Impl rule__ExternalLibrary__Group__4
            {
            pushFollow(FOLLOW_7);
            rule__ExternalLibrary__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ExternalLibrary__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExternalLibrary__Group__3"


    // $ANTLR start "rule__ExternalLibrary__Group__3__Impl"
    // InternalJniMap.g:4929:1: rule__ExternalLibrary__Group__3__Impl : ( ( rule__ExternalLibrary__MappingAssignment_3 ) ) ;
    public final void rule__ExternalLibrary__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4933:1: ( ( ( rule__ExternalLibrary__MappingAssignment_3 ) ) )
            // InternalJniMap.g:4934:1: ( ( rule__ExternalLibrary__MappingAssignment_3 ) )
            {
            // InternalJniMap.g:4934:1: ( ( rule__ExternalLibrary__MappingAssignment_3 ) )
            // InternalJniMap.g:4935:1: ( rule__ExternalLibrary__MappingAssignment_3 )
            {
             before(grammarAccess.getExternalLibraryAccess().getMappingAssignment_3()); 
            // InternalJniMap.g:4936:1: ( rule__ExternalLibrary__MappingAssignment_3 )
            // InternalJniMap.g:4936:2: rule__ExternalLibrary__MappingAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ExternalLibrary__MappingAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getExternalLibraryAccess().getMappingAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExternalLibrary__Group__3__Impl"


    // $ANTLR start "rule__ExternalLibrary__Group__4"
    // InternalJniMap.g:4946:1: rule__ExternalLibrary__Group__4 : rule__ExternalLibrary__Group__4__Impl ;
    public final void rule__ExternalLibrary__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4950:1: ( rule__ExternalLibrary__Group__4__Impl )
            // InternalJniMap.g:4951:2: rule__ExternalLibrary__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ExternalLibrary__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExternalLibrary__Group__4"


    // $ANTLR start "rule__ExternalLibrary__Group__4__Impl"
    // InternalJniMap.g:4957:1: rule__ExternalLibrary__Group__4__Impl : ( ';' ) ;
    public final void rule__ExternalLibrary__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4961:1: ( ( ';' ) )
            // InternalJniMap.g:4962:1: ( ';' )
            {
            // InternalJniMap.g:4962:1: ( ';' )
            // InternalJniMap.g:4963:1: ';'
            {
             before(grammarAccess.getExternalLibraryAccess().getSemicolonKeyword_4()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getExternalLibraryAccess().getSemicolonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExternalLibrary__Group__4__Impl"


    // $ANTLR start "rule__Library__Group__0"
    // InternalJniMap.g:4986:1: rule__Library__Group__0 : rule__Library__Group__0__Impl rule__Library__Group__1 ;
    public final void rule__Library__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:4990:1: ( rule__Library__Group__0__Impl rule__Library__Group__1 )
            // InternalJniMap.g:4991:2: rule__Library__Group__0__Impl rule__Library__Group__1
            {
            pushFollow(FOLLOW_31);
            rule__Library__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Library__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__0"


    // $ANTLR start "rule__Library__Group__0__Impl"
    // InternalJniMap.g:4998:1: rule__Library__Group__0__Impl : ( 'library' ) ;
    public final void rule__Library__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5002:1: ( ( 'library' ) )
            // InternalJniMap.g:5003:1: ( 'library' )
            {
            // InternalJniMap.g:5003:1: ( 'library' )
            // InternalJniMap.g:5004:1: 'library'
            {
             before(grammarAccess.getLibraryAccess().getLibraryKeyword_0()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getLibraryAccess().getLibraryKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__0__Impl"


    // $ANTLR start "rule__Library__Group__1"
    // InternalJniMap.g:5017:1: rule__Library__Group__1 : rule__Library__Group__1__Impl rule__Library__Group__2 ;
    public final void rule__Library__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5021:1: ( rule__Library__Group__1__Impl rule__Library__Group__2 )
            // InternalJniMap.g:5022:2: rule__Library__Group__1__Impl rule__Library__Group__2
            {
            pushFollow(FOLLOW_32);
            rule__Library__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Library__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__1"


    // $ANTLR start "rule__Library__Group__1__Impl"
    // InternalJniMap.g:5029:1: rule__Library__Group__1__Impl : ( '{' ) ;
    public final void rule__Library__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5033:1: ( ( '{' ) )
            // InternalJniMap.g:5034:1: ( '{' )
            {
            // InternalJniMap.g:5034:1: ( '{' )
            // InternalJniMap.g:5035:1: '{'
            {
             before(grammarAccess.getLibraryAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getLibraryAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__1__Impl"


    // $ANTLR start "rule__Library__Group__2"
    // InternalJniMap.g:5048:1: rule__Library__Group__2 : rule__Library__Group__2__Impl rule__Library__Group__3 ;
    public final void rule__Library__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5052:1: ( rule__Library__Group__2__Impl rule__Library__Group__3 )
            // InternalJniMap.g:5053:2: rule__Library__Group__2__Impl rule__Library__Group__3
            {
            pushFollow(FOLLOW_16);
            rule__Library__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Library__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__2"


    // $ANTLR start "rule__Library__Group__2__Impl"
    // InternalJniMap.g:5060:1: rule__Library__Group__2__Impl : ( 'libname' ) ;
    public final void rule__Library__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5064:1: ( ( 'libname' ) )
            // InternalJniMap.g:5065:1: ( 'libname' )
            {
            // InternalJniMap.g:5065:1: ( 'libname' )
            // InternalJniMap.g:5066:1: 'libname'
            {
             before(grammarAccess.getLibraryAccess().getLibnameKeyword_2()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getLibraryAccess().getLibnameKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__2__Impl"


    // $ANTLR start "rule__Library__Group__3"
    // InternalJniMap.g:5079:1: rule__Library__Group__3 : rule__Library__Group__3__Impl rule__Library__Group__4 ;
    public final void rule__Library__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5083:1: ( rule__Library__Group__3__Impl rule__Library__Group__4 )
            // InternalJniMap.g:5084:2: rule__Library__Group__3__Impl rule__Library__Group__4
            {
            pushFollow(FOLLOW_17);
            rule__Library__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Library__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__3"


    // $ANTLR start "rule__Library__Group__3__Impl"
    // InternalJniMap.g:5091:1: rule__Library__Group__3__Impl : ( '=' ) ;
    public final void rule__Library__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5095:1: ( ( '=' ) )
            // InternalJniMap.g:5096:1: ( '=' )
            {
            // InternalJniMap.g:5096:1: ( '=' )
            // InternalJniMap.g:5097:1: '='
            {
             before(grammarAccess.getLibraryAccess().getEqualsSignKeyword_3()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getLibraryAccess().getEqualsSignKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__3__Impl"


    // $ANTLR start "rule__Library__Group__4"
    // InternalJniMap.g:5110:1: rule__Library__Group__4 : rule__Library__Group__4__Impl rule__Library__Group__5 ;
    public final void rule__Library__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5114:1: ( rule__Library__Group__4__Impl rule__Library__Group__5 )
            // InternalJniMap.g:5115:2: rule__Library__Group__4__Impl rule__Library__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__Library__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Library__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__4"


    // $ANTLR start "rule__Library__Group__4__Impl"
    // InternalJniMap.g:5122:1: rule__Library__Group__4__Impl : ( ( rule__Library__NameAssignment_4 ) ) ;
    public final void rule__Library__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5126:1: ( ( ( rule__Library__NameAssignment_4 ) ) )
            // InternalJniMap.g:5127:1: ( ( rule__Library__NameAssignment_4 ) )
            {
            // InternalJniMap.g:5127:1: ( ( rule__Library__NameAssignment_4 ) )
            // InternalJniMap.g:5128:1: ( rule__Library__NameAssignment_4 )
            {
             before(grammarAccess.getLibraryAccess().getNameAssignment_4()); 
            // InternalJniMap.g:5129:1: ( rule__Library__NameAssignment_4 )
            // InternalJniMap.g:5129:2: rule__Library__NameAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Library__NameAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getLibraryAccess().getNameAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__4__Impl"


    // $ANTLR start "rule__Library__Group__5"
    // InternalJniMap.g:5139:1: rule__Library__Group__5 : rule__Library__Group__5__Impl rule__Library__Group__6 ;
    public final void rule__Library__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5143:1: ( rule__Library__Group__5__Impl rule__Library__Group__6 )
            // InternalJniMap.g:5144:2: rule__Library__Group__5__Impl rule__Library__Group__6
            {
            pushFollow(FOLLOW_19);
            rule__Library__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Library__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__5"


    // $ANTLR start "rule__Library__Group__5__Impl"
    // InternalJniMap.g:5151:1: rule__Library__Group__5__Impl : ( ';' ) ;
    public final void rule__Library__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5155:1: ( ( ';' ) )
            // InternalJniMap.g:5156:1: ( ';' )
            {
            // InternalJniMap.g:5156:1: ( ';' )
            // InternalJniMap.g:5157:1: ';'
            {
             before(grammarAccess.getLibraryAccess().getSemicolonKeyword_5()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getLibraryAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__5__Impl"


    // $ANTLR start "rule__Library__Group__6"
    // InternalJniMap.g:5170:1: rule__Library__Group__6 : rule__Library__Group__6__Impl rule__Library__Group__7 ;
    public final void rule__Library__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5174:1: ( rule__Library__Group__6__Impl rule__Library__Group__7 )
            // InternalJniMap.g:5175:2: rule__Library__Group__6__Impl rule__Library__Group__7
            {
            pushFollow(FOLLOW_33);
            rule__Library__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Library__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__6"


    // $ANTLR start "rule__Library__Group__6__Impl"
    // InternalJniMap.g:5182:1: rule__Library__Group__6__Impl : ( ( ( rule__Library__Group_6__0 ) ) ( ( rule__Library__Group_6__0 )* ) ) ;
    public final void rule__Library__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5186:1: ( ( ( ( rule__Library__Group_6__0 ) ) ( ( rule__Library__Group_6__0 )* ) ) )
            // InternalJniMap.g:5187:1: ( ( ( rule__Library__Group_6__0 ) ) ( ( rule__Library__Group_6__0 )* ) )
            {
            // InternalJniMap.g:5187:1: ( ( ( rule__Library__Group_6__0 ) ) ( ( rule__Library__Group_6__0 )* ) )
            // InternalJniMap.g:5188:1: ( ( rule__Library__Group_6__0 ) ) ( ( rule__Library__Group_6__0 )* )
            {
            // InternalJniMap.g:5188:1: ( ( rule__Library__Group_6__0 ) )
            // InternalJniMap.g:5189:1: ( rule__Library__Group_6__0 )
            {
             before(grammarAccess.getLibraryAccess().getGroup_6()); 
            // InternalJniMap.g:5190:1: ( rule__Library__Group_6__0 )
            // InternalJniMap.g:5190:2: rule__Library__Group_6__0
            {
            pushFollow(FOLLOW_34);
            rule__Library__Group_6__0();

            state._fsp--;


            }

             after(grammarAccess.getLibraryAccess().getGroup_6()); 

            }

            // InternalJniMap.g:5193:1: ( ( rule__Library__Group_6__0 )* )
            // InternalJniMap.g:5194:1: ( rule__Library__Group_6__0 )*
            {
             before(grammarAccess.getLibraryAccess().getGroup_6()); 
            // InternalJniMap.g:5195:1: ( rule__Library__Group_6__0 )*
            loop42:
            do {
                int alt42=2;
                int LA42_0 = input.LA(1);

                if ( ((LA42_0>=19 && LA42_0<=24)) ) {
                    alt42=1;
                }


                switch (alt42) {
            	case 1 :
            	    // InternalJniMap.g:5195:2: rule__Library__Group_6__0
            	    {
            	    pushFollow(FOLLOW_34);
            	    rule__Library__Group_6__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop42;
                }
            } while (true);

             after(grammarAccess.getLibraryAccess().getGroup_6()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__6__Impl"


    // $ANTLR start "rule__Library__Group__7"
    // InternalJniMap.g:5206:1: rule__Library__Group__7 : rule__Library__Group__7__Impl ;
    public final void rule__Library__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5210:1: ( rule__Library__Group__7__Impl )
            // InternalJniMap.g:5211:2: rule__Library__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Library__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__7"


    // $ANTLR start "rule__Library__Group__7__Impl"
    // InternalJniMap.g:5217:1: rule__Library__Group__7__Impl : ( '}' ) ;
    public final void rule__Library__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5221:1: ( ( '}' ) )
            // InternalJniMap.g:5222:1: ( '}' )
            {
            // InternalJniMap.g:5222:1: ( '}' )
            // InternalJniMap.g:5223:1: '}'
            {
             before(grammarAccess.getLibraryAccess().getRightCurlyBracketKeyword_7()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getLibraryAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group__7__Impl"


    // $ANTLR start "rule__Library__Group_6__0"
    // InternalJniMap.g:5252:1: rule__Library__Group_6__0 : rule__Library__Group_6__0__Impl rule__Library__Group_6__1 ;
    public final void rule__Library__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5256:1: ( rule__Library__Group_6__0__Impl rule__Library__Group_6__1 )
            // InternalJniMap.g:5257:2: rule__Library__Group_6__0__Impl rule__Library__Group_6__1
            {
            pushFollow(FOLLOW_7);
            rule__Library__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Library__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group_6__0"


    // $ANTLR start "rule__Library__Group_6__0__Impl"
    // InternalJniMap.g:5264:1: rule__Library__Group_6__0__Impl : ( ( rule__Library__FilenamesAssignment_6_0 ) ) ;
    public final void rule__Library__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5268:1: ( ( ( rule__Library__FilenamesAssignment_6_0 ) ) )
            // InternalJniMap.g:5269:1: ( ( rule__Library__FilenamesAssignment_6_0 ) )
            {
            // InternalJniMap.g:5269:1: ( ( rule__Library__FilenamesAssignment_6_0 ) )
            // InternalJniMap.g:5270:1: ( rule__Library__FilenamesAssignment_6_0 )
            {
             before(grammarAccess.getLibraryAccess().getFilenamesAssignment_6_0()); 
            // InternalJniMap.g:5271:1: ( rule__Library__FilenamesAssignment_6_0 )
            // InternalJniMap.g:5271:2: rule__Library__FilenamesAssignment_6_0
            {
            pushFollow(FOLLOW_2);
            rule__Library__FilenamesAssignment_6_0();

            state._fsp--;


            }

             after(grammarAccess.getLibraryAccess().getFilenamesAssignment_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group_6__0__Impl"


    // $ANTLR start "rule__Library__Group_6__1"
    // InternalJniMap.g:5281:1: rule__Library__Group_6__1 : rule__Library__Group_6__1__Impl ;
    public final void rule__Library__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5285:1: ( rule__Library__Group_6__1__Impl )
            // InternalJniMap.g:5286:2: rule__Library__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Library__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group_6__1"


    // $ANTLR start "rule__Library__Group_6__1__Impl"
    // InternalJniMap.g:5292:1: rule__Library__Group_6__1__Impl : ( ';' ) ;
    public final void rule__Library__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5296:1: ( ( ';' ) )
            // InternalJniMap.g:5297:1: ( ';' )
            {
            // InternalJniMap.g:5297:1: ( ';' )
            // InternalJniMap.g:5298:1: ';'
            {
             before(grammarAccess.getLibraryAccess().getSemicolonKeyword_6_1()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getLibraryAccess().getSemicolonKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__Group_6__1__Impl"


    // $ANTLR start "rule__LibraryFileName__Group__0"
    // InternalJniMap.g:5315:1: rule__LibraryFileName__Group__0 : rule__LibraryFileName__Group__0__Impl rule__LibraryFileName__Group__1 ;
    public final void rule__LibraryFileName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5319:1: ( rule__LibraryFileName__Group__0__Impl rule__LibraryFileName__Group__1 )
            // InternalJniMap.g:5320:2: rule__LibraryFileName__Group__0__Impl rule__LibraryFileName__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__LibraryFileName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LibraryFileName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LibraryFileName__Group__0"


    // $ANTLR start "rule__LibraryFileName__Group__0__Impl"
    // InternalJniMap.g:5327:1: rule__LibraryFileName__Group__0__Impl : ( ( rule__LibraryFileName__OsAssignment_0 ) ) ;
    public final void rule__LibraryFileName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5331:1: ( ( ( rule__LibraryFileName__OsAssignment_0 ) ) )
            // InternalJniMap.g:5332:1: ( ( rule__LibraryFileName__OsAssignment_0 ) )
            {
            // InternalJniMap.g:5332:1: ( ( rule__LibraryFileName__OsAssignment_0 ) )
            // InternalJniMap.g:5333:1: ( rule__LibraryFileName__OsAssignment_0 )
            {
             before(grammarAccess.getLibraryFileNameAccess().getOsAssignment_0()); 
            // InternalJniMap.g:5334:1: ( rule__LibraryFileName__OsAssignment_0 )
            // InternalJniMap.g:5334:2: rule__LibraryFileName__OsAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__LibraryFileName__OsAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getLibraryFileNameAccess().getOsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LibraryFileName__Group__0__Impl"


    // $ANTLR start "rule__LibraryFileName__Group__1"
    // InternalJniMap.g:5344:1: rule__LibraryFileName__Group__1 : rule__LibraryFileName__Group__1__Impl rule__LibraryFileName__Group__2 ;
    public final void rule__LibraryFileName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5348:1: ( rule__LibraryFileName__Group__1__Impl rule__LibraryFileName__Group__2 )
            // InternalJniMap.g:5349:2: rule__LibraryFileName__Group__1__Impl rule__LibraryFileName__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__LibraryFileName__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LibraryFileName__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LibraryFileName__Group__1"


    // $ANTLR start "rule__LibraryFileName__Group__1__Impl"
    // InternalJniMap.g:5356:1: rule__LibraryFileName__Group__1__Impl : ( '=' ) ;
    public final void rule__LibraryFileName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5360:1: ( ( '=' ) )
            // InternalJniMap.g:5361:1: ( '=' )
            {
            // InternalJniMap.g:5361:1: ( '=' )
            // InternalJniMap.g:5362:1: '='
            {
             before(grammarAccess.getLibraryFileNameAccess().getEqualsSignKeyword_1()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getLibraryFileNameAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LibraryFileName__Group__1__Impl"


    // $ANTLR start "rule__LibraryFileName__Group__2"
    // InternalJniMap.g:5375:1: rule__LibraryFileName__Group__2 : rule__LibraryFileName__Group__2__Impl ;
    public final void rule__LibraryFileName__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5379:1: ( rule__LibraryFileName__Group__2__Impl )
            // InternalJniMap.g:5380:2: rule__LibraryFileName__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LibraryFileName__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LibraryFileName__Group__2"


    // $ANTLR start "rule__LibraryFileName__Group__2__Impl"
    // InternalJniMap.g:5386:1: rule__LibraryFileName__Group__2__Impl : ( ( rule__LibraryFileName__FilenameAssignment_2 ) ) ;
    public final void rule__LibraryFileName__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5390:1: ( ( ( rule__LibraryFileName__FilenameAssignment_2 ) ) )
            // InternalJniMap.g:5391:1: ( ( rule__LibraryFileName__FilenameAssignment_2 ) )
            {
            // InternalJniMap.g:5391:1: ( ( rule__LibraryFileName__FilenameAssignment_2 ) )
            // InternalJniMap.g:5392:1: ( rule__LibraryFileName__FilenameAssignment_2 )
            {
             before(grammarAccess.getLibraryFileNameAccess().getFilenameAssignment_2()); 
            // InternalJniMap.g:5393:1: ( rule__LibraryFileName__FilenameAssignment_2 )
            // InternalJniMap.g:5393:2: rule__LibraryFileName__FilenameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__LibraryFileName__FilenameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getLibraryFileNameAccess().getFilenameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LibraryFileName__Group__2__Impl"


    // $ANTLR start "rule__IncludeDef__Group__0"
    // InternalJniMap.g:5409:1: rule__IncludeDef__Group__0 : rule__IncludeDef__Group__0__Impl rule__IncludeDef__Group__1 ;
    public final void rule__IncludeDef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5413:1: ( rule__IncludeDef__Group__0__Impl rule__IncludeDef__Group__1 )
            // InternalJniMap.g:5414:2: rule__IncludeDef__Group__0__Impl rule__IncludeDef__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__IncludeDef__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IncludeDef__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IncludeDef__Group__0"


    // $ANTLR start "rule__IncludeDef__Group__0__Impl"
    // InternalJniMap.g:5421:1: rule__IncludeDef__Group__0__Impl : ( 'include' ) ;
    public final void rule__IncludeDef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5425:1: ( ( 'include' ) )
            // InternalJniMap.g:5426:1: ( 'include' )
            {
            // InternalJniMap.g:5426:1: ( 'include' )
            // InternalJniMap.g:5427:1: 'include'
            {
             before(grammarAccess.getIncludeDefAccess().getIncludeKeyword_0()); 
            match(input,48,FOLLOW_2); 
             after(grammarAccess.getIncludeDefAccess().getIncludeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IncludeDef__Group__0__Impl"


    // $ANTLR start "rule__IncludeDef__Group__1"
    // InternalJniMap.g:5440:1: rule__IncludeDef__Group__1 : rule__IncludeDef__Group__1__Impl rule__IncludeDef__Group__2 ;
    public final void rule__IncludeDef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5444:1: ( rule__IncludeDef__Group__1__Impl rule__IncludeDef__Group__2 )
            // InternalJniMap.g:5445:2: rule__IncludeDef__Group__1__Impl rule__IncludeDef__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__IncludeDef__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IncludeDef__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IncludeDef__Group__1"


    // $ANTLR start "rule__IncludeDef__Group__1__Impl"
    // InternalJniMap.g:5452:1: rule__IncludeDef__Group__1__Impl : ( ( rule__IncludeDef__IncludeAssignment_1 ) ) ;
    public final void rule__IncludeDef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5456:1: ( ( ( rule__IncludeDef__IncludeAssignment_1 ) ) )
            // InternalJniMap.g:5457:1: ( ( rule__IncludeDef__IncludeAssignment_1 ) )
            {
            // InternalJniMap.g:5457:1: ( ( rule__IncludeDef__IncludeAssignment_1 ) )
            // InternalJniMap.g:5458:1: ( rule__IncludeDef__IncludeAssignment_1 )
            {
             before(grammarAccess.getIncludeDefAccess().getIncludeAssignment_1()); 
            // InternalJniMap.g:5459:1: ( rule__IncludeDef__IncludeAssignment_1 )
            // InternalJniMap.g:5459:2: rule__IncludeDef__IncludeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__IncludeDef__IncludeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getIncludeDefAccess().getIncludeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IncludeDef__Group__1__Impl"


    // $ANTLR start "rule__IncludeDef__Group__2"
    // InternalJniMap.g:5469:1: rule__IncludeDef__Group__2 : rule__IncludeDef__Group__2__Impl ;
    public final void rule__IncludeDef__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5473:1: ( rule__IncludeDef__Group__2__Impl )
            // InternalJniMap.g:5474:2: rule__IncludeDef__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IncludeDef__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IncludeDef__Group__2"


    // $ANTLR start "rule__IncludeDef__Group__2__Impl"
    // InternalJniMap.g:5480:1: rule__IncludeDef__Group__2__Impl : ( ';' ) ;
    public final void rule__IncludeDef__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5484:1: ( ( ';' ) )
            // InternalJniMap.g:5485:1: ( ';' )
            {
            // InternalJniMap.g:5485:1: ( ';' )
            // InternalJniMap.g:5486:1: ';'
            {
             before(grammarAccess.getIncludeDefAccess().getSemicolonKeyword_2()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getIncludeDefAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IncludeDef__Group__2__Impl"


    // $ANTLR start "rule__UserModule__Group__0"
    // InternalJniMap.g:5505:1: rule__UserModule__Group__0 : rule__UserModule__Group__0__Impl rule__UserModule__Group__1 ;
    public final void rule__UserModule__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5509:1: ( rule__UserModule__Group__0__Impl rule__UserModule__Group__1 )
            // InternalJniMap.g:5510:2: rule__UserModule__Group__0__Impl rule__UserModule__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__UserModule__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserModule__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserModule__Group__0"


    // $ANTLR start "rule__UserModule__Group__0__Impl"
    // InternalJniMap.g:5517:1: rule__UserModule__Group__0__Impl : ( 'module' ) ;
    public final void rule__UserModule__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5521:1: ( ( 'module' ) )
            // InternalJniMap.g:5522:1: ( 'module' )
            {
            // InternalJniMap.g:5522:1: ( 'module' )
            // InternalJniMap.g:5523:1: 'module'
            {
             before(grammarAccess.getUserModuleAccess().getModuleKeyword_0()); 
            match(input,49,FOLLOW_2); 
             after(grammarAccess.getUserModuleAccess().getModuleKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserModule__Group__0__Impl"


    // $ANTLR start "rule__UserModule__Group__1"
    // InternalJniMap.g:5536:1: rule__UserModule__Group__1 : rule__UserModule__Group__1__Impl rule__UserModule__Group__2 ;
    public final void rule__UserModule__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5540:1: ( rule__UserModule__Group__1__Impl rule__UserModule__Group__2 )
            // InternalJniMap.g:5541:2: rule__UserModule__Group__1__Impl rule__UserModule__Group__2
            {
            pushFollow(FOLLOW_31);
            rule__UserModule__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserModule__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserModule__Group__1"


    // $ANTLR start "rule__UserModule__Group__1__Impl"
    // InternalJniMap.g:5548:1: rule__UserModule__Group__1__Impl : ( ( rule__UserModule__NameAssignment_1 ) ) ;
    public final void rule__UserModule__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5552:1: ( ( ( rule__UserModule__NameAssignment_1 ) ) )
            // InternalJniMap.g:5553:1: ( ( rule__UserModule__NameAssignment_1 ) )
            {
            // InternalJniMap.g:5553:1: ( ( rule__UserModule__NameAssignment_1 ) )
            // InternalJniMap.g:5554:1: ( rule__UserModule__NameAssignment_1 )
            {
             before(grammarAccess.getUserModuleAccess().getNameAssignment_1()); 
            // InternalJniMap.g:5555:1: ( rule__UserModule__NameAssignment_1 )
            // InternalJniMap.g:5555:2: rule__UserModule__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__UserModule__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getUserModuleAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserModule__Group__1__Impl"


    // $ANTLR start "rule__UserModule__Group__2"
    // InternalJniMap.g:5565:1: rule__UserModule__Group__2 : rule__UserModule__Group__2__Impl rule__UserModule__Group__3 ;
    public final void rule__UserModule__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5569:1: ( rule__UserModule__Group__2__Impl rule__UserModule__Group__3 )
            // InternalJniMap.g:5570:2: rule__UserModule__Group__2__Impl rule__UserModule__Group__3
            {
            pushFollow(FOLLOW_35);
            rule__UserModule__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserModule__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserModule__Group__2"


    // $ANTLR start "rule__UserModule__Group__2__Impl"
    // InternalJniMap.g:5577:1: rule__UserModule__Group__2__Impl : ( '{' ) ;
    public final void rule__UserModule__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5581:1: ( ( '{' ) )
            // InternalJniMap.g:5582:1: ( '{' )
            {
            // InternalJniMap.g:5582:1: ( '{' )
            // InternalJniMap.g:5583:1: '{'
            {
             before(grammarAccess.getUserModuleAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getUserModuleAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserModule__Group__2__Impl"


    // $ANTLR start "rule__UserModule__Group__3"
    // InternalJniMap.g:5596:1: rule__UserModule__Group__3 : rule__UserModule__Group__3__Impl rule__UserModule__Group__4 ;
    public final void rule__UserModule__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5600:1: ( rule__UserModule__Group__3__Impl rule__UserModule__Group__4 )
            // InternalJniMap.g:5601:2: rule__UserModule__Group__3__Impl rule__UserModule__Group__4
            {
            pushFollow(FOLLOW_35);
            rule__UserModule__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserModule__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserModule__Group__3"


    // $ANTLR start "rule__UserModule__Group__3__Impl"
    // InternalJniMap.g:5608:1: rule__UserModule__Group__3__Impl : ( ( rule__UserModule__MethodsAssignment_3 )* ) ;
    public final void rule__UserModule__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5612:1: ( ( ( rule__UserModule__MethodsAssignment_3 )* ) )
            // InternalJniMap.g:5613:1: ( ( rule__UserModule__MethodsAssignment_3 )* )
            {
            // InternalJniMap.g:5613:1: ( ( rule__UserModule__MethodsAssignment_3 )* )
            // InternalJniMap.g:5614:1: ( rule__UserModule__MethodsAssignment_3 )*
            {
             before(grammarAccess.getUserModuleAccess().getMethodsAssignment_3()); 
            // InternalJniMap.g:5615:1: ( rule__UserModule__MethodsAssignment_3 )*
            loop43:
            do {
                int alt43=2;
                int LA43_0 = input.LA(1);

                if ( (LA43_0==RULE_ID||(LA43_0>=11 && LA43_0<=18)||LA43_0==51||LA43_0==53||LA43_0==56||(LA43_0>=68 && LA43_0<=70)) ) {
                    alt43=1;
                }


                switch (alt43) {
            	case 1 :
            	    // InternalJniMap.g:5615:2: rule__UserModule__MethodsAssignment_3
            	    {
            	    pushFollow(FOLLOW_36);
            	    rule__UserModule__MethodsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop43;
                }
            } while (true);

             after(grammarAccess.getUserModuleAccess().getMethodsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserModule__Group__3__Impl"


    // $ANTLR start "rule__UserModule__Group__4"
    // InternalJniMap.g:5625:1: rule__UserModule__Group__4 : rule__UserModule__Group__4__Impl ;
    public final void rule__UserModule__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5629:1: ( rule__UserModule__Group__4__Impl )
            // InternalJniMap.g:5630:2: rule__UserModule__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UserModule__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserModule__Group__4"


    // $ANTLR start "rule__UserModule__Group__4__Impl"
    // InternalJniMap.g:5636:1: rule__UserModule__Group__4__Impl : ( '}' ) ;
    public final void rule__UserModule__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5640:1: ( ( '}' ) )
            // InternalJniMap.g:5641:1: ( '}' )
            {
            // InternalJniMap.g:5641:1: ( '}' )
            // InternalJniMap.g:5642:1: '}'
            {
             before(grammarAccess.getUserModuleAccess().getRightCurlyBracketKeyword_4()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getUserModuleAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserModule__Group__4__Impl"


    // $ANTLR start "rule__CMethod__Group__0"
    // InternalJniMap.g:5665:1: rule__CMethod__Group__0 : rule__CMethod__Group__0__Impl rule__CMethod__Group__1 ;
    public final void rule__CMethod__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5669:1: ( rule__CMethod__Group__0__Impl rule__CMethod__Group__1 )
            // InternalJniMap.g:5670:2: rule__CMethod__Group__0__Impl rule__CMethod__Group__1
            {
            pushFollow(FOLLOW_37);
            rule__CMethod__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CMethod__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__0"


    // $ANTLR start "rule__CMethod__Group__0__Impl"
    // InternalJniMap.g:5677:1: rule__CMethod__Group__0__Impl : ( ( rule__CMethod__Alternatives_0 )? ) ;
    public final void rule__CMethod__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5681:1: ( ( ( rule__CMethod__Alternatives_0 )? ) )
            // InternalJniMap.g:5682:1: ( ( rule__CMethod__Alternatives_0 )? )
            {
            // InternalJniMap.g:5682:1: ( ( rule__CMethod__Alternatives_0 )? )
            // InternalJniMap.g:5683:1: ( rule__CMethod__Alternatives_0 )?
            {
             before(grammarAccess.getCMethodAccess().getAlternatives_0()); 
            // InternalJniMap.g:5684:1: ( rule__CMethod__Alternatives_0 )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( ((LA44_0>=11 && LA44_0<=12)) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalJniMap.g:5684:2: rule__CMethod__Alternatives_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CMethod__Alternatives_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCMethodAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__0__Impl"


    // $ANTLR start "rule__CMethod__Group__1"
    // InternalJniMap.g:5694:1: rule__CMethod__Group__1 : rule__CMethod__Group__1__Impl rule__CMethod__Group__2 ;
    public final void rule__CMethod__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5698:1: ( rule__CMethod__Group__1__Impl rule__CMethod__Group__2 )
            // InternalJniMap.g:5699:2: rule__CMethod__Group__1__Impl rule__CMethod__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__CMethod__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CMethod__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__1"


    // $ANTLR start "rule__CMethod__Group__1__Impl"
    // InternalJniMap.g:5706:1: rule__CMethod__Group__1__Impl : ( ( rule__CMethod__ResAssignment_1 ) ) ;
    public final void rule__CMethod__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5710:1: ( ( ( rule__CMethod__ResAssignment_1 ) ) )
            // InternalJniMap.g:5711:1: ( ( rule__CMethod__ResAssignment_1 ) )
            {
            // InternalJniMap.g:5711:1: ( ( rule__CMethod__ResAssignment_1 ) )
            // InternalJniMap.g:5712:1: ( rule__CMethod__ResAssignment_1 )
            {
             before(grammarAccess.getCMethodAccess().getResAssignment_1()); 
            // InternalJniMap.g:5713:1: ( rule__CMethod__ResAssignment_1 )
            // InternalJniMap.g:5713:2: rule__CMethod__ResAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__CMethod__ResAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCMethodAccess().getResAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__1__Impl"


    // $ANTLR start "rule__CMethod__Group__2"
    // InternalJniMap.g:5723:1: rule__CMethod__Group__2 : rule__CMethod__Group__2__Impl rule__CMethod__Group__3 ;
    public final void rule__CMethod__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5727:1: ( rule__CMethod__Group__2__Impl rule__CMethod__Group__3 )
            // InternalJniMap.g:5728:2: rule__CMethod__Group__2__Impl rule__CMethod__Group__3
            {
            pushFollow(FOLLOW_38);
            rule__CMethod__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CMethod__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__2"


    // $ANTLR start "rule__CMethod__Group__2__Impl"
    // InternalJniMap.g:5735:1: rule__CMethod__Group__2__Impl : ( ( rule__CMethod__NameAssignment_2 ) ) ;
    public final void rule__CMethod__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5739:1: ( ( ( rule__CMethod__NameAssignment_2 ) ) )
            // InternalJniMap.g:5740:1: ( ( rule__CMethod__NameAssignment_2 ) )
            {
            // InternalJniMap.g:5740:1: ( ( rule__CMethod__NameAssignment_2 ) )
            // InternalJniMap.g:5741:1: ( rule__CMethod__NameAssignment_2 )
            {
             before(grammarAccess.getCMethodAccess().getNameAssignment_2()); 
            // InternalJniMap.g:5742:1: ( rule__CMethod__NameAssignment_2 )
            // InternalJniMap.g:5742:2: rule__CMethod__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__CMethod__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCMethodAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__2__Impl"


    // $ANTLR start "rule__CMethod__Group__3"
    // InternalJniMap.g:5752:1: rule__CMethod__Group__3 : rule__CMethod__Group__3__Impl rule__CMethod__Group__4 ;
    public final void rule__CMethod__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5756:1: ( rule__CMethod__Group__3__Impl rule__CMethod__Group__4 )
            // InternalJniMap.g:5757:2: rule__CMethod__Group__3__Impl rule__CMethod__Group__4
            {
            pushFollow(FOLLOW_39);
            rule__CMethod__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CMethod__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__3"


    // $ANTLR start "rule__CMethod__Group__3__Impl"
    // InternalJniMap.g:5764:1: rule__CMethod__Group__3__Impl : ( '(' ) ;
    public final void rule__CMethod__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5768:1: ( ( '(' ) )
            // InternalJniMap.g:5769:1: ( '(' )
            {
            // InternalJniMap.g:5769:1: ( '(' )
            // InternalJniMap.g:5770:1: '('
            {
             before(grammarAccess.getCMethodAccess().getLeftParenthesisKeyword_3()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getCMethodAccess().getLeftParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__3__Impl"


    // $ANTLR start "rule__CMethod__Group__4"
    // InternalJniMap.g:5783:1: rule__CMethod__Group__4 : rule__CMethod__Group__4__Impl rule__CMethod__Group__5 ;
    public final void rule__CMethod__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5787:1: ( rule__CMethod__Group__4__Impl rule__CMethod__Group__5 )
            // InternalJniMap.g:5788:2: rule__CMethod__Group__4__Impl rule__CMethod__Group__5
            {
            pushFollow(FOLLOW_39);
            rule__CMethod__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CMethod__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__4"


    // $ANTLR start "rule__CMethod__Group__4__Impl"
    // InternalJniMap.g:5795:1: rule__CMethod__Group__4__Impl : ( ( rule__CMethod__Group_4__0 )? ) ;
    public final void rule__CMethod__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5799:1: ( ( ( rule__CMethod__Group_4__0 )? ) )
            // InternalJniMap.g:5800:1: ( ( rule__CMethod__Group_4__0 )? )
            {
            // InternalJniMap.g:5800:1: ( ( rule__CMethod__Group_4__0 )? )
            // InternalJniMap.g:5801:1: ( rule__CMethod__Group_4__0 )?
            {
             before(grammarAccess.getCMethodAccess().getGroup_4()); 
            // InternalJniMap.g:5802:1: ( rule__CMethod__Group_4__0 )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==RULE_ID||(LA45_0>=13 && LA45_0<=18)||LA45_0==51||LA45_0==53||LA45_0==56||(LA45_0>=58 && LA45_0<=60)||(LA45_0>=68 && LA45_0<=70)) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalJniMap.g:5802:2: rule__CMethod__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CMethod__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCMethodAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__4__Impl"


    // $ANTLR start "rule__CMethod__Group__5"
    // InternalJniMap.g:5812:1: rule__CMethod__Group__5 : rule__CMethod__Group__5__Impl rule__CMethod__Group__6 ;
    public final void rule__CMethod__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5816:1: ( rule__CMethod__Group__5__Impl rule__CMethod__Group__6 )
            // InternalJniMap.g:5817:2: rule__CMethod__Group__5__Impl rule__CMethod__Group__6
            {
            pushFollow(FOLLOW_7);
            rule__CMethod__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CMethod__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__5"


    // $ANTLR start "rule__CMethod__Group__5__Impl"
    // InternalJniMap.g:5824:1: rule__CMethod__Group__5__Impl : ( ')' ) ;
    public final void rule__CMethod__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5828:1: ( ( ')' ) )
            // InternalJniMap.g:5829:1: ( ')' )
            {
            // InternalJniMap.g:5829:1: ( ')' )
            // InternalJniMap.g:5830:1: ')'
            {
             before(grammarAccess.getCMethodAccess().getRightParenthesisKeyword_5()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getCMethodAccess().getRightParenthesisKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__5__Impl"


    // $ANTLR start "rule__CMethod__Group__6"
    // InternalJniMap.g:5843:1: rule__CMethod__Group__6 : rule__CMethod__Group__6__Impl ;
    public final void rule__CMethod__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5847:1: ( rule__CMethod__Group__6__Impl )
            // InternalJniMap.g:5848:2: rule__CMethod__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CMethod__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__6"


    // $ANTLR start "rule__CMethod__Group__6__Impl"
    // InternalJniMap.g:5854:1: rule__CMethod__Group__6__Impl : ( ';' ) ;
    public final void rule__CMethod__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5858:1: ( ( ';' ) )
            // InternalJniMap.g:5859:1: ( ';' )
            {
            // InternalJniMap.g:5859:1: ( ';' )
            // InternalJniMap.g:5860:1: ';'
            {
             before(grammarAccess.getCMethodAccess().getSemicolonKeyword_6()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getCMethodAccess().getSemicolonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group__6__Impl"


    // $ANTLR start "rule__CMethod__Group_4__0"
    // InternalJniMap.g:5887:1: rule__CMethod__Group_4__0 : rule__CMethod__Group_4__0__Impl rule__CMethod__Group_4__1 ;
    public final void rule__CMethod__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5891:1: ( rule__CMethod__Group_4__0__Impl rule__CMethod__Group_4__1 )
            // InternalJniMap.g:5892:2: rule__CMethod__Group_4__0__Impl rule__CMethod__Group_4__1
            {
            pushFollow(FOLLOW_27);
            rule__CMethod__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CMethod__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group_4__0"


    // $ANTLR start "rule__CMethod__Group_4__0__Impl"
    // InternalJniMap.g:5899:1: rule__CMethod__Group_4__0__Impl : ( ( rule__CMethod__ParamsAssignment_4_0 ) ) ;
    public final void rule__CMethod__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5903:1: ( ( ( rule__CMethod__ParamsAssignment_4_0 ) ) )
            // InternalJniMap.g:5904:1: ( ( rule__CMethod__ParamsAssignment_4_0 ) )
            {
            // InternalJniMap.g:5904:1: ( ( rule__CMethod__ParamsAssignment_4_0 ) )
            // InternalJniMap.g:5905:1: ( rule__CMethod__ParamsAssignment_4_0 )
            {
             before(grammarAccess.getCMethodAccess().getParamsAssignment_4_0()); 
            // InternalJniMap.g:5906:1: ( rule__CMethod__ParamsAssignment_4_0 )
            // InternalJniMap.g:5906:2: rule__CMethod__ParamsAssignment_4_0
            {
            pushFollow(FOLLOW_2);
            rule__CMethod__ParamsAssignment_4_0();

            state._fsp--;


            }

             after(grammarAccess.getCMethodAccess().getParamsAssignment_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group_4__0__Impl"


    // $ANTLR start "rule__CMethod__Group_4__1"
    // InternalJniMap.g:5916:1: rule__CMethod__Group_4__1 : rule__CMethod__Group_4__1__Impl ;
    public final void rule__CMethod__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5920:1: ( rule__CMethod__Group_4__1__Impl )
            // InternalJniMap.g:5921:2: rule__CMethod__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CMethod__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group_4__1"


    // $ANTLR start "rule__CMethod__Group_4__1__Impl"
    // InternalJniMap.g:5927:1: rule__CMethod__Group_4__1__Impl : ( ( rule__CMethod__Group_4_1__0 )* ) ;
    public final void rule__CMethod__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5931:1: ( ( ( rule__CMethod__Group_4_1__0 )* ) )
            // InternalJniMap.g:5932:1: ( ( rule__CMethod__Group_4_1__0 )* )
            {
            // InternalJniMap.g:5932:1: ( ( rule__CMethod__Group_4_1__0 )* )
            // InternalJniMap.g:5933:1: ( rule__CMethod__Group_4_1__0 )*
            {
             before(grammarAccess.getCMethodAccess().getGroup_4_1()); 
            // InternalJniMap.g:5934:1: ( rule__CMethod__Group_4_1__0 )*
            loop46:
            do {
                int alt46=2;
                int LA46_0 = input.LA(1);

                if ( (LA46_0==33) ) {
                    alt46=1;
                }


                switch (alt46) {
            	case 1 :
            	    // InternalJniMap.g:5934:2: rule__CMethod__Group_4_1__0
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__CMethod__Group_4_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop46;
                }
            } while (true);

             after(grammarAccess.getCMethodAccess().getGroup_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group_4__1__Impl"


    // $ANTLR start "rule__CMethod__Group_4_1__0"
    // InternalJniMap.g:5948:1: rule__CMethod__Group_4_1__0 : rule__CMethod__Group_4_1__0__Impl rule__CMethod__Group_4_1__1 ;
    public final void rule__CMethod__Group_4_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5952:1: ( rule__CMethod__Group_4_1__0__Impl rule__CMethod__Group_4_1__1 )
            // InternalJniMap.g:5953:2: rule__CMethod__Group_4_1__0__Impl rule__CMethod__Group_4_1__1
            {
            pushFollow(FOLLOW_40);
            rule__CMethod__Group_4_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CMethod__Group_4_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group_4_1__0"


    // $ANTLR start "rule__CMethod__Group_4_1__0__Impl"
    // InternalJniMap.g:5960:1: rule__CMethod__Group_4_1__0__Impl : ( ',' ) ;
    public final void rule__CMethod__Group_4_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5964:1: ( ( ',' ) )
            // InternalJniMap.g:5965:1: ( ',' )
            {
            // InternalJniMap.g:5965:1: ( ',' )
            // InternalJniMap.g:5966:1: ','
            {
             before(grammarAccess.getCMethodAccess().getCommaKeyword_4_1_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getCMethodAccess().getCommaKeyword_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group_4_1__0__Impl"


    // $ANTLR start "rule__CMethod__Group_4_1__1"
    // InternalJniMap.g:5979:1: rule__CMethod__Group_4_1__1 : rule__CMethod__Group_4_1__1__Impl ;
    public final void rule__CMethod__Group_4_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5983:1: ( rule__CMethod__Group_4_1__1__Impl )
            // InternalJniMap.g:5984:2: rule__CMethod__Group_4_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CMethod__Group_4_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group_4_1__1"


    // $ANTLR start "rule__CMethod__Group_4_1__1__Impl"
    // InternalJniMap.g:5990:1: rule__CMethod__Group_4_1__1__Impl : ( ( rule__CMethod__ParamsAssignment_4_1_1 ) ) ;
    public final void rule__CMethod__Group_4_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:5994:1: ( ( ( rule__CMethod__ParamsAssignment_4_1_1 ) ) )
            // InternalJniMap.g:5995:1: ( ( rule__CMethod__ParamsAssignment_4_1_1 ) )
            {
            // InternalJniMap.g:5995:1: ( ( rule__CMethod__ParamsAssignment_4_1_1 ) )
            // InternalJniMap.g:5996:1: ( rule__CMethod__ParamsAssignment_4_1_1 )
            {
             before(grammarAccess.getCMethodAccess().getParamsAssignment_4_1_1()); 
            // InternalJniMap.g:5997:1: ( rule__CMethod__ParamsAssignment_4_1_1 )
            // InternalJniMap.g:5997:2: rule__CMethod__ParamsAssignment_4_1_1
            {
            pushFollow(FOLLOW_2);
            rule__CMethod__ParamsAssignment_4_1_1();

            state._fsp--;


            }

             after(grammarAccess.getCMethodAccess().getParamsAssignment_4_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__Group_4_1__1__Impl"


    // $ANTLR start "rule__TypeAliasDeclaration__Group__0"
    // InternalJniMap.g:6011:1: rule__TypeAliasDeclaration__Group__0 : rule__TypeAliasDeclaration__Group__0__Impl rule__TypeAliasDeclaration__Group__1 ;
    public final void rule__TypeAliasDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6015:1: ( rule__TypeAliasDeclaration__Group__0__Impl rule__TypeAliasDeclaration__Group__1 )
            // InternalJniMap.g:6016:2: rule__TypeAliasDeclaration__Group__0__Impl rule__TypeAliasDeclaration__Group__1
            {
            pushFollow(FOLLOW_37);
            rule__TypeAliasDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeAliasDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeAliasDeclaration__Group__0"


    // $ANTLR start "rule__TypeAliasDeclaration__Group__0__Impl"
    // InternalJniMap.g:6023:1: rule__TypeAliasDeclaration__Group__0__Impl : ( 'typedef' ) ;
    public final void rule__TypeAliasDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6027:1: ( ( 'typedef' ) )
            // InternalJniMap.g:6028:1: ( 'typedef' )
            {
            // InternalJniMap.g:6028:1: ( 'typedef' )
            // InternalJniMap.g:6029:1: 'typedef'
            {
             before(grammarAccess.getTypeAliasDeclarationAccess().getTypedefKeyword_0()); 
            match(input,50,FOLLOW_2); 
             after(grammarAccess.getTypeAliasDeclarationAccess().getTypedefKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeAliasDeclaration__Group__0__Impl"


    // $ANTLR start "rule__TypeAliasDeclaration__Group__1"
    // InternalJniMap.g:6042:1: rule__TypeAliasDeclaration__Group__1 : rule__TypeAliasDeclaration__Group__1__Impl rule__TypeAliasDeclaration__Group__2 ;
    public final void rule__TypeAliasDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6046:1: ( rule__TypeAliasDeclaration__Group__1__Impl rule__TypeAliasDeclaration__Group__2 )
            // InternalJniMap.g:6047:2: rule__TypeAliasDeclaration__Group__1__Impl rule__TypeAliasDeclaration__Group__2
            {
            pushFollow(FOLLOW_37);
            rule__TypeAliasDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeAliasDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeAliasDeclaration__Group__1"


    // $ANTLR start "rule__TypeAliasDeclaration__Group__1__Impl"
    // InternalJniMap.g:6054:1: rule__TypeAliasDeclaration__Group__1__Impl : ( ( rule__TypeAliasDeclaration__TypeAssignment_1 )? ) ;
    public final void rule__TypeAliasDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6058:1: ( ( ( rule__TypeAliasDeclaration__TypeAssignment_1 )? ) )
            // InternalJniMap.g:6059:1: ( ( rule__TypeAliasDeclaration__TypeAssignment_1 )? )
            {
            // InternalJniMap.g:6059:1: ( ( rule__TypeAliasDeclaration__TypeAssignment_1 )? )
            // InternalJniMap.g:6060:1: ( rule__TypeAliasDeclaration__TypeAssignment_1 )?
            {
             before(grammarAccess.getTypeAliasDeclarationAccess().getTypeAssignment_1()); 
            // InternalJniMap.g:6061:1: ( rule__TypeAliasDeclaration__TypeAssignment_1 )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( ((LA47_0>=13 && LA47_0<=18)||LA47_0==51||LA47_0==53||LA47_0==56||(LA47_0>=68 && LA47_0<=70)) ) {
                alt47=1;
            }
            else if ( (LA47_0==RULE_ID) ) {
                int LA47_2 = input.LA(2);

                if ( (LA47_2==RULE_ID||LA47_2==31||LA47_2==71||LA47_2==73) ) {
                    alt47=1;
                }
            }
            switch (alt47) {
                case 1 :
                    // InternalJniMap.g:6061:2: rule__TypeAliasDeclaration__TypeAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__TypeAliasDeclaration__TypeAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTypeAliasDeclarationAccess().getTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeAliasDeclaration__Group__1__Impl"


    // $ANTLR start "rule__TypeAliasDeclaration__Group__2"
    // InternalJniMap.g:6071:1: rule__TypeAliasDeclaration__Group__2 : rule__TypeAliasDeclaration__Group__2__Impl ;
    public final void rule__TypeAliasDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6075:1: ( rule__TypeAliasDeclaration__Group__2__Impl )
            // InternalJniMap.g:6076:2: rule__TypeAliasDeclaration__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeAliasDeclaration__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeAliasDeclaration__Group__2"


    // $ANTLR start "rule__TypeAliasDeclaration__Group__2__Impl"
    // InternalJniMap.g:6082:1: rule__TypeAliasDeclaration__Group__2__Impl : ( ( rule__TypeAliasDeclaration__NameAssignment_2 ) ) ;
    public final void rule__TypeAliasDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6086:1: ( ( ( rule__TypeAliasDeclaration__NameAssignment_2 ) ) )
            // InternalJniMap.g:6087:1: ( ( rule__TypeAliasDeclaration__NameAssignment_2 ) )
            {
            // InternalJniMap.g:6087:1: ( ( rule__TypeAliasDeclaration__NameAssignment_2 ) )
            // InternalJniMap.g:6088:1: ( rule__TypeAliasDeclaration__NameAssignment_2 )
            {
             before(grammarAccess.getTypeAliasDeclarationAccess().getNameAssignment_2()); 
            // InternalJniMap.g:6089:1: ( rule__TypeAliasDeclaration__NameAssignment_2 )
            // InternalJniMap.g:6089:2: rule__TypeAliasDeclaration__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__TypeAliasDeclaration__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getTypeAliasDeclarationAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeAliasDeclaration__Group__2__Impl"


    // $ANTLR start "rule__StructDeclaration__Group__0"
    // InternalJniMap.g:6105:1: rule__StructDeclaration__Group__0 : rule__StructDeclaration__Group__0__Impl rule__StructDeclaration__Group__1 ;
    public final void rule__StructDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6109:1: ( rule__StructDeclaration__Group__0__Impl rule__StructDeclaration__Group__1 )
            // InternalJniMap.g:6110:2: rule__StructDeclaration__Group__0__Impl rule__StructDeclaration__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__StructDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group__0"


    // $ANTLR start "rule__StructDeclaration__Group__0__Impl"
    // InternalJniMap.g:6117:1: rule__StructDeclaration__Group__0__Impl : ( 'struct' ) ;
    public final void rule__StructDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6121:1: ( ( 'struct' ) )
            // InternalJniMap.g:6122:1: ( 'struct' )
            {
            // InternalJniMap.g:6122:1: ( 'struct' )
            // InternalJniMap.g:6123:1: 'struct'
            {
             before(grammarAccess.getStructDeclarationAccess().getStructKeyword_0()); 
            match(input,51,FOLLOW_2); 
             after(grammarAccess.getStructDeclarationAccess().getStructKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group__0__Impl"


    // $ANTLR start "rule__StructDeclaration__Group__1"
    // InternalJniMap.g:6136:1: rule__StructDeclaration__Group__1 : rule__StructDeclaration__Group__1__Impl rule__StructDeclaration__Group__2 ;
    public final void rule__StructDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6140:1: ( rule__StructDeclaration__Group__1__Impl rule__StructDeclaration__Group__2 )
            // InternalJniMap.g:6141:2: rule__StructDeclaration__Group__1__Impl rule__StructDeclaration__Group__2
            {
            pushFollow(FOLLOW_31);
            rule__StructDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group__1"


    // $ANTLR start "rule__StructDeclaration__Group__1__Impl"
    // InternalJniMap.g:6148:1: rule__StructDeclaration__Group__1__Impl : ( ( rule__StructDeclaration__NameAssignment_1 ) ) ;
    public final void rule__StructDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6152:1: ( ( ( rule__StructDeclaration__NameAssignment_1 ) ) )
            // InternalJniMap.g:6153:1: ( ( rule__StructDeclaration__NameAssignment_1 ) )
            {
            // InternalJniMap.g:6153:1: ( ( rule__StructDeclaration__NameAssignment_1 ) )
            // InternalJniMap.g:6154:1: ( rule__StructDeclaration__NameAssignment_1 )
            {
             before(grammarAccess.getStructDeclarationAccess().getNameAssignment_1()); 
            // InternalJniMap.g:6155:1: ( rule__StructDeclaration__NameAssignment_1 )
            // InternalJniMap.g:6155:2: rule__StructDeclaration__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__StructDeclaration__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStructDeclarationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group__1__Impl"


    // $ANTLR start "rule__StructDeclaration__Group__2"
    // InternalJniMap.g:6165:1: rule__StructDeclaration__Group__2 : rule__StructDeclaration__Group__2__Impl ;
    public final void rule__StructDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6169:1: ( rule__StructDeclaration__Group__2__Impl )
            // InternalJniMap.g:6170:2: rule__StructDeclaration__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StructDeclaration__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group__2"


    // $ANTLR start "rule__StructDeclaration__Group__2__Impl"
    // InternalJniMap.g:6176:1: rule__StructDeclaration__Group__2__Impl : ( ( rule__StructDeclaration__Group_2__0 )? ) ;
    public final void rule__StructDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6180:1: ( ( ( rule__StructDeclaration__Group_2__0 )? ) )
            // InternalJniMap.g:6181:1: ( ( rule__StructDeclaration__Group_2__0 )? )
            {
            // InternalJniMap.g:6181:1: ( ( rule__StructDeclaration__Group_2__0 )? )
            // InternalJniMap.g:6182:1: ( rule__StructDeclaration__Group_2__0 )?
            {
             before(grammarAccess.getStructDeclarationAccess().getGroup_2()); 
            // InternalJniMap.g:6183:1: ( rule__StructDeclaration__Group_2__0 )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==45) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalJniMap.g:6183:2: rule__StructDeclaration__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__StructDeclaration__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStructDeclarationAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group__2__Impl"


    // $ANTLR start "rule__StructDeclaration__Group_2__0"
    // InternalJniMap.g:6199:1: rule__StructDeclaration__Group_2__0 : rule__StructDeclaration__Group_2__0__Impl rule__StructDeclaration__Group_2__1 ;
    public final void rule__StructDeclaration__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6203:1: ( rule__StructDeclaration__Group_2__0__Impl rule__StructDeclaration__Group_2__1 )
            // InternalJniMap.g:6204:2: rule__StructDeclaration__Group_2__0__Impl rule__StructDeclaration__Group_2__1
            {
            pushFollow(FOLLOW_41);
            rule__StructDeclaration__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructDeclaration__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group_2__0"


    // $ANTLR start "rule__StructDeclaration__Group_2__0__Impl"
    // InternalJniMap.g:6211:1: rule__StructDeclaration__Group_2__0__Impl : ( '{' ) ;
    public final void rule__StructDeclaration__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6215:1: ( ( '{' ) )
            // InternalJniMap.g:6216:1: ( '{' )
            {
            // InternalJniMap.g:6216:1: ( '{' )
            // InternalJniMap.g:6217:1: '{'
            {
             before(grammarAccess.getStructDeclarationAccess().getLeftCurlyBracketKeyword_2_0()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getStructDeclarationAccess().getLeftCurlyBracketKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group_2__0__Impl"


    // $ANTLR start "rule__StructDeclaration__Group_2__1"
    // InternalJniMap.g:6230:1: rule__StructDeclaration__Group_2__1 : rule__StructDeclaration__Group_2__1__Impl rule__StructDeclaration__Group_2__2 ;
    public final void rule__StructDeclaration__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6234:1: ( rule__StructDeclaration__Group_2__1__Impl rule__StructDeclaration__Group_2__2 )
            // InternalJniMap.g:6235:2: rule__StructDeclaration__Group_2__1__Impl rule__StructDeclaration__Group_2__2
            {
            pushFollow(FOLLOW_33);
            rule__StructDeclaration__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructDeclaration__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group_2__1"


    // $ANTLR start "rule__StructDeclaration__Group_2__1__Impl"
    // InternalJniMap.g:6242:1: rule__StructDeclaration__Group_2__1__Impl : ( ( ( rule__StructDeclaration__Group_2_1__0 ) ) ( ( rule__StructDeclaration__Group_2_1__0 )* ) ) ;
    public final void rule__StructDeclaration__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6246:1: ( ( ( ( rule__StructDeclaration__Group_2_1__0 ) ) ( ( rule__StructDeclaration__Group_2_1__0 )* ) ) )
            // InternalJniMap.g:6247:1: ( ( ( rule__StructDeclaration__Group_2_1__0 ) ) ( ( rule__StructDeclaration__Group_2_1__0 )* ) )
            {
            // InternalJniMap.g:6247:1: ( ( ( rule__StructDeclaration__Group_2_1__0 ) ) ( ( rule__StructDeclaration__Group_2_1__0 )* ) )
            // InternalJniMap.g:6248:1: ( ( rule__StructDeclaration__Group_2_1__0 ) ) ( ( rule__StructDeclaration__Group_2_1__0 )* )
            {
            // InternalJniMap.g:6248:1: ( ( rule__StructDeclaration__Group_2_1__0 ) )
            // InternalJniMap.g:6249:1: ( rule__StructDeclaration__Group_2_1__0 )
            {
             before(grammarAccess.getStructDeclarationAccess().getGroup_2_1()); 
            // InternalJniMap.g:6250:1: ( rule__StructDeclaration__Group_2_1__0 )
            // InternalJniMap.g:6250:2: rule__StructDeclaration__Group_2_1__0
            {
            pushFollow(FOLLOW_42);
            rule__StructDeclaration__Group_2_1__0();

            state._fsp--;


            }

             after(grammarAccess.getStructDeclarationAccess().getGroup_2_1()); 

            }

            // InternalJniMap.g:6253:1: ( ( rule__StructDeclaration__Group_2_1__0 )* )
            // InternalJniMap.g:6254:1: ( rule__StructDeclaration__Group_2_1__0 )*
            {
             before(grammarAccess.getStructDeclarationAccess().getGroup_2_1()); 
            // InternalJniMap.g:6255:1: ( rule__StructDeclaration__Group_2_1__0 )*
            loop49:
            do {
                int alt49=2;
                int LA49_0 = input.LA(1);

                if ( (LA49_0==RULE_ID||(LA49_0>=13 && LA49_0<=18)||LA49_0==51||LA49_0==53||LA49_0==56||(LA49_0>=68 && LA49_0<=70)||LA49_0==74) ) {
                    alt49=1;
                }


                switch (alt49) {
            	case 1 :
            	    // InternalJniMap.g:6255:2: rule__StructDeclaration__Group_2_1__0
            	    {
            	    pushFollow(FOLLOW_42);
            	    rule__StructDeclaration__Group_2_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop49;
                }
            } while (true);

             after(grammarAccess.getStructDeclarationAccess().getGroup_2_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group_2__1__Impl"


    // $ANTLR start "rule__StructDeclaration__Group_2__2"
    // InternalJniMap.g:6266:1: rule__StructDeclaration__Group_2__2 : rule__StructDeclaration__Group_2__2__Impl ;
    public final void rule__StructDeclaration__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6270:1: ( rule__StructDeclaration__Group_2__2__Impl )
            // InternalJniMap.g:6271:2: rule__StructDeclaration__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StructDeclaration__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group_2__2"


    // $ANTLR start "rule__StructDeclaration__Group_2__2__Impl"
    // InternalJniMap.g:6277:1: rule__StructDeclaration__Group_2__2__Impl : ( '}' ) ;
    public final void rule__StructDeclaration__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6281:1: ( ( '}' ) )
            // InternalJniMap.g:6282:1: ( '}' )
            {
            // InternalJniMap.g:6282:1: ( '}' )
            // InternalJniMap.g:6283:1: '}'
            {
             before(grammarAccess.getStructDeclarationAccess().getRightCurlyBracketKeyword_2_2()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getStructDeclarationAccess().getRightCurlyBracketKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group_2__2__Impl"


    // $ANTLR start "rule__StructDeclaration__Group_2_1__0"
    // InternalJniMap.g:6302:1: rule__StructDeclaration__Group_2_1__0 : rule__StructDeclaration__Group_2_1__0__Impl rule__StructDeclaration__Group_2_1__1 ;
    public final void rule__StructDeclaration__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6306:1: ( rule__StructDeclaration__Group_2_1__0__Impl rule__StructDeclaration__Group_2_1__1 )
            // InternalJniMap.g:6307:2: rule__StructDeclaration__Group_2_1__0__Impl rule__StructDeclaration__Group_2_1__1
            {
            pushFollow(FOLLOW_7);
            rule__StructDeclaration__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructDeclaration__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group_2_1__0"


    // $ANTLR start "rule__StructDeclaration__Group_2_1__0__Impl"
    // InternalJniMap.g:6314:1: rule__StructDeclaration__Group_2_1__0__Impl : ( ( rule__StructDeclaration__FieldsAssignment_2_1_0 ) ) ;
    public final void rule__StructDeclaration__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6318:1: ( ( ( rule__StructDeclaration__FieldsAssignment_2_1_0 ) ) )
            // InternalJniMap.g:6319:1: ( ( rule__StructDeclaration__FieldsAssignment_2_1_0 ) )
            {
            // InternalJniMap.g:6319:1: ( ( rule__StructDeclaration__FieldsAssignment_2_1_0 ) )
            // InternalJniMap.g:6320:1: ( rule__StructDeclaration__FieldsAssignment_2_1_0 )
            {
             before(grammarAccess.getStructDeclarationAccess().getFieldsAssignment_2_1_0()); 
            // InternalJniMap.g:6321:1: ( rule__StructDeclaration__FieldsAssignment_2_1_0 )
            // InternalJniMap.g:6321:2: rule__StructDeclaration__FieldsAssignment_2_1_0
            {
            pushFollow(FOLLOW_2);
            rule__StructDeclaration__FieldsAssignment_2_1_0();

            state._fsp--;


            }

             after(grammarAccess.getStructDeclarationAccess().getFieldsAssignment_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group_2_1__0__Impl"


    // $ANTLR start "rule__StructDeclaration__Group_2_1__1"
    // InternalJniMap.g:6331:1: rule__StructDeclaration__Group_2_1__1 : rule__StructDeclaration__Group_2_1__1__Impl ;
    public final void rule__StructDeclaration__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6335:1: ( rule__StructDeclaration__Group_2_1__1__Impl )
            // InternalJniMap.g:6336:2: rule__StructDeclaration__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StructDeclaration__Group_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group_2_1__1"


    // $ANTLR start "rule__StructDeclaration__Group_2_1__1__Impl"
    // InternalJniMap.g:6342:1: rule__StructDeclaration__Group_2_1__1__Impl : ( ';' ) ;
    public final void rule__StructDeclaration__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6346:1: ( ( ';' ) )
            // InternalJniMap.g:6347:1: ( ';' )
            {
            // InternalJniMap.g:6347:1: ( ';' )
            // InternalJniMap.g:6348:1: ';'
            {
             before(grammarAccess.getStructDeclarationAccess().getSemicolonKeyword_2_1_1()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getStructDeclarationAccess().getSemicolonKeyword_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__Group_2_1__1__Impl"


    // $ANTLR start "rule__ClassDeclaration__Group__0"
    // InternalJniMap.g:6365:1: rule__ClassDeclaration__Group__0 : rule__ClassDeclaration__Group__0__Impl rule__ClassDeclaration__Group__1 ;
    public final void rule__ClassDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6369:1: ( rule__ClassDeclaration__Group__0__Impl rule__ClassDeclaration__Group__1 )
            // InternalJniMap.g:6370:2: rule__ClassDeclaration__Group__0__Impl rule__ClassDeclaration__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__ClassDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group__0"


    // $ANTLR start "rule__ClassDeclaration__Group__0__Impl"
    // InternalJniMap.g:6377:1: rule__ClassDeclaration__Group__0__Impl : ( 'class' ) ;
    public final void rule__ClassDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6381:1: ( ( 'class' ) )
            // InternalJniMap.g:6382:1: ( 'class' )
            {
            // InternalJniMap.g:6382:1: ( 'class' )
            // InternalJniMap.g:6383:1: 'class'
            {
             before(grammarAccess.getClassDeclarationAccess().getClassKeyword_0()); 
            match(input,52,FOLLOW_2); 
             after(grammarAccess.getClassDeclarationAccess().getClassKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group__0__Impl"


    // $ANTLR start "rule__ClassDeclaration__Group__1"
    // InternalJniMap.g:6396:1: rule__ClassDeclaration__Group__1 : rule__ClassDeclaration__Group__1__Impl rule__ClassDeclaration__Group__2 ;
    public final void rule__ClassDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6400:1: ( rule__ClassDeclaration__Group__1__Impl rule__ClassDeclaration__Group__2 )
            // InternalJniMap.g:6401:2: rule__ClassDeclaration__Group__1__Impl rule__ClassDeclaration__Group__2
            {
            pushFollow(FOLLOW_31);
            rule__ClassDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group__1"


    // $ANTLR start "rule__ClassDeclaration__Group__1__Impl"
    // InternalJniMap.g:6408:1: rule__ClassDeclaration__Group__1__Impl : ( ( rule__ClassDeclaration__NameAssignment_1 ) ) ;
    public final void rule__ClassDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6412:1: ( ( ( rule__ClassDeclaration__NameAssignment_1 ) ) )
            // InternalJniMap.g:6413:1: ( ( rule__ClassDeclaration__NameAssignment_1 ) )
            {
            // InternalJniMap.g:6413:1: ( ( rule__ClassDeclaration__NameAssignment_1 ) )
            // InternalJniMap.g:6414:1: ( rule__ClassDeclaration__NameAssignment_1 )
            {
             before(grammarAccess.getClassDeclarationAccess().getNameAssignment_1()); 
            // InternalJniMap.g:6415:1: ( rule__ClassDeclaration__NameAssignment_1 )
            // InternalJniMap.g:6415:2: rule__ClassDeclaration__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ClassDeclaration__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getClassDeclarationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group__1__Impl"


    // $ANTLR start "rule__ClassDeclaration__Group__2"
    // InternalJniMap.g:6425:1: rule__ClassDeclaration__Group__2 : rule__ClassDeclaration__Group__2__Impl ;
    public final void rule__ClassDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6429:1: ( rule__ClassDeclaration__Group__2__Impl )
            // InternalJniMap.g:6430:2: rule__ClassDeclaration__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ClassDeclaration__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group__2"


    // $ANTLR start "rule__ClassDeclaration__Group__2__Impl"
    // InternalJniMap.g:6436:1: rule__ClassDeclaration__Group__2__Impl : ( ( rule__ClassDeclaration__Group_2__0 )? ) ;
    public final void rule__ClassDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6440:1: ( ( ( rule__ClassDeclaration__Group_2__0 )? ) )
            // InternalJniMap.g:6441:1: ( ( rule__ClassDeclaration__Group_2__0 )? )
            {
            // InternalJniMap.g:6441:1: ( ( rule__ClassDeclaration__Group_2__0 )? )
            // InternalJniMap.g:6442:1: ( rule__ClassDeclaration__Group_2__0 )?
            {
             before(grammarAccess.getClassDeclarationAccess().getGroup_2()); 
            // InternalJniMap.g:6443:1: ( rule__ClassDeclaration__Group_2__0 )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==45) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // InternalJniMap.g:6443:2: rule__ClassDeclaration__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ClassDeclaration__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getClassDeclarationAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group__2__Impl"


    // $ANTLR start "rule__ClassDeclaration__Group_2__0"
    // InternalJniMap.g:6459:1: rule__ClassDeclaration__Group_2__0 : rule__ClassDeclaration__Group_2__0__Impl rule__ClassDeclaration__Group_2__1 ;
    public final void rule__ClassDeclaration__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6463:1: ( rule__ClassDeclaration__Group_2__0__Impl rule__ClassDeclaration__Group_2__1 )
            // InternalJniMap.g:6464:2: rule__ClassDeclaration__Group_2__0__Impl rule__ClassDeclaration__Group_2__1
            {
            pushFollow(FOLLOW_41);
            rule__ClassDeclaration__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassDeclaration__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group_2__0"


    // $ANTLR start "rule__ClassDeclaration__Group_2__0__Impl"
    // InternalJniMap.g:6471:1: rule__ClassDeclaration__Group_2__0__Impl : ( '{' ) ;
    public final void rule__ClassDeclaration__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6475:1: ( ( '{' ) )
            // InternalJniMap.g:6476:1: ( '{' )
            {
            // InternalJniMap.g:6476:1: ( '{' )
            // InternalJniMap.g:6477:1: '{'
            {
             before(grammarAccess.getClassDeclarationAccess().getLeftCurlyBracketKeyword_2_0()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getClassDeclarationAccess().getLeftCurlyBracketKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group_2__0__Impl"


    // $ANTLR start "rule__ClassDeclaration__Group_2__1"
    // InternalJniMap.g:6490:1: rule__ClassDeclaration__Group_2__1 : rule__ClassDeclaration__Group_2__1__Impl rule__ClassDeclaration__Group_2__2 ;
    public final void rule__ClassDeclaration__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6494:1: ( rule__ClassDeclaration__Group_2__1__Impl rule__ClassDeclaration__Group_2__2 )
            // InternalJniMap.g:6495:2: rule__ClassDeclaration__Group_2__1__Impl rule__ClassDeclaration__Group_2__2
            {
            pushFollow(FOLLOW_33);
            rule__ClassDeclaration__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassDeclaration__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group_2__1"


    // $ANTLR start "rule__ClassDeclaration__Group_2__1__Impl"
    // InternalJniMap.g:6502:1: rule__ClassDeclaration__Group_2__1__Impl : ( ( ( rule__ClassDeclaration__Group_2_1__0 ) ) ( ( rule__ClassDeclaration__Group_2_1__0 )* ) ) ;
    public final void rule__ClassDeclaration__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6506:1: ( ( ( ( rule__ClassDeclaration__Group_2_1__0 ) ) ( ( rule__ClassDeclaration__Group_2_1__0 )* ) ) )
            // InternalJniMap.g:6507:1: ( ( ( rule__ClassDeclaration__Group_2_1__0 ) ) ( ( rule__ClassDeclaration__Group_2_1__0 )* ) )
            {
            // InternalJniMap.g:6507:1: ( ( ( rule__ClassDeclaration__Group_2_1__0 ) ) ( ( rule__ClassDeclaration__Group_2_1__0 )* ) )
            // InternalJniMap.g:6508:1: ( ( rule__ClassDeclaration__Group_2_1__0 ) ) ( ( rule__ClassDeclaration__Group_2_1__0 )* )
            {
            // InternalJniMap.g:6508:1: ( ( rule__ClassDeclaration__Group_2_1__0 ) )
            // InternalJniMap.g:6509:1: ( rule__ClassDeclaration__Group_2_1__0 )
            {
             before(grammarAccess.getClassDeclarationAccess().getGroup_2_1()); 
            // InternalJniMap.g:6510:1: ( rule__ClassDeclaration__Group_2_1__0 )
            // InternalJniMap.g:6510:2: rule__ClassDeclaration__Group_2_1__0
            {
            pushFollow(FOLLOW_42);
            rule__ClassDeclaration__Group_2_1__0();

            state._fsp--;


            }

             after(grammarAccess.getClassDeclarationAccess().getGroup_2_1()); 

            }

            // InternalJniMap.g:6513:1: ( ( rule__ClassDeclaration__Group_2_1__0 )* )
            // InternalJniMap.g:6514:1: ( rule__ClassDeclaration__Group_2_1__0 )*
            {
             before(grammarAccess.getClassDeclarationAccess().getGroup_2_1()); 
            // InternalJniMap.g:6515:1: ( rule__ClassDeclaration__Group_2_1__0 )*
            loop51:
            do {
                int alt51=2;
                int LA51_0 = input.LA(1);

                if ( (LA51_0==RULE_ID||(LA51_0>=13 && LA51_0<=18)||LA51_0==51||LA51_0==53||LA51_0==56||(LA51_0>=68 && LA51_0<=70)||LA51_0==74) ) {
                    alt51=1;
                }


                switch (alt51) {
            	case 1 :
            	    // InternalJniMap.g:6515:2: rule__ClassDeclaration__Group_2_1__0
            	    {
            	    pushFollow(FOLLOW_42);
            	    rule__ClassDeclaration__Group_2_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop51;
                }
            } while (true);

             after(grammarAccess.getClassDeclarationAccess().getGroup_2_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group_2__1__Impl"


    // $ANTLR start "rule__ClassDeclaration__Group_2__2"
    // InternalJniMap.g:6526:1: rule__ClassDeclaration__Group_2__2 : rule__ClassDeclaration__Group_2__2__Impl ;
    public final void rule__ClassDeclaration__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6530:1: ( rule__ClassDeclaration__Group_2__2__Impl )
            // InternalJniMap.g:6531:2: rule__ClassDeclaration__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ClassDeclaration__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group_2__2"


    // $ANTLR start "rule__ClassDeclaration__Group_2__2__Impl"
    // InternalJniMap.g:6537:1: rule__ClassDeclaration__Group_2__2__Impl : ( '}' ) ;
    public final void rule__ClassDeclaration__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6541:1: ( ( '}' ) )
            // InternalJniMap.g:6542:1: ( '}' )
            {
            // InternalJniMap.g:6542:1: ( '}' )
            // InternalJniMap.g:6543:1: '}'
            {
             before(grammarAccess.getClassDeclarationAccess().getRightCurlyBracketKeyword_2_2()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getClassDeclarationAccess().getRightCurlyBracketKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group_2__2__Impl"


    // $ANTLR start "rule__ClassDeclaration__Group_2_1__0"
    // InternalJniMap.g:6562:1: rule__ClassDeclaration__Group_2_1__0 : rule__ClassDeclaration__Group_2_1__0__Impl rule__ClassDeclaration__Group_2_1__1 ;
    public final void rule__ClassDeclaration__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6566:1: ( rule__ClassDeclaration__Group_2_1__0__Impl rule__ClassDeclaration__Group_2_1__1 )
            // InternalJniMap.g:6567:2: rule__ClassDeclaration__Group_2_1__0__Impl rule__ClassDeclaration__Group_2_1__1
            {
            pushFollow(FOLLOW_7);
            rule__ClassDeclaration__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassDeclaration__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group_2_1__0"


    // $ANTLR start "rule__ClassDeclaration__Group_2_1__0__Impl"
    // InternalJniMap.g:6574:1: rule__ClassDeclaration__Group_2_1__0__Impl : ( ( rule__ClassDeclaration__FieldsAssignment_2_1_0 ) ) ;
    public final void rule__ClassDeclaration__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6578:1: ( ( ( rule__ClassDeclaration__FieldsAssignment_2_1_0 ) ) )
            // InternalJniMap.g:6579:1: ( ( rule__ClassDeclaration__FieldsAssignment_2_1_0 ) )
            {
            // InternalJniMap.g:6579:1: ( ( rule__ClassDeclaration__FieldsAssignment_2_1_0 ) )
            // InternalJniMap.g:6580:1: ( rule__ClassDeclaration__FieldsAssignment_2_1_0 )
            {
             before(grammarAccess.getClassDeclarationAccess().getFieldsAssignment_2_1_0()); 
            // InternalJniMap.g:6581:1: ( rule__ClassDeclaration__FieldsAssignment_2_1_0 )
            // InternalJniMap.g:6581:2: rule__ClassDeclaration__FieldsAssignment_2_1_0
            {
            pushFollow(FOLLOW_2);
            rule__ClassDeclaration__FieldsAssignment_2_1_0();

            state._fsp--;


            }

             after(grammarAccess.getClassDeclarationAccess().getFieldsAssignment_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group_2_1__0__Impl"


    // $ANTLR start "rule__ClassDeclaration__Group_2_1__1"
    // InternalJniMap.g:6591:1: rule__ClassDeclaration__Group_2_1__1 : rule__ClassDeclaration__Group_2_1__1__Impl ;
    public final void rule__ClassDeclaration__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6595:1: ( rule__ClassDeclaration__Group_2_1__1__Impl )
            // InternalJniMap.g:6596:2: rule__ClassDeclaration__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ClassDeclaration__Group_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group_2_1__1"


    // $ANTLR start "rule__ClassDeclaration__Group_2_1__1__Impl"
    // InternalJniMap.g:6602:1: rule__ClassDeclaration__Group_2_1__1__Impl : ( ';' ) ;
    public final void rule__ClassDeclaration__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6606:1: ( ( ';' ) )
            // InternalJniMap.g:6607:1: ( ';' )
            {
            // InternalJniMap.g:6607:1: ( ';' )
            // InternalJniMap.g:6608:1: ';'
            {
             before(grammarAccess.getClassDeclarationAccess().getSemicolonKeyword_2_1_1()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getClassDeclarationAccess().getSemicolonKeyword_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__Group_2_1__1__Impl"


    // $ANTLR start "rule__EnumDeclaration__Group__0"
    // InternalJniMap.g:6625:1: rule__EnumDeclaration__Group__0 : rule__EnumDeclaration__Group__0__Impl rule__EnumDeclaration__Group__1 ;
    public final void rule__EnumDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6629:1: ( rule__EnumDeclaration__Group__0__Impl rule__EnumDeclaration__Group__1 )
            // InternalJniMap.g:6630:2: rule__EnumDeclaration__Group__0__Impl rule__EnumDeclaration__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__EnumDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group__0"


    // $ANTLR start "rule__EnumDeclaration__Group__0__Impl"
    // InternalJniMap.g:6637:1: rule__EnumDeclaration__Group__0__Impl : ( 'enum' ) ;
    public final void rule__EnumDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6641:1: ( ( 'enum' ) )
            // InternalJniMap.g:6642:1: ( 'enum' )
            {
            // InternalJniMap.g:6642:1: ( 'enum' )
            // InternalJniMap.g:6643:1: 'enum'
            {
             before(grammarAccess.getEnumDeclarationAccess().getEnumKeyword_0()); 
            match(input,53,FOLLOW_2); 
             after(grammarAccess.getEnumDeclarationAccess().getEnumKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group__0__Impl"


    // $ANTLR start "rule__EnumDeclaration__Group__1"
    // InternalJniMap.g:6656:1: rule__EnumDeclaration__Group__1 : rule__EnumDeclaration__Group__1__Impl rule__EnumDeclaration__Group__2 ;
    public final void rule__EnumDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6660:1: ( rule__EnumDeclaration__Group__1__Impl rule__EnumDeclaration__Group__2 )
            // InternalJniMap.g:6661:2: rule__EnumDeclaration__Group__1__Impl rule__EnumDeclaration__Group__2
            {
            pushFollow(FOLLOW_31);
            rule__EnumDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group__1"


    // $ANTLR start "rule__EnumDeclaration__Group__1__Impl"
    // InternalJniMap.g:6668:1: rule__EnumDeclaration__Group__1__Impl : ( ( rule__EnumDeclaration__NameAssignment_1 ) ) ;
    public final void rule__EnumDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6672:1: ( ( ( rule__EnumDeclaration__NameAssignment_1 ) ) )
            // InternalJniMap.g:6673:1: ( ( rule__EnumDeclaration__NameAssignment_1 ) )
            {
            // InternalJniMap.g:6673:1: ( ( rule__EnumDeclaration__NameAssignment_1 ) )
            // InternalJniMap.g:6674:1: ( rule__EnumDeclaration__NameAssignment_1 )
            {
             before(grammarAccess.getEnumDeclarationAccess().getNameAssignment_1()); 
            // InternalJniMap.g:6675:1: ( rule__EnumDeclaration__NameAssignment_1 )
            // InternalJniMap.g:6675:2: rule__EnumDeclaration__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEnumDeclarationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group__1__Impl"


    // $ANTLR start "rule__EnumDeclaration__Group__2"
    // InternalJniMap.g:6685:1: rule__EnumDeclaration__Group__2 : rule__EnumDeclaration__Group__2__Impl ;
    public final void rule__EnumDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6689:1: ( rule__EnumDeclaration__Group__2__Impl )
            // InternalJniMap.g:6690:2: rule__EnumDeclaration__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group__2"


    // $ANTLR start "rule__EnumDeclaration__Group__2__Impl"
    // InternalJniMap.g:6696:1: rule__EnumDeclaration__Group__2__Impl : ( ( rule__EnumDeclaration__Group_2__0 )? ) ;
    public final void rule__EnumDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6700:1: ( ( ( rule__EnumDeclaration__Group_2__0 )? ) )
            // InternalJniMap.g:6701:1: ( ( rule__EnumDeclaration__Group_2__0 )? )
            {
            // InternalJniMap.g:6701:1: ( ( rule__EnumDeclaration__Group_2__0 )? )
            // InternalJniMap.g:6702:1: ( rule__EnumDeclaration__Group_2__0 )?
            {
             before(grammarAccess.getEnumDeclarationAccess().getGroup_2()); 
            // InternalJniMap.g:6703:1: ( rule__EnumDeclaration__Group_2__0 )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==45) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // InternalJniMap.g:6703:2: rule__EnumDeclaration__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__EnumDeclaration__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEnumDeclarationAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group__2__Impl"


    // $ANTLR start "rule__EnumDeclaration__Group_2__0"
    // InternalJniMap.g:6719:1: rule__EnumDeclaration__Group_2__0 : rule__EnumDeclaration__Group_2__0__Impl rule__EnumDeclaration__Group_2__1 ;
    public final void rule__EnumDeclaration__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6723:1: ( rule__EnumDeclaration__Group_2__0__Impl rule__EnumDeclaration__Group_2__1 )
            // InternalJniMap.g:6724:2: rule__EnumDeclaration__Group_2__0__Impl rule__EnumDeclaration__Group_2__1
            {
            pushFollow(FOLLOW_5);
            rule__EnumDeclaration__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group_2__0"


    // $ANTLR start "rule__EnumDeclaration__Group_2__0__Impl"
    // InternalJniMap.g:6731:1: rule__EnumDeclaration__Group_2__0__Impl : ( '{' ) ;
    public final void rule__EnumDeclaration__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6735:1: ( ( '{' ) )
            // InternalJniMap.g:6736:1: ( '{' )
            {
            // InternalJniMap.g:6736:1: ( '{' )
            // InternalJniMap.g:6737:1: '{'
            {
             before(grammarAccess.getEnumDeclarationAccess().getLeftCurlyBracketKeyword_2_0()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getEnumDeclarationAccess().getLeftCurlyBracketKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group_2__0__Impl"


    // $ANTLR start "rule__EnumDeclaration__Group_2__1"
    // InternalJniMap.g:6750:1: rule__EnumDeclaration__Group_2__1 : rule__EnumDeclaration__Group_2__1__Impl rule__EnumDeclaration__Group_2__2 ;
    public final void rule__EnumDeclaration__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6754:1: ( rule__EnumDeclaration__Group_2__1__Impl rule__EnumDeclaration__Group_2__2 )
            // InternalJniMap.g:6755:2: rule__EnumDeclaration__Group_2__1__Impl rule__EnumDeclaration__Group_2__2
            {
            pushFollow(FOLLOW_43);
            rule__EnumDeclaration__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group_2__1"


    // $ANTLR start "rule__EnumDeclaration__Group_2__1__Impl"
    // InternalJniMap.g:6762:1: rule__EnumDeclaration__Group_2__1__Impl : ( ( rule__EnumDeclaration__ValuesAssignment_2_1 ) ) ;
    public final void rule__EnumDeclaration__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6766:1: ( ( ( rule__EnumDeclaration__ValuesAssignment_2_1 ) ) )
            // InternalJniMap.g:6767:1: ( ( rule__EnumDeclaration__ValuesAssignment_2_1 ) )
            {
            // InternalJniMap.g:6767:1: ( ( rule__EnumDeclaration__ValuesAssignment_2_1 ) )
            // InternalJniMap.g:6768:1: ( rule__EnumDeclaration__ValuesAssignment_2_1 )
            {
             before(grammarAccess.getEnumDeclarationAccess().getValuesAssignment_2_1()); 
            // InternalJniMap.g:6769:1: ( rule__EnumDeclaration__ValuesAssignment_2_1 )
            // InternalJniMap.g:6769:2: rule__EnumDeclaration__ValuesAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__ValuesAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getEnumDeclarationAccess().getValuesAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group_2__1__Impl"


    // $ANTLR start "rule__EnumDeclaration__Group_2__2"
    // InternalJniMap.g:6779:1: rule__EnumDeclaration__Group_2__2 : rule__EnumDeclaration__Group_2__2__Impl rule__EnumDeclaration__Group_2__3 ;
    public final void rule__EnumDeclaration__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6783:1: ( rule__EnumDeclaration__Group_2__2__Impl rule__EnumDeclaration__Group_2__3 )
            // InternalJniMap.g:6784:2: rule__EnumDeclaration__Group_2__2__Impl rule__EnumDeclaration__Group_2__3
            {
            pushFollow(FOLLOW_43);
            rule__EnumDeclaration__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group_2__2"


    // $ANTLR start "rule__EnumDeclaration__Group_2__2__Impl"
    // InternalJniMap.g:6791:1: rule__EnumDeclaration__Group_2__2__Impl : ( ( rule__EnumDeclaration__Group_2_2__0 )* ) ;
    public final void rule__EnumDeclaration__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6795:1: ( ( ( rule__EnumDeclaration__Group_2_2__0 )* ) )
            // InternalJniMap.g:6796:1: ( ( rule__EnumDeclaration__Group_2_2__0 )* )
            {
            // InternalJniMap.g:6796:1: ( ( rule__EnumDeclaration__Group_2_2__0 )* )
            // InternalJniMap.g:6797:1: ( rule__EnumDeclaration__Group_2_2__0 )*
            {
             before(grammarAccess.getEnumDeclarationAccess().getGroup_2_2()); 
            // InternalJniMap.g:6798:1: ( rule__EnumDeclaration__Group_2_2__0 )*
            loop53:
            do {
                int alt53=2;
                int LA53_0 = input.LA(1);

                if ( (LA53_0==33) ) {
                    alt53=1;
                }


                switch (alt53) {
            	case 1 :
            	    // InternalJniMap.g:6798:2: rule__EnumDeclaration__Group_2_2__0
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__EnumDeclaration__Group_2_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop53;
                }
            } while (true);

             after(grammarAccess.getEnumDeclarationAccess().getGroup_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group_2__2__Impl"


    // $ANTLR start "rule__EnumDeclaration__Group_2__3"
    // InternalJniMap.g:6808:1: rule__EnumDeclaration__Group_2__3 : rule__EnumDeclaration__Group_2__3__Impl ;
    public final void rule__EnumDeclaration__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6812:1: ( rule__EnumDeclaration__Group_2__3__Impl )
            // InternalJniMap.g:6813:2: rule__EnumDeclaration__Group_2__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__Group_2__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group_2__3"


    // $ANTLR start "rule__EnumDeclaration__Group_2__3__Impl"
    // InternalJniMap.g:6819:1: rule__EnumDeclaration__Group_2__3__Impl : ( '}' ) ;
    public final void rule__EnumDeclaration__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6823:1: ( ( '}' ) )
            // InternalJniMap.g:6824:1: ( '}' )
            {
            // InternalJniMap.g:6824:1: ( '}' )
            // InternalJniMap.g:6825:1: '}'
            {
             before(grammarAccess.getEnumDeclarationAccess().getRightCurlyBracketKeyword_2_3()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getEnumDeclarationAccess().getRightCurlyBracketKeyword_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group_2__3__Impl"


    // $ANTLR start "rule__EnumDeclaration__Group_2_2__0"
    // InternalJniMap.g:6846:1: rule__EnumDeclaration__Group_2_2__0 : rule__EnumDeclaration__Group_2_2__0__Impl rule__EnumDeclaration__Group_2_2__1 ;
    public final void rule__EnumDeclaration__Group_2_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6850:1: ( rule__EnumDeclaration__Group_2_2__0__Impl rule__EnumDeclaration__Group_2_2__1 )
            // InternalJniMap.g:6851:2: rule__EnumDeclaration__Group_2_2__0__Impl rule__EnumDeclaration__Group_2_2__1
            {
            pushFollow(FOLLOW_5);
            rule__EnumDeclaration__Group_2_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__Group_2_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group_2_2__0"


    // $ANTLR start "rule__EnumDeclaration__Group_2_2__0__Impl"
    // InternalJniMap.g:6858:1: rule__EnumDeclaration__Group_2_2__0__Impl : ( ',' ) ;
    public final void rule__EnumDeclaration__Group_2_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6862:1: ( ( ',' ) )
            // InternalJniMap.g:6863:1: ( ',' )
            {
            // InternalJniMap.g:6863:1: ( ',' )
            // InternalJniMap.g:6864:1: ','
            {
             before(grammarAccess.getEnumDeclarationAccess().getCommaKeyword_2_2_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getEnumDeclarationAccess().getCommaKeyword_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group_2_2__0__Impl"


    // $ANTLR start "rule__EnumDeclaration__Group_2_2__1"
    // InternalJniMap.g:6877:1: rule__EnumDeclaration__Group_2_2__1 : rule__EnumDeclaration__Group_2_2__1__Impl ;
    public final void rule__EnumDeclaration__Group_2_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6881:1: ( rule__EnumDeclaration__Group_2_2__1__Impl )
            // InternalJniMap.g:6882:2: rule__EnumDeclaration__Group_2_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__Group_2_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group_2_2__1"


    // $ANTLR start "rule__EnumDeclaration__Group_2_2__1__Impl"
    // InternalJniMap.g:6888:1: rule__EnumDeclaration__Group_2_2__1__Impl : ( ( rule__EnumDeclaration__ValuesAssignment_2_2_1 ) ) ;
    public final void rule__EnumDeclaration__Group_2_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6892:1: ( ( ( rule__EnumDeclaration__ValuesAssignment_2_2_1 ) ) )
            // InternalJniMap.g:6893:1: ( ( rule__EnumDeclaration__ValuesAssignment_2_2_1 ) )
            {
            // InternalJniMap.g:6893:1: ( ( rule__EnumDeclaration__ValuesAssignment_2_2_1 ) )
            // InternalJniMap.g:6894:1: ( rule__EnumDeclaration__ValuesAssignment_2_2_1 )
            {
             before(grammarAccess.getEnumDeclarationAccess().getValuesAssignment_2_2_1()); 
            // InternalJniMap.g:6895:1: ( rule__EnumDeclaration__ValuesAssignment_2_2_1 )
            // InternalJniMap.g:6895:2: rule__EnumDeclaration__ValuesAssignment_2_2_1
            {
            pushFollow(FOLLOW_2);
            rule__EnumDeclaration__ValuesAssignment_2_2_1();

            state._fsp--;


            }

             after(grammarAccess.getEnumDeclarationAccess().getValuesAssignment_2_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__Group_2_2__1__Impl"


    // $ANTLR start "rule__EnumType__Group__0"
    // InternalJniMap.g:6909:1: rule__EnumType__Group__0 : rule__EnumType__Group__0__Impl rule__EnumType__Group__1 ;
    public final void rule__EnumType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6913:1: ( rule__EnumType__Group__0__Impl rule__EnumType__Group__1 )
            // InternalJniMap.g:6914:2: rule__EnumType__Group__0__Impl rule__EnumType__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__EnumType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EnumType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumType__Group__0"


    // $ANTLR start "rule__EnumType__Group__0__Impl"
    // InternalJniMap.g:6921:1: rule__EnumType__Group__0__Impl : ( 'enum' ) ;
    public final void rule__EnumType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6925:1: ( ( 'enum' ) )
            // InternalJniMap.g:6926:1: ( 'enum' )
            {
            // InternalJniMap.g:6926:1: ( 'enum' )
            // InternalJniMap.g:6927:1: 'enum'
            {
             before(grammarAccess.getEnumTypeAccess().getEnumKeyword_0()); 
            match(input,53,FOLLOW_2); 
             after(grammarAccess.getEnumTypeAccess().getEnumKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumType__Group__0__Impl"


    // $ANTLR start "rule__EnumType__Group__1"
    // InternalJniMap.g:6940:1: rule__EnumType__Group__1 : rule__EnumType__Group__1__Impl ;
    public final void rule__EnumType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6944:1: ( rule__EnumType__Group__1__Impl )
            // InternalJniMap.g:6945:2: rule__EnumType__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EnumType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumType__Group__1"


    // $ANTLR start "rule__EnumType__Group__1__Impl"
    // InternalJniMap.g:6951:1: rule__EnumType__Group__1__Impl : ( ( rule__EnumType__NameAssignment_1 ) ) ;
    public final void rule__EnumType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6955:1: ( ( ( rule__EnumType__NameAssignment_1 ) ) )
            // InternalJniMap.g:6956:1: ( ( rule__EnumType__NameAssignment_1 ) )
            {
            // InternalJniMap.g:6956:1: ( ( rule__EnumType__NameAssignment_1 ) )
            // InternalJniMap.g:6957:1: ( rule__EnumType__NameAssignment_1 )
            {
             before(grammarAccess.getEnumTypeAccess().getNameAssignment_1()); 
            // InternalJniMap.g:6958:1: ( rule__EnumType__NameAssignment_1 )
            // InternalJniMap.g:6958:2: rule__EnumType__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__EnumType__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEnumTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumType__Group__1__Impl"


    // $ANTLR start "rule__EnumValue__Group__0"
    // InternalJniMap.g:6972:1: rule__EnumValue__Group__0 : rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1 ;
    public final void rule__EnumValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6976:1: ( rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1 )
            // InternalJniMap.g:6977:2: rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__EnumValue__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EnumValue__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__0"


    // $ANTLR start "rule__EnumValue__Group__0__Impl"
    // InternalJniMap.g:6984:1: rule__EnumValue__Group__0__Impl : ( ( rule__EnumValue__NameAssignment_0 ) ) ;
    public final void rule__EnumValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:6988:1: ( ( ( rule__EnumValue__NameAssignment_0 ) ) )
            // InternalJniMap.g:6989:1: ( ( rule__EnumValue__NameAssignment_0 ) )
            {
            // InternalJniMap.g:6989:1: ( ( rule__EnumValue__NameAssignment_0 ) )
            // InternalJniMap.g:6990:1: ( rule__EnumValue__NameAssignment_0 )
            {
             before(grammarAccess.getEnumValueAccess().getNameAssignment_0()); 
            // InternalJniMap.g:6991:1: ( rule__EnumValue__NameAssignment_0 )
            // InternalJniMap.g:6991:2: rule__EnumValue__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__EnumValue__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getEnumValueAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__0__Impl"


    // $ANTLR start "rule__EnumValue__Group__1"
    // InternalJniMap.g:7001:1: rule__EnumValue__Group__1 : rule__EnumValue__Group__1__Impl ;
    public final void rule__EnumValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7005:1: ( rule__EnumValue__Group__1__Impl )
            // InternalJniMap.g:7006:2: rule__EnumValue__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EnumValue__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__1"


    // $ANTLR start "rule__EnumValue__Group__1__Impl"
    // InternalJniMap.g:7012:1: rule__EnumValue__Group__1__Impl : ( ( rule__EnumValue__Group_1__0 )? ) ;
    public final void rule__EnumValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7016:1: ( ( ( rule__EnumValue__Group_1__0 )? ) )
            // InternalJniMap.g:7017:1: ( ( rule__EnumValue__Group_1__0 )? )
            {
            // InternalJniMap.g:7017:1: ( ( rule__EnumValue__Group_1__0 )? )
            // InternalJniMap.g:7018:1: ( rule__EnumValue__Group_1__0 )?
            {
             before(grammarAccess.getEnumValueAccess().getGroup_1()); 
            // InternalJniMap.g:7019:1: ( rule__EnumValue__Group_1__0 )?
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==29) ) {
                alt54=1;
            }
            switch (alt54) {
                case 1 :
                    // InternalJniMap.g:7019:2: rule__EnumValue__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__EnumValue__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEnumValueAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__1__Impl"


    // $ANTLR start "rule__EnumValue__Group_1__0"
    // InternalJniMap.g:7033:1: rule__EnumValue__Group_1__0 : rule__EnumValue__Group_1__0__Impl rule__EnumValue__Group_1__1 ;
    public final void rule__EnumValue__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7037:1: ( rule__EnumValue__Group_1__0__Impl rule__EnumValue__Group_1__1 )
            // InternalJniMap.g:7038:2: rule__EnumValue__Group_1__0__Impl rule__EnumValue__Group_1__1
            {
            pushFollow(FOLLOW_44);
            rule__EnumValue__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EnumValue__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group_1__0"


    // $ANTLR start "rule__EnumValue__Group_1__0__Impl"
    // InternalJniMap.g:7045:1: rule__EnumValue__Group_1__0__Impl : ( '=' ) ;
    public final void rule__EnumValue__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7049:1: ( ( '=' ) )
            // InternalJniMap.g:7050:1: ( '=' )
            {
            // InternalJniMap.g:7050:1: ( '=' )
            // InternalJniMap.g:7051:1: '='
            {
             before(grammarAccess.getEnumValueAccess().getEqualsSignKeyword_1_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getEnumValueAccess().getEqualsSignKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group_1__0__Impl"


    // $ANTLR start "rule__EnumValue__Group_1__1"
    // InternalJniMap.g:7064:1: rule__EnumValue__Group_1__1 : rule__EnumValue__Group_1__1__Impl ;
    public final void rule__EnumValue__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7068:1: ( rule__EnumValue__Group_1__1__Impl )
            // InternalJniMap.g:7069:2: rule__EnumValue__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EnumValue__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group_1__1"


    // $ANTLR start "rule__EnumValue__Group_1__1__Impl"
    // InternalJniMap.g:7075:1: rule__EnumValue__Group_1__1__Impl : ( ( rule__EnumValue__Alternatives_1_1 ) ) ;
    public final void rule__EnumValue__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7079:1: ( ( ( rule__EnumValue__Alternatives_1_1 ) ) )
            // InternalJniMap.g:7080:1: ( ( rule__EnumValue__Alternatives_1_1 ) )
            {
            // InternalJniMap.g:7080:1: ( ( rule__EnumValue__Alternatives_1_1 ) )
            // InternalJniMap.g:7081:1: ( rule__EnumValue__Alternatives_1_1 )
            {
             before(grammarAccess.getEnumValueAccess().getAlternatives_1_1()); 
            // InternalJniMap.g:7082:1: ( rule__EnumValue__Alternatives_1_1 )
            // InternalJniMap.g:7082:2: rule__EnumValue__Alternatives_1_1
            {
            pushFollow(FOLLOW_2);
            rule__EnumValue__Alternatives_1_1();

            state._fsp--;


            }

             after(grammarAccess.getEnumValueAccess().getAlternatives_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group_1__1__Impl"


    // $ANTLR start "rule__ParamDef__Group__0"
    // InternalJniMap.g:7096:1: rule__ParamDef__Group__0 : rule__ParamDef__Group__0__Impl rule__ParamDef__Group__1 ;
    public final void rule__ParamDef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7100:1: ( rule__ParamDef__Group__0__Impl rule__ParamDef__Group__1 )
            // InternalJniMap.g:7101:2: rule__ParamDef__Group__0__Impl rule__ParamDef__Group__1
            {
            pushFollow(FOLLOW_40);
            rule__ParamDef__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ParamDef__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamDef__Group__0"


    // $ANTLR start "rule__ParamDef__Group__0__Impl"
    // InternalJniMap.g:7108:1: rule__ParamDef__Group__0__Impl : ( ( rule__ParamDef__Alternatives_0 )? ) ;
    public final void rule__ParamDef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7112:1: ( ( ( rule__ParamDef__Alternatives_0 )? ) )
            // InternalJniMap.g:7113:1: ( ( rule__ParamDef__Alternatives_0 )? )
            {
            // InternalJniMap.g:7113:1: ( ( rule__ParamDef__Alternatives_0 )? )
            // InternalJniMap.g:7114:1: ( rule__ParamDef__Alternatives_0 )?
            {
             before(grammarAccess.getParamDefAccess().getAlternatives_0()); 
            // InternalJniMap.g:7115:1: ( rule__ParamDef__Alternatives_0 )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( ((LA55_0>=58 && LA55_0<=60)) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalJniMap.g:7115:2: rule__ParamDef__Alternatives_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ParamDef__Alternatives_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getParamDefAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamDef__Group__0__Impl"


    // $ANTLR start "rule__ParamDef__Group__1"
    // InternalJniMap.g:7125:1: rule__ParamDef__Group__1 : rule__ParamDef__Group__1__Impl rule__ParamDef__Group__2 ;
    public final void rule__ParamDef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7129:1: ( rule__ParamDef__Group__1__Impl rule__ParamDef__Group__2 )
            // InternalJniMap.g:7130:2: rule__ParamDef__Group__1__Impl rule__ParamDef__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__ParamDef__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ParamDef__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamDef__Group__1"


    // $ANTLR start "rule__ParamDef__Group__1__Impl"
    // InternalJniMap.g:7137:1: rule__ParamDef__Group__1__Impl : ( ( rule__ParamDef__TypeAssignment_1 ) ) ;
    public final void rule__ParamDef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7141:1: ( ( ( rule__ParamDef__TypeAssignment_1 ) ) )
            // InternalJniMap.g:7142:1: ( ( rule__ParamDef__TypeAssignment_1 ) )
            {
            // InternalJniMap.g:7142:1: ( ( rule__ParamDef__TypeAssignment_1 ) )
            // InternalJniMap.g:7143:1: ( rule__ParamDef__TypeAssignment_1 )
            {
             before(grammarAccess.getParamDefAccess().getTypeAssignment_1()); 
            // InternalJniMap.g:7144:1: ( rule__ParamDef__TypeAssignment_1 )
            // InternalJniMap.g:7144:2: rule__ParamDef__TypeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ParamDef__TypeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getParamDefAccess().getTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamDef__Group__1__Impl"


    // $ANTLR start "rule__ParamDef__Group__2"
    // InternalJniMap.g:7154:1: rule__ParamDef__Group__2 : rule__ParamDef__Group__2__Impl ;
    public final void rule__ParamDef__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7158:1: ( rule__ParamDef__Group__2__Impl )
            // InternalJniMap.g:7159:2: rule__ParamDef__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ParamDef__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamDef__Group__2"


    // $ANTLR start "rule__ParamDef__Group__2__Impl"
    // InternalJniMap.g:7165:1: rule__ParamDef__Group__2__Impl : ( ( rule__ParamDef__NameAssignment_2 ) ) ;
    public final void rule__ParamDef__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7169:1: ( ( ( rule__ParamDef__NameAssignment_2 ) ) )
            // InternalJniMap.g:7170:1: ( ( rule__ParamDef__NameAssignment_2 ) )
            {
            // InternalJniMap.g:7170:1: ( ( rule__ParamDef__NameAssignment_2 ) )
            // InternalJniMap.g:7171:1: ( rule__ParamDef__NameAssignment_2 )
            {
             before(grammarAccess.getParamDefAccess().getNameAssignment_2()); 
            // InternalJniMap.g:7172:1: ( rule__ParamDef__NameAssignment_2 )
            // InternalJniMap.g:7172:2: rule__ParamDef__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ParamDef__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getParamDefAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamDef__Group__2__Impl"


    // $ANTLR start "rule__MethodGroup__Group__0"
    // InternalJniMap.g:7188:1: rule__MethodGroup__Group__0 : rule__MethodGroup__Group__0__Impl rule__MethodGroup__Group__1 ;
    public final void rule__MethodGroup__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7192:1: ( rule__MethodGroup__Group__0__Impl rule__MethodGroup__Group__1 )
            // InternalJniMap.g:7193:2: rule__MethodGroup__Group__0__Impl rule__MethodGroup__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__MethodGroup__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MethodGroup__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodGroup__Group__0"


    // $ANTLR start "rule__MethodGroup__Group__0__Impl"
    // InternalJniMap.g:7200:1: rule__MethodGroup__Group__0__Impl : ( 'group' ) ;
    public final void rule__MethodGroup__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7204:1: ( ( 'group' ) )
            // InternalJniMap.g:7205:1: ( 'group' )
            {
            // InternalJniMap.g:7205:1: ( 'group' )
            // InternalJniMap.g:7206:1: 'group'
            {
             before(grammarAccess.getMethodGroupAccess().getGroupKeyword_0()); 
            match(input,54,FOLLOW_2); 
             after(grammarAccess.getMethodGroupAccess().getGroupKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodGroup__Group__0__Impl"


    // $ANTLR start "rule__MethodGroup__Group__1"
    // InternalJniMap.g:7219:1: rule__MethodGroup__Group__1 : rule__MethodGroup__Group__1__Impl rule__MethodGroup__Group__2 ;
    public final void rule__MethodGroup__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7223:1: ( rule__MethodGroup__Group__1__Impl rule__MethodGroup__Group__2 )
            // InternalJniMap.g:7224:2: rule__MethodGroup__Group__1__Impl rule__MethodGroup__Group__2
            {
            pushFollow(FOLLOW_31);
            rule__MethodGroup__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MethodGroup__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodGroup__Group__1"


    // $ANTLR start "rule__MethodGroup__Group__1__Impl"
    // InternalJniMap.g:7231:1: rule__MethodGroup__Group__1__Impl : ( ( rule__MethodGroup__ClassAssignment_1 ) ) ;
    public final void rule__MethodGroup__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7235:1: ( ( ( rule__MethodGroup__ClassAssignment_1 ) ) )
            // InternalJniMap.g:7236:1: ( ( rule__MethodGroup__ClassAssignment_1 ) )
            {
            // InternalJniMap.g:7236:1: ( ( rule__MethodGroup__ClassAssignment_1 ) )
            // InternalJniMap.g:7237:1: ( rule__MethodGroup__ClassAssignment_1 )
            {
             before(grammarAccess.getMethodGroupAccess().getClassAssignment_1()); 
            // InternalJniMap.g:7238:1: ( rule__MethodGroup__ClassAssignment_1 )
            // InternalJniMap.g:7238:2: rule__MethodGroup__ClassAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__MethodGroup__ClassAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMethodGroupAccess().getClassAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodGroup__Group__1__Impl"


    // $ANTLR start "rule__MethodGroup__Group__2"
    // InternalJniMap.g:7248:1: rule__MethodGroup__Group__2 : rule__MethodGroup__Group__2__Impl rule__MethodGroup__Group__3 ;
    public final void rule__MethodGroup__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7252:1: ( rule__MethodGroup__Group__2__Impl rule__MethodGroup__Group__3 )
            // InternalJniMap.g:7253:2: rule__MethodGroup__Group__2__Impl rule__MethodGroup__Group__3
            {
            pushFollow(FOLLOW_45);
            rule__MethodGroup__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MethodGroup__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodGroup__Group__2"


    // $ANTLR start "rule__MethodGroup__Group__2__Impl"
    // InternalJniMap.g:7260:1: rule__MethodGroup__Group__2__Impl : ( '{' ) ;
    public final void rule__MethodGroup__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7264:1: ( ( '{' ) )
            // InternalJniMap.g:7265:1: ( '{' )
            {
            // InternalJniMap.g:7265:1: ( '{' )
            // InternalJniMap.g:7266:1: '{'
            {
             before(grammarAccess.getMethodGroupAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getMethodGroupAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodGroup__Group__2__Impl"


    // $ANTLR start "rule__MethodGroup__Group__3"
    // InternalJniMap.g:7279:1: rule__MethodGroup__Group__3 : rule__MethodGroup__Group__3__Impl rule__MethodGroup__Group__4 ;
    public final void rule__MethodGroup__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7283:1: ( rule__MethodGroup__Group__3__Impl rule__MethodGroup__Group__4 )
            // InternalJniMap.g:7284:2: rule__MethodGroup__Group__3__Impl rule__MethodGroup__Group__4
            {
            pushFollow(FOLLOW_33);
            rule__MethodGroup__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MethodGroup__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodGroup__Group__3"


    // $ANTLR start "rule__MethodGroup__Group__3__Impl"
    // InternalJniMap.g:7291:1: rule__MethodGroup__Group__3__Impl : ( ( ( rule__MethodGroup__MethodsAssignment_3 ) ) ( ( rule__MethodGroup__MethodsAssignment_3 )* ) ) ;
    public final void rule__MethodGroup__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7295:1: ( ( ( ( rule__MethodGroup__MethodsAssignment_3 ) ) ( ( rule__MethodGroup__MethodsAssignment_3 )* ) ) )
            // InternalJniMap.g:7296:1: ( ( ( rule__MethodGroup__MethodsAssignment_3 ) ) ( ( rule__MethodGroup__MethodsAssignment_3 )* ) )
            {
            // InternalJniMap.g:7296:1: ( ( ( rule__MethodGroup__MethodsAssignment_3 ) ) ( ( rule__MethodGroup__MethodsAssignment_3 )* ) )
            // InternalJniMap.g:7297:1: ( ( rule__MethodGroup__MethodsAssignment_3 ) ) ( ( rule__MethodGroup__MethodsAssignment_3 )* )
            {
            // InternalJniMap.g:7297:1: ( ( rule__MethodGroup__MethodsAssignment_3 ) )
            // InternalJniMap.g:7298:1: ( rule__MethodGroup__MethodsAssignment_3 )
            {
             before(grammarAccess.getMethodGroupAccess().getMethodsAssignment_3()); 
            // InternalJniMap.g:7299:1: ( rule__MethodGroup__MethodsAssignment_3 )
            // InternalJniMap.g:7299:2: rule__MethodGroup__MethodsAssignment_3
            {
            pushFollow(FOLLOW_46);
            rule__MethodGroup__MethodsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getMethodGroupAccess().getMethodsAssignment_3()); 

            }

            // InternalJniMap.g:7302:1: ( ( rule__MethodGroup__MethodsAssignment_3 )* )
            // InternalJniMap.g:7303:1: ( rule__MethodGroup__MethodsAssignment_3 )*
            {
             before(grammarAccess.getMethodGroupAccess().getMethodsAssignment_3()); 
            // InternalJniMap.g:7304:1: ( rule__MethodGroup__MethodsAssignment_3 )*
            loop56:
            do {
                int alt56=2;
                int LA56_0 = input.LA(1);

                if ( (LA56_0==RULE_ID||(LA56_0>=11 && LA56_0<=18)||LA56_0==31||LA56_0==51||LA56_0==53||LA56_0==56||(LA56_0>=68 && LA56_0<=70)) ) {
                    alt56=1;
                }


                switch (alt56) {
            	case 1 :
            	    // InternalJniMap.g:7304:2: rule__MethodGroup__MethodsAssignment_3
            	    {
            	    pushFollow(FOLLOW_46);
            	    rule__MethodGroup__MethodsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop56;
                }
            } while (true);

             after(grammarAccess.getMethodGroupAccess().getMethodsAssignment_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodGroup__Group__3__Impl"


    // $ANTLR start "rule__MethodGroup__Group__4"
    // InternalJniMap.g:7315:1: rule__MethodGroup__Group__4 : rule__MethodGroup__Group__4__Impl ;
    public final void rule__MethodGroup__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7319:1: ( rule__MethodGroup__Group__4__Impl )
            // InternalJniMap.g:7320:2: rule__MethodGroup__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MethodGroup__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodGroup__Group__4"


    // $ANTLR start "rule__MethodGroup__Group__4__Impl"
    // InternalJniMap.g:7326:1: rule__MethodGroup__Group__4__Impl : ( '}' ) ;
    public final void rule__MethodGroup__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7330:1: ( ( '}' ) )
            // InternalJniMap.g:7331:1: ( '}' )
            {
            // InternalJniMap.g:7331:1: ( '}' )
            // InternalJniMap.g:7332:1: '}'
            {
             before(grammarAccess.getMethodGroupAccess().getRightCurlyBracketKeyword_4()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getMethodGroupAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodGroup__Group__4__Impl"


    // $ANTLR start "rule__Method__Group__0"
    // InternalJniMap.g:7355:1: rule__Method__Group__0 : rule__Method__Group__0__Impl rule__Method__Group__1 ;
    public final void rule__Method__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7359:1: ( rule__Method__Group__0__Impl rule__Method__Group__1 )
            // InternalJniMap.g:7360:2: rule__Method__Group__0__Impl rule__Method__Group__1
            {
            pushFollow(FOLLOW_45);
            rule__Method__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__0"


    // $ANTLR start "rule__Method__Group__0__Impl"
    // InternalJniMap.g:7367:1: rule__Method__Group__0__Impl : ( ( rule__Method__Group_0__0 )? ) ;
    public final void rule__Method__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7371:1: ( ( ( rule__Method__Group_0__0 )? ) )
            // InternalJniMap.g:7372:1: ( ( rule__Method__Group_0__0 )? )
            {
            // InternalJniMap.g:7372:1: ( ( rule__Method__Group_0__0 )? )
            // InternalJniMap.g:7373:1: ( rule__Method__Group_0__0 )?
            {
             before(grammarAccess.getMethodAccess().getGroup_0()); 
            // InternalJniMap.g:7374:1: ( rule__Method__Group_0__0 )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==31) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // InternalJniMap.g:7374:2: rule__Method__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__Group_0__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMethodAccess().getGroup_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__0__Impl"


    // $ANTLR start "rule__Method__Group__1"
    // InternalJniMap.g:7384:1: rule__Method__Group__1 : rule__Method__Group__1__Impl rule__Method__Group__2 ;
    public final void rule__Method__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7388:1: ( rule__Method__Group__1__Impl rule__Method__Group__2 )
            // InternalJniMap.g:7389:2: rule__Method__Group__1__Impl rule__Method__Group__2
            {
            pushFollow(FOLLOW_45);
            rule__Method__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__1"


    // $ANTLR start "rule__Method__Group__1__Impl"
    // InternalJniMap.g:7396:1: rule__Method__Group__1__Impl : ( ( rule__Method__Alternatives_1 )? ) ;
    public final void rule__Method__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7400:1: ( ( ( rule__Method__Alternatives_1 )? ) )
            // InternalJniMap.g:7401:1: ( ( rule__Method__Alternatives_1 )? )
            {
            // InternalJniMap.g:7401:1: ( ( rule__Method__Alternatives_1 )? )
            // InternalJniMap.g:7402:1: ( rule__Method__Alternatives_1 )?
            {
             before(grammarAccess.getMethodAccess().getAlternatives_1()); 
            // InternalJniMap.g:7403:1: ( rule__Method__Alternatives_1 )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( ((LA58_0>=11 && LA58_0<=12)) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // InternalJniMap.g:7403:2: rule__Method__Alternatives_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__Alternatives_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMethodAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__1__Impl"


    // $ANTLR start "rule__Method__Group__2"
    // InternalJniMap.g:7413:1: rule__Method__Group__2 : rule__Method__Group__2__Impl rule__Method__Group__3 ;
    public final void rule__Method__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7417:1: ( rule__Method__Group__2__Impl rule__Method__Group__3 )
            // InternalJniMap.g:7418:2: rule__Method__Group__2__Impl rule__Method__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Method__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__2"


    // $ANTLR start "rule__Method__Group__2__Impl"
    // InternalJniMap.g:7425:1: rule__Method__Group__2__Impl : ( ( rule__Method__ResAssignment_2 ) ) ;
    public final void rule__Method__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7429:1: ( ( ( rule__Method__ResAssignment_2 ) ) )
            // InternalJniMap.g:7430:1: ( ( rule__Method__ResAssignment_2 ) )
            {
            // InternalJniMap.g:7430:1: ( ( rule__Method__ResAssignment_2 ) )
            // InternalJniMap.g:7431:1: ( rule__Method__ResAssignment_2 )
            {
             before(grammarAccess.getMethodAccess().getResAssignment_2()); 
            // InternalJniMap.g:7432:1: ( rule__Method__ResAssignment_2 )
            // InternalJniMap.g:7432:2: rule__Method__ResAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Method__ResAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getMethodAccess().getResAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__2__Impl"


    // $ANTLR start "rule__Method__Group__3"
    // InternalJniMap.g:7442:1: rule__Method__Group__3 : rule__Method__Group__3__Impl rule__Method__Group__4 ;
    public final void rule__Method__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7446:1: ( rule__Method__Group__3__Impl rule__Method__Group__4 )
            // InternalJniMap.g:7447:2: rule__Method__Group__3__Impl rule__Method__Group__4
            {
            pushFollow(FOLLOW_38);
            rule__Method__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__3"


    // $ANTLR start "rule__Method__Group__3__Impl"
    // InternalJniMap.g:7454:1: rule__Method__Group__3__Impl : ( ( rule__Method__NameAssignment_3 ) ) ;
    public final void rule__Method__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7458:1: ( ( ( rule__Method__NameAssignment_3 ) ) )
            // InternalJniMap.g:7459:1: ( ( rule__Method__NameAssignment_3 ) )
            {
            // InternalJniMap.g:7459:1: ( ( rule__Method__NameAssignment_3 ) )
            // InternalJniMap.g:7460:1: ( rule__Method__NameAssignment_3 )
            {
             before(grammarAccess.getMethodAccess().getNameAssignment_3()); 
            // InternalJniMap.g:7461:1: ( rule__Method__NameAssignment_3 )
            // InternalJniMap.g:7461:2: rule__Method__NameAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Method__NameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getMethodAccess().getNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__3__Impl"


    // $ANTLR start "rule__Method__Group__4"
    // InternalJniMap.g:7471:1: rule__Method__Group__4 : rule__Method__Group__4__Impl rule__Method__Group__5 ;
    public final void rule__Method__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7475:1: ( rule__Method__Group__4__Impl rule__Method__Group__5 )
            // InternalJniMap.g:7476:2: rule__Method__Group__4__Impl rule__Method__Group__5
            {
            pushFollow(FOLLOW_39);
            rule__Method__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__4"


    // $ANTLR start "rule__Method__Group__4__Impl"
    // InternalJniMap.g:7483:1: rule__Method__Group__4__Impl : ( '(' ) ;
    public final void rule__Method__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7487:1: ( ( '(' ) )
            // InternalJniMap.g:7488:1: ( '(' )
            {
            // InternalJniMap.g:7488:1: ( '(' )
            // InternalJniMap.g:7489:1: '('
            {
             before(grammarAccess.getMethodAccess().getLeftParenthesisKeyword_4()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getLeftParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__4__Impl"


    // $ANTLR start "rule__Method__Group__5"
    // InternalJniMap.g:7502:1: rule__Method__Group__5 : rule__Method__Group__5__Impl rule__Method__Group__6 ;
    public final void rule__Method__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7506:1: ( rule__Method__Group__5__Impl rule__Method__Group__6 )
            // InternalJniMap.g:7507:2: rule__Method__Group__5__Impl rule__Method__Group__6
            {
            pushFollow(FOLLOW_39);
            rule__Method__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__5"


    // $ANTLR start "rule__Method__Group__5__Impl"
    // InternalJniMap.g:7514:1: rule__Method__Group__5__Impl : ( ( rule__Method__Group_5__0 )? ) ;
    public final void rule__Method__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7518:1: ( ( ( rule__Method__Group_5__0 )? ) )
            // InternalJniMap.g:7519:1: ( ( rule__Method__Group_5__0 )? )
            {
            // InternalJniMap.g:7519:1: ( ( rule__Method__Group_5__0 )? )
            // InternalJniMap.g:7520:1: ( rule__Method__Group_5__0 )?
            {
             before(grammarAccess.getMethodAccess().getGroup_5()); 
            // InternalJniMap.g:7521:1: ( rule__Method__Group_5__0 )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==RULE_ID||(LA59_0>=13 && LA59_0<=18)||LA59_0==51||LA59_0==53||LA59_0==56||(LA59_0>=58 && LA59_0<=60)||(LA59_0>=68 && LA59_0<=70)) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // InternalJniMap.g:7521:2: rule__Method__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMethodAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__5__Impl"


    // $ANTLR start "rule__Method__Group__6"
    // InternalJniMap.g:7531:1: rule__Method__Group__6 : rule__Method__Group__6__Impl rule__Method__Group__7 ;
    public final void rule__Method__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7535:1: ( rule__Method__Group__6__Impl rule__Method__Group__7 )
            // InternalJniMap.g:7536:2: rule__Method__Group__6__Impl rule__Method__Group__7
            {
            pushFollow(FOLLOW_7);
            rule__Method__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__6"


    // $ANTLR start "rule__Method__Group__6__Impl"
    // InternalJniMap.g:7543:1: rule__Method__Group__6__Impl : ( ')' ) ;
    public final void rule__Method__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7547:1: ( ( ')' ) )
            // InternalJniMap.g:7548:1: ( ')' )
            {
            // InternalJniMap.g:7548:1: ( ')' )
            // InternalJniMap.g:7549:1: ')'
            {
             before(grammarAccess.getMethodAccess().getRightParenthesisKeyword_6()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getRightParenthesisKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__6__Impl"


    // $ANTLR start "rule__Method__Group__7"
    // InternalJniMap.g:7562:1: rule__Method__Group__7 : rule__Method__Group__7__Impl ;
    public final void rule__Method__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7566:1: ( rule__Method__Group__7__Impl )
            // InternalJniMap.g:7567:2: rule__Method__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Method__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__7"


    // $ANTLR start "rule__Method__Group__7__Impl"
    // InternalJniMap.g:7573:1: rule__Method__Group__7__Impl : ( ';' ) ;
    public final void rule__Method__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7577:1: ( ( ';' ) )
            // InternalJniMap.g:7578:1: ( ';' )
            {
            // InternalJniMap.g:7578:1: ( ';' )
            // InternalJniMap.g:7579:1: ';'
            {
             before(grammarAccess.getMethodAccess().getSemicolonKeyword_7()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getSemicolonKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__7__Impl"


    // $ANTLR start "rule__Method__Group_0__0"
    // InternalJniMap.g:7608:1: rule__Method__Group_0__0 : rule__Method__Group_0__0__Impl rule__Method__Group_0__1 ;
    public final void rule__Method__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7612:1: ( rule__Method__Group_0__0__Impl rule__Method__Group_0__1 )
            // InternalJniMap.g:7613:2: rule__Method__Group_0__0__Impl rule__Method__Group_0__1
            {
            pushFollow(FOLLOW_47);
            rule__Method__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0__0"


    // $ANTLR start "rule__Method__Group_0__0__Impl"
    // InternalJniMap.g:7620:1: rule__Method__Group_0__0__Impl : ( '[' ) ;
    public final void rule__Method__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7624:1: ( ( '[' ) )
            // InternalJniMap.g:7625:1: ( '[' )
            {
            // InternalJniMap.g:7625:1: ( '[' )
            // InternalJniMap.g:7626:1: '['
            {
             before(grammarAccess.getMethodAccess().getLeftSquareBracketKeyword_0_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getLeftSquareBracketKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0__0__Impl"


    // $ANTLR start "rule__Method__Group_0__1"
    // InternalJniMap.g:7639:1: rule__Method__Group_0__1 : rule__Method__Group_0__1__Impl rule__Method__Group_0__2 ;
    public final void rule__Method__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7643:1: ( rule__Method__Group_0__1__Impl rule__Method__Group_0__2 )
            // InternalJniMap.g:7644:2: rule__Method__Group_0__1__Impl rule__Method__Group_0__2
            {
            pushFollow(FOLLOW_48);
            rule__Method__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0__1"


    // $ANTLR start "rule__Method__Group_0__1__Impl"
    // InternalJniMap.g:7651:1: rule__Method__Group_0__1__Impl : ( ( rule__Method__UnorderedGroup_0_1 ) ) ;
    public final void rule__Method__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7655:1: ( ( ( rule__Method__UnorderedGroup_0_1 ) ) )
            // InternalJniMap.g:7656:1: ( ( rule__Method__UnorderedGroup_0_1 ) )
            {
            // InternalJniMap.g:7656:1: ( ( rule__Method__UnorderedGroup_0_1 ) )
            // InternalJniMap.g:7657:1: ( rule__Method__UnorderedGroup_0_1 )
            {
             before(grammarAccess.getMethodAccess().getUnorderedGroup_0_1()); 
            // InternalJniMap.g:7658:1: ( rule__Method__UnorderedGroup_0_1 )
            // InternalJniMap.g:7658:2: rule__Method__UnorderedGroup_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Method__UnorderedGroup_0_1();

            state._fsp--;


            }

             after(grammarAccess.getMethodAccess().getUnorderedGroup_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0__1__Impl"


    // $ANTLR start "rule__Method__Group_0__2"
    // InternalJniMap.g:7668:1: rule__Method__Group_0__2 : rule__Method__Group_0__2__Impl ;
    public final void rule__Method__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7672:1: ( rule__Method__Group_0__2__Impl )
            // InternalJniMap.g:7673:2: rule__Method__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Method__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0__2"


    // $ANTLR start "rule__Method__Group_0__2__Impl"
    // InternalJniMap.g:7679:1: rule__Method__Group_0__2__Impl : ( ']' ) ;
    public final void rule__Method__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7683:1: ( ( ']' ) )
            // InternalJniMap.g:7684:1: ( ']' )
            {
            // InternalJniMap.g:7684:1: ( ']' )
            // InternalJniMap.g:7685:1: ']'
            {
             before(grammarAccess.getMethodAccess().getRightSquareBracketKeyword_0_2()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getRightSquareBracketKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0__2__Impl"


    // $ANTLR start "rule__Method__Group_0_1_3__0"
    // InternalJniMap.g:7704:1: rule__Method__Group_0_1_3__0 : rule__Method__Group_0_1_3__0__Impl rule__Method__Group_0_1_3__1 ;
    public final void rule__Method__Group_0_1_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7708:1: ( rule__Method__Group_0_1_3__0__Impl rule__Method__Group_0_1_3__1 )
            // InternalJniMap.g:7709:2: rule__Method__Group_0_1_3__0__Impl rule__Method__Group_0_1_3__1
            {
            pushFollow(FOLLOW_16);
            rule__Method__Group_0_1_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group_0_1_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0_1_3__0"


    // $ANTLR start "rule__Method__Group_0_1_3__0__Impl"
    // InternalJniMap.g:7716:1: rule__Method__Group_0_1_3__0__Impl : ( ( rule__Method__RenamedAssignment_0_1_3_0 ) ) ;
    public final void rule__Method__Group_0_1_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7720:1: ( ( ( rule__Method__RenamedAssignment_0_1_3_0 ) ) )
            // InternalJniMap.g:7721:1: ( ( rule__Method__RenamedAssignment_0_1_3_0 ) )
            {
            // InternalJniMap.g:7721:1: ( ( rule__Method__RenamedAssignment_0_1_3_0 ) )
            // InternalJniMap.g:7722:1: ( rule__Method__RenamedAssignment_0_1_3_0 )
            {
             before(grammarAccess.getMethodAccess().getRenamedAssignment_0_1_3_0()); 
            // InternalJniMap.g:7723:1: ( rule__Method__RenamedAssignment_0_1_3_0 )
            // InternalJniMap.g:7723:2: rule__Method__RenamedAssignment_0_1_3_0
            {
            pushFollow(FOLLOW_2);
            rule__Method__RenamedAssignment_0_1_3_0();

            state._fsp--;


            }

             after(grammarAccess.getMethodAccess().getRenamedAssignment_0_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0_1_3__0__Impl"


    // $ANTLR start "rule__Method__Group_0_1_3__1"
    // InternalJniMap.g:7733:1: rule__Method__Group_0_1_3__1 : rule__Method__Group_0_1_3__1__Impl rule__Method__Group_0_1_3__2 ;
    public final void rule__Method__Group_0_1_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7737:1: ( rule__Method__Group_0_1_3__1__Impl rule__Method__Group_0_1_3__2 )
            // InternalJniMap.g:7738:2: rule__Method__Group_0_1_3__1__Impl rule__Method__Group_0_1_3__2
            {
            pushFollow(FOLLOW_5);
            rule__Method__Group_0_1_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group_0_1_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0_1_3__1"


    // $ANTLR start "rule__Method__Group_0_1_3__1__Impl"
    // InternalJniMap.g:7745:1: rule__Method__Group_0_1_3__1__Impl : ( '=' ) ;
    public final void rule__Method__Group_0_1_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7749:1: ( ( '=' ) )
            // InternalJniMap.g:7750:1: ( '=' )
            {
            // InternalJniMap.g:7750:1: ( '=' )
            // InternalJniMap.g:7751:1: '='
            {
             before(grammarAccess.getMethodAccess().getEqualsSignKeyword_0_1_3_1()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getEqualsSignKeyword_0_1_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0_1_3__1__Impl"


    // $ANTLR start "rule__Method__Group_0_1_3__2"
    // InternalJniMap.g:7764:1: rule__Method__Group_0_1_3__2 : rule__Method__Group_0_1_3__2__Impl ;
    public final void rule__Method__Group_0_1_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7768:1: ( rule__Method__Group_0_1_3__2__Impl )
            // InternalJniMap.g:7769:2: rule__Method__Group_0_1_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Method__Group_0_1_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0_1_3__2"


    // $ANTLR start "rule__Method__Group_0_1_3__2__Impl"
    // InternalJniMap.g:7775:1: rule__Method__Group_0_1_3__2__Impl : ( ( rule__Method__NewnameAssignment_0_1_3_2 ) ) ;
    public final void rule__Method__Group_0_1_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7779:1: ( ( ( rule__Method__NewnameAssignment_0_1_3_2 ) ) )
            // InternalJniMap.g:7780:1: ( ( rule__Method__NewnameAssignment_0_1_3_2 ) )
            {
            // InternalJniMap.g:7780:1: ( ( rule__Method__NewnameAssignment_0_1_3_2 ) )
            // InternalJniMap.g:7781:1: ( rule__Method__NewnameAssignment_0_1_3_2 )
            {
             before(grammarAccess.getMethodAccess().getNewnameAssignment_0_1_3_2()); 
            // InternalJniMap.g:7782:1: ( rule__Method__NewnameAssignment_0_1_3_2 )
            // InternalJniMap.g:7782:2: rule__Method__NewnameAssignment_0_1_3_2
            {
            pushFollow(FOLLOW_2);
            rule__Method__NewnameAssignment_0_1_3_2();

            state._fsp--;


            }

             after(grammarAccess.getMethodAccess().getNewnameAssignment_0_1_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0_1_3__2__Impl"


    // $ANTLR start "rule__Method__Group_0_1_5__0"
    // InternalJniMap.g:7798:1: rule__Method__Group_0_1_5__0 : rule__Method__Group_0_1_5__0__Impl rule__Method__Group_0_1_5__1 ;
    public final void rule__Method__Group_0_1_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7802:1: ( rule__Method__Group_0_1_5__0__Impl rule__Method__Group_0_1_5__1 )
            // InternalJniMap.g:7803:2: rule__Method__Group_0_1_5__0__Impl rule__Method__Group_0_1_5__1
            {
            pushFollow(FOLLOW_16);
            rule__Method__Group_0_1_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group_0_1_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0_1_5__0"


    // $ANTLR start "rule__Method__Group_0_1_5__0__Impl"
    // InternalJniMap.g:7810:1: rule__Method__Group_0_1_5__0__Impl : ( 'this' ) ;
    public final void rule__Method__Group_0_1_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7814:1: ( ( 'this' ) )
            // InternalJniMap.g:7815:1: ( 'this' )
            {
            // InternalJniMap.g:7815:1: ( 'this' )
            // InternalJniMap.g:7816:1: 'this'
            {
             before(grammarAccess.getMethodAccess().getThisKeyword_0_1_5_0()); 
            match(input,55,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getThisKeyword_0_1_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0_1_5__0__Impl"


    // $ANTLR start "rule__Method__Group_0_1_5__1"
    // InternalJniMap.g:7829:1: rule__Method__Group_0_1_5__1 : rule__Method__Group_0_1_5__1__Impl rule__Method__Group_0_1_5__2 ;
    public final void rule__Method__Group_0_1_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7833:1: ( rule__Method__Group_0_1_5__1__Impl rule__Method__Group_0_1_5__2 )
            // InternalJniMap.g:7834:2: rule__Method__Group_0_1_5__1__Impl rule__Method__Group_0_1_5__2
            {
            pushFollow(FOLLOW_49);
            rule__Method__Group_0_1_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group_0_1_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0_1_5__1"


    // $ANTLR start "rule__Method__Group_0_1_5__1__Impl"
    // InternalJniMap.g:7841:1: rule__Method__Group_0_1_5__1__Impl : ( '=' ) ;
    public final void rule__Method__Group_0_1_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7845:1: ( ( '=' ) )
            // InternalJniMap.g:7846:1: ( '=' )
            {
            // InternalJniMap.g:7846:1: ( '=' )
            // InternalJniMap.g:7847:1: '='
            {
             before(grammarAccess.getMethodAccess().getEqualsSignKeyword_0_1_5_1()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getEqualsSignKeyword_0_1_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0_1_5__1__Impl"


    // $ANTLR start "rule__Method__Group_0_1_5__2"
    // InternalJniMap.g:7860:1: rule__Method__Group_0_1_5__2 : rule__Method__Group_0_1_5__2__Impl ;
    public final void rule__Method__Group_0_1_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7864:1: ( rule__Method__Group_0_1_5__2__Impl )
            // InternalJniMap.g:7865:2: rule__Method__Group_0_1_5__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Method__Group_0_1_5__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0_1_5__2"


    // $ANTLR start "rule__Method__Group_0_1_5__2__Impl"
    // InternalJniMap.g:7871:1: rule__Method__Group_0_1_5__2__Impl : ( ( rule__Method__ArgAssignment_0_1_5_2 ) ) ;
    public final void rule__Method__Group_0_1_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7875:1: ( ( ( rule__Method__ArgAssignment_0_1_5_2 ) ) )
            // InternalJniMap.g:7876:1: ( ( rule__Method__ArgAssignment_0_1_5_2 ) )
            {
            // InternalJniMap.g:7876:1: ( ( rule__Method__ArgAssignment_0_1_5_2 ) )
            // InternalJniMap.g:7877:1: ( rule__Method__ArgAssignment_0_1_5_2 )
            {
             before(grammarAccess.getMethodAccess().getArgAssignment_0_1_5_2()); 
            // InternalJniMap.g:7878:1: ( rule__Method__ArgAssignment_0_1_5_2 )
            // InternalJniMap.g:7878:2: rule__Method__ArgAssignment_0_1_5_2
            {
            pushFollow(FOLLOW_2);
            rule__Method__ArgAssignment_0_1_5_2();

            state._fsp--;


            }

             after(grammarAccess.getMethodAccess().getArgAssignment_0_1_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_0_1_5__2__Impl"


    // $ANTLR start "rule__Method__Group_5__0"
    // InternalJniMap.g:7894:1: rule__Method__Group_5__0 : rule__Method__Group_5__0__Impl rule__Method__Group_5__1 ;
    public final void rule__Method__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7898:1: ( rule__Method__Group_5__0__Impl rule__Method__Group_5__1 )
            // InternalJniMap.g:7899:2: rule__Method__Group_5__0__Impl rule__Method__Group_5__1
            {
            pushFollow(FOLLOW_27);
            rule__Method__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_5__0"


    // $ANTLR start "rule__Method__Group_5__0__Impl"
    // InternalJniMap.g:7906:1: rule__Method__Group_5__0__Impl : ( ( rule__Method__ParamsAssignment_5_0 ) ) ;
    public final void rule__Method__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7910:1: ( ( ( rule__Method__ParamsAssignment_5_0 ) ) )
            // InternalJniMap.g:7911:1: ( ( rule__Method__ParamsAssignment_5_0 ) )
            {
            // InternalJniMap.g:7911:1: ( ( rule__Method__ParamsAssignment_5_0 ) )
            // InternalJniMap.g:7912:1: ( rule__Method__ParamsAssignment_5_0 )
            {
             before(grammarAccess.getMethodAccess().getParamsAssignment_5_0()); 
            // InternalJniMap.g:7913:1: ( rule__Method__ParamsAssignment_5_0 )
            // InternalJniMap.g:7913:2: rule__Method__ParamsAssignment_5_0
            {
            pushFollow(FOLLOW_2);
            rule__Method__ParamsAssignment_5_0();

            state._fsp--;


            }

             after(grammarAccess.getMethodAccess().getParamsAssignment_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_5__0__Impl"


    // $ANTLR start "rule__Method__Group_5__1"
    // InternalJniMap.g:7923:1: rule__Method__Group_5__1 : rule__Method__Group_5__1__Impl ;
    public final void rule__Method__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7927:1: ( rule__Method__Group_5__1__Impl )
            // InternalJniMap.g:7928:2: rule__Method__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Method__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_5__1"


    // $ANTLR start "rule__Method__Group_5__1__Impl"
    // InternalJniMap.g:7934:1: rule__Method__Group_5__1__Impl : ( ( rule__Method__Group_5_1__0 )* ) ;
    public final void rule__Method__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7938:1: ( ( ( rule__Method__Group_5_1__0 )* ) )
            // InternalJniMap.g:7939:1: ( ( rule__Method__Group_5_1__0 )* )
            {
            // InternalJniMap.g:7939:1: ( ( rule__Method__Group_5_1__0 )* )
            // InternalJniMap.g:7940:1: ( rule__Method__Group_5_1__0 )*
            {
             before(grammarAccess.getMethodAccess().getGroup_5_1()); 
            // InternalJniMap.g:7941:1: ( rule__Method__Group_5_1__0 )*
            loop60:
            do {
                int alt60=2;
                int LA60_0 = input.LA(1);

                if ( (LA60_0==33) ) {
                    alt60=1;
                }


                switch (alt60) {
            	case 1 :
            	    // InternalJniMap.g:7941:2: rule__Method__Group_5_1__0
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__Method__Group_5_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop60;
                }
            } while (true);

             after(grammarAccess.getMethodAccess().getGroup_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_5__1__Impl"


    // $ANTLR start "rule__Method__Group_5_1__0"
    // InternalJniMap.g:7955:1: rule__Method__Group_5_1__0 : rule__Method__Group_5_1__0__Impl rule__Method__Group_5_1__1 ;
    public final void rule__Method__Group_5_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7959:1: ( rule__Method__Group_5_1__0__Impl rule__Method__Group_5_1__1 )
            // InternalJniMap.g:7960:2: rule__Method__Group_5_1__0__Impl rule__Method__Group_5_1__1
            {
            pushFollow(FOLLOW_40);
            rule__Method__Group_5_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Method__Group_5_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_5_1__0"


    // $ANTLR start "rule__Method__Group_5_1__0__Impl"
    // InternalJniMap.g:7967:1: rule__Method__Group_5_1__0__Impl : ( ',' ) ;
    public final void rule__Method__Group_5_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7971:1: ( ( ',' ) )
            // InternalJniMap.g:7972:1: ( ',' )
            {
            // InternalJniMap.g:7972:1: ( ',' )
            // InternalJniMap.g:7973:1: ','
            {
             before(grammarAccess.getMethodAccess().getCommaKeyword_5_1_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getCommaKeyword_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_5_1__0__Impl"


    // $ANTLR start "rule__Method__Group_5_1__1"
    // InternalJniMap.g:7986:1: rule__Method__Group_5_1__1 : rule__Method__Group_5_1__1__Impl ;
    public final void rule__Method__Group_5_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:7990:1: ( rule__Method__Group_5_1__1__Impl )
            // InternalJniMap.g:7991:2: rule__Method__Group_5_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Method__Group_5_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_5_1__1"


    // $ANTLR start "rule__Method__Group_5_1__1__Impl"
    // InternalJniMap.g:7997:1: rule__Method__Group_5_1__1__Impl : ( ( rule__Method__ParamsAssignment_5_1_1 ) ) ;
    public final void rule__Method__Group_5_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8001:1: ( ( ( rule__Method__ParamsAssignment_5_1_1 ) ) )
            // InternalJniMap.g:8002:1: ( ( rule__Method__ParamsAssignment_5_1_1 ) )
            {
            // InternalJniMap.g:8002:1: ( ( rule__Method__ParamsAssignment_5_1_1 ) )
            // InternalJniMap.g:8003:1: ( rule__Method__ParamsAssignment_5_1_1 )
            {
             before(grammarAccess.getMethodAccess().getParamsAssignment_5_1_1()); 
            // InternalJniMap.g:8004:1: ( rule__Method__ParamsAssignment_5_1_1 )
            // InternalJniMap.g:8004:2: rule__Method__ParamsAssignment_5_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Method__ParamsAssignment_5_1_1();

            state._fsp--;


            }

             after(grammarAccess.getMethodAccess().getParamsAssignment_5_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_5_1__1__Impl"


    // $ANTLR start "rule__ArrayType__Group__0"
    // InternalJniMap.g:8018:1: rule__ArrayType__Group__0 : rule__ArrayType__Group__0__Impl rule__ArrayType__Group__1 ;
    public final void rule__ArrayType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8022:1: ( rule__ArrayType__Group__0__Impl rule__ArrayType__Group__1 )
            // InternalJniMap.g:8023:2: rule__ArrayType__Group__0__Impl rule__ArrayType__Group__1
            {
            pushFollow(FOLLOW_50);
            rule__ArrayType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArrayType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArrayType__Group__0"


    // $ANTLR start "rule__ArrayType__Group__0__Impl"
    // InternalJniMap.g:8030:1: rule__ArrayType__Group__0__Impl : ( ( rule__ArrayType__BaseAssignment_0 ) ) ;
    public final void rule__ArrayType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8034:1: ( ( ( rule__ArrayType__BaseAssignment_0 ) ) )
            // InternalJniMap.g:8035:1: ( ( rule__ArrayType__BaseAssignment_0 ) )
            {
            // InternalJniMap.g:8035:1: ( ( rule__ArrayType__BaseAssignment_0 ) )
            // InternalJniMap.g:8036:1: ( rule__ArrayType__BaseAssignment_0 )
            {
             before(grammarAccess.getArrayTypeAccess().getBaseAssignment_0()); 
            // InternalJniMap.g:8037:1: ( rule__ArrayType__BaseAssignment_0 )
            // InternalJniMap.g:8037:2: rule__ArrayType__BaseAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ArrayType__BaseAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getArrayTypeAccess().getBaseAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArrayType__Group__0__Impl"


    // $ANTLR start "rule__ArrayType__Group__1"
    // InternalJniMap.g:8047:1: rule__ArrayType__Group__1 : rule__ArrayType__Group__1__Impl rule__ArrayType__Group__2 ;
    public final void rule__ArrayType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8051:1: ( rule__ArrayType__Group__1__Impl rule__ArrayType__Group__2 )
            // InternalJniMap.g:8052:2: rule__ArrayType__Group__1__Impl rule__ArrayType__Group__2
            {
            pushFollow(FOLLOW_48);
            rule__ArrayType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArrayType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArrayType__Group__1"


    // $ANTLR start "rule__ArrayType__Group__1__Impl"
    // InternalJniMap.g:8059:1: rule__ArrayType__Group__1__Impl : ( '[' ) ;
    public final void rule__ArrayType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8063:1: ( ( '[' ) )
            // InternalJniMap.g:8064:1: ( '[' )
            {
            // InternalJniMap.g:8064:1: ( '[' )
            // InternalJniMap.g:8065:1: '['
            {
             before(grammarAccess.getArrayTypeAccess().getLeftSquareBracketKeyword_1()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getArrayTypeAccess().getLeftSquareBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArrayType__Group__1__Impl"


    // $ANTLR start "rule__ArrayType__Group__2"
    // InternalJniMap.g:8078:1: rule__ArrayType__Group__2 : rule__ArrayType__Group__2__Impl ;
    public final void rule__ArrayType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8082:1: ( rule__ArrayType__Group__2__Impl )
            // InternalJniMap.g:8083:2: rule__ArrayType__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ArrayType__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArrayType__Group__2"


    // $ANTLR start "rule__ArrayType__Group__2__Impl"
    // InternalJniMap.g:8089:1: rule__ArrayType__Group__2__Impl : ( ']' ) ;
    public final void rule__ArrayType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8093:1: ( ( ']' ) )
            // InternalJniMap.g:8094:1: ( ']' )
            {
            // InternalJniMap.g:8094:1: ( ']' )
            // InternalJniMap.g:8095:1: ']'
            {
             before(grammarAccess.getArrayTypeAccess().getRightSquareBracketKeyword_2()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getArrayTypeAccess().getRightSquareBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArrayType__Group__2__Impl"


    // $ANTLR start "rule__PtrType__Group__0"
    // InternalJniMap.g:8114:1: rule__PtrType__Group__0 : rule__PtrType__Group__0__Impl rule__PtrType__Group__1 ;
    public final void rule__PtrType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8118:1: ( rule__PtrType__Group__0__Impl rule__PtrType__Group__1 )
            // InternalJniMap.g:8119:2: rule__PtrType__Group__0__Impl rule__PtrType__Group__1
            {
            pushFollow(FOLLOW_51);
            rule__PtrType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PtrType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PtrType__Group__0"


    // $ANTLR start "rule__PtrType__Group__0__Impl"
    // InternalJniMap.g:8126:1: rule__PtrType__Group__0__Impl : ( ( rule__PtrType__BaseAssignment_0 ) ) ;
    public final void rule__PtrType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8130:1: ( ( ( rule__PtrType__BaseAssignment_0 ) ) )
            // InternalJniMap.g:8131:1: ( ( rule__PtrType__BaseAssignment_0 ) )
            {
            // InternalJniMap.g:8131:1: ( ( rule__PtrType__BaseAssignment_0 ) )
            // InternalJniMap.g:8132:1: ( rule__PtrType__BaseAssignment_0 )
            {
             before(grammarAccess.getPtrTypeAccess().getBaseAssignment_0()); 
            // InternalJniMap.g:8133:1: ( rule__PtrType__BaseAssignment_0 )
            // InternalJniMap.g:8133:2: rule__PtrType__BaseAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__PtrType__BaseAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPtrTypeAccess().getBaseAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PtrType__Group__0__Impl"


    // $ANTLR start "rule__PtrType__Group__1"
    // InternalJniMap.g:8143:1: rule__PtrType__Group__1 : rule__PtrType__Group__1__Impl rule__PtrType__Group__2 ;
    public final void rule__PtrType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8147:1: ( rule__PtrType__Group__1__Impl rule__PtrType__Group__2 )
            // InternalJniMap.g:8148:2: rule__PtrType__Group__1__Impl rule__PtrType__Group__2
            {
            pushFollow(FOLLOW_52);
            rule__PtrType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PtrType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PtrType__Group__1"


    // $ANTLR start "rule__PtrType__Group__1__Impl"
    // InternalJniMap.g:8155:1: rule__PtrType__Group__1__Impl : ( ( ( rule__PtrType__IndirAssignment_1 ) ) ( ( rule__PtrType__IndirAssignment_1 )* ) ) ;
    public final void rule__PtrType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8159:1: ( ( ( ( rule__PtrType__IndirAssignment_1 ) ) ( ( rule__PtrType__IndirAssignment_1 )* ) ) )
            // InternalJniMap.g:8160:1: ( ( ( rule__PtrType__IndirAssignment_1 ) ) ( ( rule__PtrType__IndirAssignment_1 )* ) )
            {
            // InternalJniMap.g:8160:1: ( ( ( rule__PtrType__IndirAssignment_1 ) ) ( ( rule__PtrType__IndirAssignment_1 )* ) )
            // InternalJniMap.g:8161:1: ( ( rule__PtrType__IndirAssignment_1 ) ) ( ( rule__PtrType__IndirAssignment_1 )* )
            {
            // InternalJniMap.g:8161:1: ( ( rule__PtrType__IndirAssignment_1 ) )
            // InternalJniMap.g:8162:1: ( rule__PtrType__IndirAssignment_1 )
            {
             before(grammarAccess.getPtrTypeAccess().getIndirAssignment_1()); 
            // InternalJniMap.g:8163:1: ( rule__PtrType__IndirAssignment_1 )
            // InternalJniMap.g:8163:2: rule__PtrType__IndirAssignment_1
            {
            pushFollow(FOLLOW_53);
            rule__PtrType__IndirAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPtrTypeAccess().getIndirAssignment_1()); 

            }

            // InternalJniMap.g:8166:1: ( ( rule__PtrType__IndirAssignment_1 )* )
            // InternalJniMap.g:8167:1: ( rule__PtrType__IndirAssignment_1 )*
            {
             before(grammarAccess.getPtrTypeAccess().getIndirAssignment_1()); 
            // InternalJniMap.g:8168:1: ( rule__PtrType__IndirAssignment_1 )*
            loop61:
            do {
                int alt61=2;
                int LA61_0 = input.LA(1);

                if ( (LA61_0==71) ) {
                    alt61=1;
                }


                switch (alt61) {
            	case 1 :
            	    // InternalJniMap.g:8168:2: rule__PtrType__IndirAssignment_1
            	    {
            	    pushFollow(FOLLOW_53);
            	    rule__PtrType__IndirAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop61;
                }
            } while (true);

             after(grammarAccess.getPtrTypeAccess().getIndirAssignment_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PtrType__Group__1__Impl"


    // $ANTLR start "rule__PtrType__Group__2"
    // InternalJniMap.g:8179:1: rule__PtrType__Group__2 : rule__PtrType__Group__2__Impl ;
    public final void rule__PtrType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8183:1: ( rule__PtrType__Group__2__Impl )
            // InternalJniMap.g:8184:2: rule__PtrType__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PtrType__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PtrType__Group__2"


    // $ANTLR start "rule__PtrType__Group__2__Impl"
    // InternalJniMap.g:8190:1: rule__PtrType__Group__2__Impl : ( ( rule__PtrType__IgnoredAssignment_2 )? ) ;
    public final void rule__PtrType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8194:1: ( ( ( rule__PtrType__IgnoredAssignment_2 )? ) )
            // InternalJniMap.g:8195:1: ( ( rule__PtrType__IgnoredAssignment_2 )? )
            {
            // InternalJniMap.g:8195:1: ( ( rule__PtrType__IgnoredAssignment_2 )? )
            // InternalJniMap.g:8196:1: ( rule__PtrType__IgnoredAssignment_2 )?
            {
             before(grammarAccess.getPtrTypeAccess().getIgnoredAssignment_2()); 
            // InternalJniMap.g:8197:1: ( rule__PtrType__IgnoredAssignment_2 )?
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==72) ) {
                alt62=1;
            }
            switch (alt62) {
                case 1 :
                    // InternalJniMap.g:8197:2: rule__PtrType__IgnoredAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__PtrType__IgnoredAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPtrTypeAccess().getIgnoredAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PtrType__Group__2__Impl"


    // $ANTLR start "rule__ModifiableType__Group__0"
    // InternalJniMap.g:8213:1: rule__ModifiableType__Group__0 : rule__ModifiableType__Group__0__Impl rule__ModifiableType__Group__1 ;
    public final void rule__ModifiableType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8217:1: ( rule__ModifiableType__Group__0__Impl rule__ModifiableType__Group__1 )
            // InternalJniMap.g:8218:2: rule__ModifiableType__Group__0__Impl rule__ModifiableType__Group__1
            {
            pushFollow(FOLLOW_54);
            rule__ModifiableType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ModifiableType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModifiableType__Group__0"


    // $ANTLR start "rule__ModifiableType__Group__0__Impl"
    // InternalJniMap.g:8225:1: rule__ModifiableType__Group__0__Impl : ( ( rule__ModifiableType__BaseAssignment_0 ) ) ;
    public final void rule__ModifiableType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8229:1: ( ( ( rule__ModifiableType__BaseAssignment_0 ) ) )
            // InternalJniMap.g:8230:1: ( ( rule__ModifiableType__BaseAssignment_0 ) )
            {
            // InternalJniMap.g:8230:1: ( ( rule__ModifiableType__BaseAssignment_0 ) )
            // InternalJniMap.g:8231:1: ( rule__ModifiableType__BaseAssignment_0 )
            {
             before(grammarAccess.getModifiableTypeAccess().getBaseAssignment_0()); 
            // InternalJniMap.g:8232:1: ( rule__ModifiableType__BaseAssignment_0 )
            // InternalJniMap.g:8232:2: rule__ModifiableType__BaseAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ModifiableType__BaseAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getModifiableTypeAccess().getBaseAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModifiableType__Group__0__Impl"


    // $ANTLR start "rule__ModifiableType__Group__1"
    // InternalJniMap.g:8242:1: rule__ModifiableType__Group__1 : rule__ModifiableType__Group__1__Impl ;
    public final void rule__ModifiableType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8246:1: ( rule__ModifiableType__Group__1__Impl )
            // InternalJniMap.g:8247:2: rule__ModifiableType__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ModifiableType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModifiableType__Group__1"


    // $ANTLR start "rule__ModifiableType__Group__1__Impl"
    // InternalJniMap.g:8253:1: rule__ModifiableType__Group__1__Impl : ( ( rule__ModifiableType__IndirAssignment_1 ) ) ;
    public final void rule__ModifiableType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8257:1: ( ( ( rule__ModifiableType__IndirAssignment_1 ) ) )
            // InternalJniMap.g:8258:1: ( ( rule__ModifiableType__IndirAssignment_1 ) )
            {
            // InternalJniMap.g:8258:1: ( ( rule__ModifiableType__IndirAssignment_1 ) )
            // InternalJniMap.g:8259:1: ( rule__ModifiableType__IndirAssignment_1 )
            {
             before(grammarAccess.getModifiableTypeAccess().getIndirAssignment_1()); 
            // InternalJniMap.g:8260:1: ( rule__ModifiableType__IndirAssignment_1 )
            // InternalJniMap.g:8260:2: rule__ModifiableType__IndirAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ModifiableType__IndirAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getModifiableTypeAccess().getIndirAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModifiableType__Group__1__Impl"


    // $ANTLR start "rule__StructType__Group__0"
    // InternalJniMap.g:8274:1: rule__StructType__Group__0 : rule__StructType__Group__0__Impl rule__StructType__Group__1 ;
    public final void rule__StructType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8278:1: ( rule__StructType__Group__0__Impl rule__StructType__Group__1 )
            // InternalJniMap.g:8279:2: rule__StructType__Group__0__Impl rule__StructType__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__StructType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__0"


    // $ANTLR start "rule__StructType__Group__0__Impl"
    // InternalJniMap.g:8286:1: rule__StructType__Group__0__Impl : ( 'struct' ) ;
    public final void rule__StructType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8290:1: ( ( 'struct' ) )
            // InternalJniMap.g:8291:1: ( 'struct' )
            {
            // InternalJniMap.g:8291:1: ( 'struct' )
            // InternalJniMap.g:8292:1: 'struct'
            {
             before(grammarAccess.getStructTypeAccess().getStructKeyword_0()); 
            match(input,51,FOLLOW_2); 
             after(grammarAccess.getStructTypeAccess().getStructKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__0__Impl"


    // $ANTLR start "rule__StructType__Group__1"
    // InternalJniMap.g:8305:1: rule__StructType__Group__1 : rule__StructType__Group__1__Impl ;
    public final void rule__StructType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8309:1: ( rule__StructType__Group__1__Impl )
            // InternalJniMap.g:8310:2: rule__StructType__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StructType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__1"


    // $ANTLR start "rule__StructType__Group__1__Impl"
    // InternalJniMap.g:8316:1: rule__StructType__Group__1__Impl : ( ( rule__StructType__NameAssignment_1 ) ) ;
    public final void rule__StructType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8320:1: ( ( ( rule__StructType__NameAssignment_1 ) ) )
            // InternalJniMap.g:8321:1: ( ( rule__StructType__NameAssignment_1 ) )
            {
            // InternalJniMap.g:8321:1: ( ( rule__StructType__NameAssignment_1 ) )
            // InternalJniMap.g:8322:1: ( rule__StructType__NameAssignment_1 )
            {
             before(grammarAccess.getStructTypeAccess().getNameAssignment_1()); 
            // InternalJniMap.g:8323:1: ( rule__StructType__NameAssignment_1 )
            // InternalJniMap.g:8323:2: rule__StructType__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__StructType__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStructTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__1__Impl"


    // $ANTLR start "rule__FieldDef__Group__0"
    // InternalJniMap.g:8337:1: rule__FieldDef__Group__0 : rule__FieldDef__Group__0__Impl rule__FieldDef__Group__1 ;
    public final void rule__FieldDef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8341:1: ( rule__FieldDef__Group__0__Impl rule__FieldDef__Group__1 )
            // InternalJniMap.g:8342:2: rule__FieldDef__Group__0__Impl rule__FieldDef__Group__1
            {
            pushFollow(FOLLOW_41);
            rule__FieldDef__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FieldDef__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group__0"


    // $ANTLR start "rule__FieldDef__Group__0__Impl"
    // InternalJniMap.g:8349:1: rule__FieldDef__Group__0__Impl : ( ( rule__FieldDef__CopyAssignment_0 )? ) ;
    public final void rule__FieldDef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8353:1: ( ( ( rule__FieldDef__CopyAssignment_0 )? ) )
            // InternalJniMap.g:8354:1: ( ( rule__FieldDef__CopyAssignment_0 )? )
            {
            // InternalJniMap.g:8354:1: ( ( rule__FieldDef__CopyAssignment_0 )? )
            // InternalJniMap.g:8355:1: ( rule__FieldDef__CopyAssignment_0 )?
            {
             before(grammarAccess.getFieldDefAccess().getCopyAssignment_0()); 
            // InternalJniMap.g:8356:1: ( rule__FieldDef__CopyAssignment_0 )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==74) ) {
                alt63=1;
            }
            switch (alt63) {
                case 1 :
                    // InternalJniMap.g:8356:2: rule__FieldDef__CopyAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__FieldDef__CopyAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFieldDefAccess().getCopyAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group__0__Impl"


    // $ANTLR start "rule__FieldDef__Group__1"
    // InternalJniMap.g:8366:1: rule__FieldDef__Group__1 : rule__FieldDef__Group__1__Impl rule__FieldDef__Group__2 ;
    public final void rule__FieldDef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8370:1: ( rule__FieldDef__Group__1__Impl rule__FieldDef__Group__2 )
            // InternalJniMap.g:8371:2: rule__FieldDef__Group__1__Impl rule__FieldDef__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__FieldDef__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FieldDef__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group__1"


    // $ANTLR start "rule__FieldDef__Group__1__Impl"
    // InternalJniMap.g:8378:1: rule__FieldDef__Group__1__Impl : ( ( rule__FieldDef__TypeAssignment_1 ) ) ;
    public final void rule__FieldDef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8382:1: ( ( ( rule__FieldDef__TypeAssignment_1 ) ) )
            // InternalJniMap.g:8383:1: ( ( rule__FieldDef__TypeAssignment_1 ) )
            {
            // InternalJniMap.g:8383:1: ( ( rule__FieldDef__TypeAssignment_1 ) )
            // InternalJniMap.g:8384:1: ( rule__FieldDef__TypeAssignment_1 )
            {
             before(grammarAccess.getFieldDefAccess().getTypeAssignment_1()); 
            // InternalJniMap.g:8385:1: ( rule__FieldDef__TypeAssignment_1 )
            // InternalJniMap.g:8385:2: rule__FieldDef__TypeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__FieldDef__TypeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFieldDefAccess().getTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group__1__Impl"


    // $ANTLR start "rule__FieldDef__Group__2"
    // InternalJniMap.g:8395:1: rule__FieldDef__Group__2 : rule__FieldDef__Group__2__Impl rule__FieldDef__Group__3 ;
    public final void rule__FieldDef__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8399:1: ( rule__FieldDef__Group__2__Impl rule__FieldDef__Group__3 )
            // InternalJniMap.g:8400:2: rule__FieldDef__Group__2__Impl rule__FieldDef__Group__3
            {
            pushFollow(FOLLOW_50);
            rule__FieldDef__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FieldDef__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group__2"


    // $ANTLR start "rule__FieldDef__Group__2__Impl"
    // InternalJniMap.g:8407:1: rule__FieldDef__Group__2__Impl : ( ( rule__FieldDef__NameAssignment_2 ) ) ;
    public final void rule__FieldDef__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8411:1: ( ( ( rule__FieldDef__NameAssignment_2 ) ) )
            // InternalJniMap.g:8412:1: ( ( rule__FieldDef__NameAssignment_2 ) )
            {
            // InternalJniMap.g:8412:1: ( ( rule__FieldDef__NameAssignment_2 ) )
            // InternalJniMap.g:8413:1: ( rule__FieldDef__NameAssignment_2 )
            {
             before(grammarAccess.getFieldDefAccess().getNameAssignment_2()); 
            // InternalJniMap.g:8414:1: ( rule__FieldDef__NameAssignment_2 )
            // InternalJniMap.g:8414:2: rule__FieldDef__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__FieldDef__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFieldDefAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group__2__Impl"


    // $ANTLR start "rule__FieldDef__Group__3"
    // InternalJniMap.g:8424:1: rule__FieldDef__Group__3 : rule__FieldDef__Group__3__Impl ;
    public final void rule__FieldDef__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8428:1: ( rule__FieldDef__Group__3__Impl )
            // InternalJniMap.g:8429:2: rule__FieldDef__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FieldDef__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group__3"


    // $ANTLR start "rule__FieldDef__Group__3__Impl"
    // InternalJniMap.g:8435:1: rule__FieldDef__Group__3__Impl : ( ( rule__FieldDef__Group_3__0 )? ) ;
    public final void rule__FieldDef__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8439:1: ( ( ( rule__FieldDef__Group_3__0 )? ) )
            // InternalJniMap.g:8440:1: ( ( rule__FieldDef__Group_3__0 )? )
            {
            // InternalJniMap.g:8440:1: ( ( rule__FieldDef__Group_3__0 )? )
            // InternalJniMap.g:8441:1: ( rule__FieldDef__Group_3__0 )?
            {
             before(grammarAccess.getFieldDefAccess().getGroup_3()); 
            // InternalJniMap.g:8442:1: ( rule__FieldDef__Group_3__0 )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==31) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // InternalJniMap.g:8442:2: rule__FieldDef__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__FieldDef__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFieldDefAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group__3__Impl"


    // $ANTLR start "rule__FieldDef__Group_3__0"
    // InternalJniMap.g:8460:1: rule__FieldDef__Group_3__0 : rule__FieldDef__Group_3__0__Impl rule__FieldDef__Group_3__1 ;
    public final void rule__FieldDef__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8464:1: ( rule__FieldDef__Group_3__0__Impl rule__FieldDef__Group_3__1 )
            // InternalJniMap.g:8465:2: rule__FieldDef__Group_3__0__Impl rule__FieldDef__Group_3__1
            {
            pushFollow(FOLLOW_55);
            rule__FieldDef__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FieldDef__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group_3__0"


    // $ANTLR start "rule__FieldDef__Group_3__0__Impl"
    // InternalJniMap.g:8472:1: rule__FieldDef__Group_3__0__Impl : ( '[' ) ;
    public final void rule__FieldDef__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8476:1: ( ( '[' ) )
            // InternalJniMap.g:8477:1: ( '[' )
            {
            // InternalJniMap.g:8477:1: ( '[' )
            // InternalJniMap.g:8478:1: '['
            {
             before(grammarAccess.getFieldDefAccess().getLeftSquareBracketKeyword_3_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getFieldDefAccess().getLeftSquareBracketKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group_3__0__Impl"


    // $ANTLR start "rule__FieldDef__Group_3__1"
    // InternalJniMap.g:8491:1: rule__FieldDef__Group_3__1 : rule__FieldDef__Group_3__1__Impl rule__FieldDef__Group_3__2 ;
    public final void rule__FieldDef__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8495:1: ( rule__FieldDef__Group_3__1__Impl rule__FieldDef__Group_3__2 )
            // InternalJniMap.g:8496:2: rule__FieldDef__Group_3__1__Impl rule__FieldDef__Group_3__2
            {
            pushFollow(FOLLOW_48);
            rule__FieldDef__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FieldDef__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group_3__1"


    // $ANTLR start "rule__FieldDef__Group_3__1__Impl"
    // InternalJniMap.g:8503:1: rule__FieldDef__Group_3__1__Impl : ( ( rule__FieldDef__ManyAssignment_3_1 ) ) ;
    public final void rule__FieldDef__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8507:1: ( ( ( rule__FieldDef__ManyAssignment_3_1 ) ) )
            // InternalJniMap.g:8508:1: ( ( rule__FieldDef__ManyAssignment_3_1 ) )
            {
            // InternalJniMap.g:8508:1: ( ( rule__FieldDef__ManyAssignment_3_1 ) )
            // InternalJniMap.g:8509:1: ( rule__FieldDef__ManyAssignment_3_1 )
            {
             before(grammarAccess.getFieldDefAccess().getManyAssignment_3_1()); 
            // InternalJniMap.g:8510:1: ( rule__FieldDef__ManyAssignment_3_1 )
            // InternalJniMap.g:8510:2: rule__FieldDef__ManyAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__FieldDef__ManyAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getFieldDefAccess().getManyAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group_3__1__Impl"


    // $ANTLR start "rule__FieldDef__Group_3__2"
    // InternalJniMap.g:8520:1: rule__FieldDef__Group_3__2 : rule__FieldDef__Group_3__2__Impl ;
    public final void rule__FieldDef__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8524:1: ( rule__FieldDef__Group_3__2__Impl )
            // InternalJniMap.g:8525:2: rule__FieldDef__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FieldDef__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group_3__2"


    // $ANTLR start "rule__FieldDef__Group_3__2__Impl"
    // InternalJniMap.g:8531:1: rule__FieldDef__Group_3__2__Impl : ( ']' ) ;
    public final void rule__FieldDef__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8535:1: ( ( ']' ) )
            // InternalJniMap.g:8536:1: ( ']' )
            {
            // InternalJniMap.g:8536:1: ( ']' )
            // InternalJniMap.g:8537:1: ']'
            {
             before(grammarAccess.getFieldDefAccess().getRightSquareBracketKeyword_3_2()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getFieldDefAccess().getRightSquareBracketKeyword_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__Group_3__2__Impl"


    // $ANTLR start "rule__ConstType__Group__0"
    // InternalJniMap.g:8556:1: rule__ConstType__Group__0 : rule__ConstType__Group__0__Impl rule__ConstType__Group__1 ;
    public final void rule__ConstType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8560:1: ( rule__ConstType__Group__0__Impl rule__ConstType__Group__1 )
            // InternalJniMap.g:8561:2: rule__ConstType__Group__0__Impl rule__ConstType__Group__1
            {
            pushFollow(FOLLOW_37);
            rule__ConstType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConstType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstType__Group__0"


    // $ANTLR start "rule__ConstType__Group__0__Impl"
    // InternalJniMap.g:8568:1: rule__ConstType__Group__0__Impl : ( 'const' ) ;
    public final void rule__ConstType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8572:1: ( ( 'const' ) )
            // InternalJniMap.g:8573:1: ( 'const' )
            {
            // InternalJniMap.g:8573:1: ( 'const' )
            // InternalJniMap.g:8574:1: 'const'
            {
             before(grammarAccess.getConstTypeAccess().getConstKeyword_0()); 
            match(input,56,FOLLOW_2); 
             after(grammarAccess.getConstTypeAccess().getConstKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstType__Group__0__Impl"


    // $ANTLR start "rule__ConstType__Group__1"
    // InternalJniMap.g:8587:1: rule__ConstType__Group__1 : rule__ConstType__Group__1__Impl ;
    public final void rule__ConstType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8591:1: ( rule__ConstType__Group__1__Impl )
            // InternalJniMap.g:8592:2: rule__ConstType__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ConstType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstType__Group__1"


    // $ANTLR start "rule__ConstType__Group__1__Impl"
    // InternalJniMap.g:8598:1: rule__ConstType__Group__1__Impl : ( ( rule__ConstType__BaseAssignment_1 ) ) ;
    public final void rule__ConstType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8602:1: ( ( ( rule__ConstType__BaseAssignment_1 ) ) )
            // InternalJniMap.g:8603:1: ( ( rule__ConstType__BaseAssignment_1 ) )
            {
            // InternalJniMap.g:8603:1: ( ( rule__ConstType__BaseAssignment_1 ) )
            // InternalJniMap.g:8604:1: ( rule__ConstType__BaseAssignment_1 )
            {
             before(grammarAccess.getConstTypeAccess().getBaseAssignment_1()); 
            // InternalJniMap.g:8605:1: ( rule__ConstType__BaseAssignment_1 )
            // InternalJniMap.g:8605:2: rule__ConstType__BaseAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ConstType__BaseAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getConstTypeAccess().getBaseAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstType__Group__1__Impl"


    // $ANTLR start "rule__Method__UnorderedGroup_0_1"
    // InternalJniMap.g:8620:1: rule__Method__UnorderedGroup_0_1 : ( rule__Method__UnorderedGroup_0_1__0 )? ;
    public final void rule__Method__UnorderedGroup_0_1() throws RecognitionException {

            	int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getMethodAccess().getUnorderedGroup_0_1());
            
        try {
            // InternalJniMap.g:8625:1: ( ( rule__Method__UnorderedGroup_0_1__0 )? )
            // InternalJniMap.g:8626:2: ( rule__Method__UnorderedGroup_0_1__0 )?
            {
            // InternalJniMap.g:8626:2: ( rule__Method__UnorderedGroup_0_1__0 )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( LA65_0 >= 61 && LA65_0 <= 63 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {
                alt65=1;
            }
            else if ( LA65_0 == 12 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1) ) {
                alt65=1;
            }
            else if ( LA65_0 >= 64 && LA65_0 <= 65 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2) ) {
                alt65=1;
            }
            else if ( LA65_0 == 66 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3) ) {
                alt65=1;
            }
            else if ( LA65_0 == 67 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4) ) {
                alt65=1;
            }
            else if ( LA65_0 == 55 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5) ) {
                alt65=1;
            }
            switch (alt65) {
                case 1 :
                    // InternalJniMap.g:8626:2: rule__Method__UnorderedGroup_0_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__UnorderedGroup_0_1__0();

                    state._fsp--;


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getMethodAccess().getUnorderedGroup_0_1());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__UnorderedGroup_0_1"


    // $ANTLR start "rule__Method__UnorderedGroup_0_1__Impl"
    // InternalJniMap.g:8636:1: rule__Method__UnorderedGroup_0_1__Impl : ( ({...}? => ( ( ( rule__Method__Alternatives_0_1_0 ) ) ) ) | ({...}? => ( ( ( rule__Method__StaticAssignment_0_1_1 ) ) ) ) | ({...}? => ( ( ( rule__Method__Alternatives_0_1_2 ) ) ) ) | ({...}? => ( ( ( rule__Method__Group_0_1_3__0 ) ) ) ) | ({...}? => ( ( ( rule__Method__StubAssignment_0_1_4 ) ) ) ) | ({...}? => ( ( ( rule__Method__Group_0_1_5__0 ) ) ) ) ) ;
    public final void rule__Method__UnorderedGroup_0_1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
            
        try {
            // InternalJniMap.g:8641:1: ( ( ({...}? => ( ( ( rule__Method__Alternatives_0_1_0 ) ) ) ) | ({...}? => ( ( ( rule__Method__StaticAssignment_0_1_1 ) ) ) ) | ({...}? => ( ( ( rule__Method__Alternatives_0_1_2 ) ) ) ) | ({...}? => ( ( ( rule__Method__Group_0_1_3__0 ) ) ) ) | ({...}? => ( ( ( rule__Method__StubAssignment_0_1_4 ) ) ) ) | ({...}? => ( ( ( rule__Method__Group_0_1_5__0 ) ) ) ) ) )
            // InternalJniMap.g:8642:3: ( ({...}? => ( ( ( rule__Method__Alternatives_0_1_0 ) ) ) ) | ({...}? => ( ( ( rule__Method__StaticAssignment_0_1_1 ) ) ) ) | ({...}? => ( ( ( rule__Method__Alternatives_0_1_2 ) ) ) ) | ({...}? => ( ( ( rule__Method__Group_0_1_3__0 ) ) ) ) | ({...}? => ( ( ( rule__Method__StubAssignment_0_1_4 ) ) ) ) | ({...}? => ( ( ( rule__Method__Group_0_1_5__0 ) ) ) ) )
            {
            // InternalJniMap.g:8642:3: ( ({...}? => ( ( ( rule__Method__Alternatives_0_1_0 ) ) ) ) | ({...}? => ( ( ( rule__Method__StaticAssignment_0_1_1 ) ) ) ) | ({...}? => ( ( ( rule__Method__Alternatives_0_1_2 ) ) ) ) | ({...}? => ( ( ( rule__Method__Group_0_1_3__0 ) ) ) ) | ({...}? => ( ( ( rule__Method__StubAssignment_0_1_4 ) ) ) ) | ({...}? => ( ( ( rule__Method__Group_0_1_5__0 ) ) ) ) )
            int alt66=6;
            int LA66_0 = input.LA(1);

            if ( LA66_0 >= 61 && LA66_0 <= 63 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {
                alt66=1;
            }
            else if ( LA66_0 == 12 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1) ) {
                alt66=2;
            }
            else if ( LA66_0 >= 64 && LA66_0 <= 65 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2) ) {
                alt66=3;
            }
            else if ( LA66_0 == 66 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3) ) {
                alt66=4;
            }
            else if ( LA66_0 == 67 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4) ) {
                alt66=5;
            }
            else if ( LA66_0 == 55 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5) ) {
                alt66=6;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 66, 0, input);

                throw nvae;
            }
            switch (alt66) {
                case 1 :
                    // InternalJniMap.g:8644:4: ({...}? => ( ( ( rule__Method__Alternatives_0_1_0 ) ) ) )
                    {
                    // InternalJniMap.g:8644:4: ({...}? => ( ( ( rule__Method__Alternatives_0_1_0 ) ) ) )
                    // InternalJniMap.g:8645:5: {...}? => ( ( ( rule__Method__Alternatives_0_1_0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {
                        throw new FailedPredicateException(input, "rule__Method__UnorderedGroup_0_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0)");
                    }
                    // InternalJniMap.g:8645:105: ( ( ( rule__Method__Alternatives_0_1_0 ) ) )
                    // InternalJniMap.g:8646:6: ( ( rule__Method__Alternatives_0_1_0 ) )
                    {
                     
                    	 				  getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0);
                    	 				

                    	 				  selected = true;
                    	 				
                    // InternalJniMap.g:8652:6: ( ( rule__Method__Alternatives_0_1_0 ) )
                    // InternalJniMap.g:8654:7: ( rule__Method__Alternatives_0_1_0 )
                    {
                     before(grammarAccess.getMethodAccess().getAlternatives_0_1_0()); 
                    // InternalJniMap.g:8655:7: ( rule__Method__Alternatives_0_1_0 )
                    // InternalJniMap.g:8655:8: rule__Method__Alternatives_0_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__Alternatives_0_1_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMethodAccess().getAlternatives_0_1_0()); 

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:8661:4: ({...}? => ( ( ( rule__Method__StaticAssignment_0_1_1 ) ) ) )
                    {
                    // InternalJniMap.g:8661:4: ({...}? => ( ( ( rule__Method__StaticAssignment_0_1_1 ) ) ) )
                    // InternalJniMap.g:8662:5: {...}? => ( ( ( rule__Method__StaticAssignment_0_1_1 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1) ) {
                        throw new FailedPredicateException(input, "rule__Method__UnorderedGroup_0_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1)");
                    }
                    // InternalJniMap.g:8662:105: ( ( ( rule__Method__StaticAssignment_0_1_1 ) ) )
                    // InternalJniMap.g:8663:6: ( ( rule__Method__StaticAssignment_0_1_1 ) )
                    {
                     
                    	 				  getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1);
                    	 				

                    	 				  selected = true;
                    	 				
                    // InternalJniMap.g:8669:6: ( ( rule__Method__StaticAssignment_0_1_1 ) )
                    // InternalJniMap.g:8671:7: ( rule__Method__StaticAssignment_0_1_1 )
                    {
                     before(grammarAccess.getMethodAccess().getStaticAssignment_0_1_1()); 
                    // InternalJniMap.g:8672:7: ( rule__Method__StaticAssignment_0_1_1 )
                    // InternalJniMap.g:8672:8: rule__Method__StaticAssignment_0_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__StaticAssignment_0_1_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getMethodAccess().getStaticAssignment_0_1_1()); 

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalJniMap.g:8678:4: ({...}? => ( ( ( rule__Method__Alternatives_0_1_2 ) ) ) )
                    {
                    // InternalJniMap.g:8678:4: ({...}? => ( ( ( rule__Method__Alternatives_0_1_2 ) ) ) )
                    // InternalJniMap.g:8679:5: {...}? => ( ( ( rule__Method__Alternatives_0_1_2 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2) ) {
                        throw new FailedPredicateException(input, "rule__Method__UnorderedGroup_0_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2)");
                    }
                    // InternalJniMap.g:8679:105: ( ( ( rule__Method__Alternatives_0_1_2 ) ) )
                    // InternalJniMap.g:8680:6: ( ( rule__Method__Alternatives_0_1_2 ) )
                    {
                     
                    	 				  getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2);
                    	 				

                    	 				  selected = true;
                    	 				
                    // InternalJniMap.g:8686:6: ( ( rule__Method__Alternatives_0_1_2 ) )
                    // InternalJniMap.g:8688:7: ( rule__Method__Alternatives_0_1_2 )
                    {
                     before(grammarAccess.getMethodAccess().getAlternatives_0_1_2()); 
                    // InternalJniMap.g:8689:7: ( rule__Method__Alternatives_0_1_2 )
                    // InternalJniMap.g:8689:8: rule__Method__Alternatives_0_1_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__Alternatives_0_1_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getMethodAccess().getAlternatives_0_1_2()); 

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalJniMap.g:8695:4: ({...}? => ( ( ( rule__Method__Group_0_1_3__0 ) ) ) )
                    {
                    // InternalJniMap.g:8695:4: ({...}? => ( ( ( rule__Method__Group_0_1_3__0 ) ) ) )
                    // InternalJniMap.g:8696:5: {...}? => ( ( ( rule__Method__Group_0_1_3__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3) ) {
                        throw new FailedPredicateException(input, "rule__Method__UnorderedGroup_0_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3)");
                    }
                    // InternalJniMap.g:8696:105: ( ( ( rule__Method__Group_0_1_3__0 ) ) )
                    // InternalJniMap.g:8697:6: ( ( rule__Method__Group_0_1_3__0 ) )
                    {
                     
                    	 				  getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3);
                    	 				

                    	 				  selected = true;
                    	 				
                    // InternalJniMap.g:8703:6: ( ( rule__Method__Group_0_1_3__0 ) )
                    // InternalJniMap.g:8705:7: ( rule__Method__Group_0_1_3__0 )
                    {
                     before(grammarAccess.getMethodAccess().getGroup_0_1_3()); 
                    // InternalJniMap.g:8706:7: ( rule__Method__Group_0_1_3__0 )
                    // InternalJniMap.g:8706:8: rule__Method__Group_0_1_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__Group_0_1_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMethodAccess().getGroup_0_1_3()); 

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalJniMap.g:8712:4: ({...}? => ( ( ( rule__Method__StubAssignment_0_1_4 ) ) ) )
                    {
                    // InternalJniMap.g:8712:4: ({...}? => ( ( ( rule__Method__StubAssignment_0_1_4 ) ) ) )
                    // InternalJniMap.g:8713:5: {...}? => ( ( ( rule__Method__StubAssignment_0_1_4 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4) ) {
                        throw new FailedPredicateException(input, "rule__Method__UnorderedGroup_0_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4)");
                    }
                    // InternalJniMap.g:8713:105: ( ( ( rule__Method__StubAssignment_0_1_4 ) ) )
                    // InternalJniMap.g:8714:6: ( ( rule__Method__StubAssignment_0_1_4 ) )
                    {
                     
                    	 				  getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4);
                    	 				

                    	 				  selected = true;
                    	 				
                    // InternalJniMap.g:8720:6: ( ( rule__Method__StubAssignment_0_1_4 ) )
                    // InternalJniMap.g:8722:7: ( rule__Method__StubAssignment_0_1_4 )
                    {
                     before(grammarAccess.getMethodAccess().getStubAssignment_0_1_4()); 
                    // InternalJniMap.g:8723:7: ( rule__Method__StubAssignment_0_1_4 )
                    // InternalJniMap.g:8723:8: rule__Method__StubAssignment_0_1_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__StubAssignment_0_1_4();

                    state._fsp--;


                    }

                     after(grammarAccess.getMethodAccess().getStubAssignment_0_1_4()); 

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalJniMap.g:8729:4: ({...}? => ( ( ( rule__Method__Group_0_1_5__0 ) ) ) )
                    {
                    // InternalJniMap.g:8729:4: ({...}? => ( ( ( rule__Method__Group_0_1_5__0 ) ) ) )
                    // InternalJniMap.g:8730:5: {...}? => ( ( ( rule__Method__Group_0_1_5__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5) ) {
                        throw new FailedPredicateException(input, "rule__Method__UnorderedGroup_0_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5)");
                    }
                    // InternalJniMap.g:8730:105: ( ( ( rule__Method__Group_0_1_5__0 ) ) )
                    // InternalJniMap.g:8731:6: ( ( rule__Method__Group_0_1_5__0 ) )
                    {
                     
                    	 				  getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5);
                    	 				

                    	 				  selected = true;
                    	 				
                    // InternalJniMap.g:8737:6: ( ( rule__Method__Group_0_1_5__0 ) )
                    // InternalJniMap.g:8739:7: ( rule__Method__Group_0_1_5__0 )
                    {
                     before(grammarAccess.getMethodAccess().getGroup_0_1_5()); 
                    // InternalJniMap.g:8740:7: ( rule__Method__Group_0_1_5__0 )
                    // InternalJniMap.g:8740:8: rule__Method__Group_0_1_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__Group_0_1_5__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMethodAccess().getGroup_0_1_5()); 

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMethodAccess().getUnorderedGroup_0_1());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__UnorderedGroup_0_1__Impl"


    // $ANTLR start "rule__Method__UnorderedGroup_0_1__0"
    // InternalJniMap.g:8755:1: rule__Method__UnorderedGroup_0_1__0 : rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__1 )? ;
    public final void rule__Method__UnorderedGroup_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8759:1: ( rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__1 )? )
            // InternalJniMap.g:8760:2: rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__1 )?
            {
            pushFollow(FOLLOW_56);
            rule__Method__UnorderedGroup_0_1__Impl();

            state._fsp--;

            // InternalJniMap.g:8761:2: ( rule__Method__UnorderedGroup_0_1__1 )?
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( LA67_0 >= 61 && LA67_0 <= 63 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {
                alt67=1;
            }
            else if ( LA67_0 == 12 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1) ) {
                alt67=1;
            }
            else if ( LA67_0 >= 64 && LA67_0 <= 65 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2) ) {
                alt67=1;
            }
            else if ( LA67_0 == 66 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3) ) {
                alt67=1;
            }
            else if ( LA67_0 == 67 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4) ) {
                alt67=1;
            }
            else if ( LA67_0 == 55 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5) ) {
                alt67=1;
            }
            switch (alt67) {
                case 1 :
                    // InternalJniMap.g:8761:2: rule__Method__UnorderedGroup_0_1__1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__UnorderedGroup_0_1__1();

                    state._fsp--;


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__UnorderedGroup_0_1__0"


    // $ANTLR start "rule__Method__UnorderedGroup_0_1__1"
    // InternalJniMap.g:8768:1: rule__Method__UnorderedGroup_0_1__1 : rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__2 )? ;
    public final void rule__Method__UnorderedGroup_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8772:1: ( rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__2 )? )
            // InternalJniMap.g:8773:2: rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__2 )?
            {
            pushFollow(FOLLOW_56);
            rule__Method__UnorderedGroup_0_1__Impl();

            state._fsp--;

            // InternalJniMap.g:8774:2: ( rule__Method__UnorderedGroup_0_1__2 )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( LA68_0 >= 61 && LA68_0 <= 63 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {
                alt68=1;
            }
            else if ( LA68_0 == 12 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1) ) {
                alt68=1;
            }
            else if ( LA68_0 >= 64 && LA68_0 <= 65 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2) ) {
                alt68=1;
            }
            else if ( LA68_0 == 66 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3) ) {
                alt68=1;
            }
            else if ( LA68_0 == 67 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4) ) {
                alt68=1;
            }
            else if ( LA68_0 == 55 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // InternalJniMap.g:8774:2: rule__Method__UnorderedGroup_0_1__2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__UnorderedGroup_0_1__2();

                    state._fsp--;


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__UnorderedGroup_0_1__1"


    // $ANTLR start "rule__Method__UnorderedGroup_0_1__2"
    // InternalJniMap.g:8781:1: rule__Method__UnorderedGroup_0_1__2 : rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__3 )? ;
    public final void rule__Method__UnorderedGroup_0_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8785:1: ( rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__3 )? )
            // InternalJniMap.g:8786:2: rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__3 )?
            {
            pushFollow(FOLLOW_56);
            rule__Method__UnorderedGroup_0_1__Impl();

            state._fsp--;

            // InternalJniMap.g:8787:2: ( rule__Method__UnorderedGroup_0_1__3 )?
            int alt69=2;
            int LA69_0 = input.LA(1);

            if ( LA69_0 >= 61 && LA69_0 <= 63 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {
                alt69=1;
            }
            else if ( LA69_0 == 12 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1) ) {
                alt69=1;
            }
            else if ( LA69_0 >= 64 && LA69_0 <= 65 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2) ) {
                alt69=1;
            }
            else if ( LA69_0 == 66 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3) ) {
                alt69=1;
            }
            else if ( LA69_0 == 67 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4) ) {
                alt69=1;
            }
            else if ( LA69_0 == 55 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5) ) {
                alt69=1;
            }
            switch (alt69) {
                case 1 :
                    // InternalJniMap.g:8787:2: rule__Method__UnorderedGroup_0_1__3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__UnorderedGroup_0_1__3();

                    state._fsp--;


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__UnorderedGroup_0_1__2"


    // $ANTLR start "rule__Method__UnorderedGroup_0_1__3"
    // InternalJniMap.g:8794:1: rule__Method__UnorderedGroup_0_1__3 : rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__4 )? ;
    public final void rule__Method__UnorderedGroup_0_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8798:1: ( rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__4 )? )
            // InternalJniMap.g:8799:2: rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__4 )?
            {
            pushFollow(FOLLOW_56);
            rule__Method__UnorderedGroup_0_1__Impl();

            state._fsp--;

            // InternalJniMap.g:8800:2: ( rule__Method__UnorderedGroup_0_1__4 )?
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( LA70_0 >= 61 && LA70_0 <= 63 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {
                alt70=1;
            }
            else if ( LA70_0 == 12 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1) ) {
                alt70=1;
            }
            else if ( LA70_0 >= 64 && LA70_0 <= 65 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2) ) {
                alt70=1;
            }
            else if ( LA70_0 == 66 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3) ) {
                alt70=1;
            }
            else if ( LA70_0 == 67 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4) ) {
                alt70=1;
            }
            else if ( LA70_0 == 55 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5) ) {
                alt70=1;
            }
            switch (alt70) {
                case 1 :
                    // InternalJniMap.g:8800:2: rule__Method__UnorderedGroup_0_1__4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__UnorderedGroup_0_1__4();

                    state._fsp--;


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__UnorderedGroup_0_1__3"


    // $ANTLR start "rule__Method__UnorderedGroup_0_1__4"
    // InternalJniMap.g:8807:1: rule__Method__UnorderedGroup_0_1__4 : rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__5 )? ;
    public final void rule__Method__UnorderedGroup_0_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8811:1: ( rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__5 )? )
            // InternalJniMap.g:8812:2: rule__Method__UnorderedGroup_0_1__Impl ( rule__Method__UnorderedGroup_0_1__5 )?
            {
            pushFollow(FOLLOW_56);
            rule__Method__UnorderedGroup_0_1__Impl();

            state._fsp--;

            // InternalJniMap.g:8813:2: ( rule__Method__UnorderedGroup_0_1__5 )?
            int alt71=2;
            int LA71_0 = input.LA(1);

            if ( LA71_0 >= 61 && LA71_0 <= 63 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {
                alt71=1;
            }
            else if ( LA71_0 == 12 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1) ) {
                alt71=1;
            }
            else if ( LA71_0 >= 64 && LA71_0 <= 65 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2) ) {
                alt71=1;
            }
            else if ( LA71_0 == 66 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3) ) {
                alt71=1;
            }
            else if ( LA71_0 == 67 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4) ) {
                alt71=1;
            }
            else if ( LA71_0 == 55 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5) ) {
                alt71=1;
            }
            switch (alt71) {
                case 1 :
                    // InternalJniMap.g:8813:2: rule__Method__UnorderedGroup_0_1__5
                    {
                    pushFollow(FOLLOW_2);
                    rule__Method__UnorderedGroup_0_1__5();

                    state._fsp--;


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__UnorderedGroup_0_1__4"


    // $ANTLR start "rule__Method__UnorderedGroup_0_1__5"
    // InternalJniMap.g:8820:1: rule__Method__UnorderedGroup_0_1__5 : rule__Method__UnorderedGroup_0_1__Impl ;
    public final void rule__Method__UnorderedGroup_0_1__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8824:1: ( rule__Method__UnorderedGroup_0_1__Impl )
            // InternalJniMap.g:8825:2: rule__Method__UnorderedGroup_0_1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Method__UnorderedGroup_0_1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__UnorderedGroup_0_1__5"


    // $ANTLR start "rule__Mapping__ImportsAssignment_0"
    // InternalJniMap.g:8844:1: rule__Mapping__ImportsAssignment_0 : ( ruleImport ) ;
    public final void rule__Mapping__ImportsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8848:1: ( ( ruleImport ) )
            // InternalJniMap.g:8849:1: ( ruleImport )
            {
            // InternalJniMap.g:8849:1: ( ruleImport )
            // InternalJniMap.g:8850:1: ruleImport
            {
             before(grammarAccess.getMappingAccess().getImportsImportParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getImportsImportParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__ImportsAssignment_0"


    // $ANTLR start "rule__Mapping__NameAssignment_2"
    // InternalJniMap.g:8859:1: rule__Mapping__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Mapping__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8863:1: ( ( RULE_ID ) )
            // InternalJniMap.g:8864:1: ( RULE_ID )
            {
            // InternalJniMap.g:8864:1: ( RULE_ID )
            // InternalJniMap.g:8865:1: RULE_ID
            {
             before(grammarAccess.getMappingAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__NameAssignment_2"


    // $ANTLR start "rule__Mapping__PackageAssignment_3_3"
    // InternalJniMap.g:8874:1: rule__Mapping__PackageAssignment_3_3 : ( RULE_STRING ) ;
    public final void rule__Mapping__PackageAssignment_3_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8878:1: ( ( RULE_STRING ) )
            // InternalJniMap.g:8879:1: ( RULE_STRING )
            {
            // InternalJniMap.g:8879:1: ( RULE_STRING )
            // InternalJniMap.g:8880:1: RULE_STRING
            {
             before(grammarAccess.getMappingAccess().getPackageSTRINGTerminalRuleCall_3_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getPackageSTRINGTerminalRuleCall_3_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__PackageAssignment_3_3"


    // $ANTLR start "rule__Mapping__HostsAssignment_4_1"
    // InternalJniMap.g:8889:1: rule__Mapping__HostsAssignment_4_1 : ( ruleHostType ) ;
    public final void rule__Mapping__HostsAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8893:1: ( ( ruleHostType ) )
            // InternalJniMap.g:8894:1: ( ruleHostType )
            {
            // InternalJniMap.g:8894:1: ( ruleHostType )
            // InternalJniMap.g:8895:1: ruleHostType
            {
             before(grammarAccess.getMappingAccess().getHostsHostTypeEnumRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleHostType();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getHostsHostTypeEnumRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__HostsAssignment_4_1"


    // $ANTLR start "rule__Mapping__HostsAssignment_4_2_1"
    // InternalJniMap.g:8904:1: rule__Mapping__HostsAssignment_4_2_1 : ( ruleHostType ) ;
    public final void rule__Mapping__HostsAssignment_4_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8908:1: ( ( ruleHostType ) )
            // InternalJniMap.g:8909:1: ( ruleHostType )
            {
            // InternalJniMap.g:8909:1: ( ruleHostType )
            // InternalJniMap.g:8910:1: ruleHostType
            {
             before(grammarAccess.getMappingAccess().getHostsHostTypeEnumRuleCall_4_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleHostType();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getHostsHostTypeEnumRuleCall_4_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__HostsAssignment_4_2_1"


    // $ANTLR start "rule__Mapping__ExternalLibrariesAssignment_6"
    // InternalJniMap.g:8919:1: rule__Mapping__ExternalLibrariesAssignment_6 : ( ruleExternalLibrary ) ;
    public final void rule__Mapping__ExternalLibrariesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8923:1: ( ( ruleExternalLibrary ) )
            // InternalJniMap.g:8924:1: ( ruleExternalLibrary )
            {
            // InternalJniMap.g:8924:1: ( ruleExternalLibrary )
            // InternalJniMap.g:8925:1: ruleExternalLibrary
            {
             before(grammarAccess.getMappingAccess().getExternalLibrariesExternalLibraryParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleExternalLibrary();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getExternalLibrariesExternalLibraryParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__ExternalLibrariesAssignment_6"


    // $ANTLR start "rule__Mapping__LibrariesAssignment_7"
    // InternalJniMap.g:8934:1: rule__Mapping__LibrariesAssignment_7 : ( ruleLibrary ) ;
    public final void rule__Mapping__LibrariesAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8938:1: ( ( ruleLibrary ) )
            // InternalJniMap.g:8939:1: ( ruleLibrary )
            {
            // InternalJniMap.g:8939:1: ( ruleLibrary )
            // InternalJniMap.g:8940:1: ruleLibrary
            {
             before(grammarAccess.getMappingAccess().getLibrariesLibraryParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleLibrary();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getLibrariesLibraryParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__LibrariesAssignment_7"


    // $ANTLR start "rule__Mapping__UserModulesAssignment_8"
    // InternalJniMap.g:8949:1: rule__Mapping__UserModulesAssignment_8 : ( ruleUserModule ) ;
    public final void rule__Mapping__UserModulesAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8953:1: ( ( ruleUserModule ) )
            // InternalJniMap.g:8954:1: ( ruleUserModule )
            {
            // InternalJniMap.g:8954:1: ( ruleUserModule )
            // InternalJniMap.g:8955:1: ruleUserModule
            {
             before(grammarAccess.getMappingAccess().getUserModulesUserModuleParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleUserModule();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getUserModulesUserModuleParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__UserModulesAssignment_8"


    // $ANTLR start "rule__Mapping__ClassMappingAssignment_9_0"
    // InternalJniMap.g:8964:1: rule__Mapping__ClassMappingAssignment_9_0 : ( ruleClassMapping ) ;
    public final void rule__Mapping__ClassMappingAssignment_9_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8968:1: ( ( ruleClassMapping ) )
            // InternalJniMap.g:8969:1: ( ruleClassMapping )
            {
            // InternalJniMap.g:8969:1: ( ruleClassMapping )
            // InternalJniMap.g:8970:1: ruleClassMapping
            {
             before(grammarAccess.getMappingAccess().getClassMappingClassMappingParserRuleCall_9_0_0()); 
            pushFollow(FOLLOW_2);
            ruleClassMapping();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getClassMappingClassMappingParserRuleCall_9_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__ClassMappingAssignment_9_0"


    // $ANTLR start "rule__Mapping__IncludesAssignment_9_1"
    // InternalJniMap.g:8979:1: rule__Mapping__IncludesAssignment_9_1 : ( ruleIncludeDef ) ;
    public final void rule__Mapping__IncludesAssignment_9_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8983:1: ( ( ruleIncludeDef ) )
            // InternalJniMap.g:8984:1: ( ruleIncludeDef )
            {
            // InternalJniMap.g:8984:1: ( ruleIncludeDef )
            // InternalJniMap.g:8985:1: ruleIncludeDef
            {
             before(grammarAccess.getMappingAccess().getIncludesIncludeDefParserRuleCall_9_1_0()); 
            pushFollow(FOLLOW_2);
            ruleIncludeDef();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getIncludesIncludeDefParserRuleCall_9_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__IncludesAssignment_9_1"


    // $ANTLR start "rule__Mapping__MethodGroupsAssignment_10"
    // InternalJniMap.g:8994:1: rule__Mapping__MethodGroupsAssignment_10 : ( ruleMethodGroup ) ;
    public final void rule__Mapping__MethodGroupsAssignment_10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:8998:1: ( ( ruleMethodGroup ) )
            // InternalJniMap.g:8999:1: ( ruleMethodGroup )
            {
            // InternalJniMap.g:8999:1: ( ruleMethodGroup )
            // InternalJniMap.g:9000:1: ruleMethodGroup
            {
             before(grammarAccess.getMappingAccess().getMethodGroupsMethodGroupParserRuleCall_10_0()); 
            pushFollow(FOLLOW_2);
            ruleMethodGroup();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getMethodGroupsMethodGroupParserRuleCall_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__MethodGroupsAssignment_10"


    // $ANTLR start "rule__Import__ImportURIAssignment_1"
    // InternalJniMap.g:9009:1: rule__Import__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Import__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9013:1: ( ( RULE_STRING ) )
            // InternalJniMap.g:9014:1: ( RULE_STRING )
            {
            // InternalJniMap.g:9014:1: ( RULE_STRING )
            // InternalJniMap.g:9015:1: RULE_STRING
            {
             before(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportURIAssignment_1"


    // $ANTLR start "rule__GenericsDeclaration__NamesAssignment_1"
    // InternalJniMap.g:9024:1: rule__GenericsDeclaration__NamesAssignment_1 : ( RULE_ID ) ;
    public final void rule__GenericsDeclaration__NamesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9028:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9029:1: ( RULE_ID )
            {
            // InternalJniMap.g:9029:1: ( RULE_ID )
            // InternalJniMap.g:9030:1: RULE_ID
            {
             before(grammarAccess.getGenericsDeclarationAccess().getNamesIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getGenericsDeclarationAccess().getNamesIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__NamesAssignment_1"


    // $ANTLR start "rule__GenericsDeclaration__NamesAssignment_2_1"
    // InternalJniMap.g:9039:1: rule__GenericsDeclaration__NamesAssignment_2_1 : ( RULE_ID ) ;
    public final void rule__GenericsDeclaration__NamesAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9043:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9044:1: ( RULE_ID )
            {
            // InternalJniMap.g:9044:1: ( RULE_ID )
            // InternalJniMap.g:9045:1: RULE_ID
            {
             before(grammarAccess.getGenericsDeclarationAccess().getNamesIDTerminalRuleCall_2_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getGenericsDeclarationAccess().getNamesIDTerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsDeclaration__NamesAssignment_2_1"


    // $ANTLR start "rule__GenericsInstantiation__InstanceNamesAssignment_1"
    // InternalJniMap.g:9054:1: rule__GenericsInstantiation__InstanceNamesAssignment_1 : ( RULE_ID ) ;
    public final void rule__GenericsInstantiation__InstanceNamesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9058:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9059:1: ( RULE_ID )
            {
            // InternalJniMap.g:9059:1: ( RULE_ID )
            // InternalJniMap.g:9060:1: RULE_ID
            {
             before(grammarAccess.getGenericsInstantiationAccess().getInstanceNamesIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getGenericsInstantiationAccess().getInstanceNamesIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__InstanceNamesAssignment_1"


    // $ANTLR start "rule__GenericsInstantiation__InstanceNamesAssignment_2_1"
    // InternalJniMap.g:9069:1: rule__GenericsInstantiation__InstanceNamesAssignment_2_1 : ( RULE_ID ) ;
    public final void rule__GenericsInstantiation__InstanceNamesAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9073:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9074:1: ( RULE_ID )
            {
            // InternalJniMap.g:9074:1: ( RULE_ID )
            // InternalJniMap.g:9075:1: RULE_ID
            {
             before(grammarAccess.getGenericsInstantiationAccess().getInstanceNamesIDTerminalRuleCall_2_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getGenericsInstantiationAccess().getInstanceNamesIDTerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GenericsInstantiation__InstanceNamesAssignment_2_1"


    // $ANTLR start "rule__InterfaceImplementation__InterfaceAssignment_0"
    // InternalJniMap.g:9084:1: rule__InterfaceImplementation__InterfaceAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__InterfaceImplementation__InterfaceAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9088:1: ( ( ( RULE_ID ) ) )
            // InternalJniMap.g:9089:1: ( ( RULE_ID ) )
            {
            // InternalJniMap.g:9089:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9090:1: ( RULE_ID )
            {
             before(grammarAccess.getInterfaceImplementationAccess().getInterfaceInterfaceClassCrossReference_0_0()); 
            // InternalJniMap.g:9091:1: ( RULE_ID )
            // InternalJniMap.g:9092:1: RULE_ID
            {
             before(grammarAccess.getInterfaceImplementationAccess().getInterfaceInterfaceClassIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getInterfaceImplementationAccess().getInterfaceInterfaceClassIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getInterfaceImplementationAccess().getInterfaceInterfaceClassCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceImplementation__InterfaceAssignment_0"


    // $ANTLR start "rule__InterfaceImplementation__GenericsInstantiationAssignment_1"
    // InternalJniMap.g:9103:1: rule__InterfaceImplementation__GenericsInstantiationAssignment_1 : ( ruleGenericsInstantiation ) ;
    public final void rule__InterfaceImplementation__GenericsInstantiationAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9107:1: ( ( ruleGenericsInstantiation ) )
            // InternalJniMap.g:9108:1: ( ruleGenericsInstantiation )
            {
            // InternalJniMap.g:9108:1: ( ruleGenericsInstantiation )
            // InternalJniMap.g:9109:1: ruleGenericsInstantiation
            {
             before(grammarAccess.getInterfaceImplementationAccess().getGenericsInstantiationGenericsInstantiationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleGenericsInstantiation();

            state._fsp--;

             after(grammarAccess.getInterfaceImplementationAccess().getGenericsInstantiationGenericsInstantiationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceImplementation__GenericsInstantiationAssignment_1"


    // $ANTLR start "rule__EnumToClassMapping__Enum_Assignment_0"
    // InternalJniMap.g:9118:1: rule__EnumToClassMapping__Enum_Assignment_0 : ( ruleEnumDeclaration ) ;
    public final void rule__EnumToClassMapping__Enum_Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9122:1: ( ( ruleEnumDeclaration ) )
            // InternalJniMap.g:9123:1: ( ruleEnumDeclaration )
            {
            // InternalJniMap.g:9123:1: ( ruleEnumDeclaration )
            // InternalJniMap.g:9124:1: ruleEnumDeclaration
            {
             before(grammarAccess.getEnumToClassMappingAccess().getEnum_EnumDeclarationParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleEnumDeclaration();

            state._fsp--;

             after(grammarAccess.getEnumToClassMappingAccess().getEnum_EnumDeclarationParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumToClassMapping__Enum_Assignment_0"


    // $ANTLR start "rule__EnumToClassMapping__NameAssignment_2"
    // InternalJniMap.g:9133:1: rule__EnumToClassMapping__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__EnumToClassMapping__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9137:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9138:1: ( RULE_ID )
            {
            // InternalJniMap.g:9138:1: ( RULE_ID )
            // InternalJniMap.g:9139:1: RULE_ID
            {
             before(grammarAccess.getEnumToClassMappingAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEnumToClassMappingAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumToClassMapping__NameAssignment_2"


    // $ANTLR start "rule__StructToClassMapping__StructAssignment_0"
    // InternalJniMap.g:9148:1: rule__StructToClassMapping__StructAssignment_0 : ( ruleStructDeclaration ) ;
    public final void rule__StructToClassMapping__StructAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9152:1: ( ( ruleStructDeclaration ) )
            // InternalJniMap.g:9153:1: ( ruleStructDeclaration )
            {
            // InternalJniMap.g:9153:1: ( ruleStructDeclaration )
            // InternalJniMap.g:9154:1: ruleStructDeclaration
            {
             before(grammarAccess.getStructToClassMappingAccess().getStructStructDeclarationParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleStructDeclaration();

            state._fsp--;

             after(grammarAccess.getStructToClassMappingAccess().getStructStructDeclarationParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__StructAssignment_0"


    // $ANTLR start "rule__StructToClassMapping__NameAssignment_2"
    // InternalJniMap.g:9163:1: rule__StructToClassMapping__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__StructToClassMapping__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9167:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9168:1: ( RULE_ID )
            {
            // InternalJniMap.g:9168:1: ( RULE_ID )
            // InternalJniMap.g:9169:1: RULE_ID
            {
             before(grammarAccess.getStructToClassMappingAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getStructToClassMappingAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__NameAssignment_2"


    // $ANTLR start "rule__StructToClassMapping__SuperAssignment_3_1"
    // InternalJniMap.g:9178:1: rule__StructToClassMapping__SuperAssignment_3_1 : ( ( RULE_ID ) ) ;
    public final void rule__StructToClassMapping__SuperAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9182:1: ( ( ( RULE_ID ) ) )
            // InternalJniMap.g:9183:1: ( ( RULE_ID ) )
            {
            // InternalJniMap.g:9183:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9184:1: ( RULE_ID )
            {
             before(grammarAccess.getStructToClassMappingAccess().getSuperClassMappingCrossReference_3_1_0()); 
            // InternalJniMap.g:9185:1: ( RULE_ID )
            // InternalJniMap.g:9186:1: RULE_ID
            {
             before(grammarAccess.getStructToClassMappingAccess().getSuperClassMappingIDTerminalRuleCall_3_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getStructToClassMappingAccess().getSuperClassMappingIDTerminalRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getStructToClassMappingAccess().getSuperClassMappingCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructToClassMapping__SuperAssignment_3_1"


    // $ANTLR start "rule__ClassToClassMapping__ClassAssignment_0"
    // InternalJniMap.g:9197:1: rule__ClassToClassMapping__ClassAssignment_0 : ( ruleClassDeclaration ) ;
    public final void rule__ClassToClassMapping__ClassAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9201:1: ( ( ruleClassDeclaration ) )
            // InternalJniMap.g:9202:1: ( ruleClassDeclaration )
            {
            // InternalJniMap.g:9202:1: ( ruleClassDeclaration )
            // InternalJniMap.g:9203:1: ruleClassDeclaration
            {
             before(grammarAccess.getClassToClassMappingAccess().getClassClassDeclarationParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleClassDeclaration();

            state._fsp--;

             after(grammarAccess.getClassToClassMappingAccess().getClassClassDeclarationParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__ClassAssignment_0"


    // $ANTLR start "rule__ClassToClassMapping__NameAssignment_2"
    // InternalJniMap.g:9212:1: rule__ClassToClassMapping__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__ClassToClassMapping__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9216:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9217:1: ( RULE_ID )
            {
            // InternalJniMap.g:9217:1: ( RULE_ID )
            // InternalJniMap.g:9218:1: RULE_ID
            {
             before(grammarAccess.getClassToClassMappingAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getClassToClassMappingAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__NameAssignment_2"


    // $ANTLR start "rule__ClassToClassMapping__SuperAssignment_3_1"
    // InternalJniMap.g:9227:1: rule__ClassToClassMapping__SuperAssignment_3_1 : ( ( RULE_ID ) ) ;
    public final void rule__ClassToClassMapping__SuperAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9231:1: ( ( ( RULE_ID ) ) )
            // InternalJniMap.g:9232:1: ( ( RULE_ID ) )
            {
            // InternalJniMap.g:9232:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9233:1: ( RULE_ID )
            {
             before(grammarAccess.getClassToClassMappingAccess().getSuperClassMappingCrossReference_3_1_0()); 
            // InternalJniMap.g:9234:1: ( RULE_ID )
            // InternalJniMap.g:9235:1: RULE_ID
            {
             before(grammarAccess.getClassToClassMappingAccess().getSuperClassMappingIDTerminalRuleCall_3_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getClassToClassMappingAccess().getSuperClassMappingIDTerminalRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getClassToClassMappingAccess().getSuperClassMappingCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassToClassMapping__SuperAssignment_3_1"


    // $ANTLR start "rule__AliasToClassMapping__AliasAssignment_0"
    // InternalJniMap.g:9246:1: rule__AliasToClassMapping__AliasAssignment_0 : ( ruleTypeAliasDeclaration ) ;
    public final void rule__AliasToClassMapping__AliasAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9250:1: ( ( ruleTypeAliasDeclaration ) )
            // InternalJniMap.g:9251:1: ( ruleTypeAliasDeclaration )
            {
            // InternalJniMap.g:9251:1: ( ruleTypeAliasDeclaration )
            // InternalJniMap.g:9252:1: ruleTypeAliasDeclaration
            {
             before(grammarAccess.getAliasToClassMappingAccess().getAliasTypeAliasDeclarationParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeAliasDeclaration();

            state._fsp--;

             after(grammarAccess.getAliasToClassMappingAccess().getAliasTypeAliasDeclarationParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__AliasAssignment_0"


    // $ANTLR start "rule__AliasToClassMapping__NameAssignment_2"
    // InternalJniMap.g:9261:1: rule__AliasToClassMapping__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__AliasToClassMapping__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9265:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9266:1: ( RULE_ID )
            {
            // InternalJniMap.g:9266:1: ( RULE_ID )
            // InternalJniMap.g:9267:1: RULE_ID
            {
             before(grammarAccess.getAliasToClassMappingAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAliasToClassMappingAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__NameAssignment_2"


    // $ANTLR start "rule__AliasToClassMapping__GenericsDeclAssignment_3"
    // InternalJniMap.g:9276:1: rule__AliasToClassMapping__GenericsDeclAssignment_3 : ( ruleGenericsDeclaration ) ;
    public final void rule__AliasToClassMapping__GenericsDeclAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9280:1: ( ( ruleGenericsDeclaration ) )
            // InternalJniMap.g:9281:1: ( ruleGenericsDeclaration )
            {
            // InternalJniMap.g:9281:1: ( ruleGenericsDeclaration )
            // InternalJniMap.g:9282:1: ruleGenericsDeclaration
            {
             before(grammarAccess.getAliasToClassMappingAccess().getGenericsDeclGenericsDeclarationParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleGenericsDeclaration();

            state._fsp--;

             after(grammarAccess.getAliasToClassMappingAccess().getGenericsDeclGenericsDeclarationParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__GenericsDeclAssignment_3"


    // $ANTLR start "rule__AliasToClassMapping__SuperAssignment_4_1"
    // InternalJniMap.g:9291:1: rule__AliasToClassMapping__SuperAssignment_4_1 : ( ( RULE_ID ) ) ;
    public final void rule__AliasToClassMapping__SuperAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9295:1: ( ( ( RULE_ID ) ) )
            // InternalJniMap.g:9296:1: ( ( RULE_ID ) )
            {
            // InternalJniMap.g:9296:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9297:1: ( RULE_ID )
            {
             before(grammarAccess.getAliasToClassMappingAccess().getSuperClassMappingCrossReference_4_1_0()); 
            // InternalJniMap.g:9298:1: ( RULE_ID )
            // InternalJniMap.g:9299:1: RULE_ID
            {
             before(grammarAccess.getAliasToClassMappingAccess().getSuperClassMappingIDTerminalRuleCall_4_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAliasToClassMappingAccess().getSuperClassMappingIDTerminalRuleCall_4_1_0_1()); 

            }

             after(grammarAccess.getAliasToClassMappingAccess().getSuperClassMappingCrossReference_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__SuperAssignment_4_1"


    // $ANTLR start "rule__AliasToClassMapping__GenericsInstantiationAssignment_4_2"
    // InternalJniMap.g:9310:1: rule__AliasToClassMapping__GenericsInstantiationAssignment_4_2 : ( ruleGenericsInstantiation ) ;
    public final void rule__AliasToClassMapping__GenericsInstantiationAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9314:1: ( ( ruleGenericsInstantiation ) )
            // InternalJniMap.g:9315:1: ( ruleGenericsInstantiation )
            {
            // InternalJniMap.g:9315:1: ( ruleGenericsInstantiation )
            // InternalJniMap.g:9316:1: ruleGenericsInstantiation
            {
             before(grammarAccess.getAliasToClassMappingAccess().getGenericsInstantiationGenericsInstantiationParserRuleCall_4_2_0()); 
            pushFollow(FOLLOW_2);
            ruleGenericsInstantiation();

            state._fsp--;

             after(grammarAccess.getAliasToClassMappingAccess().getGenericsInstantiationGenericsInstantiationParserRuleCall_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__GenericsInstantiationAssignment_4_2"


    // $ANTLR start "rule__AliasToClassMapping__InterfacesAssignment_5_1"
    // InternalJniMap.g:9325:1: rule__AliasToClassMapping__InterfacesAssignment_5_1 : ( ruleInterfaceImplementation ) ;
    public final void rule__AliasToClassMapping__InterfacesAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9329:1: ( ( ruleInterfaceImplementation ) )
            // InternalJniMap.g:9330:1: ( ruleInterfaceImplementation )
            {
            // InternalJniMap.g:9330:1: ( ruleInterfaceImplementation )
            // InternalJniMap.g:9331:1: ruleInterfaceImplementation
            {
             before(grammarAccess.getAliasToClassMappingAccess().getInterfacesInterfaceImplementationParserRuleCall_5_1_0()); 
            pushFollow(FOLLOW_2);
            ruleInterfaceImplementation();

            state._fsp--;

             after(grammarAccess.getAliasToClassMappingAccess().getInterfacesInterfaceImplementationParserRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__InterfacesAssignment_5_1"


    // $ANTLR start "rule__AliasToClassMapping__InterfacesAssignment_5_2_1"
    // InternalJniMap.g:9340:1: rule__AliasToClassMapping__InterfacesAssignment_5_2_1 : ( ruleInterfaceImplementation ) ;
    public final void rule__AliasToClassMapping__InterfacesAssignment_5_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9344:1: ( ( ruleInterfaceImplementation ) )
            // InternalJniMap.g:9345:1: ( ruleInterfaceImplementation )
            {
            // InternalJniMap.g:9345:1: ( ruleInterfaceImplementation )
            // InternalJniMap.g:9346:1: ruleInterfaceImplementation
            {
             before(grammarAccess.getAliasToClassMappingAccess().getInterfacesInterfaceImplementationParserRuleCall_5_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleInterfaceImplementation();

            state._fsp--;

             after(grammarAccess.getAliasToClassMappingAccess().getInterfacesInterfaceImplementationParserRuleCall_5_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasToClassMapping__InterfacesAssignment_5_2_1"


    // $ANTLR start "rule__UnmappedClass__AbstractAssignment_1"
    // InternalJniMap.g:9355:1: rule__UnmappedClass__AbstractAssignment_1 : ( ( 'abstract' ) ) ;
    public final void rule__UnmappedClass__AbstractAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9359:1: ( ( ( 'abstract' ) ) )
            // InternalJniMap.g:9360:1: ( ( 'abstract' ) )
            {
            // InternalJniMap.g:9360:1: ( ( 'abstract' ) )
            // InternalJniMap.g:9361:1: ( 'abstract' )
            {
             before(grammarAccess.getUnmappedClassAccess().getAbstractAbstractKeyword_1_0()); 
            // InternalJniMap.g:9362:1: ( 'abstract' )
            // InternalJniMap.g:9363:1: 'abstract'
            {
             before(grammarAccess.getUnmappedClassAccess().getAbstractAbstractKeyword_1_0()); 
            match(input,57,FOLLOW_2); 
             after(grammarAccess.getUnmappedClassAccess().getAbstractAbstractKeyword_1_0()); 

            }

             after(grammarAccess.getUnmappedClassAccess().getAbstractAbstractKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__AbstractAssignment_1"


    // $ANTLR start "rule__UnmappedClass__NameAssignment_2"
    // InternalJniMap.g:9378:1: rule__UnmappedClass__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__UnmappedClass__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9382:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9383:1: ( RULE_ID )
            {
            // InternalJniMap.g:9383:1: ( RULE_ID )
            // InternalJniMap.g:9384:1: RULE_ID
            {
             before(grammarAccess.getUnmappedClassAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getUnmappedClassAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__NameAssignment_2"


    // $ANTLR start "rule__UnmappedClass__GenericsDeclAssignment_3"
    // InternalJniMap.g:9393:1: rule__UnmappedClass__GenericsDeclAssignment_3 : ( ruleGenericsDeclaration ) ;
    public final void rule__UnmappedClass__GenericsDeclAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9397:1: ( ( ruleGenericsDeclaration ) )
            // InternalJniMap.g:9398:1: ( ruleGenericsDeclaration )
            {
            // InternalJniMap.g:9398:1: ( ruleGenericsDeclaration )
            // InternalJniMap.g:9399:1: ruleGenericsDeclaration
            {
             before(grammarAccess.getUnmappedClassAccess().getGenericsDeclGenericsDeclarationParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleGenericsDeclaration();

            state._fsp--;

             after(grammarAccess.getUnmappedClassAccess().getGenericsDeclGenericsDeclarationParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__GenericsDeclAssignment_3"


    // $ANTLR start "rule__UnmappedClass__SuperAssignment_4_1"
    // InternalJniMap.g:9408:1: rule__UnmappedClass__SuperAssignment_4_1 : ( ( RULE_ID ) ) ;
    public final void rule__UnmappedClass__SuperAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9412:1: ( ( ( RULE_ID ) ) )
            // InternalJniMap.g:9413:1: ( ( RULE_ID ) )
            {
            // InternalJniMap.g:9413:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9414:1: ( RULE_ID )
            {
             before(grammarAccess.getUnmappedClassAccess().getSuperClassMappingCrossReference_4_1_0()); 
            // InternalJniMap.g:9415:1: ( RULE_ID )
            // InternalJniMap.g:9416:1: RULE_ID
            {
             before(grammarAccess.getUnmappedClassAccess().getSuperClassMappingIDTerminalRuleCall_4_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getUnmappedClassAccess().getSuperClassMappingIDTerminalRuleCall_4_1_0_1()); 

            }

             after(grammarAccess.getUnmappedClassAccess().getSuperClassMappingCrossReference_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__SuperAssignment_4_1"


    // $ANTLR start "rule__UnmappedClass__GenericsInstantiationAssignment_4_2"
    // InternalJniMap.g:9427:1: rule__UnmappedClass__GenericsInstantiationAssignment_4_2 : ( ruleGenericsInstantiation ) ;
    public final void rule__UnmappedClass__GenericsInstantiationAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9431:1: ( ( ruleGenericsInstantiation ) )
            // InternalJniMap.g:9432:1: ( ruleGenericsInstantiation )
            {
            // InternalJniMap.g:9432:1: ( ruleGenericsInstantiation )
            // InternalJniMap.g:9433:1: ruleGenericsInstantiation
            {
             before(grammarAccess.getUnmappedClassAccess().getGenericsInstantiationGenericsInstantiationParserRuleCall_4_2_0()); 
            pushFollow(FOLLOW_2);
            ruleGenericsInstantiation();

            state._fsp--;

             after(grammarAccess.getUnmappedClassAccess().getGenericsInstantiationGenericsInstantiationParserRuleCall_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__GenericsInstantiationAssignment_4_2"


    // $ANTLR start "rule__UnmappedClass__InterfacesAssignment_5_1"
    // InternalJniMap.g:9442:1: rule__UnmappedClass__InterfacesAssignment_5_1 : ( ruleInterfaceImplementation ) ;
    public final void rule__UnmappedClass__InterfacesAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9446:1: ( ( ruleInterfaceImplementation ) )
            // InternalJniMap.g:9447:1: ( ruleInterfaceImplementation )
            {
            // InternalJniMap.g:9447:1: ( ruleInterfaceImplementation )
            // InternalJniMap.g:9448:1: ruleInterfaceImplementation
            {
             before(grammarAccess.getUnmappedClassAccess().getInterfacesInterfaceImplementationParserRuleCall_5_1_0()); 
            pushFollow(FOLLOW_2);
            ruleInterfaceImplementation();

            state._fsp--;

             after(grammarAccess.getUnmappedClassAccess().getInterfacesInterfaceImplementationParserRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__InterfacesAssignment_5_1"


    // $ANTLR start "rule__UnmappedClass__InterfacesAssignment_5_2_1"
    // InternalJniMap.g:9457:1: rule__UnmappedClass__InterfacesAssignment_5_2_1 : ( ruleInterfaceImplementation ) ;
    public final void rule__UnmappedClass__InterfacesAssignment_5_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9461:1: ( ( ruleInterfaceImplementation ) )
            // InternalJniMap.g:9462:1: ( ruleInterfaceImplementation )
            {
            // InternalJniMap.g:9462:1: ( ruleInterfaceImplementation )
            // InternalJniMap.g:9463:1: ruleInterfaceImplementation
            {
             before(grammarAccess.getUnmappedClassAccess().getInterfacesInterfaceImplementationParserRuleCall_5_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleInterfaceImplementation();

            state._fsp--;

             after(grammarAccess.getUnmappedClassAccess().getInterfacesInterfaceImplementationParserRuleCall_5_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnmappedClass__InterfacesAssignment_5_2_1"


    // $ANTLR start "rule__InterfaceClass__NameAssignment_1"
    // InternalJniMap.g:9472:1: rule__InterfaceClass__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__InterfaceClass__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9476:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9477:1: ( RULE_ID )
            {
            // InternalJniMap.g:9477:1: ( RULE_ID )
            // InternalJniMap.g:9478:1: RULE_ID
            {
             before(grammarAccess.getInterfaceClassAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getInterfaceClassAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__NameAssignment_1"


    // $ANTLR start "rule__InterfaceClass__GenericsDeclAssignment_2"
    // InternalJniMap.g:9487:1: rule__InterfaceClass__GenericsDeclAssignment_2 : ( ruleGenericsDeclaration ) ;
    public final void rule__InterfaceClass__GenericsDeclAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9491:1: ( ( ruleGenericsDeclaration ) )
            // InternalJniMap.g:9492:1: ( ruleGenericsDeclaration )
            {
            // InternalJniMap.g:9492:1: ( ruleGenericsDeclaration )
            // InternalJniMap.g:9493:1: ruleGenericsDeclaration
            {
             before(grammarAccess.getInterfaceClassAccess().getGenericsDeclGenericsDeclarationParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleGenericsDeclaration();

            state._fsp--;

             after(grammarAccess.getInterfaceClassAccess().getGenericsDeclGenericsDeclarationParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__GenericsDeclAssignment_2"


    // $ANTLR start "rule__InterfaceClass__InterfacesAssignment_3_1"
    // InternalJniMap.g:9502:1: rule__InterfaceClass__InterfacesAssignment_3_1 : ( ruleInterfaceImplementation ) ;
    public final void rule__InterfaceClass__InterfacesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9506:1: ( ( ruleInterfaceImplementation ) )
            // InternalJniMap.g:9507:1: ( ruleInterfaceImplementation )
            {
            // InternalJniMap.g:9507:1: ( ruleInterfaceImplementation )
            // InternalJniMap.g:9508:1: ruleInterfaceImplementation
            {
             before(grammarAccess.getInterfaceClassAccess().getInterfacesInterfaceImplementationParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleInterfaceImplementation();

            state._fsp--;

             after(grammarAccess.getInterfaceClassAccess().getInterfacesInterfaceImplementationParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__InterfacesAssignment_3_1"


    // $ANTLR start "rule__InterfaceClass__InterfacesAssignment_3_2_1"
    // InternalJniMap.g:9517:1: rule__InterfaceClass__InterfacesAssignment_3_2_1 : ( ruleInterfaceImplementation ) ;
    public final void rule__InterfaceClass__InterfacesAssignment_3_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9521:1: ( ( ruleInterfaceImplementation ) )
            // InternalJniMap.g:9522:1: ( ruleInterfaceImplementation )
            {
            // InternalJniMap.g:9522:1: ( ruleInterfaceImplementation )
            // InternalJniMap.g:9523:1: ruleInterfaceImplementation
            {
             before(grammarAccess.getInterfaceClassAccess().getInterfacesInterfaceImplementationParserRuleCall_3_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleInterfaceImplementation();

            state._fsp--;

             after(grammarAccess.getInterfaceClassAccess().getInterfacesInterfaceImplementationParserRuleCall_3_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfaceClass__InterfacesAssignment_3_2_1"


    // $ANTLR start "rule__ExternalLibrary__LibraryAssignment_1"
    // InternalJniMap.g:9532:1: rule__ExternalLibrary__LibraryAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__ExternalLibrary__LibraryAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9536:1: ( ( ( RULE_ID ) ) )
            // InternalJniMap.g:9537:1: ( ( RULE_ID ) )
            {
            // InternalJniMap.g:9537:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9538:1: ( RULE_ID )
            {
             before(grammarAccess.getExternalLibraryAccess().getLibraryLibraryCrossReference_1_0()); 
            // InternalJniMap.g:9539:1: ( RULE_ID )
            // InternalJniMap.g:9540:1: RULE_ID
            {
             before(grammarAccess.getExternalLibraryAccess().getLibraryLibraryIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getExternalLibraryAccess().getLibraryLibraryIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getExternalLibraryAccess().getLibraryLibraryCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExternalLibrary__LibraryAssignment_1"


    // $ANTLR start "rule__ExternalLibrary__MappingAssignment_3"
    // InternalJniMap.g:9551:1: rule__ExternalLibrary__MappingAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__ExternalLibrary__MappingAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9555:1: ( ( ( RULE_ID ) ) )
            // InternalJniMap.g:9556:1: ( ( RULE_ID ) )
            {
            // InternalJniMap.g:9556:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9557:1: ( RULE_ID )
            {
             before(grammarAccess.getExternalLibraryAccess().getMappingMappingCrossReference_3_0()); 
            // InternalJniMap.g:9558:1: ( RULE_ID )
            // InternalJniMap.g:9559:1: RULE_ID
            {
             before(grammarAccess.getExternalLibraryAccess().getMappingMappingIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getExternalLibraryAccess().getMappingMappingIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getExternalLibraryAccess().getMappingMappingCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExternalLibrary__MappingAssignment_3"


    // $ANTLR start "rule__Library__NameAssignment_4"
    // InternalJniMap.g:9570:1: rule__Library__NameAssignment_4 : ( RULE_STRING ) ;
    public final void rule__Library__NameAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9574:1: ( ( RULE_STRING ) )
            // InternalJniMap.g:9575:1: ( RULE_STRING )
            {
            // InternalJniMap.g:9575:1: ( RULE_STRING )
            // InternalJniMap.g:9576:1: RULE_STRING
            {
             before(grammarAccess.getLibraryAccess().getNameSTRINGTerminalRuleCall_4_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getLibraryAccess().getNameSTRINGTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__NameAssignment_4"


    // $ANTLR start "rule__Library__FilenamesAssignment_6_0"
    // InternalJniMap.g:9585:1: rule__Library__FilenamesAssignment_6_0 : ( ruleLibraryFileName ) ;
    public final void rule__Library__FilenamesAssignment_6_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9589:1: ( ( ruleLibraryFileName ) )
            // InternalJniMap.g:9590:1: ( ruleLibraryFileName )
            {
            // InternalJniMap.g:9590:1: ( ruleLibraryFileName )
            // InternalJniMap.g:9591:1: ruleLibraryFileName
            {
             before(grammarAccess.getLibraryAccess().getFilenamesLibraryFileNameParserRuleCall_6_0_0()); 
            pushFollow(FOLLOW_2);
            ruleLibraryFileName();

            state._fsp--;

             after(grammarAccess.getLibraryAccess().getFilenamesLibraryFileNameParserRuleCall_6_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Library__FilenamesAssignment_6_0"


    // $ANTLR start "rule__LibraryFileName__OsAssignment_0"
    // InternalJniMap.g:9600:1: rule__LibraryFileName__OsAssignment_0 : ( ruleHostType ) ;
    public final void rule__LibraryFileName__OsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9604:1: ( ( ruleHostType ) )
            // InternalJniMap.g:9605:1: ( ruleHostType )
            {
            // InternalJniMap.g:9605:1: ( ruleHostType )
            // InternalJniMap.g:9606:1: ruleHostType
            {
             before(grammarAccess.getLibraryFileNameAccess().getOsHostTypeEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleHostType();

            state._fsp--;

             after(grammarAccess.getLibraryFileNameAccess().getOsHostTypeEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LibraryFileName__OsAssignment_0"


    // $ANTLR start "rule__LibraryFileName__FilenameAssignment_2"
    // InternalJniMap.g:9615:1: rule__LibraryFileName__FilenameAssignment_2 : ( RULE_STRING ) ;
    public final void rule__LibraryFileName__FilenameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9619:1: ( ( RULE_STRING ) )
            // InternalJniMap.g:9620:1: ( RULE_STRING )
            {
            // InternalJniMap.g:9620:1: ( RULE_STRING )
            // InternalJniMap.g:9621:1: RULE_STRING
            {
             before(grammarAccess.getLibraryFileNameAccess().getFilenameSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getLibraryFileNameAccess().getFilenameSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LibraryFileName__FilenameAssignment_2"


    // $ANTLR start "rule__IncludeDef__IncludeAssignment_1"
    // InternalJniMap.g:9630:1: rule__IncludeDef__IncludeAssignment_1 : ( RULE_STRING ) ;
    public final void rule__IncludeDef__IncludeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9634:1: ( ( RULE_STRING ) )
            // InternalJniMap.g:9635:1: ( RULE_STRING )
            {
            // InternalJniMap.g:9635:1: ( RULE_STRING )
            // InternalJniMap.g:9636:1: RULE_STRING
            {
             before(grammarAccess.getIncludeDefAccess().getIncludeSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getIncludeDefAccess().getIncludeSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IncludeDef__IncludeAssignment_1"


    // $ANTLR start "rule__UserModule__NameAssignment_1"
    // InternalJniMap.g:9645:1: rule__UserModule__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__UserModule__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9649:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9650:1: ( RULE_ID )
            {
            // InternalJniMap.g:9650:1: ( RULE_ID )
            // InternalJniMap.g:9651:1: RULE_ID
            {
             before(grammarAccess.getUserModuleAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getUserModuleAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserModule__NameAssignment_1"


    // $ANTLR start "rule__UserModule__MethodsAssignment_3"
    // InternalJniMap.g:9660:1: rule__UserModule__MethodsAssignment_3 : ( ruleCMethod ) ;
    public final void rule__UserModule__MethodsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9664:1: ( ( ruleCMethod ) )
            // InternalJniMap.g:9665:1: ( ruleCMethod )
            {
            // InternalJniMap.g:9665:1: ( ruleCMethod )
            // InternalJniMap.g:9666:1: ruleCMethod
            {
             before(grammarAccess.getUserModuleAccess().getMethodsCMethodParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleCMethod();

            state._fsp--;

             after(grammarAccess.getUserModuleAccess().getMethodsCMethodParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserModule__MethodsAssignment_3"


    // $ANTLR start "rule__CMethod__ResAssignment_1"
    // InternalJniMap.g:9675:1: rule__CMethod__ResAssignment_1 : ( ruleType ) ;
    public final void rule__CMethod__ResAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9679:1: ( ( ruleType ) )
            // InternalJniMap.g:9680:1: ( ruleType )
            {
            // InternalJniMap.g:9680:1: ( ruleType )
            // InternalJniMap.g:9681:1: ruleType
            {
             before(grammarAccess.getCMethodAccess().getResTypeParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getCMethodAccess().getResTypeParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__ResAssignment_1"


    // $ANTLR start "rule__CMethod__NameAssignment_2"
    // InternalJniMap.g:9690:1: rule__CMethod__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__CMethod__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9694:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9695:1: ( RULE_ID )
            {
            // InternalJniMap.g:9695:1: ( RULE_ID )
            // InternalJniMap.g:9696:1: RULE_ID
            {
             before(grammarAccess.getCMethodAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCMethodAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__NameAssignment_2"


    // $ANTLR start "rule__CMethod__ParamsAssignment_4_0"
    // InternalJniMap.g:9705:1: rule__CMethod__ParamsAssignment_4_0 : ( ruleParamDef ) ;
    public final void rule__CMethod__ParamsAssignment_4_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9709:1: ( ( ruleParamDef ) )
            // InternalJniMap.g:9710:1: ( ruleParamDef )
            {
            // InternalJniMap.g:9710:1: ( ruleParamDef )
            // InternalJniMap.g:9711:1: ruleParamDef
            {
             before(grammarAccess.getCMethodAccess().getParamsParamDefParserRuleCall_4_0_0()); 
            pushFollow(FOLLOW_2);
            ruleParamDef();

            state._fsp--;

             after(grammarAccess.getCMethodAccess().getParamsParamDefParserRuleCall_4_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__ParamsAssignment_4_0"


    // $ANTLR start "rule__CMethod__ParamsAssignment_4_1_1"
    // InternalJniMap.g:9720:1: rule__CMethod__ParamsAssignment_4_1_1 : ( ruleParamDef ) ;
    public final void rule__CMethod__ParamsAssignment_4_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9724:1: ( ( ruleParamDef ) )
            // InternalJniMap.g:9725:1: ( ruleParamDef )
            {
            // InternalJniMap.g:9725:1: ( ruleParamDef )
            // InternalJniMap.g:9726:1: ruleParamDef
            {
             before(grammarAccess.getCMethodAccess().getParamsParamDefParserRuleCall_4_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParamDef();

            state._fsp--;

             after(grammarAccess.getCMethodAccess().getParamsParamDefParserRuleCall_4_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CMethod__ParamsAssignment_4_1_1"


    // $ANTLR start "rule__TypeAliasDeclaration__TypeAssignment_1"
    // InternalJniMap.g:9735:1: rule__TypeAliasDeclaration__TypeAssignment_1 : ( ruleType ) ;
    public final void rule__TypeAliasDeclaration__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9739:1: ( ( ruleType ) )
            // InternalJniMap.g:9740:1: ( ruleType )
            {
            // InternalJniMap.g:9740:1: ( ruleType )
            // InternalJniMap.g:9741:1: ruleType
            {
             before(grammarAccess.getTypeAliasDeclarationAccess().getTypeTypeParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeAliasDeclarationAccess().getTypeTypeParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeAliasDeclaration__TypeAssignment_1"


    // $ANTLR start "rule__TypeAliasDeclaration__NameAssignment_2"
    // InternalJniMap.g:9750:1: rule__TypeAliasDeclaration__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__TypeAliasDeclaration__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9754:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9755:1: ( RULE_ID )
            {
            // InternalJniMap.g:9755:1: ( RULE_ID )
            // InternalJniMap.g:9756:1: RULE_ID
            {
             before(grammarAccess.getTypeAliasDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTypeAliasDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeAliasDeclaration__NameAssignment_2"


    // $ANTLR start "rule__StructDeclaration__NameAssignment_1"
    // InternalJniMap.g:9765:1: rule__StructDeclaration__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__StructDeclaration__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9769:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9770:1: ( RULE_ID )
            {
            // InternalJniMap.g:9770:1: ( RULE_ID )
            // InternalJniMap.g:9771:1: RULE_ID
            {
             before(grammarAccess.getStructDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getStructDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__NameAssignment_1"


    // $ANTLR start "rule__StructDeclaration__FieldsAssignment_2_1_0"
    // InternalJniMap.g:9780:1: rule__StructDeclaration__FieldsAssignment_2_1_0 : ( ruleFieldDef ) ;
    public final void rule__StructDeclaration__FieldsAssignment_2_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9784:1: ( ( ruleFieldDef ) )
            // InternalJniMap.g:9785:1: ( ruleFieldDef )
            {
            // InternalJniMap.g:9785:1: ( ruleFieldDef )
            // InternalJniMap.g:9786:1: ruleFieldDef
            {
             before(grammarAccess.getStructDeclarationAccess().getFieldsFieldDefParserRuleCall_2_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleFieldDef();

            state._fsp--;

             after(grammarAccess.getStructDeclarationAccess().getFieldsFieldDefParserRuleCall_2_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructDeclaration__FieldsAssignment_2_1_0"


    // $ANTLR start "rule__ClassDeclaration__NameAssignment_1"
    // InternalJniMap.g:9795:1: rule__ClassDeclaration__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__ClassDeclaration__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9799:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9800:1: ( RULE_ID )
            {
            // InternalJniMap.g:9800:1: ( RULE_ID )
            // InternalJniMap.g:9801:1: RULE_ID
            {
             before(grammarAccess.getClassDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getClassDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__NameAssignment_1"


    // $ANTLR start "rule__ClassDeclaration__FieldsAssignment_2_1_0"
    // InternalJniMap.g:9810:1: rule__ClassDeclaration__FieldsAssignment_2_1_0 : ( ruleFieldDef ) ;
    public final void rule__ClassDeclaration__FieldsAssignment_2_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9814:1: ( ( ruleFieldDef ) )
            // InternalJniMap.g:9815:1: ( ruleFieldDef )
            {
            // InternalJniMap.g:9815:1: ( ruleFieldDef )
            // InternalJniMap.g:9816:1: ruleFieldDef
            {
             before(grammarAccess.getClassDeclarationAccess().getFieldsFieldDefParserRuleCall_2_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleFieldDef();

            state._fsp--;

             after(grammarAccess.getClassDeclarationAccess().getFieldsFieldDefParserRuleCall_2_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDeclaration__FieldsAssignment_2_1_0"


    // $ANTLR start "rule__EnumDeclaration__NameAssignment_1"
    // InternalJniMap.g:9825:1: rule__EnumDeclaration__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__EnumDeclaration__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9829:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9830:1: ( RULE_ID )
            {
            // InternalJniMap.g:9830:1: ( RULE_ID )
            // InternalJniMap.g:9831:1: RULE_ID
            {
             before(grammarAccess.getEnumDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEnumDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__NameAssignment_1"


    // $ANTLR start "rule__EnumDeclaration__ValuesAssignment_2_1"
    // InternalJniMap.g:9840:1: rule__EnumDeclaration__ValuesAssignment_2_1 : ( ruleEnumValue ) ;
    public final void rule__EnumDeclaration__ValuesAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9844:1: ( ( ruleEnumValue ) )
            // InternalJniMap.g:9845:1: ( ruleEnumValue )
            {
            // InternalJniMap.g:9845:1: ( ruleEnumValue )
            // InternalJniMap.g:9846:1: ruleEnumValue
            {
             before(grammarAccess.getEnumDeclarationAccess().getValuesEnumValueParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEnumValue();

            state._fsp--;

             after(grammarAccess.getEnumDeclarationAccess().getValuesEnumValueParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__ValuesAssignment_2_1"


    // $ANTLR start "rule__EnumDeclaration__ValuesAssignment_2_2_1"
    // InternalJniMap.g:9855:1: rule__EnumDeclaration__ValuesAssignment_2_2_1 : ( ruleEnumValue ) ;
    public final void rule__EnumDeclaration__ValuesAssignment_2_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9859:1: ( ( ruleEnumValue ) )
            // InternalJniMap.g:9860:1: ( ruleEnumValue )
            {
            // InternalJniMap.g:9860:1: ( ruleEnumValue )
            // InternalJniMap.g:9861:1: ruleEnumValue
            {
             before(grammarAccess.getEnumDeclarationAccess().getValuesEnumValueParserRuleCall_2_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEnumValue();

            state._fsp--;

             after(grammarAccess.getEnumDeclarationAccess().getValuesEnumValueParserRuleCall_2_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumDeclaration__ValuesAssignment_2_2_1"


    // $ANTLR start "rule__EnumType__NameAssignment_1"
    // InternalJniMap.g:9870:1: rule__EnumType__NameAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__EnumType__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9874:1: ( ( ( RULE_ID ) ) )
            // InternalJniMap.g:9875:1: ( ( RULE_ID ) )
            {
            // InternalJniMap.g:9875:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9876:1: ( RULE_ID )
            {
             before(grammarAccess.getEnumTypeAccess().getNameEnumDeclarationCrossReference_1_0()); 
            // InternalJniMap.g:9877:1: ( RULE_ID )
            // InternalJniMap.g:9878:1: RULE_ID
            {
             before(grammarAccess.getEnumTypeAccess().getNameEnumDeclarationIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEnumTypeAccess().getNameEnumDeclarationIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getEnumTypeAccess().getNameEnumDeclarationCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumType__NameAssignment_1"


    // $ANTLR start "rule__EnumValue__NameAssignment_0"
    // InternalJniMap.g:9889:1: rule__EnumValue__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__EnumValue__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9893:1: ( ( RULE_ID ) )
            // InternalJniMap.g:9894:1: ( RULE_ID )
            {
            // InternalJniMap.g:9894:1: ( RULE_ID )
            // InternalJniMap.g:9895:1: RULE_ID
            {
             before(grammarAccess.getEnumValueAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEnumValueAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__NameAssignment_0"


    // $ANTLR start "rule__EnumValue__ValueAssignment_1_1_0"
    // InternalJniMap.g:9904:1: rule__EnumValue__ValueAssignment_1_1_0 : ( RULE_INT ) ;
    public final void rule__EnumValue__ValueAssignment_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9908:1: ( ( RULE_INT ) )
            // InternalJniMap.g:9909:1: ( RULE_INT )
            {
            // InternalJniMap.g:9909:1: ( RULE_INT )
            // InternalJniMap.g:9910:1: RULE_INT
            {
             before(grammarAccess.getEnumValueAccess().getValueINTTerminalRuleCall_1_1_0_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEnumValueAccess().getValueINTTerminalRuleCall_1_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__ValueAssignment_1_1_0"


    // $ANTLR start "rule__EnumValue__EnumNextAssignment_1_1_1"
    // InternalJniMap.g:9919:1: rule__EnumValue__EnumNextAssignment_1_1_1 : ( ruleEnumValue ) ;
    public final void rule__EnumValue__EnumNextAssignment_1_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9923:1: ( ( ruleEnumValue ) )
            // InternalJniMap.g:9924:1: ( ruleEnumValue )
            {
            // InternalJniMap.g:9924:1: ( ruleEnumValue )
            // InternalJniMap.g:9925:1: ruleEnumValue
            {
             before(grammarAccess.getEnumValueAccess().getEnumNextEnumValueParserRuleCall_1_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEnumValue();

            state._fsp--;

             after(grammarAccess.getEnumValueAccess().getEnumNextEnumValueParserRuleCall_1_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__EnumNextAssignment_1_1_1"


    // $ANTLR start "rule__ParamDef__GiveAssignment_0_0"
    // InternalJniMap.g:9934:1: rule__ParamDef__GiveAssignment_0_0 : ( ( 'give' ) ) ;
    public final void rule__ParamDef__GiveAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9938:1: ( ( ( 'give' ) ) )
            // InternalJniMap.g:9939:1: ( ( 'give' ) )
            {
            // InternalJniMap.g:9939:1: ( ( 'give' ) )
            // InternalJniMap.g:9940:1: ( 'give' )
            {
             before(grammarAccess.getParamDefAccess().getGiveGiveKeyword_0_0_0()); 
            // InternalJniMap.g:9941:1: ( 'give' )
            // InternalJniMap.g:9942:1: 'give'
            {
             before(grammarAccess.getParamDefAccess().getGiveGiveKeyword_0_0_0()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getParamDefAccess().getGiveGiveKeyword_0_0_0()); 

            }

             after(grammarAccess.getParamDefAccess().getGiveGiveKeyword_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamDef__GiveAssignment_0_0"


    // $ANTLR start "rule__ParamDef__TakeAssignment_0_1"
    // InternalJniMap.g:9957:1: rule__ParamDef__TakeAssignment_0_1 : ( ( 'take' ) ) ;
    public final void rule__ParamDef__TakeAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9961:1: ( ( ( 'take' ) ) )
            // InternalJniMap.g:9962:1: ( ( 'take' ) )
            {
            // InternalJniMap.g:9962:1: ( ( 'take' ) )
            // InternalJniMap.g:9963:1: ( 'take' )
            {
             before(grammarAccess.getParamDefAccess().getTakeTakeKeyword_0_1_0()); 
            // InternalJniMap.g:9964:1: ( 'take' )
            // InternalJniMap.g:9965:1: 'take'
            {
             before(grammarAccess.getParamDefAccess().getTakeTakeKeyword_0_1_0()); 
            match(input,59,FOLLOW_2); 
             after(grammarAccess.getParamDefAccess().getTakeTakeKeyword_0_1_0()); 

            }

             after(grammarAccess.getParamDefAccess().getTakeTakeKeyword_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamDef__TakeAssignment_0_1"


    // $ANTLR start "rule__ParamDef__KeepAssignment_0_2"
    // InternalJniMap.g:9980:1: rule__ParamDef__KeepAssignment_0_2 : ( ( 'keep' ) ) ;
    public final void rule__ParamDef__KeepAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:9984:1: ( ( ( 'keep' ) ) )
            // InternalJniMap.g:9985:1: ( ( 'keep' ) )
            {
            // InternalJniMap.g:9985:1: ( ( 'keep' ) )
            // InternalJniMap.g:9986:1: ( 'keep' )
            {
             before(grammarAccess.getParamDefAccess().getKeepKeepKeyword_0_2_0()); 
            // InternalJniMap.g:9987:1: ( 'keep' )
            // InternalJniMap.g:9988:1: 'keep'
            {
             before(grammarAccess.getParamDefAccess().getKeepKeepKeyword_0_2_0()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getParamDefAccess().getKeepKeepKeyword_0_2_0()); 

            }

             after(grammarAccess.getParamDefAccess().getKeepKeepKeyword_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamDef__KeepAssignment_0_2"


    // $ANTLR start "rule__ParamDef__TypeAssignment_1"
    // InternalJniMap.g:10003:1: rule__ParamDef__TypeAssignment_1 : ( ruleType ) ;
    public final void rule__ParamDef__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10007:1: ( ( ruleType ) )
            // InternalJniMap.g:10008:1: ( ruleType )
            {
            // InternalJniMap.g:10008:1: ( ruleType )
            // InternalJniMap.g:10009:1: ruleType
            {
             before(grammarAccess.getParamDefAccess().getTypeTypeParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getParamDefAccess().getTypeTypeParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamDef__TypeAssignment_1"


    // $ANTLR start "rule__ParamDef__NameAssignment_2"
    // InternalJniMap.g:10018:1: rule__ParamDef__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__ParamDef__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10022:1: ( ( RULE_ID ) )
            // InternalJniMap.g:10023:1: ( RULE_ID )
            {
            // InternalJniMap.g:10023:1: ( RULE_ID )
            // InternalJniMap.g:10024:1: RULE_ID
            {
             before(grammarAccess.getParamDefAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getParamDefAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamDef__NameAssignment_2"


    // $ANTLR start "rule__MethodGroup__ClassAssignment_1"
    // InternalJniMap.g:10033:1: rule__MethodGroup__ClassAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__MethodGroup__ClassAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10037:1: ( ( ( RULE_ID ) ) )
            // InternalJniMap.g:10038:1: ( ( RULE_ID ) )
            {
            // InternalJniMap.g:10038:1: ( ( RULE_ID ) )
            // InternalJniMap.g:10039:1: ( RULE_ID )
            {
             before(grammarAccess.getMethodGroupAccess().getClassClassMappingCrossReference_1_0()); 
            // InternalJniMap.g:10040:1: ( RULE_ID )
            // InternalJniMap.g:10041:1: RULE_ID
            {
             before(grammarAccess.getMethodGroupAccess().getClassClassMappingIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getMethodGroupAccess().getClassClassMappingIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getMethodGroupAccess().getClassClassMappingCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodGroup__ClassAssignment_1"


    // $ANTLR start "rule__MethodGroup__MethodsAssignment_3"
    // InternalJniMap.g:10052:1: rule__MethodGroup__MethodsAssignment_3 : ( ruleMethod ) ;
    public final void rule__MethodGroup__MethodsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10056:1: ( ( ruleMethod ) )
            // InternalJniMap.g:10057:1: ( ruleMethod )
            {
            // InternalJniMap.g:10057:1: ( ruleMethod )
            // InternalJniMap.g:10058:1: ruleMethod
            {
             before(grammarAccess.getMethodGroupAccess().getMethodsMethodParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleMethod();

            state._fsp--;

             after(grammarAccess.getMethodGroupAccess().getMethodsMethodParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodGroup__MethodsAssignment_3"


    // $ANTLR start "rule__Method__ConstructorAssignment_0_1_0_0"
    // InternalJniMap.g:10067:1: rule__Method__ConstructorAssignment_0_1_0_0 : ( ( 'constructor' ) ) ;
    public final void rule__Method__ConstructorAssignment_0_1_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10071:1: ( ( ( 'constructor' ) ) )
            // InternalJniMap.g:10072:1: ( ( 'constructor' ) )
            {
            // InternalJniMap.g:10072:1: ( ( 'constructor' ) )
            // InternalJniMap.g:10073:1: ( 'constructor' )
            {
             before(grammarAccess.getMethodAccess().getConstructorConstructorKeyword_0_1_0_0_0()); 
            // InternalJniMap.g:10074:1: ( 'constructor' )
            // InternalJniMap.g:10075:1: 'constructor'
            {
             before(grammarAccess.getMethodAccess().getConstructorConstructorKeyword_0_1_0_0_0()); 
            match(input,61,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getConstructorConstructorKeyword_0_1_0_0_0()); 

            }

             after(grammarAccess.getMethodAccess().getConstructorConstructorKeyword_0_1_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__ConstructorAssignment_0_1_0_0"


    // $ANTLR start "rule__Method__DestructorAssignment_0_1_0_1"
    // InternalJniMap.g:10090:1: rule__Method__DestructorAssignment_0_1_0_1 : ( ( 'destructor' ) ) ;
    public final void rule__Method__DestructorAssignment_0_1_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10094:1: ( ( ( 'destructor' ) ) )
            // InternalJniMap.g:10095:1: ( ( 'destructor' ) )
            {
            // InternalJniMap.g:10095:1: ( ( 'destructor' ) )
            // InternalJniMap.g:10096:1: ( 'destructor' )
            {
             before(grammarAccess.getMethodAccess().getDestructorDestructorKeyword_0_1_0_1_0()); 
            // InternalJniMap.g:10097:1: ( 'destructor' )
            // InternalJniMap.g:10098:1: 'destructor'
            {
             before(grammarAccess.getMethodAccess().getDestructorDestructorKeyword_0_1_0_1_0()); 
            match(input,62,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getDestructorDestructorKeyword_0_1_0_1_0()); 

            }

             after(grammarAccess.getMethodAccess().getDestructorDestructorKeyword_0_1_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__DestructorAssignment_0_1_0_1"


    // $ANTLR start "rule__Method__InstanceofAssignment_0_1_0_2"
    // InternalJniMap.g:10113:1: rule__Method__InstanceofAssignment_0_1_0_2 : ( ( 'instanceOf' ) ) ;
    public final void rule__Method__InstanceofAssignment_0_1_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10117:1: ( ( ( 'instanceOf' ) ) )
            // InternalJniMap.g:10118:1: ( ( 'instanceOf' ) )
            {
            // InternalJniMap.g:10118:1: ( ( 'instanceOf' ) )
            // InternalJniMap.g:10119:1: ( 'instanceOf' )
            {
             before(grammarAccess.getMethodAccess().getInstanceofInstanceOfKeyword_0_1_0_2_0()); 
            // InternalJniMap.g:10120:1: ( 'instanceOf' )
            // InternalJniMap.g:10121:1: 'instanceOf'
            {
             before(grammarAccess.getMethodAccess().getInstanceofInstanceOfKeyword_0_1_0_2_0()); 
            match(input,63,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getInstanceofInstanceOfKeyword_0_1_0_2_0()); 

            }

             after(grammarAccess.getMethodAccess().getInstanceofInstanceOfKeyword_0_1_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__InstanceofAssignment_0_1_0_2"


    // $ANTLR start "rule__Method__StaticAssignment_0_1_1"
    // InternalJniMap.g:10136:1: rule__Method__StaticAssignment_0_1_1 : ( ( 'static' ) ) ;
    public final void rule__Method__StaticAssignment_0_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10140:1: ( ( ( 'static' ) ) )
            // InternalJniMap.g:10141:1: ( ( 'static' ) )
            {
            // InternalJniMap.g:10141:1: ( ( 'static' ) )
            // InternalJniMap.g:10142:1: ( 'static' )
            {
             before(grammarAccess.getMethodAccess().getStaticStaticKeyword_0_1_1_0()); 
            // InternalJniMap.g:10143:1: ( 'static' )
            // InternalJniMap.g:10144:1: 'static'
            {
             before(grammarAccess.getMethodAccess().getStaticStaticKeyword_0_1_1_0()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getStaticStaticKeyword_0_1_1_0()); 

            }

             after(grammarAccess.getMethodAccess().getStaticStaticKeyword_0_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__StaticAssignment_0_1_1"


    // $ANTLR start "rule__Method__PrivateAssignment_0_1_2_0"
    // InternalJniMap.g:10159:1: rule__Method__PrivateAssignment_0_1_2_0 : ( ( 'private' ) ) ;
    public final void rule__Method__PrivateAssignment_0_1_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10163:1: ( ( ( 'private' ) ) )
            // InternalJniMap.g:10164:1: ( ( 'private' ) )
            {
            // InternalJniMap.g:10164:1: ( ( 'private' ) )
            // InternalJniMap.g:10165:1: ( 'private' )
            {
             before(grammarAccess.getMethodAccess().getPrivatePrivateKeyword_0_1_2_0_0()); 
            // InternalJniMap.g:10166:1: ( 'private' )
            // InternalJniMap.g:10167:1: 'private'
            {
             before(grammarAccess.getMethodAccess().getPrivatePrivateKeyword_0_1_2_0_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getPrivatePrivateKeyword_0_1_2_0_0()); 

            }

             after(grammarAccess.getMethodAccess().getPrivatePrivateKeyword_0_1_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__PrivateAssignment_0_1_2_0"


    // $ANTLR start "rule__Method__ProtectedAssignment_0_1_2_1"
    // InternalJniMap.g:10182:1: rule__Method__ProtectedAssignment_0_1_2_1 : ( ( 'protected' ) ) ;
    public final void rule__Method__ProtectedAssignment_0_1_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10186:1: ( ( ( 'protected' ) ) )
            // InternalJniMap.g:10187:1: ( ( 'protected' ) )
            {
            // InternalJniMap.g:10187:1: ( ( 'protected' ) )
            // InternalJniMap.g:10188:1: ( 'protected' )
            {
             before(grammarAccess.getMethodAccess().getProtectedProtectedKeyword_0_1_2_1_0()); 
            // InternalJniMap.g:10189:1: ( 'protected' )
            // InternalJniMap.g:10190:1: 'protected'
            {
             before(grammarAccess.getMethodAccess().getProtectedProtectedKeyword_0_1_2_1_0()); 
            match(input,65,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getProtectedProtectedKeyword_0_1_2_1_0()); 

            }

             after(grammarAccess.getMethodAccess().getProtectedProtectedKeyword_0_1_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__ProtectedAssignment_0_1_2_1"


    // $ANTLR start "rule__Method__RenamedAssignment_0_1_3_0"
    // InternalJniMap.g:10205:1: rule__Method__RenamedAssignment_0_1_3_0 : ( ( 'rename' ) ) ;
    public final void rule__Method__RenamedAssignment_0_1_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10209:1: ( ( ( 'rename' ) ) )
            // InternalJniMap.g:10210:1: ( ( 'rename' ) )
            {
            // InternalJniMap.g:10210:1: ( ( 'rename' ) )
            // InternalJniMap.g:10211:1: ( 'rename' )
            {
             before(grammarAccess.getMethodAccess().getRenamedRenameKeyword_0_1_3_0_0()); 
            // InternalJniMap.g:10212:1: ( 'rename' )
            // InternalJniMap.g:10213:1: 'rename'
            {
             before(grammarAccess.getMethodAccess().getRenamedRenameKeyword_0_1_3_0_0()); 
            match(input,66,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getRenamedRenameKeyword_0_1_3_0_0()); 

            }

             after(grammarAccess.getMethodAccess().getRenamedRenameKeyword_0_1_3_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__RenamedAssignment_0_1_3_0"


    // $ANTLR start "rule__Method__NewnameAssignment_0_1_3_2"
    // InternalJniMap.g:10228:1: rule__Method__NewnameAssignment_0_1_3_2 : ( RULE_ID ) ;
    public final void rule__Method__NewnameAssignment_0_1_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10232:1: ( ( RULE_ID ) )
            // InternalJniMap.g:10233:1: ( RULE_ID )
            {
            // InternalJniMap.g:10233:1: ( RULE_ID )
            // InternalJniMap.g:10234:1: RULE_ID
            {
             before(grammarAccess.getMethodAccess().getNewnameIDTerminalRuleCall_0_1_3_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getNewnameIDTerminalRuleCall_0_1_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__NewnameAssignment_0_1_3_2"


    // $ANTLR start "rule__Method__StubAssignment_0_1_4"
    // InternalJniMap.g:10243:1: rule__Method__StubAssignment_0_1_4 : ( ( 'stub' ) ) ;
    public final void rule__Method__StubAssignment_0_1_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10247:1: ( ( ( 'stub' ) ) )
            // InternalJniMap.g:10248:1: ( ( 'stub' ) )
            {
            // InternalJniMap.g:10248:1: ( ( 'stub' ) )
            // InternalJniMap.g:10249:1: ( 'stub' )
            {
             before(grammarAccess.getMethodAccess().getStubStubKeyword_0_1_4_0()); 
            // InternalJniMap.g:10250:1: ( 'stub' )
            // InternalJniMap.g:10251:1: 'stub'
            {
             before(grammarAccess.getMethodAccess().getStubStubKeyword_0_1_4_0()); 
            match(input,67,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getStubStubKeyword_0_1_4_0()); 

            }

             after(grammarAccess.getMethodAccess().getStubStubKeyword_0_1_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__StubAssignment_0_1_4"


    // $ANTLR start "rule__Method__ArgAssignment_0_1_5_2"
    // InternalJniMap.g:10266:1: rule__Method__ArgAssignment_0_1_5_2 : ( RULE_INT ) ;
    public final void rule__Method__ArgAssignment_0_1_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10270:1: ( ( RULE_INT ) )
            // InternalJniMap.g:10271:1: ( RULE_INT )
            {
            // InternalJniMap.g:10271:1: ( RULE_INT )
            // InternalJniMap.g:10272:1: RULE_INT
            {
             before(grammarAccess.getMethodAccess().getArgINTTerminalRuleCall_0_1_5_2_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getArgINTTerminalRuleCall_0_1_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__ArgAssignment_0_1_5_2"


    // $ANTLR start "rule__Method__ResAssignment_2"
    // InternalJniMap.g:10281:1: rule__Method__ResAssignment_2 : ( ruleType ) ;
    public final void rule__Method__ResAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10285:1: ( ( ruleType ) )
            // InternalJniMap.g:10286:1: ( ruleType )
            {
            // InternalJniMap.g:10286:1: ( ruleType )
            // InternalJniMap.g:10287:1: ruleType
            {
             before(grammarAccess.getMethodAccess().getResTypeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getMethodAccess().getResTypeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__ResAssignment_2"


    // $ANTLR start "rule__Method__NameAssignment_3"
    // InternalJniMap.g:10296:1: rule__Method__NameAssignment_3 : ( RULE_ID ) ;
    public final void rule__Method__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10300:1: ( ( RULE_ID ) )
            // InternalJniMap.g:10301:1: ( RULE_ID )
            {
            // InternalJniMap.g:10301:1: ( RULE_ID )
            // InternalJniMap.g:10302:1: RULE_ID
            {
             before(grammarAccess.getMethodAccess().getNameIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getMethodAccess().getNameIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__NameAssignment_3"


    // $ANTLR start "rule__Method__ParamsAssignment_5_0"
    // InternalJniMap.g:10311:1: rule__Method__ParamsAssignment_5_0 : ( ruleParamDef ) ;
    public final void rule__Method__ParamsAssignment_5_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10315:1: ( ( ruleParamDef ) )
            // InternalJniMap.g:10316:1: ( ruleParamDef )
            {
            // InternalJniMap.g:10316:1: ( ruleParamDef )
            // InternalJniMap.g:10317:1: ruleParamDef
            {
             before(grammarAccess.getMethodAccess().getParamsParamDefParserRuleCall_5_0_0()); 
            pushFollow(FOLLOW_2);
            ruleParamDef();

            state._fsp--;

             after(grammarAccess.getMethodAccess().getParamsParamDefParserRuleCall_5_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__ParamsAssignment_5_0"


    // $ANTLR start "rule__Method__ParamsAssignment_5_1_1"
    // InternalJniMap.g:10326:1: rule__Method__ParamsAssignment_5_1_1 : ( ruleParamDef ) ;
    public final void rule__Method__ParamsAssignment_5_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10330:1: ( ( ruleParamDef ) )
            // InternalJniMap.g:10331:1: ( ruleParamDef )
            {
            // InternalJniMap.g:10331:1: ( ruleParamDef )
            // InternalJniMap.g:10332:1: ruleParamDef
            {
             before(grammarAccess.getMethodAccess().getParamsParamDefParserRuleCall_5_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParamDef();

            state._fsp--;

             after(grammarAccess.getMethodAccess().getParamsParamDefParserRuleCall_5_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__ParamsAssignment_5_1_1"


    // $ANTLR start "rule__BooleanType__NameAssignment"
    // InternalJniMap.g:10341:1: rule__BooleanType__NameAssignment : ( ( 'boolean' ) ) ;
    public final void rule__BooleanType__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10345:1: ( ( ( 'boolean' ) ) )
            // InternalJniMap.g:10346:1: ( ( 'boolean' ) )
            {
            // InternalJniMap.g:10346:1: ( ( 'boolean' ) )
            // InternalJniMap.g:10347:1: ( 'boolean' )
            {
             before(grammarAccess.getBooleanTypeAccess().getNameBooleanKeyword_0()); 
            // InternalJniMap.g:10348:1: ( 'boolean' )
            // InternalJniMap.g:10349:1: 'boolean'
            {
             before(grammarAccess.getBooleanTypeAccess().getNameBooleanKeyword_0()); 
            match(input,68,FOLLOW_2); 
             after(grammarAccess.getBooleanTypeAccess().getNameBooleanKeyword_0()); 

            }

             after(grammarAccess.getBooleanTypeAccess().getNameBooleanKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanType__NameAssignment"


    // $ANTLR start "rule__IntegerType__NameAssignment"
    // InternalJniMap.g:10364:1: rule__IntegerType__NameAssignment : ( ( rule__IntegerType__NameAlternatives_0 ) ) ;
    public final void rule__IntegerType__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10368:1: ( ( ( rule__IntegerType__NameAlternatives_0 ) ) )
            // InternalJniMap.g:10369:1: ( ( rule__IntegerType__NameAlternatives_0 ) )
            {
            // InternalJniMap.g:10369:1: ( ( rule__IntegerType__NameAlternatives_0 ) )
            // InternalJniMap.g:10370:1: ( rule__IntegerType__NameAlternatives_0 )
            {
             before(grammarAccess.getIntegerTypeAccess().getNameAlternatives_0()); 
            // InternalJniMap.g:10371:1: ( rule__IntegerType__NameAlternatives_0 )
            // InternalJniMap.g:10371:2: rule__IntegerType__NameAlternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__IntegerType__NameAlternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getIntegerTypeAccess().getNameAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerType__NameAssignment"


    // $ANTLR start "rule__RealType__NameAssignment"
    // InternalJniMap.g:10380:1: rule__RealType__NameAssignment : ( ( rule__RealType__NameAlternatives_0 ) ) ;
    public final void rule__RealType__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10384:1: ( ( ( rule__RealType__NameAlternatives_0 ) ) )
            // InternalJniMap.g:10385:1: ( ( rule__RealType__NameAlternatives_0 ) )
            {
            // InternalJniMap.g:10385:1: ( ( rule__RealType__NameAlternatives_0 ) )
            // InternalJniMap.g:10386:1: ( rule__RealType__NameAlternatives_0 )
            {
             before(grammarAccess.getRealTypeAccess().getNameAlternatives_0()); 
            // InternalJniMap.g:10387:1: ( rule__RealType__NameAlternatives_0 )
            // InternalJniMap.g:10387:2: rule__RealType__NameAlternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__RealType__NameAlternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getRealTypeAccess().getNameAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RealType__NameAssignment"


    // $ANTLR start "rule__StringType__NameAssignment"
    // InternalJniMap.g:10396:1: rule__StringType__NameAssignment : ( ( 'string' ) ) ;
    public final void rule__StringType__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10400:1: ( ( ( 'string' ) ) )
            // InternalJniMap.g:10401:1: ( ( 'string' ) )
            {
            // InternalJniMap.g:10401:1: ( ( 'string' ) )
            // InternalJniMap.g:10402:1: ( 'string' )
            {
             before(grammarAccess.getStringTypeAccess().getNameStringKeyword_0()); 
            // InternalJniMap.g:10403:1: ( 'string' )
            // InternalJniMap.g:10404:1: 'string'
            {
             before(grammarAccess.getStringTypeAccess().getNameStringKeyword_0()); 
            match(input,69,FOLLOW_2); 
             after(grammarAccess.getStringTypeAccess().getNameStringKeyword_0()); 

            }

             after(grammarAccess.getStringTypeAccess().getNameStringKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringType__NameAssignment"


    // $ANTLR start "rule__VoidType__NameAssignment"
    // InternalJniMap.g:10419:1: rule__VoidType__NameAssignment : ( ( 'void' ) ) ;
    public final void rule__VoidType__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10423:1: ( ( ( 'void' ) ) )
            // InternalJniMap.g:10424:1: ( ( 'void' ) )
            {
            // InternalJniMap.g:10424:1: ( ( 'void' ) )
            // InternalJniMap.g:10425:1: ( 'void' )
            {
             before(grammarAccess.getVoidTypeAccess().getNameVoidKeyword_0()); 
            // InternalJniMap.g:10426:1: ( 'void' )
            // InternalJniMap.g:10427:1: 'void'
            {
             before(grammarAccess.getVoidTypeAccess().getNameVoidKeyword_0()); 
            match(input,70,FOLLOW_2); 
             after(grammarAccess.getVoidTypeAccess().getNameVoidKeyword_0()); 

            }

             after(grammarAccess.getVoidTypeAccess().getNameVoidKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VoidType__NameAssignment"


    // $ANTLR start "rule__TypeAlias__AliasAssignment"
    // InternalJniMap.g:10442:1: rule__TypeAlias__AliasAssignment : ( ( RULE_ID ) ) ;
    public final void rule__TypeAlias__AliasAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10446:1: ( ( ( RULE_ID ) ) )
            // InternalJniMap.g:10447:1: ( ( RULE_ID ) )
            {
            // InternalJniMap.g:10447:1: ( ( RULE_ID ) )
            // InternalJniMap.g:10448:1: ( RULE_ID )
            {
             before(grammarAccess.getTypeAliasAccess().getAliasTypeAliasDeclarationCrossReference_0()); 
            // InternalJniMap.g:10449:1: ( RULE_ID )
            // InternalJniMap.g:10450:1: RULE_ID
            {
             before(grammarAccess.getTypeAliasAccess().getAliasTypeAliasDeclarationIDTerminalRuleCall_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTypeAliasAccess().getAliasTypeAliasDeclarationIDTerminalRuleCall_0_1()); 

            }

             after(grammarAccess.getTypeAliasAccess().getAliasTypeAliasDeclarationCrossReference_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeAlias__AliasAssignment"


    // $ANTLR start "rule__ArrayType__BaseAssignment_0"
    // InternalJniMap.g:10461:1: rule__ArrayType__BaseAssignment_0 : ( ruleBaseType ) ;
    public final void rule__ArrayType__BaseAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10465:1: ( ( ruleBaseType ) )
            // InternalJniMap.g:10466:1: ( ruleBaseType )
            {
            // InternalJniMap.g:10466:1: ( ruleBaseType )
            // InternalJniMap.g:10467:1: ruleBaseType
            {
             before(grammarAccess.getArrayTypeAccess().getBaseBaseTypeParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleBaseType();

            state._fsp--;

             after(grammarAccess.getArrayTypeAccess().getBaseBaseTypeParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArrayType__BaseAssignment_0"


    // $ANTLR start "rule__PtrType__BaseAssignment_0"
    // InternalJniMap.g:10476:1: rule__PtrType__BaseAssignment_0 : ( ruleBaseType ) ;
    public final void rule__PtrType__BaseAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10480:1: ( ( ruleBaseType ) )
            // InternalJniMap.g:10481:1: ( ruleBaseType )
            {
            // InternalJniMap.g:10481:1: ( ruleBaseType )
            // InternalJniMap.g:10482:1: ruleBaseType
            {
             before(grammarAccess.getPtrTypeAccess().getBaseBaseTypeParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleBaseType();

            state._fsp--;

             after(grammarAccess.getPtrTypeAccess().getBaseBaseTypeParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PtrType__BaseAssignment_0"


    // $ANTLR start "rule__PtrType__IndirAssignment_1"
    // InternalJniMap.g:10491:1: rule__PtrType__IndirAssignment_1 : ( ( '*' ) ) ;
    public final void rule__PtrType__IndirAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10495:1: ( ( ( '*' ) ) )
            // InternalJniMap.g:10496:1: ( ( '*' ) )
            {
            // InternalJniMap.g:10496:1: ( ( '*' ) )
            // InternalJniMap.g:10497:1: ( '*' )
            {
             before(grammarAccess.getPtrTypeAccess().getIndirAsteriskKeyword_1_0()); 
            // InternalJniMap.g:10498:1: ( '*' )
            // InternalJniMap.g:10499:1: '*'
            {
             before(grammarAccess.getPtrTypeAccess().getIndirAsteriskKeyword_1_0()); 
            match(input,71,FOLLOW_2); 
             after(grammarAccess.getPtrTypeAccess().getIndirAsteriskKeyword_1_0()); 

            }

             after(grammarAccess.getPtrTypeAccess().getIndirAsteriskKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PtrType__IndirAssignment_1"


    // $ANTLR start "rule__PtrType__IgnoredAssignment_2"
    // InternalJniMap.g:10514:1: rule__PtrType__IgnoredAssignment_2 : ( ( 'ignored' ) ) ;
    public final void rule__PtrType__IgnoredAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10518:1: ( ( ( 'ignored' ) ) )
            // InternalJniMap.g:10519:1: ( ( 'ignored' ) )
            {
            // InternalJniMap.g:10519:1: ( ( 'ignored' ) )
            // InternalJniMap.g:10520:1: ( 'ignored' )
            {
             before(grammarAccess.getPtrTypeAccess().getIgnoredIgnoredKeyword_2_0()); 
            // InternalJniMap.g:10521:1: ( 'ignored' )
            // InternalJniMap.g:10522:1: 'ignored'
            {
             before(grammarAccess.getPtrTypeAccess().getIgnoredIgnoredKeyword_2_0()); 
            match(input,72,FOLLOW_2); 
             after(grammarAccess.getPtrTypeAccess().getIgnoredIgnoredKeyword_2_0()); 

            }

             after(grammarAccess.getPtrTypeAccess().getIgnoredIgnoredKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PtrType__IgnoredAssignment_2"


    // $ANTLR start "rule__ModifiableType__BaseAssignment_0"
    // InternalJniMap.g:10537:1: rule__ModifiableType__BaseAssignment_0 : ( ruleBaseType ) ;
    public final void rule__ModifiableType__BaseAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10541:1: ( ( ruleBaseType ) )
            // InternalJniMap.g:10542:1: ( ruleBaseType )
            {
            // InternalJniMap.g:10542:1: ( ruleBaseType )
            // InternalJniMap.g:10543:1: ruleBaseType
            {
             before(grammarAccess.getModifiableTypeAccess().getBaseBaseTypeParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleBaseType();

            state._fsp--;

             after(grammarAccess.getModifiableTypeAccess().getBaseBaseTypeParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModifiableType__BaseAssignment_0"


    // $ANTLR start "rule__ModifiableType__IndirAssignment_1"
    // InternalJniMap.g:10552:1: rule__ModifiableType__IndirAssignment_1 : ( ( '&' ) ) ;
    public final void rule__ModifiableType__IndirAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10556:1: ( ( ( '&' ) ) )
            // InternalJniMap.g:10557:1: ( ( '&' ) )
            {
            // InternalJniMap.g:10557:1: ( ( '&' ) )
            // InternalJniMap.g:10558:1: ( '&' )
            {
             before(grammarAccess.getModifiableTypeAccess().getIndirAmpersandKeyword_1_0()); 
            // InternalJniMap.g:10559:1: ( '&' )
            // InternalJniMap.g:10560:1: '&'
            {
             before(grammarAccess.getModifiableTypeAccess().getIndirAmpersandKeyword_1_0()); 
            match(input,73,FOLLOW_2); 
             after(grammarAccess.getModifiableTypeAccess().getIndirAmpersandKeyword_1_0()); 

            }

             after(grammarAccess.getModifiableTypeAccess().getIndirAmpersandKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModifiableType__IndirAssignment_1"


    // $ANTLR start "rule__StructType__NameAssignment_1"
    // InternalJniMap.g:10575:1: rule__StructType__NameAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__StructType__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10579:1: ( ( ( RULE_ID ) ) )
            // InternalJniMap.g:10580:1: ( ( RULE_ID ) )
            {
            // InternalJniMap.g:10580:1: ( ( RULE_ID ) )
            // InternalJniMap.g:10581:1: ( RULE_ID )
            {
             before(grammarAccess.getStructTypeAccess().getNameStructDeclarationCrossReference_1_0()); 
            // InternalJniMap.g:10582:1: ( RULE_ID )
            // InternalJniMap.g:10583:1: RULE_ID
            {
             before(grammarAccess.getStructTypeAccess().getNameStructDeclarationIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getStructTypeAccess().getNameStructDeclarationIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getStructTypeAccess().getNameStructDeclarationCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__NameAssignment_1"


    // $ANTLR start "rule__FieldDef__CopyAssignment_0"
    // InternalJniMap.g:10594:1: rule__FieldDef__CopyAssignment_0 : ( ( 'copyOnGet' ) ) ;
    public final void rule__FieldDef__CopyAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10598:1: ( ( ( 'copyOnGet' ) ) )
            // InternalJniMap.g:10599:1: ( ( 'copyOnGet' ) )
            {
            // InternalJniMap.g:10599:1: ( ( 'copyOnGet' ) )
            // InternalJniMap.g:10600:1: ( 'copyOnGet' )
            {
             before(grammarAccess.getFieldDefAccess().getCopyCopyOnGetKeyword_0_0()); 
            // InternalJniMap.g:10601:1: ( 'copyOnGet' )
            // InternalJniMap.g:10602:1: 'copyOnGet'
            {
             before(grammarAccess.getFieldDefAccess().getCopyCopyOnGetKeyword_0_0()); 
            match(input,74,FOLLOW_2); 
             after(grammarAccess.getFieldDefAccess().getCopyCopyOnGetKeyword_0_0()); 

            }

             after(grammarAccess.getFieldDefAccess().getCopyCopyOnGetKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__CopyAssignment_0"


    // $ANTLR start "rule__FieldDef__TypeAssignment_1"
    // InternalJniMap.g:10617:1: rule__FieldDef__TypeAssignment_1 : ( ruleType ) ;
    public final void rule__FieldDef__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10621:1: ( ( ruleType ) )
            // InternalJniMap.g:10622:1: ( ruleType )
            {
            // InternalJniMap.g:10622:1: ( ruleType )
            // InternalJniMap.g:10623:1: ruleType
            {
             before(grammarAccess.getFieldDefAccess().getTypeTypeParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getFieldDefAccess().getTypeTypeParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__TypeAssignment_1"


    // $ANTLR start "rule__FieldDef__NameAssignment_2"
    // InternalJniMap.g:10632:1: rule__FieldDef__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__FieldDef__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10636:1: ( ( RULE_ID ) )
            // InternalJniMap.g:10637:1: ( RULE_ID )
            {
            // InternalJniMap.g:10637:1: ( RULE_ID )
            // InternalJniMap.g:10638:1: RULE_ID
            {
             before(grammarAccess.getFieldDefAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFieldDefAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__NameAssignment_2"


    // $ANTLR start "rule__FieldDef__ManyAssignment_3_1"
    // InternalJniMap.g:10647:1: rule__FieldDef__ManyAssignment_3_1 : ( ( 'many' ) ) ;
    public final void rule__FieldDef__ManyAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10651:1: ( ( ( 'many' ) ) )
            // InternalJniMap.g:10652:1: ( ( 'many' ) )
            {
            // InternalJniMap.g:10652:1: ( ( 'many' ) )
            // InternalJniMap.g:10653:1: ( 'many' )
            {
             before(grammarAccess.getFieldDefAccess().getManyManyKeyword_3_1_0()); 
            // InternalJniMap.g:10654:1: ( 'many' )
            // InternalJniMap.g:10655:1: 'many'
            {
             before(grammarAccess.getFieldDefAccess().getManyManyKeyword_3_1_0()); 
            match(input,75,FOLLOW_2); 
             after(grammarAccess.getFieldDefAccess().getManyManyKeyword_3_1_0()); 

            }

             after(grammarAccess.getFieldDefAccess().getManyManyKeyword_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldDef__ManyAssignment_3_1"


    // $ANTLR start "rule__ConstType__BaseAssignment_1"
    // InternalJniMap.g:10670:1: rule__ConstType__BaseAssignment_1 : ( ruleType ) ;
    public final void rule__ConstType__BaseAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalJniMap.g:10674:1: ( ( ruleType ) )
            // InternalJniMap.g:10675:1: ( ruleType )
            {
            // InternalJniMap.g:10675:1: ( ruleType )
            // InternalJniMap.g:10676:1: ruleType
            {
             before(grammarAccess.getConstTypeAccess().getBaseTypeParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getConstTypeAccess().getBaseTypeParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstType__BaseAssignment_1"

    // Delegated rules


    protected DFA9 dfa9 = new DFA9(this);
    static final String dfa_1s = "\24\uffff";
    static final String dfa_2s = "\1\uffff\11\20\2\uffff\1\20\5\uffff\2\20";
    static final String dfa_3s = "\15\4\5\uffff\2\4";
    static final String dfa_4s = "\1\106\11\111\2\4\1\111\5\uffff\2\111";
    static final String dfa_5s = "\15\uffff\1\4\1\2\1\5\1\3\1\1\2\uffff";
    static final String dfa_6s = "\24\uffff}>";
    static final String[] dfa_7s = {
            "\1\14\10\uffff\1\2\1\3\1\4\1\5\1\6\1\7\40\uffff\1\12\1\uffff\1\13\2\uffff\1\15\13\uffff\1\1\1\10\1\11",
            "\1\20\32\uffff\1\17\47\uffff\1\21\1\uffff\1\16",
            "\1\20\32\uffff\1\17\47\uffff\1\21\1\uffff\1\16",
            "\1\20\32\uffff\1\17\47\uffff\1\21\1\uffff\1\16",
            "\1\20\32\uffff\1\17\47\uffff\1\21\1\uffff\1\16",
            "\1\20\32\uffff\1\17\47\uffff\1\21\1\uffff\1\16",
            "\1\20\32\uffff\1\17\47\uffff\1\21\1\uffff\1\16",
            "\1\20\32\uffff\1\17\47\uffff\1\21\1\uffff\1\16",
            "\1\20\32\uffff\1\17\47\uffff\1\21\1\uffff\1\16",
            "\1\20\32\uffff\1\17\47\uffff\1\21\1\uffff\1\16",
            "\1\22",
            "\1\23",
            "\1\20\32\uffff\1\17\47\uffff\1\21\1\uffff\1\16",
            "",
            "",
            "",
            "",
            "",
            "\1\20\32\uffff\1\17\47\uffff\1\21\1\uffff\1\16",
            "\1\20\32\uffff\1\17\47\uffff\1\21\1\uffff\1\16"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA9 extends DFA {

        public DFA9(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "1482:1: rule__Type__Alternatives : ( ( rulePtrType ) | ( ruleModifiableType ) | ( ruleBaseType ) | ( ruleConstType ) | ( ruleArrayType ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000088000000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x003F170000000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000040000000002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000100000000002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x003F170000000002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0040000000000002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000001F80000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000300000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000001200000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000004004000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x000000C804000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0200000000000010L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000004804000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000001F80002L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x012880000007F810L,0x0000000000000070L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x012800000007F812L,0x0000000000000070L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x012800000007F810L,0x0000000000000070L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x1D2800004007F810L,0x0000000000000070L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x1D2800000007F810L,0x0000000000000070L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x012800000007F810L,0x0000000000000470L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x012800000007F812L,0x0000000000000470L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000800200000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x012800008007F810L,0x0000000000000070L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x012800008007F812L,0x0000000000000070L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0xE080000000001000L,0x000000000000000FL});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000080L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0xE080000000001002L,0x000000000000000FL});

}
