/*
 * generated by Xtext
 */
package fr.irisa.cairn.ui.labeling;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.xtext.ui.label.DefaultEObjectLabelProvider;

import com.google.inject.Inject;

import fr.irisa.cairn.jniMap.ClassDeclaration;
import fr.irisa.cairn.jniMap.ClassMapping;
import fr.irisa.cairn.jniMap.EnumDeclaration;
import fr.irisa.cairn.jniMap.FieldDef;
import fr.irisa.cairn.jniMap.IncludeDef;
import fr.irisa.cairn.jniMap.Library;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.Method;
import fr.irisa.cairn.jniMap.MethodGroup;
import fr.irisa.cairn.jniMap.StructDeclaration;
import fr.irisa.cairn.jniMap.TypeAliasDeclaration;
import fr.irisa.cairn.jniMap.UserModule;

/**
 * Provides labels for a EObjects.
 * 
 * see
 * http://www.eclipse.org/Xtext/documentation/latest/xtext.html#labelProvider
 */
public class JniMapLabelProvider extends DefaultEObjectLabelProvider {

	@Inject
	public JniMapLabelProvider(AdapterFactoryLabelProvider delegate) {
		super(delegate);
	}

	/*
	 * //Labels and icons can be computed like this:
	 * 
	 * String text(MyModel ele) { return "my "+ele.getName(); }
	 */
	String image(ClassMapping ele) {
		return "class_obj.gif";
	}
	
	String image(StructDeclaration ele) {
		return "struct_obj.gif";
	}
	
	String image (UserModule lib){
		return "usermodule.png";
	}
	String image (Library lib){
		return "jar.png";
	}
	
	String image(EnumDeclaration ele) {
		return "enum_obj.gif";
	}
	
	String image(ClassDeclaration ele) {
		return "class_obj.gif";
	}
	
	String image(FieldDef ele){
		return "field_public_obj.gif";
	}
	
	String image(Method ele){
		return "function_obj.gif";
	}
	
	String image(Mapping ele){
		return "mapping.png";
	}
	
	String image(TypeAliasDeclaration ele){
		return "typedef_obj.gif";
	}
	
	String image(IncludeDef ele){
		return "include_obj.gif";
	}
	
	String text(MethodGroup ele) {
		return ele.getClass_().getName();
	}
}
