package fr.irisa.cairn.ui.handler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;

import com.google.inject.Inject;
import com.google.inject.Provider;

import fr.irisa.cairn.jnimap.generator.JNIMappingGenerator;

public class GenerationHandler extends AbstractHandler implements IHandler {
	
	private static final boolean debug = false;
	private static void debug(Object o) {
		if (debug) System.out.println(o);
	}

//    @Inject
//    private IGenerator generator;
    
    @Inject
    private JNIMappingGenerator jnigenerator;

    @Inject
    private Provider<EclipseResourceFileSystemAccess2> fileAccessProvider;

    @Inject
    IResourceDescriptions resourceDescriptions;
    
    @Inject
    IResourceSetProvider resourceSetProvider;
    
    /**
     * Workaround for displaying full stack trace.
     * Copied from: 
     *    http://stackoverflow.com/questions/2826959/jface-errordialog-how-do-i-show-something-in-the-details-portion/9404081#9404081
     * 
     * @param msg
     * @param t
     */
    private static void errorDialogWithStackTrace(String msg, Throwable t) {
    	final String PLUGIN_ID = "fr.irisa.cairn.jnimap.ui";

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);

        final String trace = sw.toString(); // stack trace as a string

        // Temp holder of child statuses
        List<Status> childStatuses = new ArrayList<Status>();

        // Split output by OS-independent new-line
        for (String line : trace.split(System.getProperty("line.separator"))) {
            // build & add status
            childStatuses.add(new Status(IStatus.ERROR, PLUGIN_ID, line));
        }

        MultiStatus ms = new MultiStatus(PLUGIN_ID, IStatus.ERROR,
                childStatuses.toArray(new Status[] {}), // convert to array of statuses
                t.getLocalizedMessage(), t);
        ErrorDialog.openError(null, "Error Generating JNI Mapping", msg, ms);
    }
    
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
    	debug("[JNIMapping] START");
        try {
	        ISelection selection = HandlerUtil.getCurrentSelection(event);
            if (selection instanceof StructuredSelection) {
            	StructuredSelection structuredSelection = (StructuredSelection) selection;
    			for (Object o : structuredSelection.toArray()) {
    				final IFile file = (IFile) (o);
    				generate(file);
    			}
    	        MessageDialog.openInformation(null, "JNIMapping Generated", "JNIMapping have been successfully generated.");
            } else if (selection instanceof TextSelection) {      
            	IEditorInput input = HandlerUtil.getActiveEditorInput(event);
            	final IFile file = (IFile)input.getAdapter(IFile.class);
            	generate(file);
    	        MessageDialog.openInformation(null, "JNIMapping Generated", "JNIMapping have been successfully generated.");
            } else {
            	String msg = "[JNIMapping] ERROR. Unsupported selection : "+selection.getClass().getSimpleName();
            	errorDialogWithStackTrace(msg,new IllegalArgumentException(msg));
            }
        } catch (Exception e) {
        	String msg = "[JNIMapping] ERROR while processing selection.";
        	errorDialogWithStackTrace(msg, e);
        	throw new RuntimeException(msg,e);
        }
        debug("[JNIMapping] END");
        return null;
    }

    private void generate(IFile file) {
    	debug("[JNIMapping] using IFile : "+file.toString());
        IProject project = file.getProject();
        URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
        generate(uri,project);
	}

	private void generate(URI uri, final IProject project) {
        debug("[JNIMapping] JNIMap URI : "+uri.toString());
		debug("[JNIMapping] IProject : "+project.toString());
        final EclipseResourceFileSystemAccess2 fsa = fileAccessProvider.get();
        fsa.setProject(project);
        fsa.setMonitor(new NullProgressMonitor());
        fsa.setOutputPath("./");
        fsa.getOutputConfigurations().get(EclipseResourceFileSystemAccess2.DEFAULT_OUTPUT).setSetDerivedProperty(false);
        fsa.getOutputConfigurations().get(EclipseResourceFileSystemAccess2.DEFAULT_OUTPUT).setCreateOutputDirectory(true);

        ResourceSet rs = resourceSetProvider.get(project);
        Resource r = rs.getResource(uri, true);
        EcoreUtil.resolveAll(r);
        jnigenerator.doGenerate(r, fsa); // Instead of the basic generator
        
		try {
			final IWorkspace ws = ResourcesPlugin.getWorkspace();
			ws.run(new IWorkspaceRunnable() {
				@Override
				public void run(IProgressMonitor monitor) throws CoreException {
					project.refreshLocal(IResource.DEPTH_INFINITE, null);
				}
			}, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	@Override
    public boolean isEnabled() {
        return true;
    }
    
}
