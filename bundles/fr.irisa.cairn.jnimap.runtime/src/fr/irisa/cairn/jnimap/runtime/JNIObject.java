package fr.irisa.cairn.jnimap.runtime;

/**
 * Common parent for all the classes wrapping a JNI object.
 * 
 * @author sderrien
 * @author amorvan
 * @author tyuki
 * @author giooss
 */
public abstract class JNIObject {
	
	public static Object LOCK = new Object();
	
	private final long nativePtr;
	protected boolean taken = false;
	
	private static final long NULL_VALUE = 0;
	
	protected JNIObject(long ptr) {
		if (isNull(ptr)) {
			throw new RuntimeException("Cannot create a JNIObject from a null pointer");
		} else {
			this.nativePtr = ptr;
		}
	}
	
	public static boolean isNull(long ptr) {
		return (ptr == NULL_VALUE);
	}
	
	private long getNativePtr() {
		if (taken) {
			throw new IllegalStateException("This object has already been freed.");
		}
		return nativePtr; 
	}
	
	//for static methods in subclasses to access nativePtr
	protected static final long getNativePtr(JNIObject obj) {
		return obj.getNativePtr();
	}
	
	protected boolean isTaken() {
		return taken;
	}
	
	protected void taken() {
		//Taken should never be called when the object is already taken
		// the getNativePtr method should throw and exception when calling the method before taken is called
		assert(!taken);
		taken = true;
	}

	//for static methods in subclasses to access taken
	protected static final void taken(JNIObject obj) {
		obj.taken();
	}
	
	@Override
	protected void finalize() {
		if (!taken) {
			free();
		}
	}
	
	public void free() {
		
	}
	
	protected static final int toInt(boolean b) {
		return b ? 1 : 0;
	}
	
	protected static void checkParameters(JNIObject...objs) {
		for (JNIObject obj : objs) {
			if (obj.isTaken()) {
				throw new IllegalStateException("This object has already been freed.");
			}
		}
	}
}
