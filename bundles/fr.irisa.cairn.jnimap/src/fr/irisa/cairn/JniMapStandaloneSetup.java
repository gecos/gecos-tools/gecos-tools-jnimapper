
package fr.irisa.cairn;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class JniMapStandaloneSetup extends JniMapStandaloneSetupGenerated{

	public static void doSetup() {
		new JniMapStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

