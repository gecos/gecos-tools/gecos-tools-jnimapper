/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.Mapping#getImports <em>Imports</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Mapping#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Mapping#getPackage <em>Package</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Mapping#getHosts <em>Hosts</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Mapping#getExternalLibraries <em>External Libraries</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Mapping#getLibraries <em>Libraries</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Mapping#getUserModules <em>User Modules</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Mapping#getClassMapping <em>Class Mapping</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Mapping#getIncludes <em>Includes</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Mapping#getMethodGroups <em>Method Groups</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getMapping()
 * @model
 * @generated
 */
public interface Mapping extends EObject
{
  /**
   * Returns the value of the '<em><b>Imports</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.Import}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Imports</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMapping_Imports()
   * @model containment="true"
   * @generated
   */
  EList<Import> getImports();

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMapping_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Mapping#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Package</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Package</em>' attribute.
   * @see #setPackage(String)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMapping_Package()
   * @model
   * @generated
   */
  String getPackage();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Mapping#getPackage <em>Package</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Package</em>' attribute.
   * @see #getPackage()
   * @generated
   */
  void setPackage(String value);

  /**
   * Returns the value of the '<em><b>Hosts</b></em>' attribute list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.HostType}.
   * The literals are from the enumeration {@link fr.irisa.cairn.jniMap.HostType}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Hosts</em>' attribute list.
   * @see fr.irisa.cairn.jniMap.HostType
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMapping_Hosts()
   * @model unique="false"
   * @generated
   */
  EList<HostType> getHosts();

  /**
   * Returns the value of the '<em><b>External Libraries</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.ExternalLibrary}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>External Libraries</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMapping_ExternalLibraries()
   * @model containment="true"
   * @generated
   */
  EList<ExternalLibrary> getExternalLibraries();

  /**
   * Returns the value of the '<em><b>Libraries</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.Library}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Libraries</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMapping_Libraries()
   * @model containment="true"
   * @generated
   */
  EList<Library> getLibraries();

  /**
   * Returns the value of the '<em><b>User Modules</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.UserModule}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>User Modules</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMapping_UserModules()
   * @model containment="true"
   * @generated
   */
  EList<UserModule> getUserModules();

  /**
   * Returns the value of the '<em><b>Class Mapping</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.ClassMapping}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Class Mapping</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMapping_ClassMapping()
   * @model containment="true"
   * @generated
   */
  EList<ClassMapping> getClassMapping();

  /**
   * Returns the value of the '<em><b>Includes</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.IncludeDef}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Includes</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMapping_Includes()
   * @model containment="true"
   * @generated
   */
  EList<IncludeDef> getIncludes();

  /**
   * Returns the value of the '<em><b>Method Groups</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.MethodGroup}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Method Groups</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMapping_MethodGroups()
   * @model containment="true"
   * @generated
   */
  EList<MethodGroup> getMethodGroups();

} // Mapping
