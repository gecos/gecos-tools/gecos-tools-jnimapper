/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Modifiable Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.ModifiableType#getBase <em>Base</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.ModifiableType#getIndir <em>Indir</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getModifiableType()
 * @model
 * @generated
 */
public interface ModifiableType extends Type, NestedType
{
  /**
   * Returns the value of the '<em><b>Base</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Base</em>' containment reference.
   * @see #setBase(BaseType)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getModifiableType_Base()
   * @model containment="true"
   * @generated
   */
  BaseType getBase();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.ModifiableType#getBase <em>Base</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Base</em>' containment reference.
   * @see #getBase()
   * @generated
   */
  void setBase(BaseType value);

  /**
   * Returns the value of the '<em><b>Indir</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Indir</em>' attribute list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getModifiableType_Indir()
   * @model unique="false"
   * @generated
   */
  EList<String> getIndir();

} // ModifiableType
