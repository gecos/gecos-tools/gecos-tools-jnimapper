/**
 */
package fr.irisa.cairn.jniMap;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.EnumType#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getEnumType()
 * @model
 * @generated
 */
public interface EnumType extends ComplexType, BaseType
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' reference.
   * @see #setName(EnumDeclaration)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getEnumType_Name()
   * @model
   * @generated
   */
  EnumDeclaration getName();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.EnumType#getName <em>Name</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' reference.
   * @see #getName()
   * @generated
   */
  void setName(EnumDeclaration value);

} // EnumType
