/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.EnumDeclaration#getValues <em>Values</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getEnumDeclaration()
 * @model
 * @generated
 */
public interface EnumDeclaration extends TypeDeclaration
{
  /**
   * Returns the value of the '<em><b>Values</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.EnumValue}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Values</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getEnumDeclaration_Values()
   * @model containment="true"
   * @generated
   */
  EList<EnumValue> getValues();

} // EnumDeclaration
