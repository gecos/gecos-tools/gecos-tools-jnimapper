/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.MethodGroup#getClass_ <em>Class</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.MethodGroup#getMethods <em>Methods</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethodGroup()
 * @model
 * @generated
 */
public interface MethodGroup extends EObject
{
  /**
   * Returns the value of the '<em><b>Class</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Class</em>' reference.
   * @see #setClass(ClassMapping)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethodGroup_Class()
   * @model
   * @generated
   */
  ClassMapping getClass_();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.MethodGroup#getClass_ <em>Class</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Class</em>' reference.
   * @see #getClass_()
   * @generated
   */
  void setClass(ClassMapping value);

  /**
   * Returns the value of the '<em><b>Methods</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.Method}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Methods</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethodGroup_Methods()
   * @model containment="true"
   * @generated
   */
  EList<Method> getMethods();

} // MethodGroup
