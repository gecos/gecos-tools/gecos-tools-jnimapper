/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generics Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.GenericsDeclaration#getNames <em>Names</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getGenericsDeclaration()
 * @model
 * @generated
 */
public interface GenericsDeclaration extends EObject
{
  /**
   * Returns the value of the '<em><b>Names</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Names</em>' attribute list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getGenericsDeclaration_Names()
   * @model unique="false"
   * @generated
   */
  EList<String> getNames();

} // GenericsDeclaration
