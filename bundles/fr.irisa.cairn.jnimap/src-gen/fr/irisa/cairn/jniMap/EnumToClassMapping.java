/**
 */
package fr.irisa.cairn.jniMap;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum To Class Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.EnumToClassMapping#getEnum_ <em>Enum </em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getEnumToClassMapping()
 * @model
 * @generated
 */
public interface EnumToClassMapping extends ClassMapping
{
  /**
   * Returns the value of the '<em><b>Enum </b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Enum </em>' containment reference.
   * @see #setEnum_(EnumDeclaration)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getEnumToClassMapping_Enum_()
   * @model containment="true"
   * @generated
   */
  EnumDeclaration getEnum_();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.EnumToClassMapping#getEnum_ <em>Enum </em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Enum </em>' containment reference.
   * @see #getEnum_()
   * @generated
   */
  void setEnum_(EnumDeclaration value);

} // EnumToClassMapping
