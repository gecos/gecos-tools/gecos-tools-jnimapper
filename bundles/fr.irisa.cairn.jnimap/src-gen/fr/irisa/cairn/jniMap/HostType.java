/**
 */
package fr.irisa.cairn.jniMap;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Host Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getHostType()
 * @model
 * @generated
 */
public enum HostType implements Enumerator
{
  /**
   * The '<em><b>Linux32</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #LINUX32_VALUE
   * @generated
   * @ordered
   */
  LINUX32(0, "Linux32", "linux_32"),

  /**
   * The '<em><b>Linux64</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #LINUX64_VALUE
   * @generated
   * @ordered
   */
  LINUX64(1, "Linux64", "linux_64"),

  /**
   * The '<em><b>Macosx32</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #MACOSX32_VALUE
   * @generated
   * @ordered
   */
  MACOSX32(2, "Macosx32", "macosx_32"),

  /**
   * The '<em><b>Macosx64</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #MACOSX64_VALUE
   * @generated
   * @ordered
   */
  MACOSX64(3, "Macosx64", "macosx_64"),

  /**
   * The '<em><b>Mingw32</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #MINGW32_VALUE
   * @generated
   * @ordered
   */
  MINGW32(4, "Mingw32", "mingw_32"),

  /**
   * The '<em><b>Cygwin32</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CYGWIN32_VALUE
   * @generated
   * @ordered
   */
  CYGWIN32(5, "Cygwin32", "cygwin_32");

  /**
   * The '<em><b>Linux32</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #LINUX32
   * @model name="Linux32" literal="linux_32"
   * @generated
   * @ordered
   */
  public static final int LINUX32_VALUE = 0;

  /**
   * The '<em><b>Linux64</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #LINUX64
   * @model name="Linux64" literal="linux_64"
   * @generated
   * @ordered
   */
  public static final int LINUX64_VALUE = 1;

  /**
   * The '<em><b>Macosx32</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #MACOSX32
   * @model name="Macosx32" literal="macosx_32"
   * @generated
   * @ordered
   */
  public static final int MACOSX32_VALUE = 2;

  /**
   * The '<em><b>Macosx64</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #MACOSX64
   * @model name="Macosx64" literal="macosx_64"
   * @generated
   * @ordered
   */
  public static final int MACOSX64_VALUE = 3;

  /**
   * The '<em><b>Mingw32</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #MINGW32
   * @model name="Mingw32" literal="mingw_32"
   * @generated
   * @ordered
   */
  public static final int MINGW32_VALUE = 4;

  /**
   * The '<em><b>Cygwin32</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CYGWIN32
   * @model name="Cygwin32" literal="cygwin_32"
   * @generated
   * @ordered
   */
  public static final int CYGWIN32_VALUE = 5;

  /**
   * An array of all the '<em><b>Host Type</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final HostType[] VALUES_ARRAY =
    new HostType[]
    {
      LINUX32,
      LINUX64,
      MACOSX32,
      MACOSX64,
      MINGW32,
      CYGWIN32,
    };

  /**
   * A public read-only list of all the '<em><b>Host Type</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<HostType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>Host Type</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param literal the literal.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static HostType get(String literal)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      HostType result = VALUES_ARRAY[i];
      if (result.toString().equals(literal))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>Host Type</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param name the name.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static HostType getByName(String name)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      HostType result = VALUES_ARRAY[i];
      if (result.getName().equals(name))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>Host Type</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the integer value.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static HostType get(int value)
  {
    switch (value)
    {
      case LINUX32_VALUE: return LINUX32;
      case LINUX64_VALUE: return LINUX64;
      case MACOSX32_VALUE: return MACOSX32;
      case MACOSX64_VALUE: return MACOSX64;
      case MINGW32_VALUE: return MINGW32;
      case CYGWIN32_VALUE: return CYGWIN32;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private HostType(int value, String name, String literal)
  {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral()
  {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    return literal;
  }
  
} //HostType
