/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.InterfaceImplementation#getInterface <em>Interface</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.InterfaceImplementation#getGenericsInstantiation <em>Generics Instantiation</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getInterfaceImplementation()
 * @model
 * @generated
 */
public interface InterfaceImplementation extends EObject
{
  /**
   * Returns the value of the '<em><b>Interface</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Interface</em>' reference.
   * @see #setInterface(InterfaceClass)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getInterfaceImplementation_Interface()
   * @model
   * @generated
   */
  InterfaceClass getInterface();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.InterfaceImplementation#getInterface <em>Interface</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Interface</em>' reference.
   * @see #getInterface()
   * @generated
   */
  void setInterface(InterfaceClass value);

  /**
   * Returns the value of the '<em><b>Generics Instantiation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Generics Instantiation</em>' containment reference.
   * @see #setGenericsInstantiation(GenericsInstantiation)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getInterfaceImplementation_GenericsInstantiation()
   * @model containment="true"
   * @generated
   */
  GenericsInstantiation getGenericsInstantiation();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.InterfaceImplementation#getGenericsInstantiation <em>Generics Instantiation</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Generics Instantiation</em>' containment reference.
   * @see #getGenericsInstantiation()
   * @generated
   */
  void setGenericsInstantiation(GenericsInstantiation value);

} // InterfaceImplementation
