/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getComplexType()
 * @model
 * @generated
 */
public interface ComplexType extends EObject
{
} // ComplexType
