/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Param Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.ParamDef#isGive <em>Give</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.ParamDef#isTake <em>Take</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.ParamDef#isKeep <em>Keep</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.ParamDef#getType <em>Type</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.ParamDef#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getParamDef()
 * @model
 * @generated
 */
public interface ParamDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Give</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Give</em>' attribute.
   * @see #setGive(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getParamDef_Give()
   * @model
   * @generated
   */
  boolean isGive();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.ParamDef#isGive <em>Give</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Give</em>' attribute.
   * @see #isGive()
   * @generated
   */
  void setGive(boolean value);

  /**
   * Returns the value of the '<em><b>Take</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Take</em>' attribute.
   * @see #setTake(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getParamDef_Take()
   * @model
   * @generated
   */
  boolean isTake();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.ParamDef#isTake <em>Take</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Take</em>' attribute.
   * @see #isTake()
   * @generated
   */
  void setTake(boolean value);

  /**
   * Returns the value of the '<em><b>Keep</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Keep</em>' attribute.
   * @see #setKeep(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getParamDef_Keep()
   * @model
   * @generated
   */
  boolean isKeep();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.ParamDef#isKeep <em>Keep</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Keep</em>' attribute.
   * @see #isKeep()
   * @generated
   */
  void setKeep(boolean value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getParamDef_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.ParamDef#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getParamDef_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.ParamDef#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

} // ParamDef
