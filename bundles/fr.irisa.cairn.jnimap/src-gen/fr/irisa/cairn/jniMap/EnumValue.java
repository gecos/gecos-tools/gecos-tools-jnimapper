/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.EnumValue#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.EnumValue#getValue <em>Value</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.EnumValue#getEnumNext <em>Enum Next</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getEnumValue()
 * @model
 * @generated
 */
public interface EnumValue extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getEnumValue_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.EnumValue#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(int)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getEnumValue_Value()
   * @model
   * @generated
   */
  int getValue();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.EnumValue#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(int value);

  /**
   * Returns the value of the '<em><b>Enum Next</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Enum Next</em>' containment reference.
   * @see #setEnumNext(EnumValue)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getEnumValue_EnumNext()
   * @model containment="true"
   * @generated
   */
  EnumValue getEnumNext();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.EnumValue#getEnumNext <em>Enum Next</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Enum Next</em>' containment reference.
   * @see #getEnumNext()
   * @generated
   */
  void setEnumNext(EnumValue value);

} // EnumValue
