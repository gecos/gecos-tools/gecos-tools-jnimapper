/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Library File Name</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.LibraryFileName#getOs <em>Os</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.LibraryFileName#getFilename <em>Filename</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getLibraryFileName()
 * @model
 * @generated
 */
public interface LibraryFileName extends EObject
{
  /**
   * Returns the value of the '<em><b>Os</b></em>' attribute.
   * The literals are from the enumeration {@link fr.irisa.cairn.jniMap.HostType}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Os</em>' attribute.
   * @see fr.irisa.cairn.jniMap.HostType
   * @see #setOs(HostType)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getLibraryFileName_Os()
   * @model
   * @generated
   */
  HostType getOs();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.LibraryFileName#getOs <em>Os</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Os</em>' attribute.
   * @see fr.irisa.cairn.jniMap.HostType
   * @see #getOs()
   * @generated
   */
  void setOs(HostType value);

  /**
   * Returns the value of the '<em><b>Filename</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Filename</em>' attribute.
   * @see #setFilename(String)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getLibraryFileName_Filename()
   * @model
   * @generated
   */
  String getFilename();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.LibraryFileName#getFilename <em>Filename</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Filename</em>' attribute.
   * @see #getFilename()
   * @generated
   */
  void setFilename(String value);

} // LibraryFileName
