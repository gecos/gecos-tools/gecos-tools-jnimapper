/**
 */
package fr.irisa.cairn.jniMap;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Struct To Class Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.StructToClassMapping#getStruct <em>Struct</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.StructToClassMapping#getSuper <em>Super</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getStructToClassMapping()
 * @model
 * @generated
 */
public interface StructToClassMapping extends ClassMapping
{
  /**
   * Returns the value of the '<em><b>Struct</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Struct</em>' containment reference.
   * @see #setStruct(StructDeclaration)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getStructToClassMapping_Struct()
   * @model containment="true"
   * @generated
   */
  StructDeclaration getStruct();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.StructToClassMapping#getStruct <em>Struct</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Struct</em>' containment reference.
   * @see #getStruct()
   * @generated
   */
  void setStruct(StructDeclaration value);

  /**
   * Returns the value of the '<em><b>Super</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Super</em>' reference.
   * @see #setSuper(ClassMapping)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getStructToClassMapping_Super()
   * @model
   * @generated
   */
  ClassMapping getSuper();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.StructToClassMapping#getSuper <em>Super</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Super</em>' reference.
   * @see #getSuper()
   * @generated
   */
  void setSuper(ClassMapping value);

} // StructToClassMapping
