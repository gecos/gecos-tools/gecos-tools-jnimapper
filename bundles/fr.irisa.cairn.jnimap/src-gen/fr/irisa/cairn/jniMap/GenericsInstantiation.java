/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generics Instantiation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.GenericsInstantiation#getInstanceNames <em>Instance Names</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getGenericsInstantiation()
 * @model
 * @generated
 */
public interface GenericsInstantiation extends EObject
{
  /**
   * Returns the value of the '<em><b>Instance Names</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Instance Names</em>' attribute list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getGenericsInstantiation_InstanceNames()
   * @model unique="false"
   * @generated
   */
  EList<String> getInstanceNames();

} // GenericsInstantiation
