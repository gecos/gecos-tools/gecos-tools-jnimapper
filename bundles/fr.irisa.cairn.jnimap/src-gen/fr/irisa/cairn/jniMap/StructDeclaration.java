/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Struct Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.StructDeclaration#getFields <em>Fields</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getStructDeclaration()
 * @model
 * @generated
 */
public interface StructDeclaration extends TypeDeclaration
{
  /**
   * Returns the value of the '<em><b>Fields</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.FieldDef}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fields</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getStructDeclaration_Fields()
   * @model containment="true"
   * @generated
   */
  EList<FieldDef> getFields();

} // StructDeclaration
