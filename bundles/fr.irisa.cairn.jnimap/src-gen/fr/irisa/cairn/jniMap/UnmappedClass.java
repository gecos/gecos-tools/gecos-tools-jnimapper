/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unmapped Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.UnmappedClass#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.UnmappedClass#getGenericsDecl <em>Generics Decl</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.UnmappedClass#getSuper <em>Super</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.UnmappedClass#getGenericsInstantiation <em>Generics Instantiation</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.UnmappedClass#getInterfaces <em>Interfaces</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getUnmappedClass()
 * @model
 * @generated
 */
public interface UnmappedClass extends ClassMapping
{
  /**
   * Returns the value of the '<em><b>Abstract</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Abstract</em>' attribute.
   * @see #setAbstract(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getUnmappedClass_Abstract()
   * @model
   * @generated
   */
  boolean isAbstract();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.UnmappedClass#isAbstract <em>Abstract</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Abstract</em>' attribute.
   * @see #isAbstract()
   * @generated
   */
  void setAbstract(boolean value);

  /**
   * Returns the value of the '<em><b>Generics Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Generics Decl</em>' containment reference.
   * @see #setGenericsDecl(GenericsDeclaration)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getUnmappedClass_GenericsDecl()
   * @model containment="true"
   * @generated
   */
  GenericsDeclaration getGenericsDecl();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.UnmappedClass#getGenericsDecl <em>Generics Decl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Generics Decl</em>' containment reference.
   * @see #getGenericsDecl()
   * @generated
   */
  void setGenericsDecl(GenericsDeclaration value);

  /**
   * Returns the value of the '<em><b>Super</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Super</em>' reference.
   * @see #setSuper(ClassMapping)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getUnmappedClass_Super()
   * @model
   * @generated
   */
  ClassMapping getSuper();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.UnmappedClass#getSuper <em>Super</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Super</em>' reference.
   * @see #getSuper()
   * @generated
   */
  void setSuper(ClassMapping value);

  /**
   * Returns the value of the '<em><b>Generics Instantiation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Generics Instantiation</em>' containment reference.
   * @see #setGenericsInstantiation(GenericsInstantiation)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getUnmappedClass_GenericsInstantiation()
   * @model containment="true"
   * @generated
   */
  GenericsInstantiation getGenericsInstantiation();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.UnmappedClass#getGenericsInstantiation <em>Generics Instantiation</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Generics Instantiation</em>' containment reference.
   * @see #getGenericsInstantiation()
   * @generated
   */
  void setGenericsInstantiation(GenericsInstantiation value);

  /**
   * Returns the value of the '<em><b>Interfaces</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.InterfaceImplementation}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Interfaces</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getUnmappedClass_Interfaces()
   * @model containment="true"
   * @generated
   */
  EList<InterfaceImplementation> getInterfaces();

} // UnmappedClass
