/**
 */
package fr.irisa.cairn.jniMap;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Built In Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.BuiltInType#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getBuiltInType()
 * @model
 * @generated
 */
public interface BuiltInType extends BaseType
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getBuiltInType_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.BuiltInType#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

} // BuiltInType
