/**
 */
package fr.irisa.cairn.jniMap;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Struct Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.StructType#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getStructType()
 * @model
 * @generated
 */
public interface StructType extends ComplexType, BaseType
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' reference.
   * @see #setName(StructDeclaration)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getStructType_Name()
   * @model
   * @generated
   */
  StructDeclaration getName();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.StructType#getName <em>Name</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' reference.
   * @see #getName()
   * @generated
   */
  void setName(StructDeclaration value);

} // StructType
