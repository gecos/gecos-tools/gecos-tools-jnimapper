/**
 */
package fr.irisa.cairn.jniMap;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class To Class Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.ClassToClassMapping#getClass_ <em>Class</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.ClassToClassMapping#getSuper <em>Super</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getClassToClassMapping()
 * @model
 * @generated
 */
public interface ClassToClassMapping extends ClassMapping
{
  /**
   * Returns the value of the '<em><b>Class</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Class</em>' containment reference.
   * @see #setClass(ClassDeclaration)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getClassToClassMapping_Class()
   * @model containment="true"
   * @generated
   */
  ClassDeclaration getClass_();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.ClassToClassMapping#getClass_ <em>Class</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Class</em>' containment reference.
   * @see #getClass_()
   * @generated
   */
  void setClass(ClassDeclaration value);

  /**
   * Returns the value of the '<em><b>Super</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Super</em>' reference.
   * @see #setSuper(ClassMapping)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getClassToClassMapping_Super()
   * @model
   * @generated
   */
  ClassMapping getSuper();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.ClassToClassMapping#getSuper <em>Super</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Super</em>' reference.
   * @see #getSuper()
   * @generated
   */
  void setSuper(ClassMapping value);

} // ClassToClassMapping
