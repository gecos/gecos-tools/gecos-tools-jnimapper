/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nested Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getNestedType()
 * @model
 * @generated
 */
public interface NestedType extends EObject
{
} // NestedType
