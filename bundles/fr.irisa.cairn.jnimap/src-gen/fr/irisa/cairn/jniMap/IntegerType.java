/**
 */
package fr.irisa.cairn.jniMap;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getIntegerType()
 * @model
 * @generated
 */
public interface IntegerType extends BuiltInType
{
} // IntegerType
