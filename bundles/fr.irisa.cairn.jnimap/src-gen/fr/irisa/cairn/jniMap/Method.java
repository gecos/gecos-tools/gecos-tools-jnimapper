/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#isConstructor <em>Constructor</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#isDestructor <em>Destructor</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#isInstanceof <em>Instanceof</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#isStatic <em>Static</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#isPrivate <em>Private</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#isProtected <em>Protected</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#isRenamed <em>Renamed</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#getNewname <em>Newname</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#isStub <em>Stub</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#getArg <em>Arg</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#getRes <em>Res</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.Method#getParams <em>Params</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod()
 * @model
 * @generated
 */
public interface Method extends EObject
{
  /**
   * Returns the value of the '<em><b>Constructor</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Constructor</em>' attribute.
   * @see #setConstructor(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Constructor()
   * @model
   * @generated
   */
  boolean isConstructor();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Method#isConstructor <em>Constructor</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Constructor</em>' attribute.
   * @see #isConstructor()
   * @generated
   */
  void setConstructor(boolean value);

  /**
   * Returns the value of the '<em><b>Destructor</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Destructor</em>' attribute.
   * @see #setDestructor(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Destructor()
   * @model
   * @generated
   */
  boolean isDestructor();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Method#isDestructor <em>Destructor</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Destructor</em>' attribute.
   * @see #isDestructor()
   * @generated
   */
  void setDestructor(boolean value);

  /**
   * Returns the value of the '<em><b>Instanceof</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Instanceof</em>' attribute.
   * @see #setInstanceof(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Instanceof()
   * @model
   * @generated
   */
  boolean isInstanceof();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Method#isInstanceof <em>Instanceof</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Instanceof</em>' attribute.
   * @see #isInstanceof()
   * @generated
   */
  void setInstanceof(boolean value);

  /**
   * Returns the value of the '<em><b>Static</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Static</em>' attribute.
   * @see #setStatic(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Static()
   * @model
   * @generated
   */
  boolean isStatic();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Method#isStatic <em>Static</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Static</em>' attribute.
   * @see #isStatic()
   * @generated
   */
  void setStatic(boolean value);

  /**
   * Returns the value of the '<em><b>Private</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Private</em>' attribute.
   * @see #setPrivate(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Private()
   * @model
   * @generated
   */
  boolean isPrivate();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Method#isPrivate <em>Private</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Private</em>' attribute.
   * @see #isPrivate()
   * @generated
   */
  void setPrivate(boolean value);

  /**
   * Returns the value of the '<em><b>Protected</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Protected</em>' attribute.
   * @see #setProtected(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Protected()
   * @model
   * @generated
   */
  boolean isProtected();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Method#isProtected <em>Protected</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Protected</em>' attribute.
   * @see #isProtected()
   * @generated
   */
  void setProtected(boolean value);

  /**
   * Returns the value of the '<em><b>Renamed</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Renamed</em>' attribute.
   * @see #setRenamed(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Renamed()
   * @model
   * @generated
   */
  boolean isRenamed();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Method#isRenamed <em>Renamed</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Renamed</em>' attribute.
   * @see #isRenamed()
   * @generated
   */
  void setRenamed(boolean value);

  /**
   * Returns the value of the '<em><b>Newname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Newname</em>' attribute.
   * @see #setNewname(String)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Newname()
   * @model
   * @generated
   */
  String getNewname();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Method#getNewname <em>Newname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Newname</em>' attribute.
   * @see #getNewname()
   * @generated
   */
  void setNewname(String value);

  /**
   * Returns the value of the '<em><b>Stub</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Stub</em>' attribute.
   * @see #setStub(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Stub()
   * @model
   * @generated
   */
  boolean isStub();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Method#isStub <em>Stub</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Stub</em>' attribute.
   * @see #isStub()
   * @generated
   */
  void setStub(boolean value);

  /**
   * Returns the value of the '<em><b>Arg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arg</em>' attribute.
   * @see #setArg(int)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Arg()
   * @model
   * @generated
   */
  int getArg();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Method#getArg <em>Arg</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Arg</em>' attribute.
   * @see #getArg()
   * @generated
   */
  void setArg(int value);

  /**
   * Returns the value of the '<em><b>Res</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Res</em>' containment reference.
   * @see #setRes(Type)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Res()
   * @model containment="true"
   * @generated
   */
  Type getRes();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Method#getRes <em>Res</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Res</em>' containment reference.
   * @see #getRes()
   * @generated
   */
  void setRes(Type value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.Method#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Params</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.ParamDef}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Params</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getMethod_Params()
   * @model containment="true"
   * @generated
   */
  EList<ParamDef> getParams();

} // Method
