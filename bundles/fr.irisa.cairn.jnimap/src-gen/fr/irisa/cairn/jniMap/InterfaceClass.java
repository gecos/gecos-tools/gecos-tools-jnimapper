/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.InterfaceClass#getGenericsDecl <em>Generics Decl</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.InterfaceClass#getInterfaces <em>Interfaces</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getInterfaceClass()
 * @model
 * @generated
 */
public interface InterfaceClass extends ClassMapping
{
  /**
   * Returns the value of the '<em><b>Generics Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Generics Decl</em>' containment reference.
   * @see #setGenericsDecl(GenericsDeclaration)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getInterfaceClass_GenericsDecl()
   * @model containment="true"
   * @generated
   */
  GenericsDeclaration getGenericsDecl();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.InterfaceClass#getGenericsDecl <em>Generics Decl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Generics Decl</em>' containment reference.
   * @see #getGenericsDecl()
   * @generated
   */
  void setGenericsDecl(GenericsDeclaration value);

  /**
   * Returns the value of the '<em><b>Interfaces</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.InterfaceImplementation}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Interfaces</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getInterfaceClass_Interfaces()
   * @model containment="true"
   * @generated
   */
  EList<InterfaceImplementation> getInterfaces();

} // InterfaceClass
