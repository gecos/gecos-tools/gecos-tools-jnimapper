/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Include Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.IncludeDef#getInclude <em>Include</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getIncludeDef()
 * @model
 * @generated
 */
public interface IncludeDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Include</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Include</em>' attribute.
   * @see #setInclude(String)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getIncludeDef_Include()
   * @model
   * @generated
   */
  String getInclude();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.IncludeDef#getInclude <em>Include</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Include</em>' attribute.
   * @see #getInclude()
   * @generated
   */
  void setInclude(String value);

} // IncludeDef
