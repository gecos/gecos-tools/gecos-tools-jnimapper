/**
 */
package fr.irisa.cairn.jniMap.util;

import fr.irisa.cairn.jniMap.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.jniMap.JniMapPackage
 * @generated
 */
public class JniMapAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static JniMapPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JniMapAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = JniMapPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected JniMapSwitch<Adapter> modelSwitch =
    new JniMapSwitch<Adapter>()
    {
      @Override
      public Adapter caseMapping(Mapping object)
      {
        return createMappingAdapter();
      }
      @Override
      public Adapter caseImport(Import object)
      {
        return createImportAdapter();
      }
      @Override
      public Adapter caseClassMapping(ClassMapping object)
      {
        return createClassMappingAdapter();
      }
      @Override
      public Adapter caseGenericsDeclaration(GenericsDeclaration object)
      {
        return createGenericsDeclarationAdapter();
      }
      @Override
      public Adapter caseGenericsInstantiation(GenericsInstantiation object)
      {
        return createGenericsInstantiationAdapter();
      }
      @Override
      public Adapter caseInterfaceImplementation(InterfaceImplementation object)
      {
        return createInterfaceImplementationAdapter();
      }
      @Override
      public Adapter caseEnumToClassMapping(EnumToClassMapping object)
      {
        return createEnumToClassMappingAdapter();
      }
      @Override
      public Adapter caseStructToClassMapping(StructToClassMapping object)
      {
        return createStructToClassMappingAdapter();
      }
      @Override
      public Adapter caseClassToClassMapping(ClassToClassMapping object)
      {
        return createClassToClassMappingAdapter();
      }
      @Override
      public Adapter caseAliasToClassMapping(AliasToClassMapping object)
      {
        return createAliasToClassMappingAdapter();
      }
      @Override
      public Adapter caseUnmappedClass(UnmappedClass object)
      {
        return createUnmappedClassAdapter();
      }
      @Override
      public Adapter caseInterfaceClass(InterfaceClass object)
      {
        return createInterfaceClassAdapter();
      }
      @Override
      public Adapter caseExternalLibrary(ExternalLibrary object)
      {
        return createExternalLibraryAdapter();
      }
      @Override
      public Adapter caseLibrary(Library object)
      {
        return createLibraryAdapter();
      }
      @Override
      public Adapter caseLibraryFileName(LibraryFileName object)
      {
        return createLibraryFileNameAdapter();
      }
      @Override
      public Adapter caseIncludeDef(IncludeDef object)
      {
        return createIncludeDefAdapter();
      }
      @Override
      public Adapter caseUserModule(UserModule object)
      {
        return createUserModuleAdapter();
      }
      @Override
      public Adapter caseCMethod(CMethod object)
      {
        return createCMethodAdapter();
      }
      @Override
      public Adapter caseTypeDeclaration(TypeDeclaration object)
      {
        return createTypeDeclarationAdapter();
      }
      @Override
      public Adapter caseTypeAliasDeclaration(TypeAliasDeclaration object)
      {
        return createTypeAliasDeclarationAdapter();
      }
      @Override
      public Adapter caseStructDeclaration(StructDeclaration object)
      {
        return createStructDeclarationAdapter();
      }
      @Override
      public Adapter caseClassDeclaration(ClassDeclaration object)
      {
        return createClassDeclarationAdapter();
      }
      @Override
      public Adapter caseEnumDeclaration(EnumDeclaration object)
      {
        return createEnumDeclarationAdapter();
      }
      @Override
      public Adapter caseEnumType(EnumType object)
      {
        return createEnumTypeAdapter();
      }
      @Override
      public Adapter caseEnumValue(EnumValue object)
      {
        return createEnumValueAdapter();
      }
      @Override
      public Adapter caseParamDef(ParamDef object)
      {
        return createParamDefAdapter();
      }
      @Override
      public Adapter caseMethodGroup(MethodGroup object)
      {
        return createMethodGroupAdapter();
      }
      @Override
      public Adapter caseMethod(Method object)
      {
        return createMethodAdapter();
      }
      @Override
      public Adapter caseType(Type object)
      {
        return createTypeAdapter();
      }
      @Override
      public Adapter caseBuiltInType(BuiltInType object)
      {
        return createBuiltInTypeAdapter();
      }
      @Override
      public Adapter caseBooleanType(BooleanType object)
      {
        return createBooleanTypeAdapter();
      }
      @Override
      public Adapter caseIntegerType(IntegerType object)
      {
        return createIntegerTypeAdapter();
      }
      @Override
      public Adapter caseRealType(RealType object)
      {
        return createRealTypeAdapter();
      }
      @Override
      public Adapter caseStringType(StringType object)
      {
        return createStringTypeAdapter();
      }
      @Override
      public Adapter caseVoidType(VoidType object)
      {
        return createVoidTypeAdapter();
      }
      @Override
      public Adapter caseNestedType(NestedType object)
      {
        return createNestedTypeAdapter();
      }
      @Override
      public Adapter caseComplexType(ComplexType object)
      {
        return createComplexTypeAdapter();
      }
      @Override
      public Adapter caseTypeAlias(TypeAlias object)
      {
        return createTypeAliasAdapter();
      }
      @Override
      public Adapter caseArrayType(ArrayType object)
      {
        return createArrayTypeAdapter();
      }
      @Override
      public Adapter casePtrType(PtrType object)
      {
        return createPtrTypeAdapter();
      }
      @Override
      public Adapter caseModifiableType(ModifiableType object)
      {
        return createModifiableTypeAdapter();
      }
      @Override
      public Adapter caseStructType(StructType object)
      {
        return createStructTypeAdapter();
      }
      @Override
      public Adapter caseFieldDef(FieldDef object)
      {
        return createFieldDefAdapter();
      }
      @Override
      public Adapter caseConstType(ConstType object)
      {
        return createConstTypeAdapter();
      }
      @Override
      public Adapter caseBaseType(BaseType object)
      {
        return createBaseTypeAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.Mapping <em>Mapping</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.Mapping
   * @generated
   */
  public Adapter createMappingAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.Import <em>Import</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.Import
   * @generated
   */
  public Adapter createImportAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.ClassMapping <em>Class Mapping</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.ClassMapping
   * @generated
   */
  public Adapter createClassMappingAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.GenericsDeclaration <em>Generics Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.GenericsDeclaration
   * @generated
   */
  public Adapter createGenericsDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.GenericsInstantiation <em>Generics Instantiation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.GenericsInstantiation
   * @generated
   */
  public Adapter createGenericsInstantiationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.InterfaceImplementation <em>Interface Implementation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.InterfaceImplementation
   * @generated
   */
  public Adapter createInterfaceImplementationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.EnumToClassMapping <em>Enum To Class Mapping</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.EnumToClassMapping
   * @generated
   */
  public Adapter createEnumToClassMappingAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.StructToClassMapping <em>Struct To Class Mapping</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.StructToClassMapping
   * @generated
   */
  public Adapter createStructToClassMappingAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.ClassToClassMapping <em>Class To Class Mapping</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.ClassToClassMapping
   * @generated
   */
  public Adapter createClassToClassMappingAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.AliasToClassMapping <em>Alias To Class Mapping</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.AliasToClassMapping
   * @generated
   */
  public Adapter createAliasToClassMappingAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.UnmappedClass <em>Unmapped Class</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.UnmappedClass
   * @generated
   */
  public Adapter createUnmappedClassAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.InterfaceClass <em>Interface Class</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.InterfaceClass
   * @generated
   */
  public Adapter createInterfaceClassAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.ExternalLibrary <em>External Library</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.ExternalLibrary
   * @generated
   */
  public Adapter createExternalLibraryAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.Library <em>Library</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.Library
   * @generated
   */
  public Adapter createLibraryAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.LibraryFileName <em>Library File Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.LibraryFileName
   * @generated
   */
  public Adapter createLibraryFileNameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.IncludeDef <em>Include Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.IncludeDef
   * @generated
   */
  public Adapter createIncludeDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.UserModule <em>User Module</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.UserModule
   * @generated
   */
  public Adapter createUserModuleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.CMethod <em>CMethod</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.CMethod
   * @generated
   */
  public Adapter createCMethodAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.TypeDeclaration <em>Type Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.TypeDeclaration
   * @generated
   */
  public Adapter createTypeDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.TypeAliasDeclaration <em>Type Alias Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.TypeAliasDeclaration
   * @generated
   */
  public Adapter createTypeAliasDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.StructDeclaration <em>Struct Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.StructDeclaration
   * @generated
   */
  public Adapter createStructDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.ClassDeclaration <em>Class Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.ClassDeclaration
   * @generated
   */
  public Adapter createClassDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.EnumDeclaration <em>Enum Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.EnumDeclaration
   * @generated
   */
  public Adapter createEnumDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.EnumType <em>Enum Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.EnumType
   * @generated
   */
  public Adapter createEnumTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.EnumValue <em>Enum Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.EnumValue
   * @generated
   */
  public Adapter createEnumValueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.ParamDef <em>Param Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.ParamDef
   * @generated
   */
  public Adapter createParamDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.MethodGroup <em>Method Group</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.MethodGroup
   * @generated
   */
  public Adapter createMethodGroupAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.Method <em>Method</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.Method
   * @generated
   */
  public Adapter createMethodAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.Type
   * @generated
   */
  public Adapter createTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.BuiltInType <em>Built In Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.BuiltInType
   * @generated
   */
  public Adapter createBuiltInTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.BooleanType <em>Boolean Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.BooleanType
   * @generated
   */
  public Adapter createBooleanTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.IntegerType <em>Integer Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.IntegerType
   * @generated
   */
  public Adapter createIntegerTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.RealType <em>Real Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.RealType
   * @generated
   */
  public Adapter createRealTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.StringType <em>String Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.StringType
   * @generated
   */
  public Adapter createStringTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.VoidType <em>Void Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.VoidType
   * @generated
   */
  public Adapter createVoidTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.NestedType <em>Nested Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.NestedType
   * @generated
   */
  public Adapter createNestedTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.ComplexType <em>Complex Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.ComplexType
   * @generated
   */
  public Adapter createComplexTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.TypeAlias <em>Type Alias</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.TypeAlias
   * @generated
   */
  public Adapter createTypeAliasAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.ArrayType <em>Array Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.ArrayType
   * @generated
   */
  public Adapter createArrayTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.PtrType <em>Ptr Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.PtrType
   * @generated
   */
  public Adapter createPtrTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.ModifiableType <em>Modifiable Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.ModifiableType
   * @generated
   */
  public Adapter createModifiableTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.StructType <em>Struct Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.StructType
   * @generated
   */
  public Adapter createStructTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.FieldDef <em>Field Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.FieldDef
   * @generated
   */
  public Adapter createFieldDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.ConstType <em>Const Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.ConstType
   * @generated
   */
  public Adapter createConstTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.jniMap.BaseType <em>Base Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.jniMap.BaseType
   * @generated
   */
  public Adapter createBaseTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //JniMapAdapterFactory
