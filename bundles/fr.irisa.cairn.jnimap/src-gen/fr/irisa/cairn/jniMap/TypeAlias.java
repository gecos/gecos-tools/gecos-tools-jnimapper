/**
 */
package fr.irisa.cairn.jniMap;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Alias</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.TypeAlias#getAlias <em>Alias</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getTypeAlias()
 * @model
 * @generated
 */
public interface TypeAlias extends ComplexType, BaseType
{
  /**
   * Returns the value of the '<em><b>Alias</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Alias</em>' reference.
   * @see #setAlias(TypeAliasDeclaration)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getTypeAlias_Alias()
   * @model
   * @generated
   */
  TypeAliasDeclaration getAlias();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.TypeAlias#getAlias <em>Alias</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Alias</em>' reference.
   * @see #getAlias()
   * @generated
   */
  void setAlias(TypeAliasDeclaration value);

} // TypeAlias
