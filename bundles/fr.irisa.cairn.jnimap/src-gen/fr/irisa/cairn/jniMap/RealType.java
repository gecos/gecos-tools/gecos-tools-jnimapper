/**
 */
package fr.irisa.cairn.jniMap;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Real Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getRealType()
 * @model
 * @generated
 */
public interface RealType extends BuiltInType
{
} // RealType
