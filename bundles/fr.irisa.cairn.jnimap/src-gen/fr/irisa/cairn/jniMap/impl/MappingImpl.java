/**
 */
package fr.irisa.cairn.jniMap.impl;

import fr.irisa.cairn.jniMap.ClassMapping;
import fr.irisa.cairn.jniMap.ExternalLibrary;
import fr.irisa.cairn.jniMap.HostType;
import fr.irisa.cairn.jniMap.Import;
import fr.irisa.cairn.jniMap.IncludeDef;
import fr.irisa.cairn.jniMap.JniMapPackage;
import fr.irisa.cairn.jniMap.Library;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.MethodGroup;
import fr.irisa.cairn.jniMap.UserModule;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MappingImpl#getImports <em>Imports</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MappingImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MappingImpl#getPackage <em>Package</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MappingImpl#getHosts <em>Hosts</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MappingImpl#getExternalLibraries <em>External Libraries</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MappingImpl#getLibraries <em>Libraries</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MappingImpl#getUserModules <em>User Modules</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MappingImpl#getClassMapping <em>Class Mapping</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MappingImpl#getIncludes <em>Includes</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MappingImpl#getMethodGroups <em>Method Groups</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MappingImpl extends MinimalEObjectImpl.Container implements Mapping
{
  /**
   * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getImports()
   * @generated
   * @ordered
   */
  protected EList<Import> imports;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getPackage() <em>Package</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPackage()
   * @generated
   * @ordered
   */
  protected static final String PACKAGE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPackage() <em>Package</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPackage()
   * @generated
   * @ordered
   */
  protected String package_ = PACKAGE_EDEFAULT;

  /**
   * The cached value of the '{@link #getHosts() <em>Hosts</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHosts()
   * @generated
   * @ordered
   */
  protected EList<HostType> hosts;

  /**
   * The cached value of the '{@link #getExternalLibraries() <em>External Libraries</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExternalLibraries()
   * @generated
   * @ordered
   */
  protected EList<ExternalLibrary> externalLibraries;

  /**
   * The cached value of the '{@link #getLibraries() <em>Libraries</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLibraries()
   * @generated
   * @ordered
   */
  protected EList<Library> libraries;

  /**
   * The cached value of the '{@link #getUserModules() <em>User Modules</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUserModules()
   * @generated
   * @ordered
   */
  protected EList<UserModule> userModules;

  /**
   * The cached value of the '{@link #getClassMapping() <em>Class Mapping</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClassMapping()
   * @generated
   * @ordered
   */
  protected EList<ClassMapping> classMapping;

  /**
   * The cached value of the '{@link #getIncludes() <em>Includes</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIncludes()
   * @generated
   * @ordered
   */
  protected EList<IncludeDef> includes;

  /**
   * The cached value of the '{@link #getMethodGroups() <em>Method Groups</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMethodGroups()
   * @generated
   * @ordered
   */
  protected EList<MethodGroup> methodGroups;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MappingImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JniMapPackage.Literals.MAPPING;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Import> getImports()
  {
    if (imports == null)
    {
      imports = new EObjectContainmentEList<Import>(Import.class, this, JniMapPackage.MAPPING__IMPORTS);
    }
    return imports;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.MAPPING__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPackage()
  {
    return package_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPackage(String newPackage)
  {
    String oldPackage = package_;
    package_ = newPackage;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.MAPPING__PACKAGE, oldPackage, package_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<HostType> getHosts()
  {
    if (hosts == null)
    {
      hosts = new EDataTypeEList<HostType>(HostType.class, this, JniMapPackage.MAPPING__HOSTS);
    }
    return hosts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ExternalLibrary> getExternalLibraries()
  {
    if (externalLibraries == null)
    {
      externalLibraries = new EObjectContainmentEList<ExternalLibrary>(ExternalLibrary.class, this, JniMapPackage.MAPPING__EXTERNAL_LIBRARIES);
    }
    return externalLibraries;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Library> getLibraries()
  {
    if (libraries == null)
    {
      libraries = new EObjectContainmentEList<Library>(Library.class, this, JniMapPackage.MAPPING__LIBRARIES);
    }
    return libraries;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<UserModule> getUserModules()
  {
    if (userModules == null)
    {
      userModules = new EObjectContainmentEList<UserModule>(UserModule.class, this, JniMapPackage.MAPPING__USER_MODULES);
    }
    return userModules;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ClassMapping> getClassMapping()
  {
    if (classMapping == null)
    {
      classMapping = new EObjectContainmentEList<ClassMapping>(ClassMapping.class, this, JniMapPackage.MAPPING__CLASS_MAPPING);
    }
    return classMapping;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<IncludeDef> getIncludes()
  {
    if (includes == null)
    {
      includes = new EObjectContainmentEList<IncludeDef>(IncludeDef.class, this, JniMapPackage.MAPPING__INCLUDES);
    }
    return includes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<MethodGroup> getMethodGroups()
  {
    if (methodGroups == null)
    {
      methodGroups = new EObjectContainmentEList<MethodGroup>(MethodGroup.class, this, JniMapPackage.MAPPING__METHOD_GROUPS);
    }
    return methodGroups;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JniMapPackage.MAPPING__IMPORTS:
        return ((InternalEList<?>)getImports()).basicRemove(otherEnd, msgs);
      case JniMapPackage.MAPPING__EXTERNAL_LIBRARIES:
        return ((InternalEList<?>)getExternalLibraries()).basicRemove(otherEnd, msgs);
      case JniMapPackage.MAPPING__LIBRARIES:
        return ((InternalEList<?>)getLibraries()).basicRemove(otherEnd, msgs);
      case JniMapPackage.MAPPING__USER_MODULES:
        return ((InternalEList<?>)getUserModules()).basicRemove(otherEnd, msgs);
      case JniMapPackage.MAPPING__CLASS_MAPPING:
        return ((InternalEList<?>)getClassMapping()).basicRemove(otherEnd, msgs);
      case JniMapPackage.MAPPING__INCLUDES:
        return ((InternalEList<?>)getIncludes()).basicRemove(otherEnd, msgs);
      case JniMapPackage.MAPPING__METHOD_GROUPS:
        return ((InternalEList<?>)getMethodGroups()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JniMapPackage.MAPPING__IMPORTS:
        return getImports();
      case JniMapPackage.MAPPING__NAME:
        return getName();
      case JniMapPackage.MAPPING__PACKAGE:
        return getPackage();
      case JniMapPackage.MAPPING__HOSTS:
        return getHosts();
      case JniMapPackage.MAPPING__EXTERNAL_LIBRARIES:
        return getExternalLibraries();
      case JniMapPackage.MAPPING__LIBRARIES:
        return getLibraries();
      case JniMapPackage.MAPPING__USER_MODULES:
        return getUserModules();
      case JniMapPackage.MAPPING__CLASS_MAPPING:
        return getClassMapping();
      case JniMapPackage.MAPPING__INCLUDES:
        return getIncludes();
      case JniMapPackage.MAPPING__METHOD_GROUPS:
        return getMethodGroups();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JniMapPackage.MAPPING__IMPORTS:
        getImports().clear();
        getImports().addAll((Collection<? extends Import>)newValue);
        return;
      case JniMapPackage.MAPPING__NAME:
        setName((String)newValue);
        return;
      case JniMapPackage.MAPPING__PACKAGE:
        setPackage((String)newValue);
        return;
      case JniMapPackage.MAPPING__HOSTS:
        getHosts().clear();
        getHosts().addAll((Collection<? extends HostType>)newValue);
        return;
      case JniMapPackage.MAPPING__EXTERNAL_LIBRARIES:
        getExternalLibraries().clear();
        getExternalLibraries().addAll((Collection<? extends ExternalLibrary>)newValue);
        return;
      case JniMapPackage.MAPPING__LIBRARIES:
        getLibraries().clear();
        getLibraries().addAll((Collection<? extends Library>)newValue);
        return;
      case JniMapPackage.MAPPING__USER_MODULES:
        getUserModules().clear();
        getUserModules().addAll((Collection<? extends UserModule>)newValue);
        return;
      case JniMapPackage.MAPPING__CLASS_MAPPING:
        getClassMapping().clear();
        getClassMapping().addAll((Collection<? extends ClassMapping>)newValue);
        return;
      case JniMapPackage.MAPPING__INCLUDES:
        getIncludes().clear();
        getIncludes().addAll((Collection<? extends IncludeDef>)newValue);
        return;
      case JniMapPackage.MAPPING__METHOD_GROUPS:
        getMethodGroups().clear();
        getMethodGroups().addAll((Collection<? extends MethodGroup>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.MAPPING__IMPORTS:
        getImports().clear();
        return;
      case JniMapPackage.MAPPING__NAME:
        setName(NAME_EDEFAULT);
        return;
      case JniMapPackage.MAPPING__PACKAGE:
        setPackage(PACKAGE_EDEFAULT);
        return;
      case JniMapPackage.MAPPING__HOSTS:
        getHosts().clear();
        return;
      case JniMapPackage.MAPPING__EXTERNAL_LIBRARIES:
        getExternalLibraries().clear();
        return;
      case JniMapPackage.MAPPING__LIBRARIES:
        getLibraries().clear();
        return;
      case JniMapPackage.MAPPING__USER_MODULES:
        getUserModules().clear();
        return;
      case JniMapPackage.MAPPING__CLASS_MAPPING:
        getClassMapping().clear();
        return;
      case JniMapPackage.MAPPING__INCLUDES:
        getIncludes().clear();
        return;
      case JniMapPackage.MAPPING__METHOD_GROUPS:
        getMethodGroups().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.MAPPING__IMPORTS:
        return imports != null && !imports.isEmpty();
      case JniMapPackage.MAPPING__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case JniMapPackage.MAPPING__PACKAGE:
        return PACKAGE_EDEFAULT == null ? package_ != null : !PACKAGE_EDEFAULT.equals(package_);
      case JniMapPackage.MAPPING__HOSTS:
        return hosts != null && !hosts.isEmpty();
      case JniMapPackage.MAPPING__EXTERNAL_LIBRARIES:
        return externalLibraries != null && !externalLibraries.isEmpty();
      case JniMapPackage.MAPPING__LIBRARIES:
        return libraries != null && !libraries.isEmpty();
      case JniMapPackage.MAPPING__USER_MODULES:
        return userModules != null && !userModules.isEmpty();
      case JniMapPackage.MAPPING__CLASS_MAPPING:
        return classMapping != null && !classMapping.isEmpty();
      case JniMapPackage.MAPPING__INCLUDES:
        return includes != null && !includes.isEmpty();
      case JniMapPackage.MAPPING__METHOD_GROUPS:
        return methodGroups != null && !methodGroups.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", package: ");
    result.append(package_);
    result.append(", hosts: ");
    result.append(hosts);
    result.append(')');
    return result.toString();
  }

} //MappingImpl
