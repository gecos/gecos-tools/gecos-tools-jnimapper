/**
 */
package fr.irisa.cairn.jniMap.impl;

import fr.irisa.cairn.jniMap.EnumValue;
import fr.irisa.cairn.jniMap.JniMapPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enum Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.EnumValueImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.EnumValueImpl#getValue <em>Value</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.EnumValueImpl#getEnumNext <em>Enum Next</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnumValueImpl extends MinimalEObjectImpl.Container implements EnumValue
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected static final int VALUE_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected int value = VALUE_EDEFAULT;

  /**
   * The cached value of the '{@link #getEnumNext() <em>Enum Next</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEnumNext()
   * @generated
   * @ordered
   */
  protected EnumValue enumNext;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EnumValueImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JniMapPackage.Literals.ENUM_VALUE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.ENUM_VALUE__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue(int newValue)
  {
    int oldValue = value;
    value = newValue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.ENUM_VALUE__VALUE, oldValue, value));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumValue getEnumNext()
  {
    return enumNext;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetEnumNext(EnumValue newEnumNext, NotificationChain msgs)
  {
    EnumValue oldEnumNext = enumNext;
    enumNext = newEnumNext;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JniMapPackage.ENUM_VALUE__ENUM_NEXT, oldEnumNext, newEnumNext);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEnumNext(EnumValue newEnumNext)
  {
    if (newEnumNext != enumNext)
    {
      NotificationChain msgs = null;
      if (enumNext != null)
        msgs = ((InternalEObject)enumNext).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.ENUM_VALUE__ENUM_NEXT, null, msgs);
      if (newEnumNext != null)
        msgs = ((InternalEObject)newEnumNext).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.ENUM_VALUE__ENUM_NEXT, null, msgs);
      msgs = basicSetEnumNext(newEnumNext, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.ENUM_VALUE__ENUM_NEXT, newEnumNext, newEnumNext));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JniMapPackage.ENUM_VALUE__ENUM_NEXT:
        return basicSetEnumNext(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JniMapPackage.ENUM_VALUE__NAME:
        return getName();
      case JniMapPackage.ENUM_VALUE__VALUE:
        return getValue();
      case JniMapPackage.ENUM_VALUE__ENUM_NEXT:
        return getEnumNext();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JniMapPackage.ENUM_VALUE__NAME:
        setName((String)newValue);
        return;
      case JniMapPackage.ENUM_VALUE__VALUE:
        setValue((Integer)newValue);
        return;
      case JniMapPackage.ENUM_VALUE__ENUM_NEXT:
        setEnumNext((EnumValue)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.ENUM_VALUE__NAME:
        setName(NAME_EDEFAULT);
        return;
      case JniMapPackage.ENUM_VALUE__VALUE:
        setValue(VALUE_EDEFAULT);
        return;
      case JniMapPackage.ENUM_VALUE__ENUM_NEXT:
        setEnumNext((EnumValue)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.ENUM_VALUE__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case JniMapPackage.ENUM_VALUE__VALUE:
        return value != VALUE_EDEFAULT;
      case JniMapPackage.ENUM_VALUE__ENUM_NEXT:
        return enumNext != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", value: ");
    result.append(value);
    result.append(')');
    return result.toString();
  }

} //EnumValueImpl
