/**
 */
package fr.irisa.cairn.jniMap.impl;

import fr.irisa.cairn.jniMap.JniMapPackage;
import fr.irisa.cairn.jniMap.ParamDef;
import fr.irisa.cairn.jniMap.Type;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Param Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.ParamDefImpl#isGive <em>Give</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.ParamDefImpl#isTake <em>Take</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.ParamDefImpl#isKeep <em>Keep</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.ParamDefImpl#getType <em>Type</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.ParamDefImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParamDefImpl extends MinimalEObjectImpl.Container implements ParamDef
{
  /**
   * The default value of the '{@link #isGive() <em>Give</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isGive()
   * @generated
   * @ordered
   */
  protected static final boolean GIVE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isGive() <em>Give</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isGive()
   * @generated
   * @ordered
   */
  protected boolean give = GIVE_EDEFAULT;

  /**
   * The default value of the '{@link #isTake() <em>Take</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isTake()
   * @generated
   * @ordered
   */
  protected static final boolean TAKE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isTake() <em>Take</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isTake()
   * @generated
   * @ordered
   */
  protected boolean take = TAKE_EDEFAULT;

  /**
   * The default value of the '{@link #isKeep() <em>Keep</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isKeep()
   * @generated
   * @ordered
   */
  protected static final boolean KEEP_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isKeep() <em>Keep</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isKeep()
   * @generated
   * @ordered
   */
  protected boolean keep = KEEP_EDEFAULT;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected Type type;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ParamDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JniMapPackage.Literals.PARAM_DEF;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isGive()
  {
    return give;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGive(boolean newGive)
  {
    boolean oldGive = give;
    give = newGive;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.PARAM_DEF__GIVE, oldGive, give));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isTake()
  {
    return take;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTake(boolean newTake)
  {
    boolean oldTake = take;
    take = newTake;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.PARAM_DEF__TAKE, oldTake, take));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isKeep()
  {
    return keep;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setKeep(boolean newKeep)
  {
    boolean oldKeep = keep;
    keep = newKeep;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.PARAM_DEF__KEEP, oldKeep, keep));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(Type newType, NotificationChain msgs)
  {
    Type oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JniMapPackage.PARAM_DEF__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(Type newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.PARAM_DEF__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.PARAM_DEF__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.PARAM_DEF__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.PARAM_DEF__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JniMapPackage.PARAM_DEF__TYPE:
        return basicSetType(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JniMapPackage.PARAM_DEF__GIVE:
        return isGive();
      case JniMapPackage.PARAM_DEF__TAKE:
        return isTake();
      case JniMapPackage.PARAM_DEF__KEEP:
        return isKeep();
      case JniMapPackage.PARAM_DEF__TYPE:
        return getType();
      case JniMapPackage.PARAM_DEF__NAME:
        return getName();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JniMapPackage.PARAM_DEF__GIVE:
        setGive((Boolean)newValue);
        return;
      case JniMapPackage.PARAM_DEF__TAKE:
        setTake((Boolean)newValue);
        return;
      case JniMapPackage.PARAM_DEF__KEEP:
        setKeep((Boolean)newValue);
        return;
      case JniMapPackage.PARAM_DEF__TYPE:
        setType((Type)newValue);
        return;
      case JniMapPackage.PARAM_DEF__NAME:
        setName((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.PARAM_DEF__GIVE:
        setGive(GIVE_EDEFAULT);
        return;
      case JniMapPackage.PARAM_DEF__TAKE:
        setTake(TAKE_EDEFAULT);
        return;
      case JniMapPackage.PARAM_DEF__KEEP:
        setKeep(KEEP_EDEFAULT);
        return;
      case JniMapPackage.PARAM_DEF__TYPE:
        setType((Type)null);
        return;
      case JniMapPackage.PARAM_DEF__NAME:
        setName(NAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.PARAM_DEF__GIVE:
        return give != GIVE_EDEFAULT;
      case JniMapPackage.PARAM_DEF__TAKE:
        return take != TAKE_EDEFAULT;
      case JniMapPackage.PARAM_DEF__KEEP:
        return keep != KEEP_EDEFAULT;
      case JniMapPackage.PARAM_DEF__TYPE:
        return type != null;
      case JniMapPackage.PARAM_DEF__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (give: ");
    result.append(give);
    result.append(", take: ");
    result.append(take);
    result.append(", keep: ");
    result.append(keep);
    result.append(", name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //ParamDefImpl
