/**
 */
package fr.irisa.cairn.jniMap.impl;

import fr.irisa.cairn.jniMap.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JniMapFactoryImpl extends EFactoryImpl implements JniMapFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static JniMapFactory init()
  {
    try
    {
      JniMapFactory theJniMapFactory = (JniMapFactory)EPackage.Registry.INSTANCE.getEFactory(JniMapPackage.eNS_URI);
      if (theJniMapFactory != null)
      {
        return theJniMapFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new JniMapFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JniMapFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case JniMapPackage.MAPPING: return createMapping();
      case JniMapPackage.IMPORT: return createImport();
      case JniMapPackage.CLASS_MAPPING: return createClassMapping();
      case JniMapPackage.GENERICS_DECLARATION: return createGenericsDeclaration();
      case JniMapPackage.GENERICS_INSTANTIATION: return createGenericsInstantiation();
      case JniMapPackage.INTERFACE_IMPLEMENTATION: return createInterfaceImplementation();
      case JniMapPackage.ENUM_TO_CLASS_MAPPING: return createEnumToClassMapping();
      case JniMapPackage.STRUCT_TO_CLASS_MAPPING: return createStructToClassMapping();
      case JniMapPackage.CLASS_TO_CLASS_MAPPING: return createClassToClassMapping();
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING: return createAliasToClassMapping();
      case JniMapPackage.UNMAPPED_CLASS: return createUnmappedClass();
      case JniMapPackage.INTERFACE_CLASS: return createInterfaceClass();
      case JniMapPackage.EXTERNAL_LIBRARY: return createExternalLibrary();
      case JniMapPackage.LIBRARY: return createLibrary();
      case JniMapPackage.LIBRARY_FILE_NAME: return createLibraryFileName();
      case JniMapPackage.INCLUDE_DEF: return createIncludeDef();
      case JniMapPackage.USER_MODULE: return createUserModule();
      case JniMapPackage.CMETHOD: return createCMethod();
      case JniMapPackage.TYPE_DECLARATION: return createTypeDeclaration();
      case JniMapPackage.TYPE_ALIAS_DECLARATION: return createTypeAliasDeclaration();
      case JniMapPackage.STRUCT_DECLARATION: return createStructDeclaration();
      case JniMapPackage.CLASS_DECLARATION: return createClassDeclaration();
      case JniMapPackage.ENUM_DECLARATION: return createEnumDeclaration();
      case JniMapPackage.ENUM_TYPE: return createEnumType();
      case JniMapPackage.ENUM_VALUE: return createEnumValue();
      case JniMapPackage.PARAM_DEF: return createParamDef();
      case JniMapPackage.METHOD_GROUP: return createMethodGroup();
      case JniMapPackage.METHOD: return createMethod();
      case JniMapPackage.TYPE: return createType();
      case JniMapPackage.BUILT_IN_TYPE: return createBuiltInType();
      case JniMapPackage.BOOLEAN_TYPE: return createBooleanType();
      case JniMapPackage.INTEGER_TYPE: return createIntegerType();
      case JniMapPackage.REAL_TYPE: return createRealType();
      case JniMapPackage.STRING_TYPE: return createStringType();
      case JniMapPackage.VOID_TYPE: return createVoidType();
      case JniMapPackage.NESTED_TYPE: return createNestedType();
      case JniMapPackage.COMPLEX_TYPE: return createComplexType();
      case JniMapPackage.TYPE_ALIAS: return createTypeAlias();
      case JniMapPackage.ARRAY_TYPE: return createArrayType();
      case JniMapPackage.PTR_TYPE: return createPtrType();
      case JniMapPackage.MODIFIABLE_TYPE: return createModifiableType();
      case JniMapPackage.STRUCT_TYPE: return createStructType();
      case JniMapPackage.FIELD_DEF: return createFieldDef();
      case JniMapPackage.CONST_TYPE: return createConstType();
      case JniMapPackage.BASE_TYPE: return createBaseType();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
    switch (eDataType.getClassifierID())
    {
      case JniMapPackage.HOST_TYPE:
        return createHostTypeFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
    switch (eDataType.getClassifierID())
    {
      case JniMapPackage.HOST_TYPE:
        return convertHostTypeToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Mapping createMapping()
  {
    MappingImpl mapping = new MappingImpl();
    return mapping;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Import createImport()
  {
    ImportImpl import_ = new ImportImpl();
    return import_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClassMapping createClassMapping()
  {
    ClassMappingImpl classMapping = new ClassMappingImpl();
    return classMapping;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericsDeclaration createGenericsDeclaration()
  {
    GenericsDeclarationImpl genericsDeclaration = new GenericsDeclarationImpl();
    return genericsDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericsInstantiation createGenericsInstantiation()
  {
    GenericsInstantiationImpl genericsInstantiation = new GenericsInstantiationImpl();
    return genericsInstantiation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InterfaceImplementation createInterfaceImplementation()
  {
    InterfaceImplementationImpl interfaceImplementation = new InterfaceImplementationImpl();
    return interfaceImplementation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumToClassMapping createEnumToClassMapping()
  {
    EnumToClassMappingImpl enumToClassMapping = new EnumToClassMappingImpl();
    return enumToClassMapping;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructToClassMapping createStructToClassMapping()
  {
    StructToClassMappingImpl structToClassMapping = new StructToClassMappingImpl();
    return structToClassMapping;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClassToClassMapping createClassToClassMapping()
  {
    ClassToClassMappingImpl classToClassMapping = new ClassToClassMappingImpl();
    return classToClassMapping;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AliasToClassMapping createAliasToClassMapping()
  {
    AliasToClassMappingImpl aliasToClassMapping = new AliasToClassMappingImpl();
    return aliasToClassMapping;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnmappedClass createUnmappedClass()
  {
    UnmappedClassImpl unmappedClass = new UnmappedClassImpl();
    return unmappedClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InterfaceClass createInterfaceClass()
  {
    InterfaceClassImpl interfaceClass = new InterfaceClassImpl();
    return interfaceClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExternalLibrary createExternalLibrary()
  {
    ExternalLibraryImpl externalLibrary = new ExternalLibraryImpl();
    return externalLibrary;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Library createLibrary()
  {
    LibraryImpl library = new LibraryImpl();
    return library;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LibraryFileName createLibraryFileName()
  {
    LibraryFileNameImpl libraryFileName = new LibraryFileNameImpl();
    return libraryFileName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IncludeDef createIncludeDef()
  {
    IncludeDefImpl includeDef = new IncludeDefImpl();
    return includeDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UserModule createUserModule()
  {
    UserModuleImpl userModule = new UserModuleImpl();
    return userModule;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CMethod createCMethod()
  {
    CMethodImpl cMethod = new CMethodImpl();
    return cMethod;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeDeclaration createTypeDeclaration()
  {
    TypeDeclarationImpl typeDeclaration = new TypeDeclarationImpl();
    return typeDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeAliasDeclaration createTypeAliasDeclaration()
  {
    TypeAliasDeclarationImpl typeAliasDeclaration = new TypeAliasDeclarationImpl();
    return typeAliasDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructDeclaration createStructDeclaration()
  {
    StructDeclarationImpl structDeclaration = new StructDeclarationImpl();
    return structDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClassDeclaration createClassDeclaration()
  {
    ClassDeclarationImpl classDeclaration = new ClassDeclarationImpl();
    return classDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumDeclaration createEnumDeclaration()
  {
    EnumDeclarationImpl enumDeclaration = new EnumDeclarationImpl();
    return enumDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumType createEnumType()
  {
    EnumTypeImpl enumType = new EnumTypeImpl();
    return enumType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumValue createEnumValue()
  {
    EnumValueImpl enumValue = new EnumValueImpl();
    return enumValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParamDef createParamDef()
  {
    ParamDefImpl paramDef = new ParamDefImpl();
    return paramDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodGroup createMethodGroup()
  {
    MethodGroupImpl methodGroup = new MethodGroupImpl();
    return methodGroup;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Method createMethod()
  {
    MethodImpl method = new MethodImpl();
    return method;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type createType()
  {
    TypeImpl type = new TypeImpl();
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BuiltInType createBuiltInType()
  {
    BuiltInTypeImpl builtInType = new BuiltInTypeImpl();
    return builtInType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanType createBooleanType()
  {
    BooleanTypeImpl booleanType = new BooleanTypeImpl();
    return booleanType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerType createIntegerType()
  {
    IntegerTypeImpl integerType = new IntegerTypeImpl();
    return integerType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RealType createRealType()
  {
    RealTypeImpl realType = new RealTypeImpl();
    return realType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StringType createStringType()
  {
    StringTypeImpl stringType = new StringTypeImpl();
    return stringType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VoidType createVoidType()
  {
    VoidTypeImpl voidType = new VoidTypeImpl();
    return voidType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedType createNestedType()
  {
    NestedTypeImpl nestedType = new NestedTypeImpl();
    return nestedType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComplexType createComplexType()
  {
    ComplexTypeImpl complexType = new ComplexTypeImpl();
    return complexType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeAlias createTypeAlias()
  {
    TypeAliasImpl typeAlias = new TypeAliasImpl();
    return typeAlias;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayType createArrayType()
  {
    ArrayTypeImpl arrayType = new ArrayTypeImpl();
    return arrayType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PtrType createPtrType()
  {
    PtrTypeImpl ptrType = new PtrTypeImpl();
    return ptrType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModifiableType createModifiableType()
  {
    ModifiableTypeImpl modifiableType = new ModifiableTypeImpl();
    return modifiableType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructType createStructType()
  {
    StructTypeImpl structType = new StructTypeImpl();
    return structType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldDef createFieldDef()
  {
    FieldDefImpl fieldDef = new FieldDefImpl();
    return fieldDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstType createConstType()
  {
    ConstTypeImpl constType = new ConstTypeImpl();
    return constType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseType createBaseType()
  {
    BaseTypeImpl baseType = new BaseTypeImpl();
    return baseType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HostType createHostTypeFromString(EDataType eDataType, String initialValue)
  {
    HostType result = HostType.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertHostTypeToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JniMapPackage getJniMapPackage()
  {
    return (JniMapPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static JniMapPackage getPackage()
  {
    return JniMapPackage.eINSTANCE;
  }

} //JniMapFactoryImpl
