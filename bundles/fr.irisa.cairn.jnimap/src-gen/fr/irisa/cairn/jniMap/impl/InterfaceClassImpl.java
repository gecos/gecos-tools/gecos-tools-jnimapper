/**
 */
package fr.irisa.cairn.jniMap.impl;

import fr.irisa.cairn.jniMap.GenericsDeclaration;
import fr.irisa.cairn.jniMap.InterfaceClass;
import fr.irisa.cairn.jniMap.InterfaceImplementation;
import fr.irisa.cairn.jniMap.JniMapPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interface Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.InterfaceClassImpl#getGenericsDecl <em>Generics Decl</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.InterfaceClassImpl#getInterfaces <em>Interfaces</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InterfaceClassImpl extends ClassMappingImpl implements InterfaceClass
{
  /**
   * The cached value of the '{@link #getGenericsDecl() <em>Generics Decl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGenericsDecl()
   * @generated
   * @ordered
   */
  protected GenericsDeclaration genericsDecl;

  /**
   * The cached value of the '{@link #getInterfaces() <em>Interfaces</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInterfaces()
   * @generated
   * @ordered
   */
  protected EList<InterfaceImplementation> interfaces;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InterfaceClassImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JniMapPackage.Literals.INTERFACE_CLASS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericsDeclaration getGenericsDecl()
  {
    return genericsDecl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGenericsDecl(GenericsDeclaration newGenericsDecl, NotificationChain msgs)
  {
    GenericsDeclaration oldGenericsDecl = genericsDecl;
    genericsDecl = newGenericsDecl;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JniMapPackage.INTERFACE_CLASS__GENERICS_DECL, oldGenericsDecl, newGenericsDecl);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGenericsDecl(GenericsDeclaration newGenericsDecl)
  {
    if (newGenericsDecl != genericsDecl)
    {
      NotificationChain msgs = null;
      if (genericsDecl != null)
        msgs = ((InternalEObject)genericsDecl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.INTERFACE_CLASS__GENERICS_DECL, null, msgs);
      if (newGenericsDecl != null)
        msgs = ((InternalEObject)newGenericsDecl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.INTERFACE_CLASS__GENERICS_DECL, null, msgs);
      msgs = basicSetGenericsDecl(newGenericsDecl, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.INTERFACE_CLASS__GENERICS_DECL, newGenericsDecl, newGenericsDecl));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<InterfaceImplementation> getInterfaces()
  {
    if (interfaces == null)
    {
      interfaces = new EObjectContainmentEList<InterfaceImplementation>(InterfaceImplementation.class, this, JniMapPackage.INTERFACE_CLASS__INTERFACES);
    }
    return interfaces;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JniMapPackage.INTERFACE_CLASS__GENERICS_DECL:
        return basicSetGenericsDecl(null, msgs);
      case JniMapPackage.INTERFACE_CLASS__INTERFACES:
        return ((InternalEList<?>)getInterfaces()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JniMapPackage.INTERFACE_CLASS__GENERICS_DECL:
        return getGenericsDecl();
      case JniMapPackage.INTERFACE_CLASS__INTERFACES:
        return getInterfaces();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JniMapPackage.INTERFACE_CLASS__GENERICS_DECL:
        setGenericsDecl((GenericsDeclaration)newValue);
        return;
      case JniMapPackage.INTERFACE_CLASS__INTERFACES:
        getInterfaces().clear();
        getInterfaces().addAll((Collection<? extends InterfaceImplementation>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.INTERFACE_CLASS__GENERICS_DECL:
        setGenericsDecl((GenericsDeclaration)null);
        return;
      case JniMapPackage.INTERFACE_CLASS__INTERFACES:
        getInterfaces().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.INTERFACE_CLASS__GENERICS_DECL:
        return genericsDecl != null;
      case JniMapPackage.INTERFACE_CLASS__INTERFACES:
        return interfaces != null && !interfaces.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //InterfaceClassImpl
