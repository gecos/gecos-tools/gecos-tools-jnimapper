/**
 */
package fr.irisa.cairn.jniMap.impl;

import fr.irisa.cairn.jniMap.BaseType;
import fr.irisa.cairn.jniMap.JniMapPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BaseTypeImpl extends TypeImpl implements BaseType
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BaseTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JniMapPackage.Literals.BASE_TYPE;
  }

} //BaseTypeImpl
