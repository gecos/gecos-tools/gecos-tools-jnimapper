/**
 */
package fr.irisa.cairn.jniMap.impl;

import fr.irisa.cairn.jniMap.AliasToClassMapping;
import fr.irisa.cairn.jniMap.ClassMapping;
import fr.irisa.cairn.jniMap.GenericsDeclaration;
import fr.irisa.cairn.jniMap.GenericsInstantiation;
import fr.irisa.cairn.jniMap.InterfaceImplementation;
import fr.irisa.cairn.jniMap.JniMapPackage;
import fr.irisa.cairn.jniMap.TypeAliasDeclaration;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alias To Class Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.AliasToClassMappingImpl#getAlias <em>Alias</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.AliasToClassMappingImpl#getGenericsDecl <em>Generics Decl</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.AliasToClassMappingImpl#getSuper <em>Super</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.AliasToClassMappingImpl#getGenericsInstantiation <em>Generics Instantiation</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.AliasToClassMappingImpl#getInterfaces <em>Interfaces</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AliasToClassMappingImpl extends ClassMappingImpl implements AliasToClassMapping
{
  /**
   * The cached value of the '{@link #getAlias() <em>Alias</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAlias()
   * @generated
   * @ordered
   */
  protected TypeAliasDeclaration alias;

  /**
   * The cached value of the '{@link #getGenericsDecl() <em>Generics Decl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGenericsDecl()
   * @generated
   * @ordered
   */
  protected GenericsDeclaration genericsDecl;

  /**
   * The cached value of the '{@link #getSuper() <em>Super</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSuper()
   * @generated
   * @ordered
   */
  protected ClassMapping super_;

  /**
   * The cached value of the '{@link #getGenericsInstantiation() <em>Generics Instantiation</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGenericsInstantiation()
   * @generated
   * @ordered
   */
  protected GenericsInstantiation genericsInstantiation;

  /**
   * The cached value of the '{@link #getInterfaces() <em>Interfaces</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInterfaces()
   * @generated
   * @ordered
   */
  protected EList<InterfaceImplementation> interfaces;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AliasToClassMappingImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JniMapPackage.Literals.ALIAS_TO_CLASS_MAPPING;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeAliasDeclaration getAlias()
  {
    return alias;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAlias(TypeAliasDeclaration newAlias, NotificationChain msgs)
  {
    TypeAliasDeclaration oldAlias = alias;
    alias = newAlias;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JniMapPackage.ALIAS_TO_CLASS_MAPPING__ALIAS, oldAlias, newAlias);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAlias(TypeAliasDeclaration newAlias)
  {
    if (newAlias != alias)
    {
      NotificationChain msgs = null;
      if (alias != null)
        msgs = ((InternalEObject)alias).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.ALIAS_TO_CLASS_MAPPING__ALIAS, null, msgs);
      if (newAlias != null)
        msgs = ((InternalEObject)newAlias).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.ALIAS_TO_CLASS_MAPPING__ALIAS, null, msgs);
      msgs = basicSetAlias(newAlias, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.ALIAS_TO_CLASS_MAPPING__ALIAS, newAlias, newAlias));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericsDeclaration getGenericsDecl()
  {
    return genericsDecl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGenericsDecl(GenericsDeclaration newGenericsDecl, NotificationChain msgs)
  {
    GenericsDeclaration oldGenericsDecl = genericsDecl;
    genericsDecl = newGenericsDecl;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_DECL, oldGenericsDecl, newGenericsDecl);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGenericsDecl(GenericsDeclaration newGenericsDecl)
  {
    if (newGenericsDecl != genericsDecl)
    {
      NotificationChain msgs = null;
      if (genericsDecl != null)
        msgs = ((InternalEObject)genericsDecl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_DECL, null, msgs);
      if (newGenericsDecl != null)
        msgs = ((InternalEObject)newGenericsDecl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_DECL, null, msgs);
      msgs = basicSetGenericsDecl(newGenericsDecl, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_DECL, newGenericsDecl, newGenericsDecl));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClassMapping getSuper()
  {
    if (super_ != null && super_.eIsProxy())
    {
      InternalEObject oldSuper = (InternalEObject)super_;
      super_ = (ClassMapping)eResolveProxy(oldSuper);
      if (super_ != oldSuper)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, JniMapPackage.ALIAS_TO_CLASS_MAPPING__SUPER, oldSuper, super_));
      }
    }
    return super_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClassMapping basicGetSuper()
  {
    return super_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSuper(ClassMapping newSuper)
  {
    ClassMapping oldSuper = super_;
    super_ = newSuper;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.ALIAS_TO_CLASS_MAPPING__SUPER, oldSuper, super_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericsInstantiation getGenericsInstantiation()
  {
    return genericsInstantiation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGenericsInstantiation(GenericsInstantiation newGenericsInstantiation, NotificationChain msgs)
  {
    GenericsInstantiation oldGenericsInstantiation = genericsInstantiation;
    genericsInstantiation = newGenericsInstantiation;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_INSTANTIATION, oldGenericsInstantiation, newGenericsInstantiation);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGenericsInstantiation(GenericsInstantiation newGenericsInstantiation)
  {
    if (newGenericsInstantiation != genericsInstantiation)
    {
      NotificationChain msgs = null;
      if (genericsInstantiation != null)
        msgs = ((InternalEObject)genericsInstantiation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_INSTANTIATION, null, msgs);
      if (newGenericsInstantiation != null)
        msgs = ((InternalEObject)newGenericsInstantiation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_INSTANTIATION, null, msgs);
      msgs = basicSetGenericsInstantiation(newGenericsInstantiation, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_INSTANTIATION, newGenericsInstantiation, newGenericsInstantiation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<InterfaceImplementation> getInterfaces()
  {
    if (interfaces == null)
    {
      interfaces = new EObjectContainmentEList<InterfaceImplementation>(InterfaceImplementation.class, this, JniMapPackage.ALIAS_TO_CLASS_MAPPING__INTERFACES);
    }
    return interfaces;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__ALIAS:
        return basicSetAlias(null, msgs);
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_DECL:
        return basicSetGenericsDecl(null, msgs);
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_INSTANTIATION:
        return basicSetGenericsInstantiation(null, msgs);
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__INTERFACES:
        return ((InternalEList<?>)getInterfaces()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__ALIAS:
        return getAlias();
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_DECL:
        return getGenericsDecl();
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__SUPER:
        if (resolve) return getSuper();
        return basicGetSuper();
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_INSTANTIATION:
        return getGenericsInstantiation();
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__INTERFACES:
        return getInterfaces();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__ALIAS:
        setAlias((TypeAliasDeclaration)newValue);
        return;
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_DECL:
        setGenericsDecl((GenericsDeclaration)newValue);
        return;
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__SUPER:
        setSuper((ClassMapping)newValue);
        return;
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_INSTANTIATION:
        setGenericsInstantiation((GenericsInstantiation)newValue);
        return;
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__INTERFACES:
        getInterfaces().clear();
        getInterfaces().addAll((Collection<? extends InterfaceImplementation>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__ALIAS:
        setAlias((TypeAliasDeclaration)null);
        return;
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_DECL:
        setGenericsDecl((GenericsDeclaration)null);
        return;
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__SUPER:
        setSuper((ClassMapping)null);
        return;
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_INSTANTIATION:
        setGenericsInstantiation((GenericsInstantiation)null);
        return;
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__INTERFACES:
        getInterfaces().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__ALIAS:
        return alias != null;
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_DECL:
        return genericsDecl != null;
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__SUPER:
        return super_ != null;
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__GENERICS_INSTANTIATION:
        return genericsInstantiation != null;
      case JniMapPackage.ALIAS_TO_CLASS_MAPPING__INTERFACES:
        return interfaces != null && !interfaces.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //AliasToClassMappingImpl
