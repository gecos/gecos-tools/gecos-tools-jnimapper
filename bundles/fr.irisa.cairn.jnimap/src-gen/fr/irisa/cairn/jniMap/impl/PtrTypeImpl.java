/**
 */
package fr.irisa.cairn.jniMap.impl;

import fr.irisa.cairn.jniMap.BaseType;
import fr.irisa.cairn.jniMap.JniMapPackage;
import fr.irisa.cairn.jniMap.PtrType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ptr Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.PtrTypeImpl#getBase <em>Base</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.PtrTypeImpl#getIndir <em>Indir</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.PtrTypeImpl#isIgnored <em>Ignored</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PtrTypeImpl extends TypeImpl implements PtrType
{
  /**
   * The cached value of the '{@link #getBase() <em>Base</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBase()
   * @generated
   * @ordered
   */
  protected BaseType base;

  /**
   * The cached value of the '{@link #getIndir() <em>Indir</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIndir()
   * @generated
   * @ordered
   */
  protected EList<String> indir;

  /**
   * The default value of the '{@link #isIgnored() <em>Ignored</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isIgnored()
   * @generated
   * @ordered
   */
  protected static final boolean IGNORED_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isIgnored() <em>Ignored</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isIgnored()
   * @generated
   * @ordered
   */
  protected boolean ignored = IGNORED_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PtrTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JniMapPackage.Literals.PTR_TYPE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseType getBase()
  {
    return base;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBase(BaseType newBase, NotificationChain msgs)
  {
    BaseType oldBase = base;
    base = newBase;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JniMapPackage.PTR_TYPE__BASE, oldBase, newBase);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBase(BaseType newBase)
  {
    if (newBase != base)
    {
      NotificationChain msgs = null;
      if (base != null)
        msgs = ((InternalEObject)base).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.PTR_TYPE__BASE, null, msgs);
      if (newBase != null)
        msgs = ((InternalEObject)newBase).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.PTR_TYPE__BASE, null, msgs);
      msgs = basicSetBase(newBase, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.PTR_TYPE__BASE, newBase, newBase));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getIndir()
  {
    if (indir == null)
    {
      indir = new EDataTypeEList<String>(String.class, this, JniMapPackage.PTR_TYPE__INDIR);
    }
    return indir;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isIgnored()
  {
    return ignored;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIgnored(boolean newIgnored)
  {
    boolean oldIgnored = ignored;
    ignored = newIgnored;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.PTR_TYPE__IGNORED, oldIgnored, ignored));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JniMapPackage.PTR_TYPE__BASE:
        return basicSetBase(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JniMapPackage.PTR_TYPE__BASE:
        return getBase();
      case JniMapPackage.PTR_TYPE__INDIR:
        return getIndir();
      case JniMapPackage.PTR_TYPE__IGNORED:
        return isIgnored();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JniMapPackage.PTR_TYPE__BASE:
        setBase((BaseType)newValue);
        return;
      case JniMapPackage.PTR_TYPE__INDIR:
        getIndir().clear();
        getIndir().addAll((Collection<? extends String>)newValue);
        return;
      case JniMapPackage.PTR_TYPE__IGNORED:
        setIgnored((Boolean)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.PTR_TYPE__BASE:
        setBase((BaseType)null);
        return;
      case JniMapPackage.PTR_TYPE__INDIR:
        getIndir().clear();
        return;
      case JniMapPackage.PTR_TYPE__IGNORED:
        setIgnored(IGNORED_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.PTR_TYPE__BASE:
        return base != null;
      case JniMapPackage.PTR_TYPE__INDIR:
        return indir != null && !indir.isEmpty();
      case JniMapPackage.PTR_TYPE__IGNORED:
        return ignored != IGNORED_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (indir: ");
    result.append(indir);
    result.append(", ignored: ");
    result.append(ignored);
    result.append(')');
    return result.toString();
  }

} //PtrTypeImpl
