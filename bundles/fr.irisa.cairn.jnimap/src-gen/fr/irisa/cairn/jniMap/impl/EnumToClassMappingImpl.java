/**
 */
package fr.irisa.cairn.jniMap.impl;

import fr.irisa.cairn.jniMap.EnumDeclaration;
import fr.irisa.cairn.jniMap.EnumToClassMapping;
import fr.irisa.cairn.jniMap.JniMapPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enum To Class Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.EnumToClassMappingImpl#getEnum_ <em>Enum </em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnumToClassMappingImpl extends ClassMappingImpl implements EnumToClassMapping
{
  /**
   * The cached value of the '{@link #getEnum_() <em>Enum </em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEnum_()
   * @generated
   * @ordered
   */
  protected EnumDeclaration enum_;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EnumToClassMappingImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JniMapPackage.Literals.ENUM_TO_CLASS_MAPPING;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumDeclaration getEnum_()
  {
    return enum_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetEnum_(EnumDeclaration newEnum_, NotificationChain msgs)
  {
    EnumDeclaration oldEnum_ = enum_;
    enum_ = newEnum_;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JniMapPackage.ENUM_TO_CLASS_MAPPING__ENUM_, oldEnum_, newEnum_);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEnum_(EnumDeclaration newEnum_)
  {
    if (newEnum_ != enum_)
    {
      NotificationChain msgs = null;
      if (enum_ != null)
        msgs = ((InternalEObject)enum_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.ENUM_TO_CLASS_MAPPING__ENUM_, null, msgs);
      if (newEnum_ != null)
        msgs = ((InternalEObject)newEnum_).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.ENUM_TO_CLASS_MAPPING__ENUM_, null, msgs);
      msgs = basicSetEnum_(newEnum_, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.ENUM_TO_CLASS_MAPPING__ENUM_, newEnum_, newEnum_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JniMapPackage.ENUM_TO_CLASS_MAPPING__ENUM_:
        return basicSetEnum_(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JniMapPackage.ENUM_TO_CLASS_MAPPING__ENUM_:
        return getEnum_();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JniMapPackage.ENUM_TO_CLASS_MAPPING__ENUM_:
        setEnum_((EnumDeclaration)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.ENUM_TO_CLASS_MAPPING__ENUM_:
        setEnum_((EnumDeclaration)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.ENUM_TO_CLASS_MAPPING__ENUM_:
        return enum_ != null;
    }
    return super.eIsSet(featureID);
  }

} //EnumToClassMappingImpl
