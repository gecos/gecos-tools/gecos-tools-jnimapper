/**
 */
package fr.irisa.cairn.jniMap.impl;

import fr.irisa.cairn.jniMap.GenericsInstantiation;
import fr.irisa.cairn.jniMap.InterfaceClass;
import fr.irisa.cairn.jniMap.InterfaceImplementation;
import fr.irisa.cairn.jniMap.JniMapPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interface Implementation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.InterfaceImplementationImpl#getInterface <em>Interface</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.InterfaceImplementationImpl#getGenericsInstantiation <em>Generics Instantiation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InterfaceImplementationImpl extends MinimalEObjectImpl.Container implements InterfaceImplementation
{
  /**
   * The cached value of the '{@link #getInterface() <em>Interface</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInterface()
   * @generated
   * @ordered
   */
  protected InterfaceClass interface_;

  /**
   * The cached value of the '{@link #getGenericsInstantiation() <em>Generics Instantiation</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGenericsInstantiation()
   * @generated
   * @ordered
   */
  protected GenericsInstantiation genericsInstantiation;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InterfaceImplementationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JniMapPackage.Literals.INTERFACE_IMPLEMENTATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InterfaceClass getInterface()
  {
    if (interface_ != null && interface_.eIsProxy())
    {
      InternalEObject oldInterface = (InternalEObject)interface_;
      interface_ = (InterfaceClass)eResolveProxy(oldInterface);
      if (interface_ != oldInterface)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, JniMapPackage.INTERFACE_IMPLEMENTATION__INTERFACE, oldInterface, interface_));
      }
    }
    return interface_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InterfaceClass basicGetInterface()
  {
    return interface_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInterface(InterfaceClass newInterface)
  {
    InterfaceClass oldInterface = interface_;
    interface_ = newInterface;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.INTERFACE_IMPLEMENTATION__INTERFACE, oldInterface, interface_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericsInstantiation getGenericsInstantiation()
  {
    return genericsInstantiation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGenericsInstantiation(GenericsInstantiation newGenericsInstantiation, NotificationChain msgs)
  {
    GenericsInstantiation oldGenericsInstantiation = genericsInstantiation;
    genericsInstantiation = newGenericsInstantiation;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JniMapPackage.INTERFACE_IMPLEMENTATION__GENERICS_INSTANTIATION, oldGenericsInstantiation, newGenericsInstantiation);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGenericsInstantiation(GenericsInstantiation newGenericsInstantiation)
  {
    if (newGenericsInstantiation != genericsInstantiation)
    {
      NotificationChain msgs = null;
      if (genericsInstantiation != null)
        msgs = ((InternalEObject)genericsInstantiation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.INTERFACE_IMPLEMENTATION__GENERICS_INSTANTIATION, null, msgs);
      if (newGenericsInstantiation != null)
        msgs = ((InternalEObject)newGenericsInstantiation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.INTERFACE_IMPLEMENTATION__GENERICS_INSTANTIATION, null, msgs);
      msgs = basicSetGenericsInstantiation(newGenericsInstantiation, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.INTERFACE_IMPLEMENTATION__GENERICS_INSTANTIATION, newGenericsInstantiation, newGenericsInstantiation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JniMapPackage.INTERFACE_IMPLEMENTATION__GENERICS_INSTANTIATION:
        return basicSetGenericsInstantiation(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JniMapPackage.INTERFACE_IMPLEMENTATION__INTERFACE:
        if (resolve) return getInterface();
        return basicGetInterface();
      case JniMapPackage.INTERFACE_IMPLEMENTATION__GENERICS_INSTANTIATION:
        return getGenericsInstantiation();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JniMapPackage.INTERFACE_IMPLEMENTATION__INTERFACE:
        setInterface((InterfaceClass)newValue);
        return;
      case JniMapPackage.INTERFACE_IMPLEMENTATION__GENERICS_INSTANTIATION:
        setGenericsInstantiation((GenericsInstantiation)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.INTERFACE_IMPLEMENTATION__INTERFACE:
        setInterface((InterfaceClass)null);
        return;
      case JniMapPackage.INTERFACE_IMPLEMENTATION__GENERICS_INSTANTIATION:
        setGenericsInstantiation((GenericsInstantiation)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.INTERFACE_IMPLEMENTATION__INTERFACE:
        return interface_ != null;
      case JniMapPackage.INTERFACE_IMPLEMENTATION__GENERICS_INSTANTIATION:
        return genericsInstantiation != null;
    }
    return super.eIsSet(featureID);
  }

} //InterfaceImplementationImpl
