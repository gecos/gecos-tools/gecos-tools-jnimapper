/**
 */
package fr.irisa.cairn.jniMap.impl;

import fr.irisa.cairn.jniMap.JniMapPackage;
import fr.irisa.cairn.jniMap.Method;
import fr.irisa.cairn.jniMap.ParamDef;
import fr.irisa.cairn.jniMap.Type;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Method</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#isConstructor <em>Constructor</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#isDestructor <em>Destructor</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#isInstanceof <em>Instanceof</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#isStatic <em>Static</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#isPrivate <em>Private</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#isProtected <em>Protected</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#isRenamed <em>Renamed</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#getNewname <em>Newname</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#isStub <em>Stub</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#getArg <em>Arg</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#getRes <em>Res</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.impl.MethodImpl#getParams <em>Params</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MethodImpl extends MinimalEObjectImpl.Container implements Method
{
  /**
   * The default value of the '{@link #isConstructor() <em>Constructor</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isConstructor()
   * @generated
   * @ordered
   */
  protected static final boolean CONSTRUCTOR_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isConstructor() <em>Constructor</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isConstructor()
   * @generated
   * @ordered
   */
  protected boolean constructor = CONSTRUCTOR_EDEFAULT;

  /**
   * The default value of the '{@link #isDestructor() <em>Destructor</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isDestructor()
   * @generated
   * @ordered
   */
  protected static final boolean DESTRUCTOR_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isDestructor() <em>Destructor</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isDestructor()
   * @generated
   * @ordered
   */
  protected boolean destructor = DESTRUCTOR_EDEFAULT;

  /**
   * The default value of the '{@link #isInstanceof() <em>Instanceof</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isInstanceof()
   * @generated
   * @ordered
   */
  protected static final boolean INSTANCEOF_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isInstanceof() <em>Instanceof</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isInstanceof()
   * @generated
   * @ordered
   */
  protected boolean instanceof_ = INSTANCEOF_EDEFAULT;

  /**
   * The default value of the '{@link #isStatic() <em>Static</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStatic()
   * @generated
   * @ordered
   */
  protected static final boolean STATIC_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isStatic() <em>Static</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStatic()
   * @generated
   * @ordered
   */
  protected boolean static_ = STATIC_EDEFAULT;

  /**
   * The default value of the '{@link #isPrivate() <em>Private</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isPrivate()
   * @generated
   * @ordered
   */
  protected static final boolean PRIVATE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isPrivate() <em>Private</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isPrivate()
   * @generated
   * @ordered
   */
  protected boolean private_ = PRIVATE_EDEFAULT;

  /**
   * The default value of the '{@link #isProtected() <em>Protected</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isProtected()
   * @generated
   * @ordered
   */
  protected static final boolean PROTECTED_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isProtected() <em>Protected</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isProtected()
   * @generated
   * @ordered
   */
  protected boolean protected_ = PROTECTED_EDEFAULT;

  /**
   * The default value of the '{@link #isRenamed() <em>Renamed</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isRenamed()
   * @generated
   * @ordered
   */
  protected static final boolean RENAMED_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isRenamed() <em>Renamed</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isRenamed()
   * @generated
   * @ordered
   */
  protected boolean renamed = RENAMED_EDEFAULT;

  /**
   * The default value of the '{@link #getNewname() <em>Newname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNewname()
   * @generated
   * @ordered
   */
  protected static final String NEWNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNewname() <em>Newname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNewname()
   * @generated
   * @ordered
   */
  protected String newname = NEWNAME_EDEFAULT;

  /**
   * The default value of the '{@link #isStub() <em>Stub</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStub()
   * @generated
   * @ordered
   */
  protected static final boolean STUB_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isStub() <em>Stub</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStub()
   * @generated
   * @ordered
   */
  protected boolean stub = STUB_EDEFAULT;

  /**
   * The default value of the '{@link #getArg() <em>Arg</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArg()
   * @generated
   * @ordered
   */
  protected static final int ARG_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getArg() <em>Arg</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArg()
   * @generated
   * @ordered
   */
  protected int arg = ARG_EDEFAULT;

  /**
   * The cached value of the '{@link #getRes() <em>Res</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRes()
   * @generated
   * @ordered
   */
  protected Type res;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getParams() <em>Params</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParams()
   * @generated
   * @ordered
   */
  protected EList<ParamDef> params;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MethodImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JniMapPackage.Literals.METHOD;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isConstructor()
  {
    return constructor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConstructor(boolean newConstructor)
  {
    boolean oldConstructor = constructor;
    constructor = newConstructor;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__CONSTRUCTOR, oldConstructor, constructor));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isDestructor()
  {
    return destructor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDestructor(boolean newDestructor)
  {
    boolean oldDestructor = destructor;
    destructor = newDestructor;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__DESTRUCTOR, oldDestructor, destructor));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isInstanceof()
  {
    return instanceof_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInstanceof(boolean newInstanceof)
  {
    boolean oldInstanceof = instanceof_;
    instanceof_ = newInstanceof;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__INSTANCEOF, oldInstanceof, instanceof_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isStatic()
  {
    return static_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatic(boolean newStatic)
  {
    boolean oldStatic = static_;
    static_ = newStatic;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__STATIC, oldStatic, static_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isPrivate()
  {
    return private_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPrivate(boolean newPrivate)
  {
    boolean oldPrivate = private_;
    private_ = newPrivate;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__PRIVATE, oldPrivate, private_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isProtected()
  {
    return protected_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setProtected(boolean newProtected)
  {
    boolean oldProtected = protected_;
    protected_ = newProtected;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__PROTECTED, oldProtected, protected_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isRenamed()
  {
    return renamed;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRenamed(boolean newRenamed)
  {
    boolean oldRenamed = renamed;
    renamed = newRenamed;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__RENAMED, oldRenamed, renamed));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getNewname()
  {
    return newname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNewname(String newNewname)
  {
    String oldNewname = newname;
    newname = newNewname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__NEWNAME, oldNewname, newname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isStub()
  {
    return stub;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStub(boolean newStub)
  {
    boolean oldStub = stub;
    stub = newStub;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__STUB, oldStub, stub));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getArg()
  {
    return arg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setArg(int newArg)
  {
    int oldArg = arg;
    arg = newArg;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__ARG, oldArg, arg));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getRes()
  {
    return res;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRes(Type newRes, NotificationChain msgs)
  {
    Type oldRes = res;
    res = newRes;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__RES, oldRes, newRes);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRes(Type newRes)
  {
    if (newRes != res)
    {
      NotificationChain msgs = null;
      if (res != null)
        msgs = ((InternalEObject)res).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.METHOD__RES, null, msgs);
      if (newRes != null)
        msgs = ((InternalEObject)newRes).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JniMapPackage.METHOD__RES, null, msgs);
      msgs = basicSetRes(newRes, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__RES, newRes, newRes));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JniMapPackage.METHOD__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ParamDef> getParams()
  {
    if (params == null)
    {
      params = new EObjectContainmentEList<ParamDef>(ParamDef.class, this, JniMapPackage.METHOD__PARAMS);
    }
    return params;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JniMapPackage.METHOD__RES:
        return basicSetRes(null, msgs);
      case JniMapPackage.METHOD__PARAMS:
        return ((InternalEList<?>)getParams()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JniMapPackage.METHOD__CONSTRUCTOR:
        return isConstructor();
      case JniMapPackage.METHOD__DESTRUCTOR:
        return isDestructor();
      case JniMapPackage.METHOD__INSTANCEOF:
        return isInstanceof();
      case JniMapPackage.METHOD__STATIC:
        return isStatic();
      case JniMapPackage.METHOD__PRIVATE:
        return isPrivate();
      case JniMapPackage.METHOD__PROTECTED:
        return isProtected();
      case JniMapPackage.METHOD__RENAMED:
        return isRenamed();
      case JniMapPackage.METHOD__NEWNAME:
        return getNewname();
      case JniMapPackage.METHOD__STUB:
        return isStub();
      case JniMapPackage.METHOD__ARG:
        return getArg();
      case JniMapPackage.METHOD__RES:
        return getRes();
      case JniMapPackage.METHOD__NAME:
        return getName();
      case JniMapPackage.METHOD__PARAMS:
        return getParams();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JniMapPackage.METHOD__CONSTRUCTOR:
        setConstructor((Boolean)newValue);
        return;
      case JniMapPackage.METHOD__DESTRUCTOR:
        setDestructor((Boolean)newValue);
        return;
      case JniMapPackage.METHOD__INSTANCEOF:
        setInstanceof((Boolean)newValue);
        return;
      case JniMapPackage.METHOD__STATIC:
        setStatic((Boolean)newValue);
        return;
      case JniMapPackage.METHOD__PRIVATE:
        setPrivate((Boolean)newValue);
        return;
      case JniMapPackage.METHOD__PROTECTED:
        setProtected((Boolean)newValue);
        return;
      case JniMapPackage.METHOD__RENAMED:
        setRenamed((Boolean)newValue);
        return;
      case JniMapPackage.METHOD__NEWNAME:
        setNewname((String)newValue);
        return;
      case JniMapPackage.METHOD__STUB:
        setStub((Boolean)newValue);
        return;
      case JniMapPackage.METHOD__ARG:
        setArg((Integer)newValue);
        return;
      case JniMapPackage.METHOD__RES:
        setRes((Type)newValue);
        return;
      case JniMapPackage.METHOD__NAME:
        setName((String)newValue);
        return;
      case JniMapPackage.METHOD__PARAMS:
        getParams().clear();
        getParams().addAll((Collection<? extends ParamDef>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.METHOD__CONSTRUCTOR:
        setConstructor(CONSTRUCTOR_EDEFAULT);
        return;
      case JniMapPackage.METHOD__DESTRUCTOR:
        setDestructor(DESTRUCTOR_EDEFAULT);
        return;
      case JniMapPackage.METHOD__INSTANCEOF:
        setInstanceof(INSTANCEOF_EDEFAULT);
        return;
      case JniMapPackage.METHOD__STATIC:
        setStatic(STATIC_EDEFAULT);
        return;
      case JniMapPackage.METHOD__PRIVATE:
        setPrivate(PRIVATE_EDEFAULT);
        return;
      case JniMapPackage.METHOD__PROTECTED:
        setProtected(PROTECTED_EDEFAULT);
        return;
      case JniMapPackage.METHOD__RENAMED:
        setRenamed(RENAMED_EDEFAULT);
        return;
      case JniMapPackage.METHOD__NEWNAME:
        setNewname(NEWNAME_EDEFAULT);
        return;
      case JniMapPackage.METHOD__STUB:
        setStub(STUB_EDEFAULT);
        return;
      case JniMapPackage.METHOD__ARG:
        setArg(ARG_EDEFAULT);
        return;
      case JniMapPackage.METHOD__RES:
        setRes((Type)null);
        return;
      case JniMapPackage.METHOD__NAME:
        setName(NAME_EDEFAULT);
        return;
      case JniMapPackage.METHOD__PARAMS:
        getParams().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JniMapPackage.METHOD__CONSTRUCTOR:
        return constructor != CONSTRUCTOR_EDEFAULT;
      case JniMapPackage.METHOD__DESTRUCTOR:
        return destructor != DESTRUCTOR_EDEFAULT;
      case JniMapPackage.METHOD__INSTANCEOF:
        return instanceof_ != INSTANCEOF_EDEFAULT;
      case JniMapPackage.METHOD__STATIC:
        return static_ != STATIC_EDEFAULT;
      case JniMapPackage.METHOD__PRIVATE:
        return private_ != PRIVATE_EDEFAULT;
      case JniMapPackage.METHOD__PROTECTED:
        return protected_ != PROTECTED_EDEFAULT;
      case JniMapPackage.METHOD__RENAMED:
        return renamed != RENAMED_EDEFAULT;
      case JniMapPackage.METHOD__NEWNAME:
        return NEWNAME_EDEFAULT == null ? newname != null : !NEWNAME_EDEFAULT.equals(newname);
      case JniMapPackage.METHOD__STUB:
        return stub != STUB_EDEFAULT;
      case JniMapPackage.METHOD__ARG:
        return arg != ARG_EDEFAULT;
      case JniMapPackage.METHOD__RES:
        return res != null;
      case JniMapPackage.METHOD__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case JniMapPackage.METHOD__PARAMS:
        return params != null && !params.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (constructor: ");
    result.append(constructor);
    result.append(", destructor: ");
    result.append(destructor);
    result.append(", instanceof: ");
    result.append(instanceof_);
    result.append(", static: ");
    result.append(static_);
    result.append(", private: ");
    result.append(private_);
    result.append(", protected: ");
    result.append(protected_);
    result.append(", renamed: ");
    result.append(renamed);
    result.append(", newname: ");
    result.append(newname);
    result.append(", stub: ");
    result.append(stub);
    result.append(", arg: ");
    result.append(arg);
    result.append(", name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //MethodImpl
