/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Library</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.ExternalLibrary#getLibrary <em>Library</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.ExternalLibrary#getMapping <em>Mapping</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getExternalLibrary()
 * @model
 * @generated
 */
public interface ExternalLibrary extends EObject
{
  /**
   * Returns the value of the '<em><b>Library</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Library</em>' reference.
   * @see #setLibrary(Library)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getExternalLibrary_Library()
   * @model
   * @generated
   */
  Library getLibrary();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.ExternalLibrary#getLibrary <em>Library</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Library</em>' reference.
   * @see #getLibrary()
   * @generated
   */
  void setLibrary(Library value);

  /**
   * Returns the value of the '<em><b>Mapping</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mapping</em>' reference.
   * @see #setMapping(Mapping)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getExternalLibrary_Mapping()
   * @model
   * @generated
   */
  Mapping getMapping();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.ExternalLibrary#getMapping <em>Mapping</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Mapping</em>' reference.
   * @see #getMapping()
   * @generated
   */
  void setMapping(Mapping value);

} // ExternalLibrary
