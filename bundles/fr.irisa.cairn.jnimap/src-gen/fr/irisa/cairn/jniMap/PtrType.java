/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ptr Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.PtrType#getBase <em>Base</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.PtrType#getIndir <em>Indir</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.PtrType#isIgnored <em>Ignored</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getPtrType()
 * @model
 * @generated
 */
public interface PtrType extends Type, NestedType
{
  /**
   * Returns the value of the '<em><b>Base</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Base</em>' containment reference.
   * @see #setBase(BaseType)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getPtrType_Base()
   * @model containment="true"
   * @generated
   */
  BaseType getBase();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.PtrType#getBase <em>Base</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Base</em>' containment reference.
   * @see #getBase()
   * @generated
   */
  void setBase(BaseType value);

  /**
   * Returns the value of the '<em><b>Indir</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Indir</em>' attribute list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getPtrType_Indir()
   * @model unique="false"
   * @generated
   */
  EList<String> getIndir();

  /**
   * Returns the value of the '<em><b>Ignored</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ignored</em>' attribute.
   * @see #setIgnored(boolean)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getPtrType_Ignored()
   * @model
   * @generated
   */
  boolean isIgnored();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.PtrType#isIgnored <em>Ignored</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ignored</em>' attribute.
   * @see #isIgnored()
   * @generated
   */
  void setIgnored(boolean value);

} // PtrType
