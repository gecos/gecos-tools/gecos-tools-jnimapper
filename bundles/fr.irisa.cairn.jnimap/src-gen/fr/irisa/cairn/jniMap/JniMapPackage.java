/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.jniMap.JniMapFactory
 * @model kind="package"
 * @generated
 */
public interface JniMapPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "jniMap";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.irisa.fr/cairn/JniMap";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "jniMap";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  JniMapPackage eINSTANCE = fr.irisa.cairn.jniMap.impl.JniMapPackageImpl.init();

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.MappingImpl <em>Mapping</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.MappingImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getMapping()
   * @generated
   */
  int MAPPING = 0;

  /**
   * The feature id for the '<em><b>Imports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING__IMPORTS = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING__NAME = 1;

  /**
   * The feature id for the '<em><b>Package</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING__PACKAGE = 2;

  /**
   * The feature id for the '<em><b>Hosts</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING__HOSTS = 3;

  /**
   * The feature id for the '<em><b>External Libraries</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING__EXTERNAL_LIBRARIES = 4;

  /**
   * The feature id for the '<em><b>Libraries</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING__LIBRARIES = 5;

  /**
   * The feature id for the '<em><b>User Modules</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING__USER_MODULES = 6;

  /**
   * The feature id for the '<em><b>Class Mapping</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING__CLASS_MAPPING = 7;

  /**
   * The feature id for the '<em><b>Includes</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING__INCLUDES = 8;

  /**
   * The feature id for the '<em><b>Method Groups</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING__METHOD_GROUPS = 9;

  /**
   * The number of structural features of the '<em>Mapping</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING_FEATURE_COUNT = 10;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.ImportImpl <em>Import</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.ImportImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getImport()
   * @generated
   */
  int IMPORT = 1;

  /**
   * The feature id for the '<em><b>Import URI</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__IMPORT_URI = 0;

  /**
   * The number of structural features of the '<em>Import</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.ClassMappingImpl <em>Class Mapping</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.ClassMappingImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getClassMapping()
   * @generated
   */
  int CLASS_MAPPING = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_MAPPING__NAME = 0;

  /**
   * The number of structural features of the '<em>Class Mapping</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_MAPPING_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.GenericsDeclarationImpl <em>Generics Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.GenericsDeclarationImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getGenericsDeclaration()
   * @generated
   */
  int GENERICS_DECLARATION = 3;

  /**
   * The feature id for the '<em><b>Names</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERICS_DECLARATION__NAMES = 0;

  /**
   * The number of structural features of the '<em>Generics Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERICS_DECLARATION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.GenericsInstantiationImpl <em>Generics Instantiation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.GenericsInstantiationImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getGenericsInstantiation()
   * @generated
   */
  int GENERICS_INSTANTIATION = 4;

  /**
   * The feature id for the '<em><b>Instance Names</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERICS_INSTANTIATION__INSTANCE_NAMES = 0;

  /**
   * The number of structural features of the '<em>Generics Instantiation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERICS_INSTANTIATION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.InterfaceImplementationImpl <em>Interface Implementation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.InterfaceImplementationImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getInterfaceImplementation()
   * @generated
   */
  int INTERFACE_IMPLEMENTATION = 5;

  /**
   * The feature id for the '<em><b>Interface</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERFACE_IMPLEMENTATION__INTERFACE = 0;

  /**
   * The feature id for the '<em><b>Generics Instantiation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERFACE_IMPLEMENTATION__GENERICS_INSTANTIATION = 1;

  /**
   * The number of structural features of the '<em>Interface Implementation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERFACE_IMPLEMENTATION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.EnumToClassMappingImpl <em>Enum To Class Mapping</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.EnumToClassMappingImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getEnumToClassMapping()
   * @generated
   */
  int ENUM_TO_CLASS_MAPPING = 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_TO_CLASS_MAPPING__NAME = CLASS_MAPPING__NAME;

  /**
   * The feature id for the '<em><b>Enum </b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_TO_CLASS_MAPPING__ENUM_ = CLASS_MAPPING_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Enum To Class Mapping</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_TO_CLASS_MAPPING_FEATURE_COUNT = CLASS_MAPPING_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.StructToClassMappingImpl <em>Struct To Class Mapping</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.StructToClassMappingImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getStructToClassMapping()
   * @generated
   */
  int STRUCT_TO_CLASS_MAPPING = 7;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_TO_CLASS_MAPPING__NAME = CLASS_MAPPING__NAME;

  /**
   * The feature id for the '<em><b>Struct</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_TO_CLASS_MAPPING__STRUCT = CLASS_MAPPING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Super</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_TO_CLASS_MAPPING__SUPER = CLASS_MAPPING_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Struct To Class Mapping</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_TO_CLASS_MAPPING_FEATURE_COUNT = CLASS_MAPPING_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.ClassToClassMappingImpl <em>Class To Class Mapping</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.ClassToClassMappingImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getClassToClassMapping()
   * @generated
   */
  int CLASS_TO_CLASS_MAPPING = 8;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_TO_CLASS_MAPPING__NAME = CLASS_MAPPING__NAME;

  /**
   * The feature id for the '<em><b>Class</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_TO_CLASS_MAPPING__CLASS = CLASS_MAPPING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Super</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_TO_CLASS_MAPPING__SUPER = CLASS_MAPPING_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Class To Class Mapping</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_TO_CLASS_MAPPING_FEATURE_COUNT = CLASS_MAPPING_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.AliasToClassMappingImpl <em>Alias To Class Mapping</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.AliasToClassMappingImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getAliasToClassMapping()
   * @generated
   */
  int ALIAS_TO_CLASS_MAPPING = 9;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIAS_TO_CLASS_MAPPING__NAME = CLASS_MAPPING__NAME;

  /**
   * The feature id for the '<em><b>Alias</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIAS_TO_CLASS_MAPPING__ALIAS = CLASS_MAPPING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Generics Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIAS_TO_CLASS_MAPPING__GENERICS_DECL = CLASS_MAPPING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Super</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIAS_TO_CLASS_MAPPING__SUPER = CLASS_MAPPING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Generics Instantiation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIAS_TO_CLASS_MAPPING__GENERICS_INSTANTIATION = CLASS_MAPPING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Interfaces</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIAS_TO_CLASS_MAPPING__INTERFACES = CLASS_MAPPING_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Alias To Class Mapping</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIAS_TO_CLASS_MAPPING_FEATURE_COUNT = CLASS_MAPPING_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.UnmappedClassImpl <em>Unmapped Class</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.UnmappedClassImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getUnmappedClass()
   * @generated
   */
  int UNMAPPED_CLASS = 10;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNMAPPED_CLASS__NAME = CLASS_MAPPING__NAME;

  /**
   * The feature id for the '<em><b>Abstract</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNMAPPED_CLASS__ABSTRACT = CLASS_MAPPING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Generics Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNMAPPED_CLASS__GENERICS_DECL = CLASS_MAPPING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Super</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNMAPPED_CLASS__SUPER = CLASS_MAPPING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Generics Instantiation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNMAPPED_CLASS__GENERICS_INSTANTIATION = CLASS_MAPPING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Interfaces</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNMAPPED_CLASS__INTERFACES = CLASS_MAPPING_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Unmapped Class</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNMAPPED_CLASS_FEATURE_COUNT = CLASS_MAPPING_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.InterfaceClassImpl <em>Interface Class</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.InterfaceClassImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getInterfaceClass()
   * @generated
   */
  int INTERFACE_CLASS = 11;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERFACE_CLASS__NAME = CLASS_MAPPING__NAME;

  /**
   * The feature id for the '<em><b>Generics Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERFACE_CLASS__GENERICS_DECL = CLASS_MAPPING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Interfaces</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERFACE_CLASS__INTERFACES = CLASS_MAPPING_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Interface Class</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERFACE_CLASS_FEATURE_COUNT = CLASS_MAPPING_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.ExternalLibraryImpl <em>External Library</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.ExternalLibraryImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getExternalLibrary()
   * @generated
   */
  int EXTERNAL_LIBRARY = 12;

  /**
   * The feature id for the '<em><b>Library</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTERNAL_LIBRARY__LIBRARY = 0;

  /**
   * The feature id for the '<em><b>Mapping</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTERNAL_LIBRARY__MAPPING = 1;

  /**
   * The number of structural features of the '<em>External Library</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTERNAL_LIBRARY_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.LibraryImpl <em>Library</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.LibraryImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getLibrary()
   * @generated
   */
  int LIBRARY = 13;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIBRARY__NAME = 0;

  /**
   * The feature id for the '<em><b>Filenames</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIBRARY__FILENAMES = 1;

  /**
   * The number of structural features of the '<em>Library</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIBRARY_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.LibraryFileNameImpl <em>Library File Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.LibraryFileNameImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getLibraryFileName()
   * @generated
   */
  int LIBRARY_FILE_NAME = 14;

  /**
   * The feature id for the '<em><b>Os</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIBRARY_FILE_NAME__OS = 0;

  /**
   * The feature id for the '<em><b>Filename</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIBRARY_FILE_NAME__FILENAME = 1;

  /**
   * The number of structural features of the '<em>Library File Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIBRARY_FILE_NAME_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.IncludeDefImpl <em>Include Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.IncludeDefImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getIncludeDef()
   * @generated
   */
  int INCLUDE_DEF = 15;

  /**
   * The feature id for the '<em><b>Include</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCLUDE_DEF__INCLUDE = 0;

  /**
   * The number of structural features of the '<em>Include Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCLUDE_DEF_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.UserModuleImpl <em>User Module</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.UserModuleImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getUserModule()
   * @generated
   */
  int USER_MODULE = 16;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int USER_MODULE__NAME = 0;

  /**
   * The feature id for the '<em><b>Methods</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int USER_MODULE__METHODS = 1;

  /**
   * The number of structural features of the '<em>User Module</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int USER_MODULE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.CMethodImpl <em>CMethod</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.CMethodImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getCMethod()
   * @generated
   */
  int CMETHOD = 17;

  /**
   * The feature id for the '<em><b>Res</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CMETHOD__RES = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CMETHOD__NAME = 1;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CMETHOD__PARAMS = 2;

  /**
   * The number of structural features of the '<em>CMethod</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CMETHOD_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.TypeDeclarationImpl <em>Type Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.TypeDeclarationImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getTypeDeclaration()
   * @generated
   */
  int TYPE_DECLARATION = 18;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DECLARATION__NAME = 0;

  /**
   * The number of structural features of the '<em>Type Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DECLARATION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.TypeAliasDeclarationImpl <em>Type Alias Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.TypeAliasDeclarationImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getTypeAliasDeclaration()
   * @generated
   */
  int TYPE_ALIAS_DECLARATION = 19;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_ALIAS_DECLARATION__NAME = TYPE_DECLARATION__NAME;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_ALIAS_DECLARATION__TYPE = TYPE_DECLARATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Type Alias Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_ALIAS_DECLARATION_FEATURE_COUNT = TYPE_DECLARATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.StructDeclarationImpl <em>Struct Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.StructDeclarationImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getStructDeclaration()
   * @generated
   */
  int STRUCT_DECLARATION = 20;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_DECLARATION__NAME = TYPE_DECLARATION__NAME;

  /**
   * The feature id for the '<em><b>Fields</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_DECLARATION__FIELDS = TYPE_DECLARATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Struct Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_DECLARATION_FEATURE_COUNT = TYPE_DECLARATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.ClassDeclarationImpl <em>Class Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.ClassDeclarationImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getClassDeclaration()
   * @generated
   */
  int CLASS_DECLARATION = 21;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_DECLARATION__NAME = 0;

  /**
   * The feature id for the '<em><b>Fields</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_DECLARATION__FIELDS = 1;

  /**
   * The number of structural features of the '<em>Class Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_DECLARATION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.EnumDeclarationImpl <em>Enum Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.EnumDeclarationImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getEnumDeclaration()
   * @generated
   */
  int ENUM_DECLARATION = 22;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_DECLARATION__NAME = TYPE_DECLARATION__NAME;

  /**
   * The feature id for the '<em><b>Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_DECLARATION__VALUES = TYPE_DECLARATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Enum Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_DECLARATION_FEATURE_COUNT = TYPE_DECLARATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.ComplexTypeImpl <em>Complex Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.ComplexTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getComplexType()
   * @generated
   */
  int COMPLEX_TYPE = 36;

  /**
   * The number of structural features of the '<em>Complex Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEX_TYPE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.EnumTypeImpl <em>Enum Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.EnumTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getEnumType()
   * @generated
   */
  int ENUM_TYPE = 23;

  /**
   * The feature id for the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_TYPE__NAME = COMPLEX_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Enum Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_TYPE_FEATURE_COUNT = COMPLEX_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.EnumValueImpl <em>Enum Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.EnumValueImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getEnumValue()
   * @generated
   */
  int ENUM_VALUE = 24;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_VALUE__NAME = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_VALUE__VALUE = 1;

  /**
   * The feature id for the '<em><b>Enum Next</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_VALUE__ENUM_NEXT = 2;

  /**
   * The number of structural features of the '<em>Enum Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_VALUE_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.ParamDefImpl <em>Param Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.ParamDefImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getParamDef()
   * @generated
   */
  int PARAM_DEF = 25;

  /**
   * The feature id for the '<em><b>Give</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_DEF__GIVE = 0;

  /**
   * The feature id for the '<em><b>Take</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_DEF__TAKE = 1;

  /**
   * The feature id for the '<em><b>Keep</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_DEF__KEEP = 2;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_DEF__TYPE = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_DEF__NAME = 4;

  /**
   * The number of structural features of the '<em>Param Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_DEF_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.MethodGroupImpl <em>Method Group</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.MethodGroupImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getMethodGroup()
   * @generated
   */
  int METHOD_GROUP = 26;

  /**
   * The feature id for the '<em><b>Class</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_GROUP__CLASS = 0;

  /**
   * The feature id for the '<em><b>Methods</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_GROUP__METHODS = 1;

  /**
   * The number of structural features of the '<em>Method Group</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_GROUP_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.MethodImpl <em>Method</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.MethodImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getMethod()
   * @generated
   */
  int METHOD = 27;

  /**
   * The feature id for the '<em><b>Constructor</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__CONSTRUCTOR = 0;

  /**
   * The feature id for the '<em><b>Destructor</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__DESTRUCTOR = 1;

  /**
   * The feature id for the '<em><b>Instanceof</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__INSTANCEOF = 2;

  /**
   * The feature id for the '<em><b>Static</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__STATIC = 3;

  /**
   * The feature id for the '<em><b>Private</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__PRIVATE = 4;

  /**
   * The feature id for the '<em><b>Protected</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__PROTECTED = 5;

  /**
   * The feature id for the '<em><b>Renamed</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__RENAMED = 6;

  /**
   * The feature id for the '<em><b>Newname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__NEWNAME = 7;

  /**
   * The feature id for the '<em><b>Stub</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__STUB = 8;

  /**
   * The feature id for the '<em><b>Arg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__ARG = 9;

  /**
   * The feature id for the '<em><b>Res</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__RES = 10;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__NAME = 11;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__PARAMS = 12;

  /**
   * The number of structural features of the '<em>Method</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_FEATURE_COUNT = 13;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.TypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.TypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getType()
   * @generated
   */
  int TYPE = 28;

  /**
   * The number of structural features of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.BaseTypeImpl <em>Base Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.BaseTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getBaseType()
   * @generated
   */
  int BASE_TYPE = 44;

  /**
   * The number of structural features of the '<em>Base Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.BuiltInTypeImpl <em>Built In Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.BuiltInTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getBuiltInType()
   * @generated
   */
  int BUILT_IN_TYPE = 29;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BUILT_IN_TYPE__NAME = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Built In Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BUILT_IN_TYPE_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.BooleanTypeImpl <em>Boolean Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.BooleanTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getBooleanType()
   * @generated
   */
  int BOOLEAN_TYPE = 30;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_TYPE__NAME = BUILT_IN_TYPE__NAME;

  /**
   * The number of structural features of the '<em>Boolean Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_TYPE_FEATURE_COUNT = BUILT_IN_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.IntegerTypeImpl <em>Integer Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.IntegerTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getIntegerType()
   * @generated
   */
  int INTEGER_TYPE = 31;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TYPE__NAME = BUILT_IN_TYPE__NAME;

  /**
   * The number of structural features of the '<em>Integer Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TYPE_FEATURE_COUNT = BUILT_IN_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.RealTypeImpl <em>Real Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.RealTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getRealType()
   * @generated
   */
  int REAL_TYPE = 32;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_TYPE__NAME = BUILT_IN_TYPE__NAME;

  /**
   * The number of structural features of the '<em>Real Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_TYPE_FEATURE_COUNT = BUILT_IN_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.StringTypeImpl <em>String Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.StringTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getStringType()
   * @generated
   */
  int STRING_TYPE = 33;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_TYPE__NAME = BUILT_IN_TYPE__NAME;

  /**
   * The number of structural features of the '<em>String Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_TYPE_FEATURE_COUNT = BUILT_IN_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.VoidTypeImpl <em>Void Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.VoidTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getVoidType()
   * @generated
   */
  int VOID_TYPE = 34;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VOID_TYPE__NAME = BUILT_IN_TYPE__NAME;

  /**
   * The number of structural features of the '<em>Void Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VOID_TYPE_FEATURE_COUNT = BUILT_IN_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.NestedTypeImpl <em>Nested Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.NestedTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getNestedType()
   * @generated
   */
  int NESTED_TYPE = 35;

  /**
   * The number of structural features of the '<em>Nested Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_TYPE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.TypeAliasImpl <em>Type Alias</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.TypeAliasImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getTypeAlias()
   * @generated
   */
  int TYPE_ALIAS = 37;

  /**
   * The feature id for the '<em><b>Alias</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_ALIAS__ALIAS = COMPLEX_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Type Alias</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_ALIAS_FEATURE_COUNT = COMPLEX_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.ArrayTypeImpl <em>Array Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.ArrayTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getArrayType()
   * @generated
   */
  int ARRAY_TYPE = 38;

  /**
   * The feature id for the '<em><b>Base</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_TYPE__BASE = TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Array Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.PtrTypeImpl <em>Ptr Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.PtrTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getPtrType()
   * @generated
   */
  int PTR_TYPE = 39;

  /**
   * The feature id for the '<em><b>Base</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PTR_TYPE__BASE = TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Indir</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PTR_TYPE__INDIR = TYPE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Ignored</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PTR_TYPE__IGNORED = TYPE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Ptr Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PTR_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.ModifiableTypeImpl <em>Modifiable Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.ModifiableTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getModifiableType()
   * @generated
   */
  int MODIFIABLE_TYPE = 40;

  /**
   * The feature id for the '<em><b>Base</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFIABLE_TYPE__BASE = TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Indir</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFIABLE_TYPE__INDIR = TYPE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Modifiable Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFIABLE_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.StructTypeImpl <em>Struct Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.StructTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getStructType()
   * @generated
   */
  int STRUCT_TYPE = 41;

  /**
   * The feature id for the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_TYPE__NAME = COMPLEX_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Struct Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_TYPE_FEATURE_COUNT = COMPLEX_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.FieldDefImpl <em>Field Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.FieldDefImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getFieldDef()
   * @generated
   */
  int FIELD_DEF = 42;

  /**
   * The feature id for the '<em><b>Copy</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_DEF__COPY = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_DEF__TYPE = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_DEF__NAME = 2;

  /**
   * The feature id for the '<em><b>Many</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_DEF__MANY = 3;

  /**
   * The number of structural features of the '<em>Field Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_DEF_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.impl.ConstTypeImpl <em>Const Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.impl.ConstTypeImpl
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getConstType()
   * @generated
   */
  int CONST_TYPE = 43;

  /**
   * The feature id for the '<em><b>Base</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONST_TYPE__BASE = TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Const Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONST_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.jniMap.HostType <em>Host Type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.jniMap.HostType
   * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getHostType()
   * @generated
   */
  int HOST_TYPE = 45;


  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.Mapping <em>Mapping</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mapping</em>'.
   * @see fr.irisa.cairn.jniMap.Mapping
   * @generated
   */
  EClass getMapping();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.Mapping#getImports <em>Imports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Imports</em>'.
   * @see fr.irisa.cairn.jniMap.Mapping#getImports()
   * @see #getMapping()
   * @generated
   */
  EReference getMapping_Imports();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Mapping#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.Mapping#getName()
   * @see #getMapping()
   * @generated
   */
  EAttribute getMapping_Name();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Mapping#getPackage <em>Package</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Package</em>'.
   * @see fr.irisa.cairn.jniMap.Mapping#getPackage()
   * @see #getMapping()
   * @generated
   */
  EAttribute getMapping_Package();

  /**
   * Returns the meta object for the attribute list '{@link fr.irisa.cairn.jniMap.Mapping#getHosts <em>Hosts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Hosts</em>'.
   * @see fr.irisa.cairn.jniMap.Mapping#getHosts()
   * @see #getMapping()
   * @generated
   */
  EAttribute getMapping_Hosts();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.Mapping#getExternalLibraries <em>External Libraries</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>External Libraries</em>'.
   * @see fr.irisa.cairn.jniMap.Mapping#getExternalLibraries()
   * @see #getMapping()
   * @generated
   */
  EReference getMapping_ExternalLibraries();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.Mapping#getLibraries <em>Libraries</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Libraries</em>'.
   * @see fr.irisa.cairn.jniMap.Mapping#getLibraries()
   * @see #getMapping()
   * @generated
   */
  EReference getMapping_Libraries();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.Mapping#getUserModules <em>User Modules</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>User Modules</em>'.
   * @see fr.irisa.cairn.jniMap.Mapping#getUserModules()
   * @see #getMapping()
   * @generated
   */
  EReference getMapping_UserModules();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.Mapping#getClassMapping <em>Class Mapping</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Class Mapping</em>'.
   * @see fr.irisa.cairn.jniMap.Mapping#getClassMapping()
   * @see #getMapping()
   * @generated
   */
  EReference getMapping_ClassMapping();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.Mapping#getIncludes <em>Includes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Includes</em>'.
   * @see fr.irisa.cairn.jniMap.Mapping#getIncludes()
   * @see #getMapping()
   * @generated
   */
  EReference getMapping_Includes();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.Mapping#getMethodGroups <em>Method Groups</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Method Groups</em>'.
   * @see fr.irisa.cairn.jniMap.Mapping#getMethodGroups()
   * @see #getMapping()
   * @generated
   */
  EReference getMapping_MethodGroups();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.Import <em>Import</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import</em>'.
   * @see fr.irisa.cairn.jniMap.Import
   * @generated
   */
  EClass getImport();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Import#getImportURI <em>Import URI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Import URI</em>'.
   * @see fr.irisa.cairn.jniMap.Import#getImportURI()
   * @see #getImport()
   * @generated
   */
  EAttribute getImport_ImportURI();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.ClassMapping <em>Class Mapping</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Class Mapping</em>'.
   * @see fr.irisa.cairn.jniMap.ClassMapping
   * @generated
   */
  EClass getClassMapping();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.ClassMapping#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.ClassMapping#getName()
   * @see #getClassMapping()
   * @generated
   */
  EAttribute getClassMapping_Name();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.GenericsDeclaration <em>Generics Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generics Declaration</em>'.
   * @see fr.irisa.cairn.jniMap.GenericsDeclaration
   * @generated
   */
  EClass getGenericsDeclaration();

  /**
   * Returns the meta object for the attribute list '{@link fr.irisa.cairn.jniMap.GenericsDeclaration#getNames <em>Names</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Names</em>'.
   * @see fr.irisa.cairn.jniMap.GenericsDeclaration#getNames()
   * @see #getGenericsDeclaration()
   * @generated
   */
  EAttribute getGenericsDeclaration_Names();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.GenericsInstantiation <em>Generics Instantiation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generics Instantiation</em>'.
   * @see fr.irisa.cairn.jniMap.GenericsInstantiation
   * @generated
   */
  EClass getGenericsInstantiation();

  /**
   * Returns the meta object for the attribute list '{@link fr.irisa.cairn.jniMap.GenericsInstantiation#getInstanceNames <em>Instance Names</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Instance Names</em>'.
   * @see fr.irisa.cairn.jniMap.GenericsInstantiation#getInstanceNames()
   * @see #getGenericsInstantiation()
   * @generated
   */
  EAttribute getGenericsInstantiation_InstanceNames();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.InterfaceImplementation <em>Interface Implementation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Interface Implementation</em>'.
   * @see fr.irisa.cairn.jniMap.InterfaceImplementation
   * @generated
   */
  EClass getInterfaceImplementation();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.jniMap.InterfaceImplementation#getInterface <em>Interface</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Interface</em>'.
   * @see fr.irisa.cairn.jniMap.InterfaceImplementation#getInterface()
   * @see #getInterfaceImplementation()
   * @generated
   */
  EReference getInterfaceImplementation_Interface();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.InterfaceImplementation#getGenericsInstantiation <em>Generics Instantiation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Generics Instantiation</em>'.
   * @see fr.irisa.cairn.jniMap.InterfaceImplementation#getGenericsInstantiation()
   * @see #getInterfaceImplementation()
   * @generated
   */
  EReference getInterfaceImplementation_GenericsInstantiation();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.EnumToClassMapping <em>Enum To Class Mapping</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enum To Class Mapping</em>'.
   * @see fr.irisa.cairn.jniMap.EnumToClassMapping
   * @generated
   */
  EClass getEnumToClassMapping();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.EnumToClassMapping#getEnum_ <em>Enum </em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Enum </em>'.
   * @see fr.irisa.cairn.jniMap.EnumToClassMapping#getEnum_()
   * @see #getEnumToClassMapping()
   * @generated
   */
  EReference getEnumToClassMapping_Enum_();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.StructToClassMapping <em>Struct To Class Mapping</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Struct To Class Mapping</em>'.
   * @see fr.irisa.cairn.jniMap.StructToClassMapping
   * @generated
   */
  EClass getStructToClassMapping();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.StructToClassMapping#getStruct <em>Struct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Struct</em>'.
   * @see fr.irisa.cairn.jniMap.StructToClassMapping#getStruct()
   * @see #getStructToClassMapping()
   * @generated
   */
  EReference getStructToClassMapping_Struct();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.jniMap.StructToClassMapping#getSuper <em>Super</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Super</em>'.
   * @see fr.irisa.cairn.jniMap.StructToClassMapping#getSuper()
   * @see #getStructToClassMapping()
   * @generated
   */
  EReference getStructToClassMapping_Super();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.ClassToClassMapping <em>Class To Class Mapping</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Class To Class Mapping</em>'.
   * @see fr.irisa.cairn.jniMap.ClassToClassMapping
   * @generated
   */
  EClass getClassToClassMapping();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.ClassToClassMapping#getClass_ <em>Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Class</em>'.
   * @see fr.irisa.cairn.jniMap.ClassToClassMapping#getClass_()
   * @see #getClassToClassMapping()
   * @generated
   */
  EReference getClassToClassMapping_Class();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.jniMap.ClassToClassMapping#getSuper <em>Super</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Super</em>'.
   * @see fr.irisa.cairn.jniMap.ClassToClassMapping#getSuper()
   * @see #getClassToClassMapping()
   * @generated
   */
  EReference getClassToClassMapping_Super();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.AliasToClassMapping <em>Alias To Class Mapping</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Alias To Class Mapping</em>'.
   * @see fr.irisa.cairn.jniMap.AliasToClassMapping
   * @generated
   */
  EClass getAliasToClassMapping();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getAlias <em>Alias</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Alias</em>'.
   * @see fr.irisa.cairn.jniMap.AliasToClassMapping#getAlias()
   * @see #getAliasToClassMapping()
   * @generated
   */
  EReference getAliasToClassMapping_Alias();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getGenericsDecl <em>Generics Decl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Generics Decl</em>'.
   * @see fr.irisa.cairn.jniMap.AliasToClassMapping#getGenericsDecl()
   * @see #getAliasToClassMapping()
   * @generated
   */
  EReference getAliasToClassMapping_GenericsDecl();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getSuper <em>Super</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Super</em>'.
   * @see fr.irisa.cairn.jniMap.AliasToClassMapping#getSuper()
   * @see #getAliasToClassMapping()
   * @generated
   */
  EReference getAliasToClassMapping_Super();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getGenericsInstantiation <em>Generics Instantiation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Generics Instantiation</em>'.
   * @see fr.irisa.cairn.jniMap.AliasToClassMapping#getGenericsInstantiation()
   * @see #getAliasToClassMapping()
   * @generated
   */
  EReference getAliasToClassMapping_GenericsInstantiation();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getInterfaces <em>Interfaces</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Interfaces</em>'.
   * @see fr.irisa.cairn.jniMap.AliasToClassMapping#getInterfaces()
   * @see #getAliasToClassMapping()
   * @generated
   */
  EReference getAliasToClassMapping_Interfaces();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.UnmappedClass <em>Unmapped Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unmapped Class</em>'.
   * @see fr.irisa.cairn.jniMap.UnmappedClass
   * @generated
   */
  EClass getUnmappedClass();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.UnmappedClass#isAbstract <em>Abstract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Abstract</em>'.
   * @see fr.irisa.cairn.jniMap.UnmappedClass#isAbstract()
   * @see #getUnmappedClass()
   * @generated
   */
  EAttribute getUnmappedClass_Abstract();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.UnmappedClass#getGenericsDecl <em>Generics Decl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Generics Decl</em>'.
   * @see fr.irisa.cairn.jniMap.UnmappedClass#getGenericsDecl()
   * @see #getUnmappedClass()
   * @generated
   */
  EReference getUnmappedClass_GenericsDecl();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.jniMap.UnmappedClass#getSuper <em>Super</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Super</em>'.
   * @see fr.irisa.cairn.jniMap.UnmappedClass#getSuper()
   * @see #getUnmappedClass()
   * @generated
   */
  EReference getUnmappedClass_Super();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.UnmappedClass#getGenericsInstantiation <em>Generics Instantiation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Generics Instantiation</em>'.
   * @see fr.irisa.cairn.jniMap.UnmappedClass#getGenericsInstantiation()
   * @see #getUnmappedClass()
   * @generated
   */
  EReference getUnmappedClass_GenericsInstantiation();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.UnmappedClass#getInterfaces <em>Interfaces</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Interfaces</em>'.
   * @see fr.irisa.cairn.jniMap.UnmappedClass#getInterfaces()
   * @see #getUnmappedClass()
   * @generated
   */
  EReference getUnmappedClass_Interfaces();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.InterfaceClass <em>Interface Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Interface Class</em>'.
   * @see fr.irisa.cairn.jniMap.InterfaceClass
   * @generated
   */
  EClass getInterfaceClass();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.InterfaceClass#getGenericsDecl <em>Generics Decl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Generics Decl</em>'.
   * @see fr.irisa.cairn.jniMap.InterfaceClass#getGenericsDecl()
   * @see #getInterfaceClass()
   * @generated
   */
  EReference getInterfaceClass_GenericsDecl();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.InterfaceClass#getInterfaces <em>Interfaces</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Interfaces</em>'.
   * @see fr.irisa.cairn.jniMap.InterfaceClass#getInterfaces()
   * @see #getInterfaceClass()
   * @generated
   */
  EReference getInterfaceClass_Interfaces();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.ExternalLibrary <em>External Library</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>External Library</em>'.
   * @see fr.irisa.cairn.jniMap.ExternalLibrary
   * @generated
   */
  EClass getExternalLibrary();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.jniMap.ExternalLibrary#getLibrary <em>Library</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Library</em>'.
   * @see fr.irisa.cairn.jniMap.ExternalLibrary#getLibrary()
   * @see #getExternalLibrary()
   * @generated
   */
  EReference getExternalLibrary_Library();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.jniMap.ExternalLibrary#getMapping <em>Mapping</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Mapping</em>'.
   * @see fr.irisa.cairn.jniMap.ExternalLibrary#getMapping()
   * @see #getExternalLibrary()
   * @generated
   */
  EReference getExternalLibrary_Mapping();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.Library <em>Library</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Library</em>'.
   * @see fr.irisa.cairn.jniMap.Library
   * @generated
   */
  EClass getLibrary();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Library#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.Library#getName()
   * @see #getLibrary()
   * @generated
   */
  EAttribute getLibrary_Name();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.Library#getFilenames <em>Filenames</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Filenames</em>'.
   * @see fr.irisa.cairn.jniMap.Library#getFilenames()
   * @see #getLibrary()
   * @generated
   */
  EReference getLibrary_Filenames();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.LibraryFileName <em>Library File Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Library File Name</em>'.
   * @see fr.irisa.cairn.jniMap.LibraryFileName
   * @generated
   */
  EClass getLibraryFileName();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.LibraryFileName#getOs <em>Os</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Os</em>'.
   * @see fr.irisa.cairn.jniMap.LibraryFileName#getOs()
   * @see #getLibraryFileName()
   * @generated
   */
  EAttribute getLibraryFileName_Os();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.LibraryFileName#getFilename <em>Filename</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Filename</em>'.
   * @see fr.irisa.cairn.jniMap.LibraryFileName#getFilename()
   * @see #getLibraryFileName()
   * @generated
   */
  EAttribute getLibraryFileName_Filename();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.IncludeDef <em>Include Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Include Def</em>'.
   * @see fr.irisa.cairn.jniMap.IncludeDef
   * @generated
   */
  EClass getIncludeDef();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.IncludeDef#getInclude <em>Include</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Include</em>'.
   * @see fr.irisa.cairn.jniMap.IncludeDef#getInclude()
   * @see #getIncludeDef()
   * @generated
   */
  EAttribute getIncludeDef_Include();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.UserModule <em>User Module</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>User Module</em>'.
   * @see fr.irisa.cairn.jniMap.UserModule
   * @generated
   */
  EClass getUserModule();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.UserModule#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.UserModule#getName()
   * @see #getUserModule()
   * @generated
   */
  EAttribute getUserModule_Name();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.UserModule#getMethods <em>Methods</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Methods</em>'.
   * @see fr.irisa.cairn.jniMap.UserModule#getMethods()
   * @see #getUserModule()
   * @generated
   */
  EReference getUserModule_Methods();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.CMethod <em>CMethod</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>CMethod</em>'.
   * @see fr.irisa.cairn.jniMap.CMethod
   * @generated
   */
  EClass getCMethod();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.CMethod#getRes <em>Res</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Res</em>'.
   * @see fr.irisa.cairn.jniMap.CMethod#getRes()
   * @see #getCMethod()
   * @generated
   */
  EReference getCMethod_Res();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.CMethod#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.CMethod#getName()
   * @see #getCMethod()
   * @generated
   */
  EAttribute getCMethod_Name();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.CMethod#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Params</em>'.
   * @see fr.irisa.cairn.jniMap.CMethod#getParams()
   * @see #getCMethod()
   * @generated
   */
  EReference getCMethod_Params();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.TypeDeclaration <em>Type Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Declaration</em>'.
   * @see fr.irisa.cairn.jniMap.TypeDeclaration
   * @generated
   */
  EClass getTypeDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.TypeDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.TypeDeclaration#getName()
   * @see #getTypeDeclaration()
   * @generated
   */
  EAttribute getTypeDeclaration_Name();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.TypeAliasDeclaration <em>Type Alias Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Alias Declaration</em>'.
   * @see fr.irisa.cairn.jniMap.TypeAliasDeclaration
   * @generated
   */
  EClass getTypeAliasDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.TypeAliasDeclaration#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irisa.cairn.jniMap.TypeAliasDeclaration#getType()
   * @see #getTypeAliasDeclaration()
   * @generated
   */
  EReference getTypeAliasDeclaration_Type();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.StructDeclaration <em>Struct Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Struct Declaration</em>'.
   * @see fr.irisa.cairn.jniMap.StructDeclaration
   * @generated
   */
  EClass getStructDeclaration();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.StructDeclaration#getFields <em>Fields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Fields</em>'.
   * @see fr.irisa.cairn.jniMap.StructDeclaration#getFields()
   * @see #getStructDeclaration()
   * @generated
   */
  EReference getStructDeclaration_Fields();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.ClassDeclaration <em>Class Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Class Declaration</em>'.
   * @see fr.irisa.cairn.jniMap.ClassDeclaration
   * @generated
   */
  EClass getClassDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.ClassDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.ClassDeclaration#getName()
   * @see #getClassDeclaration()
   * @generated
   */
  EAttribute getClassDeclaration_Name();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.ClassDeclaration#getFields <em>Fields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Fields</em>'.
   * @see fr.irisa.cairn.jniMap.ClassDeclaration#getFields()
   * @see #getClassDeclaration()
   * @generated
   */
  EReference getClassDeclaration_Fields();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.EnumDeclaration <em>Enum Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enum Declaration</em>'.
   * @see fr.irisa.cairn.jniMap.EnumDeclaration
   * @generated
   */
  EClass getEnumDeclaration();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.EnumDeclaration#getValues <em>Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Values</em>'.
   * @see fr.irisa.cairn.jniMap.EnumDeclaration#getValues()
   * @see #getEnumDeclaration()
   * @generated
   */
  EReference getEnumDeclaration_Values();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.EnumType <em>Enum Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enum Type</em>'.
   * @see fr.irisa.cairn.jniMap.EnumType
   * @generated
   */
  EClass getEnumType();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.jniMap.EnumType#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.EnumType#getName()
   * @see #getEnumType()
   * @generated
   */
  EReference getEnumType_Name();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.EnumValue <em>Enum Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enum Value</em>'.
   * @see fr.irisa.cairn.jniMap.EnumValue
   * @generated
   */
  EClass getEnumValue();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.EnumValue#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.EnumValue#getName()
   * @see #getEnumValue()
   * @generated
   */
  EAttribute getEnumValue_Name();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.EnumValue#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irisa.cairn.jniMap.EnumValue#getValue()
   * @see #getEnumValue()
   * @generated
   */
  EAttribute getEnumValue_Value();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.EnumValue#getEnumNext <em>Enum Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Enum Next</em>'.
   * @see fr.irisa.cairn.jniMap.EnumValue#getEnumNext()
   * @see #getEnumValue()
   * @generated
   */
  EReference getEnumValue_EnumNext();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.ParamDef <em>Param Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Param Def</em>'.
   * @see fr.irisa.cairn.jniMap.ParamDef
   * @generated
   */
  EClass getParamDef();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.ParamDef#isGive <em>Give</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Give</em>'.
   * @see fr.irisa.cairn.jniMap.ParamDef#isGive()
   * @see #getParamDef()
   * @generated
   */
  EAttribute getParamDef_Give();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.ParamDef#isTake <em>Take</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Take</em>'.
   * @see fr.irisa.cairn.jniMap.ParamDef#isTake()
   * @see #getParamDef()
   * @generated
   */
  EAttribute getParamDef_Take();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.ParamDef#isKeep <em>Keep</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Keep</em>'.
   * @see fr.irisa.cairn.jniMap.ParamDef#isKeep()
   * @see #getParamDef()
   * @generated
   */
  EAttribute getParamDef_Keep();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.ParamDef#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irisa.cairn.jniMap.ParamDef#getType()
   * @see #getParamDef()
   * @generated
   */
  EReference getParamDef_Type();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.ParamDef#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.ParamDef#getName()
   * @see #getParamDef()
   * @generated
   */
  EAttribute getParamDef_Name();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.MethodGroup <em>Method Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Method Group</em>'.
   * @see fr.irisa.cairn.jniMap.MethodGroup
   * @generated
   */
  EClass getMethodGroup();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.jniMap.MethodGroup#getClass_ <em>Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Class</em>'.
   * @see fr.irisa.cairn.jniMap.MethodGroup#getClass_()
   * @see #getMethodGroup()
   * @generated
   */
  EReference getMethodGroup_Class();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.MethodGroup#getMethods <em>Methods</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Methods</em>'.
   * @see fr.irisa.cairn.jniMap.MethodGroup#getMethods()
   * @see #getMethodGroup()
   * @generated
   */
  EReference getMethodGroup_Methods();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.Method <em>Method</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Method</em>'.
   * @see fr.irisa.cairn.jniMap.Method
   * @generated
   */
  EClass getMethod();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Method#isConstructor <em>Constructor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Constructor</em>'.
   * @see fr.irisa.cairn.jniMap.Method#isConstructor()
   * @see #getMethod()
   * @generated
   */
  EAttribute getMethod_Constructor();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Method#isDestructor <em>Destructor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Destructor</em>'.
   * @see fr.irisa.cairn.jniMap.Method#isDestructor()
   * @see #getMethod()
   * @generated
   */
  EAttribute getMethod_Destructor();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Method#isInstanceof <em>Instanceof</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Instanceof</em>'.
   * @see fr.irisa.cairn.jniMap.Method#isInstanceof()
   * @see #getMethod()
   * @generated
   */
  EAttribute getMethod_Instanceof();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Method#isStatic <em>Static</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Static</em>'.
   * @see fr.irisa.cairn.jniMap.Method#isStatic()
   * @see #getMethod()
   * @generated
   */
  EAttribute getMethod_Static();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Method#isPrivate <em>Private</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Private</em>'.
   * @see fr.irisa.cairn.jniMap.Method#isPrivate()
   * @see #getMethod()
   * @generated
   */
  EAttribute getMethod_Private();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Method#isProtected <em>Protected</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Protected</em>'.
   * @see fr.irisa.cairn.jniMap.Method#isProtected()
   * @see #getMethod()
   * @generated
   */
  EAttribute getMethod_Protected();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Method#isRenamed <em>Renamed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Renamed</em>'.
   * @see fr.irisa.cairn.jniMap.Method#isRenamed()
   * @see #getMethod()
   * @generated
   */
  EAttribute getMethod_Renamed();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Method#getNewname <em>Newname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Newname</em>'.
   * @see fr.irisa.cairn.jniMap.Method#getNewname()
   * @see #getMethod()
   * @generated
   */
  EAttribute getMethod_Newname();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Method#isStub <em>Stub</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Stub</em>'.
   * @see fr.irisa.cairn.jniMap.Method#isStub()
   * @see #getMethod()
   * @generated
   */
  EAttribute getMethod_Stub();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Method#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Arg</em>'.
   * @see fr.irisa.cairn.jniMap.Method#getArg()
   * @see #getMethod()
   * @generated
   */
  EAttribute getMethod_Arg();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.Method#getRes <em>Res</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Res</em>'.
   * @see fr.irisa.cairn.jniMap.Method#getRes()
   * @see #getMethod()
   * @generated
   */
  EReference getMethod_Res();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.Method#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.Method#getName()
   * @see #getMethod()
   * @generated
   */
  EAttribute getMethod_Name();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.jniMap.Method#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Params</em>'.
   * @see fr.irisa.cairn.jniMap.Method#getParams()
   * @see #getMethod()
   * @generated
   */
  EReference getMethod_Params();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see fr.irisa.cairn.jniMap.Type
   * @generated
   */
  EClass getType();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.BuiltInType <em>Built In Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Built In Type</em>'.
   * @see fr.irisa.cairn.jniMap.BuiltInType
   * @generated
   */
  EClass getBuiltInType();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.BuiltInType#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.BuiltInType#getName()
   * @see #getBuiltInType()
   * @generated
   */
  EAttribute getBuiltInType_Name();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.BooleanType <em>Boolean Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Type</em>'.
   * @see fr.irisa.cairn.jniMap.BooleanType
   * @generated
   */
  EClass getBooleanType();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.IntegerType <em>Integer Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Type</em>'.
   * @see fr.irisa.cairn.jniMap.IntegerType
   * @generated
   */
  EClass getIntegerType();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.RealType <em>Real Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Real Type</em>'.
   * @see fr.irisa.cairn.jniMap.RealType
   * @generated
   */
  EClass getRealType();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.StringType <em>String Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>String Type</em>'.
   * @see fr.irisa.cairn.jniMap.StringType
   * @generated
   */
  EClass getStringType();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.VoidType <em>Void Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Void Type</em>'.
   * @see fr.irisa.cairn.jniMap.VoidType
   * @generated
   */
  EClass getVoidType();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.NestedType <em>Nested Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Nested Type</em>'.
   * @see fr.irisa.cairn.jniMap.NestedType
   * @generated
   */
  EClass getNestedType();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.ComplexType <em>Complex Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Complex Type</em>'.
   * @see fr.irisa.cairn.jniMap.ComplexType
   * @generated
   */
  EClass getComplexType();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.TypeAlias <em>Type Alias</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Alias</em>'.
   * @see fr.irisa.cairn.jniMap.TypeAlias
   * @generated
   */
  EClass getTypeAlias();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.jniMap.TypeAlias#getAlias <em>Alias</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Alias</em>'.
   * @see fr.irisa.cairn.jniMap.TypeAlias#getAlias()
   * @see #getTypeAlias()
   * @generated
   */
  EReference getTypeAlias_Alias();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.ArrayType <em>Array Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Type</em>'.
   * @see fr.irisa.cairn.jniMap.ArrayType
   * @generated
   */
  EClass getArrayType();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.ArrayType#getBase <em>Base</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Base</em>'.
   * @see fr.irisa.cairn.jniMap.ArrayType#getBase()
   * @see #getArrayType()
   * @generated
   */
  EReference getArrayType_Base();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.PtrType <em>Ptr Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ptr Type</em>'.
   * @see fr.irisa.cairn.jniMap.PtrType
   * @generated
   */
  EClass getPtrType();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.PtrType#getBase <em>Base</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Base</em>'.
   * @see fr.irisa.cairn.jniMap.PtrType#getBase()
   * @see #getPtrType()
   * @generated
   */
  EReference getPtrType_Base();

  /**
   * Returns the meta object for the attribute list '{@link fr.irisa.cairn.jniMap.PtrType#getIndir <em>Indir</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Indir</em>'.
   * @see fr.irisa.cairn.jniMap.PtrType#getIndir()
   * @see #getPtrType()
   * @generated
   */
  EAttribute getPtrType_Indir();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.PtrType#isIgnored <em>Ignored</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ignored</em>'.
   * @see fr.irisa.cairn.jniMap.PtrType#isIgnored()
   * @see #getPtrType()
   * @generated
   */
  EAttribute getPtrType_Ignored();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.ModifiableType <em>Modifiable Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Modifiable Type</em>'.
   * @see fr.irisa.cairn.jniMap.ModifiableType
   * @generated
   */
  EClass getModifiableType();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.ModifiableType#getBase <em>Base</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Base</em>'.
   * @see fr.irisa.cairn.jniMap.ModifiableType#getBase()
   * @see #getModifiableType()
   * @generated
   */
  EReference getModifiableType_Base();

  /**
   * Returns the meta object for the attribute list '{@link fr.irisa.cairn.jniMap.ModifiableType#getIndir <em>Indir</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Indir</em>'.
   * @see fr.irisa.cairn.jniMap.ModifiableType#getIndir()
   * @see #getModifiableType()
   * @generated
   */
  EAttribute getModifiableType_Indir();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.StructType <em>Struct Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Struct Type</em>'.
   * @see fr.irisa.cairn.jniMap.StructType
   * @generated
   */
  EClass getStructType();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.jniMap.StructType#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.StructType#getName()
   * @see #getStructType()
   * @generated
   */
  EReference getStructType_Name();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.FieldDef <em>Field Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Def</em>'.
   * @see fr.irisa.cairn.jniMap.FieldDef
   * @generated
   */
  EClass getFieldDef();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.FieldDef#isCopy <em>Copy</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Copy</em>'.
   * @see fr.irisa.cairn.jniMap.FieldDef#isCopy()
   * @see #getFieldDef()
   * @generated
   */
  EAttribute getFieldDef_Copy();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.FieldDef#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irisa.cairn.jniMap.FieldDef#getType()
   * @see #getFieldDef()
   * @generated
   */
  EReference getFieldDef_Type();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.FieldDef#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.jniMap.FieldDef#getName()
   * @see #getFieldDef()
   * @generated
   */
  EAttribute getFieldDef_Name();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.jniMap.FieldDef#isMany <em>Many</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Many</em>'.
   * @see fr.irisa.cairn.jniMap.FieldDef#isMany()
   * @see #getFieldDef()
   * @generated
   */
  EAttribute getFieldDef_Many();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.ConstType <em>Const Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Const Type</em>'.
   * @see fr.irisa.cairn.jniMap.ConstType
   * @generated
   */
  EClass getConstType();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.jniMap.ConstType#getBase <em>Base</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Base</em>'.
   * @see fr.irisa.cairn.jniMap.ConstType#getBase()
   * @see #getConstType()
   * @generated
   */
  EReference getConstType_Base();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.jniMap.BaseType <em>Base Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type</em>'.
   * @see fr.irisa.cairn.jniMap.BaseType
   * @generated
   */
  EClass getBaseType();

  /**
   * Returns the meta object for enum '{@link fr.irisa.cairn.jniMap.HostType <em>Host Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Host Type</em>'.
   * @see fr.irisa.cairn.jniMap.HostType
   * @generated
   */
  EEnum getHostType();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  JniMapFactory getJniMapFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.MappingImpl <em>Mapping</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.MappingImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getMapping()
     * @generated
     */
    EClass MAPPING = eINSTANCE.getMapping();

    /**
     * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAPPING__IMPORTS = eINSTANCE.getMapping_Imports();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MAPPING__NAME = eINSTANCE.getMapping_Name();

    /**
     * The meta object literal for the '<em><b>Package</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MAPPING__PACKAGE = eINSTANCE.getMapping_Package();

    /**
     * The meta object literal for the '<em><b>Hosts</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MAPPING__HOSTS = eINSTANCE.getMapping_Hosts();

    /**
     * The meta object literal for the '<em><b>External Libraries</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAPPING__EXTERNAL_LIBRARIES = eINSTANCE.getMapping_ExternalLibraries();

    /**
     * The meta object literal for the '<em><b>Libraries</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAPPING__LIBRARIES = eINSTANCE.getMapping_Libraries();

    /**
     * The meta object literal for the '<em><b>User Modules</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAPPING__USER_MODULES = eINSTANCE.getMapping_UserModules();

    /**
     * The meta object literal for the '<em><b>Class Mapping</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAPPING__CLASS_MAPPING = eINSTANCE.getMapping_ClassMapping();

    /**
     * The meta object literal for the '<em><b>Includes</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAPPING__INCLUDES = eINSTANCE.getMapping_Includes();

    /**
     * The meta object literal for the '<em><b>Method Groups</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAPPING__METHOD_GROUPS = eINSTANCE.getMapping_MethodGroups();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.ImportImpl <em>Import</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.ImportImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getImport()
     * @generated
     */
    EClass IMPORT = eINSTANCE.getImport();

    /**
     * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IMPORT__IMPORT_URI = eINSTANCE.getImport_ImportURI();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.ClassMappingImpl <em>Class Mapping</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.ClassMappingImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getClassMapping()
     * @generated
     */
    EClass CLASS_MAPPING = eINSTANCE.getClassMapping();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CLASS_MAPPING__NAME = eINSTANCE.getClassMapping_Name();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.GenericsDeclarationImpl <em>Generics Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.GenericsDeclarationImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getGenericsDeclaration()
     * @generated
     */
    EClass GENERICS_DECLARATION = eINSTANCE.getGenericsDeclaration();

    /**
     * The meta object literal for the '<em><b>Names</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GENERICS_DECLARATION__NAMES = eINSTANCE.getGenericsDeclaration_Names();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.GenericsInstantiationImpl <em>Generics Instantiation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.GenericsInstantiationImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getGenericsInstantiation()
     * @generated
     */
    EClass GENERICS_INSTANTIATION = eINSTANCE.getGenericsInstantiation();

    /**
     * The meta object literal for the '<em><b>Instance Names</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GENERICS_INSTANTIATION__INSTANCE_NAMES = eINSTANCE.getGenericsInstantiation_InstanceNames();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.InterfaceImplementationImpl <em>Interface Implementation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.InterfaceImplementationImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getInterfaceImplementation()
     * @generated
     */
    EClass INTERFACE_IMPLEMENTATION = eINSTANCE.getInterfaceImplementation();

    /**
     * The meta object literal for the '<em><b>Interface</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTERFACE_IMPLEMENTATION__INTERFACE = eINSTANCE.getInterfaceImplementation_Interface();

    /**
     * The meta object literal for the '<em><b>Generics Instantiation</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTERFACE_IMPLEMENTATION__GENERICS_INSTANTIATION = eINSTANCE.getInterfaceImplementation_GenericsInstantiation();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.EnumToClassMappingImpl <em>Enum To Class Mapping</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.EnumToClassMappingImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getEnumToClassMapping()
     * @generated
     */
    EClass ENUM_TO_CLASS_MAPPING = eINSTANCE.getEnumToClassMapping();

    /**
     * The meta object literal for the '<em><b>Enum </b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ENUM_TO_CLASS_MAPPING__ENUM_ = eINSTANCE.getEnumToClassMapping_Enum_();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.StructToClassMappingImpl <em>Struct To Class Mapping</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.StructToClassMappingImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getStructToClassMapping()
     * @generated
     */
    EClass STRUCT_TO_CLASS_MAPPING = eINSTANCE.getStructToClassMapping();

    /**
     * The meta object literal for the '<em><b>Struct</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STRUCT_TO_CLASS_MAPPING__STRUCT = eINSTANCE.getStructToClassMapping_Struct();

    /**
     * The meta object literal for the '<em><b>Super</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STRUCT_TO_CLASS_MAPPING__SUPER = eINSTANCE.getStructToClassMapping_Super();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.ClassToClassMappingImpl <em>Class To Class Mapping</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.ClassToClassMappingImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getClassToClassMapping()
     * @generated
     */
    EClass CLASS_TO_CLASS_MAPPING = eINSTANCE.getClassToClassMapping();

    /**
     * The meta object literal for the '<em><b>Class</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CLASS_TO_CLASS_MAPPING__CLASS = eINSTANCE.getClassToClassMapping_Class();

    /**
     * The meta object literal for the '<em><b>Super</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CLASS_TO_CLASS_MAPPING__SUPER = eINSTANCE.getClassToClassMapping_Super();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.AliasToClassMappingImpl <em>Alias To Class Mapping</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.AliasToClassMappingImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getAliasToClassMapping()
     * @generated
     */
    EClass ALIAS_TO_CLASS_MAPPING = eINSTANCE.getAliasToClassMapping();

    /**
     * The meta object literal for the '<em><b>Alias</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ALIAS_TO_CLASS_MAPPING__ALIAS = eINSTANCE.getAliasToClassMapping_Alias();

    /**
     * The meta object literal for the '<em><b>Generics Decl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ALIAS_TO_CLASS_MAPPING__GENERICS_DECL = eINSTANCE.getAliasToClassMapping_GenericsDecl();

    /**
     * The meta object literal for the '<em><b>Super</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ALIAS_TO_CLASS_MAPPING__SUPER = eINSTANCE.getAliasToClassMapping_Super();

    /**
     * The meta object literal for the '<em><b>Generics Instantiation</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ALIAS_TO_CLASS_MAPPING__GENERICS_INSTANTIATION = eINSTANCE.getAliasToClassMapping_GenericsInstantiation();

    /**
     * The meta object literal for the '<em><b>Interfaces</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ALIAS_TO_CLASS_MAPPING__INTERFACES = eINSTANCE.getAliasToClassMapping_Interfaces();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.UnmappedClassImpl <em>Unmapped Class</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.UnmappedClassImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getUnmappedClass()
     * @generated
     */
    EClass UNMAPPED_CLASS = eINSTANCE.getUnmappedClass();

    /**
     * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNMAPPED_CLASS__ABSTRACT = eINSTANCE.getUnmappedClass_Abstract();

    /**
     * The meta object literal for the '<em><b>Generics Decl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UNMAPPED_CLASS__GENERICS_DECL = eINSTANCE.getUnmappedClass_GenericsDecl();

    /**
     * The meta object literal for the '<em><b>Super</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UNMAPPED_CLASS__SUPER = eINSTANCE.getUnmappedClass_Super();

    /**
     * The meta object literal for the '<em><b>Generics Instantiation</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UNMAPPED_CLASS__GENERICS_INSTANTIATION = eINSTANCE.getUnmappedClass_GenericsInstantiation();

    /**
     * The meta object literal for the '<em><b>Interfaces</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UNMAPPED_CLASS__INTERFACES = eINSTANCE.getUnmappedClass_Interfaces();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.InterfaceClassImpl <em>Interface Class</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.InterfaceClassImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getInterfaceClass()
     * @generated
     */
    EClass INTERFACE_CLASS = eINSTANCE.getInterfaceClass();

    /**
     * The meta object literal for the '<em><b>Generics Decl</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTERFACE_CLASS__GENERICS_DECL = eINSTANCE.getInterfaceClass_GenericsDecl();

    /**
     * The meta object literal for the '<em><b>Interfaces</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTERFACE_CLASS__INTERFACES = eINSTANCE.getInterfaceClass_Interfaces();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.ExternalLibraryImpl <em>External Library</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.ExternalLibraryImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getExternalLibrary()
     * @generated
     */
    EClass EXTERNAL_LIBRARY = eINSTANCE.getExternalLibrary();

    /**
     * The meta object literal for the '<em><b>Library</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXTERNAL_LIBRARY__LIBRARY = eINSTANCE.getExternalLibrary_Library();

    /**
     * The meta object literal for the '<em><b>Mapping</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXTERNAL_LIBRARY__MAPPING = eINSTANCE.getExternalLibrary_Mapping();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.LibraryImpl <em>Library</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.LibraryImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getLibrary()
     * @generated
     */
    EClass LIBRARY = eINSTANCE.getLibrary();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LIBRARY__NAME = eINSTANCE.getLibrary_Name();

    /**
     * The meta object literal for the '<em><b>Filenames</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LIBRARY__FILENAMES = eINSTANCE.getLibrary_Filenames();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.LibraryFileNameImpl <em>Library File Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.LibraryFileNameImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getLibraryFileName()
     * @generated
     */
    EClass LIBRARY_FILE_NAME = eINSTANCE.getLibraryFileName();

    /**
     * The meta object literal for the '<em><b>Os</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LIBRARY_FILE_NAME__OS = eINSTANCE.getLibraryFileName_Os();

    /**
     * The meta object literal for the '<em><b>Filename</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LIBRARY_FILE_NAME__FILENAME = eINSTANCE.getLibraryFileName_Filename();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.IncludeDefImpl <em>Include Def</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.IncludeDefImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getIncludeDef()
     * @generated
     */
    EClass INCLUDE_DEF = eINSTANCE.getIncludeDef();

    /**
     * The meta object literal for the '<em><b>Include</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INCLUDE_DEF__INCLUDE = eINSTANCE.getIncludeDef_Include();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.UserModuleImpl <em>User Module</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.UserModuleImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getUserModule()
     * @generated
     */
    EClass USER_MODULE = eINSTANCE.getUserModule();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute USER_MODULE__NAME = eINSTANCE.getUserModule_Name();

    /**
     * The meta object literal for the '<em><b>Methods</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference USER_MODULE__METHODS = eINSTANCE.getUserModule_Methods();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.CMethodImpl <em>CMethod</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.CMethodImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getCMethod()
     * @generated
     */
    EClass CMETHOD = eINSTANCE.getCMethod();

    /**
     * The meta object literal for the '<em><b>Res</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CMETHOD__RES = eINSTANCE.getCMethod_Res();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CMETHOD__NAME = eINSTANCE.getCMethod_Name();

    /**
     * The meta object literal for the '<em><b>Params</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CMETHOD__PARAMS = eINSTANCE.getCMethod_Params();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.TypeDeclarationImpl <em>Type Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.TypeDeclarationImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getTypeDeclaration()
     * @generated
     */
    EClass TYPE_DECLARATION = eINSTANCE.getTypeDeclaration();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TYPE_DECLARATION__NAME = eINSTANCE.getTypeDeclaration_Name();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.TypeAliasDeclarationImpl <em>Type Alias Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.TypeAliasDeclarationImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getTypeAliasDeclaration()
     * @generated
     */
    EClass TYPE_ALIAS_DECLARATION = eINSTANCE.getTypeAliasDeclaration();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE_ALIAS_DECLARATION__TYPE = eINSTANCE.getTypeAliasDeclaration_Type();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.StructDeclarationImpl <em>Struct Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.StructDeclarationImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getStructDeclaration()
     * @generated
     */
    EClass STRUCT_DECLARATION = eINSTANCE.getStructDeclaration();

    /**
     * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STRUCT_DECLARATION__FIELDS = eINSTANCE.getStructDeclaration_Fields();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.ClassDeclarationImpl <em>Class Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.ClassDeclarationImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getClassDeclaration()
     * @generated
     */
    EClass CLASS_DECLARATION = eINSTANCE.getClassDeclaration();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CLASS_DECLARATION__NAME = eINSTANCE.getClassDeclaration_Name();

    /**
     * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CLASS_DECLARATION__FIELDS = eINSTANCE.getClassDeclaration_Fields();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.EnumDeclarationImpl <em>Enum Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.EnumDeclarationImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getEnumDeclaration()
     * @generated
     */
    EClass ENUM_DECLARATION = eINSTANCE.getEnumDeclaration();

    /**
     * The meta object literal for the '<em><b>Values</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ENUM_DECLARATION__VALUES = eINSTANCE.getEnumDeclaration_Values();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.EnumTypeImpl <em>Enum Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.EnumTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getEnumType()
     * @generated
     */
    EClass ENUM_TYPE = eINSTANCE.getEnumType();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ENUM_TYPE__NAME = eINSTANCE.getEnumType_Name();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.EnumValueImpl <em>Enum Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.EnumValueImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getEnumValue()
     * @generated
     */
    EClass ENUM_VALUE = eINSTANCE.getEnumValue();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ENUM_VALUE__NAME = eINSTANCE.getEnumValue_Name();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ENUM_VALUE__VALUE = eINSTANCE.getEnumValue_Value();

    /**
     * The meta object literal for the '<em><b>Enum Next</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ENUM_VALUE__ENUM_NEXT = eINSTANCE.getEnumValue_EnumNext();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.ParamDefImpl <em>Param Def</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.ParamDefImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getParamDef()
     * @generated
     */
    EClass PARAM_DEF = eINSTANCE.getParamDef();

    /**
     * The meta object literal for the '<em><b>Give</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARAM_DEF__GIVE = eINSTANCE.getParamDef_Give();

    /**
     * The meta object literal for the '<em><b>Take</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARAM_DEF__TAKE = eINSTANCE.getParamDef_Take();

    /**
     * The meta object literal for the '<em><b>Keep</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARAM_DEF__KEEP = eINSTANCE.getParamDef_Keep();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARAM_DEF__TYPE = eINSTANCE.getParamDef_Type();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARAM_DEF__NAME = eINSTANCE.getParamDef_Name();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.MethodGroupImpl <em>Method Group</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.MethodGroupImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getMethodGroup()
     * @generated
     */
    EClass METHOD_GROUP = eINSTANCE.getMethodGroup();

    /**
     * The meta object literal for the '<em><b>Class</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD_GROUP__CLASS = eINSTANCE.getMethodGroup_Class();

    /**
     * The meta object literal for the '<em><b>Methods</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD_GROUP__METHODS = eINSTANCE.getMethodGroup_Methods();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.MethodImpl <em>Method</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.MethodImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getMethod()
     * @generated
     */
    EClass METHOD = eINSTANCE.getMethod();

    /**
     * The meta object literal for the '<em><b>Constructor</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD__CONSTRUCTOR = eINSTANCE.getMethod_Constructor();

    /**
     * The meta object literal for the '<em><b>Destructor</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD__DESTRUCTOR = eINSTANCE.getMethod_Destructor();

    /**
     * The meta object literal for the '<em><b>Instanceof</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD__INSTANCEOF = eINSTANCE.getMethod_Instanceof();

    /**
     * The meta object literal for the '<em><b>Static</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD__STATIC = eINSTANCE.getMethod_Static();

    /**
     * The meta object literal for the '<em><b>Private</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD__PRIVATE = eINSTANCE.getMethod_Private();

    /**
     * The meta object literal for the '<em><b>Protected</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD__PROTECTED = eINSTANCE.getMethod_Protected();

    /**
     * The meta object literal for the '<em><b>Renamed</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD__RENAMED = eINSTANCE.getMethod_Renamed();

    /**
     * The meta object literal for the '<em><b>Newname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD__NEWNAME = eINSTANCE.getMethod_Newname();

    /**
     * The meta object literal for the '<em><b>Stub</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD__STUB = eINSTANCE.getMethod_Stub();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD__ARG = eINSTANCE.getMethod_Arg();

    /**
     * The meta object literal for the '<em><b>Res</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD__RES = eINSTANCE.getMethod_Res();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD__NAME = eINSTANCE.getMethod_Name();

    /**
     * The meta object literal for the '<em><b>Params</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD__PARAMS = eINSTANCE.getMethod_Params();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.TypeImpl <em>Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.TypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getType()
     * @generated
     */
    EClass TYPE = eINSTANCE.getType();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.BuiltInTypeImpl <em>Built In Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.BuiltInTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getBuiltInType()
     * @generated
     */
    EClass BUILT_IN_TYPE = eINSTANCE.getBuiltInType();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BUILT_IN_TYPE__NAME = eINSTANCE.getBuiltInType_Name();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.BooleanTypeImpl <em>Boolean Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.BooleanTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getBooleanType()
     * @generated
     */
    EClass BOOLEAN_TYPE = eINSTANCE.getBooleanType();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.IntegerTypeImpl <em>Integer Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.IntegerTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getIntegerType()
     * @generated
     */
    EClass INTEGER_TYPE = eINSTANCE.getIntegerType();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.RealTypeImpl <em>Real Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.RealTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getRealType()
     * @generated
     */
    EClass REAL_TYPE = eINSTANCE.getRealType();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.StringTypeImpl <em>String Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.StringTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getStringType()
     * @generated
     */
    EClass STRING_TYPE = eINSTANCE.getStringType();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.VoidTypeImpl <em>Void Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.VoidTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getVoidType()
     * @generated
     */
    EClass VOID_TYPE = eINSTANCE.getVoidType();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.NestedTypeImpl <em>Nested Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.NestedTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getNestedType()
     * @generated
     */
    EClass NESTED_TYPE = eINSTANCE.getNestedType();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.ComplexTypeImpl <em>Complex Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.ComplexTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getComplexType()
     * @generated
     */
    EClass COMPLEX_TYPE = eINSTANCE.getComplexType();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.TypeAliasImpl <em>Type Alias</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.TypeAliasImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getTypeAlias()
     * @generated
     */
    EClass TYPE_ALIAS = eINSTANCE.getTypeAlias();

    /**
     * The meta object literal for the '<em><b>Alias</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE_ALIAS__ALIAS = eINSTANCE.getTypeAlias_Alias();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.ArrayTypeImpl <em>Array Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.ArrayTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getArrayType()
     * @generated
     */
    EClass ARRAY_TYPE = eINSTANCE.getArrayType();

    /**
     * The meta object literal for the '<em><b>Base</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_TYPE__BASE = eINSTANCE.getArrayType_Base();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.PtrTypeImpl <em>Ptr Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.PtrTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getPtrType()
     * @generated
     */
    EClass PTR_TYPE = eINSTANCE.getPtrType();

    /**
     * The meta object literal for the '<em><b>Base</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PTR_TYPE__BASE = eINSTANCE.getPtrType_Base();

    /**
     * The meta object literal for the '<em><b>Indir</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PTR_TYPE__INDIR = eINSTANCE.getPtrType_Indir();

    /**
     * The meta object literal for the '<em><b>Ignored</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PTR_TYPE__IGNORED = eINSTANCE.getPtrType_Ignored();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.ModifiableTypeImpl <em>Modifiable Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.ModifiableTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getModifiableType()
     * @generated
     */
    EClass MODIFIABLE_TYPE = eINSTANCE.getModifiableType();

    /**
     * The meta object literal for the '<em><b>Base</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODIFIABLE_TYPE__BASE = eINSTANCE.getModifiableType_Base();

    /**
     * The meta object literal for the '<em><b>Indir</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MODIFIABLE_TYPE__INDIR = eINSTANCE.getModifiableType_Indir();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.StructTypeImpl <em>Struct Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.StructTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getStructType()
     * @generated
     */
    EClass STRUCT_TYPE = eINSTANCE.getStructType();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STRUCT_TYPE__NAME = eINSTANCE.getStructType_Name();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.FieldDefImpl <em>Field Def</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.FieldDefImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getFieldDef()
     * @generated
     */
    EClass FIELD_DEF = eINSTANCE.getFieldDef();

    /**
     * The meta object literal for the '<em><b>Copy</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FIELD_DEF__COPY = eINSTANCE.getFieldDef_Copy();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FIELD_DEF__TYPE = eINSTANCE.getFieldDef_Type();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FIELD_DEF__NAME = eINSTANCE.getFieldDef_Name();

    /**
     * The meta object literal for the '<em><b>Many</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FIELD_DEF__MANY = eINSTANCE.getFieldDef_Many();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.ConstTypeImpl <em>Const Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.ConstTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getConstType()
     * @generated
     */
    EClass CONST_TYPE = eINSTANCE.getConstType();

    /**
     * The meta object literal for the '<em><b>Base</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONST_TYPE__BASE = eINSTANCE.getConstType_Base();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.impl.BaseTypeImpl <em>Base Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.impl.BaseTypeImpl
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getBaseType()
     * @generated
     */
    EClass BASE_TYPE = eINSTANCE.getBaseType();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.jniMap.HostType <em>Host Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.jniMap.HostType
     * @see fr.irisa.cairn.jniMap.impl.JniMapPackageImpl#getHostType()
     * @generated
     */
    EEnum HOST_TYPE = eINSTANCE.getHostType();

  }

} //JniMapPackage
