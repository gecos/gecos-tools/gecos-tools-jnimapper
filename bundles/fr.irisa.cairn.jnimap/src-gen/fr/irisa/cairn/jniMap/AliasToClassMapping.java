/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alias To Class Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getAlias <em>Alias</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getGenericsDecl <em>Generics Decl</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getSuper <em>Super</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getGenericsInstantiation <em>Generics Instantiation</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getInterfaces <em>Interfaces</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getAliasToClassMapping()
 * @model
 * @generated
 */
public interface AliasToClassMapping extends ClassMapping
{
  /**
   * Returns the value of the '<em><b>Alias</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Alias</em>' containment reference.
   * @see #setAlias(TypeAliasDeclaration)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getAliasToClassMapping_Alias()
   * @model containment="true"
   * @generated
   */
  TypeAliasDeclaration getAlias();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getAlias <em>Alias</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Alias</em>' containment reference.
   * @see #getAlias()
   * @generated
   */
  void setAlias(TypeAliasDeclaration value);

  /**
   * Returns the value of the '<em><b>Generics Decl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Generics Decl</em>' containment reference.
   * @see #setGenericsDecl(GenericsDeclaration)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getAliasToClassMapping_GenericsDecl()
   * @model containment="true"
   * @generated
   */
  GenericsDeclaration getGenericsDecl();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getGenericsDecl <em>Generics Decl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Generics Decl</em>' containment reference.
   * @see #getGenericsDecl()
   * @generated
   */
  void setGenericsDecl(GenericsDeclaration value);

  /**
   * Returns the value of the '<em><b>Super</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Super</em>' reference.
   * @see #setSuper(ClassMapping)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getAliasToClassMapping_Super()
   * @model
   * @generated
   */
  ClassMapping getSuper();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getSuper <em>Super</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Super</em>' reference.
   * @see #getSuper()
   * @generated
   */
  void setSuper(ClassMapping value);

  /**
   * Returns the value of the '<em><b>Generics Instantiation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Generics Instantiation</em>' containment reference.
   * @see #setGenericsInstantiation(GenericsInstantiation)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getAliasToClassMapping_GenericsInstantiation()
   * @model containment="true"
   * @generated
   */
  GenericsInstantiation getGenericsInstantiation();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.AliasToClassMapping#getGenericsInstantiation <em>Generics Instantiation</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Generics Instantiation</em>' containment reference.
   * @see #getGenericsInstantiation()
   * @generated
   */
  void setGenericsInstantiation(GenericsInstantiation value);

  /**
   * Returns the value of the '<em><b>Interfaces</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.InterfaceImplementation}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Interfaces</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getAliasToClassMapping_Interfaces()
   * @model containment="true"
   * @generated
   */
  EList<InterfaceImplementation> getInterfaces();

} // AliasToClassMapping
