/**
 */
package fr.irisa.cairn.jniMap;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.ArrayType#getBase <em>Base</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getArrayType()
 * @model
 * @generated
 */
public interface ArrayType extends Type, NestedType, ComplexType
{
  /**
   * Returns the value of the '<em><b>Base</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Base</em>' containment reference.
   * @see #setBase(BaseType)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getArrayType_Base()
   * @model containment="true"
   * @generated
   */
  BaseType getBase();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.ArrayType#getBase <em>Base</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Base</em>' containment reference.
   * @see #getBase()
   * @generated
   */
  void setBase(BaseType value);

} // ArrayType
