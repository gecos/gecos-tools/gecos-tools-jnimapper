/**
 */
package fr.irisa.cairn.jniMap;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getStringType()
 * @model
 * @generated
 */
public interface StringType extends BuiltInType
{
} // StringType
