/**
 */
package fr.irisa.cairn.jniMap;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CMethod</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.jniMap.CMethod#getRes <em>Res</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.CMethod#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.jniMap.CMethod#getParams <em>Params</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.jniMap.JniMapPackage#getCMethod()
 * @model
 * @generated
 */
public interface CMethod extends EObject
{
  /**
   * Returns the value of the '<em><b>Res</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Res</em>' containment reference.
   * @see #setRes(Type)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getCMethod_Res()
   * @model containment="true"
   * @generated
   */
  Type getRes();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.CMethod#getRes <em>Res</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Res</em>' containment reference.
   * @see #getRes()
   * @generated
   */
  void setRes(Type value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getCMethod_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.jniMap.CMethod#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Params</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.jniMap.ParamDef}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Params</em>' containment reference list.
   * @see fr.irisa.cairn.jniMap.JniMapPackage#getCMethod_Params()
   * @model containment="true"
   * @generated
   */
  EList<ParamDef> getParams();

} // CMethod
