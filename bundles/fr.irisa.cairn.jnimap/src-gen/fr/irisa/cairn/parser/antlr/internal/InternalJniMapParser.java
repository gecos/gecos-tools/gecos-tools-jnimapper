package fr.irisa.cairn.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.IUnorderedGroupHelper.UnorderedGroupState;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.cairn.services.JniMapGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalJniMapParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'mapping'", "'('", "'package'", "'='", "';'", "')'", "'['", "','", "']'", "'import'", "'<'", "'>'", "'as'", "'extending'", "'implements'", "'unmapped'", "'abstract'", "'interface'", "'use'", "'from'", "'library'", "'{'", "'libname'", "'}'", "'include'", "'module'", "'extern'", "'static'", "'typedef'", "'struct'", "'class'", "'enum'", "'give'", "'take'", "'keep'", "'group'", "'constructor'", "'destructor'", "'instanceOf'", "'private'", "'protected'", "'rename'", "'stub'", "'this'", "'boolean'", "'unsigned'", "'int'", "'long'", "'char'", "'float'", "'double'", "'string'", "'void'", "'*'", "'ignored'", "'&'", "'copyOnGet'", "'many'", "'const'", "'linux_32'", "'linux_64'", "'macosx_32'", "'macosx_64'", "'mingw_32'", "'cygwin_32'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__67=67;
    public static final int T__24=24;
    public static final int T__68=68;
    public static final int T__25=25;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__20=20;
    public static final int T__64=64;
    public static final int T__21=21;
    public static final int T__65=65;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__74=74;
    public static final int T__31=31;
    public static final int T__75=75;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalJniMapParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalJniMapParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalJniMapParser.tokenNames; }
    public String getGrammarFileName() { return "InternalJniMap.g"; }



    /*
      This grammar contains a lot of empty actions to work around a bug in ANTLR.
      Otherwise the ANTLR tool will create synpreds that cannot be compiled in some rare cases.
    */
     
     	private JniMapGrammarAccess grammarAccess;
     	
        public InternalJniMapParser(TokenStream input, JniMapGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Mapping";	
       	}
       	
       	@Override
       	protected JniMapGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleMapping"
    // InternalJniMap.g:75:1: entryRuleMapping returns [EObject current=null] : iv_ruleMapping= ruleMapping EOF ;
    public final EObject entryRuleMapping() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMapping = null;


        try {
            // InternalJniMap.g:76:2: (iv_ruleMapping= ruleMapping EOF )
            // InternalJniMap.g:77:2: iv_ruleMapping= ruleMapping EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMappingRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMapping=ruleMapping();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMapping; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMapping"


    // $ANTLR start "ruleMapping"
    // InternalJniMap.g:84:1: ruleMapping returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'mapping' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '(' otherlv_4= 'package' otherlv_5= '=' ( (lv_package_6_0= RULE_STRING ) ) otherlv_7= ';' otherlv_8= ')' )? (otherlv_9= '[' ( (lv_hosts_10_0= ruleHostType ) ) (otherlv_11= ',' ( (lv_hosts_12_0= ruleHostType ) ) )* otherlv_13= ']' ) otherlv_14= ';' ( (lv_externalLibraries_15_0= ruleExternalLibrary ) )* ( (lv_libraries_16_0= ruleLibrary ) )* ( (lv_userModules_17_0= ruleUserModule ) )* ( ( (lv_classMapping_18_0= ruleClassMapping ) ) | ( (lv_includes_19_0= ruleIncludeDef ) ) )+ ( (lv_methodGroups_20_0= ruleMethodGroup ) )+ ) ;
    public final EObject ruleMapping() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token lv_package_6_0=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        EObject lv_imports_0_0 = null;

        Enumerator lv_hosts_10_0 = null;

        Enumerator lv_hosts_12_0 = null;

        EObject lv_externalLibraries_15_0 = null;

        EObject lv_libraries_16_0 = null;

        EObject lv_userModules_17_0 = null;

        EObject lv_classMapping_18_0 = null;

        EObject lv_includes_19_0 = null;

        EObject lv_methodGroups_20_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:87:28: ( ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'mapping' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '(' otherlv_4= 'package' otherlv_5= '=' ( (lv_package_6_0= RULE_STRING ) ) otherlv_7= ';' otherlv_8= ')' )? (otherlv_9= '[' ( (lv_hosts_10_0= ruleHostType ) ) (otherlv_11= ',' ( (lv_hosts_12_0= ruleHostType ) ) )* otherlv_13= ']' ) otherlv_14= ';' ( (lv_externalLibraries_15_0= ruleExternalLibrary ) )* ( (lv_libraries_16_0= ruleLibrary ) )* ( (lv_userModules_17_0= ruleUserModule ) )* ( ( (lv_classMapping_18_0= ruleClassMapping ) ) | ( (lv_includes_19_0= ruleIncludeDef ) ) )+ ( (lv_methodGroups_20_0= ruleMethodGroup ) )+ ) )
            // InternalJniMap.g:88:1: ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'mapping' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '(' otherlv_4= 'package' otherlv_5= '=' ( (lv_package_6_0= RULE_STRING ) ) otherlv_7= ';' otherlv_8= ')' )? (otherlv_9= '[' ( (lv_hosts_10_0= ruleHostType ) ) (otherlv_11= ',' ( (lv_hosts_12_0= ruleHostType ) ) )* otherlv_13= ']' ) otherlv_14= ';' ( (lv_externalLibraries_15_0= ruleExternalLibrary ) )* ( (lv_libraries_16_0= ruleLibrary ) )* ( (lv_userModules_17_0= ruleUserModule ) )* ( ( (lv_classMapping_18_0= ruleClassMapping ) ) | ( (lv_includes_19_0= ruleIncludeDef ) ) )+ ( (lv_methodGroups_20_0= ruleMethodGroup ) )+ )
            {
            // InternalJniMap.g:88:1: ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'mapping' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '(' otherlv_4= 'package' otherlv_5= '=' ( (lv_package_6_0= RULE_STRING ) ) otherlv_7= ';' otherlv_8= ')' )? (otherlv_9= '[' ( (lv_hosts_10_0= ruleHostType ) ) (otherlv_11= ',' ( (lv_hosts_12_0= ruleHostType ) ) )* otherlv_13= ']' ) otherlv_14= ';' ( (lv_externalLibraries_15_0= ruleExternalLibrary ) )* ( (lv_libraries_16_0= ruleLibrary ) )* ( (lv_userModules_17_0= ruleUserModule ) )* ( ( (lv_classMapping_18_0= ruleClassMapping ) ) | ( (lv_includes_19_0= ruleIncludeDef ) ) )+ ( (lv_methodGroups_20_0= ruleMethodGroup ) )+ )
            // InternalJniMap.g:88:2: ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'mapping' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '(' otherlv_4= 'package' otherlv_5= '=' ( (lv_package_6_0= RULE_STRING ) ) otherlv_7= ';' otherlv_8= ')' )? (otherlv_9= '[' ( (lv_hosts_10_0= ruleHostType ) ) (otherlv_11= ',' ( (lv_hosts_12_0= ruleHostType ) ) )* otherlv_13= ']' ) otherlv_14= ';' ( (lv_externalLibraries_15_0= ruleExternalLibrary ) )* ( (lv_libraries_16_0= ruleLibrary ) )* ( (lv_userModules_17_0= ruleUserModule ) )* ( ( (lv_classMapping_18_0= ruleClassMapping ) ) | ( (lv_includes_19_0= ruleIncludeDef ) ) )+ ( (lv_methodGroups_20_0= ruleMethodGroup ) )+
            {
            // InternalJniMap.g:88:2: ( (lv_imports_0_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==20) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalJniMap.g:89:1: (lv_imports_0_0= ruleImport )
            	    {
            	    // InternalJniMap.g:89:1: (lv_imports_0_0= ruleImport )
            	    // InternalJniMap.g:90:3: lv_imports_0_0= ruleImport
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getMappingAccess().getImportsImportParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_3);
            	    lv_imports_0_0=ruleImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getMappingRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"imports",
            	              		lv_imports_0_0, 
            	              		"fr.irisa.cairn.JniMap.Import");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_1=(Token)match(input,11,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getMappingAccess().getMappingKeyword_1());
                  
            }
            // InternalJniMap.g:110:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalJniMap.g:111:1: (lv_name_2_0= RULE_ID )
            {
            // InternalJniMap.g:111:1: (lv_name_2_0= RULE_ID )
            // InternalJniMap.g:112:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getMappingAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getMappingRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:128:2: (otherlv_3= '(' otherlv_4= 'package' otherlv_5= '=' ( (lv_package_6_0= RULE_STRING ) ) otherlv_7= ';' otherlv_8= ')' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalJniMap.g:128:4: otherlv_3= '(' otherlv_4= 'package' otherlv_5= '=' ( (lv_package_6_0= RULE_STRING ) ) otherlv_7= ';' otherlv_8= ')'
                    {
                    otherlv_3=(Token)match(input,12,FOLLOW_6); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getMappingAccess().getLeftParenthesisKeyword_3_0());
                          
                    }
                    otherlv_4=(Token)match(input,13,FOLLOW_7); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_4, grammarAccess.getMappingAccess().getPackageKeyword_3_1());
                          
                    }
                    otherlv_5=(Token)match(input,14,FOLLOW_8); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getMappingAccess().getEqualsSignKeyword_3_2());
                          
                    }
                    // InternalJniMap.g:140:1: ( (lv_package_6_0= RULE_STRING ) )
                    // InternalJniMap.g:141:1: (lv_package_6_0= RULE_STRING )
                    {
                    // InternalJniMap.g:141:1: (lv_package_6_0= RULE_STRING )
                    // InternalJniMap.g:142:3: lv_package_6_0= RULE_STRING
                    {
                    lv_package_6_0=(Token)match(input,RULE_STRING,FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_package_6_0, grammarAccess.getMappingAccess().getPackageSTRINGTerminalRuleCall_3_3_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getMappingRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"package",
                              		lv_package_6_0, 
                              		"org.eclipse.xtext.common.Terminals.STRING");
                      	    
                    }

                    }


                    }

                    otherlv_7=(Token)match(input,15,FOLLOW_10); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_7, grammarAccess.getMappingAccess().getSemicolonKeyword_3_4());
                          
                    }
                    otherlv_8=(Token)match(input,16,FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_8, grammarAccess.getMappingAccess().getRightParenthesisKeyword_3_5());
                          
                    }

                    }
                    break;

            }

            // InternalJniMap.g:166:3: (otherlv_9= '[' ( (lv_hosts_10_0= ruleHostType ) ) (otherlv_11= ',' ( (lv_hosts_12_0= ruleHostType ) ) )* otherlv_13= ']' )
            // InternalJniMap.g:166:5: otherlv_9= '[' ( (lv_hosts_10_0= ruleHostType ) ) (otherlv_11= ',' ( (lv_hosts_12_0= ruleHostType ) ) )* otherlv_13= ']'
            {
            otherlv_9=(Token)match(input,17,FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_9, grammarAccess.getMappingAccess().getLeftSquareBracketKeyword_4_0());
                  
            }
            // InternalJniMap.g:170:1: ( (lv_hosts_10_0= ruleHostType ) )
            // InternalJniMap.g:171:1: (lv_hosts_10_0= ruleHostType )
            {
            // InternalJniMap.g:171:1: (lv_hosts_10_0= ruleHostType )
            // InternalJniMap.g:172:3: lv_hosts_10_0= ruleHostType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getMappingAccess().getHostsHostTypeEnumRuleCall_4_1_0()); 
              	    
            }
            pushFollow(FOLLOW_13);
            lv_hosts_10_0=ruleHostType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getMappingRule());
              	        }
                     		add(
                     			current, 
                     			"hosts",
                      		lv_hosts_10_0, 
                      		"fr.irisa.cairn.JniMap.HostType");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalJniMap.g:188:2: (otherlv_11= ',' ( (lv_hosts_12_0= ruleHostType ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==18) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalJniMap.g:188:4: otherlv_11= ',' ( (lv_hosts_12_0= ruleHostType ) )
            	    {
            	    otherlv_11=(Token)match(input,18,FOLLOW_12); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_11, grammarAccess.getMappingAccess().getCommaKeyword_4_2_0());
            	          
            	    }
            	    // InternalJniMap.g:192:1: ( (lv_hosts_12_0= ruleHostType ) )
            	    // InternalJniMap.g:193:1: (lv_hosts_12_0= ruleHostType )
            	    {
            	    // InternalJniMap.g:193:1: (lv_hosts_12_0= ruleHostType )
            	    // InternalJniMap.g:194:3: lv_hosts_12_0= ruleHostType
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getMappingAccess().getHostsHostTypeEnumRuleCall_4_2_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_13);
            	    lv_hosts_12_0=ruleHostType();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getMappingRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"hosts",
            	              		lv_hosts_12_0, 
            	              		"fr.irisa.cairn.JniMap.HostType");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_13=(Token)match(input,19,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_13, grammarAccess.getMappingAccess().getRightSquareBracketKeyword_4_3());
                  
            }

            }

            otherlv_14=(Token)match(input,15,FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_14, grammarAccess.getMappingAccess().getSemicolonKeyword_5());
                  
            }
            // InternalJniMap.g:218:1: ( (lv_externalLibraries_15_0= ruleExternalLibrary ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==29) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalJniMap.g:219:1: (lv_externalLibraries_15_0= ruleExternalLibrary )
            	    {
            	    // InternalJniMap.g:219:1: (lv_externalLibraries_15_0= ruleExternalLibrary )
            	    // InternalJniMap.g:220:3: lv_externalLibraries_15_0= ruleExternalLibrary
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getMappingAccess().getExternalLibrariesExternalLibraryParserRuleCall_6_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_14);
            	    lv_externalLibraries_15_0=ruleExternalLibrary();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getMappingRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"externalLibraries",
            	              		lv_externalLibraries_15_0, 
            	              		"fr.irisa.cairn.JniMap.ExternalLibrary");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            // InternalJniMap.g:236:3: ( (lv_libraries_16_0= ruleLibrary ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==31) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalJniMap.g:237:1: (lv_libraries_16_0= ruleLibrary )
            	    {
            	    // InternalJniMap.g:237:1: (lv_libraries_16_0= ruleLibrary )
            	    // InternalJniMap.g:238:3: lv_libraries_16_0= ruleLibrary
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getMappingAccess().getLibrariesLibraryParserRuleCall_7_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_14);
            	    lv_libraries_16_0=ruleLibrary();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getMappingRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"libraries",
            	              		lv_libraries_16_0, 
            	              		"fr.irisa.cairn.JniMap.Library");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            // InternalJniMap.g:254:3: ( (lv_userModules_17_0= ruleUserModule ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==36) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalJniMap.g:255:1: (lv_userModules_17_0= ruleUserModule )
            	    {
            	    // InternalJniMap.g:255:1: (lv_userModules_17_0= ruleUserModule )
            	    // InternalJniMap.g:256:3: lv_userModules_17_0= ruleUserModule
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getMappingAccess().getUserModulesUserModuleParserRuleCall_8_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_14);
            	    lv_userModules_17_0=ruleUserModule();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getMappingRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"userModules",
            	              		lv_userModules_17_0, 
            	              		"fr.irisa.cairn.JniMap.UserModule");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalJniMap.g:272:3: ( ( (lv_classMapping_18_0= ruleClassMapping ) ) | ( (lv_includes_19_0= ruleIncludeDef ) ) )+
            int cnt7=0;
            loop7:
            do {
                int alt7=3;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==26||LA7_0==28||(LA7_0>=39 && LA7_0<=42)) ) {
                    alt7=1;
                }
                else if ( (LA7_0==35) ) {
                    alt7=2;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalJniMap.g:272:4: ( (lv_classMapping_18_0= ruleClassMapping ) )
            	    {
            	    // InternalJniMap.g:272:4: ( (lv_classMapping_18_0= ruleClassMapping ) )
            	    // InternalJniMap.g:273:1: (lv_classMapping_18_0= ruleClassMapping )
            	    {
            	    // InternalJniMap.g:273:1: (lv_classMapping_18_0= ruleClassMapping )
            	    // InternalJniMap.g:274:3: lv_classMapping_18_0= ruleClassMapping
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getMappingAccess().getClassMappingClassMappingParserRuleCall_9_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_15);
            	    lv_classMapping_18_0=ruleClassMapping();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getMappingRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"classMapping",
            	              		lv_classMapping_18_0, 
            	              		"fr.irisa.cairn.JniMap.ClassMapping");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalJniMap.g:291:6: ( (lv_includes_19_0= ruleIncludeDef ) )
            	    {
            	    // InternalJniMap.g:291:6: ( (lv_includes_19_0= ruleIncludeDef ) )
            	    // InternalJniMap.g:292:1: (lv_includes_19_0= ruleIncludeDef )
            	    {
            	    // InternalJniMap.g:292:1: (lv_includes_19_0= ruleIncludeDef )
            	    // InternalJniMap.g:293:3: lv_includes_19_0= ruleIncludeDef
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getMappingAccess().getIncludesIncludeDefParserRuleCall_9_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_15);
            	    lv_includes_19_0=ruleIncludeDef();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getMappingRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"includes",
            	              		lv_includes_19_0, 
            	              		"fr.irisa.cairn.JniMap.IncludeDef");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);

            // InternalJniMap.g:309:4: ( (lv_methodGroups_20_0= ruleMethodGroup ) )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==46) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalJniMap.g:310:1: (lv_methodGroups_20_0= ruleMethodGroup )
            	    {
            	    // InternalJniMap.g:310:1: (lv_methodGroups_20_0= ruleMethodGroup )
            	    // InternalJniMap.g:311:3: lv_methodGroups_20_0= ruleMethodGroup
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getMappingAccess().getMethodGroupsMethodGroupParserRuleCall_10_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_16);
            	    lv_methodGroups_20_0=ruleMethodGroup();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getMappingRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"methodGroups",
            	              		lv_methodGroups_20_0, 
            	              		"fr.irisa.cairn.JniMap.MethodGroup");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMapping"


    // $ANTLR start "entryRuleImport"
    // InternalJniMap.g:335:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalJniMap.g:336:2: (iv_ruleImport= ruleImport EOF )
            // InternalJniMap.g:337:2: iv_ruleImport= ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImport; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalJniMap.g:344:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';' ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:347:28: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';' ) )
            // InternalJniMap.g:348:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';' )
            {
            // InternalJniMap.g:348:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';' )
            // InternalJniMap.g:348:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,20,FOLLOW_8); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
                  
            }
            // InternalJniMap.g:352:1: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalJniMap.g:353:1: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalJniMap.g:353:1: (lv_importURI_1_0= RULE_STRING )
            // InternalJniMap.g:354:3: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getImportRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"importURI",
                      		lv_importURI_1_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,15,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getImportAccess().getSemicolonKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleClassMapping"
    // InternalJniMap.g:382:1: entryRuleClassMapping returns [EObject current=null] : iv_ruleClassMapping= ruleClassMapping EOF ;
    public final EObject entryRuleClassMapping() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassMapping = null;


        try {
            // InternalJniMap.g:383:2: (iv_ruleClassMapping= ruleClassMapping EOF )
            // InternalJniMap.g:384:2: iv_ruleClassMapping= ruleClassMapping EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClassMappingRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleClassMapping=ruleClassMapping();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClassMapping; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassMapping"


    // $ANTLR start "ruleClassMapping"
    // InternalJniMap.g:391:1: ruleClassMapping returns [EObject current=null] : (this_StructToClassMapping_0= ruleStructToClassMapping | this_EnumToClassMapping_1= ruleEnumToClassMapping | this_AliasToClassMapping_2= ruleAliasToClassMapping | this_ClassToClassMapping_3= ruleClassToClassMapping | this_UnmappedClass_4= ruleUnmappedClass | this_InterfaceClass_5= ruleInterfaceClass ) ;
    public final EObject ruleClassMapping() throws RecognitionException {
        EObject current = null;

        EObject this_StructToClassMapping_0 = null;

        EObject this_EnumToClassMapping_1 = null;

        EObject this_AliasToClassMapping_2 = null;

        EObject this_ClassToClassMapping_3 = null;

        EObject this_UnmappedClass_4 = null;

        EObject this_InterfaceClass_5 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:394:28: ( (this_StructToClassMapping_0= ruleStructToClassMapping | this_EnumToClassMapping_1= ruleEnumToClassMapping | this_AliasToClassMapping_2= ruleAliasToClassMapping | this_ClassToClassMapping_3= ruleClassToClassMapping | this_UnmappedClass_4= ruleUnmappedClass | this_InterfaceClass_5= ruleInterfaceClass ) )
            // InternalJniMap.g:395:1: (this_StructToClassMapping_0= ruleStructToClassMapping | this_EnumToClassMapping_1= ruleEnumToClassMapping | this_AliasToClassMapping_2= ruleAliasToClassMapping | this_ClassToClassMapping_3= ruleClassToClassMapping | this_UnmappedClass_4= ruleUnmappedClass | this_InterfaceClass_5= ruleInterfaceClass )
            {
            // InternalJniMap.g:395:1: (this_StructToClassMapping_0= ruleStructToClassMapping | this_EnumToClassMapping_1= ruleEnumToClassMapping | this_AliasToClassMapping_2= ruleAliasToClassMapping | this_ClassToClassMapping_3= ruleClassToClassMapping | this_UnmappedClass_4= ruleUnmappedClass | this_InterfaceClass_5= ruleInterfaceClass )
            int alt9=6;
            switch ( input.LA(1) ) {
            case 40:
                {
                alt9=1;
                }
                break;
            case 42:
                {
                alt9=2;
                }
                break;
            case 39:
                {
                alt9=3;
                }
                break;
            case 41:
                {
                alt9=4;
                }
                break;
            case 26:
                {
                alt9=5;
                }
                break;
            case 28:
                {
                alt9=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalJniMap.g:396:2: this_StructToClassMapping_0= ruleStructToClassMapping
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getClassMappingAccess().getStructToClassMappingParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_StructToClassMapping_0=ruleStructToClassMapping();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StructToClassMapping_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalJniMap.g:409:2: this_EnumToClassMapping_1= ruleEnumToClassMapping
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getClassMappingAccess().getEnumToClassMappingParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_EnumToClassMapping_1=ruleEnumToClassMapping();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EnumToClassMapping_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalJniMap.g:422:2: this_AliasToClassMapping_2= ruleAliasToClassMapping
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getClassMappingAccess().getAliasToClassMappingParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_AliasToClassMapping_2=ruleAliasToClassMapping();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_AliasToClassMapping_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalJniMap.g:435:2: this_ClassToClassMapping_3= ruleClassToClassMapping
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getClassMappingAccess().getClassToClassMappingParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_ClassToClassMapping_3=ruleClassToClassMapping();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ClassToClassMapping_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalJniMap.g:448:2: this_UnmappedClass_4= ruleUnmappedClass
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getClassMappingAccess().getUnmappedClassParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_UnmappedClass_4=ruleUnmappedClass();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_UnmappedClass_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // InternalJniMap.g:461:2: this_InterfaceClass_5= ruleInterfaceClass
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getClassMappingAccess().getInterfaceClassParserRuleCall_5()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_InterfaceClass_5=ruleInterfaceClass();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_InterfaceClass_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassMapping"


    // $ANTLR start "entryRuleGenericsDeclaration"
    // InternalJniMap.g:480:1: entryRuleGenericsDeclaration returns [EObject current=null] : iv_ruleGenericsDeclaration= ruleGenericsDeclaration EOF ;
    public final EObject entryRuleGenericsDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGenericsDeclaration = null;


        try {
            // InternalJniMap.g:481:2: (iv_ruleGenericsDeclaration= ruleGenericsDeclaration EOF )
            // InternalJniMap.g:482:2: iv_ruleGenericsDeclaration= ruleGenericsDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGenericsDeclarationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGenericsDeclaration=ruleGenericsDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGenericsDeclaration; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGenericsDeclaration"


    // $ANTLR start "ruleGenericsDeclaration"
    // InternalJniMap.g:489:1: ruleGenericsDeclaration returns [EObject current=null] : (otherlv_0= '<' ( (lv_names_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_names_3_0= RULE_ID ) ) )* otherlv_4= '>' ) ;
    public final EObject ruleGenericsDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_names_1_0=null;
        Token otherlv_2=null;
        Token lv_names_3_0=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:492:28: ( (otherlv_0= '<' ( (lv_names_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_names_3_0= RULE_ID ) ) )* otherlv_4= '>' ) )
            // InternalJniMap.g:493:1: (otherlv_0= '<' ( (lv_names_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_names_3_0= RULE_ID ) ) )* otherlv_4= '>' )
            {
            // InternalJniMap.g:493:1: (otherlv_0= '<' ( (lv_names_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_names_3_0= RULE_ID ) ) )* otherlv_4= '>' )
            // InternalJniMap.g:493:3: otherlv_0= '<' ( (lv_names_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_names_3_0= RULE_ID ) ) )* otherlv_4= '>'
            {
            otherlv_0=(Token)match(input,21,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getGenericsDeclarationAccess().getLessThanSignKeyword_0());
                  
            }
            // InternalJniMap.g:497:1: ( (lv_names_1_0= RULE_ID ) )
            // InternalJniMap.g:498:1: (lv_names_1_0= RULE_ID )
            {
            // InternalJniMap.g:498:1: (lv_names_1_0= RULE_ID )
            // InternalJniMap.g:499:3: lv_names_1_0= RULE_ID
            {
            lv_names_1_0=(Token)match(input,RULE_ID,FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_names_1_0, grammarAccess.getGenericsDeclarationAccess().getNamesIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getGenericsDeclarationRule());
              	        }
                     		addWithLastConsumed(
                     			current, 
                     			"names",
                      		lv_names_1_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:515:2: (otherlv_2= ',' ( (lv_names_3_0= RULE_ID ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==18) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalJniMap.g:515:4: otherlv_2= ',' ( (lv_names_3_0= RULE_ID ) )
            	    {
            	    otherlv_2=(Token)match(input,18,FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getGenericsDeclarationAccess().getCommaKeyword_2_0());
            	          
            	    }
            	    // InternalJniMap.g:519:1: ( (lv_names_3_0= RULE_ID ) )
            	    // InternalJniMap.g:520:1: (lv_names_3_0= RULE_ID )
            	    {
            	    // InternalJniMap.g:520:1: (lv_names_3_0= RULE_ID )
            	    // InternalJniMap.g:521:3: lv_names_3_0= RULE_ID
            	    {
            	    lv_names_3_0=(Token)match(input,RULE_ID,FOLLOW_17); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      			newLeafNode(lv_names_3_0, grammarAccess.getGenericsDeclarationAccess().getNamesIDTerminalRuleCall_2_1_0()); 
            	      		
            	    }
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElement(grammarAccess.getGenericsDeclarationRule());
            	      	        }
            	             		addWithLastConsumed(
            	             			current, 
            	             			"names",
            	              		lv_names_3_0, 
            	              		"org.eclipse.xtext.common.Terminals.ID");
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            otherlv_4=(Token)match(input,22,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getGenericsDeclarationAccess().getGreaterThanSignKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGenericsDeclaration"


    // $ANTLR start "entryRuleGenericsInstantiation"
    // InternalJniMap.g:549:1: entryRuleGenericsInstantiation returns [EObject current=null] : iv_ruleGenericsInstantiation= ruleGenericsInstantiation EOF ;
    public final EObject entryRuleGenericsInstantiation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGenericsInstantiation = null;


        try {
            // InternalJniMap.g:550:2: (iv_ruleGenericsInstantiation= ruleGenericsInstantiation EOF )
            // InternalJniMap.g:551:2: iv_ruleGenericsInstantiation= ruleGenericsInstantiation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGenericsInstantiationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGenericsInstantiation=ruleGenericsInstantiation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGenericsInstantiation; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGenericsInstantiation"


    // $ANTLR start "ruleGenericsInstantiation"
    // InternalJniMap.g:558:1: ruleGenericsInstantiation returns [EObject current=null] : (otherlv_0= '<' ( (lv_instanceNames_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_instanceNames_3_0= RULE_ID ) ) )* otherlv_4= '>' ) ;
    public final EObject ruleGenericsInstantiation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_instanceNames_1_0=null;
        Token otherlv_2=null;
        Token lv_instanceNames_3_0=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:561:28: ( (otherlv_0= '<' ( (lv_instanceNames_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_instanceNames_3_0= RULE_ID ) ) )* otherlv_4= '>' ) )
            // InternalJniMap.g:562:1: (otherlv_0= '<' ( (lv_instanceNames_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_instanceNames_3_0= RULE_ID ) ) )* otherlv_4= '>' )
            {
            // InternalJniMap.g:562:1: (otherlv_0= '<' ( (lv_instanceNames_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_instanceNames_3_0= RULE_ID ) ) )* otherlv_4= '>' )
            // InternalJniMap.g:562:3: otherlv_0= '<' ( (lv_instanceNames_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_instanceNames_3_0= RULE_ID ) ) )* otherlv_4= '>'
            {
            otherlv_0=(Token)match(input,21,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getGenericsInstantiationAccess().getLessThanSignKeyword_0());
                  
            }
            // InternalJniMap.g:566:1: ( (lv_instanceNames_1_0= RULE_ID ) )
            // InternalJniMap.g:567:1: (lv_instanceNames_1_0= RULE_ID )
            {
            // InternalJniMap.g:567:1: (lv_instanceNames_1_0= RULE_ID )
            // InternalJniMap.g:568:3: lv_instanceNames_1_0= RULE_ID
            {
            lv_instanceNames_1_0=(Token)match(input,RULE_ID,FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_instanceNames_1_0, grammarAccess.getGenericsInstantiationAccess().getInstanceNamesIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getGenericsInstantiationRule());
              	        }
                     		addWithLastConsumed(
                     			current, 
                     			"instanceNames",
                      		lv_instanceNames_1_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:584:2: (otherlv_2= ',' ( (lv_instanceNames_3_0= RULE_ID ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==18) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalJniMap.g:584:4: otherlv_2= ',' ( (lv_instanceNames_3_0= RULE_ID ) )
            	    {
            	    otherlv_2=(Token)match(input,18,FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getGenericsInstantiationAccess().getCommaKeyword_2_0());
            	          
            	    }
            	    // InternalJniMap.g:588:1: ( (lv_instanceNames_3_0= RULE_ID ) )
            	    // InternalJniMap.g:589:1: (lv_instanceNames_3_0= RULE_ID )
            	    {
            	    // InternalJniMap.g:589:1: (lv_instanceNames_3_0= RULE_ID )
            	    // InternalJniMap.g:590:3: lv_instanceNames_3_0= RULE_ID
            	    {
            	    lv_instanceNames_3_0=(Token)match(input,RULE_ID,FOLLOW_17); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      			newLeafNode(lv_instanceNames_3_0, grammarAccess.getGenericsInstantiationAccess().getInstanceNamesIDTerminalRuleCall_2_1_0()); 
            	      		
            	    }
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElement(grammarAccess.getGenericsInstantiationRule());
            	      	        }
            	             		addWithLastConsumed(
            	             			current, 
            	             			"instanceNames",
            	              		lv_instanceNames_3_0, 
            	              		"org.eclipse.xtext.common.Terminals.ID");
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            otherlv_4=(Token)match(input,22,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getGenericsInstantiationAccess().getGreaterThanSignKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGenericsInstantiation"


    // $ANTLR start "entryRuleInterfaceImplementation"
    // InternalJniMap.g:618:1: entryRuleInterfaceImplementation returns [EObject current=null] : iv_ruleInterfaceImplementation= ruleInterfaceImplementation EOF ;
    public final EObject entryRuleInterfaceImplementation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterfaceImplementation = null;


        try {
            // InternalJniMap.g:619:2: (iv_ruleInterfaceImplementation= ruleInterfaceImplementation EOF )
            // InternalJniMap.g:620:2: iv_ruleInterfaceImplementation= ruleInterfaceImplementation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInterfaceImplementationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInterfaceImplementation=ruleInterfaceImplementation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInterfaceImplementation; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterfaceImplementation"


    // $ANTLR start "ruleInterfaceImplementation"
    // InternalJniMap.g:627:1: ruleInterfaceImplementation returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) ( (lv_genericsInstantiation_1_0= ruleGenericsInstantiation ) )? ) ;
    public final EObject ruleInterfaceImplementation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_genericsInstantiation_1_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:630:28: ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_genericsInstantiation_1_0= ruleGenericsInstantiation ) )? ) )
            // InternalJniMap.g:631:1: ( ( (otherlv_0= RULE_ID ) ) ( (lv_genericsInstantiation_1_0= ruleGenericsInstantiation ) )? )
            {
            // InternalJniMap.g:631:1: ( ( (otherlv_0= RULE_ID ) ) ( (lv_genericsInstantiation_1_0= ruleGenericsInstantiation ) )? )
            // InternalJniMap.g:631:2: ( (otherlv_0= RULE_ID ) ) ( (lv_genericsInstantiation_1_0= ruleGenericsInstantiation ) )?
            {
            // InternalJniMap.g:631:2: ( (otherlv_0= RULE_ID ) )
            // InternalJniMap.g:632:1: (otherlv_0= RULE_ID )
            {
            // InternalJniMap.g:632:1: (otherlv_0= RULE_ID )
            // InternalJniMap.g:633:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getInterfaceImplementationRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_18); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getInterfaceImplementationAccess().getInterfaceInterfaceClassCrossReference_0_0()); 
              	
            }

            }


            }

            // InternalJniMap.g:647:2: ( (lv_genericsInstantiation_1_0= ruleGenericsInstantiation ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==21) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalJniMap.g:648:1: (lv_genericsInstantiation_1_0= ruleGenericsInstantiation )
                    {
                    // InternalJniMap.g:648:1: (lv_genericsInstantiation_1_0= ruleGenericsInstantiation )
                    // InternalJniMap.g:649:3: lv_genericsInstantiation_1_0= ruleGenericsInstantiation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getInterfaceImplementationAccess().getGenericsInstantiationGenericsInstantiationParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_2);
                    lv_genericsInstantiation_1_0=ruleGenericsInstantiation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getInterfaceImplementationRule());
                      	        }
                             		set(
                             			current, 
                             			"genericsInstantiation",
                              		lv_genericsInstantiation_1_0, 
                              		"fr.irisa.cairn.JniMap.GenericsInstantiation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterfaceImplementation"


    // $ANTLR start "entryRuleEnumToClassMapping"
    // InternalJniMap.g:673:1: entryRuleEnumToClassMapping returns [EObject current=null] : iv_ruleEnumToClassMapping= ruleEnumToClassMapping EOF ;
    public final EObject entryRuleEnumToClassMapping() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumToClassMapping = null;


        try {
            // InternalJniMap.g:674:2: (iv_ruleEnumToClassMapping= ruleEnumToClassMapping EOF )
            // InternalJniMap.g:675:2: iv_ruleEnumToClassMapping= ruleEnumToClassMapping EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumToClassMappingRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEnumToClassMapping=ruleEnumToClassMapping();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumToClassMapping; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumToClassMapping"


    // $ANTLR start "ruleEnumToClassMapping"
    // InternalJniMap.g:682:1: ruleEnumToClassMapping returns [EObject current=null] : ( ( (lv_enum__0_0= ruleEnumDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ';' ) ;
    public final EObject ruleEnumToClassMapping() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_enum__0_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:685:28: ( ( ( (lv_enum__0_0= ruleEnumDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ';' ) )
            // InternalJniMap.g:686:1: ( ( (lv_enum__0_0= ruleEnumDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ';' )
            {
            // InternalJniMap.g:686:1: ( ( (lv_enum__0_0= ruleEnumDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ';' )
            // InternalJniMap.g:686:2: ( (lv_enum__0_0= ruleEnumDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ';'
            {
            // InternalJniMap.g:686:2: ( (lv_enum__0_0= ruleEnumDeclaration ) )
            // InternalJniMap.g:687:1: (lv_enum__0_0= ruleEnumDeclaration )
            {
            // InternalJniMap.g:687:1: (lv_enum__0_0= ruleEnumDeclaration )
            // InternalJniMap.g:688:3: lv_enum__0_0= ruleEnumDeclaration
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEnumToClassMappingAccess().getEnum_EnumDeclarationParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_19);
            lv_enum__0_0=ruleEnumDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEnumToClassMappingRule());
              	        }
                     		set(
                     			current, 
                     			"enum_",
                      		lv_enum__0_0, 
                      		"fr.irisa.cairn.JniMap.EnumDeclaration");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,23,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getEnumToClassMappingAccess().getAsKeyword_1());
                  
            }
            // InternalJniMap.g:708:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalJniMap.g:709:1: (lv_name_2_0= RULE_ID )
            {
            // InternalJniMap.g:709:1: (lv_name_2_0= RULE_ID )
            // InternalJniMap.g:710:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getEnumToClassMappingAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumToClassMappingRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,15,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getEnumToClassMappingAccess().getSemicolonKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumToClassMapping"


    // $ANTLR start "entryRuleStructToClassMapping"
    // InternalJniMap.g:738:1: entryRuleStructToClassMapping returns [EObject current=null] : iv_ruleStructToClassMapping= ruleStructToClassMapping EOF ;
    public final EObject entryRuleStructToClassMapping() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructToClassMapping = null;


        try {
            // InternalJniMap.g:739:2: (iv_ruleStructToClassMapping= ruleStructToClassMapping EOF )
            // InternalJniMap.g:740:2: iv_ruleStructToClassMapping= ruleStructToClassMapping EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStructToClassMappingRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleStructToClassMapping=ruleStructToClassMapping();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStructToClassMapping; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructToClassMapping"


    // $ANTLR start "ruleStructToClassMapping"
    // InternalJniMap.g:747:1: ruleStructToClassMapping returns [EObject current=null] : ( ( (lv_struct_0_0= ruleStructDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= ';' ) ;
    public final EObject ruleStructToClassMapping() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_struct_0_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:750:28: ( ( ( (lv_struct_0_0= ruleStructDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= ';' ) )
            // InternalJniMap.g:751:1: ( ( (lv_struct_0_0= ruleStructDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= ';' )
            {
            // InternalJniMap.g:751:1: ( ( (lv_struct_0_0= ruleStructDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= ';' )
            // InternalJniMap.g:751:2: ( (lv_struct_0_0= ruleStructDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= ';'
            {
            // InternalJniMap.g:751:2: ( (lv_struct_0_0= ruleStructDeclaration ) )
            // InternalJniMap.g:752:1: (lv_struct_0_0= ruleStructDeclaration )
            {
            // InternalJniMap.g:752:1: (lv_struct_0_0= ruleStructDeclaration )
            // InternalJniMap.g:753:3: lv_struct_0_0= ruleStructDeclaration
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getStructToClassMappingAccess().getStructStructDeclarationParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_19);
            lv_struct_0_0=ruleStructDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getStructToClassMappingRule());
              	        }
                     		set(
                     			current, 
                     			"struct",
                      		lv_struct_0_0, 
                      		"fr.irisa.cairn.JniMap.StructDeclaration");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,23,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getStructToClassMappingAccess().getAsKeyword_1());
                  
            }
            // InternalJniMap.g:773:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalJniMap.g:774:1: (lv_name_2_0= RULE_ID )
            {
            // InternalJniMap.g:774:1: (lv_name_2_0= RULE_ID )
            // InternalJniMap.g:775:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_20); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getStructToClassMappingAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStructToClassMappingRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:791:2: (otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==24) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalJniMap.g:791:4: otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) )
                    {
                    otherlv_3=(Token)match(input,24,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getStructToClassMappingAccess().getExtendingKeyword_3_0());
                          
                    }
                    // InternalJniMap.g:795:1: ( (otherlv_4= RULE_ID ) )
                    // InternalJniMap.g:796:1: (otherlv_4= RULE_ID )
                    {
                    // InternalJniMap.g:796:1: (otherlv_4= RULE_ID )
                    // InternalJniMap.g:797:3: otherlv_4= RULE_ID
                    {
                    if ( state.backtracking==0 ) {
                       
                      		  /* */ 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getStructToClassMappingRule());
                      	        }
                              
                    }
                    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_4, grammarAccess.getStructToClassMappingAccess().getSuperClassMappingCrossReference_3_1_0()); 
                      	
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,15,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getStructToClassMappingAccess().getSemicolonKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructToClassMapping"


    // $ANTLR start "entryRuleClassToClassMapping"
    // InternalJniMap.g:823:1: entryRuleClassToClassMapping returns [EObject current=null] : iv_ruleClassToClassMapping= ruleClassToClassMapping EOF ;
    public final EObject entryRuleClassToClassMapping() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassToClassMapping = null;


        try {
            // InternalJniMap.g:824:2: (iv_ruleClassToClassMapping= ruleClassToClassMapping EOF )
            // InternalJniMap.g:825:2: iv_ruleClassToClassMapping= ruleClassToClassMapping EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClassToClassMappingRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleClassToClassMapping=ruleClassToClassMapping();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClassToClassMapping; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassToClassMapping"


    // $ANTLR start "ruleClassToClassMapping"
    // InternalJniMap.g:832:1: ruleClassToClassMapping returns [EObject current=null] : ( ( (lv_class_0_0= ruleClassDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= ';' ) ;
    public final EObject ruleClassToClassMapping() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_class_0_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:835:28: ( ( ( (lv_class_0_0= ruleClassDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= ';' ) )
            // InternalJniMap.g:836:1: ( ( (lv_class_0_0= ruleClassDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= ';' )
            {
            // InternalJniMap.g:836:1: ( ( (lv_class_0_0= ruleClassDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= ';' )
            // InternalJniMap.g:836:2: ( (lv_class_0_0= ruleClassDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= ';'
            {
            // InternalJniMap.g:836:2: ( (lv_class_0_0= ruleClassDeclaration ) )
            // InternalJniMap.g:837:1: (lv_class_0_0= ruleClassDeclaration )
            {
            // InternalJniMap.g:837:1: (lv_class_0_0= ruleClassDeclaration )
            // InternalJniMap.g:838:3: lv_class_0_0= ruleClassDeclaration
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getClassToClassMappingAccess().getClassClassDeclarationParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_19);
            lv_class_0_0=ruleClassDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getClassToClassMappingRule());
              	        }
                     		set(
                     			current, 
                     			"class",
                      		lv_class_0_0, 
                      		"fr.irisa.cairn.JniMap.ClassDeclaration");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,23,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getClassToClassMappingAccess().getAsKeyword_1());
                  
            }
            // InternalJniMap.g:858:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalJniMap.g:859:1: (lv_name_2_0= RULE_ID )
            {
            // InternalJniMap.g:859:1: (lv_name_2_0= RULE_ID )
            // InternalJniMap.g:860:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_20); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getClassToClassMappingAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getClassToClassMappingRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:876:2: (otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==24) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalJniMap.g:876:4: otherlv_3= 'extending' ( (otherlv_4= RULE_ID ) )
                    {
                    otherlv_3=(Token)match(input,24,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getClassToClassMappingAccess().getExtendingKeyword_3_0());
                          
                    }
                    // InternalJniMap.g:880:1: ( (otherlv_4= RULE_ID ) )
                    // InternalJniMap.g:881:1: (otherlv_4= RULE_ID )
                    {
                    // InternalJniMap.g:881:1: (otherlv_4= RULE_ID )
                    // InternalJniMap.g:882:3: otherlv_4= RULE_ID
                    {
                    if ( state.backtracking==0 ) {
                       
                      		  /* */ 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getClassToClassMappingRule());
                      	        }
                              
                    }
                    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_4, grammarAccess.getClassToClassMappingAccess().getSuperClassMappingCrossReference_3_1_0()); 
                      	
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,15,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getClassToClassMappingAccess().getSemicolonKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassToClassMapping"


    // $ANTLR start "entryRuleAliasToClassMapping"
    // InternalJniMap.g:908:1: entryRuleAliasToClassMapping returns [EObject current=null] : iv_ruleAliasToClassMapping= ruleAliasToClassMapping EOF ;
    public final EObject entryRuleAliasToClassMapping() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAliasToClassMapping = null;


        try {
            // InternalJniMap.g:909:2: (iv_ruleAliasToClassMapping= ruleAliasToClassMapping EOF )
            // InternalJniMap.g:910:2: iv_ruleAliasToClassMapping= ruleAliasToClassMapping EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAliasToClassMappingRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAliasToClassMapping=ruleAliasToClassMapping();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAliasToClassMapping; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAliasToClassMapping"


    // $ANTLR start "ruleAliasToClassMapping"
    // InternalJniMap.g:917:1: ruleAliasToClassMapping returns [EObject current=null] : ( ( (lv_alias_0_0= ruleTypeAliasDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) ( (lv_genericsDecl_3_0= ruleGenericsDeclaration ) )? (otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )? )? (otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )* )? otherlv_11= ';' ) ;
    public final EObject ruleAliasToClassMapping() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        EObject lv_alias_0_0 = null;

        EObject lv_genericsDecl_3_0 = null;

        EObject lv_genericsInstantiation_6_0 = null;

        EObject lv_interfaces_8_0 = null;

        EObject lv_interfaces_10_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:920:28: ( ( ( (lv_alias_0_0= ruleTypeAliasDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) ( (lv_genericsDecl_3_0= ruleGenericsDeclaration ) )? (otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )? )? (otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )* )? otherlv_11= ';' ) )
            // InternalJniMap.g:921:1: ( ( (lv_alias_0_0= ruleTypeAliasDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) ( (lv_genericsDecl_3_0= ruleGenericsDeclaration ) )? (otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )? )? (otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )* )? otherlv_11= ';' )
            {
            // InternalJniMap.g:921:1: ( ( (lv_alias_0_0= ruleTypeAliasDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) ( (lv_genericsDecl_3_0= ruleGenericsDeclaration ) )? (otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )? )? (otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )* )? otherlv_11= ';' )
            // InternalJniMap.g:921:2: ( (lv_alias_0_0= ruleTypeAliasDeclaration ) ) otherlv_1= 'as' ( (lv_name_2_0= RULE_ID ) ) ( (lv_genericsDecl_3_0= ruleGenericsDeclaration ) )? (otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )? )? (otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )* )? otherlv_11= ';'
            {
            // InternalJniMap.g:921:2: ( (lv_alias_0_0= ruleTypeAliasDeclaration ) )
            // InternalJniMap.g:922:1: (lv_alias_0_0= ruleTypeAliasDeclaration )
            {
            // InternalJniMap.g:922:1: (lv_alias_0_0= ruleTypeAliasDeclaration )
            // InternalJniMap.g:923:3: lv_alias_0_0= ruleTypeAliasDeclaration
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAliasToClassMappingAccess().getAliasTypeAliasDeclarationParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_19);
            lv_alias_0_0=ruleTypeAliasDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAliasToClassMappingRule());
              	        }
                     		set(
                     			current, 
                     			"alias",
                      		lv_alias_0_0, 
                      		"fr.irisa.cairn.JniMap.TypeAliasDeclaration");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,23,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getAliasToClassMappingAccess().getAsKeyword_1());
                  
            }
            // InternalJniMap.g:943:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalJniMap.g:944:1: (lv_name_2_0= RULE_ID )
            {
            // InternalJniMap.g:944:1: (lv_name_2_0= RULE_ID )
            // InternalJniMap.g:945:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getAliasToClassMappingAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getAliasToClassMappingRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:961:2: ( (lv_genericsDecl_3_0= ruleGenericsDeclaration ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==21) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalJniMap.g:962:1: (lv_genericsDecl_3_0= ruleGenericsDeclaration )
                    {
                    // InternalJniMap.g:962:1: (lv_genericsDecl_3_0= ruleGenericsDeclaration )
                    // InternalJniMap.g:963:3: lv_genericsDecl_3_0= ruleGenericsDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAliasToClassMappingAccess().getGenericsDeclGenericsDeclarationParserRuleCall_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_22);
                    lv_genericsDecl_3_0=ruleGenericsDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAliasToClassMappingRule());
                      	        }
                             		set(
                             			current, 
                             			"genericsDecl",
                              		lv_genericsDecl_3_0, 
                              		"fr.irisa.cairn.JniMap.GenericsDeclaration");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalJniMap.g:979:3: (otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )? )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==24) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalJniMap.g:979:5: otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )?
                    {
                    otherlv_4=(Token)match(input,24,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_4, grammarAccess.getAliasToClassMappingAccess().getExtendingKeyword_4_0());
                          
                    }
                    // InternalJniMap.g:983:1: ( (otherlv_5= RULE_ID ) )
                    // InternalJniMap.g:984:1: (otherlv_5= RULE_ID )
                    {
                    // InternalJniMap.g:984:1: (otherlv_5= RULE_ID )
                    // InternalJniMap.g:985:3: otherlv_5= RULE_ID
                    {
                    if ( state.backtracking==0 ) {
                       
                      		  /* */ 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getAliasToClassMappingRule());
                      	        }
                              
                    }
                    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_23); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_5, grammarAccess.getAliasToClassMappingAccess().getSuperClassMappingCrossReference_4_1_0()); 
                      	
                    }

                    }


                    }

                    // InternalJniMap.g:999:2: ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )?
                    int alt16=2;
                    int LA16_0 = input.LA(1);

                    if ( (LA16_0==21) ) {
                        alt16=1;
                    }
                    switch (alt16) {
                        case 1 :
                            // InternalJniMap.g:1000:1: (lv_genericsInstantiation_6_0= ruleGenericsInstantiation )
                            {
                            // InternalJniMap.g:1000:1: (lv_genericsInstantiation_6_0= ruleGenericsInstantiation )
                            // InternalJniMap.g:1001:3: lv_genericsInstantiation_6_0= ruleGenericsInstantiation
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getAliasToClassMappingAccess().getGenericsInstantiationGenericsInstantiationParserRuleCall_4_2_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_24);
                            lv_genericsInstantiation_6_0=ruleGenericsInstantiation();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getAliasToClassMappingRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"genericsInstantiation",
                                      		lv_genericsInstantiation_6_0, 
                                      		"fr.irisa.cairn.JniMap.GenericsInstantiation");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }
                            break;

                    }


                    }
                    break;

            }

            // InternalJniMap.g:1017:5: (otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )* )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==25) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalJniMap.g:1017:7: otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )*
                    {
                    otherlv_7=(Token)match(input,25,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_7, grammarAccess.getAliasToClassMappingAccess().getImplementsKeyword_5_0());
                          
                    }
                    // InternalJniMap.g:1021:1: ( (lv_interfaces_8_0= ruleInterfaceImplementation ) )
                    // InternalJniMap.g:1022:1: (lv_interfaces_8_0= ruleInterfaceImplementation )
                    {
                    // InternalJniMap.g:1022:1: (lv_interfaces_8_0= ruleInterfaceImplementation )
                    // InternalJniMap.g:1023:3: lv_interfaces_8_0= ruleInterfaceImplementation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAliasToClassMappingAccess().getInterfacesInterfaceImplementationParserRuleCall_5_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_25);
                    lv_interfaces_8_0=ruleInterfaceImplementation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAliasToClassMappingRule());
                      	        }
                             		add(
                             			current, 
                             			"interfaces",
                              		lv_interfaces_8_0, 
                              		"fr.irisa.cairn.JniMap.InterfaceImplementation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalJniMap.g:1039:2: (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )*
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( (LA18_0==18) ) {
                            alt18=1;
                        }


                        switch (alt18) {
                    	case 1 :
                    	    // InternalJniMap.g:1039:4: otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) )
                    	    {
                    	    otherlv_9=(Token)match(input,18,FOLLOW_4); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_9, grammarAccess.getAliasToClassMappingAccess().getCommaKeyword_5_2_0());
                    	          
                    	    }
                    	    // InternalJniMap.g:1043:1: ( (lv_interfaces_10_0= ruleInterfaceImplementation ) )
                    	    // InternalJniMap.g:1044:1: (lv_interfaces_10_0= ruleInterfaceImplementation )
                    	    {
                    	    // InternalJniMap.g:1044:1: (lv_interfaces_10_0= ruleInterfaceImplementation )
                    	    // InternalJniMap.g:1045:3: lv_interfaces_10_0= ruleInterfaceImplementation
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getAliasToClassMappingAccess().getInterfacesInterfaceImplementationParserRuleCall_5_2_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_25);
                    	    lv_interfaces_10_0=ruleInterfaceImplementation();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getAliasToClassMappingRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"interfaces",
                    	              		lv_interfaces_10_0, 
                    	              		"fr.irisa.cairn.JniMap.InterfaceImplementation");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop18;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_11=(Token)match(input,15,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_11, grammarAccess.getAliasToClassMappingAccess().getSemicolonKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAliasToClassMapping"


    // $ANTLR start "entryRuleUnmappedClass"
    // InternalJniMap.g:1073:1: entryRuleUnmappedClass returns [EObject current=null] : iv_ruleUnmappedClass= ruleUnmappedClass EOF ;
    public final EObject entryRuleUnmappedClass() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnmappedClass = null;


        try {
            // InternalJniMap.g:1074:2: (iv_ruleUnmappedClass= ruleUnmappedClass EOF )
            // InternalJniMap.g:1075:2: iv_ruleUnmappedClass= ruleUnmappedClass EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnmappedClassRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUnmappedClass=ruleUnmappedClass();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnmappedClass; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnmappedClass"


    // $ANTLR start "ruleUnmappedClass"
    // InternalJniMap.g:1082:1: ruleUnmappedClass returns [EObject current=null] : (otherlv_0= 'unmapped' ( (lv_abstract_1_0= 'abstract' ) )? ( (lv_name_2_0= RULE_ID ) ) ( (lv_genericsDecl_3_0= ruleGenericsDeclaration ) )? (otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )? )? (otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )* )? otherlv_11= ';' ) ;
    public final EObject ruleUnmappedClass() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_abstract_1_0=null;
        Token lv_name_2_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        EObject lv_genericsDecl_3_0 = null;

        EObject lv_genericsInstantiation_6_0 = null;

        EObject lv_interfaces_8_0 = null;

        EObject lv_interfaces_10_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:1085:28: ( (otherlv_0= 'unmapped' ( (lv_abstract_1_0= 'abstract' ) )? ( (lv_name_2_0= RULE_ID ) ) ( (lv_genericsDecl_3_0= ruleGenericsDeclaration ) )? (otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )? )? (otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )* )? otherlv_11= ';' ) )
            // InternalJniMap.g:1086:1: (otherlv_0= 'unmapped' ( (lv_abstract_1_0= 'abstract' ) )? ( (lv_name_2_0= RULE_ID ) ) ( (lv_genericsDecl_3_0= ruleGenericsDeclaration ) )? (otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )? )? (otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )* )? otherlv_11= ';' )
            {
            // InternalJniMap.g:1086:1: (otherlv_0= 'unmapped' ( (lv_abstract_1_0= 'abstract' ) )? ( (lv_name_2_0= RULE_ID ) ) ( (lv_genericsDecl_3_0= ruleGenericsDeclaration ) )? (otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )? )? (otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )* )? otherlv_11= ';' )
            // InternalJniMap.g:1086:3: otherlv_0= 'unmapped' ( (lv_abstract_1_0= 'abstract' ) )? ( (lv_name_2_0= RULE_ID ) ) ( (lv_genericsDecl_3_0= ruleGenericsDeclaration ) )? (otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )? )? (otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )* )? otherlv_11= ';'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_26); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getUnmappedClassAccess().getUnmappedKeyword_0());
                  
            }
            // InternalJniMap.g:1090:1: ( (lv_abstract_1_0= 'abstract' ) )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==27) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalJniMap.g:1091:1: (lv_abstract_1_0= 'abstract' )
                    {
                    // InternalJniMap.g:1091:1: (lv_abstract_1_0= 'abstract' )
                    // InternalJniMap.g:1092:3: lv_abstract_1_0= 'abstract'
                    {
                    lv_abstract_1_0=(Token)match(input,27,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_abstract_1_0, grammarAccess.getUnmappedClassAccess().getAbstractAbstractKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getUnmappedClassRule());
                      	        }
                             		setWithLastConsumed(current, "abstract", true, "abstract");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalJniMap.g:1105:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalJniMap.g:1106:1: (lv_name_2_0= RULE_ID )
            {
            // InternalJniMap.g:1106:1: (lv_name_2_0= RULE_ID )
            // InternalJniMap.g:1107:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getUnmappedClassAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getUnmappedClassRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:1123:2: ( (lv_genericsDecl_3_0= ruleGenericsDeclaration ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==21) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalJniMap.g:1124:1: (lv_genericsDecl_3_0= ruleGenericsDeclaration )
                    {
                    // InternalJniMap.g:1124:1: (lv_genericsDecl_3_0= ruleGenericsDeclaration )
                    // InternalJniMap.g:1125:3: lv_genericsDecl_3_0= ruleGenericsDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnmappedClassAccess().getGenericsDeclGenericsDeclarationParserRuleCall_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_22);
                    lv_genericsDecl_3_0=ruleGenericsDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnmappedClassRule());
                      	        }
                             		set(
                             			current, 
                             			"genericsDecl",
                              		lv_genericsDecl_3_0, 
                              		"fr.irisa.cairn.JniMap.GenericsDeclaration");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalJniMap.g:1141:3: (otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )? )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==24) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalJniMap.g:1141:5: otherlv_4= 'extending' ( (otherlv_5= RULE_ID ) ) ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )?
                    {
                    otherlv_4=(Token)match(input,24,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_4, grammarAccess.getUnmappedClassAccess().getExtendingKeyword_4_0());
                          
                    }
                    // InternalJniMap.g:1145:1: ( (otherlv_5= RULE_ID ) )
                    // InternalJniMap.g:1146:1: (otherlv_5= RULE_ID )
                    {
                    // InternalJniMap.g:1146:1: (otherlv_5= RULE_ID )
                    // InternalJniMap.g:1147:3: otherlv_5= RULE_ID
                    {
                    if ( state.backtracking==0 ) {
                       
                      		  /* */ 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getUnmappedClassRule());
                      	        }
                              
                    }
                    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_23); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_5, grammarAccess.getUnmappedClassAccess().getSuperClassMappingCrossReference_4_1_0()); 
                      	
                    }

                    }


                    }

                    // InternalJniMap.g:1161:2: ( (lv_genericsInstantiation_6_0= ruleGenericsInstantiation ) )?
                    int alt22=2;
                    int LA22_0 = input.LA(1);

                    if ( (LA22_0==21) ) {
                        alt22=1;
                    }
                    switch (alt22) {
                        case 1 :
                            // InternalJniMap.g:1162:1: (lv_genericsInstantiation_6_0= ruleGenericsInstantiation )
                            {
                            // InternalJniMap.g:1162:1: (lv_genericsInstantiation_6_0= ruleGenericsInstantiation )
                            // InternalJniMap.g:1163:3: lv_genericsInstantiation_6_0= ruleGenericsInstantiation
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getUnmappedClassAccess().getGenericsInstantiationGenericsInstantiationParserRuleCall_4_2_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_24);
                            lv_genericsInstantiation_6_0=ruleGenericsInstantiation();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getUnmappedClassRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"genericsInstantiation",
                                      		lv_genericsInstantiation_6_0, 
                                      		"fr.irisa.cairn.JniMap.GenericsInstantiation");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }
                            break;

                    }


                    }
                    break;

            }

            // InternalJniMap.g:1179:5: (otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )* )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==25) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalJniMap.g:1179:7: otherlv_7= 'implements' ( (lv_interfaces_8_0= ruleInterfaceImplementation ) ) (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )*
                    {
                    otherlv_7=(Token)match(input,25,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_7, grammarAccess.getUnmappedClassAccess().getImplementsKeyword_5_0());
                          
                    }
                    // InternalJniMap.g:1183:1: ( (lv_interfaces_8_0= ruleInterfaceImplementation ) )
                    // InternalJniMap.g:1184:1: (lv_interfaces_8_0= ruleInterfaceImplementation )
                    {
                    // InternalJniMap.g:1184:1: (lv_interfaces_8_0= ruleInterfaceImplementation )
                    // InternalJniMap.g:1185:3: lv_interfaces_8_0= ruleInterfaceImplementation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnmappedClassAccess().getInterfacesInterfaceImplementationParserRuleCall_5_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_25);
                    lv_interfaces_8_0=ruleInterfaceImplementation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnmappedClassRule());
                      	        }
                             		add(
                             			current, 
                             			"interfaces",
                              		lv_interfaces_8_0, 
                              		"fr.irisa.cairn.JniMap.InterfaceImplementation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalJniMap.g:1201:2: (otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) ) )*
                    loop24:
                    do {
                        int alt24=2;
                        int LA24_0 = input.LA(1);

                        if ( (LA24_0==18) ) {
                            alt24=1;
                        }


                        switch (alt24) {
                    	case 1 :
                    	    // InternalJniMap.g:1201:4: otherlv_9= ',' ( (lv_interfaces_10_0= ruleInterfaceImplementation ) )
                    	    {
                    	    otherlv_9=(Token)match(input,18,FOLLOW_4); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_9, grammarAccess.getUnmappedClassAccess().getCommaKeyword_5_2_0());
                    	          
                    	    }
                    	    // InternalJniMap.g:1205:1: ( (lv_interfaces_10_0= ruleInterfaceImplementation ) )
                    	    // InternalJniMap.g:1206:1: (lv_interfaces_10_0= ruleInterfaceImplementation )
                    	    {
                    	    // InternalJniMap.g:1206:1: (lv_interfaces_10_0= ruleInterfaceImplementation )
                    	    // InternalJniMap.g:1207:3: lv_interfaces_10_0= ruleInterfaceImplementation
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getUnmappedClassAccess().getInterfacesInterfaceImplementationParserRuleCall_5_2_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_25);
                    	    lv_interfaces_10_0=ruleInterfaceImplementation();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getUnmappedClassRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"interfaces",
                    	              		lv_interfaces_10_0, 
                    	              		"fr.irisa.cairn.JniMap.InterfaceImplementation");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop24;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_11=(Token)match(input,15,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_11, grammarAccess.getUnmappedClassAccess().getSemicolonKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnmappedClass"


    // $ANTLR start "entryRuleInterfaceClass"
    // InternalJniMap.g:1235:1: entryRuleInterfaceClass returns [EObject current=null] : iv_ruleInterfaceClass= ruleInterfaceClass EOF ;
    public final EObject entryRuleInterfaceClass() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterfaceClass = null;


        try {
            // InternalJniMap.g:1236:2: (iv_ruleInterfaceClass= ruleInterfaceClass EOF )
            // InternalJniMap.g:1237:2: iv_ruleInterfaceClass= ruleInterfaceClass EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInterfaceClassRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInterfaceClass=ruleInterfaceClass();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInterfaceClass; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterfaceClass"


    // $ANTLR start "ruleInterfaceClass"
    // InternalJniMap.g:1244:1: ruleInterfaceClass returns [EObject current=null] : (otherlv_0= 'interface' ( (lv_name_1_0= RULE_ID ) ) ( (lv_genericsDecl_2_0= ruleGenericsDeclaration ) )? (otherlv_3= 'extending' ( (lv_interfaces_4_0= ruleInterfaceImplementation ) ) (otherlv_5= ',' ( (lv_interfaces_6_0= ruleInterfaceImplementation ) ) )* )? otherlv_7= ';' ) ;
    public final EObject ruleInterfaceClass() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_genericsDecl_2_0 = null;

        EObject lv_interfaces_4_0 = null;

        EObject lv_interfaces_6_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:1247:28: ( (otherlv_0= 'interface' ( (lv_name_1_0= RULE_ID ) ) ( (lv_genericsDecl_2_0= ruleGenericsDeclaration ) )? (otherlv_3= 'extending' ( (lv_interfaces_4_0= ruleInterfaceImplementation ) ) (otherlv_5= ',' ( (lv_interfaces_6_0= ruleInterfaceImplementation ) ) )* )? otherlv_7= ';' ) )
            // InternalJniMap.g:1248:1: (otherlv_0= 'interface' ( (lv_name_1_0= RULE_ID ) ) ( (lv_genericsDecl_2_0= ruleGenericsDeclaration ) )? (otherlv_3= 'extending' ( (lv_interfaces_4_0= ruleInterfaceImplementation ) ) (otherlv_5= ',' ( (lv_interfaces_6_0= ruleInterfaceImplementation ) ) )* )? otherlv_7= ';' )
            {
            // InternalJniMap.g:1248:1: (otherlv_0= 'interface' ( (lv_name_1_0= RULE_ID ) ) ( (lv_genericsDecl_2_0= ruleGenericsDeclaration ) )? (otherlv_3= 'extending' ( (lv_interfaces_4_0= ruleInterfaceImplementation ) ) (otherlv_5= ',' ( (lv_interfaces_6_0= ruleInterfaceImplementation ) ) )* )? otherlv_7= ';' )
            // InternalJniMap.g:1248:3: otherlv_0= 'interface' ( (lv_name_1_0= RULE_ID ) ) ( (lv_genericsDecl_2_0= ruleGenericsDeclaration ) )? (otherlv_3= 'extending' ( (lv_interfaces_4_0= ruleInterfaceImplementation ) ) (otherlv_5= ',' ( (lv_interfaces_6_0= ruleInterfaceImplementation ) ) )* )? otherlv_7= ';'
            {
            otherlv_0=(Token)match(input,28,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getInterfaceClassAccess().getInterfaceKeyword_0());
                  
            }
            // InternalJniMap.g:1252:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalJniMap.g:1253:1: (lv_name_1_0= RULE_ID )
            {
            // InternalJniMap.g:1253:1: (lv_name_1_0= RULE_ID )
            // InternalJniMap.g:1254:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getInterfaceClassAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getInterfaceClassRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:1270:2: ( (lv_genericsDecl_2_0= ruleGenericsDeclaration ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==21) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalJniMap.g:1271:1: (lv_genericsDecl_2_0= ruleGenericsDeclaration )
                    {
                    // InternalJniMap.g:1271:1: (lv_genericsDecl_2_0= ruleGenericsDeclaration )
                    // InternalJniMap.g:1272:3: lv_genericsDecl_2_0= ruleGenericsDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getInterfaceClassAccess().getGenericsDeclGenericsDeclarationParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_20);
                    lv_genericsDecl_2_0=ruleGenericsDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getInterfaceClassRule());
                      	        }
                             		set(
                             			current, 
                             			"genericsDecl",
                              		lv_genericsDecl_2_0, 
                              		"fr.irisa.cairn.JniMap.GenericsDeclaration");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalJniMap.g:1288:3: (otherlv_3= 'extending' ( (lv_interfaces_4_0= ruleInterfaceImplementation ) ) (otherlv_5= ',' ( (lv_interfaces_6_0= ruleInterfaceImplementation ) ) )* )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==24) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalJniMap.g:1288:5: otherlv_3= 'extending' ( (lv_interfaces_4_0= ruleInterfaceImplementation ) ) (otherlv_5= ',' ( (lv_interfaces_6_0= ruleInterfaceImplementation ) ) )*
                    {
                    otherlv_3=(Token)match(input,24,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getInterfaceClassAccess().getExtendingKeyword_3_0());
                          
                    }
                    // InternalJniMap.g:1292:1: ( (lv_interfaces_4_0= ruleInterfaceImplementation ) )
                    // InternalJniMap.g:1293:1: (lv_interfaces_4_0= ruleInterfaceImplementation )
                    {
                    // InternalJniMap.g:1293:1: (lv_interfaces_4_0= ruleInterfaceImplementation )
                    // InternalJniMap.g:1294:3: lv_interfaces_4_0= ruleInterfaceImplementation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getInterfaceClassAccess().getInterfacesInterfaceImplementationParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_25);
                    lv_interfaces_4_0=ruleInterfaceImplementation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getInterfaceClassRule());
                      	        }
                             		add(
                             			current, 
                             			"interfaces",
                              		lv_interfaces_4_0, 
                              		"fr.irisa.cairn.JniMap.InterfaceImplementation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalJniMap.g:1310:2: (otherlv_5= ',' ( (lv_interfaces_6_0= ruleInterfaceImplementation ) ) )*
                    loop27:
                    do {
                        int alt27=2;
                        int LA27_0 = input.LA(1);

                        if ( (LA27_0==18) ) {
                            alt27=1;
                        }


                        switch (alt27) {
                    	case 1 :
                    	    // InternalJniMap.g:1310:4: otherlv_5= ',' ( (lv_interfaces_6_0= ruleInterfaceImplementation ) )
                    	    {
                    	    otherlv_5=(Token)match(input,18,FOLLOW_4); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_5, grammarAccess.getInterfaceClassAccess().getCommaKeyword_3_2_0());
                    	          
                    	    }
                    	    // InternalJniMap.g:1314:1: ( (lv_interfaces_6_0= ruleInterfaceImplementation ) )
                    	    // InternalJniMap.g:1315:1: (lv_interfaces_6_0= ruleInterfaceImplementation )
                    	    {
                    	    // InternalJniMap.g:1315:1: (lv_interfaces_6_0= ruleInterfaceImplementation )
                    	    // InternalJniMap.g:1316:3: lv_interfaces_6_0= ruleInterfaceImplementation
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getInterfaceClassAccess().getInterfacesInterfaceImplementationParserRuleCall_3_2_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_25);
                    	    lv_interfaces_6_0=ruleInterfaceImplementation();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getInterfaceClassRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"interfaces",
                    	              		lv_interfaces_6_0, 
                    	              		"fr.irisa.cairn.JniMap.InterfaceImplementation");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop27;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_7=(Token)match(input,15,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_7, grammarAccess.getInterfaceClassAccess().getSemicolonKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterfaceClass"


    // $ANTLR start "entryRuleExternalLibrary"
    // InternalJniMap.g:1344:1: entryRuleExternalLibrary returns [EObject current=null] : iv_ruleExternalLibrary= ruleExternalLibrary EOF ;
    public final EObject entryRuleExternalLibrary() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExternalLibrary = null;


        try {
            // InternalJniMap.g:1345:2: (iv_ruleExternalLibrary= ruleExternalLibrary EOF )
            // InternalJniMap.g:1346:2: iv_ruleExternalLibrary= ruleExternalLibrary EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExternalLibraryRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleExternalLibrary=ruleExternalLibrary();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExternalLibrary; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExternalLibrary"


    // $ANTLR start "ruleExternalLibrary"
    // InternalJniMap.g:1353:1: ruleExternalLibrary returns [EObject current=null] : (otherlv_0= 'use' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'from' ( (otherlv_3= RULE_ID ) ) otherlv_4= ';' ) ;
    public final EObject ruleExternalLibrary() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:1356:28: ( (otherlv_0= 'use' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'from' ( (otherlv_3= RULE_ID ) ) otherlv_4= ';' ) )
            // InternalJniMap.g:1357:1: (otherlv_0= 'use' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'from' ( (otherlv_3= RULE_ID ) ) otherlv_4= ';' )
            {
            // InternalJniMap.g:1357:1: (otherlv_0= 'use' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'from' ( (otherlv_3= RULE_ID ) ) otherlv_4= ';' )
            // InternalJniMap.g:1357:3: otherlv_0= 'use' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'from' ( (otherlv_3= RULE_ID ) ) otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,29,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getExternalLibraryAccess().getUseKeyword_0());
                  
            }
            // InternalJniMap.g:1361:1: ( (otherlv_1= RULE_ID ) )
            // InternalJniMap.g:1362:1: (otherlv_1= RULE_ID )
            {
            // InternalJniMap.g:1362:1: (otherlv_1= RULE_ID )
            // InternalJniMap.g:1363:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getExternalLibraryRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_28); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getExternalLibraryAccess().getLibraryLibraryCrossReference_1_0()); 
              	
            }

            }


            }

            otherlv_2=(Token)match(input,30,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getExternalLibraryAccess().getFromKeyword_2());
                  
            }
            // InternalJniMap.g:1381:1: ( (otherlv_3= RULE_ID ) )
            // InternalJniMap.g:1382:1: (otherlv_3= RULE_ID )
            {
            // InternalJniMap.g:1382:1: (otherlv_3= RULE_ID )
            // InternalJniMap.g:1383:3: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getExternalLibraryRule());
              	        }
                      
            }
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_3, grammarAccess.getExternalLibraryAccess().getMappingMappingCrossReference_3_0()); 
              	
            }

            }


            }

            otherlv_4=(Token)match(input,15,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getExternalLibraryAccess().getSemicolonKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExternalLibrary"


    // $ANTLR start "entryRuleLibrary"
    // InternalJniMap.g:1409:1: entryRuleLibrary returns [EObject current=null] : iv_ruleLibrary= ruleLibrary EOF ;
    public final EObject entryRuleLibrary() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLibrary = null;


        try {
            // InternalJniMap.g:1410:2: (iv_ruleLibrary= ruleLibrary EOF )
            // InternalJniMap.g:1411:2: iv_ruleLibrary= ruleLibrary EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLibraryRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLibrary=ruleLibrary();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLibrary; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLibrary"


    // $ANTLR start "ruleLibrary"
    // InternalJniMap.g:1418:1: ruleLibrary returns [EObject current=null] : (otherlv_0= 'library' otherlv_1= '{' otherlv_2= 'libname' otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= ';' ( ( (lv_filenames_6_0= ruleLibraryFileName ) ) otherlv_7= ';' )+ otherlv_8= '}' ) ;
    public final EObject ruleLibrary() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        EObject lv_filenames_6_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:1421:28: ( (otherlv_0= 'library' otherlv_1= '{' otherlv_2= 'libname' otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= ';' ( ( (lv_filenames_6_0= ruleLibraryFileName ) ) otherlv_7= ';' )+ otherlv_8= '}' ) )
            // InternalJniMap.g:1422:1: (otherlv_0= 'library' otherlv_1= '{' otherlv_2= 'libname' otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= ';' ( ( (lv_filenames_6_0= ruleLibraryFileName ) ) otherlv_7= ';' )+ otherlv_8= '}' )
            {
            // InternalJniMap.g:1422:1: (otherlv_0= 'library' otherlv_1= '{' otherlv_2= 'libname' otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= ';' ( ( (lv_filenames_6_0= ruleLibraryFileName ) ) otherlv_7= ';' )+ otherlv_8= '}' )
            // InternalJniMap.g:1422:3: otherlv_0= 'library' otherlv_1= '{' otherlv_2= 'libname' otherlv_3= '=' ( (lv_name_4_0= RULE_STRING ) ) otherlv_5= ';' ( ( (lv_filenames_6_0= ruleLibraryFileName ) ) otherlv_7= ';' )+ otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,31,FOLLOW_29); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLibraryAccess().getLibraryKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,32,FOLLOW_30); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getLibraryAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,33,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getLibraryAccess().getLibnameKeyword_2());
                  
            }
            otherlv_3=(Token)match(input,14,FOLLOW_8); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getLibraryAccess().getEqualsSignKeyword_3());
                  
            }
            // InternalJniMap.g:1438:1: ( (lv_name_4_0= RULE_STRING ) )
            // InternalJniMap.g:1439:1: (lv_name_4_0= RULE_STRING )
            {
            // InternalJniMap.g:1439:1: (lv_name_4_0= RULE_STRING )
            // InternalJniMap.g:1440:3: lv_name_4_0= RULE_STRING
            {
            lv_name_4_0=(Token)match(input,RULE_STRING,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_4_0, grammarAccess.getLibraryAccess().getNameSTRINGTerminalRuleCall_4_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getLibraryRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_4_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }

            otherlv_5=(Token)match(input,15,FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getLibraryAccess().getSemicolonKeyword_5());
                  
            }
            // InternalJniMap.g:1460:1: ( ( (lv_filenames_6_0= ruleLibraryFileName ) ) otherlv_7= ';' )+
            int cnt29=0;
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( ((LA29_0>=70 && LA29_0<=75)) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalJniMap.g:1460:2: ( (lv_filenames_6_0= ruleLibraryFileName ) ) otherlv_7= ';'
            	    {
            	    // InternalJniMap.g:1460:2: ( (lv_filenames_6_0= ruleLibraryFileName ) )
            	    // InternalJniMap.g:1461:1: (lv_filenames_6_0= ruleLibraryFileName )
            	    {
            	    // InternalJniMap.g:1461:1: (lv_filenames_6_0= ruleLibraryFileName )
            	    // InternalJniMap.g:1462:3: lv_filenames_6_0= ruleLibraryFileName
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getLibraryAccess().getFilenamesLibraryFileNameParserRuleCall_6_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_9);
            	    lv_filenames_6_0=ruleLibraryFileName();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getLibraryRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"filenames",
            	              		lv_filenames_6_0, 
            	              		"fr.irisa.cairn.JniMap.LibraryFileName");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    otherlv_7=(Token)match(input,15,FOLLOW_31); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_7, grammarAccess.getLibraryAccess().getSemicolonKeyword_6_1());
            	          
            	    }

            	    }
            	    break;

            	default :
            	    if ( cnt29 >= 1 ) break loop29;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(29, input);
                        throw eee;
                }
                cnt29++;
            } while (true);

            otherlv_8=(Token)match(input,34,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_8, grammarAccess.getLibraryAccess().getRightCurlyBracketKeyword_7());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLibrary"


    // $ANTLR start "entryRuleLibraryFileName"
    // InternalJniMap.g:1494:1: entryRuleLibraryFileName returns [EObject current=null] : iv_ruleLibraryFileName= ruleLibraryFileName EOF ;
    public final EObject entryRuleLibraryFileName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLibraryFileName = null;


        try {
            // InternalJniMap.g:1495:2: (iv_ruleLibraryFileName= ruleLibraryFileName EOF )
            // InternalJniMap.g:1496:2: iv_ruleLibraryFileName= ruleLibraryFileName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLibraryFileNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLibraryFileName=ruleLibraryFileName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLibraryFileName; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLibraryFileName"


    // $ANTLR start "ruleLibraryFileName"
    // InternalJniMap.g:1503:1: ruleLibraryFileName returns [EObject current=null] : ( ( (lv_os_0_0= ruleHostType ) ) otherlv_1= '=' ( (lv_filename_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleLibraryFileName() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_filename_2_0=null;
        Enumerator lv_os_0_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:1506:28: ( ( ( (lv_os_0_0= ruleHostType ) ) otherlv_1= '=' ( (lv_filename_2_0= RULE_STRING ) ) ) )
            // InternalJniMap.g:1507:1: ( ( (lv_os_0_0= ruleHostType ) ) otherlv_1= '=' ( (lv_filename_2_0= RULE_STRING ) ) )
            {
            // InternalJniMap.g:1507:1: ( ( (lv_os_0_0= ruleHostType ) ) otherlv_1= '=' ( (lv_filename_2_0= RULE_STRING ) ) )
            // InternalJniMap.g:1507:2: ( (lv_os_0_0= ruleHostType ) ) otherlv_1= '=' ( (lv_filename_2_0= RULE_STRING ) )
            {
            // InternalJniMap.g:1507:2: ( (lv_os_0_0= ruleHostType ) )
            // InternalJniMap.g:1508:1: (lv_os_0_0= ruleHostType )
            {
            // InternalJniMap.g:1508:1: (lv_os_0_0= ruleHostType )
            // InternalJniMap.g:1509:3: lv_os_0_0= ruleHostType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLibraryFileNameAccess().getOsHostTypeEnumRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_7);
            lv_os_0_0=ruleHostType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLibraryFileNameRule());
              	        }
                     		set(
                     			current, 
                     			"os",
                      		lv_os_0_0, 
                      		"fr.irisa.cairn.JniMap.HostType");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_8); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getLibraryFileNameAccess().getEqualsSignKeyword_1());
                  
            }
            // InternalJniMap.g:1529:1: ( (lv_filename_2_0= RULE_STRING ) )
            // InternalJniMap.g:1530:1: (lv_filename_2_0= RULE_STRING )
            {
            // InternalJniMap.g:1530:1: (lv_filename_2_0= RULE_STRING )
            // InternalJniMap.g:1531:3: lv_filename_2_0= RULE_STRING
            {
            lv_filename_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_filename_2_0, grammarAccess.getLibraryFileNameAccess().getFilenameSTRINGTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getLibraryFileNameRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"filename",
                      		lv_filename_2_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLibraryFileName"


    // $ANTLR start "entryRuleIncludeDef"
    // InternalJniMap.g:1555:1: entryRuleIncludeDef returns [EObject current=null] : iv_ruleIncludeDef= ruleIncludeDef EOF ;
    public final EObject entryRuleIncludeDef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIncludeDef = null;


        try {
            // InternalJniMap.g:1556:2: (iv_ruleIncludeDef= ruleIncludeDef EOF )
            // InternalJniMap.g:1557:2: iv_ruleIncludeDef= ruleIncludeDef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIncludeDefRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleIncludeDef=ruleIncludeDef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIncludeDef; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIncludeDef"


    // $ANTLR start "ruleIncludeDef"
    // InternalJniMap.g:1564:1: ruleIncludeDef returns [EObject current=null] : (otherlv_0= 'include' ( (lv_include_1_0= RULE_STRING ) ) otherlv_2= ';' ) ;
    public final EObject ruleIncludeDef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_include_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:1567:28: ( (otherlv_0= 'include' ( (lv_include_1_0= RULE_STRING ) ) otherlv_2= ';' ) )
            // InternalJniMap.g:1568:1: (otherlv_0= 'include' ( (lv_include_1_0= RULE_STRING ) ) otherlv_2= ';' )
            {
            // InternalJniMap.g:1568:1: (otherlv_0= 'include' ( (lv_include_1_0= RULE_STRING ) ) otherlv_2= ';' )
            // InternalJniMap.g:1568:3: otherlv_0= 'include' ( (lv_include_1_0= RULE_STRING ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,35,FOLLOW_8); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getIncludeDefAccess().getIncludeKeyword_0());
                  
            }
            // InternalJniMap.g:1572:1: ( (lv_include_1_0= RULE_STRING ) )
            // InternalJniMap.g:1573:1: (lv_include_1_0= RULE_STRING )
            {
            // InternalJniMap.g:1573:1: (lv_include_1_0= RULE_STRING )
            // InternalJniMap.g:1574:3: lv_include_1_0= RULE_STRING
            {
            lv_include_1_0=(Token)match(input,RULE_STRING,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_include_1_0, grammarAccess.getIncludeDefAccess().getIncludeSTRINGTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIncludeDefRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"include",
                      		lv_include_1_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,15,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getIncludeDefAccess().getSemicolonKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIncludeDef"


    // $ANTLR start "entryRuleUserModule"
    // InternalJniMap.g:1602:1: entryRuleUserModule returns [EObject current=null] : iv_ruleUserModule= ruleUserModule EOF ;
    public final EObject entryRuleUserModule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUserModule = null;


        try {
            // InternalJniMap.g:1603:2: (iv_ruleUserModule= ruleUserModule EOF )
            // InternalJniMap.g:1604:2: iv_ruleUserModule= ruleUserModule EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUserModuleRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUserModule=ruleUserModule();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUserModule; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUserModule"


    // $ANTLR start "ruleUserModule"
    // InternalJniMap.g:1611:1: ruleUserModule returns [EObject current=null] : (otherlv_0= 'module' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_methods_3_0= ruleCMethod ) )* otherlv_4= '}' ) ;
    public final EObject ruleUserModule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_methods_3_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:1614:28: ( (otherlv_0= 'module' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_methods_3_0= ruleCMethod ) )* otherlv_4= '}' ) )
            // InternalJniMap.g:1615:1: (otherlv_0= 'module' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_methods_3_0= ruleCMethod ) )* otherlv_4= '}' )
            {
            // InternalJniMap.g:1615:1: (otherlv_0= 'module' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_methods_3_0= ruleCMethod ) )* otherlv_4= '}' )
            // InternalJniMap.g:1615:3: otherlv_0= 'module' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_methods_3_0= ruleCMethod ) )* otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,36,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getUserModuleAccess().getModuleKeyword_0());
                  
            }
            // InternalJniMap.g:1619:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalJniMap.g:1620:1: (lv_name_1_0= RULE_ID )
            {
            // InternalJniMap.g:1620:1: (lv_name_1_0= RULE_ID )
            // InternalJniMap.g:1621:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_29); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getUserModuleAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getUserModuleRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,32,FOLLOW_32); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getUserModuleAccess().getLeftCurlyBracketKeyword_2());
                  
            }
            // InternalJniMap.g:1641:1: ( (lv_methods_3_0= ruleCMethod ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==RULE_ID||(LA30_0>=37 && LA30_0<=38)||LA30_0==40||LA30_0==42||(LA30_0>=55 && LA30_0<=63)||LA30_0==69) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalJniMap.g:1642:1: (lv_methods_3_0= ruleCMethod )
            	    {
            	    // InternalJniMap.g:1642:1: (lv_methods_3_0= ruleCMethod )
            	    // InternalJniMap.g:1643:3: lv_methods_3_0= ruleCMethod
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getUserModuleAccess().getMethodsCMethodParserRuleCall_3_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_32);
            	    lv_methods_3_0=ruleCMethod();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getUserModuleRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"methods",
            	              		lv_methods_3_0, 
            	              		"fr.irisa.cairn.JniMap.CMethod");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

            otherlv_4=(Token)match(input,34,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getUserModuleAccess().getRightCurlyBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUserModule"


    // $ANTLR start "entryRuleCMethod"
    // InternalJniMap.g:1671:1: entryRuleCMethod returns [EObject current=null] : iv_ruleCMethod= ruleCMethod EOF ;
    public final EObject entryRuleCMethod() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCMethod = null;


        try {
            // InternalJniMap.g:1672:2: (iv_ruleCMethod= ruleCMethod EOF )
            // InternalJniMap.g:1673:2: iv_ruleCMethod= ruleCMethod EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCMethodRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCMethod=ruleCMethod();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCMethod; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCMethod"


    // $ANTLR start "ruleCMethod"
    // InternalJniMap.g:1680:1: ruleCMethod returns [EObject current=null] : ( (otherlv_0= 'extern' | otherlv_1= 'static' )? ( (lv_res_2_0= ruleType ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( (lv_params_5_0= ruleParamDef ) ) (otherlv_6= ',' ( (lv_params_7_0= ruleParamDef ) ) )* )? otherlv_8= ')' otherlv_9= ';' ) ;
    public final EObject ruleCMethod() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        EObject lv_res_2_0 = null;

        EObject lv_params_5_0 = null;

        EObject lv_params_7_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:1683:28: ( ( (otherlv_0= 'extern' | otherlv_1= 'static' )? ( (lv_res_2_0= ruleType ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( (lv_params_5_0= ruleParamDef ) ) (otherlv_6= ',' ( (lv_params_7_0= ruleParamDef ) ) )* )? otherlv_8= ')' otherlv_9= ';' ) )
            // InternalJniMap.g:1684:1: ( (otherlv_0= 'extern' | otherlv_1= 'static' )? ( (lv_res_2_0= ruleType ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( (lv_params_5_0= ruleParamDef ) ) (otherlv_6= ',' ( (lv_params_7_0= ruleParamDef ) ) )* )? otherlv_8= ')' otherlv_9= ';' )
            {
            // InternalJniMap.g:1684:1: ( (otherlv_0= 'extern' | otherlv_1= 'static' )? ( (lv_res_2_0= ruleType ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( (lv_params_5_0= ruleParamDef ) ) (otherlv_6= ',' ( (lv_params_7_0= ruleParamDef ) ) )* )? otherlv_8= ')' otherlv_9= ';' )
            // InternalJniMap.g:1684:2: (otherlv_0= 'extern' | otherlv_1= 'static' )? ( (lv_res_2_0= ruleType ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( (lv_params_5_0= ruleParamDef ) ) (otherlv_6= ',' ( (lv_params_7_0= ruleParamDef ) ) )* )? otherlv_8= ')' otherlv_9= ';'
            {
            // InternalJniMap.g:1684:2: (otherlv_0= 'extern' | otherlv_1= 'static' )?
            int alt31=3;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==37) ) {
                alt31=1;
            }
            else if ( (LA31_0==38) ) {
                alt31=2;
            }
            switch (alt31) {
                case 1 :
                    // InternalJniMap.g:1684:4: otherlv_0= 'extern'
                    {
                    otherlv_0=(Token)match(input,37,FOLLOW_33); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_0, grammarAccess.getCMethodAccess().getExternKeyword_0_0());
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalJniMap.g:1689:7: otherlv_1= 'static'
                    {
                    otherlv_1=(Token)match(input,38,FOLLOW_33); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getCMethodAccess().getStaticKeyword_0_1());
                          
                    }

                    }
                    break;

            }

            // InternalJniMap.g:1693:3: ( (lv_res_2_0= ruleType ) )
            // InternalJniMap.g:1694:1: (lv_res_2_0= ruleType )
            {
            // InternalJniMap.g:1694:1: (lv_res_2_0= ruleType )
            // InternalJniMap.g:1695:3: lv_res_2_0= ruleType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCMethodAccess().getResTypeParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_4);
            lv_res_2_0=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCMethodRule());
              	        }
                     		set(
                     			current, 
                     			"res",
                      		lv_res_2_0, 
                      		"fr.irisa.cairn.JniMap.Type");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalJniMap.g:1711:2: ( (lv_name_3_0= RULE_ID ) )
            // InternalJniMap.g:1712:1: (lv_name_3_0= RULE_ID )
            {
            // InternalJniMap.g:1712:1: (lv_name_3_0= RULE_ID )
            // InternalJniMap.g:1713:3: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FOLLOW_34); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_3_0, grammarAccess.getCMethodAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getCMethodRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_3_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,12,FOLLOW_35); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getCMethodAccess().getLeftParenthesisKeyword_3());
                  
            }
            // InternalJniMap.g:1733:1: ( ( (lv_params_5_0= ruleParamDef ) ) (otherlv_6= ',' ( (lv_params_7_0= ruleParamDef ) ) )* )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==RULE_ID||LA33_0==40||(LA33_0>=42 && LA33_0<=45)||(LA33_0>=55 && LA33_0<=63)||LA33_0==69) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalJniMap.g:1733:2: ( (lv_params_5_0= ruleParamDef ) ) (otherlv_6= ',' ( (lv_params_7_0= ruleParamDef ) ) )*
                    {
                    // InternalJniMap.g:1733:2: ( (lv_params_5_0= ruleParamDef ) )
                    // InternalJniMap.g:1734:1: (lv_params_5_0= ruleParamDef )
                    {
                    // InternalJniMap.g:1734:1: (lv_params_5_0= ruleParamDef )
                    // InternalJniMap.g:1735:3: lv_params_5_0= ruleParamDef
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getCMethodAccess().getParamsParamDefParserRuleCall_4_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_36);
                    lv_params_5_0=ruleParamDef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getCMethodRule());
                      	        }
                             		add(
                             			current, 
                             			"params",
                              		lv_params_5_0, 
                              		"fr.irisa.cairn.JniMap.ParamDef");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalJniMap.g:1751:2: (otherlv_6= ',' ( (lv_params_7_0= ruleParamDef ) ) )*
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( (LA32_0==18) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // InternalJniMap.g:1751:4: otherlv_6= ',' ( (lv_params_7_0= ruleParamDef ) )
                    	    {
                    	    otherlv_6=(Token)match(input,18,FOLLOW_37); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_6, grammarAccess.getCMethodAccess().getCommaKeyword_4_1_0());
                    	          
                    	    }
                    	    // InternalJniMap.g:1755:1: ( (lv_params_7_0= ruleParamDef ) )
                    	    // InternalJniMap.g:1756:1: (lv_params_7_0= ruleParamDef )
                    	    {
                    	    // InternalJniMap.g:1756:1: (lv_params_7_0= ruleParamDef )
                    	    // InternalJniMap.g:1757:3: lv_params_7_0= ruleParamDef
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getCMethodAccess().getParamsParamDefParserRuleCall_4_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_36);
                    	    lv_params_7_0=ruleParamDef();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getCMethodRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"params",
                    	              		lv_params_7_0, 
                    	              		"fr.irisa.cairn.JniMap.ParamDef");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop32;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_8=(Token)match(input,16,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_8, grammarAccess.getCMethodAccess().getRightParenthesisKeyword_5());
                  
            }
            otherlv_9=(Token)match(input,15,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_9, grammarAccess.getCMethodAccess().getSemicolonKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCMethod"


    // $ANTLR start "entryRuleTypeAliasDeclaration"
    // InternalJniMap.g:1791:1: entryRuleTypeAliasDeclaration returns [EObject current=null] : iv_ruleTypeAliasDeclaration= ruleTypeAliasDeclaration EOF ;
    public final EObject entryRuleTypeAliasDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeAliasDeclaration = null;


        try {
            // InternalJniMap.g:1792:2: (iv_ruleTypeAliasDeclaration= ruleTypeAliasDeclaration EOF )
            // InternalJniMap.g:1793:2: iv_ruleTypeAliasDeclaration= ruleTypeAliasDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeAliasDeclarationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTypeAliasDeclaration=ruleTypeAliasDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypeAliasDeclaration; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeAliasDeclaration"


    // $ANTLR start "ruleTypeAliasDeclaration"
    // InternalJniMap.g:1800:1: ruleTypeAliasDeclaration returns [EObject current=null] : (otherlv_0= 'typedef' ( (lv_type_1_0= ruleType ) )? ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject ruleTypeAliasDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_2_0=null;
        EObject lv_type_1_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:1803:28: ( (otherlv_0= 'typedef' ( (lv_type_1_0= ruleType ) )? ( (lv_name_2_0= RULE_ID ) ) ) )
            // InternalJniMap.g:1804:1: (otherlv_0= 'typedef' ( (lv_type_1_0= ruleType ) )? ( (lv_name_2_0= RULE_ID ) ) )
            {
            // InternalJniMap.g:1804:1: (otherlv_0= 'typedef' ( (lv_type_1_0= ruleType ) )? ( (lv_name_2_0= RULE_ID ) ) )
            // InternalJniMap.g:1804:3: otherlv_0= 'typedef' ( (lv_type_1_0= ruleType ) )? ( (lv_name_2_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,39,FOLLOW_33); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTypeAliasDeclarationAccess().getTypedefKeyword_0());
                  
            }
            // InternalJniMap.g:1808:1: ( (lv_type_1_0= ruleType ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==40||LA34_0==42||(LA34_0>=55 && LA34_0<=63)||LA34_0==69) ) {
                alt34=1;
            }
            else if ( (LA34_0==RULE_ID) ) {
                int LA34_2 = input.LA(2);

                if ( (LA34_2==RULE_ID||LA34_2==17||LA34_2==64||LA34_2==66) ) {
                    alt34=1;
                }
            }
            switch (alt34) {
                case 1 :
                    // InternalJniMap.g:1809:1: (lv_type_1_0= ruleType )
                    {
                    // InternalJniMap.g:1809:1: (lv_type_1_0= ruleType )
                    // InternalJniMap.g:1810:3: lv_type_1_0= ruleType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getTypeAliasDeclarationAccess().getTypeTypeParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_4);
                    lv_type_1_0=ruleType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getTypeAliasDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"type",
                              		lv_type_1_0, 
                              		"fr.irisa.cairn.JniMap.Type");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalJniMap.g:1826:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalJniMap.g:1827:1: (lv_name_2_0= RULE_ID )
            {
            // InternalJniMap.g:1827:1: (lv_name_2_0= RULE_ID )
            // InternalJniMap.g:1828:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getTypeAliasDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getTypeAliasDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeAliasDeclaration"


    // $ANTLR start "entryRuleStructDeclaration"
    // InternalJniMap.g:1852:1: entryRuleStructDeclaration returns [EObject current=null] : iv_ruleStructDeclaration= ruleStructDeclaration EOF ;
    public final EObject entryRuleStructDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructDeclaration = null;


        try {
            // InternalJniMap.g:1853:2: (iv_ruleStructDeclaration= ruleStructDeclaration EOF )
            // InternalJniMap.g:1854:2: iv_ruleStructDeclaration= ruleStructDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStructDeclarationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleStructDeclaration=ruleStructDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStructDeclaration; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructDeclaration"


    // $ANTLR start "ruleStructDeclaration"
    // InternalJniMap.g:1861:1: ruleStructDeclaration returns [EObject current=null] : (otherlv_0= 'struct' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}' )? ) ;
    public final EObject ruleStructDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_fields_3_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:1864:28: ( (otherlv_0= 'struct' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}' )? ) )
            // InternalJniMap.g:1865:1: (otherlv_0= 'struct' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}' )? )
            {
            // InternalJniMap.g:1865:1: (otherlv_0= 'struct' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}' )? )
            // InternalJniMap.g:1865:3: otherlv_0= 'struct' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}' )?
            {
            otherlv_0=(Token)match(input,40,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getStructDeclarationAccess().getStructKeyword_0());
                  
            }
            // InternalJniMap.g:1869:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalJniMap.g:1870:1: (lv_name_1_0= RULE_ID )
            {
            // InternalJniMap.g:1870:1: (lv_name_1_0= RULE_ID )
            // InternalJniMap.g:1871:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getStructDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStructDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:1887:2: (otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}' )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==32) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalJniMap.g:1887:4: otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}'
                    {
                    otherlv_2=(Token)match(input,32,FOLLOW_39); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getStructDeclarationAccess().getLeftCurlyBracketKeyword_2_0());
                          
                    }
                    // InternalJniMap.g:1891:1: ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+
                    int cnt35=0;
                    loop35:
                    do {
                        int alt35=2;
                        int LA35_0 = input.LA(1);

                        if ( (LA35_0==RULE_ID||LA35_0==40||LA35_0==42||(LA35_0>=55 && LA35_0<=63)||LA35_0==67||LA35_0==69) ) {
                            alt35=1;
                        }


                        switch (alt35) {
                    	case 1 :
                    	    // InternalJniMap.g:1891:2: ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';'
                    	    {
                    	    // InternalJniMap.g:1891:2: ( (lv_fields_3_0= ruleFieldDef ) )
                    	    // InternalJniMap.g:1892:1: (lv_fields_3_0= ruleFieldDef )
                    	    {
                    	    // InternalJniMap.g:1892:1: (lv_fields_3_0= ruleFieldDef )
                    	    // InternalJniMap.g:1893:3: lv_fields_3_0= ruleFieldDef
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getStructDeclarationAccess().getFieldsFieldDefParserRuleCall_2_1_0_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_9);
                    	    lv_fields_3_0=ruleFieldDef();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getStructDeclarationRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"fields",
                    	              		lv_fields_3_0, 
                    	              		"fr.irisa.cairn.JniMap.FieldDef");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }

                    	    otherlv_4=(Token)match(input,15,FOLLOW_40); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_4, grammarAccess.getStructDeclarationAccess().getSemicolonKeyword_2_1_1());
                    	          
                    	    }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt35 >= 1 ) break loop35;
                    	    if (state.backtracking>0) {state.failed=true; return current;}
                                EarlyExitException eee =
                                    new EarlyExitException(35, input);
                                throw eee;
                        }
                        cnt35++;
                    } while (true);

                    otherlv_5=(Token)match(input,34,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getStructDeclarationAccess().getRightCurlyBracketKeyword_2_2());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructDeclaration"


    // $ANTLR start "entryRuleClassDeclaration"
    // InternalJniMap.g:1925:1: entryRuleClassDeclaration returns [EObject current=null] : iv_ruleClassDeclaration= ruleClassDeclaration EOF ;
    public final EObject entryRuleClassDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassDeclaration = null;


        try {
            // InternalJniMap.g:1926:2: (iv_ruleClassDeclaration= ruleClassDeclaration EOF )
            // InternalJniMap.g:1927:2: iv_ruleClassDeclaration= ruleClassDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClassDeclarationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleClassDeclaration=ruleClassDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClassDeclaration; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassDeclaration"


    // $ANTLR start "ruleClassDeclaration"
    // InternalJniMap.g:1934:1: ruleClassDeclaration returns [EObject current=null] : (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}' )? ) ;
    public final EObject ruleClassDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_fields_3_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:1937:28: ( (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}' )? ) )
            // InternalJniMap.g:1938:1: (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}' )? )
            {
            // InternalJniMap.g:1938:1: (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}' )? )
            // InternalJniMap.g:1938:3: otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}' )?
            {
            otherlv_0=(Token)match(input,41,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getClassDeclarationAccess().getClassKeyword_0());
                  
            }
            // InternalJniMap.g:1942:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalJniMap.g:1943:1: (lv_name_1_0= RULE_ID )
            {
            // InternalJniMap.g:1943:1: (lv_name_1_0= RULE_ID )
            // InternalJniMap.g:1944:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getClassDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getClassDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:1960:2: (otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}' )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==32) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalJniMap.g:1960:4: otherlv_2= '{' ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+ otherlv_5= '}'
                    {
                    otherlv_2=(Token)match(input,32,FOLLOW_39); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getClassDeclarationAccess().getLeftCurlyBracketKeyword_2_0());
                          
                    }
                    // InternalJniMap.g:1964:1: ( ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';' )+
                    int cnt37=0;
                    loop37:
                    do {
                        int alt37=2;
                        int LA37_0 = input.LA(1);

                        if ( (LA37_0==RULE_ID||LA37_0==40||LA37_0==42||(LA37_0>=55 && LA37_0<=63)||LA37_0==67||LA37_0==69) ) {
                            alt37=1;
                        }


                        switch (alt37) {
                    	case 1 :
                    	    // InternalJniMap.g:1964:2: ( (lv_fields_3_0= ruleFieldDef ) ) otherlv_4= ';'
                    	    {
                    	    // InternalJniMap.g:1964:2: ( (lv_fields_3_0= ruleFieldDef ) )
                    	    // InternalJniMap.g:1965:1: (lv_fields_3_0= ruleFieldDef )
                    	    {
                    	    // InternalJniMap.g:1965:1: (lv_fields_3_0= ruleFieldDef )
                    	    // InternalJniMap.g:1966:3: lv_fields_3_0= ruleFieldDef
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getClassDeclarationAccess().getFieldsFieldDefParserRuleCall_2_1_0_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_9);
                    	    lv_fields_3_0=ruleFieldDef();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getClassDeclarationRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"fields",
                    	              		lv_fields_3_0, 
                    	              		"fr.irisa.cairn.JniMap.FieldDef");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }

                    	    otherlv_4=(Token)match(input,15,FOLLOW_40); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_4, grammarAccess.getClassDeclarationAccess().getSemicolonKeyword_2_1_1());
                    	          
                    	    }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt37 >= 1 ) break loop37;
                    	    if (state.backtracking>0) {state.failed=true; return current;}
                                EarlyExitException eee =
                                    new EarlyExitException(37, input);
                                throw eee;
                        }
                        cnt37++;
                    } while (true);

                    otherlv_5=(Token)match(input,34,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getClassDeclarationAccess().getRightCurlyBracketKeyword_2_2());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassDeclaration"


    // $ANTLR start "entryRuleEnumDeclaration"
    // InternalJniMap.g:1998:1: entryRuleEnumDeclaration returns [EObject current=null] : iv_ruleEnumDeclaration= ruleEnumDeclaration EOF ;
    public final EObject entryRuleEnumDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumDeclaration = null;


        try {
            // InternalJniMap.g:1999:2: (iv_ruleEnumDeclaration= ruleEnumDeclaration EOF )
            // InternalJniMap.g:2000:2: iv_ruleEnumDeclaration= ruleEnumDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumDeclarationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEnumDeclaration=ruleEnumDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumDeclaration; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumDeclaration"


    // $ANTLR start "ruleEnumDeclaration"
    // InternalJniMap.g:2007:1: ruleEnumDeclaration returns [EObject current=null] : (otherlv_0= 'enum' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_values_3_0= ruleEnumValue ) ) (otherlv_4= ',' ( (lv_values_5_0= ruleEnumValue ) ) )* otherlv_6= '}' )? ) ;
    public final EObject ruleEnumDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_values_3_0 = null;

        EObject lv_values_5_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:2010:28: ( (otherlv_0= 'enum' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_values_3_0= ruleEnumValue ) ) (otherlv_4= ',' ( (lv_values_5_0= ruleEnumValue ) ) )* otherlv_6= '}' )? ) )
            // InternalJniMap.g:2011:1: (otherlv_0= 'enum' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_values_3_0= ruleEnumValue ) ) (otherlv_4= ',' ( (lv_values_5_0= ruleEnumValue ) ) )* otherlv_6= '}' )? )
            {
            // InternalJniMap.g:2011:1: (otherlv_0= 'enum' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_values_3_0= ruleEnumValue ) ) (otherlv_4= ',' ( (lv_values_5_0= ruleEnumValue ) ) )* otherlv_6= '}' )? )
            // InternalJniMap.g:2011:3: otherlv_0= 'enum' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_values_3_0= ruleEnumValue ) ) (otherlv_4= ',' ( (lv_values_5_0= ruleEnumValue ) ) )* otherlv_6= '}' )?
            {
            otherlv_0=(Token)match(input,42,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getEnumDeclarationAccess().getEnumKeyword_0());
                  
            }
            // InternalJniMap.g:2015:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalJniMap.g:2016:1: (lv_name_1_0= RULE_ID )
            {
            // InternalJniMap.g:2016:1: (lv_name_1_0= RULE_ID )
            // InternalJniMap.g:2017:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getEnumDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:2033:2: (otherlv_2= '{' ( (lv_values_3_0= ruleEnumValue ) ) (otherlv_4= ',' ( (lv_values_5_0= ruleEnumValue ) ) )* otherlv_6= '}' )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==32) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalJniMap.g:2033:4: otherlv_2= '{' ( (lv_values_3_0= ruleEnumValue ) ) (otherlv_4= ',' ( (lv_values_5_0= ruleEnumValue ) ) )* otherlv_6= '}'
                    {
                    otherlv_2=(Token)match(input,32,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getEnumDeclarationAccess().getLeftCurlyBracketKeyword_2_0());
                          
                    }
                    // InternalJniMap.g:2037:1: ( (lv_values_3_0= ruleEnumValue ) )
                    // InternalJniMap.g:2038:1: (lv_values_3_0= ruleEnumValue )
                    {
                    // InternalJniMap.g:2038:1: (lv_values_3_0= ruleEnumValue )
                    // InternalJniMap.g:2039:3: lv_values_3_0= ruleEnumValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getEnumDeclarationAccess().getValuesEnumValueParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_41);
                    lv_values_3_0=ruleEnumValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getEnumDeclarationRule());
                      	        }
                             		add(
                             			current, 
                             			"values",
                              		lv_values_3_0, 
                              		"fr.irisa.cairn.JniMap.EnumValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalJniMap.g:2055:2: (otherlv_4= ',' ( (lv_values_5_0= ruleEnumValue ) ) )*
                    loop39:
                    do {
                        int alt39=2;
                        int LA39_0 = input.LA(1);

                        if ( (LA39_0==18) ) {
                            alt39=1;
                        }


                        switch (alt39) {
                    	case 1 :
                    	    // InternalJniMap.g:2055:4: otherlv_4= ',' ( (lv_values_5_0= ruleEnumValue ) )
                    	    {
                    	    otherlv_4=(Token)match(input,18,FOLLOW_4); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_4, grammarAccess.getEnumDeclarationAccess().getCommaKeyword_2_2_0());
                    	          
                    	    }
                    	    // InternalJniMap.g:2059:1: ( (lv_values_5_0= ruleEnumValue ) )
                    	    // InternalJniMap.g:2060:1: (lv_values_5_0= ruleEnumValue )
                    	    {
                    	    // InternalJniMap.g:2060:1: (lv_values_5_0= ruleEnumValue )
                    	    // InternalJniMap.g:2061:3: lv_values_5_0= ruleEnumValue
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getEnumDeclarationAccess().getValuesEnumValueParserRuleCall_2_2_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_41);
                    	    lv_values_5_0=ruleEnumValue();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getEnumDeclarationRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"values",
                    	              		lv_values_5_0, 
                    	              		"fr.irisa.cairn.JniMap.EnumValue");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop39;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,34,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getEnumDeclarationAccess().getRightCurlyBracketKeyword_2_3());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumDeclaration"


    // $ANTLR start "entryRuleEnumType"
    // InternalJniMap.g:2089:1: entryRuleEnumType returns [EObject current=null] : iv_ruleEnumType= ruleEnumType EOF ;
    public final EObject entryRuleEnumType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumType = null;


        try {
            // InternalJniMap.g:2090:2: (iv_ruleEnumType= ruleEnumType EOF )
            // InternalJniMap.g:2091:2: iv_ruleEnumType= ruleEnumType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEnumType=ruleEnumType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumType"


    // $ANTLR start "ruleEnumType"
    // InternalJniMap.g:2098:1: ruleEnumType returns [EObject current=null] : (otherlv_0= 'enum' ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleEnumType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:2101:28: ( (otherlv_0= 'enum' ( (otherlv_1= RULE_ID ) ) ) )
            // InternalJniMap.g:2102:1: (otherlv_0= 'enum' ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalJniMap.g:2102:1: (otherlv_0= 'enum' ( (otherlv_1= RULE_ID ) ) )
            // InternalJniMap.g:2102:3: otherlv_0= 'enum' ( (otherlv_1= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,42,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getEnumTypeAccess().getEnumKeyword_0());
                  
            }
            // InternalJniMap.g:2106:1: ( (otherlv_1= RULE_ID ) )
            // InternalJniMap.g:2107:1: (otherlv_1= RULE_ID )
            {
            // InternalJniMap.g:2107:1: (otherlv_1= RULE_ID )
            // InternalJniMap.g:2108:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumTypeRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getEnumTypeAccess().getNameEnumDeclarationCrossReference_1_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumType"


    // $ANTLR start "entryRuleEnumValue"
    // InternalJniMap.g:2130:1: entryRuleEnumValue returns [EObject current=null] : iv_ruleEnumValue= ruleEnumValue EOF ;
    public final EObject entryRuleEnumValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumValue = null;


        try {
            // InternalJniMap.g:2131:2: (iv_ruleEnumValue= ruleEnumValue EOF )
            // InternalJniMap.g:2132:2: iv_ruleEnumValue= ruleEnumValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumValueRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEnumValue=ruleEnumValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumValue; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // InternalJniMap.g:2139:1: ruleEnumValue returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '=' ( ( (lv_value_2_0= RULE_INT ) ) | ( (lv_enumNext_3_0= ruleEnumValue ) ) ) )? ) ;
    public final EObject ruleEnumValue() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_value_2_0=null;
        EObject lv_enumNext_3_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:2142:28: ( ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '=' ( ( (lv_value_2_0= RULE_INT ) ) | ( (lv_enumNext_3_0= ruleEnumValue ) ) ) )? ) )
            // InternalJniMap.g:2143:1: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '=' ( ( (lv_value_2_0= RULE_INT ) ) | ( (lv_enumNext_3_0= ruleEnumValue ) ) ) )? )
            {
            // InternalJniMap.g:2143:1: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '=' ( ( (lv_value_2_0= RULE_INT ) ) | ( (lv_enumNext_3_0= ruleEnumValue ) ) ) )? )
            // InternalJniMap.g:2143:2: ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '=' ( ( (lv_value_2_0= RULE_INT ) ) | ( (lv_enumNext_3_0= ruleEnumValue ) ) ) )?
            {
            // InternalJniMap.g:2143:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalJniMap.g:2144:1: (lv_name_0_0= RULE_ID )
            {
            // InternalJniMap.g:2144:1: (lv_name_0_0= RULE_ID )
            // InternalJniMap.g:2145:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_42); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getEnumValueAccess().getNameIDTerminalRuleCall_0_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:2161:2: (otherlv_1= '=' ( ( (lv_value_2_0= RULE_INT ) ) | ( (lv_enumNext_3_0= ruleEnumValue ) ) ) )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==14) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalJniMap.g:2161:4: otherlv_1= '=' ( ( (lv_value_2_0= RULE_INT ) ) | ( (lv_enumNext_3_0= ruleEnumValue ) ) )
                    {
                    otherlv_1=(Token)match(input,14,FOLLOW_43); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getEnumValueAccess().getEqualsSignKeyword_1_0());
                          
                    }
                    // InternalJniMap.g:2165:1: ( ( (lv_value_2_0= RULE_INT ) ) | ( (lv_enumNext_3_0= ruleEnumValue ) ) )
                    int alt41=2;
                    int LA41_0 = input.LA(1);

                    if ( (LA41_0==RULE_INT) ) {
                        alt41=1;
                    }
                    else if ( (LA41_0==RULE_ID) ) {
                        alt41=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 41, 0, input);

                        throw nvae;
                    }
                    switch (alt41) {
                        case 1 :
                            // InternalJniMap.g:2165:2: ( (lv_value_2_0= RULE_INT ) )
                            {
                            // InternalJniMap.g:2165:2: ( (lv_value_2_0= RULE_INT ) )
                            // InternalJniMap.g:2166:1: (lv_value_2_0= RULE_INT )
                            {
                            // InternalJniMap.g:2166:1: (lv_value_2_0= RULE_INT )
                            // InternalJniMap.g:2167:3: lv_value_2_0= RULE_INT
                            {
                            lv_value_2_0=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_value_2_0, grammarAccess.getEnumValueAccess().getValueINTTerminalRuleCall_1_1_0_0()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getEnumValueRule());
                              	        }
                                     		setWithLastConsumed(
                                     			current, 
                                     			"value",
                                      		lv_value_2_0, 
                                      		"org.eclipse.xtext.common.Terminals.INT");
                              	    
                            }

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalJniMap.g:2184:6: ( (lv_enumNext_3_0= ruleEnumValue ) )
                            {
                            // InternalJniMap.g:2184:6: ( (lv_enumNext_3_0= ruleEnumValue ) )
                            // InternalJniMap.g:2185:1: (lv_enumNext_3_0= ruleEnumValue )
                            {
                            // InternalJniMap.g:2185:1: (lv_enumNext_3_0= ruleEnumValue )
                            // InternalJniMap.g:2186:3: lv_enumNext_3_0= ruleEnumValue
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getEnumValueAccess().getEnumNextEnumValueParserRuleCall_1_1_1_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_2);
                            lv_enumNext_3_0=ruleEnumValue();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getEnumValueRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"enumNext",
                                      		lv_enumNext_3_0, 
                                      		"fr.irisa.cairn.JniMap.EnumValue");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleParamDef"
    // InternalJniMap.g:2210:1: entryRuleParamDef returns [EObject current=null] : iv_ruleParamDef= ruleParamDef EOF ;
    public final EObject entryRuleParamDef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParamDef = null;


        try {
            // InternalJniMap.g:2211:2: (iv_ruleParamDef= ruleParamDef EOF )
            // InternalJniMap.g:2212:2: iv_ruleParamDef= ruleParamDef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParamDefRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleParamDef=ruleParamDef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParamDef; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParamDef"


    // $ANTLR start "ruleParamDef"
    // InternalJniMap.g:2219:1: ruleParamDef returns [EObject current=null] : ( ( ( (lv_give_0_0= 'give' ) ) | ( (lv_take_1_0= 'take' ) ) | ( (lv_keep_2_0= 'keep' ) ) )? ( (lv_type_3_0= ruleType ) ) ( (lv_name_4_0= RULE_ID ) ) ) ;
    public final EObject ruleParamDef() throws RecognitionException {
        EObject current = null;

        Token lv_give_0_0=null;
        Token lv_take_1_0=null;
        Token lv_keep_2_0=null;
        Token lv_name_4_0=null;
        EObject lv_type_3_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:2222:28: ( ( ( ( (lv_give_0_0= 'give' ) ) | ( (lv_take_1_0= 'take' ) ) | ( (lv_keep_2_0= 'keep' ) ) )? ( (lv_type_3_0= ruleType ) ) ( (lv_name_4_0= RULE_ID ) ) ) )
            // InternalJniMap.g:2223:1: ( ( ( (lv_give_0_0= 'give' ) ) | ( (lv_take_1_0= 'take' ) ) | ( (lv_keep_2_0= 'keep' ) ) )? ( (lv_type_3_0= ruleType ) ) ( (lv_name_4_0= RULE_ID ) ) )
            {
            // InternalJniMap.g:2223:1: ( ( ( (lv_give_0_0= 'give' ) ) | ( (lv_take_1_0= 'take' ) ) | ( (lv_keep_2_0= 'keep' ) ) )? ( (lv_type_3_0= ruleType ) ) ( (lv_name_4_0= RULE_ID ) ) )
            // InternalJniMap.g:2223:2: ( ( (lv_give_0_0= 'give' ) ) | ( (lv_take_1_0= 'take' ) ) | ( (lv_keep_2_0= 'keep' ) ) )? ( (lv_type_3_0= ruleType ) ) ( (lv_name_4_0= RULE_ID ) )
            {
            // InternalJniMap.g:2223:2: ( ( (lv_give_0_0= 'give' ) ) | ( (lv_take_1_0= 'take' ) ) | ( (lv_keep_2_0= 'keep' ) ) )?
            int alt43=4;
            switch ( input.LA(1) ) {
                case 43:
                    {
                    alt43=1;
                    }
                    break;
                case 44:
                    {
                    alt43=2;
                    }
                    break;
                case 45:
                    {
                    alt43=3;
                    }
                    break;
            }

            switch (alt43) {
                case 1 :
                    // InternalJniMap.g:2223:3: ( (lv_give_0_0= 'give' ) )
                    {
                    // InternalJniMap.g:2223:3: ( (lv_give_0_0= 'give' ) )
                    // InternalJniMap.g:2224:1: (lv_give_0_0= 'give' )
                    {
                    // InternalJniMap.g:2224:1: (lv_give_0_0= 'give' )
                    // InternalJniMap.g:2225:3: lv_give_0_0= 'give'
                    {
                    lv_give_0_0=(Token)match(input,43,FOLLOW_33); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_give_0_0, grammarAccess.getParamDefAccess().getGiveGiveKeyword_0_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getParamDefRule());
                      	        }
                             		setWithLastConsumed(current, "give", true, "give");
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:2239:6: ( (lv_take_1_0= 'take' ) )
                    {
                    // InternalJniMap.g:2239:6: ( (lv_take_1_0= 'take' ) )
                    // InternalJniMap.g:2240:1: (lv_take_1_0= 'take' )
                    {
                    // InternalJniMap.g:2240:1: (lv_take_1_0= 'take' )
                    // InternalJniMap.g:2241:3: lv_take_1_0= 'take'
                    {
                    lv_take_1_0=(Token)match(input,44,FOLLOW_33); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_take_1_0, grammarAccess.getParamDefAccess().getTakeTakeKeyword_0_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getParamDefRule());
                      	        }
                             		setWithLastConsumed(current, "take", true, "take");
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalJniMap.g:2255:6: ( (lv_keep_2_0= 'keep' ) )
                    {
                    // InternalJniMap.g:2255:6: ( (lv_keep_2_0= 'keep' ) )
                    // InternalJniMap.g:2256:1: (lv_keep_2_0= 'keep' )
                    {
                    // InternalJniMap.g:2256:1: (lv_keep_2_0= 'keep' )
                    // InternalJniMap.g:2257:3: lv_keep_2_0= 'keep'
                    {
                    lv_keep_2_0=(Token)match(input,45,FOLLOW_33); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_keep_2_0, grammarAccess.getParamDefAccess().getKeepKeepKeyword_0_2_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getParamDefRule());
                      	        }
                             		setWithLastConsumed(current, "keep", true, "keep");
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalJniMap.g:2270:4: ( (lv_type_3_0= ruleType ) )
            // InternalJniMap.g:2271:1: (lv_type_3_0= ruleType )
            {
            // InternalJniMap.g:2271:1: (lv_type_3_0= ruleType )
            // InternalJniMap.g:2272:3: lv_type_3_0= ruleType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getParamDefAccess().getTypeTypeParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_4);
            lv_type_3_0=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getParamDefRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_3_0, 
                      		"fr.irisa.cairn.JniMap.Type");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalJniMap.g:2288:2: ( (lv_name_4_0= RULE_ID ) )
            // InternalJniMap.g:2289:1: (lv_name_4_0= RULE_ID )
            {
            // InternalJniMap.g:2289:1: (lv_name_4_0= RULE_ID )
            // InternalJniMap.g:2290:3: lv_name_4_0= RULE_ID
            {
            lv_name_4_0=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_4_0, grammarAccess.getParamDefAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getParamDefRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_4_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParamDef"


    // $ANTLR start "entryRuleMethodGroup"
    // InternalJniMap.g:2314:1: entryRuleMethodGroup returns [EObject current=null] : iv_ruleMethodGroup= ruleMethodGroup EOF ;
    public final EObject entryRuleMethodGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMethodGroup = null;


        try {
            // InternalJniMap.g:2315:2: (iv_ruleMethodGroup= ruleMethodGroup EOF )
            // InternalJniMap.g:2316:2: iv_ruleMethodGroup= ruleMethodGroup EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMethodGroupRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMethodGroup=ruleMethodGroup();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMethodGroup; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMethodGroup"


    // $ANTLR start "ruleMethodGroup"
    // InternalJniMap.g:2323:1: ruleMethodGroup returns [EObject current=null] : (otherlv_0= 'group' ( (otherlv_1= RULE_ID ) ) otherlv_2= '{' ( (lv_methods_3_0= ruleMethod ) )+ otherlv_4= '}' ) ;
    public final EObject ruleMethodGroup() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_methods_3_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:2326:28: ( (otherlv_0= 'group' ( (otherlv_1= RULE_ID ) ) otherlv_2= '{' ( (lv_methods_3_0= ruleMethod ) )+ otherlv_4= '}' ) )
            // InternalJniMap.g:2327:1: (otherlv_0= 'group' ( (otherlv_1= RULE_ID ) ) otherlv_2= '{' ( (lv_methods_3_0= ruleMethod ) )+ otherlv_4= '}' )
            {
            // InternalJniMap.g:2327:1: (otherlv_0= 'group' ( (otherlv_1= RULE_ID ) ) otherlv_2= '{' ( (lv_methods_3_0= ruleMethod ) )+ otherlv_4= '}' )
            // InternalJniMap.g:2327:3: otherlv_0= 'group' ( (otherlv_1= RULE_ID ) ) otherlv_2= '{' ( (lv_methods_3_0= ruleMethod ) )+ otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,46,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getMethodGroupAccess().getGroupKeyword_0());
                  
            }
            // InternalJniMap.g:2331:1: ( (otherlv_1= RULE_ID ) )
            // InternalJniMap.g:2332:1: (otherlv_1= RULE_ID )
            {
            // InternalJniMap.g:2332:1: (otherlv_1= RULE_ID )
            // InternalJniMap.g:2333:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getMethodGroupRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_29); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getMethodGroupAccess().getClassClassMappingCrossReference_1_0()); 
              	
            }

            }


            }

            otherlv_2=(Token)match(input,32,FOLLOW_44); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getMethodGroupAccess().getLeftCurlyBracketKeyword_2());
                  
            }
            // InternalJniMap.g:2351:1: ( (lv_methods_3_0= ruleMethod ) )+
            int cnt44=0;
            loop44:
            do {
                int alt44=2;
                int LA44_0 = input.LA(1);

                if ( (LA44_0==RULE_ID||LA44_0==17||(LA44_0>=37 && LA44_0<=38)||LA44_0==40||LA44_0==42||(LA44_0>=55 && LA44_0<=63)||LA44_0==69) ) {
                    alt44=1;
                }


                switch (alt44) {
            	case 1 :
            	    // InternalJniMap.g:2352:1: (lv_methods_3_0= ruleMethod )
            	    {
            	    // InternalJniMap.g:2352:1: (lv_methods_3_0= ruleMethod )
            	    // InternalJniMap.g:2353:3: lv_methods_3_0= ruleMethod
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getMethodGroupAccess().getMethodsMethodParserRuleCall_3_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_45);
            	    lv_methods_3_0=ruleMethod();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getMethodGroupRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"methods",
            	              		lv_methods_3_0, 
            	              		"fr.irisa.cairn.JniMap.Method");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt44 >= 1 ) break loop44;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(44, input);
                        throw eee;
                }
                cnt44++;
            } while (true);

            otherlv_4=(Token)match(input,34,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getMethodGroupAccess().getRightCurlyBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMethodGroup"


    // $ANTLR start "entryRuleMethod"
    // InternalJniMap.g:2381:1: entryRuleMethod returns [EObject current=null] : iv_ruleMethod= ruleMethod EOF ;
    public final EObject entryRuleMethod() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMethod = null;


         
        		UnorderedGroupState myUnorderedGroupState = getUnorderedGroupHelper().snapShot(
        			grammarAccess.getMethodAccess().getUnorderedGroup_0_1()
        		);
        	
        try {
            // InternalJniMap.g:2387:2: (iv_ruleMethod= ruleMethod EOF )
            // InternalJniMap.g:2388:2: iv_ruleMethod= ruleMethod EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMethodRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMethod=ruleMethod();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMethod; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myUnorderedGroupState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleMethod"


    // $ANTLR start "ruleMethod"
    // InternalJniMap.g:2398:1: ruleMethod returns [EObject current=null] : ( (otherlv_0= '[' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )* ) ) ) otherlv_15= ']' )? (otherlv_16= 'extern' | otherlv_17= 'static' )? ( (lv_res_18_0= ruleType ) ) ( (lv_name_19_0= RULE_ID ) ) otherlv_20= '(' ( ( (lv_params_21_0= ruleParamDef ) ) (otherlv_22= ',' ( (lv_params_23_0= ruleParamDef ) ) )* )? otherlv_24= ')' otherlv_25= ';' ) ;
    public final EObject ruleMethod() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_constructor_2_0=null;
        Token lv_destructor_3_0=null;
        Token lv_instanceof_4_0=null;
        Token lv_static_5_0=null;
        Token lv_private_6_0=null;
        Token lv_protected_7_0=null;
        Token lv_renamed_8_0=null;
        Token otherlv_9=null;
        Token lv_newname_10_0=null;
        Token lv_stub_11_0=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token lv_arg_14_0=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token lv_name_19_0=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        EObject lv_res_18_0 = null;

        EObject lv_params_21_0 = null;

        EObject lv_params_23_0 = null;


         enterRule(); 
        		UnorderedGroupState myUnorderedGroupState = getUnorderedGroupHelper().snapShot(
        			grammarAccess.getMethodAccess().getUnorderedGroup_0_1()
        		);
            
        try {
            // InternalJniMap.g:2404:28: ( ( (otherlv_0= '[' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )* ) ) ) otherlv_15= ']' )? (otherlv_16= 'extern' | otherlv_17= 'static' )? ( (lv_res_18_0= ruleType ) ) ( (lv_name_19_0= RULE_ID ) ) otherlv_20= '(' ( ( (lv_params_21_0= ruleParamDef ) ) (otherlv_22= ',' ( (lv_params_23_0= ruleParamDef ) ) )* )? otherlv_24= ')' otherlv_25= ';' ) )
            // InternalJniMap.g:2405:1: ( (otherlv_0= '[' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )* ) ) ) otherlv_15= ']' )? (otherlv_16= 'extern' | otherlv_17= 'static' )? ( (lv_res_18_0= ruleType ) ) ( (lv_name_19_0= RULE_ID ) ) otherlv_20= '(' ( ( (lv_params_21_0= ruleParamDef ) ) (otherlv_22= ',' ( (lv_params_23_0= ruleParamDef ) ) )* )? otherlv_24= ')' otherlv_25= ';' )
            {
            // InternalJniMap.g:2405:1: ( (otherlv_0= '[' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )* ) ) ) otherlv_15= ']' )? (otherlv_16= 'extern' | otherlv_17= 'static' )? ( (lv_res_18_0= ruleType ) ) ( (lv_name_19_0= RULE_ID ) ) otherlv_20= '(' ( ( (lv_params_21_0= ruleParamDef ) ) (otherlv_22= ',' ( (lv_params_23_0= ruleParamDef ) ) )* )? otherlv_24= ')' otherlv_25= ';' )
            // InternalJniMap.g:2405:2: (otherlv_0= '[' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )* ) ) ) otherlv_15= ']' )? (otherlv_16= 'extern' | otherlv_17= 'static' )? ( (lv_res_18_0= ruleType ) ) ( (lv_name_19_0= RULE_ID ) ) otherlv_20= '(' ( ( (lv_params_21_0= ruleParamDef ) ) (otherlv_22= ',' ( (lv_params_23_0= ruleParamDef ) ) )* )? otherlv_24= ')' otherlv_25= ';'
            {
            // InternalJniMap.g:2405:2: (otherlv_0= '[' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )* ) ) ) otherlv_15= ']' )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==17) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalJniMap.g:2405:4: otherlv_0= '[' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )* ) ) ) otherlv_15= ']'
                    {
                    otherlv_0=(Token)match(input,17,FOLLOW_46); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_0, grammarAccess.getMethodAccess().getLeftSquareBracketKeyword_0_0());
                          
                    }
                    // InternalJniMap.g:2409:1: ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )* ) ) )
                    // InternalJniMap.g:2411:1: ( ( ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )* ) )
                    {
                    // InternalJniMap.g:2411:1: ( ( ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )* ) )
                    // InternalJniMap.g:2412:2: ( ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )* )
                    {
                    getUnorderedGroupHelper().enter(grammarAccess.getMethodAccess().getUnorderedGroup_0_1());
                    // InternalJniMap.g:2415:2: ( ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )* )
                    // InternalJniMap.g:2416:3: ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )*
                    {
                    // InternalJniMap.g:2416:3: ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )*
                    loop47:
                    do {
                        int alt47=7;
                        alt47 = dfa47.predict(input);
                        switch (alt47) {
                    	case 1 :
                    	    // InternalJniMap.g:2418:4: ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) )
                    	    {
                    	    // InternalJniMap.g:2418:4: ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) )
                    	    // InternalJniMap.g:2419:5: {...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        throw new FailedPredicateException(input, "ruleMethod", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0)");
                    	    }
                    	    // InternalJniMap.g:2419:105: ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) )
                    	    // InternalJniMap.g:2420:6: ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) )
                    	    {
                    	    getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0);
                    	    // InternalJniMap.g:2423:6: ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) )
                    	    // InternalJniMap.g:2423:7: {...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) )
                    	    {
                    	    if ( !((true)) ) {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        throw new FailedPredicateException(input, "ruleMethod", "true");
                    	    }
                    	    // InternalJniMap.g:2423:16: ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) )
                    	    int alt45=3;
                    	    switch ( input.LA(1) ) {
                    	    case 47:
                    	        {
                    	        alt45=1;
                    	        }
                    	        break;
                    	    case 48:
                    	        {
                    	        alt45=2;
                    	        }
                    	        break;
                    	    case 49:
                    	        {
                    	        alt45=3;
                    	        }
                    	        break;
                    	    default:
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        NoViableAltException nvae =
                    	            new NoViableAltException("", 45, 0, input);

                    	        throw nvae;
                    	    }

                    	    switch (alt45) {
                    	        case 1 :
                    	            // InternalJniMap.g:2423:17: ( (lv_constructor_2_0= 'constructor' ) )
                    	            {
                    	            // InternalJniMap.g:2423:17: ( (lv_constructor_2_0= 'constructor' ) )
                    	            // InternalJniMap.g:2424:1: (lv_constructor_2_0= 'constructor' )
                    	            {
                    	            // InternalJniMap.g:2424:1: (lv_constructor_2_0= 'constructor' )
                    	            // InternalJniMap.g:2425:3: lv_constructor_2_0= 'constructor'
                    	            {
                    	            lv_constructor_2_0=(Token)match(input,47,FOLLOW_46); if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	                      newLeafNode(lv_constructor_2_0, grammarAccess.getMethodAccess().getConstructorConstructorKeyword_0_1_0_0_0());
                    	                  
                    	            }
                    	            if ( state.backtracking==0 ) {

                    	              	        if (current==null) {
                    	              	            current = createModelElement(grammarAccess.getMethodRule());
                    	              	        }
                    	                     		setWithLastConsumed(current, "constructor", true, "constructor");
                    	              	    
                    	            }

                    	            }


                    	            }


                    	            }
                    	            break;
                    	        case 2 :
                    	            // InternalJniMap.g:2439:6: ( (lv_destructor_3_0= 'destructor' ) )
                    	            {
                    	            // InternalJniMap.g:2439:6: ( (lv_destructor_3_0= 'destructor' ) )
                    	            // InternalJniMap.g:2440:1: (lv_destructor_3_0= 'destructor' )
                    	            {
                    	            // InternalJniMap.g:2440:1: (lv_destructor_3_0= 'destructor' )
                    	            // InternalJniMap.g:2441:3: lv_destructor_3_0= 'destructor'
                    	            {
                    	            lv_destructor_3_0=(Token)match(input,48,FOLLOW_46); if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	                      newLeafNode(lv_destructor_3_0, grammarAccess.getMethodAccess().getDestructorDestructorKeyword_0_1_0_1_0());
                    	                  
                    	            }
                    	            if ( state.backtracking==0 ) {

                    	              	        if (current==null) {
                    	              	            current = createModelElement(grammarAccess.getMethodRule());
                    	              	        }
                    	                     		setWithLastConsumed(current, "destructor", true, "destructor");
                    	              	    
                    	            }

                    	            }


                    	            }


                    	            }
                    	            break;
                    	        case 3 :
                    	            // InternalJniMap.g:2455:6: ( (lv_instanceof_4_0= 'instanceOf' ) )
                    	            {
                    	            // InternalJniMap.g:2455:6: ( (lv_instanceof_4_0= 'instanceOf' ) )
                    	            // InternalJniMap.g:2456:1: (lv_instanceof_4_0= 'instanceOf' )
                    	            {
                    	            // InternalJniMap.g:2456:1: (lv_instanceof_4_0= 'instanceOf' )
                    	            // InternalJniMap.g:2457:3: lv_instanceof_4_0= 'instanceOf'
                    	            {
                    	            lv_instanceof_4_0=(Token)match(input,49,FOLLOW_46); if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	                      newLeafNode(lv_instanceof_4_0, grammarAccess.getMethodAccess().getInstanceofInstanceOfKeyword_0_1_0_2_0());
                    	                  
                    	            }
                    	            if ( state.backtracking==0 ) {

                    	              	        if (current==null) {
                    	              	            current = createModelElement(grammarAccess.getMethodRule());
                    	              	        }
                    	                     		setWithLastConsumed(current, "instanceof", true, "instanceOf");
                    	              	    
                    	            }

                    	            }


                    	            }


                    	            }
                    	            break;

                    	    }


                    	    }

                    	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMethodAccess().getUnorderedGroup_0_1());

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalJniMap.g:2477:4: ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) )
                    	    {
                    	    // InternalJniMap.g:2477:4: ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) )
                    	    // InternalJniMap.g:2478:5: {...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1) ) {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        throw new FailedPredicateException(input, "ruleMethod", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1)");
                    	    }
                    	    // InternalJniMap.g:2478:105: ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) )
                    	    // InternalJniMap.g:2479:6: ({...}? => ( (lv_static_5_0= 'static' ) ) )
                    	    {
                    	    getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1);
                    	    // InternalJniMap.g:2482:6: ({...}? => ( (lv_static_5_0= 'static' ) ) )
                    	    // InternalJniMap.g:2482:7: {...}? => ( (lv_static_5_0= 'static' ) )
                    	    {
                    	    if ( !((true)) ) {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        throw new FailedPredicateException(input, "ruleMethod", "true");
                    	    }
                    	    // InternalJniMap.g:2482:16: ( (lv_static_5_0= 'static' ) )
                    	    // InternalJniMap.g:2483:1: (lv_static_5_0= 'static' )
                    	    {
                    	    // InternalJniMap.g:2483:1: (lv_static_5_0= 'static' )
                    	    // InternalJniMap.g:2484:3: lv_static_5_0= 'static'
                    	    {
                    	    lv_static_5_0=(Token)match(input,38,FOLLOW_46); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	              newLeafNode(lv_static_5_0, grammarAccess.getMethodAccess().getStaticStaticKeyword_0_1_1_0());
                    	          
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElement(grammarAccess.getMethodRule());
                    	      	        }
                    	             		setWithLastConsumed(current, "static", true, "static");
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }

                    	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMethodAccess().getUnorderedGroup_0_1());

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // InternalJniMap.g:2504:4: ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) )
                    	    {
                    	    // InternalJniMap.g:2504:4: ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) )
                    	    // InternalJniMap.g:2505:5: {...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2) ) {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        throw new FailedPredicateException(input, "ruleMethod", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2)");
                    	    }
                    	    // InternalJniMap.g:2505:105: ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) )
                    	    // InternalJniMap.g:2506:6: ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) )
                    	    {
                    	    getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2);
                    	    // InternalJniMap.g:2509:6: ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) )
                    	    // InternalJniMap.g:2509:7: {...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) )
                    	    {
                    	    if ( !((true)) ) {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        throw new FailedPredicateException(input, "ruleMethod", "true");
                    	    }
                    	    // InternalJniMap.g:2509:16: ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) )
                    	    int alt46=2;
                    	    int LA46_0 = input.LA(1);

                    	    if ( (LA46_0==50) ) {
                    	        alt46=1;
                    	    }
                    	    else if ( (LA46_0==51) ) {
                    	        alt46=2;
                    	    }
                    	    else {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        NoViableAltException nvae =
                    	            new NoViableAltException("", 46, 0, input);

                    	        throw nvae;
                    	    }
                    	    switch (alt46) {
                    	        case 1 :
                    	            // InternalJniMap.g:2509:17: ( (lv_private_6_0= 'private' ) )
                    	            {
                    	            // InternalJniMap.g:2509:17: ( (lv_private_6_0= 'private' ) )
                    	            // InternalJniMap.g:2510:1: (lv_private_6_0= 'private' )
                    	            {
                    	            // InternalJniMap.g:2510:1: (lv_private_6_0= 'private' )
                    	            // InternalJniMap.g:2511:3: lv_private_6_0= 'private'
                    	            {
                    	            lv_private_6_0=(Token)match(input,50,FOLLOW_46); if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	                      newLeafNode(lv_private_6_0, grammarAccess.getMethodAccess().getPrivatePrivateKeyword_0_1_2_0_0());
                    	                  
                    	            }
                    	            if ( state.backtracking==0 ) {

                    	              	        if (current==null) {
                    	              	            current = createModelElement(grammarAccess.getMethodRule());
                    	              	        }
                    	                     		setWithLastConsumed(current, "private", true, "private");
                    	              	    
                    	            }

                    	            }


                    	            }


                    	            }
                    	            break;
                    	        case 2 :
                    	            // InternalJniMap.g:2525:6: ( (lv_protected_7_0= 'protected' ) )
                    	            {
                    	            // InternalJniMap.g:2525:6: ( (lv_protected_7_0= 'protected' ) )
                    	            // InternalJniMap.g:2526:1: (lv_protected_7_0= 'protected' )
                    	            {
                    	            // InternalJniMap.g:2526:1: (lv_protected_7_0= 'protected' )
                    	            // InternalJniMap.g:2527:3: lv_protected_7_0= 'protected'
                    	            {
                    	            lv_protected_7_0=(Token)match(input,51,FOLLOW_46); if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	                      newLeafNode(lv_protected_7_0, grammarAccess.getMethodAccess().getProtectedProtectedKeyword_0_1_2_1_0());
                    	                  
                    	            }
                    	            if ( state.backtracking==0 ) {

                    	              	        if (current==null) {
                    	              	            current = createModelElement(grammarAccess.getMethodRule());
                    	              	        }
                    	                     		setWithLastConsumed(current, "protected", true, "protected");
                    	              	    
                    	            }

                    	            }


                    	            }


                    	            }
                    	            break;

                    	    }


                    	    }

                    	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMethodAccess().getUnorderedGroup_0_1());

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // InternalJniMap.g:2547:4: ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) )
                    	    {
                    	    // InternalJniMap.g:2547:4: ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) )
                    	    // InternalJniMap.g:2548:5: {...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3) ) {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        throw new FailedPredicateException(input, "ruleMethod", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3)");
                    	    }
                    	    // InternalJniMap.g:2548:105: ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) )
                    	    // InternalJniMap.g:2549:6: ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) )
                    	    {
                    	    getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3);
                    	    // InternalJniMap.g:2552:6: ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) )
                    	    // InternalJniMap.g:2552:7: {...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) )
                    	    {
                    	    if ( !((true)) ) {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        throw new FailedPredicateException(input, "ruleMethod", "true");
                    	    }
                    	    // InternalJniMap.g:2552:16: ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) )
                    	    // InternalJniMap.g:2552:17: ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) )
                    	    {
                    	    // InternalJniMap.g:2552:17: ( (lv_renamed_8_0= 'rename' ) )
                    	    // InternalJniMap.g:2553:1: (lv_renamed_8_0= 'rename' )
                    	    {
                    	    // InternalJniMap.g:2553:1: (lv_renamed_8_0= 'rename' )
                    	    // InternalJniMap.g:2554:3: lv_renamed_8_0= 'rename'
                    	    {
                    	    lv_renamed_8_0=(Token)match(input,52,FOLLOW_7); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	              newLeafNode(lv_renamed_8_0, grammarAccess.getMethodAccess().getRenamedRenameKeyword_0_1_3_0_0());
                    	          
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElement(grammarAccess.getMethodRule());
                    	      	        }
                    	             		setWithLastConsumed(current, "renamed", true, "rename");
                    	      	    
                    	    }

                    	    }


                    	    }

                    	    otherlv_9=(Token)match(input,14,FOLLOW_4); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_9, grammarAccess.getMethodAccess().getEqualsSignKeyword_0_1_3_1());
                    	          
                    	    }
                    	    // InternalJniMap.g:2571:1: ( (lv_newname_10_0= RULE_ID ) )
                    	    // InternalJniMap.g:2572:1: (lv_newname_10_0= RULE_ID )
                    	    {
                    	    // InternalJniMap.g:2572:1: (lv_newname_10_0= RULE_ID )
                    	    // InternalJniMap.g:2573:3: lv_newname_10_0= RULE_ID
                    	    {
                    	    lv_newname_10_0=(Token)match(input,RULE_ID,FOLLOW_46); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      			newLeafNode(lv_newname_10_0, grammarAccess.getMethodAccess().getNewnameIDTerminalRuleCall_0_1_3_2_0()); 
                    	      		
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElement(grammarAccess.getMethodRule());
                    	      	        }
                    	             		setWithLastConsumed(
                    	             			current, 
                    	             			"newname",
                    	              		lv_newname_10_0, 
                    	              		"org.eclipse.xtext.common.Terminals.ID");
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }


                    	    }

                    	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMethodAccess().getUnorderedGroup_0_1());

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 5 :
                    	    // InternalJniMap.g:2596:4: ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) )
                    	    {
                    	    // InternalJniMap.g:2596:4: ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) )
                    	    // InternalJniMap.g:2597:5: {...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4) ) {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        throw new FailedPredicateException(input, "ruleMethod", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4)");
                    	    }
                    	    // InternalJniMap.g:2597:105: ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) )
                    	    // InternalJniMap.g:2598:6: ({...}? => ( (lv_stub_11_0= 'stub' ) ) )
                    	    {
                    	    getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4);
                    	    // InternalJniMap.g:2601:6: ({...}? => ( (lv_stub_11_0= 'stub' ) ) )
                    	    // InternalJniMap.g:2601:7: {...}? => ( (lv_stub_11_0= 'stub' ) )
                    	    {
                    	    if ( !((true)) ) {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        throw new FailedPredicateException(input, "ruleMethod", "true");
                    	    }
                    	    // InternalJniMap.g:2601:16: ( (lv_stub_11_0= 'stub' ) )
                    	    // InternalJniMap.g:2602:1: (lv_stub_11_0= 'stub' )
                    	    {
                    	    // InternalJniMap.g:2602:1: (lv_stub_11_0= 'stub' )
                    	    // InternalJniMap.g:2603:3: lv_stub_11_0= 'stub'
                    	    {
                    	    lv_stub_11_0=(Token)match(input,53,FOLLOW_46); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	              newLeafNode(lv_stub_11_0, grammarAccess.getMethodAccess().getStubStubKeyword_0_1_4_0());
                    	          
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElement(grammarAccess.getMethodRule());
                    	      	        }
                    	             		setWithLastConsumed(current, "stub", true, "stub");
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }

                    	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMethodAccess().getUnorderedGroup_0_1());

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 6 :
                    	    // InternalJniMap.g:2623:4: ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) )
                    	    {
                    	    // InternalJniMap.g:2623:4: ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) )
                    	    // InternalJniMap.g:2624:5: {...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5) ) {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        throw new FailedPredicateException(input, "ruleMethod", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5)");
                    	    }
                    	    // InternalJniMap.g:2624:105: ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) )
                    	    // InternalJniMap.g:2625:6: ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) )
                    	    {
                    	    getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5);
                    	    // InternalJniMap.g:2628:6: ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) )
                    	    // InternalJniMap.g:2628:7: {...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) )
                    	    {
                    	    if ( !((true)) ) {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        throw new FailedPredicateException(input, "ruleMethod", "true");
                    	    }
                    	    // InternalJniMap.g:2628:16: (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) )
                    	    // InternalJniMap.g:2628:18: otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) )
                    	    {
                    	    otherlv_12=(Token)match(input,54,FOLLOW_7); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_12, grammarAccess.getMethodAccess().getThisKeyword_0_1_5_0());
                    	          
                    	    }
                    	    otherlv_13=(Token)match(input,14,FOLLOW_47); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_13, grammarAccess.getMethodAccess().getEqualsSignKeyword_0_1_5_1());
                    	          
                    	    }
                    	    // InternalJniMap.g:2636:1: ( (lv_arg_14_0= RULE_INT ) )
                    	    // InternalJniMap.g:2637:1: (lv_arg_14_0= RULE_INT )
                    	    {
                    	    // InternalJniMap.g:2637:1: (lv_arg_14_0= RULE_INT )
                    	    // InternalJniMap.g:2638:3: lv_arg_14_0= RULE_INT
                    	    {
                    	    lv_arg_14_0=(Token)match(input,RULE_INT,FOLLOW_46); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      			newLeafNode(lv_arg_14_0, grammarAccess.getMethodAccess().getArgINTTerminalRuleCall_0_1_5_2_0()); 
                    	      		
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElement(grammarAccess.getMethodRule());
                    	      	        }
                    	             		setWithLastConsumed(
                    	             			current, 
                    	             			"arg",
                    	              		lv_arg_14_0, 
                    	              		"org.eclipse.xtext.common.Terminals.INT");
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }


                    	    }

                    	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMethodAccess().getUnorderedGroup_0_1());

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop47;
                        }
                    } while (true);


                    }


                    }

                    getUnorderedGroupHelper().leave(grammarAccess.getMethodAccess().getUnorderedGroup_0_1());

                    }

                    otherlv_15=(Token)match(input,19,FOLLOW_33); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_15, grammarAccess.getMethodAccess().getRightSquareBracketKeyword_0_2());
                          
                    }

                    }
                    break;

            }

            // InternalJniMap.g:2672:3: (otherlv_16= 'extern' | otherlv_17= 'static' )?
            int alt49=3;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==37) ) {
                alt49=1;
            }
            else if ( (LA49_0==38) ) {
                alt49=2;
            }
            switch (alt49) {
                case 1 :
                    // InternalJniMap.g:2672:5: otherlv_16= 'extern'
                    {
                    otherlv_16=(Token)match(input,37,FOLLOW_33); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_16, grammarAccess.getMethodAccess().getExternKeyword_1_0());
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalJniMap.g:2677:7: otherlv_17= 'static'
                    {
                    otherlv_17=(Token)match(input,38,FOLLOW_33); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_17, grammarAccess.getMethodAccess().getStaticKeyword_1_1());
                          
                    }

                    }
                    break;

            }

            // InternalJniMap.g:2681:3: ( (lv_res_18_0= ruleType ) )
            // InternalJniMap.g:2682:1: (lv_res_18_0= ruleType )
            {
            // InternalJniMap.g:2682:1: (lv_res_18_0= ruleType )
            // InternalJniMap.g:2683:3: lv_res_18_0= ruleType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getMethodAccess().getResTypeParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_4);
            lv_res_18_0=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getMethodRule());
              	        }
                     		set(
                     			current, 
                     			"res",
                      		lv_res_18_0, 
                      		"fr.irisa.cairn.JniMap.Type");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalJniMap.g:2699:2: ( (lv_name_19_0= RULE_ID ) )
            // InternalJniMap.g:2700:1: (lv_name_19_0= RULE_ID )
            {
            // InternalJniMap.g:2700:1: (lv_name_19_0= RULE_ID )
            // InternalJniMap.g:2701:3: lv_name_19_0= RULE_ID
            {
            lv_name_19_0=(Token)match(input,RULE_ID,FOLLOW_34); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_19_0, grammarAccess.getMethodAccess().getNameIDTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getMethodRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_19_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            otherlv_20=(Token)match(input,12,FOLLOW_35); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_20, grammarAccess.getMethodAccess().getLeftParenthesisKeyword_4());
                  
            }
            // InternalJniMap.g:2721:1: ( ( (lv_params_21_0= ruleParamDef ) ) (otherlv_22= ',' ( (lv_params_23_0= ruleParamDef ) ) )* )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0==RULE_ID||LA51_0==40||(LA51_0>=42 && LA51_0<=45)||(LA51_0>=55 && LA51_0<=63)||LA51_0==69) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // InternalJniMap.g:2721:2: ( (lv_params_21_0= ruleParamDef ) ) (otherlv_22= ',' ( (lv_params_23_0= ruleParamDef ) ) )*
                    {
                    // InternalJniMap.g:2721:2: ( (lv_params_21_0= ruleParamDef ) )
                    // InternalJniMap.g:2722:1: (lv_params_21_0= ruleParamDef )
                    {
                    // InternalJniMap.g:2722:1: (lv_params_21_0= ruleParamDef )
                    // InternalJniMap.g:2723:3: lv_params_21_0= ruleParamDef
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getMethodAccess().getParamsParamDefParserRuleCall_5_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_36);
                    lv_params_21_0=ruleParamDef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getMethodRule());
                      	        }
                             		add(
                             			current, 
                             			"params",
                              		lv_params_21_0, 
                              		"fr.irisa.cairn.JniMap.ParamDef");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalJniMap.g:2739:2: (otherlv_22= ',' ( (lv_params_23_0= ruleParamDef ) ) )*
                    loop50:
                    do {
                        int alt50=2;
                        int LA50_0 = input.LA(1);

                        if ( (LA50_0==18) ) {
                            alt50=1;
                        }


                        switch (alt50) {
                    	case 1 :
                    	    // InternalJniMap.g:2739:4: otherlv_22= ',' ( (lv_params_23_0= ruleParamDef ) )
                    	    {
                    	    otherlv_22=(Token)match(input,18,FOLLOW_37); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_22, grammarAccess.getMethodAccess().getCommaKeyword_5_1_0());
                    	          
                    	    }
                    	    // InternalJniMap.g:2743:1: ( (lv_params_23_0= ruleParamDef ) )
                    	    // InternalJniMap.g:2744:1: (lv_params_23_0= ruleParamDef )
                    	    {
                    	    // InternalJniMap.g:2744:1: (lv_params_23_0= ruleParamDef )
                    	    // InternalJniMap.g:2745:3: lv_params_23_0= ruleParamDef
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getMethodAccess().getParamsParamDefParserRuleCall_5_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_36);
                    	    lv_params_23_0=ruleParamDef();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getMethodRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"params",
                    	              		lv_params_23_0, 
                    	              		"fr.irisa.cairn.JniMap.ParamDef");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop50;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_24=(Token)match(input,16,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_24, grammarAccess.getMethodAccess().getRightParenthesisKeyword_6());
                  
            }
            otherlv_25=(Token)match(input,15,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_25, grammarAccess.getMethodAccess().getSemicolonKeyword_7());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myUnorderedGroupState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleMethod"


    // $ANTLR start "entryRuleType"
    // InternalJniMap.g:2780:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalJniMap.g:2781:2: (iv_ruleType= ruleType EOF )
            // InternalJniMap.g:2782:2: iv_ruleType= ruleType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalJniMap.g:2789:1: ruleType returns [EObject current=null] : (this_PtrType_0= rulePtrType | this_ModifiableType_1= ruleModifiableType | this_BaseType_2= ruleBaseType | this_ConstType_3= ruleConstType | this_ArrayType_4= ruleArrayType ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_PtrType_0 = null;

        EObject this_ModifiableType_1 = null;

        EObject this_BaseType_2 = null;

        EObject this_ConstType_3 = null;

        EObject this_ArrayType_4 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:2792:28: ( (this_PtrType_0= rulePtrType | this_ModifiableType_1= ruleModifiableType | this_BaseType_2= ruleBaseType | this_ConstType_3= ruleConstType | this_ArrayType_4= ruleArrayType ) )
            // InternalJniMap.g:2793:1: (this_PtrType_0= rulePtrType | this_ModifiableType_1= ruleModifiableType | this_BaseType_2= ruleBaseType | this_ConstType_3= ruleConstType | this_ArrayType_4= ruleArrayType )
            {
            // InternalJniMap.g:2793:1: (this_PtrType_0= rulePtrType | this_ModifiableType_1= ruleModifiableType | this_BaseType_2= ruleBaseType | this_ConstType_3= ruleConstType | this_ArrayType_4= ruleArrayType )
            int alt52=5;
            alt52 = dfa52.predict(input);
            switch (alt52) {
                case 1 :
                    // InternalJniMap.g:2794:2: this_PtrType_0= rulePtrType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTypeAccess().getPtrTypeParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_PtrType_0=rulePtrType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_PtrType_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalJniMap.g:2807:2: this_ModifiableType_1= ruleModifiableType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTypeAccess().getModifiableTypeParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_ModifiableType_1=ruleModifiableType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ModifiableType_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalJniMap.g:2820:2: this_BaseType_2= ruleBaseType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTypeAccess().getBaseTypeParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_BaseType_2=ruleBaseType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BaseType_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalJniMap.g:2833:2: this_ConstType_3= ruleConstType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTypeAccess().getConstTypeParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_ConstType_3=ruleConstType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ConstType_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalJniMap.g:2846:2: this_ArrayType_4= ruleArrayType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTypeAccess().getArrayTypeParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_ArrayType_4=ruleArrayType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ArrayType_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleBuiltInType"
    // InternalJniMap.g:2865:1: entryRuleBuiltInType returns [EObject current=null] : iv_ruleBuiltInType= ruleBuiltInType EOF ;
    public final EObject entryRuleBuiltInType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBuiltInType = null;


        try {
            // InternalJniMap.g:2866:2: (iv_ruleBuiltInType= ruleBuiltInType EOF )
            // InternalJniMap.g:2867:2: iv_ruleBuiltInType= ruleBuiltInType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBuiltInTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBuiltInType=ruleBuiltInType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBuiltInType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBuiltInType"


    // $ANTLR start "ruleBuiltInType"
    // InternalJniMap.g:2874:1: ruleBuiltInType returns [EObject current=null] : (this_BooleanType_0= ruleBooleanType | this_IntegerType_1= ruleIntegerType | this_RealType_2= ruleRealType | this_StringType_3= ruleStringType | this_VoidType_4= ruleVoidType ) ;
    public final EObject ruleBuiltInType() throws RecognitionException {
        EObject current = null;

        EObject this_BooleanType_0 = null;

        EObject this_IntegerType_1 = null;

        EObject this_RealType_2 = null;

        EObject this_StringType_3 = null;

        EObject this_VoidType_4 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:2877:28: ( (this_BooleanType_0= ruleBooleanType | this_IntegerType_1= ruleIntegerType | this_RealType_2= ruleRealType | this_StringType_3= ruleStringType | this_VoidType_4= ruleVoidType ) )
            // InternalJniMap.g:2878:1: (this_BooleanType_0= ruleBooleanType | this_IntegerType_1= ruleIntegerType | this_RealType_2= ruleRealType | this_StringType_3= ruleStringType | this_VoidType_4= ruleVoidType )
            {
            // InternalJniMap.g:2878:1: (this_BooleanType_0= ruleBooleanType | this_IntegerType_1= ruleIntegerType | this_RealType_2= ruleRealType | this_StringType_3= ruleStringType | this_VoidType_4= ruleVoidType )
            int alt53=5;
            switch ( input.LA(1) ) {
            case 55:
                {
                alt53=1;
                }
                break;
            case 56:
            case 57:
            case 58:
            case 59:
                {
                alt53=2;
                }
                break;
            case 60:
            case 61:
                {
                alt53=3;
                }
                break;
            case 62:
                {
                alt53=4;
                }
                break;
            case 63:
                {
                alt53=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 53, 0, input);

                throw nvae;
            }

            switch (alt53) {
                case 1 :
                    // InternalJniMap.g:2879:2: this_BooleanType_0= ruleBooleanType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBuiltInTypeAccess().getBooleanTypeParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_BooleanType_0=ruleBooleanType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BooleanType_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalJniMap.g:2892:2: this_IntegerType_1= ruleIntegerType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBuiltInTypeAccess().getIntegerTypeParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_IntegerType_1=ruleIntegerType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerType_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalJniMap.g:2905:2: this_RealType_2= ruleRealType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBuiltInTypeAccess().getRealTypeParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_RealType_2=ruleRealType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_RealType_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalJniMap.g:2918:2: this_StringType_3= ruleStringType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBuiltInTypeAccess().getStringTypeParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_StringType_3=ruleStringType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringType_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalJniMap.g:2931:2: this_VoidType_4= ruleVoidType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBuiltInTypeAccess().getVoidTypeParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_VoidType_4=ruleVoidType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VoidType_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBuiltInType"


    // $ANTLR start "entryRuleBooleanType"
    // InternalJniMap.g:2950:1: entryRuleBooleanType returns [EObject current=null] : iv_ruleBooleanType= ruleBooleanType EOF ;
    public final EObject entryRuleBooleanType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanType = null;


        try {
            // InternalJniMap.g:2951:2: (iv_ruleBooleanType= ruleBooleanType EOF )
            // InternalJniMap.g:2952:2: iv_ruleBooleanType= ruleBooleanType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBooleanType=ruleBooleanType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanType"


    // $ANTLR start "ruleBooleanType"
    // InternalJniMap.g:2959:1: ruleBooleanType returns [EObject current=null] : ( (lv_name_0_0= 'boolean' ) ) ;
    public final EObject ruleBooleanType() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:2962:28: ( ( (lv_name_0_0= 'boolean' ) ) )
            // InternalJniMap.g:2963:1: ( (lv_name_0_0= 'boolean' ) )
            {
            // InternalJniMap.g:2963:1: ( (lv_name_0_0= 'boolean' ) )
            // InternalJniMap.g:2964:1: (lv_name_0_0= 'boolean' )
            {
            // InternalJniMap.g:2964:1: (lv_name_0_0= 'boolean' )
            // InternalJniMap.g:2965:3: lv_name_0_0= 'boolean'
            {
            lv_name_0_0=(Token)match(input,55,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      newLeafNode(lv_name_0_0, grammarAccess.getBooleanTypeAccess().getNameBooleanKeyword_0());
                  
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanTypeRule());
              	        }
                     		setWithLastConsumed(current, "name", lv_name_0_0, "boolean");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanType"


    // $ANTLR start "entryRuleIntegerType"
    // InternalJniMap.g:2986:1: entryRuleIntegerType returns [EObject current=null] : iv_ruleIntegerType= ruleIntegerType EOF ;
    public final EObject entryRuleIntegerType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerType = null;


        try {
            // InternalJniMap.g:2987:2: (iv_ruleIntegerType= ruleIntegerType EOF )
            // InternalJniMap.g:2988:2: iv_ruleIntegerType= ruleIntegerType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleIntegerType=ruleIntegerType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerType"


    // $ANTLR start "ruleIntegerType"
    // InternalJniMap.g:2995:1: ruleIntegerType returns [EObject current=null] : ( ( (lv_name_0_1= 'unsigned' | lv_name_0_2= 'int' | lv_name_0_3= 'long' | lv_name_0_4= 'char' ) ) ) ;
    public final EObject ruleIntegerType() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_1=null;
        Token lv_name_0_2=null;
        Token lv_name_0_3=null;
        Token lv_name_0_4=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:2998:28: ( ( ( (lv_name_0_1= 'unsigned' | lv_name_0_2= 'int' | lv_name_0_3= 'long' | lv_name_0_4= 'char' ) ) ) )
            // InternalJniMap.g:2999:1: ( ( (lv_name_0_1= 'unsigned' | lv_name_0_2= 'int' | lv_name_0_3= 'long' | lv_name_0_4= 'char' ) ) )
            {
            // InternalJniMap.g:2999:1: ( ( (lv_name_0_1= 'unsigned' | lv_name_0_2= 'int' | lv_name_0_3= 'long' | lv_name_0_4= 'char' ) ) )
            // InternalJniMap.g:3000:1: ( (lv_name_0_1= 'unsigned' | lv_name_0_2= 'int' | lv_name_0_3= 'long' | lv_name_0_4= 'char' ) )
            {
            // InternalJniMap.g:3000:1: ( (lv_name_0_1= 'unsigned' | lv_name_0_2= 'int' | lv_name_0_3= 'long' | lv_name_0_4= 'char' ) )
            // InternalJniMap.g:3001:1: (lv_name_0_1= 'unsigned' | lv_name_0_2= 'int' | lv_name_0_3= 'long' | lv_name_0_4= 'char' )
            {
            // InternalJniMap.g:3001:1: (lv_name_0_1= 'unsigned' | lv_name_0_2= 'int' | lv_name_0_3= 'long' | lv_name_0_4= 'char' )
            int alt54=4;
            switch ( input.LA(1) ) {
            case 56:
                {
                alt54=1;
                }
                break;
            case 57:
                {
                alt54=2;
                }
                break;
            case 58:
                {
                alt54=3;
                }
                break;
            case 59:
                {
                alt54=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 54, 0, input);

                throw nvae;
            }

            switch (alt54) {
                case 1 :
                    // InternalJniMap.g:3002:3: lv_name_0_1= 'unsigned'
                    {
                    lv_name_0_1=(Token)match(input,56,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_name_0_1, grammarAccess.getIntegerTypeAccess().getNameUnsignedKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerTypeRule());
                      	        }
                             		setWithLastConsumed(current, "name", lv_name_0_1, null);
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalJniMap.g:3014:8: lv_name_0_2= 'int'
                    {
                    lv_name_0_2=(Token)match(input,57,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_name_0_2, grammarAccess.getIntegerTypeAccess().getNameIntKeyword_0_1());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerTypeRule());
                      	        }
                             		setWithLastConsumed(current, "name", lv_name_0_2, null);
                      	    
                    }

                    }
                    break;
                case 3 :
                    // InternalJniMap.g:3026:8: lv_name_0_3= 'long'
                    {
                    lv_name_0_3=(Token)match(input,58,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_name_0_3, grammarAccess.getIntegerTypeAccess().getNameLongKeyword_0_2());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerTypeRule());
                      	        }
                             		setWithLastConsumed(current, "name", lv_name_0_3, null);
                      	    
                    }

                    }
                    break;
                case 4 :
                    // InternalJniMap.g:3038:8: lv_name_0_4= 'char'
                    {
                    lv_name_0_4=(Token)match(input,59,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_name_0_4, grammarAccess.getIntegerTypeAccess().getNameCharKeyword_0_3());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerTypeRule());
                      	        }
                             		setWithLastConsumed(current, "name", lv_name_0_4, null);
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerType"


    // $ANTLR start "entryRuleRealType"
    // InternalJniMap.g:3061:1: entryRuleRealType returns [EObject current=null] : iv_ruleRealType= ruleRealType EOF ;
    public final EObject entryRuleRealType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealType = null;


        try {
            // InternalJniMap.g:3062:2: (iv_ruleRealType= ruleRealType EOF )
            // InternalJniMap.g:3063:2: iv_ruleRealType= ruleRealType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRealType=ruleRealType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRealType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealType"


    // $ANTLR start "ruleRealType"
    // InternalJniMap.g:3070:1: ruleRealType returns [EObject current=null] : ( ( (lv_name_0_1= 'float' | lv_name_0_2= 'double' ) ) ) ;
    public final EObject ruleRealType() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_1=null;
        Token lv_name_0_2=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:3073:28: ( ( ( (lv_name_0_1= 'float' | lv_name_0_2= 'double' ) ) ) )
            // InternalJniMap.g:3074:1: ( ( (lv_name_0_1= 'float' | lv_name_0_2= 'double' ) ) )
            {
            // InternalJniMap.g:3074:1: ( ( (lv_name_0_1= 'float' | lv_name_0_2= 'double' ) ) )
            // InternalJniMap.g:3075:1: ( (lv_name_0_1= 'float' | lv_name_0_2= 'double' ) )
            {
            // InternalJniMap.g:3075:1: ( (lv_name_0_1= 'float' | lv_name_0_2= 'double' ) )
            // InternalJniMap.g:3076:1: (lv_name_0_1= 'float' | lv_name_0_2= 'double' )
            {
            // InternalJniMap.g:3076:1: (lv_name_0_1= 'float' | lv_name_0_2= 'double' )
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==60) ) {
                alt55=1;
            }
            else if ( (LA55_0==61) ) {
                alt55=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 55, 0, input);

                throw nvae;
            }
            switch (alt55) {
                case 1 :
                    // InternalJniMap.g:3077:3: lv_name_0_1= 'float'
                    {
                    lv_name_0_1=(Token)match(input,60,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_name_0_1, grammarAccess.getRealTypeAccess().getNameFloatKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getRealTypeRule());
                      	        }
                             		setWithLastConsumed(current, "name", lv_name_0_1, null);
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalJniMap.g:3089:8: lv_name_0_2= 'double'
                    {
                    lv_name_0_2=(Token)match(input,61,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_name_0_2, grammarAccess.getRealTypeAccess().getNameDoubleKeyword_0_1());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getRealTypeRule());
                      	        }
                             		setWithLastConsumed(current, "name", lv_name_0_2, null);
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealType"


    // $ANTLR start "entryRuleStringType"
    // InternalJniMap.g:3112:1: entryRuleStringType returns [EObject current=null] : iv_ruleStringType= ruleStringType EOF ;
    public final EObject entryRuleStringType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringType = null;


        try {
            // InternalJniMap.g:3113:2: (iv_ruleStringType= ruleStringType EOF )
            // InternalJniMap.g:3114:2: iv_ruleStringType= ruleStringType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleStringType=ruleStringType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringType"


    // $ANTLR start "ruleStringType"
    // InternalJniMap.g:3121:1: ruleStringType returns [EObject current=null] : ( (lv_name_0_0= 'string' ) ) ;
    public final EObject ruleStringType() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:3124:28: ( ( (lv_name_0_0= 'string' ) ) )
            // InternalJniMap.g:3125:1: ( (lv_name_0_0= 'string' ) )
            {
            // InternalJniMap.g:3125:1: ( (lv_name_0_0= 'string' ) )
            // InternalJniMap.g:3126:1: (lv_name_0_0= 'string' )
            {
            // InternalJniMap.g:3126:1: (lv_name_0_0= 'string' )
            // InternalJniMap.g:3127:3: lv_name_0_0= 'string'
            {
            lv_name_0_0=(Token)match(input,62,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      newLeafNode(lv_name_0_0, grammarAccess.getStringTypeAccess().getNameStringKeyword_0());
                  
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringTypeRule());
              	        }
                     		setWithLastConsumed(current, "name", lv_name_0_0, "string");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringType"


    // $ANTLR start "entryRuleVoidType"
    // InternalJniMap.g:3148:1: entryRuleVoidType returns [EObject current=null] : iv_ruleVoidType= ruleVoidType EOF ;
    public final EObject entryRuleVoidType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVoidType = null;


        try {
            // InternalJniMap.g:3149:2: (iv_ruleVoidType= ruleVoidType EOF )
            // InternalJniMap.g:3150:2: iv_ruleVoidType= ruleVoidType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVoidTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVoidType=ruleVoidType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVoidType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVoidType"


    // $ANTLR start "ruleVoidType"
    // InternalJniMap.g:3157:1: ruleVoidType returns [EObject current=null] : ( (lv_name_0_0= 'void' ) ) ;
    public final EObject ruleVoidType() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:3160:28: ( ( (lv_name_0_0= 'void' ) ) )
            // InternalJniMap.g:3161:1: ( (lv_name_0_0= 'void' ) )
            {
            // InternalJniMap.g:3161:1: ( (lv_name_0_0= 'void' ) )
            // InternalJniMap.g:3162:1: (lv_name_0_0= 'void' )
            {
            // InternalJniMap.g:3162:1: (lv_name_0_0= 'void' )
            // InternalJniMap.g:3163:3: lv_name_0_0= 'void'
            {
            lv_name_0_0=(Token)match(input,63,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      newLeafNode(lv_name_0_0, grammarAccess.getVoidTypeAccess().getNameVoidKeyword_0());
                  
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getVoidTypeRule());
              	        }
                     		setWithLastConsumed(current, "name", lv_name_0_0, "void");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVoidType"


    // $ANTLR start "entryRuleTypeAlias"
    // InternalJniMap.g:3188:1: entryRuleTypeAlias returns [EObject current=null] : iv_ruleTypeAlias= ruleTypeAlias EOF ;
    public final EObject entryRuleTypeAlias() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeAlias = null;


        try {
            // InternalJniMap.g:3189:2: (iv_ruleTypeAlias= ruleTypeAlias EOF )
            // InternalJniMap.g:3190:2: iv_ruleTypeAlias= ruleTypeAlias EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeAliasRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTypeAlias=ruleTypeAlias();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypeAlias; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeAlias"


    // $ANTLR start "ruleTypeAlias"
    // InternalJniMap.g:3197:1: ruleTypeAlias returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleTypeAlias() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:3200:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalJniMap.g:3201:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalJniMap.g:3201:1: ( (otherlv_0= RULE_ID ) )
            // InternalJniMap.g:3202:1: (otherlv_0= RULE_ID )
            {
            // InternalJniMap.g:3202:1: (otherlv_0= RULE_ID )
            // InternalJniMap.g:3203:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTypeAliasRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getTypeAliasAccess().getAliasTypeAliasDeclarationCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeAlias"


    // $ANTLR start "entryRuleArrayType"
    // InternalJniMap.g:3225:1: entryRuleArrayType returns [EObject current=null] : iv_ruleArrayType= ruleArrayType EOF ;
    public final EObject entryRuleArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArrayType = null;


        try {
            // InternalJniMap.g:3226:2: (iv_ruleArrayType= ruleArrayType EOF )
            // InternalJniMap.g:3227:2: iv_ruleArrayType= ruleArrayType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getArrayTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleArrayType=ruleArrayType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleArrayType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArrayType"


    // $ANTLR start "ruleArrayType"
    // InternalJniMap.g:3234:1: ruleArrayType returns [EObject current=null] : ( ( (lv_base_0_0= ruleBaseType ) ) otherlv_1= '[' otherlv_2= ']' ) ;
    public final EObject ruleArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_base_0_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:3237:28: ( ( ( (lv_base_0_0= ruleBaseType ) ) otherlv_1= '[' otherlv_2= ']' ) )
            // InternalJniMap.g:3238:1: ( ( (lv_base_0_0= ruleBaseType ) ) otherlv_1= '[' otherlv_2= ']' )
            {
            // InternalJniMap.g:3238:1: ( ( (lv_base_0_0= ruleBaseType ) ) otherlv_1= '[' otherlv_2= ']' )
            // InternalJniMap.g:3238:2: ( (lv_base_0_0= ruleBaseType ) ) otherlv_1= '[' otherlv_2= ']'
            {
            // InternalJniMap.g:3238:2: ( (lv_base_0_0= ruleBaseType ) )
            // InternalJniMap.g:3239:1: (lv_base_0_0= ruleBaseType )
            {
            // InternalJniMap.g:3239:1: (lv_base_0_0= ruleBaseType )
            // InternalJniMap.g:3240:3: lv_base_0_0= ruleBaseType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getArrayTypeAccess().getBaseBaseTypeParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_11);
            lv_base_0_0=ruleBaseType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getArrayTypeRule());
              	        }
                     		set(
                     			current, 
                     			"base",
                      		lv_base_0_0, 
                      		"fr.irisa.cairn.JniMap.BaseType");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,17,FOLLOW_48); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getArrayTypeAccess().getLeftSquareBracketKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,19,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getArrayTypeAccess().getRightSquareBracketKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArrayType"


    // $ANTLR start "entryRulePtrType"
    // InternalJniMap.g:3272:1: entryRulePtrType returns [EObject current=null] : iv_rulePtrType= rulePtrType EOF ;
    public final EObject entryRulePtrType() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePtrType = null;


        try {
            // InternalJniMap.g:3273:2: (iv_rulePtrType= rulePtrType EOF )
            // InternalJniMap.g:3274:2: iv_rulePtrType= rulePtrType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPtrTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_rulePtrType=rulePtrType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePtrType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePtrType"


    // $ANTLR start "rulePtrType"
    // InternalJniMap.g:3281:1: rulePtrType returns [EObject current=null] : ( ( (lv_base_0_0= ruleBaseType ) ) ( (lv_indir_1_0= '*' ) )+ ( (lv_ignored_2_0= 'ignored' ) )? ) ;
    public final EObject rulePtrType() throws RecognitionException {
        EObject current = null;

        Token lv_indir_1_0=null;
        Token lv_ignored_2_0=null;
        EObject lv_base_0_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:3284:28: ( ( ( (lv_base_0_0= ruleBaseType ) ) ( (lv_indir_1_0= '*' ) )+ ( (lv_ignored_2_0= 'ignored' ) )? ) )
            // InternalJniMap.g:3285:1: ( ( (lv_base_0_0= ruleBaseType ) ) ( (lv_indir_1_0= '*' ) )+ ( (lv_ignored_2_0= 'ignored' ) )? )
            {
            // InternalJniMap.g:3285:1: ( ( (lv_base_0_0= ruleBaseType ) ) ( (lv_indir_1_0= '*' ) )+ ( (lv_ignored_2_0= 'ignored' ) )? )
            // InternalJniMap.g:3285:2: ( (lv_base_0_0= ruleBaseType ) ) ( (lv_indir_1_0= '*' ) )+ ( (lv_ignored_2_0= 'ignored' ) )?
            {
            // InternalJniMap.g:3285:2: ( (lv_base_0_0= ruleBaseType ) )
            // InternalJniMap.g:3286:1: (lv_base_0_0= ruleBaseType )
            {
            // InternalJniMap.g:3286:1: (lv_base_0_0= ruleBaseType )
            // InternalJniMap.g:3287:3: lv_base_0_0= ruleBaseType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getPtrTypeAccess().getBaseBaseTypeParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_49);
            lv_base_0_0=ruleBaseType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getPtrTypeRule());
              	        }
                     		set(
                     			current, 
                     			"base",
                      		lv_base_0_0, 
                      		"fr.irisa.cairn.JniMap.BaseType");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalJniMap.g:3303:2: ( (lv_indir_1_0= '*' ) )+
            int cnt56=0;
            loop56:
            do {
                int alt56=2;
                int LA56_0 = input.LA(1);

                if ( (LA56_0==64) ) {
                    alt56=1;
                }


                switch (alt56) {
            	case 1 :
            	    // InternalJniMap.g:3304:1: (lv_indir_1_0= '*' )
            	    {
            	    // InternalJniMap.g:3304:1: (lv_indir_1_0= '*' )
            	    // InternalJniMap.g:3305:3: lv_indir_1_0= '*'
            	    {
            	    lv_indir_1_0=(Token)match(input,64,FOLLOW_50); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	              newLeafNode(lv_indir_1_0, grammarAccess.getPtrTypeAccess().getIndirAsteriskKeyword_1_0());
            	          
            	    }
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElement(grammarAccess.getPtrTypeRule());
            	      	        }
            	             		addWithLastConsumed(current, "indir", lv_indir_1_0, "*");
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt56 >= 1 ) break loop56;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(56, input);
                        throw eee;
                }
                cnt56++;
            } while (true);

            // InternalJniMap.g:3318:3: ( (lv_ignored_2_0= 'ignored' ) )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==65) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // InternalJniMap.g:3319:1: (lv_ignored_2_0= 'ignored' )
                    {
                    // InternalJniMap.g:3319:1: (lv_ignored_2_0= 'ignored' )
                    // InternalJniMap.g:3320:3: lv_ignored_2_0= 'ignored'
                    {
                    lv_ignored_2_0=(Token)match(input,65,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_ignored_2_0, grammarAccess.getPtrTypeAccess().getIgnoredIgnoredKeyword_2_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getPtrTypeRule());
                      	        }
                             		setWithLastConsumed(current, "ignored", true, "ignored");
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePtrType"


    // $ANTLR start "entryRuleModifiableType"
    // InternalJniMap.g:3341:1: entryRuleModifiableType returns [EObject current=null] : iv_ruleModifiableType= ruleModifiableType EOF ;
    public final EObject entryRuleModifiableType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModifiableType = null;


        try {
            // InternalJniMap.g:3342:2: (iv_ruleModifiableType= ruleModifiableType EOF )
            // InternalJniMap.g:3343:2: iv_ruleModifiableType= ruleModifiableType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModifiableTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleModifiableType=ruleModifiableType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleModifiableType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModifiableType"


    // $ANTLR start "ruleModifiableType"
    // InternalJniMap.g:3350:1: ruleModifiableType returns [EObject current=null] : ( ( (lv_base_0_0= ruleBaseType ) ) ( (lv_indir_1_0= '&' ) ) ) ;
    public final EObject ruleModifiableType() throws RecognitionException {
        EObject current = null;

        Token lv_indir_1_0=null;
        EObject lv_base_0_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:3353:28: ( ( ( (lv_base_0_0= ruleBaseType ) ) ( (lv_indir_1_0= '&' ) ) ) )
            // InternalJniMap.g:3354:1: ( ( (lv_base_0_0= ruleBaseType ) ) ( (lv_indir_1_0= '&' ) ) )
            {
            // InternalJniMap.g:3354:1: ( ( (lv_base_0_0= ruleBaseType ) ) ( (lv_indir_1_0= '&' ) ) )
            // InternalJniMap.g:3354:2: ( (lv_base_0_0= ruleBaseType ) ) ( (lv_indir_1_0= '&' ) )
            {
            // InternalJniMap.g:3354:2: ( (lv_base_0_0= ruleBaseType ) )
            // InternalJniMap.g:3355:1: (lv_base_0_0= ruleBaseType )
            {
            // InternalJniMap.g:3355:1: (lv_base_0_0= ruleBaseType )
            // InternalJniMap.g:3356:3: lv_base_0_0= ruleBaseType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getModifiableTypeAccess().getBaseBaseTypeParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_51);
            lv_base_0_0=ruleBaseType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getModifiableTypeRule());
              	        }
                     		set(
                     			current, 
                     			"base",
                      		lv_base_0_0, 
                      		"fr.irisa.cairn.JniMap.BaseType");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalJniMap.g:3372:2: ( (lv_indir_1_0= '&' ) )
            // InternalJniMap.g:3373:1: (lv_indir_1_0= '&' )
            {
            // InternalJniMap.g:3373:1: (lv_indir_1_0= '&' )
            // InternalJniMap.g:3374:3: lv_indir_1_0= '&'
            {
            lv_indir_1_0=(Token)match(input,66,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      newLeafNode(lv_indir_1_0, grammarAccess.getModifiableTypeAccess().getIndirAmpersandKeyword_1_0());
                  
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getModifiableTypeRule());
              	        }
                     		addWithLastConsumed(current, "indir", lv_indir_1_0, "&");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModifiableType"


    // $ANTLR start "entryRuleStructType"
    // InternalJniMap.g:3395:1: entryRuleStructType returns [EObject current=null] : iv_ruleStructType= ruleStructType EOF ;
    public final EObject entryRuleStructType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructType = null;


        try {
            // InternalJniMap.g:3396:2: (iv_ruleStructType= ruleStructType EOF )
            // InternalJniMap.g:3397:2: iv_ruleStructType= ruleStructType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStructTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleStructType=ruleStructType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStructType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructType"


    // $ANTLR start "ruleStructType"
    // InternalJniMap.g:3404:1: ruleStructType returns [EObject current=null] : (otherlv_0= 'struct' ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleStructType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalJniMap.g:3407:28: ( (otherlv_0= 'struct' ( (otherlv_1= RULE_ID ) ) ) )
            // InternalJniMap.g:3408:1: (otherlv_0= 'struct' ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalJniMap.g:3408:1: (otherlv_0= 'struct' ( (otherlv_1= RULE_ID ) ) )
            // InternalJniMap.g:3408:3: otherlv_0= 'struct' ( (otherlv_1= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,40,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getStructTypeAccess().getStructKeyword_0());
                  
            }
            // InternalJniMap.g:3412:1: ( (otherlv_1= RULE_ID ) )
            // InternalJniMap.g:3413:1: (otherlv_1= RULE_ID )
            {
            // InternalJniMap.g:3413:1: (otherlv_1= RULE_ID )
            // InternalJniMap.g:3414:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getStructTypeRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getStructTypeAccess().getNameStructDeclarationCrossReference_1_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructType"


    // $ANTLR start "entryRuleFieldDef"
    // InternalJniMap.g:3436:1: entryRuleFieldDef returns [EObject current=null] : iv_ruleFieldDef= ruleFieldDef EOF ;
    public final EObject entryRuleFieldDef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFieldDef = null;


        try {
            // InternalJniMap.g:3437:2: (iv_ruleFieldDef= ruleFieldDef EOF )
            // InternalJniMap.g:3438:2: iv_ruleFieldDef= ruleFieldDef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFieldDefRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFieldDef=ruleFieldDef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFieldDef; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFieldDef"


    // $ANTLR start "ruleFieldDef"
    // InternalJniMap.g:3445:1: ruleFieldDef returns [EObject current=null] : ( ( (lv_copy_0_0= 'copyOnGet' ) )? ( (lv_type_1_0= ruleType ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '[' ( (lv_many_4_0= 'many' ) ) otherlv_5= ']' )? ) ;
    public final EObject ruleFieldDef() throws RecognitionException {
        EObject current = null;

        Token lv_copy_0_0=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token lv_many_4_0=null;
        Token otherlv_5=null;
        EObject lv_type_1_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:3448:28: ( ( ( (lv_copy_0_0= 'copyOnGet' ) )? ( (lv_type_1_0= ruleType ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '[' ( (lv_many_4_0= 'many' ) ) otherlv_5= ']' )? ) )
            // InternalJniMap.g:3449:1: ( ( (lv_copy_0_0= 'copyOnGet' ) )? ( (lv_type_1_0= ruleType ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '[' ( (lv_many_4_0= 'many' ) ) otherlv_5= ']' )? )
            {
            // InternalJniMap.g:3449:1: ( ( (lv_copy_0_0= 'copyOnGet' ) )? ( (lv_type_1_0= ruleType ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '[' ( (lv_many_4_0= 'many' ) ) otherlv_5= ']' )? )
            // InternalJniMap.g:3449:2: ( (lv_copy_0_0= 'copyOnGet' ) )? ( (lv_type_1_0= ruleType ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '[' ( (lv_many_4_0= 'many' ) ) otherlv_5= ']' )?
            {
            // InternalJniMap.g:3449:2: ( (lv_copy_0_0= 'copyOnGet' ) )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==67) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // InternalJniMap.g:3450:1: (lv_copy_0_0= 'copyOnGet' )
                    {
                    // InternalJniMap.g:3450:1: (lv_copy_0_0= 'copyOnGet' )
                    // InternalJniMap.g:3451:3: lv_copy_0_0= 'copyOnGet'
                    {
                    lv_copy_0_0=(Token)match(input,67,FOLLOW_33); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_copy_0_0, grammarAccess.getFieldDefAccess().getCopyCopyOnGetKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getFieldDefRule());
                      	        }
                             		setWithLastConsumed(current, "copy", true, "copyOnGet");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalJniMap.g:3464:3: ( (lv_type_1_0= ruleType ) )
            // InternalJniMap.g:3465:1: (lv_type_1_0= ruleType )
            {
            // InternalJniMap.g:3465:1: (lv_type_1_0= ruleType )
            // InternalJniMap.g:3466:3: lv_type_1_0= ruleType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFieldDefAccess().getTypeTypeParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_4);
            lv_type_1_0=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFieldDefRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_1_0, 
                      		"fr.irisa.cairn.JniMap.Type");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalJniMap.g:3482:2: ( (lv_name_2_0= RULE_ID ) )
            // InternalJniMap.g:3483:1: (lv_name_2_0= RULE_ID )
            {
            // InternalJniMap.g:3483:1: (lv_name_2_0= RULE_ID )
            // InternalJniMap.g:3484:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_52); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getFieldDefAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getFieldDefRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalJniMap.g:3500:2: (otherlv_3= '[' ( (lv_many_4_0= 'many' ) ) otherlv_5= ']' )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==17) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // InternalJniMap.g:3500:4: otherlv_3= '[' ( (lv_many_4_0= 'many' ) ) otherlv_5= ']'
                    {
                    otherlv_3=(Token)match(input,17,FOLLOW_53); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getFieldDefAccess().getLeftSquareBracketKeyword_3_0());
                          
                    }
                    // InternalJniMap.g:3504:1: ( (lv_many_4_0= 'many' ) )
                    // InternalJniMap.g:3505:1: (lv_many_4_0= 'many' )
                    {
                    // InternalJniMap.g:3505:1: (lv_many_4_0= 'many' )
                    // InternalJniMap.g:3506:3: lv_many_4_0= 'many'
                    {
                    lv_many_4_0=(Token)match(input,68,FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_many_4_0, grammarAccess.getFieldDefAccess().getManyManyKeyword_3_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getFieldDefRule());
                      	        }
                             		setWithLastConsumed(current, "many", true, "many");
                      	    
                    }

                    }


                    }

                    otherlv_5=(Token)match(input,19,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getFieldDefAccess().getRightSquareBracketKeyword_3_2());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFieldDef"


    // $ANTLR start "entryRuleConstType"
    // InternalJniMap.g:3531:1: entryRuleConstType returns [EObject current=null] : iv_ruleConstType= ruleConstType EOF ;
    public final EObject entryRuleConstType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstType = null;


        try {
            // InternalJniMap.g:3532:2: (iv_ruleConstType= ruleConstType EOF )
            // InternalJniMap.g:3533:2: iv_ruleConstType= ruleConstType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleConstType=ruleConstType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstType"


    // $ANTLR start "ruleConstType"
    // InternalJniMap.g:3540:1: ruleConstType returns [EObject current=null] : (otherlv_0= 'const' ( (lv_base_1_0= ruleType ) ) ) ;
    public final EObject ruleConstType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_base_1_0 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:3543:28: ( (otherlv_0= 'const' ( (lv_base_1_0= ruleType ) ) ) )
            // InternalJniMap.g:3544:1: (otherlv_0= 'const' ( (lv_base_1_0= ruleType ) ) )
            {
            // InternalJniMap.g:3544:1: (otherlv_0= 'const' ( (lv_base_1_0= ruleType ) ) )
            // InternalJniMap.g:3544:3: otherlv_0= 'const' ( (lv_base_1_0= ruleType ) )
            {
            otherlv_0=(Token)match(input,69,FOLLOW_33); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getConstTypeAccess().getConstKeyword_0());
                  
            }
            // InternalJniMap.g:3548:1: ( (lv_base_1_0= ruleType ) )
            // InternalJniMap.g:3549:1: (lv_base_1_0= ruleType )
            {
            // InternalJniMap.g:3549:1: (lv_base_1_0= ruleType )
            // InternalJniMap.g:3550:3: lv_base_1_0= ruleType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getConstTypeAccess().getBaseTypeParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_2);
            lv_base_1_0=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getConstTypeRule());
              	        }
                     		set(
                     			current, 
                     			"base",
                      		lv_base_1_0, 
                      		"fr.irisa.cairn.JniMap.Type");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstType"


    // $ANTLR start "entryRuleBaseType"
    // InternalJniMap.g:3574:1: entryRuleBaseType returns [EObject current=null] : iv_ruleBaseType= ruleBaseType EOF ;
    public final EObject entryRuleBaseType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBaseType = null;


        try {
            // InternalJniMap.g:3575:2: (iv_ruleBaseType= ruleBaseType EOF )
            // InternalJniMap.g:3576:2: iv_ruleBaseType= ruleBaseType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBaseTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBaseType=ruleBaseType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBaseType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBaseType"


    // $ANTLR start "ruleBaseType"
    // InternalJniMap.g:3583:1: ruleBaseType returns [EObject current=null] : (this_BuiltInType_0= ruleBuiltInType | this_StructType_1= ruleStructType | this_EnumType_2= ruleEnumType | this_TypeAlias_3= ruleTypeAlias ) ;
    public final EObject ruleBaseType() throws RecognitionException {
        EObject current = null;

        EObject this_BuiltInType_0 = null;

        EObject this_StructType_1 = null;

        EObject this_EnumType_2 = null;

        EObject this_TypeAlias_3 = null;


         enterRule(); 
            
        try {
            // InternalJniMap.g:3586:28: ( (this_BuiltInType_0= ruleBuiltInType | this_StructType_1= ruleStructType | this_EnumType_2= ruleEnumType | this_TypeAlias_3= ruleTypeAlias ) )
            // InternalJniMap.g:3587:1: (this_BuiltInType_0= ruleBuiltInType | this_StructType_1= ruleStructType | this_EnumType_2= ruleEnumType | this_TypeAlias_3= ruleTypeAlias )
            {
            // InternalJniMap.g:3587:1: (this_BuiltInType_0= ruleBuiltInType | this_StructType_1= ruleStructType | this_EnumType_2= ruleEnumType | this_TypeAlias_3= ruleTypeAlias )
            int alt60=4;
            switch ( input.LA(1) ) {
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
                {
                alt60=1;
                }
                break;
            case 40:
                {
                alt60=2;
                }
                break;
            case 42:
                {
                alt60=3;
                }
                break;
            case RULE_ID:
                {
                alt60=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 60, 0, input);

                throw nvae;
            }

            switch (alt60) {
                case 1 :
                    // InternalJniMap.g:3588:2: this_BuiltInType_0= ruleBuiltInType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseTypeAccess().getBuiltInTypeParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_BuiltInType_0=ruleBuiltInType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BuiltInType_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalJniMap.g:3601:2: this_StructType_1= ruleStructType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseTypeAccess().getStructTypeParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_StructType_1=ruleStructType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StructType_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalJniMap.g:3614:2: this_EnumType_2= ruleEnumType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseTypeAccess().getEnumTypeParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_EnumType_2=ruleEnumType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EnumType_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalJniMap.g:3627:2: this_TypeAlias_3= ruleTypeAlias
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseTypeAccess().getTypeAliasParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_2);
                    this_TypeAlias_3=ruleTypeAlias();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TypeAlias_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBaseType"


    // $ANTLR start "ruleHostType"
    // InternalJniMap.g:3646:1: ruleHostType returns [Enumerator current=null] : ( (enumLiteral_0= 'linux_32' ) | (enumLiteral_1= 'linux_64' ) | (enumLiteral_2= 'macosx_32' ) | (enumLiteral_3= 'macosx_64' ) | (enumLiteral_4= 'mingw_32' ) | (enumLiteral_5= 'cygwin_32' ) ) ;
    public final Enumerator ruleHostType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;

         enterRule(); 
        try {
            // InternalJniMap.g:3648:28: ( ( (enumLiteral_0= 'linux_32' ) | (enumLiteral_1= 'linux_64' ) | (enumLiteral_2= 'macosx_32' ) | (enumLiteral_3= 'macosx_64' ) | (enumLiteral_4= 'mingw_32' ) | (enumLiteral_5= 'cygwin_32' ) ) )
            // InternalJniMap.g:3649:1: ( (enumLiteral_0= 'linux_32' ) | (enumLiteral_1= 'linux_64' ) | (enumLiteral_2= 'macosx_32' ) | (enumLiteral_3= 'macosx_64' ) | (enumLiteral_4= 'mingw_32' ) | (enumLiteral_5= 'cygwin_32' ) )
            {
            // InternalJniMap.g:3649:1: ( (enumLiteral_0= 'linux_32' ) | (enumLiteral_1= 'linux_64' ) | (enumLiteral_2= 'macosx_32' ) | (enumLiteral_3= 'macosx_64' ) | (enumLiteral_4= 'mingw_32' ) | (enumLiteral_5= 'cygwin_32' ) )
            int alt61=6;
            switch ( input.LA(1) ) {
            case 70:
                {
                alt61=1;
                }
                break;
            case 71:
                {
                alt61=2;
                }
                break;
            case 72:
                {
                alt61=3;
                }
                break;
            case 73:
                {
                alt61=4;
                }
                break;
            case 74:
                {
                alt61=5;
                }
                break;
            case 75:
                {
                alt61=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 61, 0, input);

                throw nvae;
            }

            switch (alt61) {
                case 1 :
                    // InternalJniMap.g:3649:2: (enumLiteral_0= 'linux_32' )
                    {
                    // InternalJniMap.g:3649:2: (enumLiteral_0= 'linux_32' )
                    // InternalJniMap.g:3649:4: enumLiteral_0= 'linux_32'
                    {
                    enumLiteral_0=(Token)match(input,70,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getHostTypeAccess().getLinux32EnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getHostTypeAccess().getLinux32EnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalJniMap.g:3655:6: (enumLiteral_1= 'linux_64' )
                    {
                    // InternalJniMap.g:3655:6: (enumLiteral_1= 'linux_64' )
                    // InternalJniMap.g:3655:8: enumLiteral_1= 'linux_64'
                    {
                    enumLiteral_1=(Token)match(input,71,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getHostTypeAccess().getLinux64EnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getHostTypeAccess().getLinux64EnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalJniMap.g:3661:6: (enumLiteral_2= 'macosx_32' )
                    {
                    // InternalJniMap.g:3661:6: (enumLiteral_2= 'macosx_32' )
                    // InternalJniMap.g:3661:8: enumLiteral_2= 'macosx_32'
                    {
                    enumLiteral_2=(Token)match(input,72,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getHostTypeAccess().getMacosx32EnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getHostTypeAccess().getMacosx32EnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalJniMap.g:3667:6: (enumLiteral_3= 'macosx_64' )
                    {
                    // InternalJniMap.g:3667:6: (enumLiteral_3= 'macosx_64' )
                    // InternalJniMap.g:3667:8: enumLiteral_3= 'macosx_64'
                    {
                    enumLiteral_3=(Token)match(input,73,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getHostTypeAccess().getMacosx64EnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getHostTypeAccess().getMacosx64EnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalJniMap.g:3673:6: (enumLiteral_4= 'mingw_32' )
                    {
                    // InternalJniMap.g:3673:6: (enumLiteral_4= 'mingw_32' )
                    // InternalJniMap.g:3673:8: enumLiteral_4= 'mingw_32'
                    {
                    enumLiteral_4=(Token)match(input,74,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getHostTypeAccess().getMingw32EnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_4, grammarAccess.getHostTypeAccess().getMingw32EnumLiteralDeclaration_4()); 
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalJniMap.g:3679:6: (enumLiteral_5= 'cygwin_32' )
                    {
                    // InternalJniMap.g:3679:6: (enumLiteral_5= 'cygwin_32' )
                    // InternalJniMap.g:3679:8: enumLiteral_5= 'cygwin_32'
                    {
                    enumLiteral_5=(Token)match(input,75,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getHostTypeAccess().getCygwin32EnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_5, grammarAccess.getHostTypeAccess().getCygwin32EnumLiteralDeclaration_5()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHostType"

    // $ANTLR start synpred55_InternalJniMap
    public final void synpred55_InternalJniMap_fragment() throws RecognitionException {   
        Token lv_constructor_2_0=null;
        Token lv_destructor_3_0=null;
        Token lv_instanceof_4_0=null;

        // InternalJniMap.g:2418:4: ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) )
        // InternalJniMap.g:2418:4: ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) )
        {
        // InternalJniMap.g:2418:4: ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) )
        // InternalJniMap.g:2419:5: {...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) )
        {
        if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred55_InternalJniMap", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0)");
        }
        // InternalJniMap.g:2419:105: ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) )
        // InternalJniMap.g:2420:6: ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) )
        {
        getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0);
        // InternalJniMap.g:2423:6: ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) )
        // InternalJniMap.g:2423:7: {...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) )
        {
        if ( !((true)) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred55_InternalJniMap", "true");
        }
        // InternalJniMap.g:2423:16: ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) )
        int alt72=3;
        switch ( input.LA(1) ) {
        case 47:
            {
            alt72=1;
            }
            break;
        case 48:
            {
            alt72=2;
            }
            break;
        case 49:
            {
            alt72=3;
            }
            break;
        default:
            if (state.backtracking>0) {state.failed=true; return ;}
            NoViableAltException nvae =
                new NoViableAltException("", 72, 0, input);

            throw nvae;
        }

        switch (alt72) {
            case 1 :
                // InternalJniMap.g:2423:17: ( (lv_constructor_2_0= 'constructor' ) )
                {
                // InternalJniMap.g:2423:17: ( (lv_constructor_2_0= 'constructor' ) )
                // InternalJniMap.g:2424:1: (lv_constructor_2_0= 'constructor' )
                {
                // InternalJniMap.g:2424:1: (lv_constructor_2_0= 'constructor' )
                // InternalJniMap.g:2425:3: lv_constructor_2_0= 'constructor'
                {
                lv_constructor_2_0=(Token)match(input,47,FOLLOW_2); if (state.failed) return ;

                }


                }


                }
                break;
            case 2 :
                // InternalJniMap.g:2439:6: ( (lv_destructor_3_0= 'destructor' ) )
                {
                // InternalJniMap.g:2439:6: ( (lv_destructor_3_0= 'destructor' ) )
                // InternalJniMap.g:2440:1: (lv_destructor_3_0= 'destructor' )
                {
                // InternalJniMap.g:2440:1: (lv_destructor_3_0= 'destructor' )
                // InternalJniMap.g:2441:3: lv_destructor_3_0= 'destructor'
                {
                lv_destructor_3_0=(Token)match(input,48,FOLLOW_2); if (state.failed) return ;

                }


                }


                }
                break;
            case 3 :
                // InternalJniMap.g:2455:6: ( (lv_instanceof_4_0= 'instanceOf' ) )
                {
                // InternalJniMap.g:2455:6: ( (lv_instanceof_4_0= 'instanceOf' ) )
                // InternalJniMap.g:2456:1: (lv_instanceof_4_0= 'instanceOf' )
                {
                // InternalJniMap.g:2456:1: (lv_instanceof_4_0= 'instanceOf' )
                // InternalJniMap.g:2457:3: lv_instanceof_4_0= 'instanceOf'
                {
                lv_instanceof_4_0=(Token)match(input,49,FOLLOW_2); if (state.failed) return ;

                }


                }


                }
                break;

        }


        }


        }


        }


        }
    }
    // $ANTLR end synpred55_InternalJniMap

    // $ANTLR start synpred56_InternalJniMap
    public final void synpred56_InternalJniMap_fragment() throws RecognitionException {   
        Token lv_static_5_0=null;

        // InternalJniMap.g:2477:4: ( ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) )
        // InternalJniMap.g:2477:4: ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) )
        {
        // InternalJniMap.g:2477:4: ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) )
        // InternalJniMap.g:2478:5: {...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) )
        {
        if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred56_InternalJniMap", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1)");
        }
        // InternalJniMap.g:2478:105: ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) )
        // InternalJniMap.g:2479:6: ({...}? => ( (lv_static_5_0= 'static' ) ) )
        {
        getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1);
        // InternalJniMap.g:2482:6: ({...}? => ( (lv_static_5_0= 'static' ) ) )
        // InternalJniMap.g:2482:7: {...}? => ( (lv_static_5_0= 'static' ) )
        {
        if ( !((true)) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred56_InternalJniMap", "true");
        }
        // InternalJniMap.g:2482:16: ( (lv_static_5_0= 'static' ) )
        // InternalJniMap.g:2483:1: (lv_static_5_0= 'static' )
        {
        // InternalJniMap.g:2483:1: (lv_static_5_0= 'static' )
        // InternalJniMap.g:2484:3: lv_static_5_0= 'static'
        {
        lv_static_5_0=(Token)match(input,38,FOLLOW_2); if (state.failed) return ;

        }


        }


        }


        }


        }


        }
    }
    // $ANTLR end synpred56_InternalJniMap

    // $ANTLR start synpred58_InternalJniMap
    public final void synpred58_InternalJniMap_fragment() throws RecognitionException {   
        Token lv_private_6_0=null;
        Token lv_protected_7_0=null;

        // InternalJniMap.g:2504:4: ( ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) )
        // InternalJniMap.g:2504:4: ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) )
        {
        // InternalJniMap.g:2504:4: ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) )
        // InternalJniMap.g:2505:5: {...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) )
        {
        if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred58_InternalJniMap", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2)");
        }
        // InternalJniMap.g:2505:105: ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) )
        // InternalJniMap.g:2506:6: ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) )
        {
        getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2);
        // InternalJniMap.g:2509:6: ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) )
        // InternalJniMap.g:2509:7: {...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) )
        {
        if ( !((true)) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred58_InternalJniMap", "true");
        }
        // InternalJniMap.g:2509:16: ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) )
        int alt73=2;
        int LA73_0 = input.LA(1);

        if ( (LA73_0==50) ) {
            alt73=1;
        }
        else if ( (LA73_0==51) ) {
            alt73=2;
        }
        else {
            if (state.backtracking>0) {state.failed=true; return ;}
            NoViableAltException nvae =
                new NoViableAltException("", 73, 0, input);

            throw nvae;
        }
        switch (alt73) {
            case 1 :
                // InternalJniMap.g:2509:17: ( (lv_private_6_0= 'private' ) )
                {
                // InternalJniMap.g:2509:17: ( (lv_private_6_0= 'private' ) )
                // InternalJniMap.g:2510:1: (lv_private_6_0= 'private' )
                {
                // InternalJniMap.g:2510:1: (lv_private_6_0= 'private' )
                // InternalJniMap.g:2511:3: lv_private_6_0= 'private'
                {
                lv_private_6_0=(Token)match(input,50,FOLLOW_2); if (state.failed) return ;

                }


                }


                }
                break;
            case 2 :
                // InternalJniMap.g:2525:6: ( (lv_protected_7_0= 'protected' ) )
                {
                // InternalJniMap.g:2525:6: ( (lv_protected_7_0= 'protected' ) )
                // InternalJniMap.g:2526:1: (lv_protected_7_0= 'protected' )
                {
                // InternalJniMap.g:2526:1: (lv_protected_7_0= 'protected' )
                // InternalJniMap.g:2527:3: lv_protected_7_0= 'protected'
                {
                lv_protected_7_0=(Token)match(input,51,FOLLOW_2); if (state.failed) return ;

                }


                }


                }
                break;

        }


        }


        }


        }


        }
    }
    // $ANTLR end synpred58_InternalJniMap

    // $ANTLR start synpred59_InternalJniMap
    public final void synpred59_InternalJniMap_fragment() throws RecognitionException {   
        Token lv_renamed_8_0=null;
        Token otherlv_9=null;
        Token lv_newname_10_0=null;

        // InternalJniMap.g:2547:4: ( ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) )
        // InternalJniMap.g:2547:4: ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) )
        {
        // InternalJniMap.g:2547:4: ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) )
        // InternalJniMap.g:2548:5: {...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) )
        {
        if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred59_InternalJniMap", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3)");
        }
        // InternalJniMap.g:2548:105: ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) )
        // InternalJniMap.g:2549:6: ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) )
        {
        getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3);
        // InternalJniMap.g:2552:6: ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) )
        // InternalJniMap.g:2552:7: {...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) )
        {
        if ( !((true)) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred59_InternalJniMap", "true");
        }
        // InternalJniMap.g:2552:16: ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) )
        // InternalJniMap.g:2552:17: ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) )
        {
        // InternalJniMap.g:2552:17: ( (lv_renamed_8_0= 'rename' ) )
        // InternalJniMap.g:2553:1: (lv_renamed_8_0= 'rename' )
        {
        // InternalJniMap.g:2553:1: (lv_renamed_8_0= 'rename' )
        // InternalJniMap.g:2554:3: lv_renamed_8_0= 'rename'
        {
        lv_renamed_8_0=(Token)match(input,52,FOLLOW_7); if (state.failed) return ;

        }


        }

        otherlv_9=(Token)match(input,14,FOLLOW_4); if (state.failed) return ;
        // InternalJniMap.g:2571:1: ( (lv_newname_10_0= RULE_ID ) )
        // InternalJniMap.g:2572:1: (lv_newname_10_0= RULE_ID )
        {
        // InternalJniMap.g:2572:1: (lv_newname_10_0= RULE_ID )
        // InternalJniMap.g:2573:3: lv_newname_10_0= RULE_ID
        {
        lv_newname_10_0=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;

        }


        }


        }


        }


        }


        }


        }
    }
    // $ANTLR end synpred59_InternalJniMap

    // $ANTLR start synpred60_InternalJniMap
    public final void synpred60_InternalJniMap_fragment() throws RecognitionException {   
        Token lv_stub_11_0=null;

        // InternalJniMap.g:2596:4: ( ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) )
        // InternalJniMap.g:2596:4: ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) )
        {
        // InternalJniMap.g:2596:4: ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) )
        // InternalJniMap.g:2597:5: {...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) )
        {
        if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred60_InternalJniMap", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4)");
        }
        // InternalJniMap.g:2597:105: ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) )
        // InternalJniMap.g:2598:6: ({...}? => ( (lv_stub_11_0= 'stub' ) ) )
        {
        getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4);
        // InternalJniMap.g:2601:6: ({...}? => ( (lv_stub_11_0= 'stub' ) ) )
        // InternalJniMap.g:2601:7: {...}? => ( (lv_stub_11_0= 'stub' ) )
        {
        if ( !((true)) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred60_InternalJniMap", "true");
        }
        // InternalJniMap.g:2601:16: ( (lv_stub_11_0= 'stub' ) )
        // InternalJniMap.g:2602:1: (lv_stub_11_0= 'stub' )
        {
        // InternalJniMap.g:2602:1: (lv_stub_11_0= 'stub' )
        // InternalJniMap.g:2603:3: lv_stub_11_0= 'stub'
        {
        lv_stub_11_0=(Token)match(input,53,FOLLOW_2); if (state.failed) return ;

        }


        }


        }


        }


        }


        }
    }
    // $ANTLR end synpred60_InternalJniMap

    // $ANTLR start synpred61_InternalJniMap
    public final void synpred61_InternalJniMap_fragment() throws RecognitionException {   
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token lv_arg_14_0=null;

        // InternalJniMap.g:2623:4: ( ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )
        // InternalJniMap.g:2623:4: ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) )
        {
        // InternalJniMap.g:2623:4: ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) )
        // InternalJniMap.g:2624:5: {...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) )
        {
        if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred61_InternalJniMap", "getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5)");
        }
        // InternalJniMap.g:2624:105: ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) )
        // InternalJniMap.g:2625:6: ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) )
        {
        getUnorderedGroupHelper().select(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5);
        // InternalJniMap.g:2628:6: ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) )
        // InternalJniMap.g:2628:7: {...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) )
        {
        if ( !((true)) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred61_InternalJniMap", "true");
        }
        // InternalJniMap.g:2628:16: (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) )
        // InternalJniMap.g:2628:18: otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) )
        {
        otherlv_12=(Token)match(input,54,FOLLOW_7); if (state.failed) return ;
        otherlv_13=(Token)match(input,14,FOLLOW_47); if (state.failed) return ;
        // InternalJniMap.g:2636:1: ( (lv_arg_14_0= RULE_INT ) )
        // InternalJniMap.g:2637:1: (lv_arg_14_0= RULE_INT )
        {
        // InternalJniMap.g:2637:1: (lv_arg_14_0= RULE_INT )
        // InternalJniMap.g:2638:3: lv_arg_14_0= RULE_INT
        {
        lv_arg_14_0=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return ;

        }


        }


        }


        }


        }


        }


        }
    }
    // $ANTLR end synpred61_InternalJniMap

    // Delegated rules

    public final boolean synpred60_InternalJniMap() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred60_InternalJniMap_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred59_InternalJniMap() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred59_InternalJniMap_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred58_InternalJniMap() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred58_InternalJniMap_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred55_InternalJniMap() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred55_InternalJniMap_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred56_InternalJniMap() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred56_InternalJniMap_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred61_InternalJniMap() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred61_InternalJniMap_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA47 dfa47 = new DFA47(this);
    protected DFA52 dfa52 = new DFA52(this);
    static final String dfa_1s = "\13\uffff";
    static final String dfa_2s = "\1\23\12\uffff";
    static final String dfa_3s = "\1\66\12\uffff";
    static final String dfa_4s = "\1\uffff\1\7\3\1\1\2\2\3\1\4\1\5\1\6";
    static final String dfa_5s = "\1\0\12\uffff}>";
    static final String[] dfa_6s = {
            "\1\1\22\uffff\1\5\10\uffff\1\2\1\3\1\4\1\6\1\7\1\10\1\11\1\12",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA47 extends DFA {

        public DFA47(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 47;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "()* loopback of 2416:3: ( ({...}? => ( ({...}? => ( ( (lv_constructor_2_0= 'constructor' ) ) | ( (lv_destructor_3_0= 'destructor' ) ) | ( (lv_instanceof_4_0= 'instanceOf' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_static_5_0= 'static' ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_private_6_0= 'private' ) ) | ( (lv_protected_7_0= 'protected' ) ) ) ) ) ) | ({...}? => ( ({...}? => ( ( (lv_renamed_8_0= 'rename' ) ) otherlv_9= '=' ( (lv_newname_10_0= RULE_ID ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_stub_11_0= 'stub' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'this' otherlv_13= '=' ( (lv_arg_14_0= RULE_INT ) ) ) ) ) ) )*";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA47_0 = input.LA(1);

                         
                        int index47_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA47_0==19) ) {s = 1;}

                        else if ( LA47_0 == 47 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {s = 2;}

                        else if ( LA47_0 == 48 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {s = 3;}

                        else if ( LA47_0 == 49 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 0) ) {s = 4;}

                        else if ( LA47_0 == 38 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 1) ) {s = 5;}

                        else if ( LA47_0 == 50 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2) ) {s = 6;}

                        else if ( LA47_0 == 51 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 2) ) {s = 7;}

                        else if ( LA47_0 == 52 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 3) ) {s = 8;}

                        else if ( LA47_0 == 53 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 4) ) {s = 9;}

                        else if ( LA47_0 == 54 && getUnorderedGroupHelper().canSelect(grammarAccess.getMethodAccess().getUnorderedGroup_0_1(), 5) ) {s = 10;}

                         
                        input.seek(index47_0);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 47, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String dfa_7s = "\24\uffff";
    static final String dfa_8s = "\1\uffff\11\21\2\uffff\1\21\5\uffff\2\21";
    static final String dfa_9s = "\15\4\5\uffff\2\4";
    static final String dfa_10s = "\1\105\11\102\2\4\1\102\5\uffff\2\102";
    static final String dfa_11s = "\15\uffff\1\4\1\1\1\2\1\5\1\3\2\uffff";
    static final String dfa_12s = "\24\uffff}>";
    static final String[] dfa_13s = {
            "\1\14\43\uffff\1\12\1\uffff\1\13\14\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\5\uffff\1\15",
            "\1\21\14\uffff\1\20\56\uffff\1\16\1\uffff\1\17",
            "\1\21\14\uffff\1\20\56\uffff\1\16\1\uffff\1\17",
            "\1\21\14\uffff\1\20\56\uffff\1\16\1\uffff\1\17",
            "\1\21\14\uffff\1\20\56\uffff\1\16\1\uffff\1\17",
            "\1\21\14\uffff\1\20\56\uffff\1\16\1\uffff\1\17",
            "\1\21\14\uffff\1\20\56\uffff\1\16\1\uffff\1\17",
            "\1\21\14\uffff\1\20\56\uffff\1\16\1\uffff\1\17",
            "\1\21\14\uffff\1\20\56\uffff\1\16\1\uffff\1\17",
            "\1\21\14\uffff\1\20\56\uffff\1\16\1\uffff\1\17",
            "\1\22",
            "\1\23",
            "\1\21\14\uffff\1\20\56\uffff\1\16\1\uffff\1\17",
            "",
            "",
            "",
            "",
            "",
            "\1\21\14\uffff\1\20\56\uffff\1\16\1\uffff\1\17",
            "\1\21\14\uffff\1\20\56\uffff\1\16\1\uffff\1\17"
    };

    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[][] dfa_13 = unpackEncodedStringArray(dfa_13s);

    class DFA52 extends DFA {

        public DFA52(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 52;
            this.eot = dfa_7;
            this.eof = dfa_8;
            this.min = dfa_9;
            this.max = dfa_10;
            this.accept = dfa_11;
            this.special = dfa_12;
            this.transition = dfa_13;
        }
        public String getDescription() {
            return "2793:1: (this_PtrType_0= rulePtrType | this_ModifiableType_1= ruleModifiableType | this_BaseType_2= ruleBaseType | this_ConstType_3= ruleConstType | this_ArrayType_4= ruleArrayType )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000100800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000021000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000FC0L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x00000798B4000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x00004798B4000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000400000000002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000440000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000001008000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000003208000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000003008000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000002208000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000002008000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000048000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000008000010L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000001208000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000400000000L,0x0000000000000FC0L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0xFF80056400000010L,0x0000000000000020L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0xFF80056000000010L,0x0000000000000020L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0xFF803D6000010010L,0x0000000000000020L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000050000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0xFF803D6000000010L,0x0000000000000020L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0xFF80056000000010L,0x0000000000000028L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0xFF80056400000010L,0x0000000000000028L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000000400040000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0xFF80056000020010L,0x0000000000000020L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0xFF80056400020010L,0x0000000000000020L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x007F804000080000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000003L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});

}
