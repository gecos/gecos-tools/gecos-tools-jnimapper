/*
 * generated by Xtext
 */
package fr.irisa.cairn.serializer;

import com.google.inject.Inject;
import fr.irisa.cairn.services.JniMapGrammarAccess;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AlternativeAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.GroupAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;

@SuppressWarnings("all")
public abstract class AbstractJniMapSyntacticSequencer extends AbstractSyntacticSequencer {

	protected JniMapGrammarAccess grammarAccess;
	protected AbstractElementAlias match_CMethod___ExternKeyword_0_0_or_StaticKeyword_0_1__q;
	protected AbstractElementAlias match_Method___ExternKeyword_1_0_or_StaticKeyword_1_1__q;
	protected AbstractElementAlias match_Method___LeftSquareBracketKeyword_0_0_RightSquareBracketKeyword_0_2__q;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (JniMapGrammarAccess) access;
		match_CMethod___ExternKeyword_0_0_or_StaticKeyword_0_1__q = new AlternativeAlias(false, true, new TokenAlias(false, false, grammarAccess.getCMethodAccess().getExternKeyword_0_0()), new TokenAlias(false, false, grammarAccess.getCMethodAccess().getStaticKeyword_0_1()));
		match_Method___ExternKeyword_1_0_or_StaticKeyword_1_1__q = new AlternativeAlias(false, true, new TokenAlias(false, false, grammarAccess.getMethodAccess().getExternKeyword_1_0()), new TokenAlias(false, false, grammarAccess.getMethodAccess().getStaticKeyword_1_1()));
		match_Method___LeftSquareBracketKeyword_0_0_RightSquareBracketKeyword_0_2__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getMethodAccess().getLeftSquareBracketKeyword_0_0()), new TokenAlias(false, false, grammarAccess.getMethodAccess().getRightSquareBracketKeyword_0_2()));
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if (match_CMethod___ExternKeyword_0_0_or_StaticKeyword_0_1__q.equals(syntax))
				emit_CMethod___ExternKeyword_0_0_or_StaticKeyword_0_1__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_Method___ExternKeyword_1_0_or_StaticKeyword_1_1__q.equals(syntax))
				emit_Method___ExternKeyword_1_0_or_StaticKeyword_1_1__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_Method___LeftSquareBracketKeyword_0_0_RightSquareBracketKeyword_0_2__q.equals(syntax))
				emit_Method___LeftSquareBracketKeyword_0_0_RightSquareBracketKeyword_0_2__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Ambiguous syntax:
	 *     ('extern' | 'static')?
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) res=Type
	 */
	protected void emit_CMethod___ExternKeyword_0_0_or_StaticKeyword_0_1__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     ('extern' | 'static')?
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) ('[' ']')? (ambiguity) res=Type
	 *     arg=INT ']' (ambiguity) res=Type
	 *     constructor?='constructor' ']' (ambiguity) res=Type
	 *     destructor?='destructor' ']' (ambiguity) res=Type
	 *     instanceof?='instanceOf' ']' (ambiguity) res=Type
	 *     newname=ID ']' (ambiguity) res=Type
	 *     private?='private' ']' (ambiguity) res=Type
	 *     protected?='protected' ']' (ambiguity) res=Type
	 *     static?='static' ']' (ambiguity) res=Type
	 *     stub?='stub' ']' (ambiguity) res=Type
	 */
	protected void emit_Method___ExternKeyword_1_0_or_StaticKeyword_1_1__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     ('[' ']')?
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) ('extern' | 'static')? res=Type
	 */
	protected void emit_Method___LeftSquareBracketKeyword_0_0_RightSquareBracketKeyword_0_2__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
