package fr.irisa.cairn.jnimap.generator.java;

import fr.irisa.cairn.jniMap.AliasToClassMapping;
import fr.irisa.cairn.jniMap.ClassMapping;
import fr.irisa.cairn.jniMap.EnumToClassMapping;
import fr.irisa.cairn.jniMap.InterfaceClass;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.PtrType;
import fr.irisa.cairn.jniMap.StructToClassMapping;
import fr.irisa.cairn.jniMap.UnmappedClass;
import fr.irisa.cairn.jnimap.generator.CommonExtensions;
import fr.irisa.cairn.jnimap.generator.TypeExtensions;
import fr.irisa.cairn.jnimap.generator.java.AliasWrapperExtension;
import fr.irisa.cairn.jnimap.generator.java.EnumWrapperExtension;
import fr.irisa.cairn.jnimap.generator.java.InterfaceWrapperExtention;
import fr.irisa.cairn.jnimap.generator.java.JNIJavaUtilities;
import fr.irisa.cairn.jnimap.generator.java.PointerWrapperExtension;
import fr.irisa.cairn.jnimap.generator.java.StructWrapperExtension;
import fr.irisa.cairn.jnimap.generator.java.UnmappedWrapperExtension;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

@SuppressWarnings("all")
public class JNIJavaWrapperTemplate {
  @Extension
  private CommonExtensions _commonExtensions = new CommonExtensions();
  
  @Extension
  private TypeExtensions _typeExtensions = new TypeExtensions();
  
  @Extension
  private JNIJavaUtilities _jNIJavaUtilities = new JNIJavaUtilities();
  
  @Extension
  private StructWrapperExtension structWrapper = new StructWrapperExtension();
  
  @Extension
  private AliasWrapperExtension aliasWrapper = new AliasWrapperExtension();
  
  @Extension
  private EnumWrapperExtension _enumWrapperExtension = new EnumWrapperExtension();
  
  @Extension
  private PointerWrapperExtension _pointerWrapperExtension = new PointerWrapperExtension();
  
  @Extension
  private UnmappedWrapperExtension unmappedWrapepr = new UnmappedWrapperExtension();
  
  @Extension
  private InterfaceWrapperExtention interfaceWrapper = new InterfaceWrapperExtention();
  
  public void compile(final Mapping m, final IFileSystemAccess fsa) {
    Set<String> generatedFiles = new TreeSet<String>();
    EList<ClassMapping> _classMapping = m.getClassMapping();
    for (final ClassMapping cm : _classMapping) {
      {
        fsa.generateFile(this.classMappingFilepath(cm, m), this.classMappingContent(cm, m));
        generatedFiles.add(this.classMappingFilepath(cm, m));
        if ((cm instanceof StructToClassMapping)) {
          final StructToClassMapping s2c = ((StructToClassMapping) cm);
          boolean _isEmpty = this._typeExtensions.getAllSubClasses(s2c).isEmpty();
          boolean _not = (!_isEmpty);
          if (_not) {
            fsa.generateFile(this.structWrapper.visitorFilepath(s2c, m), this.structWrapper.visitorContent(s2c, m));
          }
        }
      }
    }
    Iterable<PtrType> _iterable = IteratorExtensions.<PtrType>toIterable(this._typeExtensions.getAllPrimitivePtrTypes(m));
    for (final PtrType pt : _iterable) {
      boolean _contains = generatedFiles.contains(this.pointerMappingFilepath(pt, m));
      boolean _not = (!_contains);
      if (_not) {
        fsa.generateFile(this.pointerMappingFilepath(pt, m), this._pointerWrapperExtension.pointerMappingContent(pt, m));
      }
    }
  }
  
  public String pointerMappingFilepath(final PtrType pt, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _javaRootPath = this._commonExtensions.javaRootPath();
    _builder.append(_javaRootPath);
    String _packagePath = this._commonExtensions.packagePath(m);
    _builder.append(_packagePath);
    _builder.append("/");
    String _standardJavaName = this._typeExtensions.standardJavaName(pt);
    _builder.append(_standardJavaName);
    _builder.append(".java");
    return _builder.toString();
  }
  
  public String classMappingFilepath(final ClassMapping cm, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _javaRootPath = this._commonExtensions.javaRootPath();
    _builder.append(_javaRootPath);
    String _packagePath = this._commonExtensions.packagePath(m);
    _builder.append(_packagePath);
    _builder.append("/");
    CharSequence _javaClassName = this._jNIJavaUtilities.javaClassName(cm);
    _builder.append(_javaClassName);
    _builder.append(".java");
    return _builder.toString();
  }
  
  protected CharSequence _classMappingContent(final ClassMapping cm, final Mapping m) {
    Class<? extends ClassMapping> _class = cm.getClass();
    String _plus = ("[Error] Unsupported Mapping type: " + _class);
    System.err.println(_plus);
    StringConcatenation _builder = new StringConcatenation();
    return _builder.toString();
  }
  
  protected CharSequence _classMappingContent(final StructToClassMapping cm, final Mapping m) {
    return this.structWrapper.classMappingContent(cm, m);
  }
  
  protected CharSequence _classMappingContent(final EnumToClassMapping cm, final Mapping m) {
    return this._enumWrapperExtension.enumToClassMappingContent(cm, m);
  }
  
  protected CharSequence _classMappingContent(final AliasToClassMapping cm, final Mapping m) {
    return this.aliasWrapper.classMappingContent(cm, m);
  }
  
  protected CharSequence _classMappingContent(final UnmappedClass cm, final Mapping m) {
    return this.unmappedWrapepr.classMappingContent(cm, m);
  }
  
  protected CharSequence _classMappingContent(final InterfaceClass cm, final Mapping m) {
    return this.interfaceWrapper.classMappingContent(cm, m);
  }
  
  public CharSequence classMappingContent(final ClassMapping cm, final Mapping m) {
    if (cm instanceof AliasToClassMapping) {
      return _classMappingContent((AliasToClassMapping)cm, m);
    } else if (cm instanceof EnumToClassMapping) {
      return _classMappingContent((EnumToClassMapping)cm, m);
    } else if (cm instanceof InterfaceClass) {
      return _classMappingContent((InterfaceClass)cm, m);
    } else if (cm instanceof StructToClassMapping) {
      return _classMappingContent((StructToClassMapping)cm, m);
    } else if (cm instanceof UnmappedClass) {
      return _classMappingContent((UnmappedClass)cm, m);
    } else if (cm != null) {
      return _classMappingContent(cm, m);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(cm, m).toString());
    }
  }
}
