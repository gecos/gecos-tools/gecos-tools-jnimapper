package fr.irisa.cairn.jnimap.generator;

import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jnimap.generator.build.JNIMakefileTemplate;
import fr.irisa.cairn.jnimap.generator.c.JNICTemplate;
import fr.irisa.cairn.jnimap.generator.java.JNIJavaTemplate;
import fr.irisa.cairn.jnimap.generator.java.JNIJavaWrapperTemplate;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.xbase.lib.Extension;

/**
 * Naming convention:
 *  - xxxTemplate   : Main entry point for code generation.
 *                    Each one of them have a "compile" method, which does all the job.
 *                    Only the compile method should have access to FileSystemAccess
 *  - xxxExtensions : Collection of helper methods
 */
@SuppressWarnings("all")
public class JNIMappingGenerator implements IGenerator {
  @Extension
  private JNICTemplate jniCtemplate = new JNICTemplate();
  
  @Extension
  private JNIJavaTemplate jniJavaTemplate = new JNIJavaTemplate();
  
  @Extension
  private JNIJavaWrapperTemplate jniJavaWrapperTemplate = new JNIJavaWrapperTemplate();
  
  @Extension
  private JNIMakefileTemplate jniMakeTemplate = new JNIMakefileTemplate();
  
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess fsa) {
    EObject _get = resource.getContents().get(0);
    final Mapping mapping = ((Mapping) _get);
    this.jniCtemplate.compile(mapping, fsa);
    this.jniJavaTemplate.compile(mapping, fsa);
    this.jniJavaWrapperTemplate.compile(mapping, fsa);
    this.jniMakeTemplate.compile(mapping, fsa);
  }
}
