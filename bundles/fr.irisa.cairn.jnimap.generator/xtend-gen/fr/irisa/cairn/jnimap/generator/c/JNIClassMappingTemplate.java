package fr.irisa.cairn.jnimap.generator.c;

import fr.irisa.cairn.jniMap.AliasToClassMapping;
import fr.irisa.cairn.jniMap.ClassMapping;
import fr.irisa.cairn.jniMap.ClassToClassMapping;
import fr.irisa.cairn.jniMap.EnumToClassMapping;
import fr.irisa.cairn.jniMap.FieldDef;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.Method;
import fr.irisa.cairn.jniMap.ParamDef;
import fr.irisa.cairn.jniMap.PtrType;
import fr.irisa.cairn.jniMap.StringType;
import fr.irisa.cairn.jniMap.StructDeclaration;
import fr.irisa.cairn.jniMap.StructToClassMapping;
import fr.irisa.cairn.jniMap.StructType;
import fr.irisa.cairn.jniMap.Type;
import fr.irisa.cairn.jniMap.TypeAlias;
import fr.irisa.cairn.jniMap.UnmappedClass;
import fr.irisa.cairn.jniMap.UserModule;
import fr.irisa.cairn.jnimap.generator.CommonExtensions;
import fr.irisa.cairn.jnimap.generator.TypeExtensions;
import fr.irisa.cairn.jnimap.generator.c.JNICParamExtensions;
import fr.irisa.cairn.jnimap.generator.c.JNICUtilities;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * Loosely corresponds to JNIClassWrapperTemplate.xpt
 */
@SuppressWarnings("all")
public class JNIClassMappingTemplate {
  @Extension
  private CommonExtensions _commonExtensions = new CommonExtensions();
  
  @Extension
  private TypeExtensions _typeExtensions = new TypeExtensions();
  
  @Extension
  private JNICUtilities _jNICUtilities = new JNICUtilities();
  
  @Extension
  private JNICParamExtensions _jNICParamExtensions = new JNICParamExtensions();
  
  public void compile(final ClassMapping cm, final Mapping m, final IFileSystemAccess fsa) {
    boolean _isEmpty = this._commonExtensions.getMethods(cm, m).isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      fsa.generateFile(this.ClassMappingFilepath(cm, m), this.ClassMappingContent(cm, m));
    }
  }
  
  public String ClassMappingFilepath(final ClassMapping cm, final Mapping m) {
    CharSequence _cRootPath = this._commonExtensions.cRootPath();
    String _name = m.getName();
    String _plus = (_cRootPath + _name);
    String _plus_1 = (_plus + "_");
    String _name_1 = cm.getName();
    String _plus_2 = (_plus_1 + _name_1);
    return (_plus_2 + "_native.c");
  }
  
  public CharSequence ClassMappingContent(final ClassMapping cm, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _commonIncludes = this._jNICUtilities.commonIncludes(m);
    _builder.append(_commonIncludes);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    {
      EList<UserModule> _userModules = m.getUserModules();
      for(final UserModule module : _userModules) {
        _builder.append("#include \"");
        String _identifier = this._commonExtensions.identifier(module);
        _builder.append(_identifier);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("#include \"");
    String _nativeInterfaceHeader = this._commonExtensions.nativeInterfaceHeader(m);
    _builder.append(_nativeInterfaceHeader);
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("extern void throwException(JNIEnv * env, char* msg);");
    _builder.newLine();
    _builder.append("extern jobject createInteger(JNIEnv * env, int value);");
    _builder.newLine();
    _builder.append("extern jint getIntegerValue(JNIEnv * env, jobject obj);");
    _builder.newLine();
    _builder.newLine();
    CharSequence _cGetterSetter = this.cGetterSetter(cm);
    _builder.append(_cGetterSetter);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    {
      EList<Method> _methods = this._commonExtensions.getMethods(cm, m);
      for(final Method method : _methods) {
        CharSequence _cMethodDef = this.cMethodDef(method);
        _builder.append(_cMethodDef);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence cMethodDef(final Method m) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _JNIFunctionHeader = this._jNICUtilities.JNIFunctionHeader(m);
    _builder.append(_JNIFunctionHeader);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("#ifdef TRACE_ALL");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("printf(\"Entering ");
    String _name = m.getName();
    _builder.append(_name, "\t");
    _builder.append("\\n\");fflush(stdout);");
    _builder.newLineIfNotEmpty();
    _builder.append("#endif");
    _builder.newLine();
    {
      EList<ParamDef> _params = m.getParams();
      for(final ParamDef param : _params) {
        _builder.append("\t");
        CharSequence _localVarParam = this._jNICParamExtensions.localVarParam(param.getType(), param.getName(), m.getName());
        _builder.append(_localVarParam, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("\t");
    CharSequence _resultInstance = this._jNICUtilities.resultInstance(m.getRes());
    _builder.append(_resultInstance, "\t");
    _builder.append(" ");
    String _name_1 = m.getName();
    _builder.append(_name_1, "\t");
    _builder.append("(");
    final Function1<ParamDef, CharSequence> _function = (ParamDef p) -> {
      return this._jNICUtilities.paramInstance(p);
    };
    String _join = IterableExtensions.<ParamDef>join(m.getParams(), ", ", _function);
    _builder.append(_join, "\t");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    {
      EList<ParamDef> _params_1 = m.getParams();
      for(final ParamDef param_1 : _params_1) {
        _builder.append("\t");
        CharSequence _localVarParamCleanup = this._jNICParamExtensions.localVarParamCleanup(param_1.getType(), param_1.getName(), m.getName());
        _builder.append(_localVarParamCleanup, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("#ifdef TRACE_ALL");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("printf(\"Leaving ");
    String _name_2 = m.getName();
    _builder.append(_name_2, "\t");
    _builder.append("\\n\");fflush(stdout);");
    _builder.newLineIfNotEmpty();
    _builder.append("#endif");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    CharSequence _returnStatement = this._jNICUtilities.returnStatement(m.getRes());
    _builder.append(_returnStatement);
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _cGetterSetter(final StructToClassMapping cm) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<FieldDef> _fields = cm.getStruct().getFields();
      for(final FieldDef field : _fields) {
        String _cStructFieldGetter = this.cStructFieldGetter(field);
        _builder.append(_cStructFieldGetter);
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        String _cStructFieldSetter = this.cStructFieldSetter(field);
        _builder.append(_cStructFieldSetter);
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        String _cStructFieldTester = this.cStructFieldTester(field);
        _builder.append(_cStructFieldTester);
        _builder.newLineIfNotEmpty();
        _builder.newLine();
      }
    }
    return _builder;
  }
  
  protected CharSequence _cGetterSetter(final AliasToClassMapping cm) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _cGetterSetter(final ClassToClassMapping cm) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _cGetterSetter(final EnumToClassMapping cm) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _cGetterSetter(final UnmappedClass cm) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  public String cStructFieldTester(final FieldDef fd) {
    EObject _eContainer = fd.eContainer();
    final StructDeclaration struct = ((StructDeclaration) _eContainer);
    final Mapping mapping = this._commonExtensions.getRoot(fd);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("JNIEXPORT jint JNICALL");
    _builder.newLine();
    _builder.append("Java_");
    String _packagePrefix = this._commonExtensions.packagePrefix(mapping);
    _builder.append(_packagePrefix);
    _builder.append("_");
    String _ID = this._commonExtensions.ID(mapping);
    _builder.append(_ID);
    _builder.append("Native_");
    String _replaceAll = struct.getName().replaceAll("_", "_1");
    _builder.append(_replaceAll);
    _builder.append("_1test_1");
    String _replaceAll_1 = fd.getName().replaceAll("_", "_1");
    _builder.append(_replaceAll_1);
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("(JNIEnv *env, jclass class, jlong ptr) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/* PROTECTED REGION ID(");
    String _name = struct.getName();
    _builder.append(_name, "\t");
    _builder.append("_");
    String _name_1 = fd.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_tester) DISABLED START */");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("struct ");
    String _name_2 = struct.getName();
    _builder.append(_name_2, "\t");
    _builder.append("* stPtr = (struct ");
    String _name_3 = struct.getName();
    _builder.append(_name_3, "\t");
    _builder.append(" *) GECOS_PTRSIZE ptr;");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if(stPtr==NULL)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("throwException(env, \"Null Pointer in get");
    String _ID_1 = this._commonExtensions.ID(fd);
    _builder.append(_ID_1, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    CharSequence _testerResultStatement = this.testerResultStatement(fd);
    _builder.append(_testerResultStatement, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("/* PROTECTED REGION END */");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  public String cStructFieldSetter(final FieldDef fd) {
    EObject _eContainer = fd.eContainer();
    final StructDeclaration struct = ((StructDeclaration) _eContainer);
    final Mapping mapping = this._commonExtensions.getRoot(fd);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("JNIEXPORT void JNICALL");
    _builder.newLine();
    _builder.append("Java_");
    String _packagePrefix = this._commonExtensions.packagePrefix(mapping);
    _builder.append(_packagePrefix);
    _builder.append("_");
    String _ID = this._commonExtensions.ID(mapping);
    _builder.append(_ID);
    _builder.append("Native_");
    String _name = struct.getName();
    _builder.append(_name);
    _builder.append("_1set_1");
    String _replaceAll = fd.getName().replaceAll("_", "_1");
    _builder.append(_replaceAll);
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("(JNIEnv *env, jclass class, jlong ptr, ");
    String _jNICName = this._typeExtensions.getJNICName(fd.getType());
    _builder.append(_jNICName, "\t");
    _builder.append(" value) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("/* PROTECTED REGION ID(");
    String _name_1 = struct.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_");
    String _name_2 = fd.getName();
    _builder.append(_name_2, "\t");
    _builder.append("_setter) DISABLED START */");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("struct ");
    String _name_3 = struct.getName();
    _builder.append(_name_3, "\t");
    _builder.append("* stPtr = (struct ");
    String _name_4 = struct.getName();
    _builder.append(_name_4, "\t");
    _builder.append(" *) GECOS_PTRSIZE ptr;");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if(stPtr==NULL)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("throwException(env, \"Null Pointer in set");
    String _ID_1 = this._commonExtensions.ID(fd);
    _builder.append(_ID_1, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    CharSequence _setterResultStatement = this.setterResultStatement(fd);
    _builder.append(_setterResultStatement, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("/* PROTECTED REGION END */");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  public CharSequence setterResultStatement(final FieldDef fd) {
    return this.setterResultStatement(fd.getType(), fd);
  }
  
  private CharSequence _setterResultStatement(final Type type, final FieldDef fd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("stPtr->");
    String _name = fd.getName();
    _builder.append(_name);
    _builder.append("= (");
    String _standardCName = this._typeExtensions.getStandardCName(type);
    _builder.append(_standardCName);
    _builder.append(") GECOS_PTRSIZE value;");
    return _builder;
  }
  
  private CharSequence _setterResultStatement(final StructType type, final FieldDef fd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("memcpy(value, &stPtr->");
    String _name = fd.getName();
    _builder.append(_name);
    _builder.append(", sizeof(");
    String _standardCName = this._typeExtensions.getStandardCName(type);
    _builder.append(_standardCName);
    _builder.append("));");
    return _builder;
  }
  
  private CharSequence _setterResultStatement(final TypeAlias type, final FieldDef fd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("memcpy(value, &stPtr->");
    String _name = fd.getName();
    _builder.append(_name);
    _builder.append(", sizeof(");
    String _standardCName = this._typeExtensions.getStandardCName(type);
    _builder.append(_standardCName);
    _builder.append("));");
    return _builder;
  }
  
  private CharSequence _setterResultStatement(final StringType type, final FieldDef fd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("char* str_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, value, NULL);");
    _builder.newLine();
    _builder.append("strcpy(str_c, stPtr->");
    String _name = fd.getName();
    _builder.append(_name);
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  public String cStructFieldGetter(final FieldDef fd) {
    EObject _eContainer = fd.eContainer();
    final StructDeclaration struct = ((StructDeclaration) _eContainer);
    final Mapping mapping = this._commonExtensions.getRoot(fd);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("JNIEXPORT ");
    String _jNICName = this._typeExtensions.getJNICName(fd.getType());
    _builder.append(_jNICName);
    _builder.append(" JNICALL");
    _builder.newLineIfNotEmpty();
    _builder.append("Java_");
    String _packagePrefix = this._commonExtensions.packagePrefix(mapping);
    _builder.append(_packagePrefix);
    _builder.append("_");
    String _ID = this._commonExtensions.ID(mapping);
    _builder.append(_ID);
    _builder.append("Native_");
    String _replaceAll = struct.getName().replaceAll("_", "_1");
    _builder.append(_replaceAll);
    _builder.append("_1get_1");
    String _replaceAll_1 = fd.getName().replaceAll("_", "_1");
    _builder.append(_replaceAll_1);
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("(JNIEnv *env, jclass class, jlong ptr) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/* PROTECTED REGION ID(");
    String _name = struct.getName();
    _builder.append(_name, "\t");
    _builder.append("_");
    String _name_1 = fd.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_getter) DISABLED START */");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("struct ");
    String _name_2 = struct.getName();
    _builder.append(_name_2, "\t");
    _builder.append("* stPtr = (struct ");
    String _name_3 = struct.getName();
    _builder.append(_name_3, "\t");
    _builder.append(" *) GECOS_PTRSIZE ptr;");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if(stPtr==NULL)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("throwException(env, \"Null Pointer in get");
    String _ID_1 = this._commonExtensions.ID(fd);
    _builder.append(_ID_1, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    CharSequence _terResultStatement = this.getterResultStatement(fd);
    _builder.append(_terResultStatement, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("/* PROTECTED REGION END */");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  public CharSequence getterResultStatement(final FieldDef fd) {
    return this.getterResultStatement(fd.getType(), fd);
  }
  
  private CharSequence _getterResultStatement(final Type type, final FieldDef fd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("return (");
    String _jNICName = this._typeExtensions.getJNICName(type);
    _builder.append(_jNICName);
    _builder.append(") GECOS_PTRSIZE stPtr->");
    String _name = fd.getName();
    _builder.append(_name);
    _builder.append(";");
    return _builder;
  }
  
  private CharSequence _getterResultStatement(final PtrType type, final FieldDef fd) {
    CharSequence _xifexpression = null;
    boolean _isCopy = fd.isCopy();
    if (_isCopy) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("return (");
      String _jNICName = this._typeExtensions.getJNICName(type);
      _builder.append(_jNICName);
      _builder.append(") GECOS_PTRSIZE ");
      String _standardCName = this._typeExtensions.getStandardCName(type.getBase());
      _builder.append(_standardCName);
      _builder.append("_copy(stPtr->");
      String _name = fd.getName();
      _builder.append(_name);
      _builder.append(");");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("return (");
      String _jNICName_1 = this._typeExtensions.getJNICName(type);
      _builder_1.append(_jNICName_1);
      _builder_1.append(") GECOS_PTRSIZE stPtr->");
      String _name_1 = fd.getName();
      _builder_1.append(_name_1);
      _builder_1.append(";");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  private CharSequence _getterResultStatement(final StructType type, final FieldDef fd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("return (");
    String _jNICName = this._typeExtensions.getJNICName(type);
    _builder.append(_jNICName);
    _builder.append(") GECOS_PTRSIZE (& stPtr->");
    String _name = fd.getName();
    _builder.append(_name);
    _builder.append(");");
    return _builder;
  }
  
  private CharSequence _getterResultStatement(final TypeAlias type, final FieldDef fd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("return (");
    String _jNICName = this._typeExtensions.getJNICName(type);
    _builder.append(_jNICName);
    _builder.append(") GECOS_PTRSIZE (& stPtr->");
    String _name = fd.getName();
    _builder.append(_name);
    _builder.append(");");
    return _builder;
  }
  
  private CharSequence _getterResultStatement(final StringType type, final FieldDef fd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("return (*env)->NewStringUTF(env, stPtr->");
    String _name = fd.getName();
    _builder.append(_name);
    _builder.append(");");
    return _builder;
  }
  
  public CharSequence testerResultStatement(final FieldDef fd) {
    return this.testerResultStatement(fd.getType(), fd);
  }
  
  private CharSequence _testerResultStatement(final Type type, final FieldDef fd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("return (GECOS_PTRSIZE stPtr->");
    String _name = fd.getName();
    _builder.append(_name);
    _builder.append(") != (GECOS_PTRSIZE NULL);");
    return _builder;
  }
  
  private CharSequence _testerResultStatement(final PtrType type, final FieldDef fd) {
    CharSequence _xifexpression = null;
    boolean _isCopy = fd.isCopy();
    if (_isCopy) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("return (GECOS_PTRSIZE ");
      String _standardCName = this._typeExtensions.getStandardCName(type.getBase());
      _builder.append(_standardCName);
      _builder.append("_copy(stPtr->");
      String _name = fd.getName();
      _builder.append(_name);
      _builder.append(")) != (GECOS_PTRSIZE NULL);");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("return (GECOS_PTRSIZE stPtr->");
      String _name_1 = fd.getName();
      _builder_1.append(_name_1);
      _builder_1.append(") != (GECOS_PTRSIZE NULL);");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  private CharSequence _testerResultStatement(final StructType type, final FieldDef fd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("return (GECOS_PTRSIZE (& stPtr->");
    String _name = fd.getName();
    _builder.append(_name);
    _builder.append(")) != (GECOS_PTRSIZE NULL);");
    return _builder;
  }
  
  private CharSequence _testerResultStatement(final TypeAlias type, final FieldDef fd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("return (GECOS_PTRSIZE (& stPtr->");
    String _name = fd.getName();
    _builder.append(_name);
    _builder.append(")) != (GECOS_PTRSIZE NULL);");
    return _builder;
  }
  
  private CharSequence _testerResultStatement(final StringType type, final FieldDef fd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("return (GECOS_PTRSIZE stPtr->");
    String _name = fd.getName();
    _builder.append(_name);
    _builder.append(") != (GECOS_PTRSIZE NULL);");
    return _builder;
  }
  
  public CharSequence cGetterSetter(final ClassMapping cm) {
    if (cm instanceof AliasToClassMapping) {
      return _cGetterSetter((AliasToClassMapping)cm);
    } else if (cm instanceof ClassToClassMapping) {
      return _cGetterSetter((ClassToClassMapping)cm);
    } else if (cm instanceof EnumToClassMapping) {
      return _cGetterSetter((EnumToClassMapping)cm);
    } else if (cm instanceof StructToClassMapping) {
      return _cGetterSetter((StructToClassMapping)cm);
    } else if (cm instanceof UnmappedClass) {
      return _cGetterSetter((UnmappedClass)cm);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(cm).toString());
    }
  }
  
  private CharSequence setterResultStatement(final Type type, final FieldDef fd) {
    if (type instanceof StringType) {
      return _setterResultStatement((StringType)type, fd);
    } else if (type instanceof StructType) {
      return _setterResultStatement((StructType)type, fd);
    } else if (type instanceof TypeAlias) {
      return _setterResultStatement((TypeAlias)type, fd);
    } else if (type != null) {
      return _setterResultStatement(type, fd);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type, fd).toString());
    }
  }
  
  private CharSequence getterResultStatement(final Type type, final FieldDef fd) {
    if (type instanceof StringType) {
      return _getterResultStatement((StringType)type, fd);
    } else if (type instanceof StructType) {
      return _getterResultStatement((StructType)type, fd);
    } else if (type instanceof TypeAlias) {
      return _getterResultStatement((TypeAlias)type, fd);
    } else if (type instanceof PtrType) {
      return _getterResultStatement((PtrType)type, fd);
    } else if (type != null) {
      return _getterResultStatement(type, fd);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type, fd).toString());
    }
  }
  
  private CharSequence testerResultStatement(final Type type, final FieldDef fd) {
    if (type instanceof StringType) {
      return _testerResultStatement((StringType)type, fd);
    } else if (type instanceof StructType) {
      return _testerResultStatement((StructType)type, fd);
    } else if (type instanceof TypeAlias) {
      return _testerResultStatement((TypeAlias)type, fd);
    } else if (type instanceof PtrType) {
      return _testerResultStatement((PtrType)type, fd);
    } else if (type != null) {
      return _testerResultStatement(type, fd);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type, fd).toString());
    }
  }
}
