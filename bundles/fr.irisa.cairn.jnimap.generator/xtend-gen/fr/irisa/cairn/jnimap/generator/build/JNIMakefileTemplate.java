package fr.irisa.cairn.jnimap.generator.build;

import com.google.common.base.Objects;
import fr.irisa.cairn.jniMap.ClassMapping;
import fr.irisa.cairn.jniMap.ExternalLibrary;
import fr.irisa.cairn.jniMap.HostType;
import fr.irisa.cairn.jniMap.Library;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.UserModule;
import fr.irisa.cairn.jnimap.generator.CommonExtensions;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

/**
 * Port of the MakefileTemplate2.xpt
 */
@SuppressWarnings("all")
public class JNIMakefileTemplate {
  @Extension
  private CommonExtensions _commonExtensions = new CommonExtensions();
  
  public String launcherFilepath(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _cRootPath = this._commonExtensions.cRootPath();
    _builder.append(_cRootPath);
    _builder.append("/build/Makefile");
    return _builder.toString();
  }
  
  public String userFilepath(final HostType host) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _cRootPath = this._commonExtensions.cRootPath();
    _builder.append(_cRootPath);
    _builder.append("/build/");
    CharSequence _dirname = this._commonExtensions.dirname(host);
    _builder.append(_dirname);
    _builder.append("/user.mk");
    return _builder.toString();
  }
  
  public String librariesFilepath(final HostType host) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _cRootPath = this._commonExtensions.cRootPath();
    _builder.append(_cRootPath);
    _builder.append("/build/");
    CharSequence _dirname = this._commonExtensions.dirname(host);
    _builder.append(_dirname);
    _builder.append("/libraries.mk");
    return _builder.toString();
  }
  
  public String makefileFilepath(final HostType host) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _cRootPath = this._commonExtensions.cRootPath();
    _builder.append(_cRootPath);
    _builder.append("/build/");
    CharSequence _dirname = this._commonExtensions.dirname(host);
    _builder.append(_dirname);
    _builder.append("/Makefile");
    return _builder.toString();
  }
  
  public void compile(final Mapping m, final IFileSystemAccess fsa) {
    fsa.generateFile(this.launcherFilepath(m), this.launcherContent(m));
    EList<HostType> _hosts = m.getHosts();
    for (final HostType host : _hosts) {
      {
        fsa.generateFile(this.userFilepath(host), this.userContent(m, host));
        fsa.generateFile(this.librariesFilepath(host), this.librariesContent(m, host));
        fsa.generateFile(this.makefileFilepath(host), this.makefileContent(m, host));
      }
    }
  }
  
  public int checkHostTypeSupport(final Mapping m, final HostType t) {
    int _xifexpression = (int) 0;
    boolean _contains = m.getHosts().contains(t);
    if (_contains) {
      _xifexpression = 1;
    } else {
      _xifexpression = 0;
    }
    return _xifexpression;
  }
  
  public CharSequence launcherContent(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(".PHONY: ");
    final Function1<HostType, CharSequence> _function = (HostType host) -> {
      return this._commonExtensions.dirname(host);
    };
    String _join = IterableExtensions.<HostType>join(m.getHosts(), " ", _function);
    _builder.append(_join);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("AUTODETECT=0");
    _builder.newLine();
    _builder.append("SYSTEM=$(shell uname -s)");
    _builder.newLine();
    _builder.append("ifeq ($(SYSTEM),Darwin)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("HOST=Macosx");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("ARCH=$(shell sysctl -a hw.optional.x86_64 | cut -d\" \" -f2)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("ifeq ($(ARCH),1)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("ARCH=64");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("AUTODETECT=");
    int _checkHostTypeSupport = this.checkHostTypeSupport(m, HostType.MACOSX64);
    _builder.append(_checkHostTypeSupport, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("else ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("ARCH=32");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("AUTODETECT=");
    int _checkHostTypeSupport_1 = this.checkHostTypeSupport(m, HostType.MACOSX32);
    _builder.append(_checkHostTypeSupport_1, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("endif");
    _builder.newLine();
    _builder.append("else");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("ifeq ($(SYSTEM),Linux)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("HOST=Linux");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("ARCH=$(shell uname -m)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("ifeq ($(ARCH),x86_64)");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("ARCH=64");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("AUTODETECT=");
    int _checkHostTypeSupport_2 = this.checkHostTypeSupport(m, HostType.LINUX64);
    _builder.append(_checkHostTypeSupport_2, "\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("else ");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("ARCH=32");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("AUTODETECT=");
    int _checkHostTypeSupport_3 = this.checkHostTypeSupport(m, HostType.LINUX32);
    _builder.append(_checkHostTypeSupport_3, "\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("endif");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("else");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("AUTODETECT=0");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("endif");
    _builder.newLine();
    _builder.append("endif");
    _builder.newLine();
    _builder.newLine();
    _builder.append("all:");
    _builder.newLine();
    _builder.append("ifeq ($(AUTODETECT),1)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@echo \">> Autodetecting host machine : $(HOST)_$(ARCH)\"");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@$(MAKE) -s -f $(HOST)_$(ARCH)/Makefile clean");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@$(MAKE) -s -f $(HOST)_$(ARCH)/Makefile lib");
    _builder.newLine();
    _builder.append("else");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@echo \"Cannot detect host machine, or the host machine is unsupported. Please use one of the following :\"");
    _builder.newLine();
    {
      EList<HostType> _hosts = m.getHosts();
      for(final HostType host : _hosts) {
        _builder.append("\t");
        _builder.append("@echo \"    - make ");
        CharSequence _dirname = this._commonExtensions.dirname(host);
        _builder.append(_dirname, "\t");
        _builder.append(" \"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("endif");
    _builder.newLine();
    _builder.newLine();
    _builder.append("clean:");
    _builder.newLine();
    _builder.append("ifeq ($(AUTODETECT),1)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@echo \">> Autodetecting host machine : $(HOST)_$(ARCH)\"");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@$(MAKE) -s -f $(HOST)_$(ARCH)/Makefile clean");
    _builder.newLine();
    _builder.append("else");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@echo \"Cannot detect host machine, or the host machine is unsupported. Please use one of the following :\"");
    _builder.newLine();
    {
      EList<HostType> _hosts_1 = m.getHosts();
      for(final HostType host_1 : _hosts_1) {
        _builder.append("\t");
        _builder.append("@echo \"    - make -s -f ");
        CharSequence _dirname_1 = this._commonExtensions.dirname(host_1);
        _builder.append(_dirname_1, "\t");
        _builder.append("/Makefile clean \"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("endif");
    _builder.newLine();
    _builder.newLine();
    {
      EList<HostType> _hosts_2 = m.getHosts();
      for(final HostType host_2 : _hosts_2) {
        CharSequence _dirname_2 = this._commonExtensions.dirname(host_2);
        _builder.append(_dirname_2);
        _builder.append(" :");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("@$(MAKE) -s -f ./");
        CharSequence _dirname_3 = this._commonExtensions.dirname(host_2);
        _builder.append(_dirname_3, "\t");
        _builder.append("/Makefile clean");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("@$(MAKE) -s -f ./");
        CharSequence _dirname_4 = this._commonExtensions.dirname(host_2);
        _builder.append(_dirname_4, "\t");
        _builder.append("/Makefile lib");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  public CharSequence userContent(final Mapping m, final HostType host) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#PROTECTED REGION ID(user_");
    CharSequence _dirname = this._commonExtensions.dirname(host);
    _builder.append(_dirname);
    _builder.append(") ENABLED START#");
    _builder.newLineIfNotEmpty();
    {
      boolean _isMac = this._commonExtensions.isMac(host);
      if (_isMac) {
        _builder.append("JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Versions/Current");
        _builder.newLine();
        _builder.append("JAVA_EXTRA_INC= -I$(JAVA_HOME)/Headers");
        _builder.newLine();
        _builder.newLine();
        _builder.append("EXTRA_LIBS=");
        _builder.newLine();
        _builder.append("EXTRA_INCDIR=");
        _builder.newLine();
      } else {
        _builder.append("EXTRA_CFLAGS=");
        _builder.newLine();
        _builder.append("JAVA_HOME=/usr/lib/jvm/default-java/");
        _builder.newLine();
        _builder.append("JAVA_EXTRA_INC= -I$(JAVA_HOME)/include/linux");
        _builder.newLine();
        _builder.append("EXTRA_LIBS=");
        _builder.newLine();
        _builder.append("EXTRA_INCDIR=");
        _builder.newLine();
      }
    }
    _builder.append("#PROTECTED REGION END#");
    _builder.newLine();
    _builder.newLine();
    _builder.append("JAVA_H=$(JAVA_HOME)/bin/javah");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence librariesContent(final Mapping m, final HostType host) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#PROTECTED REGION ID(libraries_");
    CharSequence _dirname = this._commonExtensions.dirname(host);
    _builder.append(_dirname);
    _builder.append(") ENABLED START#");
    _builder.newLineIfNotEmpty();
    {
      final Function1<Library, Boolean> _function = (Library l) -> {
        return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(l, host));
      };
      Iterable<Library> _filter = IterableExtensions.<Library>filter(m.getLibraries(), _function);
      for(final Library lib : _filter) {
        String _name = lib.getName();
        _builder.append(_name);
        _builder.append("_INCDIR= ");
        _builder.newLineIfNotEmpty();
        String _name_1 = lib.getName();
        _builder.append(_name_1);
        _builder.append("_LIBDIR=");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      boolean _isEmpty = m.getExternalLibraries().isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        _builder.append("#Location of external jnimap with shared library");
        _builder.newLine();
        {
          final Function1<ExternalLibrary, Mapping> _function_1 = (ExternalLibrary exLib) -> {
            return exLib.getMapping();
          };
          Set<Mapping> _set = IterableExtensions.<Mapping>toSet(ListExtensions.<ExternalLibrary, Mapping>map(m.getExternalLibraries(), _function_1));
          for(final Mapping exMapping : _set) {
            String _name_2 = exMapping.getName();
            _builder.append(_name_2);
            _builder.append("_LOCATION=");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.newLine();
        _builder.append("#Libraries used by other bindigs --- use the same includes");
        _builder.newLine();
        {
          final Function1<ExternalLibrary, Boolean> _function_2 = (ExternalLibrary l) -> {
            return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(l, host));
          };
          Iterable<ExternalLibrary> _filter_1 = IterableExtensions.<ExternalLibrary>filter(m.getExternalLibraries(), _function_2);
          for(final ExternalLibrary exLib : _filter_1) {
            String _name_3 = this.getName(exLib);
            _builder.append(_name_3);
            _builder.append("_INCDIR=");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("#PROTECTED REGION END#");
    _builder.newLine();
    _builder.newLine();
    {
      boolean _isEmpty_1 = m.getExternalLibraries().isEmpty();
      boolean _not_1 = (!_isEmpty_1);
      if (_not_1) {
        {
          final Function1<ExternalLibrary, Boolean> _function_3 = (ExternalLibrary l) -> {
            return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(l, host));
          };
          Iterable<ExternalLibrary> _filter_2 = IterableExtensions.<ExternalLibrary>filter(m.getExternalLibraries(), _function_3);
          for(final ExternalLibrary exLib_1 : _filter_2) {
            String _name_4 = this.getName(exLib_1);
            _builder.append(_name_4);
            _builder.append("_LIBDIR=${");
            String _name_5 = exLib_1.getMapping().getName();
            _builder.append(_name_5);
            _builder.append("_LOCATION}/lib/");
            String _libDir = this._commonExtensions.libDir(exLib_1.getMapping(), host);
            _builder.append(_libDir);
            _builder.append("/");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }
  
  public String makefileContent(final Mapping m, final HostType host) {
    String _nativeInterfaceHeader = this._commonExtensions.nativeInterfaceHeader(m);
    String _plus = (_nativeInterfaceHeader + " ");
    String _nativeInterfaceHeaderForUserModule = this._commonExtensions.nativeInterfaceHeaderForUserModule(m);
    final String headerNames = (_plus + _nativeInterfaceHeaderForUserModule);
    final String DLLpath = this._commonExtensions.DLLpath(host, m);
    final String libName = this._commonExtensions.libPath(host, m);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("include ");
    CharSequence _dirname = this._commonExtensions.dirname(host);
    _builder.append(_dirname);
    _builder.append("/user.mk");
    _builder.newLineIfNotEmpty();
    _builder.append("include ");
    CharSequence _dirname_1 = this._commonExtensions.dirname(host);
    _builder.append(_dirname_1);
    _builder.append("/libraries.mk");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    String _makefilePreamble = this.makefilePreamble(host);
    _builder.append(_makefilePreamble);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("OBJS=");
    final Function1<ClassMapping, Boolean> _function = (ClassMapping cm) -> {
      boolean _isEmpty = this._commonExtensions.getMethods(cm, m).isEmpty();
      return Boolean.valueOf((!_isEmpty));
    };
    final Function1<ClassMapping, CharSequence> _function_1 = (ClassMapping cm) -> {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _name = m.getName();
      _builder_1.append(_name);
      _builder_1.append("_");
      String _name_1 = cm.getName();
      _builder_1.append(_name_1);
      _builder_1.append("_native.o");
      return _builder_1.toString();
    };
    String _join = IterableExtensions.<ClassMapping>join(IterableExtensions.<ClassMapping>filter(m.getClassMapping(), _function), "", "\\\n\t", "\\\n", _function_1);
    _builder.append(_join);
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _name = m.getName();
    _builder.append(_name, "\t");
    _builder.append("_common_native.o \\");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _name_1 = m.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_UserModules.o");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("#Extra objects to be linked");
    _builder.newLine();
    _builder.append("EXTRA_OBJS=");
    final Function1<UserModule, CharSequence> _function_2 = (UserModule um) -> {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _name_2 = m.getName();
      _builder_1.append(_name_2);
      _builder_1.append("User_");
      String _name_3 = um.getName();
      _builder_1.append(_name_3);
      _builder_1.append(".o");
      return _builder_1.toString();
    };
    String _join_1 = IterableExtensions.<UserModule>join(m.getUserModules(), " \\\n\t", _function_2);
    _builder.append(_join_1);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("CFLAGS = -m$(ARCH) -fPIC -O3 -Wall -Wextra -Wno-unused-label -Wno-unused-parameter \\");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("$(JAVA_INC) -I./ -I../ $(EXTRA_CFLAGS) $(EXTRA_INCDIR) \\");
    _builder.newLine();
    _builder.append("\t\t");
    final Function1<ExternalLibrary, Boolean> _function_3 = (ExternalLibrary l) -> {
      return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(l, host));
    };
    final Function1<ExternalLibrary, CharSequence> _function_4 = (ExternalLibrary exLib) -> {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("-I$(");
      String _name_2 = this.getName(exLib);
      _builder_1.append(_name_2);
      _builder_1.append("_INCDIR)");
      return _builder_1.toString();
    };
    String _join_2 = IterableExtensions.<ExternalLibrary>join(IterableExtensions.<ExternalLibrary>filter(m.getExternalLibraries(), _function_3), " ", _function_4);
    _builder.append(_join_2, "\t\t");
    _builder.append(" \\");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    final Function1<Library, Boolean> _function_5 = (Library l) -> {
      return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(l, host));
    };
    final Function1<Library, CharSequence> _function_6 = (Library lib) -> {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("-I$(");
      String _name_2 = lib.getName();
      _builder_1.append(_name_2);
      _builder_1.append("_INCDIR)");
      return _builder_1.toString();
    };
    String _join_3 = IterableExtensions.<Library>join(IterableExtensions.<Library>filter(m.getLibraries(), _function_5), " ", _function_6);
    _builder.append(_join_3, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("LIBS = \\");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("-Wl,-rpath,./");
    _builder.append(DLLpath, "\t");
    _builder.append(" \\");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    final Function1<ExternalLibrary, Boolean> _function_7 = (ExternalLibrary l) -> {
      return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(l, host));
    };
    final Function1<ExternalLibrary, CharSequence> _function_8 = (ExternalLibrary exLib) -> {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("./");
      _builder_1.append(DLLpath);
      _builder_1.append("/");
      String _filename = this._commonExtensions.getFilename(exLib, host).getFilename();
      _builder_1.append(_filename);
      return _builder_1.toString();
    };
    String _join_4 = IterableExtensions.<ExternalLibrary>join(IterableExtensions.<ExternalLibrary>filter(m.getExternalLibraries(), _function_7), " \\\n", _function_8);
    _builder.append(_join_4, "\t");
    _builder.append(" \\");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    final Function1<Library, Boolean> _function_9 = (Library l) -> {
      return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(l, host));
    };
    final Function1<Library, CharSequence> _function_10 = (Library lib) -> {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("./");
      _builder_1.append(DLLpath);
      _builder_1.append("/");
      String _filename = this._commonExtensions.getFilename(lib, host).getFilename();
      _builder_1.append(_filename);
      return _builder_1.toString();
    };
    String _join_5 = IterableExtensions.<Library>join(IterableExtensions.<Library>filter(m.getLibraries(), _function_9), " \\\n", _function_10);
    _builder.append(_join_5, "\t");
    _builder.append(" \\");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("$(EXTRA_LIBS)");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Path to the ");
    String _name_2 = m.getName();
    _builder.append(_name_2);
    _builder.append("Native class file");
    _builder.newLineIfNotEmpty();
    _builder.append("CLASSPATH=../../bin/");
    _builder.newLine();
    _builder.newLine();
    _builder.append("V=@");
    _builder.newLine();
    _builder.append("Q=");
    _builder.newLine();
    _builder.newLine();
    _builder.append("all: ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@echo usage :");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@echo  - make lib : compile ");
    String _libname = this._commonExtensions.libname(m, host);
    _builder.append(_libname, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("@echo  - make clean");
    _builder.newLine();
    _builder.newLine();
    _builder.append("###############################");
    _builder.newLine();
    _builder.append("#\t\t\tRules");
    _builder.newLine();
    _builder.append("###############################");
    _builder.newLine();
    _builder.newLine();
    _builder.append("clean: ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("rm -rf ");
    _builder.append(libName, "\t");
    _builder.append(" *.o ../*.o ");
    _builder.append(headerNames, "\t");
    _builder.append(" ../../lib/");
    String _libDir = this._commonExtensions.libDir(m, host);
    _builder.append(_libDir, "\t");
    _builder.append("/*");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("lib: ");
    _builder.append(libName);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("#Final file to be created.");
    _builder.newLine();
    _builder.append(libName);
    _builder.append(" : $(OBJS) $(EXTRA_OBJS) ");
    {
      if (((m.getLibraries().size() > 0) || (m.getExternalLibraries().size() > 0))) {
        _builder.append("libs_");
        String _string = host.toString();
        _builder.append(_string);
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("$(V)mkdir -p ../../lib/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("$(Q)@echo \"  [LINK] $@\"");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("$(V)$(CC) $(CFLAGS) -o ");
    _builder.append(libName, "\t");
    _builder.append(" -shared $(OBJS) $(EXTRA_OBJS) $(LIBS)");
    _builder.newLineIfNotEmpty();
    {
      if (((m.getLibraries().size() > 0) || (m.getExternalLibraries().size() > 0))) {
        _builder.append("\t");
        _builder.append("$(V)cd ../../lib/ && \\");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("mkdir -p ");
        String _libDir_1 = this._commonExtensions.libDir(m, host);
        _builder.append(_libDir_1, "\t");
        _builder.append("/ && \\");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("mv ../native/build/");
        _builder.append(DLLpath, "\t");
        _builder.append("/* ./");
        String _libDir_2 = this._commonExtensions.libDir(m, host);
        _builder.append(_libDir_2, "\t");
        _builder.append("/");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("$(V)$(MAKE) -f ");
        CharSequence _dirname_2 = this._commonExtensions.dirname(host);
        _builder.append(_dirname_2, "\t");
        _builder.append("/Makefile fixInstallNames");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("$(V)rm -rf ./");
        _builder.append(DLLpath, "\t");
        _builder.append("/");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    {
      if (((m.getLibraries().size() > 0) || (m.getExternalLibraries().size() > 0))) {
        _builder.append("libs_");
        String _string_1 = host.toString();
        _builder.append(_string_1);
        _builder.append(":");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("$(V)mkdir -p ./");
        _builder.append(DLLpath, "\t");
        _builder.newLineIfNotEmpty();
        {
          final Function1<ExternalLibrary, Boolean> _function_11 = (ExternalLibrary e) -> {
            return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(e, host));
          };
          Iterable<ExternalLibrary> _filter = IterableExtensions.<ExternalLibrary>filter(m.getExternalLibraries(), _function_11);
          for(final ExternalLibrary exLib : _filter) {
            _builder.append("\t");
            _builder.append("$(V)cp $(");
            String _name_3 = this.getName(exLib);
            _builder.append(_name_3, "\t");
            _builder.append("_LIBDIR)/");
            String _filename = this._commonExtensions.getFilename(exLib.getLibrary(), host).getFilename();
            _builder.append(_filename, "\t");
            _builder.append(" ");
            _builder.append(DLLpath, "\t");
            _builder.append("/");
            String _filename_1 = this._commonExtensions.getFilename(exLib.getLibrary(), host).getFilename();
            _builder.append(_filename_1, "\t");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          final Function1<Library, Boolean> _function_12 = (Library e) -> {
            return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(e, host));
          };
          Iterable<Library> _filter_1 = IterableExtensions.<Library>filter(m.getLibraries(), _function_12);
          for(final Library lib : _filter_1) {
            _builder.append("\t");
            _builder.append("$(V)cp $(");
            String _name_4 = lib.getName();
            _builder.append(_name_4, "\t");
            _builder.append("_LIBDIR)/");
            String _filename_2 = this._commonExtensions.getFilename(lib, host).getFilename();
            _builder.append(_filename_2, "\t");
            _builder.append(" ");
            _builder.append(DLLpath, "\t");
            _builder.append("/");
            String _filename_3 = this._commonExtensions.getFilename(lib, host).getFilename();
            _builder.append(_filename_3, "\t");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.newLine();
    String _nativeInterfaceHeaderForUserModule_1 = this._commonExtensions.nativeInterfaceHeaderForUserModule(m);
    _builder.append(_nativeInterfaceHeaderForUserModule_1);
    _builder.append(" : ");
    String _nativeInterfaceHeader_1 = this._commonExtensions.nativeInterfaceHeader(m);
    _builder.append(_nativeInterfaceHeader_1);
    _builder.newLineIfNotEmpty();
    String _nativeInterfaceHeader_2 = this._commonExtensions.nativeInterfaceHeader(m);
    _builder.append(_nativeInterfaceHeader_2);
    _builder.append(" : $(CLASSPATH)/");
    String _packagePath = this._commonExtensions.packagePath(m);
    String _plus_1 = (_packagePath + "/");
    String _ID = this._commonExtensions.ID(m);
    String _plus_2 = (_plus_1 + _ID);
    String _plus_3 = (_plus_2 + "Native.class");
    _builder.append(_plus_3);
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("$(Q)@echo \"  [JAVAH] $@\"");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("$(V)$(JAVA_H) -classpath $(CLASSPATH)/ ");
    String _packageName = this._commonExtensions.packageName(m);
    String _plus_4 = (_packageName + ".");
    String _ID_1 = this._commonExtensions.ID(m);
    String _plus_5 = (_plus_4 + _ID_1);
    String _plus_6 = (_plus_5 + "Native");
    _builder.append(_plus_6, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("@echo \"#define GECOS_PTRSIZE $(GECOS_PTRSIZE)\" >> $@");
    _builder.newLine();
    {
      EList<ClassMapping> _classMapping = m.getClassMapping();
      for(final ClassMapping cm : _classMapping) {
        String _name_5 = m.getName();
        _builder.append(_name_5);
        _builder.append("_");
        String _name_6 = cm.getName();
        _builder.append(_name_6);
        _builder.append("_native.o : ../");
        String _name_7 = m.getName();
        _builder.append(_name_7);
        _builder.append("_");
        String _name_8 = cm.getName();
        _builder.append(_name_8);
        _builder.append("_native.c ");
        _builder.append(headerNames);
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("$(Q)@echo \"  [CC] $@\"");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("$(V)$(CC) $(CFLAGS) -c -o $@ ../");
        String _name_9 = m.getName();
        _builder.append(_name_9, "\t");
        _builder.append("_");
        String _name_10 = cm.getName();
        _builder.append(_name_10, "\t");
        _builder.append("_native.c");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.newLine();
      }
    }
    _builder.newLine();
    String _name_11 = m.getName();
    _builder.append(_name_11);
    _builder.append("_common_native.o : ../");
    String _name_12 = m.getName();
    _builder.append(_name_12);
    _builder.append("_common_native.c ");
    _builder.append(headerNames);
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("$(Q)@echo \"  [CC] $@\"");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("$(V)$(CC) $(CFLAGS) -c -o $@ ../");
    String _name_13 = m.getName();
    _builder.append(_name_13, "\t");
    _builder.append("_common_native.c");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    String _name_14 = m.getName();
    _builder.append(_name_14);
    _builder.append("_UserModules.o : ../");
    String _name_15 = m.getName();
    _builder.append(_name_15);
    _builder.append("_UserModules.c ");
    _builder.append(headerNames);
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("$(Q)@echo \"  [CC] $@\"");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("$(V)$(CC) $(CFLAGS) -c -o $@ ../");
    String _name_16 = m.getName();
    _builder.append(_name_16, "\t");
    _builder.append("_UserModules.c");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    {
      EList<UserModule> _userModules = m.getUserModules();
      for(final UserModule um : _userModules) {
        _builder.append("#PROTECTED REGION ID(make_");
        String _moduleID = this.moduleID(um, host);
        _builder.append(_moduleID);
        _builder.append(") DISABLED START#");
        _builder.newLineIfNotEmpty();
        String _identifier = this._commonExtensions.identifier(um);
        _builder.append(_identifier);
        _builder.append(".o : ../");
        String _identifier_1 = this._commonExtensions.identifier(um);
        _builder.append(_identifier_1);
        _builder.append(".c ");
        _builder.append(headerNames);
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("$(Q)@echo \"  [CC] $@\"");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("$(V)$(CC) $(CFLAGS) -c -o $@ ../");
        String _identifier_2 = this._commonExtensions.identifier(um);
        _builder.append(_identifier_2, "\t");
        _builder.append(".c");
        _builder.newLineIfNotEmpty();
        _builder.append("#PROTECTED REGION END#");
        _builder.newLine();
        _builder.newLine();
      }
    }
    _builder.append("fixInstallNames:");
    _builder.newLine();
    _builder.append("\t");
    CharSequence _fixInstallNames = this.fixInstallNames(host, m);
    _builder.append(_fixInstallNames, "\t");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }
  
  public String moduleID(final UserModule um, final HostType host) {
    String _ID = this._commonExtensions.ID(host);
    String _plus = (_ID + "_");
    String _identifier = this._commonExtensions.identifier(um);
    return (_plus + _identifier);
  }
  
  public String makefilePreamble(final HostType host) {
    boolean _isMac = this._commonExtensions.isMac(host);
    if (_isMac) {
      return this.makefilePreambleMacOSX(host);
    } else {
      boolean _isLinux = this._commonExtensions.isLinux(host);
      if (_isLinux) {
        return this.makefilePreambleLinux(host);
      } else {
        StringConcatenation _builder = new StringConcatenation();
        return _builder.toString();
      }
    }
  }
  
  public String makefilePreambleMacOSX(final HostType host) {
    StringConcatenation _builder = new StringConcatenation();
    String ptrSize = _builder.toString();
    int arch = 0;
    boolean _equals = Objects.equal(host, HostType.MACOSX32);
    if (_equals) {
      ptrSize = "int";
      arch = 32;
    } else {
      ptrSize = "long";
      arch = 64;
    }
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("GECOS_PTRSIZE = (");
    _builder_1.append(ptrSize);
    _builder_1.append(")");
    _builder_1.newLineIfNotEmpty();
    _builder_1.append("ARCH = ");
    _builder_1.append(arch);
    _builder_1.newLineIfNotEmpty();
    _builder_1.append("JAVA_INC=-I$(JAVA_HOME)/include $(JAVA_EXTRA_INC)");
    _builder_1.newLine();
    return _builder_1.toString();
  }
  
  public CharSequence fixInstallNameMac(final Mapping m, final String libfilename, final String targetLibFilePath) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("@A=`otool -L ");
    _builder.append(targetLibFilePath);
    _builder.append(" | grep ");
    _builder.append(libfilename);
    _builder.append(" | colrm 1 8 | cut -d\" \" -f1`; \\");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if [ \"$$A\" != \"\" ]; then \\");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("(if [ \"$$A\" != \"@rpath/");
    _builder.append(libfilename, "\t\t");
    _builder.append("\" ]; then \\");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("install_name_tool -change \"$$A\" \"@rpath/");
    _builder.append(libfilename, "\t\t\t");
    _builder.append("\" \"");
    _builder.append(targetLibFilePath, "\t\t\t");
    _builder.append("\"; \\");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("fi); fi");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence fixInstallNameLinux(final HostType host, final Mapping m, final String targetLibFilePath) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("patchelf --set-rpath ./");
    String _DLLpath = this._commonExtensions.DLLpath(host, m);
    _builder.append(_DLLpath);
    _builder.append("/ ");
    _builder.append(targetLibFilePath);
    return _builder;
  }
  
  public CharSequence fixInstallNames(final HostType host, final Mapping m) {
    CharSequence _xifexpression = null;
    boolean _isMac = this._commonExtensions.isMac(host);
    if (_isMac) {
      _xifexpression = this.fixInstallNamesMac(host, m);
    } else {
      CharSequence _xifexpression_1 = null;
      boolean _isLinux = this._commonExtensions.isLinux(host);
      if (_isLinux) {
        _xifexpression_1 = this.fixInstallNamesLinux(host, m);
      } else {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("@echo \"  [RPATH FIX] not supported for this OS\"");
        _xifexpression_1 = _builder;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  public String fixInstallNamesMac(final HostType host, final Mapping m) {
    final String libName = this._commonExtensions.libPath(host, m);
    StringConcatenation _builder = new StringConcatenation();
    {
      final Function1<ExternalLibrary, Boolean> _function = (ExternalLibrary e) -> {
        return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(e, host));
      };
      Iterable<ExternalLibrary> _filter = IterableExtensions.<ExternalLibrary>filter(m.getExternalLibraries(), _function);
      for(final ExternalLibrary exLib : _filter) {
        _builder.append("@echo \"  [RPATH FIX] ");
        String _name = this.getName(exLib);
        _builder.append(_name);
        _builder.append("\"");
        _builder.newLineIfNotEmpty();
        CharSequence _fixInstallNameMac = this.fixInstallNameMac(m, this._commonExtensions.getFilename(exLib, host).getFilename(), libName);
        _builder.append(_fixInstallNameMac);
        _builder.newLineIfNotEmpty();
        {
          final Function1<Library, Boolean> _function_1 = (Library l) -> {
            return Boolean.valueOf((this._commonExtensions.hasLibraryFilename(l, host) && (!Objects.equal(l.getName(), this.getName(exLib)))));
          };
          Iterable<Library> _filter_1 = IterableExtensions.<Library>filter(m.getLibraries(), _function_1);
          for(final Library otherlib : _filter_1) {
            String _filename = this._commonExtensions.getFilename(exLib, host).getFilename();
            String _libDir = this._commonExtensions.libDir(m, host);
            String _plus = ("../../lib/" + _libDir);
            String _plus_1 = (_plus + "/");
            String _filename_1 = this._commonExtensions.getFilename(otherlib, host).getFilename();
            String _plus_2 = (_plus_1 + _filename_1);
            CharSequence _fixInstallNameMac_1 = this.fixInstallNameMac(m, _filename, _plus_2);
            _builder.append(_fixInstallNameMac_1);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    {
      final Function1<Library, Boolean> _function_2 = (Library e) -> {
        return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(e, host));
      };
      Iterable<Library> _filter_2 = IterableExtensions.<Library>filter(m.getLibraries(), _function_2);
      for(final Library lib : _filter_2) {
        _builder.append("@echo \"  [RPATH FIX] ");
        String _name_1 = lib.getName();
        _builder.append(_name_1);
        _builder.append("\"");
        _builder.newLineIfNotEmpty();
        CharSequence _fixInstallNameMac_2 = this.fixInstallNameMac(m, this._commonExtensions.getFilename(lib, host).getFilename(), libName);
        _builder.append(_fixInstallNameMac_2);
        _builder.newLineIfNotEmpty();
        {
          final Function1<Library, Boolean> _function_3 = (Library l) -> {
            return Boolean.valueOf((this._commonExtensions.hasLibraryFilename(l, host) && (!Objects.equal(l.getName(), lib.getName()))));
          };
          Iterable<Library> _filter_3 = IterableExtensions.<Library>filter(m.getLibraries(), _function_3);
          for(final Library otherlib_1 : _filter_3) {
            String _filename_2 = this._commonExtensions.getFilename(lib, host).getFilename();
            String _libDir_1 = this._commonExtensions.libDir(m, host);
            String _plus_3 = ("../../lib/" + _libDir_1);
            String _plus_4 = (_plus_3 + "/");
            String _filename_3 = this._commonExtensions.getFilename(otherlib_1, host).getFilename();
            String _plus_5 = (_plus_4 + _filename_3);
            CharSequence _fixInstallNameMac_3 = this.fixInstallNameMac(m, _filename_2, _plus_5);
            _builder.append(_fixInstallNameMac_3);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder.toString();
  }
  
  public String fixInstallNamesLinux(final HostType host, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    {
      final Function1<ExternalLibrary, Boolean> _function = (ExternalLibrary e) -> {
        return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(e, host));
      };
      Iterable<ExternalLibrary> _filter = IterableExtensions.<ExternalLibrary>filter(m.getExternalLibraries(), _function);
      for(final ExternalLibrary exLib : _filter) {
        _builder.append("@echo \"  [RPATH FIX] ");
        String _name = this.getName(exLib);
        _builder.append(_name);
        _builder.append("\"");
        _builder.newLineIfNotEmpty();
        String _libDir = this._commonExtensions.libDir(m, host);
        String _plus = ("../../lib/" + _libDir);
        String _plus_1 = (_plus + "/");
        String _filename = this._commonExtensions.getFilename(exLib, host).getFilename();
        String _plus_2 = (_plus_1 + _filename);
        CharSequence _fixInstallNameLinux = this.fixInstallNameLinux(host, m, _plus_2);
        _builder.append(_fixInstallNameLinux);
        _builder.newLineIfNotEmpty();
      }
    }
    {
      final Function1<Library, Boolean> _function_1 = (Library e) -> {
        return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(e, host));
      };
      Iterable<Library> _filter_1 = IterableExtensions.<Library>filter(m.getLibraries(), _function_1);
      for(final Library lib : _filter_1) {
        _builder.append("@echo \"  [RPATH FIX] ");
        String _name_1 = lib.getName();
        _builder.append(_name_1);
        _builder.append("\"");
        _builder.newLineIfNotEmpty();
        String _libDir_1 = this._commonExtensions.libDir(m, host);
        String _plus_3 = ("../../lib/" + _libDir_1);
        String _plus_4 = (_plus_3 + "/");
        String _filename_1 = this._commonExtensions.getFilename(lib, host).getFilename();
        String _plus_5 = (_plus_4 + _filename_1);
        CharSequence _fixInstallNameLinux_1 = this.fixInstallNameLinux(host, m, _plus_5);
        _builder.append(_fixInstallNameLinux_1);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }
  
  public String makefilePreambleLinux(final HostType host) {
    StringConcatenation _builder = new StringConcatenation();
    String ptrSize = _builder.toString();
    int arch = 0;
    boolean _equals = Objects.equal(host, HostType.LINUX32);
    if (_equals) {
      ptrSize = "int";
      arch = 32;
    } else {
      ptrSize = "long";
      arch = 64;
    }
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("GECOS_PTRSIZE = (");
    _builder_1.append(ptrSize);
    _builder_1.append(")");
    _builder_1.newLineIfNotEmpty();
    _builder_1.append("ARCH = ");
    _builder_1.append(arch);
    _builder_1.newLineIfNotEmpty();
    _builder_1.append("JAVA_INC= -I$(JAVA_HOME)/include $(JAVA_EXTRA_INC)");
    _builder_1.newLine();
    return _builder_1.toString();
  }
  
  private String getName(final ExternalLibrary exLib) {
    return exLib.getLibrary().getName();
  }
}
