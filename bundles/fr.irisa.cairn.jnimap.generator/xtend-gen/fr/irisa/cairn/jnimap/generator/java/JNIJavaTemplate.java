package fr.irisa.cairn.jnimap.generator.java;

import fr.irisa.cairn.jniMap.CMethod;
import fr.irisa.cairn.jniMap.ClassMapping;
import fr.irisa.cairn.jniMap.EnumToClassMapping;
import fr.irisa.cairn.jniMap.ExternalLibrary;
import fr.irisa.cairn.jniMap.FieldDef;
import fr.irisa.cairn.jniMap.HostType;
import fr.irisa.cairn.jniMap.Library;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.Method;
import fr.irisa.cairn.jniMap.MethodGroup;
import fr.irisa.cairn.jniMap.ParamDef;
import fr.irisa.cairn.jniMap.StructToClassMapping;
import fr.irisa.cairn.jniMap.Type;
import fr.irisa.cairn.jniMap.UserModule;
import fr.irisa.cairn.jnimap.generator.CommonExtensions;
import fr.irisa.cairn.jnimap.generator.TypeExtensions;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

/**
 * This class is responsible for the interface to C and library loading.
 * The main portion is nativeInterfaceContent, which defines XXXNative.java
 * 
 * The Java classes corresponding to C objects (wrappers) are generated
 * by JNIJavaWrapperTemplate.
 */
@SuppressWarnings("all")
public class JNIJavaTemplate {
  @Extension
  private CommonExtensions _commonExtensions = new CommonExtensions();
  
  @Extension
  private TypeExtensions _typeExtensions = new TypeExtensions();
  
  public void compile(final Mapping m, final IFileSystemAccess fsa) {
    fsa.generateFile(this.abstractPlatformFilepath(m), this.abstractPlatformContent(m));
    EList<HostType> _hosts = m.getHosts();
    for (final HostType host : _hosts) {
      fsa.generateFile(this.concretePlatformFilepath(m, host), this.concretePlatformContent(m, host));
    }
    fsa.generateFile(this.nativeInterfaceFilepath(m), this.nativeInterfaceContent(m));
    fsa.generateFile(this.exceptionFilepath(m), this.exceptionContent(m));
  }
  
  public String abstractPlatformFilepath(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _javaRootPath = this._commonExtensions.javaRootPath();
    _builder.append(_javaRootPath);
    String _packagePath = this._commonExtensions.packagePath(m);
    _builder.append(_packagePath);
    _builder.append("/platform/JNI");
    String _ID = this._commonExtensions.ID(m);
    _builder.append(_ID);
    _builder.append("AbstractPlatform.java");
    return _builder.toString();
  }
  
  public String concretePlatformFilepath(final Mapping m, final HostType host) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _javaRootPath = this._commonExtensions.javaRootPath();
    _builder.append(_javaRootPath);
    String _packagePath = this._commonExtensions.packagePath(m);
    _builder.append(_packagePath);
    _builder.append("/platform/JNI");
    String _ID = this._commonExtensions.ID(m);
    _builder.append(_ID);
    String _ID_1 = this._commonExtensions.ID(host);
    _builder.append(_ID_1);
    _builder.append(".java");
    return _builder.toString();
  }
  
  public String nativeInterfaceFilepath(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _javaRootPath = this._commonExtensions.javaRootPath();
    _builder.append(_javaRootPath);
    String _packagePath = this._commonExtensions.packagePath(m);
    _builder.append(_packagePath);
    _builder.append("/");
    String _ID = this._commonExtensions.ID(m);
    _builder.append(_ID);
    _builder.append("Native.java");
    return _builder.toString();
  }
  
  public String nativeTestFilepath(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _javaRootPath = this._commonExtensions.javaRootPath();
    _builder.append(_javaRootPath);
    String _packagePath = this._commonExtensions.packagePath(m);
    _builder.append(_packagePath);
    _builder.append("/");
    String _ID = this._commonExtensions.ID(m);
    _builder.append(_ID);
    _builder.append("NativeTest.java");
    return _builder.toString();
  }
  
  public String exceptionFilepath(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _javaRootPath = this._commonExtensions.javaRootPath();
    _builder.append(_javaRootPath);
    String _packagePath = this._commonExtensions.packagePath(m);
    _builder.append(_packagePath);
    _builder.append("/");
    String _ID = this._commonExtensions.ID(m);
    _builder.append(_ID);
    _builder.append("Exception.java");
    return _builder.toString();
  }
  
  public CharSequence abstractPlatformContent(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _packageName = this._commonExtensions.packageName(m);
    _builder.append(_packageName);
    _builder.append(".platform;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("import java.io.File;");
    _builder.newLine();
    _builder.append("import java.io.FileOutputStream;");
    _builder.newLine();
    _builder.append("import java.io.InputStream;");
    _builder.newLine();
    _builder.append("import java.net.URL;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("public abstract class JNI");
    String _ID = this._commonExtensions.ID(m);
    _builder.append(_ID);
    _builder.append("AbstractPlatform {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("abstract public void loadPlatformLibraries();");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("abstract protected String getDllPath();");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("protected File copyLibToTemp(String location, String libName) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("String path = File.separator+location+libName;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("try {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("File dir = new File(getDllPath()).getAbsoluteFile();");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("dir.mkdirs();");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("dir.deleteOnExit();");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("URL resource = JNI");
    String _ID_1 = this._commonExtensions.ID(m);
    _builder.append(_ID_1, "\t\t\t");
    _builder.append("AbstractPlatform.class.getResource(path);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("InputStream inputStream = resource.openStream();");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("// Copy resource to filesystem in a temp folder with a unique name");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("String dllPath = dir.getAbsolutePath()+File.separator+libName;");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("File temporaryDll = new File(dllPath);");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("//skip if the library is already loaded");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if (temporaryDll.exists()) return temporaryDll;");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("FileOutputStream outputStream = new FileOutputStream(temporaryDll);");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("byte[] array = new byte[8192];");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("int read = 0;");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("while ( (read = inputStream.read(array)) > 0)");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("outputStream.write(array, 0, read);");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("outputStream.close();  ");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("// Delete on exit the dll");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("temporaryDll.deleteOnExit();");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return temporaryDll;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("} catch (Exception e) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("throw new RuntimeException(\"Could not copy library : \" + path,e);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence concretePlatformContent(final Mapping m, final HostType host) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _packageName = this._commonExtensions.packageName(m);
    _builder.append(_packageName);
    _builder.append(".platform;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("import java.io.File;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("public class JNI");
    String _ID = this._commonExtensions.ID(m);
    _builder.append(_ID);
    String _ID_1 = this._commonExtensions.ID(host);
    _builder.append(_ID_1);
    _builder.append(" extends JNI");
    String _ID_2 = this._commonExtensions.ID(m);
    _builder.append(_ID_2);
    _builder.append("AbstractPlatform {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("protected String getDllPath() {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return new File(\".\")+File.separator+\"./");
    String _DLLpath = this._commonExtensions.DLLpath(host, m);
    _builder.append(_DLLpath, "\t\t");
    _builder.append("\";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public void loadPlatformLibraries() {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("// Get input stream from jar resource");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("String lib = \"");
    String _libName = this._commonExtensions.libName(host, m);
    _builder.append(_libName, "\t\t");
    _builder.append("\";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("try {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("//Copy other dynamic libraries from jar to temporary location");
    _builder.newLine();
    {
      final Function1<Library, Boolean> _function = (Library l) -> {
        return Boolean.valueOf(this._commonExtensions.hasLibraryFilename(l, host));
      };
      Iterable<Library> _filter = IterableExtensions.<Library>filter(m.getLibraries(), _function);
      for(final Library lib : _filter) {
        _builder.append("\t\t\t");
        _builder.append("//Copy ");
        String _name = lib.getName();
        _builder.append(_name, "\t\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t\t");
        _builder.append("copyLibToTemp(\"");
        String _name_1 = m.getName();
        String _plus = (_name_1 + "_");
        String _string = host.toString();
        String _plus_1 = (_plus + _string);
        _builder.append(_plus_1, "\t\t\t");
        _builder.append("\" + File.separator, \"");
        String _filename = this._commonExtensions.getFilename(lib, host).getFilename();
        _builder.append(_filename, "\t\t\t");
        _builder.append("\");");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t\t");
    _builder.append("//Copy the binding object file from jar to temporary location");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("File JNIDLL = copyLibToTemp(\"\", lib);");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("// Finally, load the dll");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("System.load(JNIDLL.getAbsolutePath());");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("} catch (Exception e) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("throw new RuntimeException(\"Problem during native library loading ");
    String _ID_3 = this._commonExtensions.ID(m);
    _builder.append(_ID_3, "\t\t\t");
    _builder.append(":\",e);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence checkHostTypeSupport(final Mapping m, final HostType t) {
    CharSequence _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      String _firstUpper = StringExtensions.toFirstUpper(t.getLiteral());
      _builder.append(_firstUpper);
      final String platformName = _builder.toString();
      CharSequence _xifexpression = null;
      boolean _contains = m.getHosts().contains(t);
      if (_contains) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("loader = new JNI");
        String _ID = this._commonExtensions.ID(m);
        _builder_1.append(_ID);
        _builder_1.append(platformName);
        _builder_1.append("();");
        _xifexpression = _builder_1;
      } else {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("throw new RuntimeException(\"");
        _builder_2.append(platformName);
        _builder_2.append(" is not supported.\");");
        _xifexpression = _builder_2;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public String nativeInterfaceContent(final Mapping m) {
    final Set<Mapping> extMaps = new LinkedHashSet<Mapping>();
    final Consumer<ExternalLibrary> _function = (ExternalLibrary e) -> {
      extMaps.add(e.getMapping());
    };
    m.getExternalLibraries().forEach(_function);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _packageName = this._commonExtensions.packageName(m);
    _builder.append(_packageName);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("import ");
    String _packageName_1 = this._commonExtensions.packageName(m);
    _builder.append(_packageName_1);
    _builder.append(".platform.*;");
    _builder.newLineIfNotEmpty();
    {
      for(final Mapping extMap : extMaps) {
        _builder.append("import ");
        String _packageName_2 = this._commonExtensions.packageName(extMap);
        _builder.append(_packageName_2);
        _builder.append(".");
        String _ID = this._commonExtensions.ID(extMap);
        _builder.append(_ID);
        _builder.append("Native;");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("public class ");
    String _ID_1 = this._commonExtensions.ID(m);
    _builder.append(_ID_1);
    _builder.append("Native {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private static final String arch = System.getProperty(\"sun.arch.data.model\");");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private static JNI");
    String _ID_2 = this._commonExtensions.ID(m);
    _builder.append(_ID_2, "\t");
    _builder.append("AbstractPlatform loader ;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private static boolean loaded = false;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public static void loadLibrary() {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("if (loaded) return;");
    _builder.newLine();
    {
      for(final Mapping extMap_1 : extMaps) {
        _builder.append("\t\t");
        String _ID_3 = this._commonExtensions.ID(extMap_1);
        _builder.append(_ID_3, "\t\t");
        _builder.append("Native.loadLibrary();");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append("String osName = System.getProperty(\"os.name\");");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("String arch   = System.getProperty(\"sun.arch.data.model\");");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("if (osName.equals(\"Linux\")) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if (arch.equals(\"32\")) {");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    CharSequence _checkHostTypeSupport = this.checkHostTypeSupport(m, HostType.LINUX32);
    _builder.append(_checkHostTypeSupport, "\t\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("} else {");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    CharSequence _checkHostTypeSupport_1 = this.checkHostTypeSupport(m, HostType.LINUX64);
    _builder.append(_checkHostTypeSupport_1, "\t\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("} else if (osName.equals(\"Windows XP\")) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    CharSequence _checkHostTypeSupport_2 = this.checkHostTypeSupport(m, HostType.CYGWIN32);
    _builder.append(_checkHostTypeSupport_2, "\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("} else if (osName.equals(\"Mac OS X\")) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if (arch.equals(\"32\")) {");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    CharSequence _checkHostTypeSupport_3 = this.checkHostTypeSupport(m, HostType.MACOSX32);
    _builder.append(_checkHostTypeSupport_3, "\t\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("} else {");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    CharSequence _checkHostTypeSupport_4 = this.checkHostTypeSupport(m, HostType.MACOSX64);
    _builder.append(_checkHostTypeSupport_4, "\t\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("} ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("} else  {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("throw new RuntimeException(\"Unsupported Operating system : \" + osName+\"_\"+arch);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("loader.loadPlatformLibraries();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("loaded = true;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("static {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("loadLibrary();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public static native long derefPointer(long ptr);");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*************************************************************");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*\t                     USER MODULES                        *");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*************************************************************/");
    _builder.newLine();
    _builder.append("\t ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("public static class UserModules {");
    _builder.newLine();
    {
      EList<UserModule> _userModules = m.getUserModules();
      for(final UserModule module : _userModules) {
        _builder.append("\t \t");
        CharSequence _javaUserModule = this.javaUserModule(module);
        _builder.append(_javaUserModule, "\t \t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*************************************************************");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*\t                        METHODS                          *");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*************************************************************/");
    _builder.newLine();
    {
      EList<MethodGroup> _methodGroups = m.getMethodGroups();
      for(final MethodGroup mg : _methodGroups) {
        {
          EList<Method> _methods = mg.getMethods();
          for(final Method method : _methods) {
            _builder.append("\t ");
            CharSequence _javaMethodDef = this.javaMethodDef(method);
            _builder.append(_javaMethodDef, "\t ");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("\t ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*************************************************************");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*\t                    CLASS MAPPING                        *");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*************************************************************/");
    _builder.newLine();
    {
      EList<ClassMapping> _classMapping = m.getClassMapping();
      for(final ClassMapping cm : _classMapping) {
        _builder.append("\t ");
        CharSequence _javaGetterSetter = this.javaGetterSetter(cm);
        _builder.append(_javaGetterSetter, "\t ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  public CharSequence nativeTestContent(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _lowerCase = m.getName().toLowerCase();
    _builder.append(_lowerCase);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("public class ");
    String _ID = this._commonExtensions.ID(m);
    _builder.append(_ID);
    _builder.append("NativeTest {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public static void main(String args[]) {");
    _builder.newLine();
    _builder.append("\t\t");
    String _ID_1 = this._commonExtensions.ID(m);
    _builder.append(_ID_1, "\t\t");
    _builder.append("Native.test();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("} ");
    _builder.newLine();
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence exceptionContent(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _packageName = this._commonExtensions.packageName(m);
    _builder.append(_packageName);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("public class ");
    String _ID = this._commonExtensions.ID(m);
    _builder.append(_ID);
    _builder.append("Exception extends Exception {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("private static final long serialVersionUID = 1L;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public ");
    String _ID_1 = this._commonExtensions.ID(m);
    _builder.append(_ID_1, "\t");
    _builder.append("Exception(String msg) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("super(msg);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("} ");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _javaGetterSetter(final ClassMapping cm) {
    CharSequence _xblockexpression = null;
    {
      Class<? extends ClassMapping> _class = cm.getClass();
      String _plus = ("ClassMapping " + _class);
      String _plus_1 = (_plus + " not supported in javaGetterSetter");
      System.err.println(_plus_1);
      StringConcatenation _builder = new StringConcatenation();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _javaGetterSetter(final EnumToClassMapping ecm) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _javaGetterSetter(final StructToClassMapping scm) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<FieldDef> _fields = scm.getStruct().getFields();
      for(final FieldDef field : _fields) {
        {
          Type _type = field.getType();
          boolean _tripleEquals = (_type == null);
          if (_tripleEquals) {
            String _name = field.getName();
            String _plus = ("[ERROR] Field " + _name);
            String _plus_1 = (_plus + " for ");
            String _name_1 = scm.getName();
            String _plus_2 = (_plus_1 + _name_1);
            String _plus_3 = (_plus_2 + " is null");
            System.err.println(_plus_3);
            _builder.newLineIfNotEmpty();
          } else {
            _builder.append("// getter for ");
            String _name_2 = scm.getName();
            _builder.append(_name_2);
            _builder.append(".");
            String _name_3 = field.getName();
            _builder.append(_name_3);
            _builder.newLineIfNotEmpty();
            _builder.append("public static native ");
            String _jNIJavaName = this._typeExtensions.getJNIJavaName(field.getType());
            _builder.append(_jNIJavaName);
            _builder.append(" ");
            String _name_4 = scm.getStruct().getName();
            _builder.append(_name_4);
            _builder.append("_get_");
            String _name_5 = field.getName();
            _builder.append(_name_5);
            _builder.append("(long ptr);");
            _builder.newLineIfNotEmpty();
            _builder.append("// tester for ");
            String _name_6 = scm.getName();
            _builder.append(_name_6);
            _builder.append(".");
            String _name_7 = field.getName();
            _builder.append(_name_7);
            _builder.newLineIfNotEmpty();
            _builder.append("public static native int ");
            String _name_8 = scm.getStruct().getName();
            _builder.append(_name_8);
            _builder.append("_test_");
            String _name_9 = field.getName();
            _builder.append(_name_9);
            _builder.append("(long ptr);");
            _builder.newLineIfNotEmpty();
            _builder.append("// setter for ");
            String _name_10 = scm.getName();
            _builder.append(_name_10);
            _builder.append(".");
            String _name_11 = field.getName();
            _builder.append(_name_11);
            _builder.newLineIfNotEmpty();
            _builder.append("public static native void ");
            String _name_12 = scm.getStruct().getName();
            _builder.append(_name_12);
            _builder.append("_set_");
            String _name_13 = field.getName();
            _builder.append(_name_13);
            _builder.append("(long ptr, ");
            String _jNIJavaName_1 = this._typeExtensions.getJNIJavaName(field.getType());
            _builder.append(_jNIJavaName_1);
            _builder.append(" value);");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }
  
  public CharSequence javaUserModule(final UserModule um) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// UserModule : ");
    String _name = um.getName();
    _builder.append(_name);
    _builder.newLineIfNotEmpty();
    {
      EList<CMethod> _methods = um.getMethods();
      for(final CMethod method : _methods) {
        CharSequence _javaMethodDef = this.javaMethodDef(method);
        _builder.append(_javaMethodDef);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  public CharSequence javaMethodDef(final Method method) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public static native ");
    String _jNIJavaName = this._typeExtensions.getJNIJavaName(method.getRes());
    _builder.append(_jNIJavaName);
    _builder.append(" ");
    String _name = method.getName();
    _builder.append(_name);
    _builder.append("(");
    final Function1<ParamDef, CharSequence> _function = (ParamDef p) -> {
      return this.javaParamDef(p);
    };
    String _join = IterableExtensions.<ParamDef>join(this._commonExtensions.getActiveParams(method), ", ", _function);
    _builder.append(_join);
    _builder.append(");");
    return _builder;
  }
  
  public CharSequence javaMethodDef(final CMethod method) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public static native ");
    String _jNIJavaName = this._typeExtensions.getJNIJavaName(method.getRes());
    _builder.append(_jNIJavaName);
    _builder.append(" ");
    String _name = method.getName();
    _builder.append(_name);
    _builder.append("(");
    final Function1<ParamDef, CharSequence> _function = (ParamDef p) -> {
      return this.javaParamDef(p);
    };
    String _join = IterableExtensions.<ParamDef>join(method.getParams(), ", ", _function);
    _builder.append(_join);
    _builder.append(");");
    return _builder;
  }
  
  public CharSequence javaParamDef(final ParamDef pd) {
    StringConcatenation _builder = new StringConcatenation();
    String _jNIJavaName = this._typeExtensions.getJNIJavaName(pd.getType());
    _builder.append(_jNIJavaName);
    _builder.append(" ");
    String _name = pd.getName();
    _builder.append(_name);
    return _builder;
  }
  
  public CharSequence javaGetterSetter(final ClassMapping ecm) {
    if (ecm instanceof EnumToClassMapping) {
      return _javaGetterSetter((EnumToClassMapping)ecm);
    } else if (ecm instanceof StructToClassMapping) {
      return _javaGetterSetter((StructToClassMapping)ecm);
    } else if (ecm != null) {
      return _javaGetterSetter(ecm);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(ecm).toString());
    }
  }
}
