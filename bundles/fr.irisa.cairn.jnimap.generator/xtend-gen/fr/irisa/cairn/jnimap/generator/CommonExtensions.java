package fr.irisa.cairn.jnimap.generator;

import com.google.common.base.Objects;
import fr.irisa.cairn.jniMap.ClassMapping;
import fr.irisa.cairn.jniMap.ExternalLibrary;
import fr.irisa.cairn.jniMap.FieldDef;
import fr.irisa.cairn.jniMap.HostType;
import fr.irisa.cairn.jniMap.Library;
import fr.irisa.cairn.jniMap.LibraryFileName;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.Method;
import fr.irisa.cairn.jniMap.MethodGroup;
import fr.irisa.cairn.jniMap.ParamDef;
import fr.irisa.cairn.jniMap.PtrType;
import fr.irisa.cairn.jniMap.UserModule;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class CommonExtensions {
  public CharSequence javaRootPath() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("./src-gen/");
    return _builder;
  }
  
  public CharSequence cRootPath() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("./native/");
    return _builder;
  }
  
  public String ID(final Mapping m) {
    return StringExtensions.toFirstUpper(m.getName());
  }
  
  public String ID(final HostType host) {
    return StringExtensions.toFirstUpper(host.toString());
  }
  
  public String ID(final FieldDef fd) {
    return StringExtensions.toFirstUpper(fd.getName());
  }
  
  public EObject eRootContainer(final EObject e) {
    EObject _xifexpression = null;
    EObject _eContainer = e.eContainer();
    boolean _tripleEquals = (_eContainer == null);
    if (_tripleEquals) {
      _xifexpression = e;
    } else {
      _xifexpression = this.eRootContainer(e.eContainer());
    }
    return _xifexpression;
  }
  
  public Mapping getRoot(final Object obj) {
    EObject _eRootContainer = this.eRootContainer(((EObject) obj));
    return ((Mapping) _eRootContainer);
  }
  
  public Mapping getEnclosingMapping(final Object obj) {
    EObject _eContainer = ((EObject) obj).eContainer();
    if ((_eContainer instanceof Mapping)) {
      EObject _eContainer_1 = ((EObject) obj).eContainer();
      return ((Mapping) _eContainer_1);
    }
    return this.getEnclosingMapping(((EObject) obj).eContainer());
  }
  
  public ClassMapping getMappedClass(final Method m) {
    EObject _eContainer = m.eContainer();
    return ((MethodGroup) _eContainer).getClass_();
  }
  
  public String packageName(final Mapping m) {
    if (((m.getPackage() != null) && (m.getPackage().length() > 0))) {
      return m.getPackage();
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("fr.irisa.cairn.jnimap.");
    String _lowerCase = m.getName().toLowerCase();
    _builder.append(_lowerCase);
    return _builder.toString();
  }
  
  public String packagePath(final Mapping m) {
    return this.packageName(m).replaceAll("\\.", "/");
  }
  
  public String packagePrefix(final Mapping m) {
    return this.packageName(m).replaceAll("\\.", "_");
  }
  
  public String DLLpath(final HostType host, final Mapping m) {
    String _string = host.toString();
    return (".jnimap.temp." + _string);
  }
  
  public String nativeInterfaceHeader(final Mapping m) {
    String _packagePrefix = this.packagePrefix(m);
    String _plus = (_packagePrefix + "_");
    String _name = m.getName();
    String _plus_1 = (_plus + _name);
    return (_plus_1 + "Native.h");
  }
  
  public String nativeInterfaceHeaderForUserModule(final Mapping m) {
    String _packagePrefix = this.packagePrefix(m);
    String _plus = (_packagePrefix + "_");
    String _name = m.getName();
    String _plus_1 = (_plus + _name);
    return (_plus_1 + "Native_UserModules.h");
  }
  
  public String libName(final HostType host, final Mapping m) {
    String _string = host.toString();
    String _plus = (_string + "_libjni");
    String _lowerCase = m.getName().toLowerCase();
    String _plus_1 = (_plus + _lowerCase);
    return (_plus_1 + ".so");
  }
  
  public String libPath(final HostType host, final Mapping m) {
    String _libName = this.libName(host, m);
    return ("../../lib/" + _libName);
  }
  
  public String identifier(final UserModule um) {
    String _name = this.getRoot(um).getName();
    String _plus = (_name + "User_");
    String _name_1 = um.getName();
    return (_plus + _name_1);
  }
  
  public final CharSequence dirname(final HostType host) {
    return this.ID(host);
  }
  
  public final boolean isMac(final HostType host) {
    return (Objects.equal(host, HostType.MACOSX64) || Objects.equal(host, HostType.MACOSX32));
  }
  
  public final boolean isLinux(final HostType host) {
    return (Objects.equal(host, HostType.LINUX64) || Objects.equal(host, HostType.LINUX32));
  }
  
  public final boolean hasLibraryFilename(final Library lib, final HostType host) {
    final Function1<LibraryFileName, Boolean> _function = (LibraryFileName e) -> {
      HostType _os = e.getOs();
      return Boolean.valueOf(Objects.equal(_os, host));
    };
    boolean _isEmpty = IterableExtensions.isEmpty(IterableExtensions.<LibraryFileName>filter(lib.getFilenames(), _function));
    return (!_isEmpty);
  }
  
  public final LibraryFileName getFilename(final Library lib, final HostType host) {
    final Function1<LibraryFileName, Boolean> _function = (LibraryFileName e) -> {
      HostType _os = e.getOs();
      return Boolean.valueOf(Objects.equal(_os, host));
    };
    return IterableExtensions.<LibraryFileName>head(IterableExtensions.<LibraryFileName>filter(lib.getFilenames(), _function));
  }
  
  public final boolean hasLibraryFilename(final ExternalLibrary exLib, final HostType host) {
    return this.hasLibraryFilename(exLib.getLibrary(), host);
  }
  
  public final LibraryFileName getFilename(final ExternalLibrary exLib, final HostType host) {
    return this.getFilename(exLib.getLibrary(), host);
  }
  
  public final String libname(final Mapping m, final HostType host) {
    StringConcatenation _builder = new StringConcatenation();
    String _string = host.toString();
    _builder.append(_string);
    _builder.append("_libjni");
    String _lowerCase = m.getName().toLowerCase();
    _builder.append(_lowerCase);
    _builder.append(".so");
    return _builder.toString();
  }
  
  public final String libDir(final Mapping m, final HostType host) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = m.getName();
    _builder.append(_name);
    _builder.append("_");
    String _string = host.toString();
    _builder.append(_string);
    return _builder.toString();
  }
  
  public MethodGroup getMethodGroup(final ClassMapping cm, final Mapping m) {
    final Function1<MethodGroup, Boolean> _function = (MethodGroup mg) -> {
      ClassMapping _class_ = mg.getClass_();
      return Boolean.valueOf(Objects.equal(_class_, cm));
    };
    return IterableExtensions.<MethodGroup>findFirst(m.getMethodGroups(), _function);
  }
  
  public EList<Method> getMethods(final ClassMapping cm, final Mapping m) {
    EList<Method> _xblockexpression = null;
    {
      final Function1<MethodGroup, Boolean> _function = (MethodGroup mg) -> {
        ClassMapping _class_ = mg.getClass_();
        return Boolean.valueOf(Objects.equal(_class_, cm));
      };
      final MethodGroup mg = IterableExtensions.<MethodGroup>findFirst(m.getMethodGroups(), _function);
      EList<Method> _xifexpression = null;
      if ((mg == null)) {
        return new BasicEList<Method>();
      } else {
        _xifexpression = mg.getMethods();
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public Iterable<ParamDef> getActiveParams(final Method method) {
    final Function1<ParamDef, Boolean> _function = (ParamDef p) -> {
      return Boolean.valueOf((!((p.getType() instanceof PtrType) && ((PtrType) p.getType()).isIgnored())));
    };
    return IterableExtensions.<ParamDef>filter(method.getParams(), _function);
  }
  
  public String visibility(final Method method) {
    boolean _isPrivate = method.isPrivate();
    if (_isPrivate) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("private");
      return _builder.toString();
    }
    boolean _isProtected = method.isProtected();
    if (_isProtected) {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("protected");
      return _builder_1.toString();
    }
    StringConcatenation _builder_2 = new StringConcatenation();
    _builder_2.append("public");
    return _builder_2.toString();
  }
}
