package fr.irisa.cairn.jnimap.generator.c;

import fr.irisa.cairn.jniMap.BuiltInType;
import fr.irisa.cairn.jniMap.ConstType;
import fr.irisa.cairn.jniMap.EnumType;
import fr.irisa.cairn.jniMap.IntegerType;
import fr.irisa.cairn.jniMap.PtrType;
import fr.irisa.cairn.jniMap.StringType;
import fr.irisa.cairn.jniMap.StructType;
import fr.irisa.cairn.jniMap.Type;
import fr.irisa.cairn.jniMap.TypeAlias;
import fr.irisa.cairn.jnimap.generator.TypeExtensions;
import java.util.Arrays;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("all")
public class JNICParamExtensions {
  @Extension
  private TypeExtensions _typeExtensions = new TypeExtensions();
  
  protected CharSequence _localVarParam(final Type t, final String varName, final String methodName) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _localVarParam(final StringType t, final String varName, final String methodName) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("char* ");
    _builder.append(varName);
    _builder.append("_c;");
    _builder.newLineIfNotEmpty();
    _builder.append(varName);
    _builder.append("_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, ");
    _builder.append(varName);
    _builder.append(", NULL);");
    _builder.newLineIfNotEmpty();
    _builder.append("if (");
    _builder.append(varName);
    _builder.append("_c==NULL) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("throwException(env, \"GetStringUTFChars Failed  in ");
    _builder.append(methodName, "\t");
    _builder.append(" for parameter ");
    _builder.append(varName, "\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("goto error;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _localVarParam(final IntegerType t, final String varName, final String methodName) {
    StringConcatenation _builder = new StringConcatenation();
    String _standardCName = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName);
    _builder.append(" ");
    _builder.append(varName);
    _builder.append("_c = (");
    String _standardCName_1 = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName_1);
    _builder.append(") ");
    _builder.append(varName);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  protected CharSequence _localVarParam(final BuiltInType t, final String varName, final String methodName) {
    StringConcatenation _builder = new StringConcatenation();
    String _standardCName = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName);
    _builder.append(" ");
    _builder.append(varName);
    _builder.append("_c = (");
    String _standardCName_1 = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName_1);
    _builder.append(") ");
    _builder.append(varName);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  protected CharSequence _localVarParam(final ConstType t, final String varName, final String methodName) {
    return this.localVarParam(t.getBase(), varName, methodName);
  }
  
  protected CharSequence _localVarParam(final TypeAlias t, final String varName, final String methodName) {
    StringConcatenation _builder = new StringConcatenation();
    String _standardCName = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName);
    _builder.append(" ");
    _builder.append(varName);
    _builder.append("_c = (");
    String _standardCName_1 = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName_1);
    _builder.append(") ");
    _builder.append(varName);
    _builder.append("; ");
    _builder.newLineIfNotEmpty();
    _builder.append("if(((void*)");
    _builder.append(varName);
    _builder.append("_c)==NULL) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("throwException(env, \"Null pointer in ");
    _builder.append(methodName, "\t");
    _builder.append(" for parameter ");
    _builder.append(varName, "\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("goto error;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _localVarParam(final StructType t, final String varName, final String methodName) {
    StringConcatenation _builder = new StringConcatenation();
    String _standardCName = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName);
    _builder.append(" ");
    _builder.append(varName);
    _builder.append("_c = (");
    String _standardCName_1 = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName_1);
    _builder.append(") ");
    _builder.append(varName);
    _builder.append("; ");
    _builder.newLineIfNotEmpty();
    _builder.append("if(((void*)");
    _builder.append(varName);
    _builder.append("_c)==NULL) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("throwException(env, \"Null pointer in ");
    _builder.append(methodName, "\t");
    _builder.append(" for parameter ");
    _builder.append(varName, "\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("goto error;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _localVarParam(final PtrType t, final String varName, final String methodName) {
    boolean _isIgnored = t.isIgnored();
    if (_isIgnored) {
      StringConcatenation _builder = new StringConcatenation();
      return _builder;
    }
    boolean _isPrimitivePtrType = this._typeExtensions.isPrimitivePtrType(t);
    if (_isPrimitivePtrType) {
      final String typeName = this._typeExtensions.getStandardCName(t.getBase());
      final String clsName = (varName + "_cls");
      final String fidName = (varName + "_fid");
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("jclass ");
      _builder_1.append(clsName);
      _builder_1.append(" = (*env)->GetObjectClass(env, ");
      _builder_1.append(varName);
      _builder_1.append("); ");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("jfieldID ");
      _builder_1.append(fidName);
      _builder_1.append(" = (*env)->GetFieldID(env, ");
      _builder_1.append(clsName);
      _builder_1.append(", \"value\", \"");
      String _jNIFieldSignature = this._typeExtensions.getJNIFieldSignature(t);
      _builder_1.append(_jNIFieldSignature);
      _builder_1.append("\");");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append(typeName);
      _builder_1.append(" ");
      _builder_1.append(varName);
      _builder_1.append("_c = (");
      _builder_1.append(typeName);
      _builder_1.append(") (*env)->");
      String _jNIFieldGetterName = this._typeExtensions.getJNIFieldGetterName(t);
      _builder_1.append(_jNIFieldGetterName);
      _builder_1.append("(env, ");
      _builder_1.append(varName);
      _builder_1.append(", ");
      _builder_1.append(fidName);
      _builder_1.append(");");
      _builder_1.newLineIfNotEmpty();
      return _builder_1;
    }
    StringConcatenation _builder_2 = new StringConcatenation();
    String _standardCName = this._typeExtensions.getStandardCName(t);
    _builder_2.append(_standardCName);
    _builder_2.append(" ");
    _builder_2.append(varName);
    _builder_2.append("_c = (");
    String _standardCName_1 = this._typeExtensions.getStandardCName(t);
    _builder_2.append(_standardCName_1);
    _builder_2.append(") GECOS_PTRSIZE ");
    _builder_2.append(varName);
    _builder_2.append("; ");
    _builder_2.newLineIfNotEmpty();
    _builder_2.append("if(((void*)");
    _builder_2.append(varName);
    _builder_2.append("_c)==NULL) {");
    _builder_2.newLineIfNotEmpty();
    _builder_2.append("\t");
    _builder_2.append("throwException(env, \"Null pointer in ");
    _builder_2.append(methodName, "\t");
    _builder_2.append(" for parameter ");
    _builder_2.append(varName, "\t");
    _builder_2.append("\");");
    _builder_2.newLineIfNotEmpty();
    _builder_2.append("\t");
    _builder_2.append("goto error;");
    _builder_2.newLine();
    _builder_2.append("}");
    _builder_2.newLine();
    return _builder_2;
  }
  
  protected CharSequence _localVarParam(final EnumType t, final String varName, final String methodName) {
    StringConcatenation _builder = new StringConcatenation();
    String _standardCName = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName);
    _builder.append(" ");
    _builder.append(varName);
    _builder.append("_c = (");
    String _standardCName_1 = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName_1);
    _builder.append(") ");
    _builder.append(varName);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  protected CharSequence _localVarParamCleanup(final Type t, final String varName, final String methodName) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _localVarParamCleanup(final StringType t, final String varName, final String methodName) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("(*env)->ReleaseStringUTFChars(env, ");
    _builder.append(varName);
    _builder.append(", ");
    _builder.append(varName);
    _builder.append("_c);");
    return _builder;
  }
  
  protected CharSequence _localVarParamCleanup(final PtrType t, final String varName, final String methodName) {
    boolean _isPrimitivePtrType = this._typeExtensions.isPrimitivePtrType(t);
    if (_isPrimitivePtrType) {
      final String fidName = (varName + "_fid");
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("(*env)->");
      String _jNIFieldSetterName = this._typeExtensions.getJNIFieldSetterName(t);
      _builder.append(_jNIFieldSetterName);
      _builder.append("(env, ");
      _builder.append(varName);
      _builder.append(", ");
      _builder.append(fidName);
      _builder.append(", ");
      _builder.append(varName);
      _builder.append("_c);");
      _builder.newLineIfNotEmpty();
      return _builder.toString();
    }
    StringConcatenation _builder_1 = new StringConcatenation();
    return _builder_1.toString();
  }
  
  public CharSequence localVarParam(final Type t, final String varName, final String methodName) {
    if (t instanceof IntegerType) {
      return _localVarParam((IntegerType)t, varName, methodName);
    } else if (t instanceof StringType) {
      return _localVarParam((StringType)t, varName, methodName);
    } else if (t instanceof BuiltInType) {
      return _localVarParam((BuiltInType)t, varName, methodName);
    } else if (t instanceof EnumType) {
      return _localVarParam((EnumType)t, varName, methodName);
    } else if (t instanceof StructType) {
      return _localVarParam((StructType)t, varName, methodName);
    } else if (t instanceof TypeAlias) {
      return _localVarParam((TypeAlias)t, varName, methodName);
    } else if (t instanceof ConstType) {
      return _localVarParam((ConstType)t, varName, methodName);
    } else if (t instanceof PtrType) {
      return _localVarParam((PtrType)t, varName, methodName);
    } else if (t != null) {
      return _localVarParam(t, varName, methodName);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(t, varName, methodName).toString());
    }
  }
  
  public CharSequence localVarParamCleanup(final Type t, final String varName, final String methodName) {
    if (t instanceof StringType) {
      return _localVarParamCleanup((StringType)t, varName, methodName);
    } else if (t instanceof PtrType) {
      return _localVarParamCleanup((PtrType)t, varName, methodName);
    } else if (t != null) {
      return _localVarParamCleanup(t, varName, methodName);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(t, varName, methodName).toString());
    }
  }
}
