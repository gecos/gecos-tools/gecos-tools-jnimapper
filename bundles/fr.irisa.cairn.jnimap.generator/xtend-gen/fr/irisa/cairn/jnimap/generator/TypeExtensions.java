package fr.irisa.cairn.jnimap.generator;

import com.google.common.base.Objects;
import com.google.common.collect.Iterators;
import fr.irisa.cairn.jniMap.AliasToClassMapping;
import fr.irisa.cairn.jniMap.BuiltInType;
import fr.irisa.cairn.jniMap.ClassMapping;
import fr.irisa.cairn.jniMap.ConstType;
import fr.irisa.cairn.jniMap.EnumToClassMapping;
import fr.irisa.cairn.jniMap.EnumType;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.Method;
import fr.irisa.cairn.jniMap.MethodGroup;
import fr.irisa.cairn.jniMap.ParamDef;
import fr.irisa.cairn.jniMap.PtrType;
import fr.irisa.cairn.jniMap.StringType;
import fr.irisa.cairn.jniMap.StructToClassMapping;
import fr.irisa.cairn.jniMap.StructType;
import fr.irisa.cairn.jniMap.Type;
import fr.irisa.cairn.jniMap.TypeAlias;
import fr.irisa.cairn.jniMap.TypeDeclaration;
import fr.irisa.cairn.jniMap.UnmappedClass;
import fr.irisa.cairn.jniMap.VoidType;
import fr.irisa.cairn.jnimap.generator.CommonExtensions;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javahelpers.JNICNameBuilder;
import javahelpers.JNIFieldSignatureBuilder;
import javahelpers.JNIJavaNameBuilder;
import javahelpers.StandardCNameBuilder;
import javahelpers.StandardJavaNameBuilder;
import javahelpers.StringUtils;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class TypeExtensions {
  @Extension
  private CommonExtensions _commonExtensions = new CommonExtensions();
  
  public String getPtrType() {
    return "long";
  }
  
  public boolean isVoid(final Method method) {
    Type _res = method.getRes();
    return (_res instanceof VoidType);
  }
  
  public boolean isStringType(final Type type) {
    return ((type instanceof StringType) || ((type instanceof ConstType) && this.isStringType(((ConstType) type).getBase())));
  }
  
  public Iterator<PtrType> getAllPrimitivePtrTypes(final Mapping m) {
    final Function1<PtrType, Boolean> _function = (PtrType e) -> {
      return Boolean.valueOf(this.isPrimitivePtrType(((PtrType) e)));
    };
    return IteratorExtensions.<PtrType>filter(Iterators.<PtrType>filter(m.eAllContents(), PtrType.class), _function);
  }
  
  public boolean isPrimitivePtrType(final Type type) {
    if ((type instanceof PtrType)) {
      return this.isPrimitivePtrType(((PtrType) type));
    }
    return false;
  }
  
  public boolean isPrimitivePtrType(final PtrType type) {
    return ((((type.getBase() instanceof BuiltInType) && (!(type.getBase() instanceof VoidType))) && (!(type.getBase() instanceof StringType))) && (((Object[])Conversions.unwrapArray(type.getIndir(), Object.class)).length == 1));
  }
  
  public List<StructToClassMapping> getAllSubClasses(final StructToClassMapping s2c) {
    final Function1<StructToClassMapping, Boolean> _function = (StructToClassMapping e) -> {
      ClassMapping _super = e.getSuper();
      return Boolean.valueOf(Objects.equal(_super, s2c));
    };
    final Function1<StructToClassMapping, String> _function_1 = (StructToClassMapping e) -> {
      return e.getName();
    };
    return IterableExtensions.<StructToClassMapping, String>sortBy(IteratorExtensions.<StructToClassMapping>toList(IteratorExtensions.<StructToClassMapping>filter(Iterators.<StructToClassMapping>filter(this._commonExtensions.getRoot(s2c).eAllContents(), StructToClassMapping.class), _function)), _function_1);
  }
  
  public boolean isBuiltInTypePointer(final Type type) {
    return ((type instanceof PtrType) && (((PtrType) type).getBase() instanceof BuiltInType));
  }
  
  public ClassMapping getMappingFor(final Type type) {
    if ((type instanceof ConstType)) {
      return this.getMappingFor(((ConstType) type).getBase());
    }
    if ((type instanceof PtrType)) {
      return this.getMappingFor(((PtrType) type).getBase());
    }
    if ((type instanceof TypeAlias)) {
      EObject _eContainer = ((TypeAlias) type).getAlias().eContainer();
      return ((AliasToClassMapping) _eContainer);
    }
    if ((type instanceof EnumType)) {
      EObject _eContainer_1 = ((EnumType) type).getName().eContainer();
      return ((EnumToClassMapping) _eContainer_1);
    }
    if ((type instanceof StructType)) {
      EObject _eContainer_2 = ((StructType) type).getName().eContainer();
      return ((StructToClassMapping) _eContainer_2);
    }
    return null;
  }
  
  public ClassMapping getMappingFor(final TypeDeclaration decl) {
    EObject _eContainer = decl.eContainer();
    if ((_eContainer instanceof ClassMapping)) {
      decl.eContainer();
    }
    return null;
  }
  
  public boolean multipleIndirection(final Type type) {
    boolean _isPointer = this.isPointer(type);
    if (_isPointer) {
      return this.isPointer(((ConstType) type).getBase());
    }
    return ((type instanceof PtrType) || (type instanceof TypeAlias));
  }
  
  public boolean isPointer(final Type type) {
    if ((type instanceof ConstType)) {
      return this.isPointer(((ConstType) type).getBase());
    }
    return ((type instanceof PtrType) || (type instanceof TypeAlias));
  }
  
  public boolean isAlias(final Type type) {
    return (type instanceof TypeAlias);
  }
  
  /**
   * String dot2underscore(List<String> params) : JAVA
   * test.StringUtils.dot2underscore(java.util.List<String>)
   * ;
   */
  public String pointerPrefix(final PtrType param) {
    return StringUtils.pointerPrefix(param);
  }
  
  public String getReturnTypeStandardJavaName(final Method m) {
    return this.standardJavaName(m.getRes(), this._commonExtensions.getMappedClass(m));
  }
  
  public String getMethodName(final Method m) {
    String _newname = m.getNewname();
    boolean _tripleNotEquals = (_newname != null);
    if (_tripleNotEquals) {
      return m.getNewname();
    } else {
      final Function1<String, CharSequence> _function = (String e) -> {
        return StringExtensions.toFirstUpper(e);
      };
      return StringExtensions.toFirstLower(IterableExtensions.<String>join(((Iterable<String>)Conversions.doWrapArray(m.getName().split("_"))), "", _function));
    }
  }
  
  public String standardJavaName(final Type type) {
    String _xblockexpression = null;
    {
      final String typeName = StandardJavaNameBuilder.nameFor(type);
      String _xifexpression = null;
      if (((this.getMappingFor(type) != null) && (!Objects.equal(this._commonExtensions.getEnclosingMapping(this.getMappingFor(type)), this._commonExtensions.getRoot(type))))) {
        StringConcatenation _builder = new StringConcatenation();
        String _packageName = this._commonExtensions.packageName(this._commonExtensions.getEnclosingMapping(this.getMappingFor(type)));
        _builder.append(_packageName);
        _builder.append(".");
        _builder.append(typeName);
        _xifexpression = _builder.toString();
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(typeName);
        _xifexpression = _builder_1.toString();
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  protected String _standardJavaName(final Type type, final ClassMapping cm) {
    return this.standardJavaName(type);
  }
  
  protected String _standardJavaName(final Type type, final UnmappedClass cm) {
    String _xblockexpression = null;
    {
      final String typeName = StandardJavaNameBuilder.nameFor(type);
      String _xifexpression = null;
      if (((!Objects.equal(this._commonExtensions.getEnclosingMapping(this.getMappingFor(type)), this._commonExtensions.getRoot(type))) && (!Objects.equal(this.getMappingFor(type), cm.getSuper())))) {
        StringConcatenation _builder = new StringConcatenation();
        String _packageName = this._commonExtensions.packageName(this._commonExtensions.getEnclosingMapping(this.getMappingFor(type)));
        _builder.append(_packageName);
        _builder.append(".");
        _builder.append(typeName);
        _xifexpression = _builder.toString();
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(typeName);
        _xifexpression = _builder_1.toString();
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public String getJNIJavaName(final Type type) {
    boolean _isPrimitivePtrType = this.isPrimitivePtrType(type);
    if (_isPrimitivePtrType) {
      return this.standardJavaName(type);
    } else {
      return JNIJavaNameBuilder.nameFor(type);
    }
  }
  
  public String getStandardCName(final Type type) {
    return StandardCNameBuilder.nameFor(type);
  }
  
  public String getJNICName(final Type type) {
    boolean _isPrimitivePtrType = this.isPrimitivePtrType(type);
    if (_isPrimitivePtrType) {
      return "jobject";
    } else {
      return JNICNameBuilder.nameFor(type);
    }
  }
  
  public String getJNIFieldGetterName(final PtrType type) {
    boolean _isPrimitivePtrType = this.isPrimitivePtrType(type);
    if (_isPrimitivePtrType) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Get");
      String _firstUpper = StringExtensions.toFirstUpper(this.standardJavaName(type.getBase()));
      _builder.append(_firstUpper);
      _builder.append("Field");
      return _builder.toString();
    } else {
      System.err.println("[Error] not a primitive pointer type");
      return "[Error] not a primitive pointer type";
    }
  }
  
  public String getJNIFieldSetterName(final PtrType type) {
    boolean _isPrimitivePtrType = this.isPrimitivePtrType(type);
    if (_isPrimitivePtrType) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Set");
      String _firstUpper = StringExtensions.toFirstUpper(this.standardJavaName(type.getBase()));
      _builder.append(_firstUpper);
      _builder.append("Field");
      return _builder.toString();
    } else {
      System.err.println("[Error] not a primitive pointer type");
      return "[Error] not a primitive pointer type";
    }
  }
  
  public String getJNIFieldSignature(final PtrType type) {
    return JNIFieldSignatureBuilder.nameFor(type.getBase());
  }
  
  public CharSequence javaParam(final ParamDef pd, final ClassMapping cm) {
    StringConcatenation _builder = new StringConcatenation();
    String _standardJavaName = this.standardJavaName(pd.getType(), cm);
    _builder.append(_standardJavaName);
    _builder.append(" ");
    String _name = pd.getName();
    _builder.append(_name);
    return _builder;
  }
  
  public Iterable<Method> constructors(final MethodGroup mg) {
    if ((mg == null)) {
      return new BasicEList<Method>();
    } else {
      final Function1<Method, Boolean> _function = (Method c) -> {
        return Boolean.valueOf((c.isConstructor() && Objects.equal(this.getMappingFor(c.getRes()), mg.getClass_())));
      };
      return IterableExtensions.<Method>filter(mg.getMethods(), _function);
    }
  }
  
  public Iterable<Method> destructors(final MethodGroup mg) {
    if ((mg == null)) {
      return new BasicEList<Method>();
    } else {
      final Function1<Method, Boolean> _function = (Method c) -> {
        return Boolean.valueOf(c.isDestructor());
      };
      return IterableExtensions.<Method>filter(mg.getMethods(), _function);
    }
  }
  
  public Iterable<Method> staticMethods(final MethodGroup mg) {
    if ((mg == null)) {
      return new BasicEList<Method>();
    } else {
      final Function1<Method, Boolean> _function = (Method e) -> {
        return Boolean.valueOf(((((!e.isDestructor()) && (!e.isConstructor())) && (!e.isInstanceof())) && e.isStatic()));
      };
      return IterableExtensions.<Method>filter(mg.getMethods(), _function);
    }
  }
  
  public Iterable<Method> memberMethod(final MethodGroup mg) {
    if ((mg == null)) {
      return new BasicEList<Method>();
    }
    ClassMapping _class_ = mg.getClass_();
    if ((_class_ instanceof StructToClassMapping)) {
      ClassMapping _class__1 = mg.getClass_();
      final StructToClassMapping cm = ((StructToClassMapping) _class__1);
      final Function1<Method, Boolean> _function = (Method e) -> {
        return Boolean.valueOf(((((((!e.isDestructor()) && (!e.isConstructor())) && (!e.isInstanceof())) && (!e.isStatic())) && (e.getParams().size() > 0)) && (Objects.equal(this.getMappingFor(e.getParams().get(e.getArg()).getType()), cm) || Objects.equal(this.getMappingFor(e.getParams().get(e.getArg()).getType()), cm.getSuper()))));
      };
      return IterableExtensions.<Method>filter(mg.getMethods(), _function);
    } else {
      final ClassMapping cm_1 = mg.getClass_();
      final Function1<Method, Boolean> _function_1 = (Method e) -> {
        return Boolean.valueOf(((((((!e.isDestructor()) && (!e.isConstructor())) && (!e.isInstanceof())) && (!e.isStatic())) && (e.getParams().size() > 0)) && Objects.equal(this.getMappingFor(e.getParams().get(e.getArg()).getType()), cm_1)));
      };
      return IterableExtensions.<Method>filter(mg.getMethods(), _function_1);
    }
  }
  
  public String standardJavaName(final Type type, final ClassMapping cm) {
    if (cm instanceof UnmappedClass) {
      return _standardJavaName(type, (UnmappedClass)cm);
    } else if (cm != null) {
      return _standardJavaName(type, cm);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type, cm).toString());
    }
  }
}
