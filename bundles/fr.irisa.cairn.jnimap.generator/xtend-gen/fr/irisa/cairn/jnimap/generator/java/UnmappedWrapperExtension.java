package fr.irisa.cairn.jnimap.generator.java;

import com.google.common.base.Objects;
import fr.irisa.cairn.jniMap.ClassMapping;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.UnmappedClass;
import fr.irisa.cairn.jnimap.generator.CommonExtensions;
import fr.irisa.cairn.jnimap.generator.java.AbstractWrapper;
import fr.irisa.cairn.jnimap.generator.java.JNIJavaUtilities;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("all")
public class UnmappedWrapperExtension extends AbstractWrapper {
  @Extension
  private CommonExtensions _commonExtensions = new CommonExtensions();
  
  @Extension
  private JNIJavaUtilities _jNIJavaUtilities = new JNIJavaUtilities();
  
  @Override
  public CharSequence nativePointerManagement(final ClassMapping cm, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _isInterface = this._jNIJavaUtilities.isInterface(cm);
      boolean _not = (!_isInterface);
      if (_not) {
        _builder.append("/* @generated */");
        _builder.newLine();
        _builder.append("protected ");
        CharSequence _javaClassName = this._jNIJavaUtilities.javaClassName(cm);
        _builder.append(_javaClassName);
        _builder.append("(long ptr) {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("/*** PROTECTED REGION ID(");
        String _name = cm.getName();
        _builder.append(_name, "\t");
        _builder.append("_Constructor) DISABLED START ***/");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("super(ptr);");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("/*** PROTECTED REGION END ***/");
        _builder.newLine();
        _builder.append("}");
        _builder.newLine();
      }
    }
    return _builder;
  }
  
  @Override
  public CharSequence importSuperClass(final ClassMapping cm, final Mapping m) {
    CharSequence _xblockexpression = null;
    {
      final ClassMapping sClass = ((UnmappedClass) cm).getSuper();
      CharSequence _xifexpression = null;
      if (((sClass != null) && (!Objects.equal(this._commonExtensions.getEnclosingMapping(sClass), m)))) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("import ");
        String _packageName = this._commonExtensions.packageName(this._commonExtensions.getEnclosingMapping(sClass));
        _builder.append(_packageName);
        _builder.append(".");
        CharSequence _javaClassName = this._jNIJavaUtilities.javaClassName(sClass);
        _builder.append(_javaClassName);
        _builder.append(";");
        _xifexpression = _builder;
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _xifexpression = _builder_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  @Override
  public CharSequence memberMethodBodies(final ClassMapping cm, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
}
