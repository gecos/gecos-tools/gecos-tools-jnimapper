package fr.irisa.cairn.jnimap.generator.java;

import fr.irisa.cairn.jniMap.BuiltInType;
import fr.irisa.cairn.jniMap.ClassMapping;
import fr.irisa.cairn.jniMap.EnumType;
import fr.irisa.cairn.jniMap.FieldDef;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.Method;
import fr.irisa.cairn.jniMap.MethodGroup;
import fr.irisa.cairn.jniMap.PtrType;
import fr.irisa.cairn.jniMap.StructDeclaration;
import fr.irisa.cairn.jniMap.StructToClassMapping;
import fr.irisa.cairn.jniMap.StructType;
import fr.irisa.cairn.jniMap.Type;
import fr.irisa.cairn.jniMap.TypeAlias;
import fr.irisa.cairn.jnimap.generator.CommonExtensions;
import fr.irisa.cairn.jnimap.generator.TypeExtensions;
import fr.irisa.cairn.jnimap.generator.java.AbstractWrapper;
import fr.irisa.cairn.jnimap.generator.java.JNIJavaUtilities;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class StructWrapperExtension extends AbstractWrapper {
  @Extension
  private CommonExtensions _commonExtensions = new CommonExtensions();
  
  @Extension
  private TypeExtensions _typeExtensions = new TypeExtensions();
  
  @Extension
  private JNIJavaUtilities _jNIJavaUtilities = new JNIJavaUtilities();
  
  @Override
  public CharSequence specializedBody(final ClassMapping cm_in, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    final StructToClassMapping cm = ((StructToClassMapping) cm_in);
    _builder.newLineIfNotEmpty();
    _builder.append("/*************************************** ");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*\t     Struct Field Accessors        * ");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("***************************************/");
    _builder.newLine();
    {
      EList<FieldDef> _fields = cm.getStruct().getFields();
      for(final FieldDef field : _fields) {
        CharSequence _genGetter = this.genGetter(field.getType(), cm.getStruct(), field, m);
        _builder.append(_genGetter);
        _builder.newLineIfNotEmpty();
        CharSequence _genSetter = this.genSetter(field.getType(), cm.getStruct(), field, m);
        _builder.append(_genSetter);
        _builder.newLineIfNotEmpty();
        CharSequence _genTester = this.genTester(field.getType(), cm.getStruct(), field, m);
        _builder.append(_genTester);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  @Override
  public CharSequence nativePointerManagement(final ClassMapping cm_in, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    final StructToClassMapping cm = ((StructToClassMapping) cm_in);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("/* @generated */");
    _builder.newLine();
    _builder.append("protected ");
    CharSequence _javaClassName = this._jNIJavaUtilities.javaClassName(cm);
    _builder.append(_javaClassName);
    _builder.append("(long ptr) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("/*** PROTECTED REGION ID(");
    String _name = cm.getName();
    _builder.append(_name, "\t");
    _builder.append("_Constructor) DISABLED START ***/");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("super(ptr);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*** PROTECTED REGION END ***/");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    {
      ClassMapping _super = cm.getSuper();
      boolean _tripleEquals = (_super == null);
      if (_tripleEquals) {
        _builder.newLine();
        _builder.append("/* @generated */");
        _builder.newLine();
        _builder.append("static ");
        CharSequence _javaClassName_1 = this._jNIJavaUtilities.javaClassName(cm);
        _builder.append(_javaClassName_1);
        _builder.append(" build(long ptr) {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        final Function1<StructToClassMapping, Boolean> _function = (StructToClassMapping sc) -> {
          final Function1<Method, Boolean> _function_1 = (Method e) -> {
            return Boolean.valueOf(e.isInstanceof());
          };
          return Boolean.valueOf(IterableExtensions.<Method>exists(this._commonExtensions.getMethodGroup(sc, m).getMethods(), _function_1));
        };
        final Iterable<StructToClassMapping> scWithInstanceOfs = IterableExtensions.<StructToClassMapping>filter(this._typeExtensions.getAllSubClasses(cm), _function);
        _builder.newLineIfNotEmpty();
        {
          for(final StructToClassMapping subClass : scWithInstanceOfs) {
            _builder.append("\t");
            _builder.append("if (isJNI");
            String _firstUpper = StringExtensions.toFirstUpper(subClass.getName());
            _builder.append(_firstUpper, "\t");
            _builder.append("(ptr)) {");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("return new ");
            CharSequence _javaClassName_2 = this._jNIJavaUtilities.javaClassName(subClass);
            _builder.append(_javaClassName_2, "\t\t");
            _builder.append("(ptr);");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("} else ");
            _builder.newLine();
          }
        }
        _builder.append("\t");
        _builder.append("{");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("return new ");
        CharSequence _javaClassName_3 = this._jNIJavaUtilities.javaClassName(cm);
        _builder.append(_javaClassName_3, "\t\t");
        _builder.append("(ptr);");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("}");
        _builder.newLine();
        _builder.append("}");
        _builder.newLine();
        _builder.newLine();
        {
          List<StructToClassMapping> _allSubClasses = this._typeExtensions.getAllSubClasses(cm);
          for(final StructToClassMapping subClass_1 : _allSubClasses) {
            final MethodGroup scMG = this._commonExtensions.getMethodGroup(subClass_1, m);
            _builder.newLineIfNotEmpty();
            {
              if (((scMG == null) || (IterableExtensions.size(IterableExtensions.<Method>filter(scMG.getMethods(), ((Function1<Method, Boolean>) (Method e) -> {
                return Boolean.valueOf(e.isInstanceof());
              }))) == 0))) {
                String _name_1 = subClass_1.getName();
                String _plus = ("[ERROR] @StructWrapper: Subclass " + _name_1);
                String _plus_1 = (_plus + " of ");
                String _name_2 = cm.getName();
                String _plus_2 = (_plus_1 + _name_2);
                String _plus_3 = (_plus_2 + " has no instanceof method");
                System.err.println(_plus_3);
                _builder.newLineIfNotEmpty();
              } else {
                {
                  final Function1<Method, Boolean> _function_1 = (Method e) -> {
                    return Boolean.valueOf(e.isInstanceof());
                  };
                  Iterable<Method> _filter = IterableExtensions.<Method>filter(this._commonExtensions.getMethodGroup(subClass_1, m).getMethods(), _function_1);
                  for(final Method method : _filter) {
                    _builder.append("/* @generated */");
                    _builder.newLine();
                    _builder.append("private static boolean isJNI");
                    String _firstUpper_1 = StringExtensions.toFirstUpper(subClass_1.getName());
                    _builder.append(_firstUpper_1);
                    _builder.append("(long ptr) {");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t");
                    _builder.append("if (");
                    String _ID = this._commonExtensions.ID(m);
                    _builder.append(_ID, "\t");
                    _builder.append("Native.");
                    String _name_3 = method.getName();
                    _builder.append(_name_3, "\t");
                    _builder.append("(ptr) != 0) {");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t\t");
                    _builder.append("return true;");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("} else {");
                    _builder.newLine();
                    _builder.append("\t\t");
                    _builder.append("return false;");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("}");
                    _builder.newLine();
                    _builder.append("}");
                    _builder.newLine();
                    _builder.newLine();
                    _builder.append("/* @generated */");
                    _builder.newLine();
                    _builder.append("public boolean isJNI");
                    String _firstUpper_2 = StringExtensions.toFirstUpper(subClass_1.getName());
                    _builder.append(_firstUpper_2);
                    _builder.append("() {");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t");
                    _builder.append("return isJNI");
                    String _firstUpper_3 = StringExtensions.toFirstUpper(subClass_1.getName());
                    _builder.append(_firstUpper_3, "\t");
                    _builder.append("(getNativePtr(this));");
                    _builder.newLineIfNotEmpty();
                    _builder.append("}");
                    _builder.newLine();
                  }
                }
              }
            }
            _builder.newLine();
          }
        }
        _builder.newLine();
        {
          int _size = this._typeExtensions.getAllSubClasses(cm).size();
          boolean _notEquals = (_size != 0);
          if (_notEquals) {
            _builder.append("public Object accept(");
            CharSequence _javaVisitorName = this._jNIJavaUtilities.javaVisitorName(cm);
            _builder.append(_javaVisitorName);
            _builder.append(" visitor, Object arg) {");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("return visitor.visit");
            CharSequence _javaClassName_4 = this._jNIJavaUtilities.javaClassName(cm);
            _builder.append(_javaClassName_4, "\t");
            _builder.append("(this, arg);");
            _builder.newLineIfNotEmpty();
            _builder.append("}");
            _builder.newLine();
          }
        }
      } else {
        _builder.newLine();
        _builder.append("\t");
        _builder.append("public Object accept(");
        CharSequence _javaVisitorName_1 = this._jNIJavaUtilities.javaVisitorName(cm.getSuper());
        _builder.append(_javaVisitorName_1, "\t");
        _builder.append(" visitor, Object arg) {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("return visitor.visit");
        CharSequence _javaClassName_5 = this._jNIJavaUtilities.javaClassName(cm);
        _builder.append(_javaClassName_5, "\t\t");
        _builder.append("(this, arg);");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("}");
        _builder.newLine();
      }
    }
    _builder.newLine();
    return _builder;
  }
  
  public String visitorFilepath(final StructToClassMapping cm, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _javaRootPath = this._commonExtensions.javaRootPath();
    _builder.append(_javaRootPath);
    String _packagePath = this._commonExtensions.packagePath(m);
    _builder.append(_packagePath);
    _builder.append("/");
    CharSequence _javaVisitorName = this._jNIJavaUtilities.javaVisitorName(cm);
    _builder.append(_javaVisitorName);
    _builder.append(".java");
    return _builder.toString();
  }
  
  public CharSequence visitorContent(final StructToClassMapping cm, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _packageName = this._commonExtensions.packageName(m);
    _builder.append(_packageName);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("/**");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*  Automatically generated by jnimap ");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* @generated");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("public interface ");
    CharSequence _javaVisitorName = this._jNIJavaUtilities.javaVisitorName(cm);
    _builder.append(_javaVisitorName);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("Object visit");
    CharSequence _javaClassName = this._jNIJavaUtilities.javaClassName(cm);
    _builder.append(_javaClassName, "\t");
    _builder.append("(");
    CharSequence _javaClassName_1 = this._jNIJavaUtilities.javaClassName(cm);
    _builder.append(_javaClassName_1, "\t");
    _builder.append(" obj, Object arg);");
    _builder.newLineIfNotEmpty();
    {
      List<StructToClassMapping> _allSubClasses = this._typeExtensions.getAllSubClasses(cm);
      for(final StructToClassMapping subClass : _allSubClasses) {
        _builder.append("\t");
        _builder.append("Object visit");
        CharSequence _javaClassName_2 = this._jNIJavaUtilities.javaClassName(subClass);
        _builder.append(_javaClassName_2, "\t");
        _builder.append("(");
        CharSequence _javaClassName_3 = this._jNIJavaUtilities.javaClassName(subClass);
        _builder.append(_javaClassName_3, "\t");
        _builder.append(" obj, Object arg);");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  /**
   * SETTERS
   */
  protected CharSequence _genSetter(final Type t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/*************************************************************");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*\t");
    String _name = field.getName();
    _builder.append(_name, " ");
    _builder.append(" : ");
    String _standardJavaName = this._typeExtensions.standardJavaName(field.getType());
    _builder.append(_standardJavaName, " ");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*************************************************************/");
    _builder.newLine();
    _builder.append("public void set");
    String _ID = this._commonExtensions.ID(field);
    _builder.append(_ID);
    _builder.append("(");
    String _standardJavaName_1 = this._typeExtensions.standardJavaName(t);
    _builder.append(_standardJavaName_1);
    _builder.append(" object) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _ID_1 = this._commonExtensions.ID(m);
    _builder.append(_ID_1, "\t");
    _builder.append("Native.");
    String _name_1 = decl.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_set_");
    String _name_2 = field.getName();
    _builder.append(_name_2, "\t");
    _builder.append("(getNativePtr(this), getNativePtr(object));");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _genSetter(final BuiltInType t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/*************************************************************");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*\t");
    String _name = field.getName();
    _builder.append(_name, " ");
    _builder.append(" : ");
    String _standardJavaName = this._typeExtensions.standardJavaName(field.getType());
    _builder.append(_standardJavaName, " ");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*************************************************************/");
    _builder.newLine();
    _builder.append("public void set");
    String _ID = this._commonExtensions.ID(field);
    _builder.append(_ID);
    _builder.append("(");
    String _standardJavaName_1 = this._typeExtensions.standardJavaName(t);
    _builder.append(_standardJavaName_1);
    _builder.append(" object) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _ID_1 = this._commonExtensions.ID(m);
    _builder.append(_ID_1, "\t");
    _builder.append("Native.");
    String _name_1 = decl.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_set_");
    String _name_2 = field.getName();
    _builder.append(_name_2, "\t");
    _builder.append("(getNativePtr(this), object);");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _genSetter(final EnumType t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/*************************************************************");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*\t");
    String _name = field.getName();
    _builder.append(_name, " ");
    _builder.append(" : ");
    String _standardJavaName = this._typeExtensions.standardJavaName(field.getType());
    _builder.append(_standardJavaName, " ");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*************************************************************/");
    _builder.newLine();
    _builder.append("public void set");
    String _ID = this._commonExtensions.ID(field);
    _builder.append(_ID);
    _builder.append("(");
    String _standardJavaName_1 = this._typeExtensions.standardJavaName(t);
    _builder.append(_standardJavaName_1);
    _builder.append(" object) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _ID_1 = this._commonExtensions.ID(m);
    _builder.append(_ID_1, "\t");
    _builder.append("Native.");
    String _name_1 = decl.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_set_");
    String _name_2 = field.getName();
    _builder.append(_name_2, "\t");
    _builder.append("(getNativePtr(this), object.getValue());");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  /**
   * TESTERS
   */
  public CharSequence genTester(final Type t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/*************************************************************");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* Test if T");
    String _name = field.getName();
    _builder.append(_name, " ");
    _builder.append(" is NULL");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*************************************************************/");
    _builder.newLine();
    _builder.append("public boolean test");
    String _ID = this._commonExtensions.ID(field);
    _builder.append(_ID);
    _builder.append("() {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("return ");
    String _ID_1 = this._commonExtensions.ID(m);
    _builder.append(_ID_1, "\t");
    _builder.append("Native.");
    String _name_1 = decl.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_test_");
    String _name_2 = field.getName();
    _builder.append(_name_2, "\t");
    _builder.append("(getNativePtr(this)) != 0;");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  /**
   * GETTERS
   */
  protected CharSequence _genGetter(final Type t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// default get ");
    String _standardJavaName = this._typeExtensions.standardJavaName(t);
    _builder.append(_standardJavaName);
    _builder.append(", ");
    String _name = field.getName();
    _builder.append(_name);
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  protected CharSequence _genGetter(final BuiltInType t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/*************************************************************");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*\t");
    String _name = field.getName();
    _builder.append(_name, " ");
    _builder.append(" : ");
    String _standardJavaName = this._typeExtensions.standardJavaName(field.getType());
    _builder.append(_standardJavaName, " ");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*************************************************************/");
    _builder.newLine();
    _builder.append("public ");
    String _standardJavaName_1 = this._typeExtensions.standardJavaName(t);
    _builder.append(_standardJavaName_1);
    _builder.append(" get");
    String _ID = this._commonExtensions.ID(field);
    _builder.append(_ID);
    _builder.append("() {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("return ");
    String _ID_1 = this._commonExtensions.ID(m);
    _builder.append(_ID_1, "\t");
    _builder.append("Native.");
    String _name_1 = decl.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_get_");
    String _name_2 = field.getName();
    _builder.append(_name_2, "\t");
    _builder.append("(getNativePtr(this));");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _genGetter(final StructType t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/*************************************************************");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*\t");
    String _name = field.getName();
    _builder.append(_name, " ");
    _builder.append(" : ");
    String _standardJavaName = this._typeExtensions.standardJavaName(field.getType());
    _builder.append(_standardJavaName, " ");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*************************************************************/");
    _builder.newLine();
    _builder.append("public ");
    String _standardJavaName_1 = this._typeExtensions.standardJavaName(t);
    _builder.append(_standardJavaName_1);
    _builder.append(" get");
    String _ID = this._commonExtensions.ID(field);
    _builder.append(_ID);
    _builder.append("() {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("return ");
    String _standardJavaName_2 = this._typeExtensions.standardJavaName(t);
    _builder.append(_standardJavaName_2, "\t");
    _builder.append(".build(");
    String _ID_1 = this._commonExtensions.ID(m);
    _builder.append(_ID_1, "\t");
    _builder.append("Native.");
    String _name_1 = decl.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_get_");
    String _name_2 = field.getName();
    _builder.append(_name_2, "\t");
    _builder.append("(getNativePtr(this)));");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _genGetter(final TypeAlias t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/*************************************************************");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*\t");
    String _name = field.getName();
    _builder.append(_name, " ");
    _builder.append(" : ");
    String _standardJavaName = this._typeExtensions.standardJavaName(field.getType());
    _builder.append(_standardJavaName, " ");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*************************************************************/");
    _builder.newLine();
    _builder.append("public ");
    String _standardJavaName_1 = this._typeExtensions.standardJavaName(t);
    _builder.append(_standardJavaName_1);
    _builder.append(" get");
    String _ID = this._commonExtensions.ID(field);
    _builder.append(_ID);
    _builder.append("() {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("return ");
    String _standardJavaName_2 = this._typeExtensions.standardJavaName(t);
    _builder.append(_standardJavaName_2, "\t");
    _builder.append(".build(");
    String _ID_1 = this._commonExtensions.ID(m);
    _builder.append(_ID_1, "\t");
    _builder.append("Native.");
    String _name_1 = decl.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_get_");
    String _name_2 = field.getName();
    _builder.append(_name_2, "\t");
    _builder.append("(getNativePtr(this)));");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _genGetter(final EnumType t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/*************************************************************");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*\t");
    String _name = field.getName();
    _builder.append(_name, " ");
    _builder.append(" : ");
    String _standardJavaName = this._typeExtensions.standardJavaName(field.getType());
    _builder.append(_standardJavaName, " ");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*************************************************************/");
    _builder.newLine();
    _builder.append("public ");
    String _standardJavaName_1 = this._typeExtensions.standardJavaName(t);
    _builder.append(_standardJavaName_1);
    _builder.append(" get");
    String _ID = this._commonExtensions.ID(field);
    _builder.append(_ID);
    _builder.append("() {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("return ");
    String _standardJavaName_2 = this._typeExtensions.standardJavaName(t);
    _builder.append(_standardJavaName_2, "\t");
    _builder.append(".getFromInt(");
    String _ID_1 = this._commonExtensions.ID(m);
    _builder.append(_ID_1, "\t");
    _builder.append("Native.");
    String _name_1 = decl.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_get_");
    String _name_2 = field.getName();
    _builder.append(_name_2, "\t");
    _builder.append("(getNativePtr(this)));");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _genGetter(final PtrType t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/*************************************************************");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*\t");
    String _name = field.getName();
    _builder.append(_name, " ");
    _builder.append(" : ");
    String _standardJavaName = this._typeExtensions.standardJavaName(field.getType());
    _builder.append(_standardJavaName, " ");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*************************************************************/");
    _builder.newLine();
    _builder.append("public ");
    String _standardJavaName_1 = this._typeExtensions.standardJavaName(t);
    _builder.append(_standardJavaName_1);
    _builder.append(" get");
    String _ID = this._commonExtensions.ID(field);
    _builder.append(_ID);
    _builder.append("() {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    CharSequence _genPtrGetter = this.genPtrGetter(t.getBase(), decl, field, m);
    _builder.append(_genPtrGetter, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _genPtrGetter(final Type t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("return ");
    String _standardJavaName = this._typeExtensions.standardJavaName(field.getType());
    _builder.append(_standardJavaName);
    _builder.append(".build(");
    String _name = m.getName();
    _builder.append(_name);
    _builder.append("Native.");
    String _name_1 = decl.getName();
    _builder.append(_name_1);
    _builder.append("_get_");
    String _name_2 = field.getName();
    _builder.append(_name_2);
    _builder.append("(getNativePtr(this)));");
    return _builder;
  }
  
  protected CharSequence _genPtrGetter(final EnumType t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("return ");
    String _standardJavaName = this._typeExtensions.standardJavaName(field.getType());
    _builder.append(_standardJavaName);
    _builder.append(".build(");
    String _name = m.getName();
    _builder.append(_name);
    _builder.append("Native.");
    String _name_1 = decl.getName();
    _builder.append(_name_1);
    _builder.append("_get_");
    String _name_2 = field.getName();
    _builder.append(_name_2);
    _builder.append("(getNativePtr(this)));");
    return _builder;
  }
  
  public CharSequence genSetter(final Type t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    if (t instanceof BuiltInType) {
      return _genSetter((BuiltInType)t, decl, field, m);
    } else if (t instanceof EnumType) {
      return _genSetter((EnumType)t, decl, field, m);
    } else if (t != null) {
      return _genSetter(t, decl, field, m);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(t, decl, field, m).toString());
    }
  }
  
  public CharSequence genGetter(final Type t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    if (t instanceof BuiltInType) {
      return _genGetter((BuiltInType)t, decl, field, m);
    } else if (t instanceof EnumType) {
      return _genGetter((EnumType)t, decl, field, m);
    } else if (t instanceof StructType) {
      return _genGetter((StructType)t, decl, field, m);
    } else if (t instanceof TypeAlias) {
      return _genGetter((TypeAlias)t, decl, field, m);
    } else if (t instanceof PtrType) {
      return _genGetter((PtrType)t, decl, field, m);
    } else if (t != null) {
      return _genGetter(t, decl, field, m);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(t, decl, field, m).toString());
    }
  }
  
  public CharSequence genPtrGetter(final Type t, final StructDeclaration decl, final FieldDef field, final Mapping m) {
    if (t instanceof EnumType) {
      return _genPtrGetter((EnumType)t, decl, field, m);
    } else if (t != null) {
      return _genPtrGetter(t, decl, field, m);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(t, decl, field, m).toString());
    }
  }
}
