package fr.irisa.cairn.jnimap.generator.java;

import com.google.common.base.Objects;
import fr.irisa.cairn.jniMap.AliasToClassMapping;
import fr.irisa.cairn.jniMap.ArrayType;
import fr.irisa.cairn.jniMap.BooleanType;
import fr.irisa.cairn.jniMap.ClassMapping;
import fr.irisa.cairn.jniMap.ConstType;
import fr.irisa.cairn.jniMap.EnumType;
import fr.irisa.cairn.jniMap.GenericsDeclaration;
import fr.irisa.cairn.jniMap.GenericsInstantiation;
import fr.irisa.cairn.jniMap.InterfaceClass;
import fr.irisa.cairn.jniMap.InterfaceImplementation;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.ParamDef;
import fr.irisa.cairn.jniMap.PtrType;
import fr.irisa.cairn.jniMap.StructToClassMapping;
import fr.irisa.cairn.jniMap.Type;
import fr.irisa.cairn.jniMap.UnmappedClass;
import fr.irisa.cairn.jnimap.generator.CommonExtensions;
import fr.irisa.cairn.jnimap.generator.TypeExtensions;
import java.util.Arrays;
import java.util.List;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class JNIJavaUtilities {
  @Extension
  private TypeExtensions _typeExtensions = new TypeExtensions();
  
  @Extension
  private CommonExtensions _commonExtensions = new CommonExtensions();
  
  public CharSequence javaClassName(final ClassMapping cm) {
    StringConcatenation _builder = new StringConcatenation();
    String _firstUpper = StringExtensions.toFirstUpper(cm.getName());
    _builder.append(_firstUpper);
    return _builder;
  }
  
  private String javaQualifiedClassName(final ClassMapping cm, final Mapping m) {
    Mapping _enclosingMapping = this._commonExtensions.getEnclosingMapping(cm);
    boolean _equals = Objects.equal(_enclosingMapping, m);
    if (_equals) {
      StringConcatenation _builder = new StringConcatenation();
      CharSequence _javaClassName = this.javaClassName(cm);
      _builder.append(_javaClassName);
      return _builder.toString();
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _packageName = this._commonExtensions.packageName(this._commonExtensions.getRoot(cm));
      _builder_1.append(_packageName);
      _builder_1.append(".");
      CharSequence _javaClassName_1 = this.javaClassName(cm);
      _builder_1.append(_javaClassName_1);
      return _builder_1.toString();
    }
  }
  
  protected String _javaSuperClass(final ClassMapping cm, final Mapping m) {
    return this.printSuperClass(null, null, m);
  }
  
  protected String _javaSuperClass(final AliasToClassMapping cm, final Mapping m) {
    return this.printSuperClass(cm.getSuper(), cm.getGenericsInstantiation(), m);
  }
  
  protected String _javaSuperClass(final StructToClassMapping cm, final Mapping m) {
    return this.printSuperClass(cm.getSuper(), null, m);
  }
  
  protected String _javaSuperClass(final UnmappedClass cm, final Mapping m) {
    return this.printSuperClass(cm.getSuper(), cm.getGenericsInstantiation(), m);
  }
  
  public String printSuperClass(final ClassMapping _super, final GenericsInstantiation gi, final Mapping m) {
    if ((_super == null)) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("JNIObject");
      return _builder.toString();
    }
    String _xifexpression = null;
    if (((gi != null) && (!gi.getInstanceNames().isEmpty()))) {
      final Function1<String, CharSequence> _function = (String s) -> {
        return s;
      };
      _xifexpression = IterableExtensions.<String>join(gi.getInstanceNames(), "<", ", ", ">", _function);
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _xifexpression = _builder_1.toString();
    }
    final String gis = _xifexpression;
    StringConcatenation _builder_2 = new StringConcatenation();
    String _javaQualifiedClassName = this.javaQualifiedClassName(_super, m);
    _builder_2.append(_javaQualifiedClassName);
    _builder_2.append(gis);
    return _builder_2.toString();
  }
  
  public CharSequence javaVisitorName(final ClassMapping cm) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("I");
    String _firstUpper = StringExtensions.toFirstUpper(cm.getName());
    _builder.append(_firstUpper);
    _builder.append("Visitor");
    return _builder;
  }
  
  public CharSequence nativeParam(final ParamDef pd) {
    return this.nativeParam(pd.getType(), pd);
  }
  
  public CharSequence nativeParamStatic(final ParamDef pd) {
    return this.nativeParamStatic(pd.getType(), pd);
  }
  
  protected CharSequence _nativeParamStatic(final Type t, final ParamDef pd) {
    return this.nativeParam(t, pd);
  }
  
  protected CharSequence _nativeParamStatic(final PtrType t, final ParamDef pd) {
    CharSequence _xifexpression = null;
    boolean _isPrimitivePtrType = this._typeExtensions.isPrimitivePtrType(t);
    if (_isPrimitivePtrType) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = pd.getName();
      _builder.append(_name);
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("getNativePtr(");
      String _name_1 = pd.getName();
      _builder_1.append(_name_1);
      _builder_1.append(")");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _nativeParam(final Type t, final ParamDef pd) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = pd.getName();
    _builder.append(_name);
    return _builder;
  }
  
  protected CharSequence _nativeParam(final BooleanType t, final ParamDef pd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("toInt(");
    String _name = pd.getName();
    _builder.append(_name);
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _nativeParam(final PtrType t, final ParamDef pd) {
    CharSequence _xifexpression = null;
    boolean _isPrimitivePtrType = this._typeExtensions.isPrimitivePtrType(t);
    if (_isPrimitivePtrType) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = pd.getName();
      _builder.append(_name);
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("getNativePtr(");
      String _name_1 = pd.getName();
      _builder_1.append(_name_1);
      _builder_1.append(")");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _nativeParam(final ConstType t, final ParamDef pd) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _nativeParam = this.nativeParam(t.getBase(), pd);
    _builder.append(_nativeParam);
    return _builder;
  }
  
  protected CharSequence _nativeParam(final ArrayType t, final ParamDef pd) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("getNativePtr(");
    String _name = pd.getName();
    _builder.append(_name);
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _nativeParam(final EnumType t, final ParamDef pd) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = pd.getName();
    _builder.append(_name);
    _builder.append(".getValue()");
    return _builder;
  }
  
  protected boolean _isAbstract(final ClassMapping cm) {
    return false;
  }
  
  protected boolean _isAbstract(final UnmappedClass cm) {
    return cm.isAbstract();
  }
  
  protected boolean _isInterface(final ClassMapping cm) {
    return false;
  }
  
  protected boolean _isInterface(final InterfaceClass cm) {
    return true;
  }
  
  protected CharSequence _genericsDeclarations(final ClassMapping cm) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _genericsDeclarations(final AliasToClassMapping cm) {
    return this.printGenericsDecl(cm.getGenericsDecl());
  }
  
  protected CharSequence _genericsDeclarations(final UnmappedClass cm) {
    return this.printGenericsDecl(cm.getGenericsDecl());
  }
  
  public CharSequence printGenericsDecl(final GenericsDeclaration gd) {
    CharSequence _xifexpression = null;
    if (((gd != null) && (!gd.getNames().isEmpty()))) {
      final Function1<String, CharSequence> _function = (String s) -> {
        return s;
      };
      _xifexpression = IterableExtensions.<String>join(gd.getNames(), "<", ", ", ">", _function);
    } else {
      StringConcatenation _builder = new StringConcatenation();
      _xifexpression = _builder;
    }
    return _xifexpression;
  }
  
  protected CharSequence _interfaceImplementations(final ClassMapping cm, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _interfaceImplementations(final AliasToClassMapping cm, final Mapping m) {
    return this.printInterfaceImplementations(cm.getInterfaces(), this.isInterface(cm), m);
  }
  
  protected CharSequence _interfaceImplementations(final UnmappedClass cm, final Mapping m) {
    return this.printInterfaceImplementations(cm.getInterfaces(), this.isInterface(cm), m);
  }
  
  protected CharSequence _interfaceImplementations(final InterfaceClass cm, final Mapping m) {
    return this.printInterfaceImplementations(cm.getInterfaces(), this.isInterface(cm), m);
  }
  
  public CharSequence printInterfaceImplementations(final List<InterfaceImplementation> interfaces, final boolean isInterface, final Mapping m) {
    CharSequence _xifexpression = null;
    boolean _isEmpty = interfaces.isEmpty();
    if (_isEmpty) {
      StringConcatenation _builder = new StringConcatenation();
      _xifexpression = _builder;
    } else {
      String _xifexpression_1 = null;
      if (isInterface) {
        _xifexpression_1 = "extends ";
      } else {
        _xifexpression_1 = "implements ";
      }
      final Function1<InterfaceImplementation, CharSequence> _function = (InterfaceImplementation i) -> {
        StringConcatenation _builder_1 = new StringConcatenation();
        String _printSuperClass = this.printSuperClass(i.getInterface(), i.getGenericsInstantiation(), m);
        _builder_1.append(_printSuperClass);
        return _builder_1.toString();
      };
      _xifexpression = IterableExtensions.<InterfaceImplementation>join(interfaces, _xifexpression_1, ", ", "", _function);
    }
    return _xifexpression;
  }
  
  public String javaSuperClass(final ClassMapping cm, final Mapping m) {
    if (cm instanceof AliasToClassMapping) {
      return _javaSuperClass((AliasToClassMapping)cm, m);
    } else if (cm instanceof StructToClassMapping) {
      return _javaSuperClass((StructToClassMapping)cm, m);
    } else if (cm instanceof UnmappedClass) {
      return _javaSuperClass((UnmappedClass)cm, m);
    } else if (cm != null) {
      return _javaSuperClass(cm, m);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(cm, m).toString());
    }
  }
  
  public CharSequence nativeParamStatic(final Type t, final ParamDef pd) {
    if (t instanceof PtrType) {
      return _nativeParamStatic((PtrType)t, pd);
    } else if (t != null) {
      return _nativeParamStatic(t, pd);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(t, pd).toString());
    }
  }
  
  public CharSequence nativeParam(final Type t, final ParamDef pd) {
    if (t instanceof BooleanType) {
      return _nativeParam((BooleanType)t, pd);
    } else if (t instanceof EnumType) {
      return _nativeParam((EnumType)t, pd);
    } else if (t instanceof ArrayType) {
      return _nativeParam((ArrayType)t, pd);
    } else if (t instanceof ConstType) {
      return _nativeParam((ConstType)t, pd);
    } else if (t instanceof PtrType) {
      return _nativeParam((PtrType)t, pd);
    } else if (t != null) {
      return _nativeParam(t, pd);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(t, pd).toString());
    }
  }
  
  public boolean isAbstract(final ClassMapping cm) {
    if (cm instanceof UnmappedClass) {
      return _isAbstract((UnmappedClass)cm);
    } else if (cm != null) {
      return _isAbstract(cm);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(cm).toString());
    }
  }
  
  public boolean isInterface(final ClassMapping cm) {
    if (cm instanceof InterfaceClass) {
      return _isInterface((InterfaceClass)cm);
    } else if (cm != null) {
      return _isInterface(cm);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(cm).toString());
    }
  }
  
  public CharSequence genericsDeclarations(final ClassMapping cm) {
    if (cm instanceof AliasToClassMapping) {
      return _genericsDeclarations((AliasToClassMapping)cm);
    } else if (cm instanceof UnmappedClass) {
      return _genericsDeclarations((UnmappedClass)cm);
    } else if (cm != null) {
      return _genericsDeclarations(cm);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(cm).toString());
    }
  }
  
  public CharSequence interfaceImplementations(final ClassMapping cm, final Mapping m) {
    if (cm instanceof AliasToClassMapping) {
      return _interfaceImplementations((AliasToClassMapping)cm, m);
    } else if (cm instanceof InterfaceClass) {
      return _interfaceImplementations((InterfaceClass)cm, m);
    } else if (cm instanceof UnmappedClass) {
      return _interfaceImplementations((UnmappedClass)cm, m);
    } else if (cm != null) {
      return _interfaceImplementations(cm, m);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(cm, m).toString());
    }
  }
}
