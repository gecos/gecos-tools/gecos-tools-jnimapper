package fr.irisa.cairn.jnimap.generator.c;

import fr.irisa.cairn.jniMap.CMethod;
import fr.irisa.cairn.jniMap.ConstType;
import fr.irisa.cairn.jniMap.EnumType;
import fr.irisa.cairn.jniMap.IncludeDef;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.Method;
import fr.irisa.cairn.jniMap.ParamDef;
import fr.irisa.cairn.jniMap.PtrType;
import fr.irisa.cairn.jniMap.StringType;
import fr.irisa.cairn.jniMap.StructType;
import fr.irisa.cairn.jniMap.Type;
import fr.irisa.cairn.jniMap.TypeAlias;
import fr.irisa.cairn.jniMap.VoidType;
import fr.irisa.cairn.jnimap.generator.CommonExtensions;
import fr.irisa.cairn.jnimap.generator.TypeExtensions;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class JNICUtilities {
  @Extension
  private CommonExtensions _commonExtensions = new CommonExtensions();
  
  @Extension
  private TypeExtensions _typeExtensions = new TypeExtensions();
  
  public CharSequence commonIncludes(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <jni.h>");
    _builder.newLine();
    _builder.append("#include <stdio.h>");
    _builder.newLine();
    _builder.append("#include <stdlib.h>");
    _builder.newLine();
    _builder.append("#include <string.h>");
    _builder.newLine();
    _builder.newLine();
    {
      EList<IncludeDef> _includes = m.getIncludes();
      for(final IncludeDef include : _includes) {
        _builder.append("#include ");
        String _include = include.getInclude();
        _builder.append(_include);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  public CharSequence jniCprefix(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    String _packagePrefix = this._commonExtensions.packagePrefix(m);
    _builder.append(_packagePrefix);
    _builder.append("_");
    String _ID = this._commonExtensions.ID(m);
    _builder.append(_ID);
    _builder.append("Native");
    return _builder;
  }
  
  protected CharSequence _JNIFunctionHeader(final Method m) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _JNIFunctionName = this.JNIFunctionName(m);
    _builder.append(_JNIFunctionName);
    _builder.append("(JNIEnv *env, jclass class");
    final Function1<ParamDef, CharSequence> _function = (ParamDef p) -> {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _jNICName = this._typeExtensions.getJNICName(p.getType());
      _builder_1.append(_jNICName);
      _builder_1.append(" ");
      String _name = p.getName();
      _builder_1.append(_name);
      return _builder_1.toString();
    };
    String _join = IterableExtensions.<ParamDef>join(this._commonExtensions.getActiveParams(m), ", ", ", ", "", _function);
    _builder.append(_join);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  private CharSequence JNIFunctionName(final Method m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("JNIEXPORT ");
    String _jNICName = this._typeExtensions.getJNICName(m.getRes());
    _builder.append(_jNICName);
    _builder.append(" JNICALL Java_");
    CharSequence _jniCprefix = this.jniCprefix(this._commonExtensions.getRoot(m));
    _builder.append(_jniCprefix);
    _builder.append("_");
    String _replaceAll = m.getName().replaceAll("_", "_1");
    _builder.append(_replaceAll);
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  protected CharSequence _JNIFunctionHeader(final CMethod cm) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _JNIFunctionName = this.JNIFunctionName(cm);
    _builder.append(_JNIFunctionName);
    _builder.append("(JNIEnv *env, jclass clazz");
    final Function1<ParamDef, CharSequence> _function = (ParamDef p) -> {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _jNICName = this._typeExtensions.getJNICName(p.getType());
      _builder_1.append(_jNICName);
      _builder_1.append(" ");
      String _name = p.getName();
      _builder_1.append(_name);
      return _builder_1.toString();
    };
    String _join = IterableExtensions.<ParamDef>join(cm.getParams(), ", ", ", ", "", _function);
    _builder.append(_join);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  private CharSequence JNIFunctionName(final CMethod m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("JNIEXPORT ");
    String _jNICName = this._typeExtensions.getJNICName(m.getRes());
    _builder.append(_jNICName);
    _builder.append(" JNICALL Java_");
    CharSequence _jniCprefix = this.jniCprefix(this._commonExtensions.getRoot(m));
    _builder.append(_jniCprefix);
    _builder.append("_00024UserModules_");
    String _replaceAll = m.getName().replaceAll("_", "_1");
    _builder.append(_replaceAll);
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  public String castParam(final ParamDef pd) {
    StringConcatenation _builder = new StringConcatenation();
    String _standardCName = this._typeExtensions.getStandardCName(pd.getType());
    _builder.append(_standardCName);
    _builder.append(" ");
    String _name = pd.getName();
    _builder.append(_name);
    _builder.append("_c = (");
    String _standardCName_1 = this._typeExtensions.getStandardCName(pd.getType());
    _builder.append(_standardCName_1);
    _builder.append(")");
    final String head = _builder.toString();
    StringConcatenation _builder_1 = new StringConcatenation();
    {
      boolean _isStringType = this._typeExtensions.isStringType(pd.getType());
      if (_isStringType) {
        _builder_1.append("(*env)->GetStringUTFChars(env,");
        String _name_1 = pd.getName();
        _builder_1.append(_name_1);
        _builder_1.append(",0);");
        _builder_1.newLineIfNotEmpty();
      } else {
        boolean _isPointer = this._typeExtensions.isPointer(pd.getType());
        if (_isPointer) {
          _builder_1.append("GECOS_PTRSIZE ");
          String _name_2 = pd.getName();
          _builder_1.append(_name_2);
          _builder_1.append(";");
          _builder_1.newLineIfNotEmpty();
        } else {
          String _name_3 = pd.getName();
          _builder_1.append(_name_3);
          _builder_1.append(";");
          _builder_1.newLineIfNotEmpty();
        }
      }
    }
    final String tail = _builder_1.toString();
    return (head + tail);
  }
  
  protected CharSequence _returnStatement(final Type t) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return (");
    String _jNICName = this._typeExtensions.getJNICName(t);
    _builder.append(_jNICName, "\t");
    _builder.append(")  res;");
    _builder.newLineIfNotEmpty();
    _builder.append("error:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return  (");
    String _jNICName_1 = this._typeExtensions.getJNICName(t);
    _builder.append(_jNICName_1, "\t");
    _builder.append(") GECOS_PTRSIZE NULL;");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  protected CharSequence _returnStatement(final ConstType t) {
    return this.returnStatement(t.getBase());
  }
  
  protected CharSequence _returnStatement(final StringType t) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return (*env)->NewStringUTF(env, res);");
    _builder.newLine();
    _builder.newLine();
    _builder.append("error:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return (jstring) GECOS_PTRSIZE NULL;");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _returnStatement(final VoidType t) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("error:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return;");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _returnStatement(final PtrType t) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return (");
    String _jNICName = this._typeExtensions.getJNICName(t);
    _builder.append(_jNICName, "\t");
    _builder.append(") GECOS_PTRSIZE res;");
    _builder.newLineIfNotEmpty();
    _builder.append("error:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return  (");
    String _jNICName_1 = this._typeExtensions.getJNICName(t);
    _builder.append(_jNICName_1, "\t");
    _builder.append(") GECOS_PTRSIZE NULL;");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  protected CharSequence _returnStatement(final EnumType t) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return (");
    String _jNICName = this._typeExtensions.getJNICName(t);
    _builder.append(_jNICName, "\t");
    _builder.append(")  res;");
    _builder.newLineIfNotEmpty();
    _builder.append("error:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return -1000;");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _resultInstance(final Type t) {
    StringConcatenation _builder = new StringConcatenation();
    String _standardCName = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName);
    _builder.append(" res = (");
    String _standardCName_1 = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName_1);
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _resultInstance(final StringType t) {
    StringConcatenation _builder = new StringConcatenation();
    String _standardCName = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName);
    _builder.append(" res = (");
    String _standardCName_1 = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName_1);
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _resultInstance(final PtrType t) {
    StringConcatenation _builder = new StringConcatenation();
    String _standardCName = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName);
    _builder.append(" res = (");
    String _standardCName_1 = this._typeExtensions.getStandardCName(t);
    _builder.append(_standardCName_1);
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _resultInstance(final StructType t) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _resultInstance(final TypeAlias t) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _resultInstance(final VoidType t) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  public CharSequence paramInstance(final ParamDef param) {
    return this.paramInstance(param.getType(), param);
  }
  
  private CharSequence _paramInstance(final Type type, final ParamDef param) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = param.getName();
    _builder.append(_name);
    _builder.append("_c");
    return _builder;
  }
  
  private CharSequence _paramInstance(final TypeAlias type, final ParamDef param) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("*");
    String _name = param.getName();
    _builder.append(_name);
    _builder.append("_c");
    return _builder;
  }
  
  private CharSequence _paramInstance(final StringType type, final ParamDef param) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = param.getName();
    _builder.append(_name);
    _builder.append("_c");
    return _builder;
  }
  
  private CharSequence _paramInstance(final PtrType type, final ParamDef param) {
    CharSequence _xifexpression = null;
    boolean _isIgnored = type.isIgnored();
    if (_isIgnored) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("NULL");
      return _builder.toString();
    } else {
      CharSequence _xifexpression_1 = null;
      boolean _isPrimitivePtrType = this._typeExtensions.isPrimitivePtrType(type);
      if (_isPrimitivePtrType) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("&");
        String _name = param.getName();
        _builder_1.append(_name);
        _builder_1.append("_c");
        _xifexpression_1 = _builder_1;
      } else {
        StringConcatenation _builder_2 = new StringConcatenation();
        String _name_1 = param.getName();
        _builder_2.append(_name_1);
        _builder_2.append("_c");
        _xifexpression_1 = _builder_2;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  public CharSequence JNIFunctionHeader(final EObject cm) {
    if (cm instanceof CMethod) {
      return _JNIFunctionHeader((CMethod)cm);
    } else if (cm instanceof Method) {
      return _JNIFunctionHeader((Method)cm);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(cm).toString());
    }
  }
  
  public CharSequence returnStatement(final Type t) {
    if (t instanceof StringType) {
      return _returnStatement((StringType)t);
    } else if (t instanceof VoidType) {
      return _returnStatement((VoidType)t);
    } else if (t instanceof EnumType) {
      return _returnStatement((EnumType)t);
    } else if (t instanceof ConstType) {
      return _returnStatement((ConstType)t);
    } else if (t instanceof PtrType) {
      return _returnStatement((PtrType)t);
    } else if (t != null) {
      return _returnStatement(t);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(t).toString());
    }
  }
  
  public CharSequence resultInstance(final Type t) {
    if (t instanceof StringType) {
      return _resultInstance((StringType)t);
    } else if (t instanceof VoidType) {
      return _resultInstance((VoidType)t);
    } else if (t instanceof StructType) {
      return _resultInstance((StructType)t);
    } else if (t instanceof TypeAlias) {
      return _resultInstance((TypeAlias)t);
    } else if (t instanceof PtrType) {
      return _resultInstance((PtrType)t);
    } else if (t != null) {
      return _resultInstance(t);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(t).toString());
    }
  }
  
  private CharSequence paramInstance(final Type type, final ParamDef param) {
    if (type instanceof StringType) {
      return _paramInstance((StringType)type, param);
    } else if (type instanceof TypeAlias) {
      return _paramInstance((TypeAlias)type, param);
    } else if (type instanceof PtrType) {
      return _paramInstance((PtrType)type, param);
    } else if (type != null) {
      return _paramInstance(type, param);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type, param).toString());
    }
  }
}
