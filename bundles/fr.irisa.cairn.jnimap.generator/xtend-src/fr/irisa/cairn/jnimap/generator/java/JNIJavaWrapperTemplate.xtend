package fr.irisa.cairn.jnimap.generator.java

import fr.irisa.cairn.jniMap.AliasToClassMapping
import fr.irisa.cairn.jniMap.ClassMapping
import fr.irisa.cairn.jniMap.EnumToClassMapping
import fr.irisa.cairn.jniMap.Mapping
import fr.irisa.cairn.jniMap.PtrType
import fr.irisa.cairn.jniMap.StructToClassMapping
import fr.irisa.cairn.jnimap.generator.CommonExtensions
import fr.irisa.cairn.jnimap.generator.TypeExtensions
import java.util.Set
import java.util.TreeSet
import org.eclipse.xtext.generator.IFileSystemAccess
import fr.irisa.cairn.jniMap.UnmappedClass
import fr.irisa.cairn.jniMap.InterfaceClass

class JNIJavaWrapperTemplate {
	
	extension CommonExtensions = new CommonExtensions();
	extension TypeExtensions = new TypeExtensions();
	extension JNIJavaUtilities = new JNIJavaUtilities();
	
	extension StructWrapperExtension structWrapper = new StructWrapperExtension();
	extension AliasWrapperExtension  aliasWrapper  = new AliasWrapperExtension();
	extension EnumWrapperExtension = new EnumWrapperExtension();
	extension PointerWrapperExtension = new PointerWrapperExtension();
	extension UnmappedWrapperExtension unmappedWrapepr = new UnmappedWrapperExtension();
	extension InterfaceWrapperExtention interfaceWrapper = new InterfaceWrapperExtention();
	
	def compile(Mapping m, IFileSystemAccess fsa){
		
		var Set<String> generatedFiles = new TreeSet<String>();
		
		//Generate wrappers for all c/c++ struct/class
		for (cm : m.classMapping) {
			fsa.generateFile(cm.classMappingFilepath(m), cm.classMappingContent(m))
			generatedFiles.add(cm.classMappingFilepath(m));
			
			if (cm instanceof StructToClassMapping) {
				val s2c = cm as StructToClassMapping
				if (!s2c.allSubClasses.empty) {
					fsa.generateFile(s2c.visitorFilepath(m), s2c.visitorContent(m))
				}
			}
		}
		
		
		//
//		val aliasMappings = m.classMapping.filter(AliasToClassMapping);
//		val aliasToPtrTypes = aliasMappings.filter([cm|(cm as AliasToClassMapping).alias.type instanceof PtrType]);
//		val definedPtrTypes = aliasToPtrTypes.map[cm|cm.alias.type];
//		//.filter([pt|definedPtrTypes.exists[x|x==pt]])

		//Generate wrappers for all pointers on primitive types
		//some of the pointer types are explicitly mapped by class mapping. don't generate them
		for (pt : m.allPrimitivePtrTypes.toIterable) {
			if (!generatedFiles.contains(pt.pointerMappingFilepath(m))) 
				fsa.generateFile(pt.pointerMappingFilepath(m), pt.pointerMappingContent(m))
		}
	}
	
	def String pointerMappingFilepath(PtrType pt, Mapping m)
		'''«javaRootPath»«m.packagePath»/«pt.standardJavaName».java'''
		
	
	def String classMappingFilepath(ClassMapping cm, Mapping m)
		'''«javaRootPath»«m.packagePath»/«cm.javaClassName».java'''
	
		
	
	//seems like dispatch doesn't work across files
    def dispatch classMappingContent(ClassMapping cm, Mapping m) {
    	System.err.println("[Error] Unsupported Mapping type: " + cm.class);
    	return ''''''
    }
    
    def dispatch classMappingContent(StructToClassMapping cm, Mapping m) {
    	return structWrapper.classMappingContent(cm, m)
    }
    
    def dispatch classMappingContent(EnumToClassMapping cm, Mapping m) {
    	return cm.enumToClassMappingContent(m);
    }
    
    def dispatch classMappingContent(AliasToClassMapping cm, Mapping m) {
    	return aliasWrapper.classMappingContent(cm, m)
    }
    
    def dispatch classMappingContent(UnmappedClass cm, Mapping m) {
    	return unmappedWrapepr.classMappingContent(cm, m)
    }
    
    def dispatch classMappingContent(InterfaceClass cm, Mapping m) {
    	return interfaceWrapper.classMappingContent(cm, m)
    }
}