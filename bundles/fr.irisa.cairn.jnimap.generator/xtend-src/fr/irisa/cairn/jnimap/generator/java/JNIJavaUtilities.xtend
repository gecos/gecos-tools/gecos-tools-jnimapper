package fr.irisa.cairn.jnimap.generator.java

import fr.irisa.cairn.jniMap.AliasToClassMapping
import fr.irisa.cairn.jniMap.ArrayType
import fr.irisa.cairn.jniMap.BooleanType
import fr.irisa.cairn.jniMap.ClassMapping
import fr.irisa.cairn.jniMap.ConstType
import fr.irisa.cairn.jniMap.EnumType
import fr.irisa.cairn.jniMap.GenericsDeclaration
import fr.irisa.cairn.jniMap.GenericsInstantiation
import fr.irisa.cairn.jniMap.InterfaceImplementation
import fr.irisa.cairn.jniMap.Mapping
import fr.irisa.cairn.jniMap.ParamDef
import fr.irisa.cairn.jniMap.PtrType
import fr.irisa.cairn.jniMap.StructToClassMapping
import fr.irisa.cairn.jniMap.Type
import fr.irisa.cairn.jniMap.UnmappedClass
import fr.irisa.cairn.jnimap.generator.CommonExtensions
import fr.irisa.cairn.jnimap.generator.TypeExtensions
import java.util.List
import fr.irisa.cairn.jniMap.InterfaceClass

class JNIJavaUtilities {
	
	extension TypeExtensions = new TypeExtensions();
	extension CommonExtensions = new CommonExtensions();
	
	def javaClassName(ClassMapping cm) {
		//must be made consistent with StandardJavaNameBuilder in javahelpers
		'''«cm.name.toFirstUpper»'''
	}
			
		
	private def javaQualifiedClassName(ClassMapping cm, Mapping m) {
		if (cm.enclosingMapping == m) {
			return '''«cm.javaClassName»''';
		} else {
			return '''«cm.root.packageName».«cm.javaClassName»''';
		}
	}
		
	def dispatch javaSuperClass(ClassMapping cm, Mapping m) {
		printSuperClass(null, null, m)
	}
	
	def dispatch javaSuperClass(AliasToClassMapping cm, Mapping m) {
		printSuperClass(cm.^super, cm.genericsInstantiation, m)
	}
	
	def dispatch javaSuperClass(StructToClassMapping cm, Mapping m) {
		printSuperClass(cm.^super, null, m)
	}
	
	def dispatch javaSuperClass(UnmappedClass cm, Mapping m) {
		printSuperClass(cm.^super, cm.genericsInstantiation, m)
	}
		
	def printSuperClass(ClassMapping _super, GenericsInstantiation gi, Mapping m) {
		if (_super === null)
			return '''JNIObject''';
			
		val gis = if (gi !== null && !gi.instanceNames.isEmpty) {
			gi.instanceNames.join('<', ', ', '>', [s|s])
		} else ''''''
			
		return '''«_super.javaQualifiedClassName(m)»«gis»'''
	}
		
	def javaVisitorName(ClassMapping cm)
		'''I«cm.name.toFirstUpper»Visitor'''
		
	def nativeParam(ParamDef pd) {
		pd.type.nativeParam(pd)
	}

	def nativeParamStatic(ParamDef pd) {
		pd.type.nativeParamStatic(pd);
	}
	
	def dispatch nativeParamStatic(Type t, ParamDef pd) {
		t.nativeParam(pd);
	}
	
	def dispatch nativeParamStatic(PtrType t, ParamDef pd) {
		if (t.primitivePtrType) {
			'''«pd.name»'''
		} else {
			'''getNativePtr(«pd.name»)'''
		}
	}
	
	def dispatch CharSequence nativeParam(Type t, ParamDef pd)
		'''«pd.name»'''
		
	def dispatch CharSequence nativeParam(BooleanType t, ParamDef pd)
		'''toInt(«pd.name»)'''
		
	def dispatch CharSequence nativeParam(PtrType t, ParamDef pd) {
		if (t.primitivePtrType) {
			'''«pd.name»'''
		} else {
			'''getNativePtr(«pd.name»)'''
		}
	}
		
	def dispatch CharSequence nativeParam(ConstType t, ParamDef pd)
		'''«t.base.nativeParam(pd)»'''
		
	def dispatch CharSequence nativeParam(ArrayType t, ParamDef pd)
		'''getNativePtr(«pd.name»)'''
		
	def dispatch CharSequence nativeParam(EnumType t, ParamDef pd)
		'''«pd.name».getValue()'''
		
	
	dispatch def isAbstract(ClassMapping cm) {
		false
	}
	
	dispatch def isAbstract(UnmappedClass cm) {
		cm.abstract
	}
	
	dispatch def isInterface(ClassMapping cm) {
		false
	}
	
	dispatch def isInterface(InterfaceClass cm) {
		true
	}
    	
	dispatch def genericsDeclarations(ClassMapping cm) {
		''''''
	}
	
	dispatch def genericsDeclarations(AliasToClassMapping cm) {
		cm.genericsDecl.printGenericsDecl
	}
	dispatch def genericsDeclarations(UnmappedClass cm) {
		cm.genericsDecl.printGenericsDecl
	}
	
	def printGenericsDecl(GenericsDeclaration gd) {
		if (gd !== null && !gd.names.isEmpty)
			gd.names.join('<', ', ', '>', [s|s])
		else
			''''''
	}
	
	dispatch def interfaceImplementations(ClassMapping cm, Mapping m) ''''''
	
	dispatch def interfaceImplementations(AliasToClassMapping cm, Mapping m) {
		cm.interfaces.printInterfaceImplementations(cm.isInterface, m)
	}
	
	dispatch def interfaceImplementations(UnmappedClass cm, Mapping m) {
		cm.interfaces.printInterfaceImplementations(cm.isInterface, m)
	}
	
	dispatch def interfaceImplementations(InterfaceClass cm, Mapping m) {
		cm.interfaces.printInterfaceImplementations(cm.isInterface, m)
	}
	
	def printInterfaceImplementations(List<InterfaceImplementation> interfaces, boolean isInterface, Mapping m) {
		if (interfaces.isEmpty) ''''''
		else interfaces.join(isInterface?'extends ':'implements ', ', ', '', [i|'''«printSuperClass(i.^interface, i.genericsInstantiation, m)»'''])
	}	
		
}