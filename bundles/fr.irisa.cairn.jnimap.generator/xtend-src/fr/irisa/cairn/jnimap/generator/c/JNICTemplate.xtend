package fr.irisa.cairn.jnimap.generator.c

import fr.irisa.cairn.jniMap.CMethod
import fr.irisa.cairn.jniMap.Mapping
import fr.irisa.cairn.jniMap.ParamDef
import fr.irisa.cairn.jniMap.UserModule
import fr.irisa.cairn.jnimap.generator.CommonExtensions
import fr.irisa.cairn.jnimap.generator.TypeExtensions
import org.eclipse.xtext.generator.IFileSystemAccess

/**
 * Mostly for UserModule related generation
 * 
 */
class JNICTemplate {
	
	extension CommonExtensions = new CommonExtensions();
	extension TypeExtensions = new TypeExtensions();
	extension JNICUtilities = new JNICUtilities();
	
	extension JNIClassMappingTemplate = new JNIClassMappingTemplate();
	
	//Correspond to CNativeMapper(Mapping) in the Xpand version
	def compile(Mapping m, IFileSystemAccess fsa){
		fsa.generateFile(m.commonFileFilepath, m.commonFileContent);
		fsa.generateFile(m.userModulesInterfaceFilepath, m.userModulesInterfaceContent);
		
		for (cm : m.classMapping) {
			cm.compile(m, fsa);
		}
		
		for (module : m.userModules) {
			fsa.generateFile(module.CModuleHeaderFilepath, module.CModuleHeaderContent);
			fsa.generateFile(module.CModuleFilepath, module.CModuleContent(m));
		}
		
		//This part was not understandable in the original Xpand
		//I believe it is never active 
		//«FOREACH this.methods.select(e|e.class==classMapping) AS method-»
		//«EXPAND JNIClassWrapperTemplate::cMethodDef(packageName) FOR method»
		//«ENDFOREACH-» 
//		
	}
	
	def prefix(Mapping m) {
		'''«m.packageName.replaceAll("\\.","_")»_«m.ID»Native'''
	}
	
	def commonFileFilepath(Mapping m) {
		cRootPath+m.name+"_common_native.c";
	}
	
	def CModuleFilepath(UserModule um) {
		cRootPath+um.identifier+".c"
	}
	
	def CModuleHeaderFilepath(UserModule um) {
		cRootPath+um.identifier+".h"
	}
	
	def userModulesInterfaceFilepath(Mapping m) {
		cRootPath+m.ID+"_UserModules.c"
	}
	
	
	
	def commonFileContent(Mapping m) '''
		«m.commonIncludes»
		
		#include "«m.nativeInterfaceHeader»"
		
		/**
		 *  Automatically generated by jnimap 
		 */
		
		void throwException(JNIEnv * env, char* msg) {
			jclass newExcCls;
			(*env)->ExceptionDescribe(env);
			(*env)->ExceptionClear(env);
			newExcCls = (*env)->FindClass(env,"«m.packagePath»/«m.ID»Exception");
			if (newExcCls == NULL) {
				/* Unable to find the exception class, give up. */
				return;
			}
			(*env)->ThrowNew(env, newExcCls, msg);
		}
		
		jobject createInteger(JNIEnv * env, int value) {
			// Create a object of type Integer
			jclass cls = (*env)->FindClass(env, "java/lang/Integer");
			jmethodID constructorId = (*env)->GetMethodID(env, cls, "<init>", "(I)V");
			jobject o = (*env)->NewObject(env, cls, constructorId, value);
			return o;
		}
		
		jint getIntegerValue(JNIEnv * env, jobject obj) {
			jclass cls = (*env)->FindClass(env, "java/lang/Integer");
			jmethodID intValueMethodId = (*env)->GetMethodID(env, cls, "intValue", "()I");
			long val = (*env)->CallIntMethod(env, obj, intValueMethodId);
			return (jint) val;
		}
		
		JNIEXPORT jboolean JNICALL Java_«m.prefix»_is_1null(JNIEnv * env, jlong ptr) {
			if(ptr==(GECOS_PTRSIZE NULL))
				return JNI_TRUE ;
			else
		    	return JNI_FALSE;
		}
		
		
		JNIEXPORT jlong JNICALL
				Java_«m.prefix»_derefPointer(JNIEnv * env, jclass class, jlong ptr) {
				jlong* jniptr= (jlong*) GECOS_PTRSIZE ptr;
				if(jniptr==NULL) throwException(env, "NPE  in pointerDeref");
				return *jniptr;
		}
	'''

	def CModuleContent(UserModule um, Mapping m) '''
		#include "«um.identifier+".h"»"
		
		/* PROTECTED REGION ID(«m.name»User_«um.name»_local) ENABLED START */
			/* Protected region for methods used locally in this file */
		/* PROTECTED REGION END */
		
		«FOR method : um.methods»
			«method.prototype» {
				/* PROTECTED REGION ID(«m.name+"User_"+um.name+"_"+method.name») ENABLED START */
				/* PROTECTED REGION END */
			}
		«ENDFOR»
	'''
	
	def CModuleHeaderContent(UserModule um) '''
		«um.root.commonIncludes»
		
		/* PROTECTED REGION ID(«um.identifier»_header) ENABLED START */
			 /* Custom includes for this module */
		/* PROTECTED REGION END */
		
		«FOR method : um.methods»
			«method.prototype»;
		«ENDFOR»
	'''
		
	def prototype(CMethod cm) {
		'''«cm.res.standardCName» «cm.name»(«cm.params.join(", ", [p|p.parameter])»)'''
	}
	
	def parameter(ParamDef pd) {
		'''«pd.type.standardCName» «pd.name»'''	
	}
	def userModulesInterfaceContent(Mapping m) '''
		«m.commonIncludes»
		
		#include "«m.nativeInterfaceHeader»"
		#include "«m.nativeInterfaceHeaderForUserModule»"
		
		
		«FOR module : m.userModules»
		«ENDFOR»
		
		«FOR module : m.userModules»
			/**********************************
			 ** «module.name»
			 **********************************/
			#include "«module.identifier».h"
			
			«FOR method : module.methods»
			//«module.name» . «method.name»
			«method.JNIFunctionHeader» {
				«FOR param : method.params»
					«param.castParam»
				«ENDFOR»
				
				«method.res.resultInstance» «method.name»(«method.params.join(", ", [p|p.paramInstance])»);
			«method.res.returnStatement»
			}
			«ENDFOR»
		«ENDFOR»
	'''

}