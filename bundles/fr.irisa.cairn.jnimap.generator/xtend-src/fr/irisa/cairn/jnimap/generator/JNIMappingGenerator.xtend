package fr.irisa.cairn.jnimap.generator

import fr.irisa.cairn.jniMap.Mapping
import fr.irisa.cairn.jnimap.generator.build.JNIMakefileTemplate
import fr.irisa.cairn.jnimap.generator.c.JNICTemplate
import fr.irisa.cairn.jnimap.generator.java.JNIJavaTemplate
import fr.irisa.cairn.jnimap.generator.java.JNIJavaWrapperTemplate
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator

/**
 * 
 * Naming convention:
 *  - xxxTemplate   : Main entry point for code generation. 
 *                    Each one of them have a "compile" method, which does all the job.
 *                    Only the compile method should have access to FileSystemAccess
 *  - xxxExtensions : Collection of helper methods 
 */
class JNIMappingGenerator implements IGenerator {
	
	
	extension JNICTemplate jniCtemplate = new JNICTemplate();
	extension JNIJavaTemplate jniJavaTemplate = new JNIJavaTemplate();
	extension JNIJavaWrapperTemplate jniJavaWrapperTemplate = new JNIJavaWrapperTemplate();
	extension JNIMakefileTemplate jniMakeTemplate = new JNIMakefileTemplate();
	
	override void doGenerate(Resource resource, IFileSystemAccess fsa) { // Compilation of the components of the resource one by one
		val mapping = resource.contents.get(0) as Mapping
		jniCtemplate.compile(mapping, fsa)
		jniJavaTemplate.compile(mapping, fsa)
		jniJavaWrapperTemplate.compile(mapping, fsa)
		jniMakeTemplate.compile(mapping, fsa)
	}
}
