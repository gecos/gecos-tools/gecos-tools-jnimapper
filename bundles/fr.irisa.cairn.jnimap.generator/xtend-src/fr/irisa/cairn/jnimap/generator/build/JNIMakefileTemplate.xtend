package fr.irisa.cairn.jnimap.generator.build

import fr.irisa.cairn.jniMap.Mapping
import org.eclipse.xtext.generator.IFileSystemAccess
import fr.irisa.cairn.jniMap.HostType

import fr.irisa.cairn.jnimap.generator.CommonExtensions
import fr.irisa.cairn.jniMap.UserModule
import fr.irisa.cairn.jniMap.ExternalLibrary

/**
 * Port of the MakefileTemplate2.xpt
 * 
 */
class JNIMakefileTemplate {
	
	extension CommonExtensions = new CommonExtensions();
	
	def String launcherFilepath(Mapping m) '''«cRootPath»/build/Makefile'''
	
	def String userFilepath(HostType host) '''«cRootPath»/build/«host.dirname»/user.mk'''
	
	def String librariesFilepath(HostType host) '''«cRootPath»/build/«host.dirname»/libraries.mk'''
	
	def String makefileFilepath(HostType host) '''«cRootPath»/build/«host.dirname»/Makefile'''
	
	def compile(Mapping m, IFileSystemAccess fsa) {
		fsa.generateFile(m.launcherFilepath, m.launcherContent());
		for (host : m.hosts) {
			fsa.generateFile(host.userFilepath, m.userContent(host));
			fsa.generateFile(host.librariesFilepath, m.librariesContent(host));
			fsa.generateFile(host.makefileFilepath, m.makefileContent(host));
		}
	}
	
	def checkHostTypeSupport(Mapping m, HostType t) {
		if (m.hosts.contains(t)) 1 else 0
	}
	
	//The top-level launcher that calls specialized makefiles for each host
	def CharSequence launcherContent(Mapping m) '''
		.PHONY: «m.hosts.join(" ", [host|host.dirname])»
		
		AUTODETECT=0
		SYSTEM=$(shell uname -s)
		ifeq ($(SYSTEM),Darwin)
			HOST=Macosx
			ARCH=$(shell sysctl -a hw.optional.x86_64 | cut -d" " -f2)
			ifeq ($(ARCH),1)
				ARCH=64
				AUTODETECT=«m.checkHostTypeSupport(HostType.MACOSX64)»
			else 
				ARCH=32
				AUTODETECT=«m.checkHostTypeSupport(HostType.MACOSX32)»
			endif
		else
			ifeq ($(SYSTEM),Linux)
				HOST=Linux
				ARCH=$(shell uname -m)
				ifeq ($(ARCH),x86_64)
					ARCH=64
					AUTODETECT=«m.checkHostTypeSupport(HostType.LINUX64)»
				else 
					ARCH=32
					AUTODETECT=«m.checkHostTypeSupport(HostType.LINUX32)»
				endif
			else
				AUTODETECT=0
			endif
		endif
		
		all:
		ifeq ($(AUTODETECT),1)
			@echo ">> Autodetecting host machine : $(HOST)_$(ARCH)"
			@$(MAKE) -s -f $(HOST)_$(ARCH)/Makefile clean
			@$(MAKE) -s -f $(HOST)_$(ARCH)/Makefile lib
		else
			@echo "Cannot detect host machine, or the host machine is unsupported. Please use one of the following :"
			«FOR host : m.hosts»
				@echo "    - make «host.dirname» "
			«ENDFOR»
		endif
		
		clean:
		ifeq ($(AUTODETECT),1)
			@echo ">> Autodetecting host machine : $(HOST)_$(ARCH)"
			@$(MAKE) -s -f $(HOST)_$(ARCH)/Makefile clean
		else
			@echo "Cannot detect host machine, or the host machine is unsupported. Please use one of the following :"
			«FOR host : m.hosts»
				@echo "    - make -s -f «host.dirname»/Makefile clean "
			«ENDFOR»
		endif
		
		«FOR host : m.hosts»
		«host.dirname» :
			@$(MAKE) -s -f ./«host.dirname»/Makefile clean
			@$(MAKE) -s -f ./«host.dirname»/Makefile lib
		«ENDFOR»
	'''

	//«REM»***************************************************** 
	//*
	//*			User specific paths
	//* 
	//***************************************************«ENDREM»
	def userContent(Mapping m, HostType host) '''
		#PROTECTED REGION ID(user_«host.dirname») ENABLED START#
		«IF host.isMac»
			JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Versions/Current
			JAVA_EXTRA_INC= -I$(JAVA_HOME)/Headers
			
			EXTRA_LIBS=
			EXTRA_INCDIR=
		«ELSE»
			EXTRA_CFLAGS=
			JAVA_HOME=/usr/lib/jvm/default-java/
			JAVA_EXTRA_INC= -I$(JAVA_HOME)/include/linux
			EXTRA_LIBS=
			EXTRA_INCDIR=
		«ENDIF»
		#PROTECTED REGION END#
		
		JAVA_H=$(JAVA_HOME)/bin/javah
	'''
	
	//«REM»***************************************************** 
	//*
	//*			User specific libraries
	//* 
	//***************************************************«ENDREM»
	def librariesContent(Mapping m, HostType host) '''
		#PROTECTED REGION ID(libraries_«host.dirname») ENABLED START#
		«FOR lib : m.libraries.filter[l|l.hasLibraryFilename(host)]»
			«lib.name»_INCDIR= 
			«lib.name»_LIBDIR=
		«ENDFOR»
		«IF !m.externalLibraries.empty»
			#Location of external jnimap with shared library
			«FOR exMapping : m.externalLibraries.map[exLib|exLib.mapping].toSet»
				«exMapping.name»_LOCATION=
			«ENDFOR»
			
			#Libraries used by other bindigs --- use the same includes
			«FOR exLib : m.externalLibraries.filter[l|l.hasLibraryFilename(host)]»
				«exLib.name»_INCDIR=
			«ENDFOR»
		«ENDIF»
		#PROTECTED REGION END#
		
		«IF !m.externalLibraries.empty»
			«FOR exLib : m.externalLibraries.filter[l|l.hasLibraryFilename(host)]»
				«exLib.name»_LIBDIR=${«exLib.mapping.name»_LOCATION}/lib/«exLib.mapping.libDir(host)»/
			«ENDFOR»
		«ENDIF»
	'''


	//«REM»***************************************************** 
	//*
	//*			All platforms make file content
	//* 
	//***************************************************«ENDREM»
	def makefileContent(Mapping m, HostType host) {
		val headerNames = m.nativeInterfaceHeader + " " + m.nativeInterfaceHeaderForUserModule;
		val DLLpath = host.DLLpath(m);
		val libName = host.libPath(m);
	
		return '''
		include «host.dirname»/user.mk
		include «host.dirname»/libraries.mk
		
		«host.makefilePreamble»
		
		OBJS=«m.classMapping.filter[cm|!cm.getMethods(m).isEmpty].join("", "\\\n\t", "\\\n", [cm|'''«m.name»_«cm.name»_native.o'''])»
			«m.name»_common_native.o \
			«m.name»_UserModules.o
		
		#Extra objects to be linked
		EXTRA_OBJS=«m.userModules.join(" \\\n\t", [um|'''«m.name»User_«um.name».o'''])»
		
		CFLAGS = -m$(ARCH) -fPIC -O3 -Wall -Wextra -Wno-unused-label -Wno-unused-parameter \
				$(JAVA_INC) -I./ -I../ $(EXTRA_CFLAGS) $(EXTRA_INCDIR) \
				«m.externalLibraries.filter[l|l.hasLibraryFilename(host)].join(" ", [exLib|'''-I$(«exLib.name»_INCDIR)'''])» \
				«m.libraries.filter[l|l.hasLibraryFilename(host)].join(" ", [lib|'''-I$(«lib.name»_INCDIR)'''])»
		
		LIBS = \
			-Wl,-rpath,./«DLLpath» \
			«m.externalLibraries.filter[l|l.hasLibraryFilename(host)].join(" \\\n", [exLib|'''./«DLLpath»/«getFilename(exLib,host).filename»'''])» \
			«m.libraries.filter[l|l.hasLibraryFilename(host)].join(" \\\n", [lib|'''./«DLLpath»/«getFilename(lib,host).filename»'''])» \
			$(EXTRA_LIBS)
		
		# Path to the «m.name»Native class file
		CLASSPATH=../../bin/
		
		V=@
		Q=
		
		all: 
			@echo usage :
			@echo  - make lib : compile «m.libname(host)»
			@echo  - make clean
		
		###############################
		#			Rules
		###############################
		
		clean: 
			rm -rf «libName» *.o ../*.o «headerNames» ../../lib/«m.libDir(host)»/*
			
		lib: «libName»
		
		#Final file to be created.
		«libName» : $(OBJS) $(EXTRA_OBJS) «IF m.libraries.size > 0 || m.externalLibraries.size > 0»libs_«host.toString()»«ENDIF»
			$(V)mkdir -p ../../lib/
			$(Q)@echo "  [LINK] $@"
			$(V)$(CC) $(CFLAGS) -o «libName» -shared $(OBJS) $(EXTRA_OBJS) $(LIBS)
			«IF m.libraries.size > 0 || m.externalLibraries.size > 0»
			$(V)cd ../../lib/ && \
			mkdir -p «m.libDir(host)»/ && \
			mv ../native/build/«DLLpath»/* ./«m.libDir(host)»/
			$(V)$(MAKE) -f «host.dirname»/Makefile fixInstallNames
			$(V)rm -rf ./«DLLpath»/
			«ENDIF»
		
		«IF m.libraries.size > 0 || m.externalLibraries.size > 0»
		libs_«host.toString()»:
			$(V)mkdir -p ./«DLLpath»
			«FOR exLib : m.externalLibraries.filter[e|hasLibraryFilename(e,host)]»
				$(V)cp $(«exLib.name»_LIBDIR)/«getFilename(exLib.library,host).filename» «DLLpath»/«getFilename(exLib.library,host).filename»
			«ENDFOR»
			«FOR lib : m.libraries.filter[e|hasLibraryFilename(e,host)]»
				$(V)cp $(«lib.name»_LIBDIR)/«getFilename(lib,host).filename» «DLLpath»/«getFilename(lib,host).filename»
			«ENDFOR»
		«ENDIF»
		
		«m.nativeInterfaceHeaderForUserModule» : «m.nativeInterfaceHeader»
		«m.nativeInterfaceHeader» : $(CLASSPATH)/«m.packagePath+"/"+m.ID+"Native.class"»
			$(Q)@echo "  [JAVAH] $@"
			$(V)$(JAVA_H) -classpath $(CLASSPATH)/ «m.packageName+"."+m.ID+"Native"»
			@echo "#define GECOS_PTRSIZE $(GECOS_PTRSIZE)" >> $@
		«FOR cm : m.classMapping»
			«m.name»_«cm.name»_native.o : ../«m.name»_«cm.name»_native.c «headerNames»
				$(Q)@echo "  [CC] $@"
				$(V)$(CC) $(CFLAGS) -c -o $@ ../«m.name»_«cm.name»_native.c
				
		«ENDFOR»
		
		«m.name»_common_native.o : ../«m.name»_common_native.c «headerNames»
			$(Q)@echo "  [CC] $@"
			$(V)$(CC) $(CFLAGS) -c -o $@ ../«m.name»_common_native.c
		
		«m.name»_UserModules.o : ../«m.name»_UserModules.c «headerNames»
			$(Q)@echo "  [CC] $@"
			$(V)$(CC) $(CFLAGS) -c -o $@ ../«m.name»_UserModules.c
		
		«FOR um : m.userModules»
			#PROTECTED REGION ID(make_«um.moduleID(host)») DISABLED START#
			«um.identifier».o : ../«um.identifier».c «headerNames»
				$(Q)@echo "  [CC] $@"
				$(V)$(CC) $(CFLAGS) -c -o $@ ../«um.identifier».c
			#PROTECTED REGION END#
			
		«ENDFOR»
		fixInstallNames:
			«host.fixInstallNames(m)»
		'''
	}

	def moduleID(UserModule um, HostType host) {
		return host.ID+"_"+um.identifier;
	}
	
	def makefilePreamble(HostType host) {
		if (host.isMac) {
			return host.makefilePreambleMacOSX;
		} else if (host.isLinux) {
			return host.makefilePreambleLinux;
		} else {
			return '''''';
		}
	}

	//«REM»***************************************************** 
	//*
	//*			OSX specific make file content
	//* 
	//***************************************************«ENDREM»
	def makefilePreambleMacOSX(HostType host) {
		
		var ptrSize = '''''';
		var arch = 0;
		
		if (host == HostType.MACOSX32) {
			ptrSize = "int";
			arch = 32;
		} else {
			ptrSize = "long";
			arch = 64;
		}
		
		return '''
			GECOS_PTRSIZE = («ptrSize»)
			ARCH = «arch»
			JAVA_INC=-I$(JAVA_HOME)/include $(JAVA_EXTRA_INC)
		''';
	}


	def fixInstallNameMac(Mapping m, String libfilename, String targetLibFilePath)'''
		@A=`otool -L «targetLibFilePath» | grep «libfilename» | colrm 1 8 | cut -d" " -f1`; \
			if [ "$$A" != "" ]; then \
				(if [ "$$A" != "@rpath/«libfilename»" ]; then \
					install_name_tool -change "$$A" "@rpath/«libfilename»" "«targetLibFilePath»"; \
				fi); fi
	'''
	
	def fixInstallNameLinux(HostType host, Mapping m, String targetLibFilePath) {
		'''patchelf --set-rpath ./«host.DLLpath(m)»/ «targetLibFilePath»'''
	}
	
	def fixInstallNames(HostType host, Mapping m) {
		if (host.isMac) {
			fixInstallNamesMac(host, m)
		} else if (host.isLinux) {
			fixInstallNamesLinux(host, m)
		} else  {
			'''@echo "  [RPATH FIX] not supported for this OS"'''
		}
	}
	
	def fixInstallNamesMac(HostType host, Mapping m) {
		val libName = host.libPath(m);
	
		return '''
			«FOR exLib : m.externalLibraries.filter[e|e.hasLibraryFilename(host)]»
				@echo "  [RPATH FIX] «exLib.name»"
				«m.fixInstallNameMac(exLib.getFilename(host).filename, libName)»
				«FOR otherlib : m.libraries.filter[l|l.hasLibraryFilename(host) && l.name != exLib.name]»
					«m.fixInstallNameMac(exLib.getFilename(host).filename, "../../lib/"+m.libDir(host)+"/"+getFilename(otherlib,host).filename)»
				«ENDFOR»
			«ENDFOR»
			«FOR lib : m.libraries.filter[e|e.hasLibraryFilename(host)]»
				@echo "  [RPATH FIX] «lib.name»"
				«m.fixInstallNameMac(lib.getFilename(host).filename, libName)»
				«FOR otherlib : m.libraries.filter[l|l.hasLibraryFilename(host) && l.name != lib.name]»
					«m.fixInstallNameMac(lib.getFilename(host).filename, "../../lib/"+m.libDir(host)+"/"+getFilename(otherlib,host).filename)»
				«ENDFOR»
			«ENDFOR»
		'''	
	}
	
	def fixInstallNamesLinux(HostType host, Mapping m) {
		return '''
			«FOR exLib : m.externalLibraries.filter[e|e.hasLibraryFilename(host)]»
				@echo "  [RPATH FIX] «exLib.name»"
				«host.fixInstallNameLinux(m, "../../lib/"+m.libDir(host)+"/"+getFilename(exLib,host).filename)»
			«ENDFOR»
			«FOR lib : m.libraries.filter[e|e.hasLibraryFilename(host)]»
				@echo "  [RPATH FIX] «lib.name»"
				«host.fixInstallNameLinux(m, "../../lib/"+m.libDir(host)+"/"+getFilename(lib,host).filename)»
			«ENDFOR»
		'''	
	}
	
	
	
	//«REM»***************************************************** 
	//*
	//*			Linux specific make file content
	//* 
	//***************************************************«ENDREM»
	def makefilePreambleLinux(HostType host) {
		var ptrSize = '''''';
		var arch = 0;
		
		if (host == HostType.LINUX32) {
			ptrSize = "int";
			arch = 32;
		} else {
			ptrSize = "long";
			arch = 64;
		}
		
		return '''
			GECOS_PTRSIZE = («ptrSize»)
			ARCH = «arch»
			JAVA_INC= -I$(JAVA_HOME)/include $(JAVA_EXTRA_INC)
		''';	
	}
	
	def private getName(ExternalLibrary exLib) {
		exLib.library.name
	}

}