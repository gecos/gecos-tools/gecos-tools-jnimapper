package fr.irisa.cairn.jnimap.generator.java

import fr.irisa.cairn.jniMap.Mapping
import fr.irisa.cairn.jniMap.PtrType
import fr.irisa.cairn.jnimap.generator.CommonExtensions
import fr.irisa.cairn.jnimap.generator.TypeExtensions

class PointerWrapperExtension {
	
	extension CommonExtensions = new CommonExtensions();
	extension TypeExtensions = new TypeExtensions();
	
	def pointerMappingContent(PtrType pt, Mapping m) '''
		/**
		 *  Automatically generated by jnimap 
		 */
		package «m.packageName»;
		
		public class «pt.standardJavaName» {
			
			public «pt.base.standardJavaName» value;

			/*** PROTECTED REGION ID(«pt.standardJavaName»_userCode) ENABLED START ***/
				/*
				
				Put you user code here !
				
			 	*/
			/*** PROTECTED REGION END ***/
		}
	'''
}