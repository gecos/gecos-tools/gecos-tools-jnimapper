package fr.irisa.cairn.jnimap.generator.c

import fr.irisa.cairn.jniMap.CMethod
import fr.irisa.cairn.jniMap.ConstType
import fr.irisa.cairn.jniMap.EnumType
import fr.irisa.cairn.jniMap.Mapping
import fr.irisa.cairn.jniMap.Method
import fr.irisa.cairn.jniMap.ParamDef
import fr.irisa.cairn.jniMap.PtrType
import fr.irisa.cairn.jniMap.StringType
import fr.irisa.cairn.jniMap.StructType
import fr.irisa.cairn.jniMap.Type
import fr.irisa.cairn.jniMap.TypeAlias
import fr.irisa.cairn.jniMap.VoidType
import fr.irisa.cairn.jnimap.generator.CommonExtensions
import fr.irisa.cairn.jnimap.generator.TypeExtensions

class JNICUtilities {
	
	extension CommonExtensions = new CommonExtensions();
	extension TypeExtensions = new TypeExtensions();
	
	def commonIncludes(Mapping m) '''
		#include <jni.h>
		#include <stdio.h>
		#include <stdlib.h>
		#include <string.h>
		
		«FOR include : m.includes»
			#include «include.include»
		«ENDFOR»
	'''
	
	def jniCprefix(Mapping m) {
		'''«m.packagePrefix»_«m.ID»Native'''
	}
	
	def dispatch JNIFunctionHeader(Method m) '''
		«m.JNIFunctionName»(JNIEnv *env, jclass class«m.activeParams.join(", ", ", ", "", [p|'''«p.type.JNICName» «p.name»'''])»)
	'''
	
	def private JNIFunctionName(Method m) '''
		JNIEXPORT «m.res.JNICName» JNICALL Java_«m.root.jniCprefix»_«m.name.replaceAll("_", "_1")»
	'''
	
	def dispatch JNIFunctionHeader(CMethod cm) '''
		«cm.JNIFunctionName»(JNIEnv *env, jclass clazz«cm.params.join(", ", ", ", "", [p|'''«p.type.JNICName» «p.name»'''])»)
	'''
	
	def private JNIFunctionName(CMethod m) '''
		JNIEXPORT «m.res.JNICName» JNICALL Java_«m.root.jniCprefix»_00024UserModules_«m.name.replaceAll("_", "_1")»
	'''
	
	def castParam(ParamDef pd) {
		val head = '''«pd.type.standardCName» «pd.name»_c = («pd.type.standardCName»)''';
		val tail = '''
			«IF pd.type.isStringType»
				(*env)->GetStringUTFChars(env,«pd.name»,0);
			«ELSEIF pd.type.isPointer»
				GECOS_PTRSIZE «pd.name»;
			«ELSE»
				«pd.name»;
			«ENDIF»
		'''
		
		return head + tail;
	}
	
	def dispatch CharSequence returnStatement(Type t) '''
		
			return («t.JNICName»)  res;
		error:
			return  («t.getJNICName») GECOS_PTRSIZE NULL;
	'''
	
	def dispatch CharSequence returnStatement(ConstType t) {
		t.base.returnStatement;
	}
	
	def dispatch CharSequence returnStatement(StringType t) '''
		
			return (*env)->NewStringUTF(env, res);
		
		error:
			return (jstring) GECOS_PTRSIZE NULL;
	'''

	
	def dispatch CharSequence returnStatement(VoidType t) '''
		error:
			return;
	'''

	def dispatch CharSequence returnStatement(PtrType t) '''
		
			return («t.JNICName») GECOS_PTRSIZE res;
		error:
			return  («t.JNICName») GECOS_PTRSIZE NULL;
	'''

	def dispatch CharSequence returnStatement(EnumType t) '''
		
			return («t.getJNICName»)  res;
		error:
			return -1000;
	'''
	
	def dispatch CharSequence resultInstance(Type t) 
		'''«t.standardCName» res = («t.standardCName»)'''

	def dispatch CharSequence resultInstance(StringType t) 
		'''«t.standardCName» res = («t.standardCName»)'''
		
	def dispatch CharSequence resultInstance(PtrType t) 
		'''«t.standardCName» res = («t.standardCName»)'''
		
	def dispatch CharSequence resultInstance(StructType t) ''''''
	//{
//«DEFINE resultInstance FOR StructType»«ERROR "in resultInstance "+this+" Not yet supported"»«ENDDEFINE»
	//}
		
	def dispatch CharSequence resultInstance(TypeAlias t) ''''''
	//{
//«DEFINE resultInstance FOR StructType»«ERROR "in resultInstance "+this+" Not yet supported"»«ENDDEFINE»
	//}
		
	def dispatch CharSequence resultInstance(VoidType t) ''''''
	
	def paramInstance(ParamDef param) {
		param.type.paramInstance(param);
	}
	
	//These are made private, since the type is already available in ParamDef,
	// and are only used for dispatch
	def dispatch private paramInstance(Type type, ParamDef param)
		'''«param.name»_c'''
		
	def dispatch private paramInstance(TypeAlias type, ParamDef param)
		'''*«param.name»_c'''
		
	def dispatch private paramInstance(StringType type, ParamDef param)
		'''«param.name»_c'''
		
	def dispatch private paramInstance(PtrType type, ParamDef param) {
		if (type.ignored) {
			return '''NULL'''
		} else if (type.primitivePtrType) {
			'''&«param.name»_c'''
		} else {
			'''«param.name»_c'''	
		}
	}
		
}
