package fr.irisa.cairn.jnimap.generator

import fr.irisa.cairn.jniMap.AliasToClassMapping
import fr.irisa.cairn.jniMap.BuiltInType
import fr.irisa.cairn.jniMap.ClassMapping
import fr.irisa.cairn.jniMap.ConstType
import fr.irisa.cairn.jniMap.EnumToClassMapping
import fr.irisa.cairn.jniMap.EnumType
import fr.irisa.cairn.jniMap.Mapping
import fr.irisa.cairn.jniMap.Method
import fr.irisa.cairn.jniMap.MethodGroup
import fr.irisa.cairn.jniMap.ParamDef
import fr.irisa.cairn.jniMap.PtrType
import fr.irisa.cairn.jniMap.StringType
import fr.irisa.cairn.jniMap.StructToClassMapping
import fr.irisa.cairn.jniMap.StructType
import fr.irisa.cairn.jniMap.Type
import fr.irisa.cairn.jniMap.TypeAlias
import fr.irisa.cairn.jniMap.TypeDeclaration
import fr.irisa.cairn.jniMap.VoidType
import javahelpers.JNICNameBuilder
import javahelpers.JNIJavaNameBuilder
import javahelpers.StandardCNameBuilder
import javahelpers.StandardJavaNameBuilder
import javahelpers.StringUtils
import org.eclipse.emf.common.util.BasicEList
import javahelpers.JNIFieldSignatureBuilder
import fr.irisa.cairn.jniMap.UnmappedClass

class TypeExtensions {
	
	extension CommonExtensions = new CommonExtensions();
	
	def String getPtrType() {
		return "long";	
	}
	
	def boolean isVoid(Method method) {
		return (method.res instanceof VoidType);
	}

	def boolean isStringType(Type type) {
		return (type instanceof StringType) || ((type instanceof ConstType) && (type as ConstType).base.stringType);
 	}

//boolean hasLibraryFilename(Library lib, HostType host) :
//	lib.filenames.select(e|e.os==host).size!=0;

//LibraryFileName getFilename(Library lib, HostType host) :
//	lib.filenames.select(e|e.os==host).first();

//def Mapping getRoot(Object obj) {
//	((Mapping)((EObject)obj).eRootContainer);
//}
	
	//only limited types of pointers are supported
	def getAllPrimitivePtrTypes(Mapping m) {
		m.eAllContents.filter(PtrType).filter([e|(e as PtrType).primitivePtrType])
	}
	
	def isPrimitivePtrType(Type type) {
		if (type instanceof PtrType) {
			return (type as PtrType).isPrimitivePtrType;
		}
		
		return false;
	}
	
	def isPrimitivePtrType(PtrType type) {
		return type.base instanceof BuiltInType && !(type.base instanceof VoidType) && !(type.base instanceof StringType) && type.indir.length==1
	}
	
	def getAllSubClasses(StructToClassMapping s2c) {
		getRoot(s2c).eAllContents.filter(StructToClassMapping).filter[e|e.^super==s2c].toList.sortBy[e|e.name];
	}

	def boolean isBuiltInTypePointer(Type type) {
		return (type instanceof PtrType) && ((type as PtrType).base instanceof BuiltInType);
	}
		
	def ClassMapping getMappingFor(Type type) {
		if (type instanceof ConstType) {
			return (type as ConstType).base.mappingFor;
		}
		if (type instanceof PtrType) {
			return (type as PtrType).base.mappingFor;
		}
		if (type instanceof TypeAlias) {
			return (type as TypeAlias).alias.eContainer as AliasToClassMapping;
		}
		if (type instanceof EnumType) {
			return (type as EnumType).name.eContainer as EnumToClassMapping;
		}
		if (type instanceof StructType) {
			return (type as StructType).name.eContainer as StructToClassMapping;
		}
		
		return null;
 	}
	 
	def ClassMapping getMappingFor(TypeDeclaration decl) {
		if (decl.eContainer instanceof ClassMapping) {
			decl.eContainer as ClassMapping;
		}
		
		return null;
	}
	
 	def boolean multipleIndirection(Type type) {
 		if (type.isPointer) {
 			return (type as ConstType).base.isPointer;
 		}
 		
		return (type instanceof PtrType) || (type instanceof TypeAlias);
 	}

	def boolean isPointer(Type type) {
		if (type instanceof ConstType) {
			return (type as ConstType).base.isPointer;
		}
		
		return (type instanceof PtrType) || (type instanceof TypeAlias);
	}

	def boolean isAlias(Type type)  {
		return type instanceof TypeAlias;
	}

	
//	def String dotToFileSeparator(List mapping) {
//	"" + mapping+"/"+dotToFileSeparator(mapping.reject(e|mapping.first()==e));
//	}
   
/*int getAttributeListSize(EClass class) :
	class.eSuperTypes.typeSelect(EClass).eAllAttributes
	//class.eAllAttributes.size;//+
	//eclass.eaddAll(getAttributeList(class.eAllSuperTypes.forAll(e|e.eAllAttributes));
	
/*
	let l = new List[] : l.addAll(class.eAllAttributes).addAll(class.eAllSuperTypes.forAll(e|e.eAllAttributes));
	*/
	
  
//String aJavaExtension(String param) : JAVA
//  javahelpers.StringUtils.upperCase(java.lang.String)
//;

/*
String dot2underscore(List<String> params) : JAVA
  test.StringUtils.dot2underscore(java.util.List<String>)
;
*/

	def String pointerPrefix(PtrType param) {
		return StringUtils.pointerPrefix(param);
	}

//Deprecated
//	def String getMethodIdentifier(Method m) {
//		return (m.root).methods.indexOf(m) + "";
//	}

	def getReturnTypeStandardJavaName(Method m) {
		return m.res.standardJavaName(m.mappedClass);
	}


	def String getMethodName(Method m) {
		if (m.newname !== null) {
			return m.newname;
		} else {
			return m.name.split("_").join("", [e|e.toFirstUpper()]).toFirstLower;
		}
	}
	
//deprecated
//private String concat(List[String] strings):JAVA
//  javahelpers.StringUtils.concat(java.util.List);
  
  //these javaHelpers should be turned into Xtend as well TODO
  
	def String standardJavaName(Type type) {
		val typeName = StandardJavaNameBuilder.nameFor(type);
		if (type.mappingFor !== null && type.mappingFor.enclosingMapping != type.root) {
			'''«type.mappingFor.enclosingMapping.packageName».«typeName»'''
		} else {
			'''«typeName»'''
		}
	}
	
	def dispatch String standardJavaName(Type type, ClassMapping cm) {
		type.standardJavaName;
	}
	def dispatch String standardJavaName(Type type, UnmappedClass cm) {
		val typeName = StandardJavaNameBuilder.nameFor(type);
		if (type.mappingFor.enclosingMapping != type.root && type.mappingFor != cm.^super) {
			'''«type.mappingFor.enclosingMapping.packageName».«typeName»'''
		} else {
			'''«typeName»'''
		}
	}
	 
	def String getJNIJavaName(Type type) {
		if (type.primitivePtrType) {
			return type.standardJavaName;
		} else {
			return JNIJavaNameBuilder.nameFor(type);
		}
	} 

	def String getStandardCName(Type type) {
		StandardCNameBuilder.nameFor(type); 
	}
	
	def String getJNICName(Type type) {
		if (type.primitivePtrType) {
			return "jobject";
		} else {
			return JNICNameBuilder.nameFor(type)
		}
	}
	
	def String getJNIFieldGetterName(PtrType type) {
		if (type.primitivePtrType) {
			return '''Get«type.base.standardJavaName.toFirstUpper»Field''';
		} else {
			System.err.println("[Error] not a primitive pointer type");
			return "[Error] not a primitive pointer type";
		}
	}
	
	def String getJNIFieldSetterName(PtrType type) {
		if (type.primitivePtrType) {
			return '''Set«type.base.standardJavaName.toFirstUpper»Field''';
		} else {
			System.err.println("[Error] not a primitive pointer type");
			return "[Error] not a primitive pointer type";
		}
	}
	
	def String getJNIFieldSignature(PtrType type) {
		JNIFieldSignatureBuilder.nameFor(type.base);
	}
	
//	def javaParam(ParamDef pd)
//		'''«pd.type.standardJavaName» «pd.name»'''
		
	def javaParam(ParamDef pd, ClassMapping cm)
		'''«pd.type.standardJavaName(cm)» «pd.name»'''
	
	
	//Various types of method registered for classMapping
	def constructors(MethodGroup mg) {
		if (mg === null) {
			return new BasicEList<Method>();
		} else { 
			return mg.methods.filter[c|c.constructor && c.res.mappingFor==mg.class_];
		}
	}
	
	def destructors(MethodGroup mg) {
		if (mg === null) {
			return new BasicEList<Method>();
		} else { 
			return mg.methods.filter[c|c.destructor]
		}
	}
	
	def staticMethods(MethodGroup mg) {
		if (mg === null) {
			return new BasicEList<Method>();
		} else { 
			return mg.methods.filter[e|!e.destructor && !e.constructor && !e.^instanceof && e.static];
		}
	}
	
	def memberMethod(MethodGroup mg) {
		if (mg === null) {
			return new BasicEList<Method>();
		} 
			
		if (mg.class_ instanceof StructToClassMapping) {
			val cm = mg.class_ as StructToClassMapping;
			return mg.methods.filter[e|!e.destructor && !e.constructor && !e.^instanceof && !e.static && e.params.size>0 && 
				(getMappingFor(e.params.get(e.arg).type)==cm || getMappingFor(e.params.get(e.arg).type)==cm.^super)]
				
		} else {
			val cm = mg.class_;
			return mg.methods.filter[e|!e.destructor && !e.constructor && !e.^instanceof && !e.static && e.params.size>0 && e.params.get(e.arg).type.mappingFor==cm]
		}
	}
		
}