package fr.irisa.cairn.jnimap.generator.c

import fr.irisa.cairn.jniMap.AliasToClassMapping
import fr.irisa.cairn.jniMap.ClassMapping
import fr.irisa.cairn.jniMap.ClassToClassMapping
import fr.irisa.cairn.jniMap.EnumToClassMapping
import fr.irisa.cairn.jniMap.FieldDef
import fr.irisa.cairn.jniMap.Mapping
import fr.irisa.cairn.jniMap.Method
import fr.irisa.cairn.jniMap.StringType
import fr.irisa.cairn.jniMap.StructDeclaration
import fr.irisa.cairn.jniMap.StructToClassMapping
import fr.irisa.cairn.jniMap.StructType
import fr.irisa.cairn.jniMap.Type
import fr.irisa.cairn.jniMap.TypeAlias
import fr.irisa.cairn.jnimap.generator.CommonExtensions
import fr.irisa.cairn.jnimap.generator.TypeExtensions
import org.eclipse.xtext.generator.IFileSystemAccess
import fr.irisa.cairn.jniMap.PtrType
import fr.irisa.cairn.jniMap.UnmappedClass

/**
 * Loosely corresponds to JNIClassWrapperTemplate.xpt
 * 
 */
class JNIClassMappingTemplate {
	
	extension CommonExtensions = new CommonExtensions();
	extension TypeExtensions = new TypeExtensions();
	extension JNICUtilities = new JNICUtilities();
	
	extension JNICParamExtensions = new JNICParamExtensions();
	
	def compile(ClassMapping cm, Mapping m, IFileSystemAccess fsa) {
		if (!cm.getMethods(m).isEmpty)
			fsa.generateFile(cm.ClassMappingFilepath(m), cm.ClassMappingContent(m));
	}
	
	def ClassMappingFilepath(ClassMapping cm, Mapping m) {
		cRootPath+m.name+"_"+cm.name+"_native.c"
	}
	
	def ClassMappingContent(ClassMapping cm, Mapping m) '''
		«m.commonIncludes»
		
		«FOR module : m.userModules»
			#include "«module.identifier».h"
		«ENDFOR»
		
		#include "«m.nativeInterfaceHeader»"

		extern void throwException(JNIEnv * env, char* msg);
		extern jobject createInteger(JNIEnv * env, int value);
		extern jint getIntegerValue(JNIEnv * env, jobject obj);
		
		«cm.cGetterSetter»
		
		«FOR method : cm.getMethods(m)»
			«method.cMethodDef»
		«ENDFOR»
		
		
	'''

	def cMethodDef(Method m) '''
		«m.JNIFunctionHeader» {
		#ifdef TRACE_ALL
			printf("Entering «m.name»\n");fflush(stdout);
		#endif
			«FOR param : m.params»
				«param.type.localVarParam(param.name, m.name)»
			«ENDFOR»
		
			«m.res.resultInstance» «m.name»(«m.params.join(", ", [p|p.paramInstance])»);
		
			«FOR param : m.params»
				«param.type.localVarParamCleanup(param.name, m.name)»
			«ENDFOR»
		
		#ifdef TRACE_ALL
			printf("Leaving «m.name»\n");fflush(stdout);
		#endif
			
		«m.res.returnStatement»
		}
	'''
	

	def dispatch cGetterSetter(StructToClassMapping cm) '''
		«FOR field : cm.struct.fields»
			«field.cStructFieldGetter»
			
			«field.cStructFieldSetter»
			
			«field.cStructFieldTester»
			
		«ENDFOR»
	'''
	
	def dispatch cGetterSetter(AliasToClassMapping cm) {
		''''''
	}
	def dispatch cGetterSetter(ClassToClassMapping cm) {
		''''''
	}
	def dispatch cGetterSetter(EnumToClassMapping cm) {
		''''''
	}
	def dispatch cGetterSetter(UnmappedClass cm) {
		''''''
	}
	
	def cStructFieldTester(FieldDef fd) {
		val struct = fd.eContainer as StructDeclaration;
		val mapping = fd.root;
		
		return '''
			JNIEXPORT jint JNICALL
			Java_«mapping.packagePrefix»_«mapping.ID»Native_«struct.name.replaceAll("_","_1")»_1test_1«fd.name.replaceAll("_","_1")»
				(JNIEnv *env, jclass class, jlong ptr) {
				/* PROTECTED REGION ID(«struct.name»_«fd.name»_tester) DISABLED START */
				struct «struct.name»* stPtr = (struct «struct.name» *) GECOS_PTRSIZE ptr;
				if(stPtr==NULL)
					throwException(env, "Null Pointer in get«fd.ID»");
				«fd.testerResultStatement»
				/* PROTECTED REGION END */
			}
		'''
	}

	def cStructFieldSetter(FieldDef fd) {
		val struct = fd.eContainer as StructDeclaration;
		val mapping = fd.root;
		
		return '''
			JNIEXPORT void JNICALL
			Java_«mapping.packagePrefix»_«mapping.ID»Native_«struct.name»_1set_1«fd.name.replaceAll("_","_1")»
				(JNIEnv *env, jclass class, jlong ptr, «fd.type.JNICName» value) {
				/* PROTECTED REGION ID(«struct.name»_«fd.name»_setter) DISABLED START */
				struct «struct.name»* stPtr = (struct «struct.name» *) GECOS_PTRSIZE ptr;
				if(stPtr==NULL)
					throwException(env, "Null Pointer in set«fd.ID»");
				«fd.setterResultStatement»
				/* PROTECTED REGION END */
			}
		'''
	}
	
	def setterResultStatement(FieldDef fd) {
		fd.type.setterResultStatement(fd);	
	}
	
	//These methods are made private, since type is available from FieldDef,
	// and are only used for dispatch
	def dispatch private setterResultStatement(Type type, FieldDef fd)
		'''stPtr->«fd.name»= («type.standardCName») GECOS_PTRSIZE value;'''
		
	def dispatch private setterResultStatement(StructType type, FieldDef fd)
		'''memcpy(value, &stPtr->«fd.name», sizeof(«type.standardCName»));'''
		
	def dispatch private setterResultStatement(TypeAlias type, FieldDef fd)
		'''memcpy(value, &stPtr->«fd.name», sizeof(«type.standardCName»));'''
		
	def dispatch private setterResultStatement(StringType type, FieldDef fd) '''
		char* str_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, value, NULL);
		strcpy(str_c, stPtr->«fd.name»);
	'''


	def cStructFieldGetter(FieldDef fd) {
		val struct = fd.eContainer as StructDeclaration;
		val mapping = fd.root;
		
		return '''
			JNIEXPORT «fd.type.JNICName» JNICALL
			Java_«mapping.packagePrefix»_«mapping.ID»Native_«struct.name.replaceAll("_","_1")»_1get_1«fd.name.replaceAll("_","_1")»
				(JNIEnv *env, jclass class, jlong ptr) {
				/* PROTECTED REGION ID(«struct.name»_«fd.name»_getter) DISABLED START */
				struct «struct.name»* stPtr = (struct «struct.name» *) GECOS_PTRSIZE ptr;
				if(stPtr==NULL)
					throwException(env, "Null Pointer in get«fd.ID»");
				«fd.getterResultStatement»
				/* PROTECTED REGION END */
			}
		'''
	}
	
	def getterResultStatement(FieldDef fd) {
		fd.type.getterResultStatement(fd);
	}
	
	def dispatch private getterResultStatement(Type type, FieldDef fd)
		'''return («type.JNICName») GECOS_PTRSIZE stPtr->«fd.name»;'''
		
	def dispatch private getterResultStatement(PtrType type, FieldDef fd) {
		if (fd.copy)
			'''return («type.JNICName») GECOS_PTRSIZE «type.base.standardCName»_copy(stPtr->«fd.name»);'''
		else 
			'''return («type.JNICName») GECOS_PTRSIZE stPtr->«fd.name»;'''
	}
		
	def dispatch private getterResultStatement(StructType type, FieldDef fd)
		'''return («type.JNICName») GECOS_PTRSIZE (& stPtr->«fd.name»);'''
		
	def dispatch private getterResultStatement(TypeAlias type, FieldDef fd)
		'''return («type.JNICName») GECOS_PTRSIZE (& stPtr->«fd.name»);'''
		
	def dispatch private getterResultStatement(StringType type, FieldDef fd)
		'''return (*env)->NewStringUTF(env, stPtr->«fd.name»);'''


	def testerResultStatement(FieldDef fd) {
		fd.type.testerResultStatement(fd);
	}
	
	def dispatch private testerResultStatement(Type type, FieldDef fd)
		'''return (GECOS_PTRSIZE stPtr->«fd.name») != (GECOS_PTRSIZE NULL);'''
		
	def dispatch private testerResultStatement(PtrType type, FieldDef fd) {
		if (fd.copy)
			'''return (GECOS_PTRSIZE «type.base.standardCName»_copy(stPtr->«fd.name»)) != (GECOS_PTRSIZE NULL);'''
		else 
			'''return (GECOS_PTRSIZE stPtr->«fd.name») != (GECOS_PTRSIZE NULL);'''
	}
		
	def dispatch private testerResultStatement(StructType type, FieldDef fd)
		'''return (GECOS_PTRSIZE (& stPtr->«fd.name»)) != (GECOS_PTRSIZE NULL);'''
		
	def dispatch private testerResultStatement(TypeAlias type, FieldDef fd)
		'''return (GECOS_PTRSIZE (& stPtr->«fd.name»)) != (GECOS_PTRSIZE NULL);'''
		
	def dispatch private testerResultStatement(StringType type, FieldDef fd)
		'''return (GECOS_PTRSIZE stPtr->«fd.name») != (GECOS_PTRSIZE NULL);'''

}

