package fr.irisa.cairn.jnimap.generator.c

import fr.irisa.cairn.jniMap.BuiltInType
import fr.irisa.cairn.jniMap.ConstType
import fr.irisa.cairn.jniMap.EnumType
import fr.irisa.cairn.jniMap.IntegerType
import fr.irisa.cairn.jniMap.PtrType
import fr.irisa.cairn.jniMap.StringType
import fr.irisa.cairn.jniMap.StructType
import fr.irisa.cairn.jniMap.Type
import fr.irisa.cairn.jniMap.TypeAlias
import fr.irisa.cairn.jnimap.generator.TypeExtensions

class JNICParamExtensions {
	
	extension TypeExtensions = new TypeExtensions();
	
	def dispatch CharSequence localVarParam(Type t, String varName, String methodName) ''''''
	
	def dispatch CharSequence localVarParam(StringType t, String varName, String methodName) '''
		char* «varName»_c;
		«varName»_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, «varName», NULL);
		if («varName»_c==NULL) {
			throwException(env, "GetStringUTFChars Failed  in «methodName» for parameter «varName»");
			goto error;
		}
	'''
	
	def dispatch CharSequence localVarParam(IntegerType t, String varName, String methodName) '''
		«t.standardCName» «varName»_c = («t.standardCName») «varName»;
	'''
	
	def dispatch CharSequence localVarParam(BuiltInType t, String varName, String methodName) '''
		«t.standardCName» «varName»_c = («t.standardCName») «varName»;
	'''
	
	def dispatch CharSequence localVarParam(ConstType t, String varName, String methodName) {
		t.base.localVarParam(varName, methodName)
	}
	
	def dispatch CharSequence localVarParam(TypeAlias t, String varName, String methodName) '''
		«t.standardCName» «varName»_c = («t.standardCName») «varName»; 
		if(((void*)«varName»_c)==NULL) {
			throwException(env, "Null pointer in «methodName» for parameter «varName»");
			goto error;
		}
	'''
	
	def dispatch CharSequence localVarParam(StructType t, String varName, String methodName) '''
		«t.standardCName» «varName»_c = («t.standardCName») «varName»; 
		if(((void*)«varName»_c)==NULL) {
			throwException(env, "Null pointer in «methodName» for parameter «varName»");
			goto error;
		}
	'''
	
	def dispatch CharSequence localVarParam(PtrType t, String varName, String methodName) {
		if (t.ignored) return '''''';
		
		if (t.primitivePtrType) {
			val typeName = t.base.standardCName;
			val clsName = varName+"_cls"
			val fidName = varName+"_fid"
			return '''
				jclass «clsName» = (*env)->GetObjectClass(env, «varName»); 
				jfieldID «fidName» = (*env)->GetFieldID(env, «clsName», "value", "«t.JNIFieldSignature»");
				«typeName» «varName»_c = («typeName») (*env)->«t.JNIFieldGetterName»(env, «varName», «fidName»);
			'''
		}
		
		return '''
			«t.standardCName» «varName»_c = («t.standardCName») GECOS_PTRSIZE «varName»; 
			if(((void*)«varName»_c)==NULL) {
				throwException(env, "Null pointer in «methodName» for parameter «varName»");
				goto error;
			}
		'''
	}
	
	def dispatch CharSequence localVarParam(EnumType t, String varName, String methodName) '''
		«t.standardCName» «varName»_c = («t.standardCName») «varName»;
	'''
	
	def dispatch localVarParamCleanup(Type t, String varName, String methodName) ''''''
	def dispatch localVarParamCleanup(StringType t, String varName, String methodName) 
		'''(*env)->ReleaseStringUTFChars(env, «varName», «varName»_c);'''
		
	def dispatch localVarParamCleanup(PtrType t, String varName, String methodName) {
		if (t.primitivePtrType) {
			val fidName = varName+"_fid"
			return '''
				(*env)->«t.JNIFieldSetterName»(env, «varName», «fidName», «varName»_c);
			'''
		}
		
		return ''''''
	}
		
	
}