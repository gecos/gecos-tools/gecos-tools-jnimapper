package fr.irisa.cairn.jnimap.generator.java

import fr.irisa.cairn.jniMap.ClassMapping
import fr.irisa.cairn.jniMap.Mapping
import fr.irisa.cairn.jniMap.UnmappedClass
import fr.irisa.cairn.jnimap.generator.CommonExtensions
import fr.irisa.cairn.jnimap.generator.java.JNIJavaUtilities;

class UnmappedWrapperExtension extends AbstractWrapper {
	
	extension CommonExtensions = new CommonExtensions();
	extension JNIJavaUtilities = new JNIJavaUtilities();
	
	override nativePointerManagement(ClassMapping cm, Mapping m) '''
		«IF !cm.isInterface»
			/* @generated */
			protected «cm.javaClassName»(long ptr) {
				/*** PROTECTED REGION ID(«cm.name»_Constructor) DISABLED START ***/
				super(ptr);
				/*** PROTECTED REGION END ***/
			}
		«ENDIF»
	'''
	
	override importSuperClass(ClassMapping cm, Mapping m) {
		val sClass = (cm as UnmappedClass).^super
		if (sClass !== null && sClass.enclosingMapping != m) {
			'''import «sClass.enclosingMapping.packageName».«sClass.javaClassName»;'''
		} else {
			''''''
		}
	}
	
	override memberMethodBodies(ClassMapping cm, Mapping m) ''''''
}