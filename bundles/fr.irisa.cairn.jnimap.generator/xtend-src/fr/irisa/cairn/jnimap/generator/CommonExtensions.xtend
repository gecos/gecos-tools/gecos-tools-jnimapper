package fr.irisa.cairn.jnimap.generator

import fr.irisa.cairn.jniMap.ClassMapping
import fr.irisa.cairn.jniMap.FieldDef
import fr.irisa.cairn.jniMap.HostType
import fr.irisa.cairn.jniMap.Library
import fr.irisa.cairn.jniMap.LibraryFileName
import fr.irisa.cairn.jniMap.Mapping
import fr.irisa.cairn.jniMap.Method
import fr.irisa.cairn.jniMap.MethodGroup
import fr.irisa.cairn.jniMap.PtrType
import fr.irisa.cairn.jniMap.UserModule
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.ecore.EObject
import fr.irisa.cairn.jniMap.ExternalLibrary

class CommonExtensions {
	
	
	def javaRootPath() '''./src-gen/'''
	def cRootPath() '''./native/'''
	
	def ID(Mapping m) {
		m.name.toFirstUpper;
	}
	
	def ID(HostType host) {
		host.toString.toFirstUpper;
	}
	
	def ID(FieldDef fd) {
		fd.name.toFirstUpper;
	}
	
	def EObject eRootContainer(EObject e) {
		if (e.eContainer === null) {
			e;
		} else {
			e.eContainer.eRootContainer;
		}	
	}
	
	def Mapping getRoot(Object obj) {
		(obj as EObject).eRootContainer as Mapping;
	}
	
	def Mapping getEnclosingMapping(Object obj) {
		if ((obj as EObject).eContainer instanceof Mapping) {
			return (obj as EObject).eContainer as Mapping;
		}
		return (obj as EObject).eContainer.enclosingMapping;
	}
	
	def ClassMapping getMappedClass(Method m) {
		(m.eContainer as MethodGroup).class_;
	}
	
	
	def String packageName(Mapping m) {
		if (m.package !== null && m.package.length > 0) 
			return m.package;
		
		return '''fr.irisa.cairn.jnimap.«m.name.toLowerCase»''';
	}
	
	def packagePath(Mapping m) {
		 m.packageName.replaceAll("\\.", "/");
	}
	
	def packagePrefix(Mapping m) {
		m.packageName.replaceAll("\\.","_");
	}
	
	def DLLpath(HostType host, Mapping m) {
		".jnimap.temp."+host.toString();
		//"."+m.packageName+".temp."+host.toString();
	}
	
	def nativeInterfaceHeader(Mapping m) {
		 m.packagePrefix+"_"+m.name+"Native.h";
	}
	
	def nativeInterfaceHeaderForUserModule(Mapping m) {
		 m.packagePrefix+"_"+m.name+"Native_UserModules.h";
	}
	  
	def libName(HostType host, Mapping m) {
		return host.toString+"_libjni"+m.name.toLowerCase+".so";
	}
	  
	def libPath(HostType host, Mapping m) {
		return "../../lib/"+host.libName(m);
	}
	
	def identifier(UserModule um) {
		return um.root.name+"User_"+um.name;
	}
	
	final def CharSequence dirname(HostType host) { return host.ID }

	final def boolean isMac(HostType host) {
		return (host==HostType.MACOSX64 || host==HostType.MACOSX32);
	}
	
	final def boolean isLinux(HostType host) {
		return (host==HostType.LINUX64 || host==HostType.LINUX32);
	}
	
	
	final def boolean hasLibraryFilename(Library lib, HostType host) {
		return !(lib.filenames.filter[e|e.os==host].empty);
	}
	
	
	final def LibraryFileName getFilename(Library lib, HostType host) {
		return lib.filenames.filter[e|e.os==host].head;
	}
	
	final def boolean hasLibraryFilename(ExternalLibrary exLib, HostType host) {
		return exLib.library.hasLibraryFilename(host);
	}
	
	final def LibraryFileName getFilename(ExternalLibrary exLib, HostType host) {
		return exLib.library.getFilename(host);
	}
	
	
	final def libname(Mapping m, HostType host) {
		return '''«host.toString»_libjni«m.name.toLowerCase».so''';
	}
	
	final def libDir(Mapping m, HostType host) {
		return '''«m.name»_«host.toString»''';
	}
	
	def getMethodGroup(ClassMapping cm, Mapping m) {
		return m.methodGroups.findFirst[mg|mg.class_ == cm]
	}
	
	def getMethods(ClassMapping cm, Mapping m) {
		val mg = m.methodGroups.findFirst[mg|mg.class_ == cm]
		if (mg === null) {
			return new BasicEList<Method>();
		} else {
			mg.methods;
		}
	}
	
	def getActiveParams(Method method) {
		method.params.filter[p|!(p.type instanceof PtrType && (p.type as PtrType).ignored)]
	}
	
	def visibility(Method method) {
		if (method.private)
			return '''private'''
		if (method.protected)
			return '''protected'''
			
		return '''public'''
	}
}