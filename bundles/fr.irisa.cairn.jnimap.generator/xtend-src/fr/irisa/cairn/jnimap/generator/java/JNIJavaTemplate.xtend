package fr.irisa.cairn.jnimap.generator.java

import fr.irisa.cairn.jniMap.CMethod
import fr.irisa.cairn.jniMap.ClassMapping
import fr.irisa.cairn.jniMap.EnumToClassMapping
import fr.irisa.cairn.jniMap.HostType
import fr.irisa.cairn.jniMap.Mapping
import fr.irisa.cairn.jniMap.Method
import fr.irisa.cairn.jniMap.ParamDef
import fr.irisa.cairn.jniMap.StructToClassMapping
import fr.irisa.cairn.jniMap.UserModule
import fr.irisa.cairn.jnimap.generator.CommonExtensions
import fr.irisa.cairn.jnimap.generator.TypeExtensions
import java.util.LinkedHashSet
import java.util.Set
import org.eclipse.xtext.generator.IFileSystemAccess

/**
 * This class is responsible for the interface to C and library loading.
 * The main portion is nativeInterfaceContent, which defines XXXNative.java
 * 
 * The Java classes corresponding to C objects (wrappers) are generated
 * by JNIJavaWrapperTemplate.
 */
class JNIJavaTemplate {
	
	extension CommonExtensions = new CommonExtensions();
	extension TypeExtensions = new TypeExtensions();
	
	def compile(Mapping m, IFileSystemAccess fsa){
		fsa.generateFile(m.abstractPlatformFilepath, m.abstractPlatformContent);
		for (HostType host : m.hosts) {
			fsa.generateFile(m.concretePlatformFilepath(host), m.concretePlatformContent(host));
		}
		fsa.generateFile(m.nativeInterfaceFilepath, m.nativeInterfaceContent);
		fsa.generateFile(m.exceptionFilepath, m.exceptionContent);
		
		//seems like no longer in use
		//fsa.generateFile(m.nativeTestFilepath, m.nativeTestContent);
	}
	
	def String abstractPlatformFilepath(Mapping m) 
		'''«javaRootPath»«m.packagePath»/platform/JNI«m.ID»AbstractPlatform.java'''
		
	def String concretePlatformFilepath(Mapping m, HostType host)
		'''«javaRootPath»«m.packagePath»/platform/JNI«m.ID»«host.ID».java'''
		
	def String nativeInterfaceFilepath(Mapping m)
		'''«javaRootPath»«m.packagePath»/«m.ID»Native.java'''

	def String nativeTestFilepath(Mapping m)
		'''«javaRootPath»«m.packagePath»/«m.ID»NativeTest.java'''
		
	def String exceptionFilepath(Mapping m)
		'''«javaRootPath»«m.packagePath»/«m.ID»Exception.java'''
		
	def abstractPlatformContent(Mapping m) '''
		package «m.packageName».platform;

		import java.io.File;
		import java.io.FileOutputStream;
		import java.io.InputStream;
		import java.net.URL;
		
		public abstract class JNI«m.ID»AbstractPlatform {
		
			abstract public void loadPlatformLibraries();

			abstract protected String getDllPath();
		
			protected File copyLibToTemp(String location, String libName) {
				String path = File.separator+location+libName;
				try {
					File dir = new File(getDllPath()).getAbsoluteFile();
					dir.mkdirs();
					dir.deleteOnExit();
					URL resource = JNI«m.ID»AbstractPlatform.class.getResource(path);
					InputStream inputStream = resource.openStream();
					
					// Copy resource to filesystem in a temp folder with a unique name
					String dllPath = dir.getAbsolutePath()+File.separator+libName;
					File temporaryDll = new File(dllPath);
					//skip if the library is already loaded
					if (temporaryDll.exists()) return temporaryDll;
					
					FileOutputStream outputStream = new FileOutputStream(temporaryDll);
					byte[] array = new byte[8192];
					int read = 0;
					while ( (read = inputStream.read(array)) > 0)
						outputStream.write(array, 0, read);
					outputStream.close();  
			
					// Delete on exit the dll
					temporaryDll.deleteOnExit();
					
					return temporaryDll;
				} catch (Exception e) {
					throw new RuntimeException("Could not copy library : " + path,e);
				}
			}
		}
	'''
	
	def concretePlatformContent(Mapping m, HostType host) '''
		package «m.packageName».platform;

		import java.io.File;

		public class JNI«m.ID»«host.ID» extends JNI«m.ID»AbstractPlatform {

			@Override
			protected String getDllPath() {
				return new File(".")+File.separator+"./«host.DLLpath(m)»";
			}

			public void loadPlatformLibraries() {
				// Get input stream from jar resource
				String lib = "«host.libName(m)»";
				try {
					//Copy other dynamic libraries from jar to temporary location
					«FOR lib : m.libraries.filter[l|l.hasLibraryFilename(host)]»
						//Copy «lib.name»
						copyLibToTemp("«m.name+"_"+host.toString()»" + File.separator, "«getFilename(lib,host).filename»");
					«ENDFOR»
					//Copy the binding object file from jar to temporary location
					File JNIDLL = copyLibToTemp("", lib);
					// Finally, load the dll
					System.load(JNIDLL.getAbsolutePath());
				} catch (Exception e) {
					throw new RuntimeException("Problem during native library loading «m.ID»:",e);
				}
			}
		}
	'''
	
	def checkHostTypeSupport(Mapping m, HostType t) {
		val platformName = '''«t.literal.toFirstUpper»'''
		
		if (m.hosts.contains(t))
			'''loader = new JNI«m.ID»«platformName»();'''
		else 
			'''throw new RuntimeException("«platformName» is not supported.");'''
	}
	
	def nativeInterfaceContent(Mapping m) {
		val Set<Mapping> extMaps = new LinkedHashSet()
		m.externalLibraries.forEach[e|extMaps.add(e.getMapping())]
		return '''
		package «m.packageName»;

		import «m.packageName».platform.*;
		«FOR extMap : extMaps »
		import «extMap.packageName».«extMap.ID»Native;
		«ENDFOR»
		
		public class «m.ID»Native {
		
			private static final String arch = System.getProperty("sun.arch.data.model");
		
			private static JNI«m.ID»AbstractPlatform loader ;
		
			private static boolean loaded = false;
			public static void loadLibrary() {
				if (loaded) return;
				«FOR extMap : extMaps »
				«extMap.ID»Native.loadLibrary();
				«ENDFOR»
				String osName = System.getProperty("os.name");
				String arch   = System.getProperty("sun.arch.data.model");
				if (osName.equals("Linux")) {
					if (arch.equals("32")) {
						«m.checkHostTypeSupport(HostType.LINUX32)»
					} else {
						«m.checkHostTypeSupport(HostType.LINUX64)»
					}
				} else if (osName.equals("Windows XP")) {
					«m.checkHostTypeSupport(HostType.CYGWIN32)»
				} else if (osName.equals("Mac OS X")) {
					if (arch.equals("32")) {
						«m.checkHostTypeSupport(HostType.MACOSX32)»
					} else {
						«m.checkHostTypeSupport(HostType.MACOSX64)»
					} 
				} else  {
					throw new RuntimeException("Unsupported Operating system : " + osName+"_"+arch);
				}
				loader.loadPlatformLibraries();
				loaded = true;
			}
			
			static {
				loadLibrary();
			}
		
			public static native long derefPointer(long ptr);
			
			
			/*************************************************************
			 *	                     USER MODULES                        *
			 *************************************************************/
			 
			 public static class UserModules {
			 	«FOR module : m.userModules»
			 		«module.javaUserModule»
			 	«ENDFOR»
			}
			
			/*************************************************************
			 *	                        METHODS                          *
			 *************************************************************/
			 «FOR mg : m.methodGroups»
			 	«FOR method : mg.methods»
			 		«method.javaMethodDef»
			 	«ENDFOR»
			 «ENDFOR»
			 
			/*************************************************************
			 *	                    CLASS MAPPING                        *
			 *************************************************************/
			 «FOR cm : m.classMapping»
			 	«cm.javaGetterSetter»
			 «ENDFOR»
			
«««			/*************************************************************
«««			 *	                    POINTER TYPES                        *
«««			 *************************************************************/
«««NOT IMPLEMENTED; it didn't seem like it worked in the Xpand version either

«««			 «FOR pt : m.allPtrTypes»
«««			 «ENDFOR»

«««			«LET getAllPtrTypes(this) AS tmpTypeList»
«««			«LET tmpTypeList.without(tmpTypeList) AS typeList»
«««			«FOREACH getAllPtrTypes(this) AS ptrType»
«««			«REM»«ERROR "Ptr found "+ptrType»«ENDREM»
«««			«EXPAND JNIDerefFunctionDef(typeList) FOR ptrType»«ENDFOREACH»«ENDLET»«ENDLET»
		}
	'''
	}
	
	def nativeTestContent(Mapping m) '''
		package «m.name.toLowerCase()»;
		
		public class «m.ID»NativeTest {
		
			public static void main(String args[]) {
				«m.ID»Native.test();
			} 
		
		}
	'''
	
	def exceptionContent(Mapping m) '''
		package «m.packageName»;
		
		public class «m.ID»Exception extends Exception {
			private static final long serialVersionUID = 1L;
			public «m.ID»Exception(String msg) {
				super(msg);
			} 
		}
	'''
	
	def dispatch javaGetterSetter(ClassMapping cm) {
		System.err.println("ClassMapping "+cm.class+" not supported in javaGetterSetter");
		''''''
	}

	def dispatch javaGetterSetter(EnumToClassMapping ecm) ''''''

	def dispatch javaGetterSetter(StructToClassMapping scm) '''
		«FOR field : scm.struct.fields»
			«IF field.type === null»
				«System.err.println("[ERROR] Field "+field.name+" for "+scm.name+ " is null")»
			«ELSE»
				// getter for «scm.name».«field.name»
				public static native «getJNIJavaName(field.type)» «scm.struct.name»_get_«field.name»(long ptr);
				// tester for «scm.name».«field.name»
				public static native int «scm.struct.name»_test_«field.name»(long ptr);
				// setter for «scm.name».«field.name»
				public static native void «scm.struct.name»_set_«field.name»(long ptr, «field.type.JNIJavaName» value);
			«ENDIF»
		«ENDFOR»
	'''	
	
	def javaUserModule(UserModule um) '''
		// UserModule : «um.name»
		«FOR method : um.methods»
			«method.javaMethodDef»
		«ENDFOR»
	'''
	
	def javaMethodDef(Method method) 
		'''public static native «method.res.JNIJavaName» «method.name»(«method.activeParams.join(", ", [p|p.javaParamDef])»);'''
	
	def javaMethodDef(CMethod method) 
		'''public static native «method.res.JNIJavaName» «method.name»(«method.params.join(", ", [p|p.javaParamDef])»);'''
	
	def javaParamDef(ParamDef pd) 
		'''«pd.type.JNIJavaName» «pd.name»'''
}