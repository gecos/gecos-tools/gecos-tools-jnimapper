package javahelpers;

import fr.irisa.cairn.jniMap.ArrayType;
import fr.irisa.cairn.jniMap.BaseType;
import fr.irisa.cairn.jniMap.EnumType;
import fr.irisa.cairn.jniMap.PtrType;
import fr.irisa.cairn.jniMap.StructType;
import fr.irisa.cairn.jniMap.Type;
import fr.irisa.cairn.jniMap.TypeAlias;

public class JNIJavaNameBuilder extends TypeNameBuilder{

	public static JNIJavaNameBuilder singleton =null;  

	public static String nameFor(Type type) {
		if(singleton==null)
			singleton= new JNIJavaNameBuilder();
		return singleton.doSwitch(type);
	}
	
	@Override
	public String caseEnumType(EnumType object) {
		return "int";
	}

	@Override
	public String casePtrType(PtrType object) {
		return "long";
	}

	@Override
	public String caseStructType(StructType object) {
		// must pass a pointer !
		return "/* implicit & */ long";
	}

	@Override
	protected String caseDouble() {
		return "double";
	}

	@Override
	protected String caseFloat() {
		return "float";
	}

	@Override
	protected String caseInt() {
		return "int";
	}

	@Override
	protected String caseChar() {
		return "char";
	} 

	@Override
	protected String caseLong() {
		return "long";
	}

	@Override
	protected String caseString() {
		return "String";
	}

	@Override
	protected String caseUnsigned() {
		return "int";
	}

	@Override
	protected String caseVoid() {
		return "void";
	}
	
	@Override
	protected String caseBoolean() {
		return "int";
	}
	
	@Override
	public String caseArrayType(ArrayType object) {
		if (object.getBase() instanceof BaseType) {
			return doSwitch(object.getBase())+"[]";
		} else return super.caseArrayType(object);
	}

	@Override
	public String caseTypeAlias(TypeAlias object) {
		// must pass a pointer !
		return "/* implicit & */ long";
	}




}
