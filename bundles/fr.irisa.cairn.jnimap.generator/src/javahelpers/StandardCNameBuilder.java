package javahelpers;

import fr.irisa.cairn.jniMap.ArrayType;
import fr.irisa.cairn.jniMap.EnumType;
import fr.irisa.cairn.jniMap.PtrType;
import fr.irisa.cairn.jniMap.StructType;
import fr.irisa.cairn.jniMap.Type;
import fr.irisa.cairn.jniMap.TypeAlias;

public class StandardCNameBuilder extends TypeNameBuilder{

	public static StandardCNameBuilder singleton =null;  

	public static String nameFor(Type type) {
		if(singleton==null)
			singleton= new StandardCNameBuilder();
		return singleton.doSwitch(type);
	}

	@Override
	public String caseEnumType(EnumType object) {
		return "enum "+object.getName().getName();
	}

	@Override
	public String casePtrType(PtrType object) {
		int size = object.getIndir().size();
		String res = "";
		while(size>0) {
			res+="*";
			size--;
		}
		return doSwitch(object.getBase())+res;
	}

	@Override
	public String caseStructType(StructType object) {
		return "struct " +object.getName().getName();
	}

	@Override
	public String caseTypeAlias(TypeAlias object) {
		return object.getAlias().getName();
	}

	@Override
	protected String caseDouble() {
		return "double";
	}

	@Override
	protected String caseFloat() {
		return "float";
	}

	@Override
	protected String caseInt() {
		return "int";
	}

	@Override
	protected String caseChar() {
		return "char";
	}

	@Override
	protected String caseLong() {
		return "long";
	}

	@Override
	protected String caseString() {
		return "char *";
	}

	@Override
	protected String caseUnsigned() {
		return "unsigned int";
	}

	@Override
	protected String caseVoid() {
		return "void";
	}
	
	@Override
	protected String caseBoolean() {
		return "int";
	}

	@Override
	public String caseArrayType(ArrayType object) {
		return doSwitch(object.getBase())+"*";
	}


}
