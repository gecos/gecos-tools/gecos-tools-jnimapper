package javahelpers;

import fr.irisa.cairn.jniMap.ArrayType;
import fr.irisa.cairn.jniMap.BaseType;
import fr.irisa.cairn.jniMap.EnumType;
import fr.irisa.cairn.jniMap.PtrType;
import fr.irisa.cairn.jniMap.StructType;
import fr.irisa.cairn.jniMap.Type;
import fr.irisa.cairn.jniMap.TypeAlias;

public class JNICNameBuilder extends TypeNameBuilder{

	public static JNICNameBuilder singleton =null;  

	public static String nameFor(Type type) {
		if(singleton==null)
			singleton= new JNICNameBuilder();
		return singleton.doSwitch(type);
	}
	

	
	@Override
	public String caseEnumType(EnumType object) {
		return "jint";
	}

	@Override
	public String casePtrType(PtrType object) {
		return "jlong";
	}

	@Override
	public String caseStructType(StructType object) {
		
		throw new UnsupportedOperationException("No Native type for structures (here "+object.getName().getName()+","+object.eContainer()+"), must use pointers instead");
	}

	@Override
	public String caseTypeAlias(TypeAlias object) {
		throw new UnsupportedOperationException("No Native type for alias (here "+object.getAlias().getName()+"), must use pointers instead");
	}

	@Override
	protected String caseDouble() {
		return "jdouble";
	}

	@Override
	protected String caseFloat() {
		return "jfloat";
	}

	@Override
	protected String caseInt() {
		return "jint";
	}

	@Override
	protected String caseChar() {
		return "jchar";
	}

	@Override
	protected String caseLong() {
		return "jlong";
	}

	@Override
	protected String caseString() {
		return "jstring";
	}

	@Override
	protected String caseUnsigned() {
		return "jint";
	}

	@Override
	protected String caseVoid() {
		return "void";
	}
	
	@Override
	protected String caseBoolean() {
		return "jint";
	}

	@Override
	public String caseArrayType(ArrayType object) {
		if (object.getBase() instanceof BaseType) {
			return "jlong";
		} else return super.caseArrayType(object);
	}


}
