package javahelpers;

import fr.irisa.cairn.jniMap.AliasToClassMapping;
import fr.irisa.cairn.jniMap.ArrayType;
import fr.irisa.cairn.jniMap.BuiltInType;
import fr.irisa.cairn.jniMap.EnumToClassMapping;
import fr.irisa.cairn.jniMap.EnumType;
import fr.irisa.cairn.jniMap.PtrType;
import fr.irisa.cairn.jniMap.StructToClassMapping;
import fr.irisa.cairn.jniMap.StructType;
import fr.irisa.cairn.jniMap.Type;
import fr.irisa.cairn.jniMap.TypeAlias;

public class StandardJavaNameBuilder extends TypeNameBuilder{

	public static StandardJavaNameBuilder singleton =null;  

	public static String nameFor(Type type) {
		if(singleton==null)
			singleton= new StandardJavaNameBuilder();
		return singleton.doSwitch(type);
	}
	
	@Override
	public String caseEnumType(EnumType object) {
		return ((EnumToClassMapping)object.getName().eContainer()).getName();
	}

	@Override
	public String casePtrType(PtrType object) {
		int size = object.getIndir().size();
		if(size>1) {
			String res = "";
			while(size>1) {
					res+="Ptr";
					size--;
			}
			return "JNI"+res+doSwitch(object.getBase());
		} else {
			if(object.getBase() instanceof BuiltInType) {
				return "JNIPtr"+toFirstUpper(doSwitch(object.getBase()));
			} else {
				return doSwitch(object.getBase());
			}
		}
		
	}

	@Override
	public String caseStructType(StructType object) {
		return ((StructToClassMapping)object.getName().eContainer()).getName();
	}


	@Override
	protected String caseDouble() {
		return "double";
	}

	@Override
	protected String caseFloat() {
		return "float";
	}

	@Override
	protected String caseInt() {
		return "int";
	}

	@Override
	protected String caseChar() {
		return "char";
	}

	@Override
	protected String caseLong() {
		return "long";
	}

	@Override
	protected String caseString() {
		return "String";
	}

	@Override
	protected String caseUnsigned() {
		return "int";
	}

	@Override
	protected String caseVoid() {
		return "void";
	}
	
	@Override
	protected String caseBoolean() {
		return "boolean";
	}

	@Override
	public String caseArrayType(ArrayType object) {
		return "JNI"+doSwitch(object.getBase()).toUpperCase()+"Array";
	}

	@Override
	public String caseTypeAlias(TypeAlias object) {
		return ((AliasToClassMapping)object.getAlias().eContainer()).getName();
	}



}
