package javahelpers;

import java.util.List;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.jniMap.PtrType;

public class StringUtils {
	
	public static String dot2underscore(EList<String> packageStr) {
		String out = "";
		for (int i = 0; i < packageStr.size(); i++) {
			out+="_"+packageStr;
		}
		return out;
 	}

	public static String upperCase(String in) {
		return in;
 	}
 
	public static String dotTounderScore(String in) {
		return in;
 	}

	public static String pointerPrefix(PtrType in) {
		String out = "";
		for(int i=0;i<in.getIndir().size()-1;i++){
			out+="Ptr";
		}
		return out;
 	}
	
	public static String concat(List<String> strings){
		StringBuffer sb = new StringBuffer();
		for(String s: strings){
			sb.append(s);
		}
		return sb.toString();
	}
}
