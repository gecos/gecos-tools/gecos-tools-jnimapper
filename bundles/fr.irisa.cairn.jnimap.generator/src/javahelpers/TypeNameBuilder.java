package javahelpers;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.jniMap.BuiltInType;
import fr.irisa.cairn.jniMap.ConstType;
import fr.irisa.cairn.jniMap.Mapping;
import fr.irisa.cairn.jniMap.Type;
import fr.irisa.cairn.jniMap.TypeAlias;
import fr.irisa.cairn.jniMap.TypeAliasDeclaration;
import fr.irisa.cairn.jniMap.util.JniMapSwitch;

public abstract class TypeNameBuilder extends JniMapSwitch<String>{

	
	protected  String toFirstUpper(String string) {
		return (""+string.charAt(0)).toUpperCase()+string.substring(1);
	}

	public Mapping getRootMapping(EObject obj) {
		while(!(obj instanceof Mapping) && obj.eContainer()!=null) {
			obj=obj.eContainer();
		}
		if(obj instanceof Mapping) 
			return (Mapping)obj; 
		else 
			return null;
	}

	@Override
	public String caseBuiltInType(BuiltInType object) {
		String name=object.getName();
		if (name.equals("void")) {
			return caseVoid();
		} else if (name.equals("string")) {
			return caseString();
		} else if (name.equals("unsigned")) {
			return caseUnsigned();
		} else if (name.equals("int")) {
			return caseInt();
		} else if (name.equals("char")) {
			return caseLong();
		} else if (name.equals("long")) {
			return caseLong();
		} else if (name.equals("float")) {
			return caseFloat();
		} else if (name.equals("double")) {
			return caseDouble();
		} else if (name.equals("boolean")) {
			return caseBoolean();
		} else {
			throw new UnsupportedOperationException("Support for type "+object.getName()+" is not yet implemented");
		}
	}

	protected abstract String caseDouble() ;
	protected abstract String caseFloat();
	protected abstract String caseLong();
	protected abstract String caseInt();
	protected abstract String caseChar();
	protected abstract String caseUnsigned();
	protected abstract String caseString() ;
	protected abstract String caseVoid();
	protected abstract String caseBoolean();

	@Override
	public String caseConstType(ConstType object) {
		return doSwitch(object.getBase());
	}


	@Override
	public String caseType(Type object) {
		throw new UnsupportedOperationException("Type "+object.getClass().getSimpleName()+" is not supported in "+this.getClass().getSimpleName());	
	}

	@Override
	public String caseTypeAlias(TypeAlias object) {
		throw new UnsupportedOperationException("Type "+object.getClass().getSimpleName()+" is not supported in "+this.getClass().getSimpleName());	
	}

	@Override
	public String caseTypeAliasDeclaration(TypeAliasDeclaration object) {
		throw new UnsupportedOperationException("Type "+object.getClass().getSimpleName()+" is not supported in "+this.getClass().getSimpleName());	
	}

	@Override
	public String defaultCase(EObject object) {
		throw new UnsupportedOperationException("Type "+object.getClass().getSimpleName()+" is not supported in "+this.getClass().getSimpleName());	
	}


}
