package javahelpers;

import fr.irisa.cairn.jniMap.Type;

public class JNIFieldSignatureBuilder extends TypeNameBuilder {

	public static JNIFieldSignatureBuilder singleton =null;  

	public static String nameFor(Type type) {
		if(singleton==null)
			singleton= new JNIFieldSignatureBuilder();
		return singleton.doSwitch(type);
	}
	

	@Override
	protected String caseDouble() {
		return "D";
	}

	@Override
	protected String caseFloat() {
		return "F";
	}

	@Override
	protected String caseLong() {
		return "J";
	}

	@Override
	protected String caseInt() {
		return "I";
	}

	@Override
	protected String caseChar() {
		return "C";
	}

	@Override
	protected String caseUnsigned() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String caseString() {
		throw new UnsupportedOperationException();
	}

	@Override
	protected String caseVoid() {
		throw new UnsupportedOperationException();
	}

	@Override
	protected String caseBoolean() {
		return "Z";
	}
	

}
