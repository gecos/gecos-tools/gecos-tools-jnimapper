package fr.irisa.cairn.jnimap.generator;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.xpand2.output.FileHandle;
import org.eclipse.xpand2.output.PostProcessor;

public class CBeautifier implements PostProcessor {

	public void beforeWriteAndClose(FileHandle info) {
		if (info.getAbsolutePath() != null && info.getAbsolutePath().endsWith(".c")) {
			BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(info.getBuffer().toString().getBytes())));
			StringBuffer sb = new StringBuffer();
			try {
				boolean skip = false;
				int uselessCount= 0;
				while (br.ready()) {
					String line = br.readLine();
					
					if (line.matches("^[\t\\s\r\n]*$")) {
						uselessCount++;
						if (uselessCount > 2) {
							skip = true;
						}
					} else {
						skip = false;
						uselessCount = 0;
					}
					if (!skip) {
						sb.append(line);
						sb.append("\n");
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			info.setBuffer(sb);
			
//			String buffer = info.getBuffer().toString();
//			int prevLength = buffer.length();
//			do {
//				buffer = buffer.replaceAll("^\t*\r$", "\n").replaceAll("^\t*\n$", "\n").replaceAll("\n\n\n", "\n\n");
//				if (prevLength > buffer.length()) {
//					prevLength = buffer.length();
//				} else {
//					break;
//				}
//						
//			} while (true);
//			
//			info.setBuffer(buffer);
//			info.setBuffer(info.getBuffer().toString().replaceAll("^\t*\r$", "\n").replaceAll("^\t*\n$", "\n").replaceAll("\n\n\n", "\n\n"));
//			info.setBuffer(info.getBuffer().toString().replaceAll("\n\n\n", "\n\n"));
		}
	}

	public void afterClose(FileHandle impl) {
		// TODO Auto-generated method stub
	}
}